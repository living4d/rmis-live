<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 11/11/2019
 * Time: 15:45
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


/**
 * App\Perm
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Perm newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Perm newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Perm query()
 * @mixin \Eloquent
 */
class Perm extends Model
{
    //return perms
public static function get_perms_all()
    {
        $classes = array();
        $perm_classes = DB::table("perm_classes")->orderBy('sequence', 'ASC')->get();

        foreach ($perm_classes as $key => $value) {
            $methods = DB::table("perm_methods")->where("class_id", $value->id)->get();

            foreach ($methods as $k => $v) {
                $classes[$value->name][$v->name] = array($value->id, $v->id, $v->name);
            }
        }
        return $classes;
    }


}