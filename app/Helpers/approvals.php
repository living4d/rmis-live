<?php
//

use App\Models\Calls\CallApplicationApproval;
use App\Models\Projects\Project;
use App\Models\Projects\ProjectActivityFundApproval;
use App\Models\Projects\ProjectActivityReport;
use App\Models\Projects\ProjectFundRequestApproval;

/*==================================================
Application Approval Levels
==================================================*/
//show application approval status
if (!function_exists('show_application_approval_status')) {
    function show_application_approval_status($app_id)
    {
        $approval_levels = ['hod' => 'HoD', 'principal' => 'Principal', 'drp' => 'DRP'];

        foreach ($approval_levels as $key => $value) {
            //check if level is approved
            $call_approval = CallApplicationApproval::where(['application_id' => $app_id, 'approval_level' => $key, 'status' => 1])->first();

            if ($call_approval) {
                echo '<i class="fa fa-check"></i>  ' . $value . '<br/>';
            } else {
                echo '<i class="fa fa-times"></i>  ' . $value . '<br/>';
            }
        }
    }
}

//show_operation
if (!function_exists('show_application_approval_operation')) {
    function show_application_approval_operation($app_id)
    {
        //call approval levels
        $levels = [];

        //approval levels
        $approval_levels = ['hod' => 'HoD', 'principal' => 'Principal', 'drp' => 'DRP'];

        foreach ($approval_levels as $key => $value) {
            array_push($levels, strtolower($key));
        }

        //logged user roles
        $roles = Auth::user()->roles;

        $serial = 1;
        foreach ($roles as $role) {
            $key = array_search($role->name, $levels);

            if ($key !== false) {
                //1. check for approve status = 1
                $call_approval = CallApplicationApproval::where(['application_id' => $app_id, 'approval_level' => $role->name, 'status' => 1])->first();

                if ($call_approval) {
                    //check if there is more than role
                    if (count($roles) > 1)
                        $serial++; //increment to another role
                } else {
                    //if key == 0 means first to approve
                    if ($key == 0) {
                        echo '<a href="#approveModal" id="btnApproveModal-' . $app_id . '" data-backdrop="static" data-keyboard="false" data-toggle="modal"  data-target="#approveModal" title="Approve application" data-app-id=' . $app_id . ' data-role-name=' . $role->name . ' class="btn btn-outline-success btn-xs"><i class="fa fa-check-square fa-2x text-success"></i></a>';
                    } else {
                        $prev_key = $key - 1; //previous key
                        $prev_role = $levels[$prev_key]; //previous role

                        //2. check the previous actor to see the current role should follow for approval
                        $prev_call_approval = CallApplicationApproval::where(['application_id' => $app_id, 'approval_level' => $prev_role, 'status' => 1])->first();

                        if ($prev_call_approval)
                            echo '<a href="#approveModal" id="btnApproveModal-' . $app_id . '" data-backdrop="static" data-keyboard="false" data-toggle="modal"  data-target="#approveModal" title="Approve application" data-app-id=' . $app_id . ' data-role-name=' . $role->name . ' class="btn btn-outline-success btn-xs"><i class="fa fa-check-square fa-2x text-success"></i></a>';
                    }
                }
            }
        }
    }
}



/*==================================================
Project Fund Approval Levels
==================================================*/
if (!function_exists('show_new_approval_notification')) {
    function show_new_approval_notification()
    {
        //call approval levels
        $levels = [];

        //approval levels
        $approval_levels = [
            'program_coordinator' => 'Program Coordinator',
            'drp' => 'Director of Research and Publication',
            'dvc_research' => 'Deputy Vice Chancellor - Research'
        ];

        foreach ($approval_levels as $key => $value) {
            array_push($levels, strtolower($key));
        }

        //logged user roles
        $roles = Auth::user()->roles;

        $number_of_new_notification = 0;
        foreach ($roles as $role) {
            $key = array_search($role->name, $levels);

            if ($key !== false) {
                $fund_approval = ProjectFundRequestApproval::where(['approval_level' => $role->name, 'status' => 0])->count();
            } else {
                $fund_approval = 0;
            }

            //add new notification
            $number_of_new_notification += $fund_approval;
        }

        return $number_of_new_notification;
    }
}


//show fund approval status
if (!function_exists('show_fund_approval_status')) {
    function show_fund_approval_status($project_fund_request_id)
    {
        $approval_levels = [
            'program_coordinator' => 'Program Coordinator',
            'drp' => 'Director of Research and Publication',
            'dvc_research' => 'Deputy Vice Chancellor - Research'
        ];

        foreach ($approval_levels as $key => $value) {
            //check if level is approved
            $approval = ProjectFundRequestApproval::where(['project_fund_request_id' => $project_fund_request_id, 'approval_level' => $key, 'status' => 1])->first();

            if ($approval)
                echo '<i class="fa fa-check"></i>  ' . $value . '<br/>';
            else
                echo '<i class="fa fa-times"></i>  ' . $value . '<br/>';
        }
    }
}

//show fund approval operation
if (!function_exists('show_fund_approval_operation')) {
    function show_fund_approval_operation($project_fund_request_id)
    {
        //call approval levels
        $levels = [];

        //approval levels
        $approval_levels = [
            'program_coordinator' => 'Program Coordinator',
            'drp' => 'Director of Research and Publication',
            'dvc_research' => 'Deputy Vice Chancellor - Research'
        ];

        foreach ($approval_levels as $key => $value) {
            array_push($levels, strtolower($key));
        }

        //logged user roles
        $roles = Auth::user()->roles;

        $serial = 1;
        foreach ($roles as $role) {
            $key = array_search($role->name, $levels);

            if ($key !== false) {
                //1. check for approve status = 1
                $fund_approval = ProjectFundRequestApproval::where(['project_fund_request_id' => $project_fund_request_id, 'approval_level' => $role->name, 'status' => 1])->first();

                if ($fund_approval) {
                    //check if there is more than role
                    if (count($roles) > 1)
                        $serial++; //increment to another role
                } else {
                    //if key == 0 means first to approve
                    if ($key == 0) {
                        echo '<a href="#approveFundModal" id="btnApproveFundModal-' . $project_fund_request_id . '" data-backdrop="static" data-keyboard="false" data-toggle="modal"  data-target="#approveFundModal" title="Approve fund request" data-project-fund-request-id=' . $project_fund_request_id . ' data-role-name=' . $role->name . ' class="btn btn-outline-success btn-xs"><i class="fa fa-check fa-2x text-success"></i></a>';
                    } else {
                        $prev_key = $key - 1; //previous key
                        $prev_role = $levels[$prev_key]; //previous role

                        //2. check the previous actor to see the current role should follow for approval
                        $prev_fund_approval = ProjectFundRequestApproval::where(['project_fund_request_id' => $project_fund_request_id, 'approval_level' => $prev_role, 'status' => 1])->first();

                        if ($prev_fund_approval)
                            echo '<a href="#approveFundModal" id="btnApproveFundModal-' . $project_fund_request_id . '" data-backdrop="static" data-keyboard="false" data-toggle="modal"  data-target="#approveFundModal" title="Approve fund request" data-project-fund-request-id=' . $project_fund_request_id . ' data-role-name=' . $role->name . ' class="btn btn-outline-success btn-xs"><i class="fa fa-check fa-2x text-success"></i></a>';
                    }
                }
            }
        }
    }
}



//show fund approval operation
if (!function_exists('check_progress_report_status')) {
    function check_progress_report_status($project_id)
    {
        //project
        $project = Project::findOrFail($project_id);

        //activities
        $activities = $project->activities;

        if ($activities) {
            $count = 0;
            foreach ($activities as $val) {
                $reports = ProjectActivityReport::where(['activity_id' => $val->id, 'status' => 'on-progress'])->count();

                $count += $reports;
            }

            if ($count > 0) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
