<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;


use App\Models\Settings\Cluster;
use App\Models\Settings\ResearchArea;
use App\Models\Researcher;
use App\User;
use App\Perms;

use App\Models\Calls\Call;
use App\Models\Calls\CallCategory;
use App\Models\Calls\CallCluster;
use App\Models\Calls\CallApplication;
use App\Models\Calls\CallApplicationPartner;



class ProposalsController extends Controller
{
    private $data;

    //lists of proposals
    function index()
    {
        $this->data['title'] = 'Proposals';

        //calls
        $calls = Call::where(['status' => 'active'])->orderBy('created_at', 'DESC')->get();
        $this->data['calls'] = $calls;

        return view('proposals.index', $this->data);
    }

    //details proposal
    function details($id)
    {
        $this->data['title'] = 'Call details';

        //call
        $call = Call::query()->find($id);

        if (!$call)
            abort(404);

        $this->data['call'] = $call;

        //call categories
        $call_categories = CallCategory::query()->where('call_id', $id)->get();
        $this->data['call_categories'] = $call_categories;

        //clusters
        $call_clusters = CallCluster::query()->where('call_id', $id)->get();
        $this->data['call_clusters'] = $call_clusters;

        foreach ($this->data['call_clusters'] as $k => $v) {
            $cluster = Cluster::query()->find($v->cluster_id);
            $this->data['call_clusters'][$k]->cluster = $cluster;

            $research_areas = ResearchArea::query()->where('cluster_id', $cluster->id)->get();
            $this->data['call_clusters'][$k]->research_areas = $research_areas;
        }

        return view('proposals.details', $this->data);
    }

    //confirm
    function confirm($app_id)
    {
        $this->data['title'] = 'Confirm Call Application Participation';

        //app_id
        $call_app = CallApplication::findOrFail($app_id);
        $this->data['call_app'] = $call_app;

        //call
        $call = Call::find($call_app->call_id);
        $this->data['call'] = $call;

        //user
        $user = User::find($call_app->principal_investigator);
        $this->data['user'] = $user;

        return view('proposals.confirm', $this->data);
    }

    //confirm
    function save_confirmation(Request $request, $app_id)
    {
        //validation
        $this->validate(
            $request,
            ['email' => 'required', 'verdict' => 'required', 'comments' => 'required'],
            [
                'email.required' => 'Email required',
                'verdict.required' => 'Verdict required',
                'comments.required' => 'Comments required',
            ]
        );

        //post data
        $email = $request->input('email');

        //check if partner exist
        $call_partner = CallApplicationPartner::where(['application_id' => $app_id, 'partner_email' => $email])->first();

        if ($call_partner) {
            //save confirmation
            $call_partner->status = $request->input('verdict');
            $call_partner->comments = $request->input('comments');
            $call_partner->confirmation_date = date('Y-m-d H:i:s');
            $call_partner->save();

            //redirect with message
            return Redirect::route('proposals.confirm', $app_id)->with('success', 'Confirmation complete.');
        } else {
            //redirect with message
            return Redirect::route('proposals.confirm', $app_id)->with('danger', 'Please verify your information and confirm.');
        }
    }
}
