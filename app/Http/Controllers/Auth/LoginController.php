<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Auth;

use App\User;
use App\Models\Calls\Call;
use App\Models\Settings\Content;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    //login form
    public function show_login_form()
    {
        $this->data['title'] = 'Research Information Management System (RIMS)';

        //check if user already login
        if (Auth::check()) {
            return redirect(route('calls.stats'));
        }

        //content
        $content = Content::find(1);
        $this->data['content'] = $content;

        //calls
        $calls = Call::where(['status' => 'active'])->orderBy('created_at', 'DESC')->get();
        $this->data['calls'] = $calls;

        //render view
        return view('auth.login', $this->data);
    }

    //authenticate

    /**
     * @throws ValidationException
     */
    public function authenticate(Request $request)
    {
        //validation
        $this->validate(
            $request,
            [
                'username' => 'required',
                'password' => 'required'
            ],
            [
                'username.required' => 'Username is required',
                'password.required' => 'Password is required',
            ]
        );

        //post data
        $username = $request->input('username');
        $password = $request->input('password');
        $remember = $request->input('remember');

        if (Auth::attempt(['username' => $username, 'password' => $password], $remember)) {
            //check if user status = active
            $user = User::find(auth()->user()->id);

            if ($user->status === 'active') {
                // Authentication passed...
                return redirect(route('calls.stats'));
            }

            Auth::logout();
            return redirect('login')->with('danger', 'You are not required to use this system');
        }

        return redirect()->back()->with('danger', 'Invalid Username or Password');
    }

    //logout
    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
