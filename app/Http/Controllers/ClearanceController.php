<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

use App\Models\Settings\College;
use App\Models\Settings\Department;
use App\Models\Clearances\AssignedResearcher;
use App\Models\Clearances\ClearanceRecommendation;
use App\Models\Clearances\ClearanceStaff;
use App\Models\Clearances\ClearanceStudent;
use App\Models\Clearances\Ethical;
use App\Models\Clearances\ResearcherAssistant;
use App\Models\Clearances\ResearchSite;
use App\Models\Clearances\Research_type;
use App\Models\Country;
use App\Models\Region;
use App\Models\District;
use App\Models\Notification;
use App\Models\Village;
use App\Models\Ward;

use Auth;
use App\User;
use App\Perms;
use App\Role;
use DB;

class ClearanceController extends Controller
{
    private $data;

    //constructor
    public function __construct()
    {
    }


    //stats
    public function stats()
    {
        $this->data['title'] = 'Clearance Summary';
        $dept_id = Auth::user()->department_id;
        $coll_id = Auth::user()->college_id;
        $today = date('Y-m-d');

        if (Auth::user()->hasRole("student")) {

            //active permits
            $students_clearance_active = ClearanceStudent::where('created_by', Auth::user()->id)
                ->where('approval_status', 'approved')
                ->where('completion_date', '>', $today)->count();
            $this->data['active'] = $students_clearance_active;

            //expired permits
            $students_clearance_expired = ClearanceStudent::where('created_by', Auth::user()->id)
                ->where('approval_status', 'approved')
                ->where('completion_date', '<', $today)->count();
            $this->data['expired'] = $students_clearance_expired;

            //rejected permits
            $students_clearance_rejected = ClearanceStudent::where('created_by', Auth::user()->id)
                ->where('approval_status', 'rejected')->count();
            $this->data['rejected'] = $students_clearance_rejected;

            //waiting permits
            $students_clearance_waiting = ClearanceStudent::where('created_by', Auth::user()->id)
                ->where('approval_status', '!=', 'approved')
                ->where('approval_status', '!=', 'rejected')->count();
            $this->data['waiting'] = $students_clearance_waiting;
        }

        //notifications
        if (Auth::user()->hasRole("staff")) {
            $this->data['staffs'] = ClearanceStaff::where('created_by', Auth::user()->id)->latest('id')->get();
            $this->data['students'] = ClearanceStudent::where('supervisor', Auth::user()->id)->oldest('id')->get();

            //notify
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();
            $this->data['notifications'] = $supervise_students_number;

            //active permits
            $staff_clearance_active = ClearanceStaff::where('created_by', Auth::user()->id)
                ->where('approval_status', 'approved')
                ->where('end_date', '>', $today)->count();
            $this->data['active'] = $staff_clearance_active;

            //expired permits
            $staff_clearance_expired = ClearanceStaff::where('created_by', Auth::user()->id)
                ->where('approval_status', 'approved')
                ->where('end_date', '<', $today)->count();
            $this->data['expired'] = $staff_clearance_expired;

            //rejected permits
            $staff_clearance_rejected = ClearanceStaff::where('created_by', Auth::user()->id)
                ->where('approval_status', 'rejected')->count();
            $this->data['rejected'] = $staff_clearance_rejected;

            //waiting permits
            $staff_clearance_waiting = ClearanceStaff::where('created_by', Auth::user()->id)
                ->where('approval_status', '!=', 'approved')
                ->where('approval_status', '!=', 'rejected')->count();
            $this->data['waiting'] = $staff_clearance_waiting;
        }

        if (Auth::user()->hasRole("hod")) {
            //notify
            $students_number = ClearanceStudent::where('department_id', $dept_id)->where('approval_status', 'hod')
                ->oldest('id')->count();
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();
            $staff_number = ClearanceStaff::where('department_id', $dept_id)
                ->where('approval_status', 'hod')
                ->oldest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number;
            $this->data['supervise_notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("principal")) {
            $students_number = ClearanceStudent::where('college_id', $coll_id)->where('approval_status', 'principal')->latest('id')->count();
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();
            $staff_number = ClearanceStaff::where('college_id', $coll_id)->where('approval_status', 'principal')->count();
            $this->data['notifications'] = $students_number + $staff_number;
            $this->data['supervise_notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("drp")) {
            $students_number = ClearanceStudent::where('approval_status', 'drp')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'drp')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number;
        }

        if (Auth::user()->hasRole("dvc")) {
            $students_number = ClearanceStudent::where('approval_status', 'dvc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'dvc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number;
        }

        if (Auth::user()->hasRole("vc")) {
            $students_number = ClearanceStudent::where('approval_status', 'vc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'vc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number;
        }

        if (Auth::user()->hasRole("admin")) {
            $students_number = ClearanceStudent::latest('id')->count();
            $staff_number = ClearanceStaff::latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number;

            //active permits
            $students_clearance_active = ClearanceStudent::where('approval_status', 'approved')
                ->where('completion_date', '>', $today)->count();
            $staff_clearance_active = ClearanceStaff::where('approval_status', 'approved')
                ->where('end_date', '>', $today)->count();
            $this->data['active'] = $students_clearance_active + $staff_clearance_active;

            //expired permits
            $students_clearance_expired = ClearanceStudent::where('approval_status', 'approved')
                ->where('completion_date', '<', $today)->count();
            $staff_clearance_expired = ClearanceStaff::where('approval_status', 'approved')
                ->where('end_date', '<', $today)->count();
            $this->data['expired'] = $students_clearance_expired + $staff_clearance_expired;

            //rejected permits
            $students_clearance_rejected = ClearanceStudent::where('approval_status', 'rejected')->count();
            $staff_clearance_rejected = ClearanceStaff::where('approval_status', 'rejected')->count();
            $this->data['rejected'] = $students_clearance_rejected + $staff_clearance_rejected;

            //waiting permits
            $students_clearance_waiting = ClearanceStudent::where('approval_status', '!=', 'approved')
                ->where('approval_status', '!=', 'rejected')->count();
            $staff_clearance_waiting = ClearanceStaff::where('approval_status', '!=', 'approved')
                ->where('approval_status', '!=', 'rejected')->count();
            $this->data['waiting'] = $students_clearance_waiting + $staff_clearance_waiting;
        }

        //render view
        return view('clearance.stats')->with('data', $this->data);
    }

    //lists of clearances
    public function index()
    {
        $this->data['title'] = 'Clearances';
        $this->data['table'] = 'clearance';
        $this->data['research_types'] = Research_type::pluck('name', 'id');
        $dept_id = Auth::user()->department_id;

        $coll_id = Auth::user()->college_id;

        if (Auth::user()->hasRole("staff")) {
            $this->data['staffs'] = ClearanceStaff::where('created_by', Auth::user()->id)->latest('id')->get();
            $this->data['students'] = ClearanceStudent::where('supervisor', Auth::user()->id)->oldest('id')->get();

            //notify
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();

            $this->data['notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("student")) {
            $this->data['students'] = ClearanceStudent::where('created_by', Auth::user()->id)->latest('id')->get();

            foreach ($this->data['students'] as $key => $value) {
                $this->data['students'][$key]->type = Research_type::where('id', $value->research_type)->first();
            }
        }

        if (Auth::user()->hasRole("hod")) {
            //notify
            $students_number = ClearanceStudent::where('department_id', $dept_id)->where('approval_status', 'hod')
                ->oldest('id')->count();
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();
            $staff_number = ClearanceStaff::where('department_id', $dept_id)
                ->where('approval_status', 'hod')
                ->oldest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'hod')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
            $this->data['supervise_notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("principal")) {
            $students_number = ClearanceStudent::where('college_id', $coll_id)->where('approval_status', 'principal')->latest('id')->count();
            $staff_number = ClearanceStaff::where('college_id', $coll_id)->where('approval_status', 'principal')->count();
            $ethical_number = Ethical::where('approval_status', 'drp')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
            $this->data['supervise_notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("drp")) {
            $students_number = ClearanceStudent::where('approval_status', 'drp')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'drp')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'drp')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("dvc")) {
            $students_number = ClearanceStudent::where('approval_status', 'dvc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'dvc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'dvc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("vc")) {
            $students_number = ClearanceStudent::where('approval_status', 'vc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'vc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'vc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("admin")) {
            $students_number = ClearanceStudent::latest('id')->count();
            $staff_number = ClearanceStaff::latest('id')->count();
            $ethical_number = Ethical::latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        return view('clearance.index')->with('data', $this->data);
    }

    //lists of ethicals
    public function list_ethical_clearance()
    {
        $this->data['title'] = 'Clearances';
        $this->data['table'] = 'ethical';
        $this->data['research_types'] = Research_type::pluck('name', 'id');
        $dept_id = Auth::user()->department_id;
        $dept = Department::find($dept_id);

        $coll_id = Auth::user()->college_id;
        $coll = College::find($coll_id);

        // ethical
        $this->data['ethical'] = Ethical::where('created_by', Auth::user()->id)->latest('id')->get();
        if ($this->data['ethical']) {
            foreach ($this->data['ethical'] as $key => $value) {
                $this->data['ethical'][$key]->region = Region::where('id', $value->region_id)->first();
                $this->data['ethical'][$key]->district = District::where('id', $value->district_id)->first();
                $this->data['ethical'][$key]->ward = Ward::where('id', $value->ward_id)->first();
                $this->data['ethical'][$key]->village = Village::where('id', $value->village_id)->first();
            }
        }


        if (Auth::user()->hasRole("staff")) {
            $this->data['staffs'] = ClearanceStaff::where('created_by', Auth::user()->id)->latest('id')->get();
            $this->data['students'] = ClearanceStudent::where('supervisor', Auth::user()->id)->oldest('id')->get();

            //notify
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();

            $this->data['notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("hod")) {
            //notify
            $students_number = ClearanceStudent::where('approval_status', 'hod')
                ->oldest('id')->count();
            $staff_number = ClearanceStaff::where('department', $dept['name'])
                ->where('approval_status', 'hod')
                ->oldest('id')->count();
            $ethical_number = Ethical::where('department', $dept['name'])->where('approval_status', 'hod')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("principal")) {
            $students_number = ClearanceStudent::where('approval_status', 'principal')->latest('id')->count();
            $staff_number = ClearanceStaff::where('college', $coll['name'])->where('approval_status', 'principal')->count();
            $ethical_number = Ethical::where('college', $coll['name'])->where('approval_status', 'drp')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("drp")) {
            $students_number = ClearanceStudent::where('approval_status', 'drp')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'drp')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'drp')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("dvc")) {
            $students_number = ClearanceStudent::where('approval_status', 'dvc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'dvc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'dvc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("vc")) {
            $students_number = ClearanceStudent::where('approval_status', 'vc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'vc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'vc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("admin")) {
            $students_number = ClearanceStudent::latest('id')->count();
            $staff_number = ClearanceStaff::latest('id')->count();
            $ethical_number = Ethical::latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        return view('clearance.index')->with('data', $this->data);
    }

    //on working
    public function process_clearance()
    {
        $this->data['title'] = 'Clearances';
        $this->data['table'] = 'process';
        $this->data['research_types'] = Research_type::pluck('name', 'id');
        $dept_id = Auth::user()->department_id;

        $coll_id = Auth::user()->college_id;

        if (Auth::user()->hasRole("staff")) {
            $this->data['students'] = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->get();

            foreach ($this->data['students'] as $key => $value) {
                $this->data['students'][$key]->type = Research_type::where('id', $value->research_type)->first();
            }
            //notify
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();

            $this->data['notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("hod")) {

            $this->data['students'] = ClearanceStudent::where('department_id', $dept_id)->where('approval_status', 'hod')
                ->oldest('id')->get();

            $this->data['staffs'] = ClearanceStaff::where('department_id', $dept_id)->where('approval_status', 'hod')
                ->oldest('id')->get();

            foreach ($this->data['students'] as $key => $value) {
                $this->data['students'][$key]->type = Research_type::where('id', $value->research_type)->first();
            }
            $this->data['ethical'] = Ethical::where('department', $dept_id)->where('approval_status', 'hod')->latest('id')->get();

            if ($this->data['ethical']) {
                foreach ($this->data['ethical'] as $key => $value) {
                    $this->data['ethical'][$key]->region = Region::where('id', $value->region_id)->first();
                    $this->data['ethical'][$key]->district = District::where('id', $value->district_id)->first();
                    $this->data['ethical'][$key]->ward = Ward::where('id', $value->ward_id)->first();
                    $this->data['ethical'][$key]->village = Village::where('id', $value->village_id)->first();
                }
            }
            //notify
            $students_number = ClearanceStudent::where('department_id', $dept_id)->where('approval_status', 'hod')
                ->oldest('id')->count();

            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();
            $staff_number = ClearanceStaff::where('department_id', $dept_id)->where('approval_status', 'hod')
                ->oldest('id')->count();
            $ethical_number = Ethical::where('department', $dept_id)->where('approval_status', 'hod')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
            $this->data['supervise_notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("principal")) {
            $this->data['students'] = ClearanceStudent::where('college_id', $coll_id)->where('approval_status', 'principal')->latest('id')->get();
            $this->data['staffs'] = ClearanceStaff::where('college_id', $coll_id)->where('approval_status', 'principal')->latest('id')->get();

            foreach ($this->data['students'] as $key => $value) {
                $this->data['students'][$key]->type = Research_type::where('id', $value->research_type)->first();
            }

            $this->data['ethical'] = Ethical::where('college', $coll_id)->where('approval_status', 'principal')->latest('id')->get();

            if ($this->data['ethical']) {
                foreach ($this->data['ethical'] as $key => $value) {
                    $this->data['ethical'][$key]->region = Region::where('id', $value->region_id)->first();
                    $this->data['ethical'][$key]->district = District::where('id', $value->district_id)->first();
                    $this->data['ethical'][$key]->ward = Ward::where('id', $value->ward_id)->first();
                    $this->data['ethical'][$key]->village = Village::where('id', $value->village_id)->first();
                }
            }

            $students_number = ClearanceStudent::where('college_id', $coll_id)->where('approval_status', 'principal')->latest('id')->count();
            $staff_number = ClearanceStaff::where('college_id', $coll_id)->where('approval_status', 'principal')->count();
            $ethical_number = Ethical::where('college', $coll_id)->where('approval_status', 'drp')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
            $this->data['supervise_notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("drp")) {
            $this->data['students'] = ClearanceStudent::where('approval_status', 'drp')->latest('id')->get();
            $this->data['staffs'] = ClearanceStaff::where('approval_status', 'drp')->latest('id')->get();
            foreach ($this->data['students'] as $key => $value) {
                $this->data['students'][$key]->type = Research_type::where('id', $value->research_type)->first();
            }

            $this->data['ethical'] = Ethical::where('approval_status', 'drp')->latest('id')->get();

            if ($this->data['ethical']) {
                foreach ($this->data['ethical'] as $key => $value) {
                    $this->data['ethical'][$key]->region = Region::where('id', $value->region_id)->first();
                    $this->data['ethical'][$key]->district = District::where('id', $value->district_id)->first();
                    $this->data['ethical'][$key]->ward = Ward::where('id', $value->ward_id)->first();
                    $this->data['ethical'][$key]->village = Village::where('id', $value->village_id)->first();
                }
            }

            $students_number = ClearanceStudent::where('approval_status', 'drp')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'drp')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'drp')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("dvc")) {
            $this->data['students'] = ClearanceStudent::where('approval_status', 'dvc')->latest('id')->get();
            $this->data['staffs'] = ClearanceStaff::where('approval_status', 'dvc')->latest('id')->get();

            foreach ($this->data['students'] as $key => $value) {
                $this->data['students'][$key]->type = Research_type::where('id', $value->research_type)->first();
            }

            $this->data['ethical'] = Ethical::where('approval_status', 'dvc')->latest('id')->get();

            if ($this->data['ethical']) {
                foreach ($this->data['ethical'] as $key => $value) {
                    $this->data['ethical'][$key]->region = Region::where('id', $value->region_id)->first();
                    $this->data['ethical'][$key]->district = District::where('id', $value->district_id)->first();
                    $this->data['ethical'][$key]->ward = Ward::where('id', $value->ward_id)->first();
                    $this->data['ethical'][$key]->village = Village::where('id', $value->village_id)->first();
                }
            }

            $students_number = ClearanceStudent::where('approval_status', 'dvc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'dvc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'dvc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("vc")) {
            $this->data['students'] = ClearanceStudent::where('approval_status', 'vc')->latest('id')->get();
            $this->data['staffs'] = ClearanceStaff::where('approval_status', 'vc')->latest('id')->get();

            foreach ($this->data['students'] as $key => $value) {
                $this->data['students'][$key]->type = Research_type::where('id', $value->research_type)->first();
            }

            $this->data['ethical'] = Ethical::where('approval_status', 'vc')->latest('id')->get();

            if ($this->data['ethical']) {
                foreach ($this->data['ethical'] as $key => $value) {
                    $this->data['ethical'][$key]->region = Region::where('id', $value->region_id)->first();
                    $this->data['ethical'][$key]->district = District::where('id', $value->district_id)->first();
                    $this->data['ethical'][$key]->ward = Ward::where('id', $value->ward_id)->first();
                    $this->data['ethical'][$key]->village = Village::where('id', $value->village_id)->first();
                }
            }

            $students_number = ClearanceStudent::where('approval_status', 'vc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'vc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'vc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("admin")) {
            $this->data['students'] = ClearanceStudent::latest('id')->get();
            $this->data['staffs'] = ClearanceStaff::latest('id')->get();

            foreach ($this->data['students'] as $key => $value) {
                $this->data['students'][$key]->type = Research_type::where('id', $value->research_type)->first();
            }

            $this->data['ethical'] = Ethical::latest('id')->get();

            if ($this->data['ethical']) {
                foreach ($this->data['ethical'] as $key => $value) {
                    $this->data['ethical'][$key]->region = Region::where('id', $value->region_id)->first();
                    $this->data['ethical'][$key]->district = District::where('id', $value->district_id)->first();
                    $this->data['ethical'][$key]->ward = Ward::where('id', $value->ward_id)->first();
                    $this->data['ethical'][$key]->village = Village::where('id', $value->village_id)->first();
                }
            }

            $ethical_number = Ethical::latest('id')->count();
            $students_number = ClearanceStudent::latest('id')->count();
            $staff_number = ClearanceStaff::latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        return view('clearance.index')->with('data', $this->data);
    }


    public function process_clearance_holder_supervise()
    {
        $this->data['title'] = 'Clearances';
        $this->data['table'] = 'process';
        $this->data['research_types'] = Research_type::pluck('name', 'id');
        $dept_id = Auth::user()->department_id;

        $coll_id = Auth::user()->college_id;

        if (Auth::user()->hasRole("hod")) {

            $this->data['students'] = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->get();

            $this->data['staffs'] = ClearanceStaff::where('department_id', $dept_id)->where('approval_status', 'hod')
                ->oldest('id')->get();


            foreach ($this->data['students'] as $key => $value) {
                $this->data['students'][$key]->type = Research_type::where('id', $value->research_type)->first();
            }
            $this->data['ethical'] = Ethical::where('department', $dept_id)->where('approval_status', 'hod')->latest('id')->get();

            if ($this->data['ethical']) {
                foreach ($this->data['ethical'] as $key => $value) {
                    $this->data['ethical'][$key]->region = Region::where('id', $value->region_id)->first();
                    $this->data['ethical'][$key]->district = District::where('id', $value->district_id)->first();
                    $this->data['ethical'][$key]->ward = Ward::where('id', $value->ward_id)->first();
                    $this->data['ethical'][$key]->village = Village::where('id', $value->village_id)->first();
                }
            }
            //notify
            $students_number = ClearanceStudent::where('department_id', $dept_id)->where('approval_status', 'hod')
                ->oldest('id')->count();

            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();
            $staff_number = ClearanceStaff::where('department_id', $dept_id)->where('approval_status', 'hod')
                ->oldest('id')->count();
            $ethical_number = Ethical::where('department', $dept_id)->where('approval_status', 'hod')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
            $this->data['supervise_notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("principal")) {
            $this->data['students'] = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->get();
            $this->data['staffs'] = ClearanceStaff::where('college_id', $coll_id)->where('approval_status', 'principal')->latest('id')->get();

            foreach ($this->data['students'] as $key => $value) {
                $this->data['students'][$key]->type = Research_type::where('id', $value->research_type)->first();
            }

            $this->data['ethical'] = Ethical::where('college', $coll_id)->where('approval_status', 'principal')->latest('id')->get();

            if ($this->data['ethical']) {
                foreach ($this->data['ethical'] as $key => $value) {
                    $this->data['ethical'][$key]->region = Region::where('id', $value->region_id)->first();
                    $this->data['ethical'][$key]->district = District::where('id', $value->district_id)->first();
                    $this->data['ethical'][$key]->ward = Ward::where('id', $value->ward_id)->first();
                    $this->data['ethical'][$key]->village = Village::where('id', $value->village_id)->first();
                }
            }

            $students_number = ClearanceStudent::where('college_id', $coll_id)->where('approval_status', 'principal')->latest('id')->count();
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();
            $staff_number = ClearanceStaff::where('college_id', $coll_id)->where('approval_status', 'principal')->count();
            $ethical_number = Ethical::where('college', $coll_id)->where('approval_status', 'drp')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
            $this->data['supervise_notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("drp")) {
            $this->data['students'] = ClearanceStudent::where('approval_status', 'drp')->latest('id')->get();
            $this->data['staffs'] = ClearanceStaff::where('approval_status', 'drp')->latest('id')->get();

            foreach ($this->data['students'] as $key => $value) {
                $this->data['students'][$key]->type = Research_type::where('id', $value->research_type)->first();
            }

            $this->data['ethical'] = Ethical::where('approval_status', 'drp')->latest('id')->get();

            if ($this->data['ethical']) {
                foreach ($this->data['ethical'] as $key => $value) {
                    $this->data['ethical'][$key]->region = Region::where('id', $value->region_id)->first();
                    $this->data['ethical'][$key]->district = District::where('id', $value->district_id)->first();
                    $this->data['ethical'][$key]->ward = Ward::where('id', $value->ward_id)->first();
                    $this->data['ethical'][$key]->village = Village::where('id', $value->village_id)->first();
                }
            }

            $students_number = ClearanceStudent::where('approval_status', 'drp')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'drp')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'drp')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("dvc")) {
            $this->data['students'] = ClearanceStudent::where('approval_status', 'dvc')->latest('id')->get();
            $this->data['staffs'] = ClearanceStaff::where('approval_status', 'dvc')->latest('id')->get();

            foreach ($this->data['students'] as $key => $value) {
                $this->data['students'][$key]->type = Research_type::where('id', $value->research_type)->first();
            }

            $this->data['ethical'] = Ethical::where('approval_status', 'dvc')->latest('id')->get();

            if ($this->data['ethical']) {
                foreach ($this->data['ethical'] as $key => $value) {
                    $this->data['ethical'][$key]->region = Region::where('id', $value->region_id)->first();
                    $this->data['ethical'][$key]->district = District::where('id', $value->district_id)->first();
                    $this->data['ethical'][$key]->ward = Ward::where('id', $value->ward_id)->first();
                    $this->data['ethical'][$key]->village = Village::where('id', $value->village_id)->first();
                }
            }

            $students_number = ClearanceStudent::where('approval_status', 'dvc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'dvc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'dvc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("vc")) {
            $this->data['students'] = ClearanceStudent::where('approval_status', 'vc')->latest('id')->get();
            $this->data['staffs'] = ClearanceStaff::where('approval_status', 'vc')->latest('id')->get();

            foreach ($this->data['students'] as $key => $value) {
                $this->data['students'][$key]->type = Research_type::where('id', $value->research_type)->first();
            }

            $this->data['ethical'] = Ethical::where('approval_status', 'vc')->latest('id')->get();

            if ($this->data['ethical']) {
                foreach ($this->data['ethical'] as $key => $value) {
                    $this->data['ethical'][$key]->region = Region::where('id', $value->region_id)->first();
                    $this->data['ethical'][$key]->district = District::where('id', $value->district_id)->first();
                    $this->data['ethical'][$key]->ward = Ward::where('id', $value->ward_id)->first();
                    $this->data['ethical'][$key]->village = Village::where('id', $value->village_id)->first();
                }
            }

            $students_number = ClearanceStudent::where('approval_status', 'vc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'vc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'vc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("admin")) {
            $this->data['students'] = ClearanceStudent::latest('id')->get();
            $this->data['staffs'] = ClearanceStaff::latest('id')->get();

            foreach ($this->data['students'] as $key => $value) {
                $this->data['students'][$key]->type = Research_type::where('id', $value->research_type)->first();
            }

            $this->data['ethical'] = Ethical::latest('id')->get();

            if ($this->data['ethical']) {
                foreach ($this->data['ethical'] as $key => $value) {
                    $this->data['ethical'][$key]->region = Region::where('id', $value->region_id)->first();
                    $this->data['ethical'][$key]->district = District::where('id', $value->district_id)->first();
                    $this->data['ethical'][$key]->ward = Ward::where('id', $value->ward_id)->first();
                    $this->data['ethical'][$key]->village = Village::where('id', $value->village_id)->first();
                }
            }

            $ethical_number = Ethical::latest('id')->count();
            $students_number = ClearanceStudent::latest('id')->count();
            $staff_number = ClearanceStaff::latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        return view('clearance.supervisor_holder_list')->with('data', $this->data);
    }

    //ethical clearance form
    public function ethical()
    {
        $this->data['title'] = 'Ethical Clearance';
        $this->data['regions'] = Region::pluck('name', 'id');
        $this->data['reseachers'] = User::all();

        $this->data['user'] = User::where('id', Auth::user()->id)->first();
        $this->data['college'] = College::where('id', $this->data['user']['college_id'])->first();
        $this->data['dept'] = Department::where('id', $this->data['user']['department_id'])->first();

        return view('clearance.ethical')->with('data', $this->data);
    }

    public function store_ethical(Request $request): RedirectResponse
    {
        $this->validate($request, [
            'project_reg_no' => 'required',
            'research_title' => 'required',
            'research_type' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'background' => 'required',
            'precaution' => 'required',
            'adverse' => 'required',
            'degree_discomfort' => 'required',
            'recruited' => 'required',
            'region' => 'required',
            'district' => 'required'
        ]);

        if (!empty($request->file('english_attachment'))) {
            $eng_file = $request->file('english_attachment');
            if ($eng_file) {
                $destinationPath = 'updloads/'; // upload path
                $eng_filename = date('YmdHis') . "." . $eng_file->getClientOriginalExtension();
                $eng_file->move($destinationPath, $eng_filename);
            }
        }
        if (!empty($request->file('swahili_attachment'))) {
            $sw_file = $request->file('swahili_attachment');
            if ($sw_file) {
                $destinationPath = 'updloads/'; // upload path
                $sw_filename = date('YmdHis') . "." . $sw_file->getClientOriginalExtension();
                $sw_file->move($destinationPath, $sw_filename);
            }
        }
        if (!empty($request->file('recruite_attachment'))) {
            $rec_file = $request->file('recruite_attachment');
            if ($rec_file) {
                $destinationPath = 'updloads/'; // upload path
                $rec_filename = date('YmdHis') . "." . $rec_file->getClientOriginalExtension();
                $rec_file->move($destinationPath, $rec_filename);
            }
        }
        if (!empty($request->file('consent_attachment'))) {
            $con_file = $request->file('consent_attachment');
            if ($con_file) {
                $destinationPath = 'updloads/'; // upload path
                $con_filename = date('YmdHis') . "." . $con_file->getClientOriginalExtension();
                $con_file->move($destinationPath, $con_filename);
            }
        }

        //ethical clearance information
        $clearance = new Ethical();
        $clearance->names = $request->get('names');
        $clearance->title = $request->get('title');
        $clearance->college = $request->get('college');
        $clearance->department = $request->get('department');
        $clearance->email = $request->get('email');
        $clearance->phone = $request->get('phone');
        $clearance->project_reg_no = $request->get('project_reg_no');
        $clearance->research_title = $request->get('research_title');
        $clearance->research_type = $request->get('research_type');
        $clearance->background = $request->get('background');
        $clearance->design = $request->get('design');
        $clearance->objective = $request->get('objective');
        $clearance->hypotheses = $request->get('hypotheses');
        $clearance->region_id = $request->get('region');
        $clearance->district_id = $request->get('district');
        $clearance->ward_id = $request->get('ward');
        $clearance->village_id = $request->get('village');
        $clearance->start_date = $request->get('start_date');
        $clearance->end_date = $request->get('end_date');
        $clearance->collection = $request->get('collection');
        $clearance->procedures = $request->get('procedures');
        $clearance->adverse = $request->get('adverse');
        $clearance->precaution = $request->get('precaution');
        $clearance->procedures_discomfort = $request->get('procedures_discomfort');
        $clearance->degree_discomfort = $request->get('degree_discomfort');
        $clearance->recruited = $request->get('recruited');
        $clearance->payment_participant = $request->get('payment_participant');
        $clearance->confidentiality = $request->get('confidentiality');

        $clearance->investigation_qn = $request->get('investigation_qn');
        $clearance->investigation_detail = $request->get('investigation_detail');
        $clearance->expertise = $request->get('expertise');
        $clearance->situation_type = $request->get('situation_type');
        $clearance->env = $request->get('env');
        $clearance->negative_impact = $request->get('negative_impact');
        $clearance->created_by = Auth::user()->id;

        if (!empty($request->file('english_attachment'))) {
            $clearance->english_attachment = $eng_filename;
        }
        if (!empty($request->file('swahili_attachment'))) {
            $clearance->swahili_attachment = $sw_filename;
        }
        if (!empty($request->file('recruite_attachment'))) {
            $clearance->recruite_attachment = $rec_filename;
        }
        if (!empty($request->file('consent_attachment'))) {
            $clearance->consent_attachment = $con_filename;
        }
        $clearance->level = "1";
        $clearance->approval_status = "hod";
        $clearance->created_by = Auth::user()->id;
        $clearance->save();

        return redirect()->route('list_ethical_clearance')->with('success', 'You have successfully
        submitted your clearance to HoD');
    }

    //open student clearance
    public function show_ethical_clearance($id)
    {

        $this->data['title'] = 'View clearance';
        $dept_id = Auth::user()->department_id;
        $dept = Department::find($dept_id);

        $coll_id = Auth::user()->college_id;
        $coll = College::find($coll_id);
        $ethical = Ethical::find($id);

        $this->data['recommendations'] = ClearanceRecommendation::where('clearance_ethical_id', $id)->get();

        $this->data['ethical'] = $ethical;

        $this->data['region'] = Region::where('id', $ethical->region_id)->first();
        $this->data['district'] = District::where('id', $ethical->region_id)->first();
        $this->data['ward'] = Ward::where('id', $ethical->ward_id)->first();
        $this->data['village'] = Village::where('id', $ethical->village_id)->first();


        if (Auth::user()->hasRole("staff")) {
            $this->data['staffs'] = ClearanceStaff::where('created_by', Auth::user()->id)->latest('id')->get();
            $this->data['students'] = ClearanceStudent::where('supervisor', Auth::user()->id)->oldest('id')->get();

            //notify
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();

            $this->data['notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("hod")) {
            //notify
            $students_number = ClearanceStudent::where('approval_status', 'hod')
                ->oldest('id')->count();
            $staff_number = ClearanceStaff::where('department', $dept['name'])
                ->where('approval_status', 'hod')
                ->oldest('id')->count();
            $ethical_number = Ethical::where('department', $dept['name'])->where('approval_status', 'hod')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("principal")) {
            $students_number = ClearanceStudent::where('approval_status', 'principal')->latest('id')->count();
            $staff_number = ClearanceStaff::where('college', $coll['name'])->where('approval_status', 'principal')->count();
            $ethical_number = Ethical::where('college', $coll['name'])->where('approval_status', 'drp')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("drp")) {
            $students_number = ClearanceStudent::where('approval_status', 'drp')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'drp')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'drp')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("dvc")) {
            $students_number = ClearanceStudent::where('approval_status', 'dvc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'dvc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'dvc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("vc")) {
            $students_number = ClearanceStudent::where('approval_status', 'vc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'vc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'vc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("admin")) {
            $students_number = ClearanceStudent::latest('id')->count();
            $staff_number = ClearanceStaff::latest('id')->count();
            $ethical_number = Ethical::latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        return view('clearance.show_ethical')->with('data', $this->data);
    }

    //ethical clearance recommendations panel
    public function recommendation_panel_ethical($id)
    {
        $this->data['title'] = 'Add recommendations';
        $this->data['ethical'] = Ethical::find($id);

        return view('clearance.recommendation_panel_ethical')->with('data', $this->data);
    }

    // save student clearance recommendations
    public function save_recommendation_ethical(Request $request, $id): RedirectResponse
    {
        $reco = new ClearanceRecommendation();
        $clearance = Ethical::find($id);

        if ($clearance->level == "1") {
            $clearance->level = "2";
            $clearance->approval_status = "principal";

            $reco->clearance_ethical_id = $id;
            $reco->from = "hod";
            $reco->comment = $request->get('recommendation');
        } elseif ($clearance->level == "2") {
            $clearance->level = "3";
            $clearance->approval_status = "drp";

            $reco->clearance_ethical_id = $id;
            $reco->from = "principal";
            $reco->comment = $request->get('recommendation');
        } elseif ($clearance->level == "3") {
            $clearance->level = "4";
            $clearance->approval_status = "dvc";

            $reco->clearance_ethical_id = $id;
            $reco->from = "drp";
            $reco->comment = $request->get('recommendation');
        } elseif ($clearance->level == "4") {
            $clearance->level = "5";
            $clearance->approval_status = "vc";

            $reco->clearance_ethical_id = $id;
            $reco->from = "dvc";
            $reco->comment = $request->get('recommendation');
        }
        $clearance->save();
        $reco->save();

        return redirect()->route('list_ethical_clearance');
    }

    // save vc approval status
    public function approve_ethical_clearance(Request $request, $id): RedirectResponse
    {
        //   Perms::has_allowed('clearance_staffs', 'approve_staff_clearance');
        $clearance = Ethical::find($id);
        $clearance->approval_status = $request->get('approve_status');

        $clearance->save();
        $status = $request->get('approve_status');

        if ($status === 'rejected') {
            return redirect()->route('list_ethical_clearance');
        }

        if ($status === 'approved') {
            return redirect()->route('list_ethical_clearance');
        }
        return redirect()->route('list_ethical_clearance');
    }


    //students clearance form
    public function students()
    {
        Perms::has_allowed('clearance_students', 'students');

        $this->data['title'] = 'Students Clearance';
        $this->data['research_types'] = Research_type::orderBy('name', 'asc')->get();

        $this->data['departments'] = Department::pluck('name', 'id');
        $this->data['regions'] = Region::pluck('name', 'id');
        $this->data['districts'] = District::all();
        $this->data['wards'] = Ward::all();
        $this->data['villages'] = Village::all();

        $this->data['user'] = User::where('id', Auth::user()->id)->first();
        $this->data['college'] = College::where('id', $this->data['user']['college_id'])->first();

        $colleges = College::all(['id', 'name']);
        $this->data['college_pool'] = $colleges;


        $this->data['dept'] = Department::where('id', $this->data['user']['department_id'])->first();

        $departments = Department::all('id', 'name');
        $this->data['dept_pool'] = $departments;

        $this->data['countries'] = Country::orderBy('name', 'asc')->get();

        $this->data['staff_details'] = User::orderBy('first_name', 'asc')->whereHas('roles', function ($q) {
            $q->where('name', 'staff');
        })->get();
        return view('clearance.students')->with('data', $this->data);
    }

    //For fetching districts
    public function getdistricts($id)
    {
        $districts = District::where('region_id', $id)->pluck("name", "id");
        return response()->json($districts);
    }

    //For fetching wards
    public function getwards($id)
    {
        $wards = Ward::where('district_id', $id)->pluck("name", "id");
        return response()->json($wards);
    }

    //For fetching wards
    public function getvillages($id): JsonResponse
    {
        $villages = Village::where('ward_id', $id)->pluck("name", "id");
        return response()->json($villages);
    }

    //brief on students clearance
    public function brief()
    {
        $this->data['title'] = 'Students Clearance Description';

        return view('clearance.brief');
    }

    //brief on students clearance
    public function brief_staff()
    {
        $this->data['title'] = 'Staff Clearance Description';

        return view('clearance.brief_staff');
    }

    public function store_student_clearance(Request $request): RedirectResponse
    {
        $re = $request->all();

        //deal with uploaded file
        $this->validate($request, [
            'proposal_attachment' => 'required|mimes:pdf',
            'student_id' => 'required|mimes:pdf',
            'financial_statement' => 'required|mimes:pdf',
            'nationality' => 'required',
            'reg_no' => 'required',
            'entry_year' => 'required',
            'degree_program' => 'required',
            'duration' => 'required',
            'program_mode' => 'required',
            'delivery_mode' => 'required',
            'contact' => 'required',
            'supervisor' => 'required',
            'research_title' => 'required',
            'research_type' => 'required',
            'commencement_date' => 'required',
            'completion_date' => 'required',
            'research_budget' => 'required',
            'fund_source' => 'required',
            'region' => 'required',
            'district' => 'required'
        ]);

        $pro_files = $request->file('proposal_attachment');
        if ($pro_files) {
            $destinationPath = 'updloads/'; // upload path
            $pro_filename = date('YmdHis') . "pro" . "." . $pro_files->getClientOriginalExtension();
            $pro_files->move($destinationPath, $pro_filename);
        }

        $id_files = $request->file('student_id');
        if ($id_files) {
            $destinationPath = 'updloads/'; // upload path
            $id_filename = date('YmdHis') . "studentid" . "." . $id_files->getClientOriginalExtension();
            $id_files->move($destinationPath, $id_filename);
        }

        $finance_files = $request->file('financial_statement');
        if ($finance_files) {
            $destinationPath = 'updloads/'; // upload path
            $finance_filename = date('YmdHis') . "finance" . "." . $finance_files->getClientOriginalExtension();
            $finance_files->move($destinationPath, $finance_filename);
        }

        //clearance information
        $clearance = new ClearanceStudent();
        $clearance->names = $request->get('names');
        $clearance->gender = $request->get('gender');
        $clearance->nationality = $request->get('nationality');
        $clearance->reg_no = $request->get('reg_no');
        $clearance->year_entry = $request->get('entry_year');
        $clearance->department_id = $request->get('department_id');
        $clearance->college_id = $request->get('college_id');

        $clearance->degree_program = $request->get('degree_program');
        $clearance->duration = $request->get('duration');
        $clearance->program_mode = $request->get('program_mode');
        $clearance->contact = $request->get('contact');
        $clearance->supervisor = $request->get('supervisor');
        $clearance->research_title = $request->get('research_title');
        $clearance->research_type = $request->get('research_type');
        $clearance->commencement_date = $request->get('commencement_date');
        $clearance->completion_date = $request->get('completion_date');
        $clearance->research_budget = floatval(str_replace(",", "", $request->get('research_budget')));

        $clearance->fund_source = $request->get('fund_source');
        $clearance->proposal_attachment = $pro_filename;
        $clearance->student_identity = $id_filename;
        $clearance->finance_statement = $finance_filename;
        $clearance->level = "1";
        $clearance->approval_status = "supervisor";
        $clearance->delivery_mode = $request->get('delivery_mode');
        $clearance->created_by = Auth::user()->id;
        $clearance->save();

        //store sites details
        $temp = count($request->get('site_count'));
        for ($i = 0; $i < $temp; $i++) {

            $region = Region::find($request->get('region')[$i]);
            $district = $request->get('district')[$i];

            if (!empty($request->get('ward'))) {
                $ward = $request->get('ward')[$i];
            }

            if (!empty($request->get('village'))) {
                $village = $request->get('village')[$i];
            }

            $site = new ResearchSite();
            $site->clearance_student_id = $clearance->id;
            $site->organization = $request->get('organization')[$i];
            $site->title = $request->get('title')[$i];
            $site->address = $request->get('address')[$i];
            $site->region = $region['name'];
            $site->district = $district;

            if (!empty($request->get('ward'))) {
                $site->ward = $ward;
            }
            if (!empty($request->get('village'))) {
                $site->village = $village;
            }
            $site->save();
        }

        //construct notification message to supervisor
        $message = "New clearance request for the project with title <b> $clearance->research_title </b> is submitted by <b><i> $clearance->names</i></b>";
        $message .= "<br/> Login in the system by  <a href=" . url('/clearance/process') . ">click here</a> and act on the request.";
        $message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

        //send notification to supervisor
        $this->send_notification($clearance->supervisor, $message);

        return redirect()->route('clearance')->with('success', 'You have successfully
        submitted your clearance to Supervisor');
    }

    //open student clearance
    public function show_student_clearance($id)
    {
        $this->data['title'] = 'View clearance';
        $this->data['student'] = ClearanceStudent::find($id);
        $this->data['user'] = User::find($this->data['student']->supervisor);

        $dept_id = Auth::user()->department_id;

        $coll_id = Auth::user()->college_id;

        //display college name
        $college_clr = College::whereId([$this->data['student']->college_id])->first();
        $this->data['college_clr'] = $college_clr;

        //display department name
        $dept_clr = Department::whereId([$this->data['student']->department_id])->first();
        $this->data['dept_clr'] = $dept_clr;

        $this->data['sites'] = ResearchSite::where('clearance_student_id', $id)->get();
        $this->data['research_type'] = Research_type::where('id', $this->data['student']->research_type)->first();
        $this->data['recommendations'] = ClearanceRecommendation::where('clearance_student_id', $id)->get();

        if (Auth::user()->hasRole("staff")) {
            $this->data['staffs'] = ClearanceStaff::where('created_by', Auth::user()->id)->latest('id')->get();
            $this->data['students'] = ClearanceStudent::where('supervisor', Auth::user()->id)->oldest('id')->get();

            //notify
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();

            $this->data['notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("hod")) {
            //notify
            $students_number = ClearanceStudent::where('department_id', $dept_id)
                ->where('approval_status', 'hod')
                ->oldest('id')->count();
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();
            $staff_number = ClearanceStaff::where('department_id', $dept_id)
                ->where('approval_status', 'hod')
                ->oldest('id')->count();
            $ethical_number = Ethical::where('department', $dept_id)->where('approval_status', 'hod')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
            $this->data['supervise_notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("principal")) {
            $students_number = ClearanceStudent::where('college_id', $coll_id)->where('approval_status', 'principal')->latest('id')->count();
            $staff_number = ClearanceStaff::where('college_id', $coll_id)->where('approval_status', 'principal')->count();
            $ethical_number = Ethical::where('college', $coll_id)->where('approval_status', 'drp')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
            $this->data['supervise_notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("drp")) {
            $students_number = ClearanceStudent::where('approval_status', 'drp')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'drp')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'drp')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("dvc")) {
            $students_number = ClearanceStudent::where('approval_status', 'dvc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'dvc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'dvc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("vc")) {
            $students_number = ClearanceStudent::where('approval_status', 'vc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'vc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'vc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("admin")) {
            $students_number = ClearanceStudent::latest('id')->count();
            $staff_number = ClearanceStaff::latest('id')->count();
            $ethical_number = Ethical::latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }
        return view('clearance.show_student')->with('data', $this->data);
    }


    //edit student clearance
    public function edit_student_clearance($id)
    {
        $this->data['title'] = 'edit clearance';
        $this->data['student'] = ClearanceStudent::find($id);

        $this->data['supervisor'] = User::find($this->data['student']->supervisor);//why this was missing?

        $this->data['research_types'] = Research_type::orderBy('name', 'asc')->get();

        $this->data['departments'] = Department::pluck('name', 'id');
        $this->data['regions'] = Region::all();

        $this->data['user'] = User::where('id', Auth::user()->id)->first();
        $this->data['college'] = College::where('id', $this->data['user']['college_id'])->first();
        $this->data['dept'] = Department::where('id', $this->data['user']['department_id'])->first();

        $college_pool = College::all(['id', 'name']);
        $this->data['college_pool'] = $college_pool;

        $dept_pool = Department::all(['id', 'name']);
        $this->data['dept_pool'] = $dept_pool;

        $this->data['staff_details'] = User::orderBy('first_name', 'asc')->whereHas('roles', function ($q) {
            $q->where('name', 'staff');
        })->get();

        $dept_id = Auth::user()->department_id;
        $dept = Department::find($dept_id);

        $coll_id = Auth::user()->college_id;
        $coll = College::find($coll_id);

        $this->data['sites'] = ResearchSite::where('clearance_student_id', $id)->get();

        foreach ($this->data['sites'] as $key => $value) {
            $this->data['sites'][$key]->selected_region = Region::where('name', $value->region)->first();
        }

        $this->data['research_type'] = Research_type::where('id', $this->data['student']->research_type)->first();

        if (Auth::user()->hasRole("staff")) {
            $this->data['staffs'] = ClearanceStaff::where('created_by', Auth::user()->id)->latest('id')->get();
            $this->data['students'] = ClearanceStudent::where('supervisor', Auth::user()->id)->oldest('id')->get();

            //notify
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();

            $this->data['notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("hod")) {
            //notify
            $students_number = ClearanceStudent::where('department', $dept['name'])
                ->where('approval_status', 'hod')
                ->oldest('id')->count();
            $staff_number = ClearanceStaff::where('department', $dept['name'])
                ->where('approval_status', 'hod')
                ->oldest('id')->count();
            $ethical_number = Ethical::where('department', $dept['name'])->where('approval_status', 'hod')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("principal")) {
            $students_number = ClearanceStudent::where('college', $coll['name'])->where('approval_status', 'principal')->latest('id')->count();
            $staff_number = ClearanceStaff::where('college', $coll['name'])->where('approval_status', 'principal')->count();
            $ethical_number = Ethical::where('college', $coll['name'])->where('approval_status', 'drp')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("drp")) {
            $students_number = ClearanceStudent::where('approval_status', 'drp')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'drp')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'drp')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("dvc")) {
            $students_number = ClearanceStudent::where('approval_status', 'dvc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'dvc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'dvc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("vc")) {
            $students_number = ClearanceStudent::where('approval_status', 'vc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'vc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'vc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("admin")) {
            $students_number = ClearanceStudent::latest('id')->count();
            $staff_number = ClearanceStaff::latest('id')->count();
            $ethical_number = Ethical::latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }
        return view('clearance.edit_student_clearance')->with('data', $this->data);
    }


    //update student clearance
    public function update_student_clearance(Request $request, $id): RedirectResponse
    {
        $re = $request->all();
        if (!empty($request->file('proposal_attachment'))) {
            $this->validate($request, ['proposal_attachment' => 'required|mimes:pdf']);
        }
        if (!empty($request->file('student_id'))) {
            $this->validate($request, ['student_id' => 'required|mimes:pdf']);
        }
        if (!empty($request->file('financial_statement'))) {
            $this->validate($request, ['financial_statement' => 'required|mimes:pdf']);
        }

        //deal with uploaded file
        $this->validate($request, [
            'nationality' => 'required',
            'reg_no' => 'required',
            'entry_year' => 'required',
            'degree_program' => 'required',
            'duration' => 'required',
            'program_mode' => 'required',
            'delivery_mode' => 'required',
            'contact' => 'required',
            'supervisor' => 'required',
            'research_title' => 'required',
            'research_type' => 'required',
            'commencement_date' => 'required',
            'completion_date' => 'required',
            'research_budget' => 'required',
            'fund_source' => 'required',
            'region' => 'required',
            'district' => 'required'
        ]);

        $pro_files = $request->file('proposal_attachment');
        if ($pro_files) {
            $destinationPath = 'updloads/'; // upload path
            $pro_filename = date('YmdHis') . "pro" . "." . $pro_files->getClientOriginalExtension();
            $pro_files->move($destinationPath, $pro_filename);
        }

        $id_files = $request->file('student_id');
        if ($id_files) {
            $destinationPath = 'updloads/'; // upload path
            $id_filename = date('YmdHis') . "studentid" . "." . $id_files->getClientOriginalExtension();
            $id_files->move($destinationPath, $id_filename);
        }

        $finance_files = $request->file('financial_statement');
        if ($finance_files) {
            $destinationPath = 'updloads/'; // upload path
            $finance_filename = date('YmdHis') . "finance" . "." . $finance_files->getClientOriginalExtension();
            $finance_files->move($destinationPath, $finance_filename);
        }

        //clearance information
        $clearance = ClearanceStudent::find($id);
        $clearance->names = $request->get('names');
        $clearance->gender = $request->get('gender');
        $clearance->nationality = $request->get('nationality');
        $clearance->reg_no = $request->get('reg_no');
        $clearance->year_entry = $request->get('entry_year');
        $clearance->department_id = $request->get('department_id');
        $clearance->college_id = $request->get('college_id');

        $clearance->degree_program = $request->get('degree_program');
        $clearance->duration = $request->get('duration');
        $clearance->program_mode = $request->get('program_mode');
        $clearance->contact = $request->get('contact');
        $clearance->supervisor = $request->get('supervisor');
        $clearance->research_title = $request->get('research_title');
        $clearance->research_type = $request->get('research_type');
        $clearance->commencement_date = $request->get('commencement_date');
        $clearance->completion_date = $request->get('completion_date');
        $clearance->research_budget = floatval(str_replace(",", "", $request->get('research_budget')));

        $clearance->fund_source = $request->get('fund_source');
        if (!empty($request->file('proposal_attachment'))) {
            $clearance->proposal_attachment = $pro_filename;
        }
        if (!empty($request->file('student_id'))) {
            $clearance->student_identity = $id_filename;
        }
        if (!empty($request->file('financial_statement'))) {
            $clearance->finance_statement = $finance_filename;
        }

        $clearance->level = "1";
        $clearance->approval_status = "supervisor";
        $clearance->delivery_mode = $request->get('delivery_mode');
        $clearance->created_by = Auth::user()->id;
        $clearance->save();

        //store sites details
        ResearchSite::where('clearance_student_id', $id)->delete();

        $temp = count($request->get('site_count'));
        for ($i = 0; $i < $temp; $i++) {

            $region = Region::find($request->get('region')[$i]);
            $district = $request->get('district')[$i];

            if (!empty($request->get('ward'))) {
                $ward = $request->get('ward')[$i];
            }

            if (!empty($request->get('village'))) {
                $village = $request->get('village')[$i];
            }

            $site = new ResearchSite();
            $site->clearance_student_id = $clearance->id;
            $site->organization = $request->get('organization')[$i];
            $site->title = $request->get('title')[$i];
            $site->address = $request->get('address')[$i];
            $site->region = $region['name'];
            $site->district = $district;

            if (!empty($request->get('ward'))) {
                $site->ward = $ward;
            }
            if (!empty($request->get('village'))) {
                $site->village = $village;
            }
            $site->save();
        }

        //construct notification message to supervisor
        $message = "The clearance request from <b><i> $clearance->names</i></b> with research title <b>$clearance->research_title</b>, has been updated.";
        $message .= "<br/> Login in the system by  <a href=" . url('/clearance/process') . ">click here</a> and act on the request.";
        $message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

        //send notification to supervisor
        $this->send_notification($clearance->supervisor, $message);

        return redirect()->route('clearance')->with('success', 'You have successfully
        updated your clearance and submitted to Supervisor');
    }


    //edit student clearance by admin
    public function edit_std_clearance_admin($id)
    {
        $this->data['title'] = 'edit clearance admin';
        $this->data['student'] = ClearanceStudent::find($id);

        $this->data['supervisor'] = User::find($this->data['student']->supervisor);

        $this->data['research_types'] = Research_type::orderBy('name', 'asc')->get();

        $this->data['departments'] = Department::pluck('name', 'id');
        $this->data['regions'] = Region::all();
        $this->data['districts'] = District::all();
        $this->data['wards'] = Ward::all();
        $this->data['villages'] = Village::all();

        $this->data['user'] = $this->data['student']->names;
        $this->data['reg_number'] = $this->data['student']->reg_no;
        $this->data['gender'] = $this->data['student']->gender;
        //college selection pool
        $colleges = College::all(['id', 'name']);
        $this->data['college_pool'] = $colleges;
        $this->data['college'] = College::where('id', $this->data['student']->college_id)->first();
        $dept_pool = Department::all(['id', 'name']);
        $this->data['dept_pool'] = $dept_pool;
        $this->data['dept'] = Department::where('id', $this->data['student']->department_id)->first();

        $this->data['gender'] = $this->data['student']->gender;

        $this->data['countries'] = Country::orderBy('name', 'asc')->get();

        $this->data['staff_details'] = User::orderBy('first_name', 'asc')->whereHas('roles', function ($q) {
            $q->where('name', 'staff');
        })->get();

        $dept_id = Auth::user()->department_id;
        $dept = Department::find($dept_id);

        $coll_id = Auth::user()->college_id;
        $coll = College::find($coll_id);

        $this->data['sites'] = ResearchSite::where('clearance_student_id', $id)->get();

        foreach ($this->data['sites'] as $key => $value) {
            $this->data['sites'][$key]->selected_region = Region::where('name', $value->region)->first();
        }

        $this->data['research_type'] = Research_type::where('id', $this->data['student']->research_type)->first();

        if (Auth::user()->hasRole("staff")) {
            $this->data['staffs'] = ClearanceStaff::where('created_by', Auth::user()->id)->latest('id')->get();
            $this->data['students'] = ClearanceStudent::where('supervisor', Auth::user()->id)->oldest('id')->get();

            //notify
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();

            $this->data['notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("hod")) {
            //notify
            $students_number = ClearanceStudent::where('department', $dept_id)
                ->where('approval_status', 'hod')
                ->oldest('id')->count();
            $staff_number = ClearanceStaff::where('department', $dept_id)
                ->where('approval_status', 'hod')
                ->oldest('id')->count();
            //$ethical_number = Ethical::where('department', $dept['name'])->where('approval_status', 'hod')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number;
        }

        if (Auth::user()->hasRole("principal")) {
            $students_number = ClearanceStudent::where('college', $coll_id)->where('approval_status', 'principal')->latest('id')->count();
            $staff_number = ClearanceStaff::where('college', $coll_id)->where('approval_status', 'principal')->count();
            //$ethical_number = Ethical::where('college', $coll['name'])->where('approval_status', 'drp')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number;
        }

        if (Auth::user()->hasRole("drp")) {
            $students_number = ClearanceStudent::where('approval_status', 'drp')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'drp')->latest('id')->count();
            //$ethical_number = Ethical::where('approval_status', 'drp')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number;
        }

        if (Auth::user()->hasRole("dvc")) {
            $students_number = ClearanceStudent::where('approval_status', 'dvc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'dvc')->latest('id')->count();
            //$ethical_number = Ethical::where('approval_status', 'dvc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number;
        }

        if (Auth::user()->hasRole("vc")) {
            $students_number = ClearanceStudent::where('approval_status', 'vc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'vc')->latest('id')->count();
            //$ethical_number = Ethical::where('approval_status', 'vc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number;
        }

        if (Auth::user()->hasRole("admin")) {
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();
            $students_number = ClearanceStudent::latest('id')->count();
            $staff_number = ClearanceStaff::latest('id')->count();
            //$ethical_number = Ethical::latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number;
            $this->data['supervise_notifications'] = $supervise_students_number;
        }
        return view('clearance.edit_std_clearance_admin')->with('data', $this->data);
    }


    //update student clearance by admin
    public function update_std_clearance_admin(Request $request, $id): RedirectResponse
    {
        $re = $request->all();

        //deal with uploaded file
        $this->validate($request, [
            'nationality' => 'required',
            'entry_year' => 'required',
            'degree_program' => 'required',
            'duration' => 'required',
            'program_mode' => 'required',
            'delivery_mode' => 'required',
            'contact' => 'required',
            'supervisor' => 'required',
            'research_title' => 'required',
            'research_type' => 'required',
            'commencement_date' => 'required',
            'completion_date' => 'required',
            'research_budget' => 'required',
            'fund_source' => 'required',
            'region' => 'required',
            'district' => 'required'
        ]);

        //clearance information
        $clearance = ClearanceStudent::find($id);
        $clearance->nationality = $request->get('nationality');
        $clearance->year_entry = $request->get('entry_year');
        $clearance->department_id = $request->get('department_id');
        $clearance->college_id = $request->get('college_id');

        $clearance->degree_program = $request->get('degree_program');
        $clearance->duration = $request->get('duration');
        $clearance->program_mode = $request->get('program_mode');
        $clearance->contact = $request->get('contact');
        $clearance->supervisor = $request->get('supervisor');
        $clearance->research_title = $request->get('research_title');
        $clearance->research_type = $request->get('research_type');
        $clearance->commencement_date = $request->get('commencement_date');
        $clearance->completion_date = $request->get('completion_date');
        $clearance->research_budget = floatval(str_replace(",", "", $request->get('research_budget')));
        $clearance->fund_source = $request->get('fund_source');

        $clearance->delivery_mode = $request->get('delivery_mode');
        $clearance->save();

        //store sites details
        ResearchSite::where('clearance_student_id', $id)->delete();

        $temp = count($request->get('site_count'));
        for ($i = 0; $i < $temp; $i++) {

            $region = Region::find($request->get('region')[$i]);
            $district = $request->get('district')[$i];

            if (!empty($request->get('ward'))) {
                $ward = $request->get('ward')[$i];
            }

            if (!empty($request->get('village'))) {
                $village = $request->get('village')[$i];
            }

            $site = new ResearchSite();
            $site->clearance_student_id = $clearance->id;
            $site->organization = $request->get('organization')[$i];
            $site->title = $request->get('title')[$i];
            $site->address = $request->get('address')[$i];
            $site->region = $region['name'];
            $site->district = $district;

            if (!empty($request->get('ward'))) {
                $site->ward = $ward;
            }
            if (!empty($request->get('village'))) {
                $site->village = $village;
            }
            $site->save();
        }

        return redirect()->route('clearance')->with('success', 'You have successfully
        updated the student clearance');
    }


    //add student single site
    public function add_student_site($id)
    {
        $this->data['title'] = 'add research site';
        $std_clearance = ClearanceStudent::find($id);

        $this->data['regions'] = Region::all();

        //for notification
        if (Auth::user()->hasRole("admin")) {
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();
            $students_number = ClearanceStudent::latest('id')->count();
            $staff_number = ClearanceStaff::latest('id')->count();
            $ethical_number = Ethical::latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
            $this->data['supervise_notifications'] = $supervise_students_number;
        }

        return view('clearance.add_student_clearance_site', [
            'std_clearance' => $std_clearance,
        ])->with('data', $this->data);
    }


    //store added research site
    function store_added_site(Request $request, $id): RedirectResponse
    {
        $clearance = ClearanceStudent::find($id);

        //validate the added site
        $this->validate($request, [
            'organization' => 'required', //this does not work ???
            'title' => 'required', //this does not work ???
            'address' => 'required',
            'region' => 'required', //this does not work ???
            'district' => 'required', //this does not work ???
        ]);

        $region = Region::find($request->get('region'));

        $site = new ResearchSite();
        $site->organization = $request->get('organization')[0];
        $site->title = $request->get('title')[0];
        $site->address = $request->get('address');
        $site->region = $region['name'];
        $site->district = $request->get('district')[0];
        $site->ward = $request->get('ward')[0];
        $site->village = $request->get('village')[0];
        $site->clearance_student_id = $clearance->id;
        $site->created_at = date('Y-m-d H:i:s');
        $site->updated_at = date('Y-m-d H:i:s');
        $site->save();

        return redirect()->route('clearance')->with('success', 'You have successfully
        updated your clearance and submitted to Supervisor');
    }


    //get student reverse recommendations panel
    function back_panel_student($id)
    {
        $this->data['title'] = 'Reverse recommendations';
        $this->data['student'] = ClearanceStudent::find($id);

        return view('clearance.return_recommendation_panel_student')->with('data', $this->data);
    }

    //get store reverse recommendation and change status for student
    function save_reverse_recomendation(Request $request, $id)
    {
        $reco = new ClearanceRecommendation();
        $clearance = ClearanceStudent::find($id);

        if ($clearance->level === "R") {
            $clearance->level = "0";
            $clearance->approval_status = "rejected";

            $reco->clearance_student_id = $id;
            $reco->from = "student";
            $reco->comment = $request->get('recommendation');

        }elseif ($clearance->level == "1") {
            $clearance->level = "R";
            $clearance->approval_status = "reversed";

            $reco->clearance_student_id = $id;
            $reco->from = "supervisor";
            $reco->comment = $request->get('recommendation');
        } elseif ($clearance->level == "2") {
            $clearance->level = "1";
            $clearance->approval_status = "supervisor";

            $reco->clearance_student_id = $id;
            $reco->from = "hod";
            $reco->comment = $request->get('recommendation');
        } elseif ($clearance->level == "3") {
            $clearance->level = "2";
            $clearance->approval_status = "hod";

            $reco->clearance_student_id = $id;
            $reco->from = "principal";
            $reco->comment = $request->get('recommendation');
        } elseif ($clearance->level == "4") {
            $clearance->level = "3";
            $clearance->approval_status = "principal";

            $reco->clearance_student_id = $id;
            $reco->from = "drp";
            $reco->comment = $request->get('recommendation');
        } elseif ($clearance->level == "5") {
            $clearance->level = "4";
            $clearance->approval_status = "drp";

            $reco->clearance_student_id = $id;
            $reco->from = "dvc";
            $reco->comment = $request->get('recommendation');
        }
        $clearance->save();
        $reco->save();

        //extract the user by role as recipients of notification email
        $roleDRP = Role::where('name', 'drp')->first();
        $rolePrincipal = Role::where('name', 'principal')->first();
        $roleHoD = Role::where('name', 'hod')->first();

        $user_drp = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleDRP->id)
            ->select('users.id', 'users.surname')
            ->first();

        $user_principal = DB::table('users')
            ->join('clearance_students', 'users.college_id', '=', 'clearance_students.college_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $rolePrincipal->id)
            ->where('clearance_students.college_id', '=', $clearance->college_id)
            ->select('users.id', 'users.surname')
            ->first();

        $user_hod = DB::table('users')
            ->join('clearance_students', 'users.department_id', '=', 'clearance_students.department_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleHoD->id)
            ->where('clearance_students.department_id', '=', $clearance->department_id)
            ->select('users.id', 'users.surname')
            ->first();

        //construct reverse notification message
        $message = "The clearance request from <b><i> $clearance->names</i></b> with research title <b>$clearance->research_title</b>, has been reversed.";
        $message .= "<br/> Please log in the system by  <a href=" . url('/clearance/process') . ">click here</a> and act on the recommendations.";
        $message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

        //send notification to respective person
        if ($clearance->level == "4") {
            $this->send_notification($user_drp->id, $message);
        } else if ($clearance->level == "3") {
            $this->send_notification($user_principal->id, $message);
        } else if ($clearance->level == "2") {
            $this->send_notification($user_hod->id, $message);
        } else if ($clearance->level == "1") {
            $this->send_notification($clearance->supervisor, $message);
        } else {
            $this->send_notification($clearance->created_by, $message);
        }

        return redirect()->route('clearance')->with('success', 'Clearance is successfully returned');
    }

    //student clearance forward recommendations panel
    public function recommendation_panel($id)
    {
        $this->data['title'] = 'Add recommendations';
        $this->data['student'] = ClearanceStudent::find($id);

        return view('clearance.recommendation_panel')->with('data', $this->data);
    }

    // save student clearance recommendations
    public function save_recommendation(Request $request, $id): RedirectResponse
    {
        $reco = new ClearanceRecommendation();
        $clearance = ClearanceStudent::find($id);

        if ($clearance->level == "1") {
            $clearance->level = "2";
            $clearance->approval_status = "hod";

            $reco->clearance_student_id = $id;
            $reco->from = "supervisor";
            $reco->comment = $request->get('recommendation');
        } elseif ($clearance->level == "2") {
            $clearance->level = "3";
            $clearance->approval_status = "principal";

            $reco->clearance_student_id = $id;
            $reco->from = "hod";
            $reco->comment = $request->get('recommendation');
        } elseif ($clearance->level == "3") {
            $clearance->level = "4";
            $clearance->approval_status = "drp";

            $reco->clearance_student_id = $id;
            $reco->from = "principal";
            $reco->comment = $request->get('recommendation');
        } elseif ($clearance->level == "4") {
            $clearance->level = "5";
            $clearance->approval_status = "dvc";

            $reco->clearance_student_id = $id;
            $reco->from = "drp";
            $reco->comment = $request->get('recommendation');
        } elseif ($clearance->level == "5") {
            $clearance->level = "6";
            $clearance->approval_status = "vc";

            $reco->clearance_student_id = $id;
            $reco->from = "dvc";
            $reco->comment = $request->get('recommendation');
        }
        $clearance->save();
        $reco->save();

        //extract the user by role as recipients of notification email
        $roleVC = Role::where('name', 'vc')->first();
        $roleDVC = Role::where('name', 'dvc')->first();
        $roleDRP = Role::where('name', 'drp')->first();
        $rolePrincipal = Role::where('name', 'principal')->first();
        $roleHoD = Role::where('name', 'hod')->first();

        $user_vc = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleVC->id)
            ->select('users.id', 'users.surname')
            ->first();

        $user_dvc = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleDVC->id)
            ->select('users.id', 'users.surname')
            ->first();

        $user_drp = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleDRP->id)
            ->select('users.id', 'users.surname')
            ->first();

        $user_principal = DB::table('users')
            ->join('clearance_students', 'users.college_id', '=', 'clearance_students.college_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $rolePrincipal->id)
            ->where('clearance_students.college_id', '=', $clearance->college_id)
            ->select('users.id', 'users.surname')
            ->first();

        $user_hod = DB::table('users')
            ->join('clearance_students', 'users.department_id', '=', 'clearance_students.department_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleHoD->id)
            ->where('clearance_students.department_id', '=', $clearance->department_id)
            ->select('users.id', 'users.surname')
            ->first();

        //construct forward notification message
        $message = "The clearance request from <b><i> $clearance->names</i></b> with research title <b>$clearance->research_title</b>, has been forwarded to you for recommendation.";
        $message .= "<br/> Please log in the system by  <a href=" . url('/clearance/process') . ">click here</a> and act on the request.";
        $message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

        //send notification to respective person
        if ($clearance->level == "6") {
            $this->send_notification($user_vc->id, $message);
        } else if ($clearance->level == "5") {
            $this->send_notification($user_dvc->id, $message);
        } else if ($clearance->level == "4") {
            $this->send_notification($user_drp->id, $message);
        } else if ($clearance->level == "3") {
            $this->send_notification($user_principal->id, $message);
        } else if ($clearance->level == "2") {
            if (isset($user_hod)) {
                $this->send_notification($user_hod->id, $message);
            }
        } else {
            $this->send_notification($clearance->supervisor, $message);
        }

        return redirect()->route('clearance')->with('success', 'Clearance is successfully forwarded');
    }

    // save vc approval status
    public function approve_student_clearance(Request $request, $id)
    {
        $approved_date = date('Y-m-d');

        $clearance = ClearanceStudent::find($id);
        $clearance->approval_status = $request->get('approve_status');
        $clearance->approved_date = $approved_date;
        $clearance->level = "7";
        $clearance->save();
        $status = $request->get('approve_status');

        if ($status === 'rejected') {
            return redirect()->route('clearance')->with('danger', 'Clearance is successfully rejected');
        }

        if ($status === 'approved') {
            return redirect()->route('student_clearance_letter', ['id' => $id]);
        }
        return redirect()->route('student_clearance_letter', ['id' => $id]);
    }

    // student clearance letters
    public function student_clearance_letter($id)
    {
        $cl = ClearanceStudent::find($id);
        $this->data['clearance'] = $cl;
        $user = User::find($cl->created_by);
        $this->data['user'] = $user;

        $sites = ResearchSite::where('clearance_student_id', $id)->get();

        $this->data['sites'] = $sites;

        return view('clearance.student_letter')->with('data', $this->data);
    }

    // ras student
    public function ras_student_letter($id)
    {

        $site = ResearchSite::where('id', $id)->first();

        $this->data['site'] = $site;
        $cl = ClearanceStudent::find($site->clearance_student_id);
        $this->data['clearance'] = $cl;
        $user = User::find($cl->created_by);
        $this->data['user'] = $user;


        $this->data['vc'] = User::whereHas('roles', function ($q) {
            $q->where('name', 'vc');
        })->first();

        return view('clearance.ras_student_letter')->with('data', $this->data);
    }

    // das student
    public function das_student_letter($id)
    {
        $site = ResearchSite::where('id', $id)->first();

        $this->data['site'] = $site;
        $cl = ClearanceStudent::find($site->clearance_student_id);
        $this->data['clearance'] = $cl;
        $user = User::find($cl->created_by);
        $this->data['user'] = $user;

        $this->data['vc'] = User::whereHas('roles', function ($q) {
            $q->where('name', 'vc');
        })->first();

        return view('clearance.das_student_letter')->with('data', $this->data);
    }

    // org student
    public function org_student_letter($id)
    {
        $site = ResearchSite::where('id', $id)->first();

        $this->data['site'] = $site;
        $cl = ClearanceStudent::find($site->clearance_student_id);
        $this->data['clearance'] = $cl;
        $user = User::find($cl->created_by);
        $this->data['user'] = $user;

        $this->data['vc'] = User::whereHas('roles', function ($q) {
            $q->where('name', 'vc');
        })->first();

        return view('clearance.organization_student_letter')->with('data', $this->data);
    }

    // twmc student
    public function twmc_student_letter($id)
    {
        $site = ResearchSite::where('id', $id)->first();

        $this->data['site'] = $site;
        $cl = ClearanceStudent::find($site->clearance_student_id);
        $this->data['clearance'] = $cl;
        $user = User::find($cl->created_by);
        $this->data['user'] = $user;

        $this->data['vc'] = User::whereHas('roles', function ($q) {
            $q->where('name', 'vc');
        })->first();

        return view('clearance.twmc_student_letter')->with('data', $this->data);
    }


    // referred student clearance letters
    public function ref_student_clearance_letter($id)
    {
        $cl = ClearanceStudent::find($id);
        $this->data['clearance'] = $cl;
        $user = User::find($cl->created_by);
        $this->data['user'] = $user;

        $sites = ResearchSite::where('clearance_student_id', $id)->get();

        $this->data['sites'] = $sites;

        return view('clearance.ref_student_letter')->with('data', $this->data);
    }

    // ras student
    public function ref_ras_student_letter($id)
    {

        $site = ResearchSite::where('id', $id)->first();

        $this->data['site'] = $site;
        $cl = ClearanceStudent::find($site->clearance_student_id);
        $this->data['clearance'] = $cl;
        $user = User::find($cl->created_by);
        $this->data['user'] = $user;


        $this->data['vc'] = User::whereHas('roles', function ($q) {
            $q->where('name', 'referred_vc');
        })->first();

        return view('clearance.ref_ras_student_letter')->with('data', $this->data);
    }

    // das student
    public function ref_das_student_letter($id)
    {
        $site = ResearchSite::where('id', $id)->first();

        $this->data['site'] = $site;
        $cl = ClearanceStudent::find($site->clearance_student_id);
        $this->data['clearance'] = $cl;
        $user = User::find($cl->created_by);
        $this->data['user'] = $user;

        $this->data['vc'] = User::whereHas('roles', function ($q) {
            $q->where('name', 'referred_vc');
        })->first();

        return view('clearance.ref_das_student_letter')->with('data', $this->data);
    }

    // org student
    public function ref_org_student_letter($id)
    {
        $site = ResearchSite::where('id', $id)->first();

        $this->data['site'] = $site;
        $cl = ClearanceStudent::find($site->clearance_student_id);
        $this->data['clearance'] = $cl;
        $user = User::find($cl->created_by);
        $this->data['user'] = $user;

        $this->data['vc'] = User::whereHas('roles', function ($q) {
            $q->where('name', 'referred_vc');
        })->first();

        return view('clearance.ref_organization_student_letter')->with('data', $this->data);
    }

    // twmc student
    public function ref_twmc_student_letter($id)
    {
        $site = ResearchSite::where('id', $id)->first();

        $this->data['site'] = $site;
        $cl = ClearanceStudent::find($site->clearance_student_id);
        $this->data['clearance'] = $cl;
        $user = User::find($cl->created_by);
        $this->data['user'] = $user;

        $this->data['vc'] = User::whereHas('roles', function ($q) {
            $q->where('name', 'referred_vc');
        })->first();

        return view('clearance.ref_twmc_student_letter')->with('data', $this->data);
    }


    //staffs clearance form
    public function staff()
    {
        Perms::has_allowed('clearance_staffs', 'staff');

        $this->data['title'] = 'Staff Clearance';
        $this->data['regions'] = Region::pluck('name', 'id');
        $this->data['reseachers'] = User::orderBy('first_name', 'asc')
            ->whereHas('roles', function ($q) {
                $q->where('name', 'staff');
            })->get();

        $this->data['user'] = User::where('id', Auth::user()->id)->first();
        $this->data['college'] = College::where('id', $this->data['user']['college_id'])->first();

        $college_pool = College::all(['id', 'name']);
        $this->data['college_pool'] = $college_pool;

        $this->data['dept'] = Department::where('id', $this->data['user']['department_id'])->first();
        $dept_pool = Department::all(['id', 'name']);
        $this->data['dept_pool'] = $dept_pool;

        $this->data['countries'] = Country::orderBy('name', 'asc')->get();

        //notify
        if (Auth::user()->hasRole("staff")) {
            $this->data['staffs'] = ClearanceStaff::where('created_by', Auth::user()->id)->latest('id')->get();
            $this->data['students'] = ClearanceStudent::where('supervisor', Auth::user()->id)->oldest('id')->get();

            //notify
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();

            $this->data['notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("hod")) {
            //notify
            $students_number = ClearanceStudent::where('department_id', $this->data['dept']->id)->where('approval_status', 'hod')
                ->oldest('id')->count();
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();
            $staff_number = ClearanceStaff::where('department_id', $this->data['dept']->id)
                ->where('approval_status', 'hod')
                ->oldest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number;
            $this->data['supervise_notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("principal")) {
            $students_number = ClearanceStudent::where('college_id', $this->data['college']->id)->where('approval_status', 'principal')->latest('id')->count();
            $staff_number = ClearanceStaff::where('college_id', $this->data['college']->id)->where('approval_status', 'principal')->count();
            $this->data['notifications'] = $students_number + $staff_number;
            $this->data['supervise_notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("drp")) {
            $students_number = ClearanceStudent::where('approval_status', 'drp')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'drp')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'drp')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("dvc")) {
            $students_number = ClearanceStudent::where('approval_status', 'dvc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'dvc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'dvc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("vc")) {
            $students_number = ClearanceStudent::where('approval_status', 'vc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'vc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'vc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("admin")) {
            $students_number = ClearanceStudent::latest('id')->count();
            $staff_number = ClearanceStaff::latest('id')->count();
            $ethical_number = Ethical::latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        return view('clearance.staffs')->with('data', $this->data);
    }


    //store staff clearance
    public function store_staff_clearance(Request $request)
    {
        $this->validate($request, [
            'agreement_attachment' => 'required|mimes:pdf',
            'project_reg_no' => 'required',
            'research_title' => 'required',
            'research_type' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'fund_amount' => 'required',
            'fund_source' => 'required',
            'region' => 'required',
            'district' => 'required'
        ]);

        if (!empty($request->file('agreement_attachment'))) {
            $files = $request->file('agreement_attachment');
            if ($files) {
                $destinationPath = 'updloads/'; // upload path
                $filename = date('YmdHis') . "pro" . "." . $files->getClientOriginalExtension();
                $files->move($destinationPath, $filename);
            }
        }

        // save clearance for staff
        $clearance = new ClearanceStaff();
        $clearance->names = $request->get('names');
        $clearance->title = $request->get('staff_title');
        $clearance->college_id = $request->get('college_id');
        $clearance->department_id = $request->get('department_id');
        $clearance->email = $request->get('email');
        $clearance->phone = $request->get('phone');
        $clearance->project_reg_no = $request->get('project_reg_no');
        $clearance->research_title = $request->get('research_title');
        $clearance->research_type = $request->get('research_type');
        $clearance->start_date = $request->get('start_date');
        $clearance->end_date = $request->get('end_date');
        $clearance->fund_amount = floatval(str_replace(",", "", $request->get('fund_amount')));

        $clearance->fund_source = $request->get('fund_source');
        $clearance->other_researcher = $request->get('other_researcher');
        $clearance->agreement_attachment = $filename;
        $clearance->level = "1";
        $clearance->approval_status = "hod";

        $clearance->created_by = Auth::user()->id;
        $clearance->save();

        //sites details
        $temp = count($request->get('site_count'));
        for ($i = 0; $i < $temp; $i++) {

            $region = Region::find($request->get('region')[$i]);
            $district = $request->get('district')[$i];

            if (!empty($request->get('ward'))) {
                $ward = $request->get('ward')[$i];
            }

            if (!empty($request->get('village'))) {
                $village = $request->get('village')[$i];
            }

            $site = new ResearchSite();
            $site->clearance_staff_id = $clearance->id;
            $site->organization = $request->get('organization')[$i];
            $site->title = $request->get('title')[$i];
            $site->address = $request->get('address')[$i];
            $site->region = $region['name'];

            $site->district = $district;

            if (!empty($request->get('ward'))) {
                $site->ward = $ward;
            }
            if (!empty($request->get('village'))) {
                $site->village = $village;
            }
            $site->save();

            //deal with assigned researchers
            $researcher = new AssignedResearcher();
            $researcher->site_id = $site->id;
            $researcher->researcher_id = $request->get('assigned_researcher')[$i];
            $researcher->save();

            //deal with researchers assistant
            $assistant = new ResearcherAssistant();
            $assistant->site_id = $site->id;
            $assistant->name = $request->get('assistant')[$i];
            $assistant->save();
        }

        //construct notification message to HoD
        $message = "New clearance request for the project with title <b> $clearance->research_title </b> is submitted by <b><i> $clearance->names</i></b>";
        $message .= "<br/> Login in the system by  <a href=" . url('/clearance/process') . ">click here</a> and act on the request.";
        $message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

        //extract recipient user by role
        $roleHoD = Role::where('name', 'hod')->first();
        $user_hod = DB::table('users')
            ->join('clearance_staffs', 'users.department_id', '=', 'clearance_staffs.department_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleHoD->id)
            ->where('clearance_staffs.department_id', '=', $clearance->department_id)
            ->select('users.id')
            ->first();
        //send notification to hod
        $this->send_notification($user_hod->id, $message);

        return redirect()->route('clearance')->with('success', 'You have successfully
        submitted your clearance to HoD');
    }

    //open staff clearance
    public function show_staff_clearance($id)
    {
        $dept_id = Auth::user()->department_id;
        // $dept = Department::find($dept_id);

        $coll_id = Auth::user()->college_id;
        // $coll = College::find($coll_id);
        $this->data['title'] = 'View clearance';
        $this->data['staff'] = ClearanceStaff::find($id);

        $coll_clr = College::whereId($this->data['staff']->college_id)->first();
        $this->data['coll_clr'] = $coll_clr;
        $dept_clr = Department::whereId($this->data['staff']->department_id)->first();
        $this->data['dept_clr'] = $dept_clr;

        $this->data['sites'] = ResearchSite::where('clearance_staff_id', $id)->get();
        $this->data['recommendations'] = ClearanceRecommendation::where('clearance_staff_id', $id)->get();

        foreach ($this->data['sites'] as $key => $value) {
            $this->data['sites'][$key]->assistants = ResearcherAssistant::where('site_id', $value->id)->get();
            // $research_id = AssignedResearcher::where('site_id', $value->id)->first();

            // $research_id['researcher_id'])->first();
        }

        if (Auth::user()->hasRole("staff")) {
            $this->data['staffs'] = ClearanceStaff::where('created_by', Auth::user()->id)->latest('id')->get();
            $this->data['students'] = ClearanceStudent::where('supervisor', Auth::user()->id)->oldest('id')->get();

            //notify
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();

            $this->data['notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("hod")) {
            //notify
            $students_number = ClearanceStudent::where('department_id', $dept_id)->where('approval_status', 'hod')
                ->oldest('id')->count();
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();
            $staff_number = ClearanceStaff::where('department_id', $dept_id)
                ->where('approval_status', 'hod')
                // ->orWhere('approval_status','approved')
                ->oldest('id')->count();
            $ethical_number = Ethical::where('department', $dept_id)->where('approval_status', 'hod')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
            $this->data['supervise_notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("principal")) {
            $students_number = ClearanceStudent::where('college_id', $coll_id)->where('approval_status', 'principal')->latest('id')->count();
            $staff_number = ClearanceStaff::where('college_id', $coll_id)->where('approval_status', 'principal')->count();
            $ethical_number = Ethical::where('college', $coll_id)->where('approval_status', 'drp')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
            $this->data['supervise_notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("drp")) {
            $students_number = ClearanceStudent::where('approval_status', 'drp')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'drp')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'drp')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("dvc")) {
            $students_number = ClearanceStudent::where('approval_status', 'dvc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'dvc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'dvc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("vc")) {
            $students_number = ClearanceStudent::where('approval_status', 'vc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'vc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'vc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("admin")) {
            $students_number = ClearanceStudent::latest('id')->count();
            $staff_number = ClearanceStaff::latest('id')->count();
            $ethical_number = Ethical::latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }
        return view('clearance.show_staff')->with('data', $this->data);
    }


    //edit staff clearance
    public function edit_staff_clearance($id)
    {
        // Perms::has_allowed('clearance_staff', 'show_staff_clearance');
        $this->data['title'] = 'edit clearance';
        $this->data['staff'] = ClearanceStaff::find($id);

        $this->data['research_types'] = Research_type::orderBy('name', 'asc')->get();

        $this->data['departments'] = Department::pluck('name', 'id');
        $this->data['regions'] = Region::all();
        $this->data['districts'] = District::all();
        $this->data['wards'] = Ward::all();
        $this->data['villages'] = Village::all();

        $this->data['user'] = User::where('id', Auth::user()->id)->first();
        $this->data['college'] = College::where('id', $this->data['user']['college_id'])->first();
        $this->data['dept'] = Department::where('id', $this->data['user']['department_id'])->first();

        $this->data['countries'] = Country::orderBy('name', 'asc')->get();

        $this->data['staff_details'] = User::orderBy('first_name', 'asc')->whereHas('roles', function ($q) {
            $q->where('name', 'staff');
        })->get();

        $dept_id = Auth::user()->department_id;
        $dept = Department::find($dept_id);

        $coll_id = Auth::user()->college_id;
        $coll = College::find($coll_id);

        $dept_pool = Department::all('id', 'name');
        $this->data['dept_pool'] = $dept_pool;

        $college_pool = College::all('id', 'name');
        $this->data['college_pool'] = $college_pool;

        $this->data['sites'] = ResearchSite::where('clearance_staff_id', $id)->get();

        foreach ($this->data['sites'] as $key => $value) {
            $this->data['sites'][$key]->selected_region = Region::where('name', $value->region)->first();
        }

        $this->data['research_type'] = ClearanceStaff::where('id', $this->data['staff']->research_type)->first();

        if (Auth::user()->hasRole("staff")) {
            $this->data['staffs'] = ClearanceStaff::where('created_by', Auth::user()->id)->latest('id')->get();
            $this->data['students'] = ClearanceStudent::where('supervisor', Auth::user()->id)->oldest('id')->get();

            //notify
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();
            $this->data['notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("hod")) {
            //notify
            $students_number = ClearanceStudent::where('department', $dept['name'])
                ->where('approval_status', 'hod')
                ->oldest('id')->count();
            $staff_number = ClearanceStaff::where('department', $dept['name'])
                ->where('approval_status', 'hod')
                ->oldest('id')->count();
            $ethical_number = Ethical::where('department', $dept['name'])->where('approval_status', 'hod')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("principal")) {
            $students_number = ClearanceStudent::where('college', $coll['name'])->where('approval_status', 'principal')->latest('id')->count();
            $staff_number = ClearanceStaff::where('college', $coll['name'])->where('approval_status', 'principal')->count();
            $ethical_number = Ethical::where('college', $coll['name'])->where('approval_status', 'drp')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("drp")) {
            $students_number = ClearanceStudent::where('approval_status', 'drp')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'drp')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'drp')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("dvc")) {
            $students_number = ClearanceStudent::where('approval_status', 'dvc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'dvc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'dvc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("vc")) {
            $students_number = ClearanceStudent::where('approval_status', 'vc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'vc')->latest('id')->count();
            $ethical_number = Ethical::where('approval_status', 'vc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        if (Auth::user()->hasRole("admin")) {
            $students_number = ClearanceStudent::latest('id')->count();
            $staff_number = ClearanceStaff::latest('id')->count();
            $ethical_number = Ethical::latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
        }

        return view('clearance.edit_staff_clearance')->with('data', $this->data);
    }


    //update staff clearance
    public function update_staff_clearance(Request $request, $id): RedirectResponse
    {
        $re = $request->all();

        if (!empty($request->file('contract_attachment'))) {
            $this->validate($request, ['contract_attachment' => 'required|mimes:pdf']);
        }

        $pro_files = $request->file('contract_attachment');
        if ($pro_files) {
            $destinationPath = 'updloads/'; // upload path
            $pro_filename = date('YmdHis') . "pro" . "." . $pro_files->getClientOriginalExtension();
            $pro_files->move($destinationPath, $pro_filename);
        }

        //clearance information
        $clearance = ClearanceStaff::find($id);

        $clearance->college_id = $request->get('college_id');
        $clearance->department_id = $request->get('department_id');
        $clearance->email = $request->get('email');
        $clearance->phone = $request->get('phone');
        $clearance->project_reg_no = $request->get('project_reg_no');
        $clearance->research_title = $request->get('research_title');
        $clearance->research_type = $request->get('research_type');
        $clearance->start_date = $request->get('start_date');
        $clearance->end_date = $request->get('end_date');
        $clearance->fund_amount = floatval(str_replace(",", "", $request->get('fund_amount')));

        $clearance->fund_source = $request->get('fund_source');
        $clearance->other_researcher = $request->get('other_researcher');

        $clearance->level = "1";
        $clearance->approval_status = "hod";

        $clearance->created_by = Auth::user()->id;

        if (!empty($request->file('contract_attachment'))) {
            $clearance->agreement_attachment = $pro_filename;
        }
        $clearance->save();

        //store sites details
        ResearchSite::where('clearance_staff_id', $id)->delete();

        $temp = count($request->get('site_count'));
        for ($i = 0; $i < $temp; $i++) {

            $region = Region::find($request->get('region')[$i]);
            $district = $request->get('district')[$i];

            if (!empty($request->get('ward'))) {
                $ward = $request->get('ward')[$i];
            }

            if (!empty($request->get('village'))) {
                $village = $request->get('village')[$i];
            }

            $site = new ResearchSite();
            $site->clearance_staff_id = $clearance->id;
            $site->organization = $request->get('organization')[$i];
            $site->title = $request->get('title')[$i];
            $site->address = $request->get('address')[$i];
            $site->region = $region['name'];
            $site->district = $district;

            if (!empty($request->get('ward'))) {
                $site->ward = $ward;
            }
            if (!empty($request->get('village'))) {
                $site->village = $village;
            }
            $site->save();
        }

        //construct notification message to hod after update
        $message = "The clearance request from <b><i> $clearance->names</i></b> with research title <b>$clearance->research_title</b>, has been updated.";
        $message .= "<br/> Login in the system by  <a href=" . url('/clearance/process') . ">click here</a> and act on the request.";
        $message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

        //extract recipient user by role
        $roleHoD = Role::where('name', 'hod')->first();
        $user_hod = DB::table('users')
            ->join('clearance_staffs', 'users.department_id', '=', 'clearance_staffs.department_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleHoD->id)
            ->where('clearance_staffs.department_id', '=', $clearance->department_id)
            ->select('users.id')
            ->first();
        //send notification to hod
        $this->send_notification($user_hod, $message);

        return redirect()->route('clearance')->with('success', 'You have successfully
        updated your clearance and submitted to Head of Department');
    }


    //edit staff clearance by admin
    public function edit_staff_clearance_admin($id)
    {
        // Perms::has_allowed('clearance_staff', 'show_staff_clearance');
        $this->data['title'] = 'edit clearance';
        $this->data['staff'] = ClearanceStaff::find($id);

        $this->data['research_types'] = Research_type::orderBy('name', 'asc')->get();

        $this->data['departments'] = Department::pluck('name', 'id');
        $this->data['regions'] = Region::all();
        $this->data['districts'] = District::all();
        $this->data['wards'] = Ward::all();
        $this->data['villages'] = Village::all();

        $this->data['user'] = $this->data['staff']->names;
        $this->data['rank'] = $this->data['staff']->title;

        $this->data['countries'] = Country::orderBy('name', 'asc')->get();

        $this->data['staff_details'] = User::orderBy('first_name', 'asc')->whereHas('roles', function ($q) {
            $q->where('name', 'staff');
        })->get();

        $dept_id = Auth::user()->department_id;
        $dept = Department::find($dept_id);

        $coll_id = Auth::user()->college_id;
        $coll = College::find($coll_id);

        //college staff clearance
        $college_clr = College::whereId($this->data['staff']->college_id)->first();
        $this->data['college'] = $college_clr;
        $college_pool = college::all('id', 'name');
        $this->data['college_pool'] = $college_pool;

        //department staff clearance
        $dept_clr = Department::whereId($this->data['staff']->department_id)->first();
        $this->data['dept'] = $dept_clr;
        $dept_pool = Department::all('id', 'name');
        $this->data['dept_pool'] = $dept_pool;

        $this->data['sites'] = ResearchSite::where('clearance_staff_id', $id)->get();

        foreach ($this->data['sites'] as $key => $value) {
            $this->data['sites'][$key]->selected_region = Region::where('name', $value->region)->first();
        }

        $this->data['research_type'] = ClearanceStaff::where('id', $this->data['staff']->research_type)->first();

        if (Auth::user()->hasRole("staff")) {
            $this->data['staffs'] = ClearanceStaff::where('created_by', Auth::user()->id)->latest('id')->get();
            $this->data['students'] = ClearanceStudent::where('supervisor', Auth::user()->id)->oldest('id')->get();

            //notify
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();
            $this->data['notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("hod")) {
            //notify
            $students_number = ClearanceStudent::where('department', $dept_id)
                ->where('approval_status', 'hod')
                ->oldest('id')->count();
            $staff_number = ClearanceStaff::where('department', $dept_id)
                ->where('approval_status', 'hod')
                ->oldest('id')->count();
            //$ethical_number = Ethical::where('department', $dept['name'])->where('approval_status', 'hod')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number;
        }

        if (Auth::user()->hasRole("principal")) {
            $students_number = ClearanceStudent::where('college', $coll_id)->where('approval_status', 'principal')->latest('id')->count();
            $staff_number = ClearanceStaff::where('college', $coll_id)->where('approval_status', 'principal')->count();
            //$ethical_number = Ethical::where('college', $coll['name'])->where('approval_status', 'drp')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number;
        }

        if (Auth::user()->hasRole("drp")) {
            $students_number = ClearanceStudent::where('approval_status', 'drp')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'drp')->latest('id')->count();
            //$ethical_number = Ethical::where('approval_status', 'drp')->latest('id')->count();

            $this->data['notifications'] = $students_number + $staff_number;
        }

        if (Auth::user()->hasRole("dvc")) {
            $students_number = ClearanceStudent::where('approval_status', 'dvc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'dvc')->latest('id')->count();
            //$ethical_number = Ethical::where('approval_status', 'dvc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number;
        }

        if (Auth::user()->hasRole("vc")) {
            $students_number = ClearanceStudent::where('approval_status', 'vc')->latest('id')->count();
            $staff_number = ClearanceStaff::where('approval_status', 'vc')->latest('id')->count();
            //$ethical_number = Ethical::where('approval_status', 'vc')->latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number;
        }

        if (Auth::user()->hasRole("admin")) {
            $students_number = ClearanceStudent::latest('id')->count();
            $staff_number = ClearanceStaff::latest('id')->count();
            //$ethical_number = Ethical::latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number;
            $this->data['supervise_notifications'] = $students_number;
        }

        return view('clearance.edit_staff_clearance_admin')->with('data', $this->data);
    }


    //update staff clearance by admin
    public function update_staff_clearance_admin(Request $request, $id): RedirectResponse
    {
        $re = $request->all();

        if (!empty($request->file('contract_attachment'))) {
            $this->validate($request, ['contract_attachment' => 'required|mimes:pdf']);
        }

        $pro_files = $request->file('contract_attachment');
        if ($pro_files) {
            $destinationPath = 'updloads/'; // upload path
            $pro_filename = date('YmdHis') . "pro" . "." . $pro_files->getClientOriginalExtension();
            $pro_files->move($destinationPath, $pro_filename);
        }

        //clearance information
        $clearance = ClearanceStaff::find($id);

        $clearance->college_id = $request->get('college_id');
        $clearance->department_id = $request->get('department_id');
        $clearance->email = $request->get('email');
        $clearance->phone = $request->get('phone');
        $clearance->research_title = $request->get('research_title');
        $clearance->research_type = $request->get('research_type');
        $clearance->start_date = $request->get('start_date');
        $clearance->end_date = $request->get('end_date');
        $clearance->fund_amount = floatval(str_replace(",", "", $request->get('fund_amount')));

        $clearance->fund_source = $request->get('fund_source');
        $clearance->other_researcher = $request->get('other_researcher');

        $clearance->save();

        //store sites details
        ResearchSite::where('clearance_staff_id', $id)->delete();

        $temp = count($request->get('site_count'));
        for ($i = 0; $i < $temp; $i++) {

            $region = Region::find($request->get('region')[$i]);
            $district = $request->get('district')[$i];

            if (!empty($request->get('ward'))) {
                $ward = $request->get('ward')[$i];
            }

            if (!empty($request->get('village'))) {
                $village = $request->get('village')[$i];
            }

            $site = new ResearchSite();
            $site->clearance_staff_id = $clearance->id;
            $site->organization = $request->get('organization')[$i];
            $site->title = $request->get('title')[$i];
            $site->address = $request->get('address')[$i];
            $site->region = $region['name'];
            $site->district = $district;

            if (!empty($request->get('ward'))) {
                $site->ward = $ward;
            }
            if (!empty($request->get('village'))) {
                $site->village = $village;
            }
            $site->save();
        }
        return redirect()->route('clearance')->with('success', 'You have successfully
        updated the clearance');
    }


    public function clear_staff_clearance_researchers($id)
    {
        $clearance = ClearanceStaff::find($id);
        $clearance->other_researcher = null;
        $clearance->save();
        $this->data['staff'] = $clearance;

        $this->data['user'] = User::where('id', Auth::user()->id)->first();
        $this->data['college'] = College::where('id', $this->data['user']['college_id'])->first();
        $this->data['dept'] = Department::where('id', $this->data['user']['department_id'])->first();
        $this->data['regions'] = Region::all();

        $this->data['sites'] = ResearchSite::where('clearance_staff_id', $id)->get();

        foreach ($this->data['sites'] as $key => $value) {
            $this->data['sites'][$key]->selected_region = Region::where('name', $value->region)->first();
        }

        if (Auth::user()->hasRole("staff")) {
            $this->data['staffs'] = ClearanceStaff::where('created_by', Auth::user()->id)->latest('id')->get();
            $this->data['students'] = ClearanceStudent::where('supervisor', Auth::user()->id)->oldest('id')->get();

            //notify
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();

            $this->data['notifications'] = $supervise_students_number;
        }
        return view('clearance.edit_staff_clearance')->with('data', $this->data);
    }


    //add staff single site
    public function add_staff_site($id)
    {
        $this->data['title'] = 'add staff research site';
        $staff_clearance = ClearanceStaff::find($id);

        $this->data['regions'] = Region::all();

        if (Auth::user()->hasRole("staff")) {

            //notify
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();

            $this->data['notifications'] = $supervise_students_number;
        }

        if (Auth::user()->hasRole("admin")) {

            //notify
            $ethical_number = Ethical::latest('id')->count();
            $students_number = ClearanceStudent::latest('id')->count();
            $supervise_students_number = ClearanceStudent::where('supervisor', Auth::user()->id)
                ->where('approval_status', 'supervisor')
                ->oldest('id')->count();
            $staff_number = ClearanceStaff::latest('id')->count();
            $this->data['notifications'] = $students_number + $staff_number + $ethical_number;
            $this->data['supervise_notifications'] = $students_number + $staff_number + $ethical_number;
        }

        return view('clearance.add_staff_clearance_site', [
            'staff_clearance' => $staff_clearance,
        ])->with('data', $this->data);
    }


    //store added staff research site
    function store_staff_added_site(Request $request, $id): RedirectResponse
    {
        $clearance = ClearanceStaff::find($id);

        //validate the added site
        $this->validate($request, [
            'organization' => 'required', //this does not work ???
            'title' => 'required', //this does not work ???
            'address' => 'required',
            'region' => 'required', //this does not work ???
            'district' => 'required', //this does not work ???
        ]);

        $region = Region::find($request->get('region'));

        $site = new ResearchSite();
        $site->organization = $request->get('organization')[0];
        $site->title = $request->get('title')[0];
        $site->address = $request->get('address');
        $site->region = $region['name'];
        $site->district = $request->get('district')[0];
        $site->ward = $request->get('ward')[0];
        $site->village = $request->get('village')[0];
        $site->clearance_staff_id = $clearance->id;
        $site->created_at = date('Y-m-d H:i:s');
        $site->updated_at = date('Y-m-d H:i:s');
        $site->save();

        return redirect()->route('clearance')->with('success', 'You have successfully
    updated your clearance and submitted to HoD');
    }


    //staff clearance recommendations panel
    public function recommendation_panel_staff($id)
    {
        $this->data['title'] = 'Add recommendations';
        $this->data['staff'] = ClearanceStaff::find($id);

        return view('clearance.recommendation_panel_staff')->with('data', $this->data);
    }

    // save staff clearance forward recommendations
    public function save_recommendation_staff(Request $request, $id)
    {
        $reco = new ClearanceRecommendation();
        $clearance = ClearanceStaff::find($id);

        if ($clearance->level == "1") {
            $clearance->level = "2";
            $clearance->approval_status = "principal";

            $reco->clearance_staff_id = $id;
            $reco->from = "hod";
            $reco->comment = $request->get('recommendation');
        } elseif ($clearance->level == "2") {
            $clearance->level = "3";
            $clearance->approval_status = "drp";

            $reco->clearance_staff_id = $id;
            $reco->from = "principal";
            $reco->comment = $request->get('recommendation');
        } elseif ($clearance->level == "3") {
            $clearance->level = "4";
            $clearance->approval_status = "dvc";

            $reco->clearance_staff_id = $id;
            $reco->from = "drp";
            $reco->comment = $request->get('recommendation');
        } elseif ($clearance->level == "4") {
            $clearance->level = "5";
            $clearance->approval_status = "vc";

            $reco->clearance_staff_id = $id;
            $reco->from = "dvc";
            $reco->comment = $request->get('recommendation');
        }
        $clearance->save();
        $reco->save();

        //extract the user by role as recipients of notification email
        $roleVC = Role::where('name', 'vc')->first();
        $roleDVC = Role::where('name', 'dvc')->first();
        $roleDRP = Role::where('name', 'drp')->first();
        $rolePrincipal = Role::where('name', 'principal')->first();
        $roleHoD = Role::where('name', 'hod')->first();

        $user_vc = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleVC->id)
            ->select('users.id')
            ->first();

        $user_dvc = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleDVC->id)
            ->select('users.id')
            ->first();

        $user_drp = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleDRP->id)
            ->select('users.id')
            ->first();

        $user_principal = DB::table('users')
            ->join('clearance_students', 'users.college_id', '=', 'clearance_students.college_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $rolePrincipal->id)
            ->where('clearance_students.college_id', '=', $clearance->college_id)
            ->select('users.id')
            ->first();

        $user_hod = DB::table('users')
            ->join('clearance_students', 'users.department_id', '=', 'clearance_students.department_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleHoD->id)
            ->where('clearance_students.department_id', '=', $clearance->department_id)
            ->select('users.id')
            ->first();

        //construct forward notification message
        $message = "The clearance request from <b><i> $clearance->names</i></b> with research title <b>$clearance->research_title</b>, has been forwarded to you for recommendation.";
        $message .= "<br/> Please log in the system by  <a href=" . url('/clearance/process') . ">click here</a> and act on the request.";
        $message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

        //send notification to respective person
        if ($clearance->level == "5") {
            $this->send_notification($user_vc->id, $message);
        } else if ($clearance->level == "4") {
            $this->send_notification($user_dvc->id, $message);
        } else if ($clearance->level == "3") {
            $this->send_notification($user_drp->id, $message);
        } else if ($clearance->level == "2") {
            $this->send_notification($user_principal->id, $message);
        } else {
            $this->send_notification($user_hod->id, $message);
        }

        return redirect()->route('clearance');
    }

    //get staff reverse recomendations panel 
    function back_panel($id)
    {
        $this->data['title'] = 'Add recommendations';
        $this->data['staff'] = ClearanceStaff::find($id);

        return view('clearance.return_recommendation_panel_staff')->with('data', $this->data);
    }

    //get store reverse recomendation and change status for staff
    public function save_return_recomendation(Request $request, $id): RedirectResponse
    {
        $reco = new ClearanceRecommendation();
        $clearance = ClearanceStaff::find($id);

        if ($clearance->level == "1") {
            $clearance->level = "0";
            $clearance->approval_status = "rejected";

            $reco->clearance_staff_id = $id;
            $reco->from = "hod";
            $reco->comment = $request->get('recommendation');
        } elseif ($clearance->level == "2") {
            $clearance->level = "1";
            $clearance->approval_status = "hod";

            $reco->clearance_staff_id = $id;
            $reco->from = "principal";
            $reco->comment = $request->get('recommendation');
        } elseif ($clearance->level == "3") {
            $clearance->level = "2";
            $clearance->approval_status = "principal";

            $reco->clearance_staff_id = $id;
            $reco->from = "drp";
            $reco->comment = $request->get('recommendation');
        } elseif ($clearance->level == "4") {
            $clearance->level = "3";
            $clearance->approval_status = "drp";

            $reco->clearance_staff_id = $id;
            $reco->from = "dvc";
            $reco->comment = $request->get('recommendation');
        }
        $clearance->save();
        $reco->save();

        //extract the user by role as recipients of notification email
        $roleVC = Role::where('name', 'vc')->first();
        $roleDVC = Role::where('name', 'dvc')->first();
        $roleDRP = Role::where('name', 'drp')->first();
        $rolePrincipal = Role::where('name', 'principal')->first();
        $roleHoD = Role::where('name', 'hod')->first();

        $user_vc = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleVC->id)
            ->select('users.id')
            ->first();

        $user_dvc = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleDVC->id)
            ->select('users.id')
            ->first();

        $user_drp = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleDRP->id)
            ->select('users.id')
            ->first();

        $user_principal = DB::table('users')
            ->join('clearance_students', 'users.college_id', '=', 'clearance_students.college_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $rolePrincipal->id)
            ->where('clearance_students.college_id', '=', $clearance->college_id)
            ->select('users.id')
            ->first();

        $user_hod = DB::table('users')
            ->join('clearance_students', 'users.department_id', '=', 'clearance_students.department_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleHoD->id)
            ->where('clearance_students.department_id', '=', $clearance->department_id)
            ->select('users.id')
            ->first();

        //construct reversed notification message
        $message = "The clearance request from <b><i> $clearance->names</i></b> with research title <b>$clearance->research_title</b>, has been reversed.";
        $message .= "<br/> Please log in the system by  <a href=" . url('/clearance/process') . ">click here</a> and act on the recommendation.";
        $message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

        //send notification to respective person
        if ($clearance->level == "4") {
            $this->send_notification($user_dvc->id, $message);
        } else if ($clearance->level == "3") {
            $this->send_notification($user_drp->id, $message);
        } else if ($clearance->level == "2") {
            $this->send_notification($user_principal->id, $message);
        } else if ($clearance->level == "1") {
            $this->send_notification($user_hod->id, $message);
        } else {
            $this->send_notification($clearance->created_by, $message);
        }

        return redirect()->route('clearance');
    }

    // save vc approval status
    public function approve_staff_clearance(Request $request, $id): RedirectResponse
    {
        // Perms::has_allowed('clearance_staffs', 'approve_staff_clearance');
        $approved_date = date('Y-m-d');
        $clearance = ClearanceStaff::find($id);
        $clearance->approval_status = $request->get('approve_status');
        $clearance->approved_date = $approved_date;
        $clearance->save();
        $status = $request->get('approve_status');

        if ($status === 'rejected') {
            return redirect()->route('clearance');
        }

        if ($status === 'approved') {
            return redirect()->route('staff_clearance_letter', ['id' => $id]);
        }
        return redirect()->route('staff_clearance_letter', ['id' => $id]);
    }

    // staff clearance letters
    public function staff_clearance_letter($id)
    {
        $cl = ClearanceStaff::find($id);
        $this->data['clearance'] = $cl;
        $user = User::find($cl->created_by);
        $this->data['user'] = $user;

        $sites = ResearchSite::where('clearance_staff_id', $id)->get();
        $this->data['sites'] = $sites;
        foreach ($this->data['sites'] as $key => $value) {
            $this->data['sites'][$key]->assistants = ResearcherAssistant::where('site_id', $value->id)->get();
            $this->data['sites'][$key]->researchers = AssignedResearcher::where('site_id', $value->id)->get();
        }

        return view('clearance.staff_letter')->with('data', $this->data);
    }

    // RAS
    public function ras_letter($id)
    {

        $site = ResearchSite::where('id', $id)->first();

        $this->data['site'] = $site;
        $cl = ClearanceStaff::find($site->clearance_staff_id);
        $this->data['clearance'] = $cl;
        $user = User::find($cl->created_by);
        $this->data['user'] = $user;


        $this->data['vc'] = User::whereHas('roles', function ($q) {
            $q->where('name', 'vc');
        })->first();

        $this->data['assistants'] = ResearcherAssistant::where('site_id', $site->id)->get();
        $this->data['researchers'] = AssignedResearcher::where('site_id', $site->id)->get();

        return view('clearance.ras_letter')->with('data', $this->data);
    }

    // das
    public function das_letter($id)
    {
        $site = ResearchSite::where('id', $id)->first();

        $this->data['site'] = $site;
        $cl = ClearanceStaff::find($site->clearance_staff_id);
        $this->data['clearance'] = $cl;
        $user = User::find($cl->created_by);
        $this->data['user'] = $user;

        $this->data['vc'] = User::whereHas('roles', function ($q) {
            $q->where('name', 'vc');
        })->first();

        $this->data['assistants'] = ResearcherAssistant::where('site_id', $site->id)->get();
        $this->data['researchers'] = AssignedResearcher::where('site_id', $site->id)->get();

        return view('clearance.das_letter')->with('data', $this->data);
    }

    // organization
    public function org_letter($id)
    {
        $site = ResearchSite::where('id', $id)->first();

        $this->data['site'] = $site;
        $cl = ClearanceStaff::find($site->clearance_staff_id);
        $this->data['clearance'] = $cl;
        $user = User::find($cl->created_by);
        $this->data['user'] = $user;

        $this->data['vc'] = User::whereHas('roles', function ($q) {
            $q->where('name', 'vc');
        })->first();

        $this->data['assistants'] = ResearcherAssistant::where('site_id', $site->id)->get();
        $this->data['researchers'] = AssignedResearcher::where('site_id', $site->id)->get();

        return view('clearance.organization_letter')->with('data', $this->data);
    }

    // twmc
    public function twmc_letter($id)
    {
        $site = ResearchSite::where('id', $id)->first();

        $this->data['site'] = $site;
        $cl = ClearanceStaff::find($site->clearance_staff_id);
        $this->data['clearance'] = $cl;
        $user = User::find($cl->created_by);
        $this->data['user'] = $user;

        $this->data['vc'] = User::whereHas('roles', function ($q) {
            $q->where('name', 'vc');
        })->first();

        $this->data['assistants'] = ResearcherAssistant::where('site_id', $site->id)->get();
        $this->data['researchers'] = AssignedResearcher::where('site_id', $site->id)->get();

        return view('clearance.twmc_letter')->with('data', $this->data);
    }


    //send notification to user
    public function send_notification($user_id, $message): void
    {
        $user = User::find($user_id);

        //construct email for sending
        $message_id = Notification::generate_message_id();

        //user
        $name = $user->first_name . ' ' . $user->surname;
        $email = $user->email;
        $phone = $user->phone;

        //insert notification
        $notification = new Notification(
            [
                'message' => $message,
                'message_id' => $message_id,
                'user_id' => $user->id,
                'email' => $email,
                'phone' => Notification::cast_mobile($phone),
                'app_status' => 'PENDING',
                'sms_status' => 'PENDING',
                'email_status' => 'PENDING',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => Auth::user()->id
            ]
        );
        $notification->save();

        //create array
        $array = ['name' => $name, 'email' => $email, 'message' => $message];

        if (Notification::send_email($array)) {
            $data = ['email_status' => 'SENT'];
        }
        else {
            $data = ['email_status' => 'REJECTED'];
        }

        Notification::where(['id' => $notification->id])->update($data);
    }
}
