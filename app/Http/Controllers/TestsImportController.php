<?php

namespace App\Http\Controllers;

use App\Imports\TestsImport;
use App\Models\Test;
use App\Models\TestRecommendation;
use Illuminate\Http\Request;
//use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Facades\Excel;
//use Excel;

class TestsImportController extends Controller
{
    public function list()
    {
        $data['test'] = Test::all();
        return view('tests.list', ['data'=>$data['test']]);
    }


    public function show()
    {
        $data['status'] = TestRecommendation::all();
        return view('tests.import', ['data'=>$data['status']]);
    }


    public function store(Request $request)
    {
        $request->validate([
           'excel_file' => 'required|mimes:xls,xlsx'
        ]);
        $file = $request->file('excel_file')->store('imports');

        Excel::import(new TestsImport, $file);

        //update the Tests records with status
        $tests = Test::all(); //identify the recently uploaded record
        foreach ($tests as $test){
            if (empty($test->status)){
                $test_rec_this = TestRecommendation::find($test->id);
                $test->status = $test_rec_this->t_status;
                $test->save();
            }
        }
        return back()->with('success', 'Excel file imported successfully and test status updated');
    }
}
