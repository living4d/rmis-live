<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Calls\Call;

class CronJobController extends Controller
{
    //

    public function close()
    {
        //select all call where today is greater than deadline and status = active
        $calls = Call::whereDate('deadline', ' < ', date('Y-m-d'))->get();

        if ($calls) {
            foreach ($calls as $v) {
                $call = Call::find($v->id);
                $call->status = 'closed';
                $call->save();
            }
        }
        echo 'closed';
    }
}
