<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use DB;
use Auth;
use App\User;
use App\Perms;
use App\Models\Notification;
use App\Models\Researcher;
use App\Models\Settings\College;
use App\Models\Settings\Department;
use App\Models\Student;

class NotificationController extends Controller
{
    private $data;

    //constructor
    public function __construct()
    {
        $this->middleware('auth');
    }

    //lists
    function index()
    {
        Perms::has_allowed('notification', 'lists');
        $this->data['title'] = 'Notification';

        //notification
        $notification = Notification::where(['user_id' => Perms::get_current_user_id()])->orderBy('created_at', 'DESC')->get();
        $this->data['notification'] = $notification;

        foreach ($notification as $k => $v) {
            $this->data['notification'][$k]->user = User::query()->find($v->created_by);
        }

        return view('notification.lists', $this->data);
    }

    //outbox
    function outbox()
    {
        Perms::has_allowed('notification', 'send');
        $this->data['title'] = 'Outbox';

        //notification
        $notification = Notification::where(['created_by' => Perms::get_current_user_id()])->orderBy('created_at', 'DESC')->get();
        $this->data['notification'] = $notification;

        foreach ($notification as $k => $v) {
            $this->data['notification'][$k]->user = User::query()->find($v->user_id);
        }

        return view('notification.outbox', $this->data);
    }

    //send notification
    function send()
    {
        Perms::has_allowed('notification', 'send');
        $this->data['title'] = 'Send Notification';

        //colleges
        $colleges = College::all();

        $this->data['colleges'] = $colleges;

        foreach ($this->data['colleges'] as $k => $v) {
            $departments = Department::query()->where('college_id', $v->id)->get();
            $this->data['colleges'][$k]->departments = $departments;
        }

        return view('notification.send', $this->data);
    }

    //store messages
    function store_message(Request $request)
    {
        //validation
        $this->validate(
            $request,
            [
                'college_ids' => 'required',
                'message' => 'required'
            ],
            [
                'college_ids.required' => 'College is required',
                'message.required' => 'Message required'
            ]
        );

        //post data
        $college_ids = $request->input('college_ids');
        $department_ids = $request->input('department_ids');
        $message = $request->input('message');

        $array = ['college_ids' => $college_ids, 'department_ids' => $department_ids];

        //deal with notification
        if ($recipients = Notification::get_recipients('notify', $array))
            Notification::insert_notifications($recipients, $message);

        return redirect('/notification')->with('success', 'Notification sent');
    }

    //change in app notification to delivered
    function change_status($id)
    {
        Perms::has_allowed('notification', 'change_status');
        $this->data['title'] = 'Change status';

        //notification
        $notification = Notification::query()->find($id);

        if (!$notification)
            abort(404);

        //create object to store data
        $notification->app_status = 'DELIVERED';
        $notification->save();

        if ($notification->save())
            return redirect('/notification')->with('success', 'Notification delivered');
        else
            return redirect('/notification')->with('danger', 'Failed to update notification');
    }

    //send email notification
    function send_email()
    {
        $limit = 10; //limit

        //pending email
        $count_pending_email = Notification::where(['email_status' => 'PENDING'])->count();
        $looping = $count_pending_email / $limit;

        for ($i = 1; $i <= ceil($looping); $i++) {
            $notification = Notification::limit($limit)->where(['email_status' => 'PENDING'])->get();

            if ($notification) {
                foreach ($notification as $v) {
                    $name = '';
                    if ($v->user == 'STAFF' || $v->user == 'INTERNAL') {
                        $user = User::find($v->user_id);
                        $name = $user->first_name . ' ' . $user->surname;
                    } else if ($v->user == 'STUDENT') {
                        $student = Student::find($v->user_id);
                        $name = $student->first_name . ' ' . $student->surname;
                    } else if ($v->user == 'EXTERNAL') {
                        $researcher = Researcher::find($v->user_id);
                        $name = $researcher->full_name;
                    }

                    //create array
                    $array = ['name' => $name, 'email' => $v->email, 'message' => $v->message];

                    if (Notification::send_email($array))
                        $data = ['email_status' => 'SENT'];
                    else
                        $data = ['email_status' => 'REJECTED'];

                    Notification::where(['id' => $v->id])->update($data); //update data
                }
            }
        }
    }
}
