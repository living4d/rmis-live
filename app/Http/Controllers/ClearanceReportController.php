<?php

namespace App\Http\Controllers;

use App\Models\Clearances\ClearanceStaff;
use App\Models\Clearances\ClearanceStudent;
use Illuminate\Http\Request;

use Auth;
use DB;

class ClearanceReportController extends Controller
{
  private $data;

  //generate clearance permit summary reports
  function clearance_report(Request $request)
  {
    $this->data['title'] = 'Clearance report';


    //post submit
    if (isset($_POST['submit'])) {
      //validation
      $this->validate(
        $request,
        [
          'report_type' => 'required',
          'date_from' => 'required_if:report_type,==,periodic',
          'date_to' => 'required_if:report_type,==,periodic',
        ]
      );

      //report type
      $report_type = $request->input('report_type');
      $clearance_type = $request->input('clearance_type');

      if ($report_type === 'combine' && $clearance_type === 'staff') {
        //reports
        $permit_summary = DB::table('clearance_staffs')
          ->select('colleges.name as College', DB::raw('count(clearance_staffs.id) as Clearance'))
          ->groupBy('colleges.name')
          ->join('colleges', 'clearance_staffs.college_id', '=', 'colleges.id')
          ->where(['clearance_staffs.approval_status' => 'approved'])
          ->get();

        $this->data['date_from'] = null;
        $this->data['date_to'] = null;
      } else if ($report_type === 'combine' && $clearance_type === 'student') {
        //reports
        $permit_summary = DB::table('clearance_students')
          ->select('colleges.name as College', DB::raw('count(clearance_students.id) as Clearance'))
          ->groupBy('colleges.name')
          ->join('colleges', 'clearance_students.college_id', '=', 'colleges.id')
          ->where(['clearance_students.approval_status' => 'approved'])
          ->get();

        $this->data['date_from'] = null;
        $this->data['date_to'] = null;
      } else if ($report_type == 'periodic' && $clearance_type == 'staff') {
        $from_date = $request->input('date_from');
        $to_date = $request->input('date_to');

        //reports
        $permit_summary = DB::table('clearance_staffs')
          ->select('colleges.name as College', DB::raw('count(clearance_staffs.id) as Clearance'))
          ->groupBy('colleges.name')
          ->join('colleges', 'clearance_staffs.college_id', '=', 'colleges.id')
          ->where(['clearance_staffs.approval_status' => 'approved'])
          ->whereBetween('clearance_staffs.approved_date', [$from_date, $to_date])
          ->get();

        $this->data['date_from'] = $from_date;
        $this->data['date_to'] = $to_date;
      } else if ($report_type === 'periodic' && $clearance_type === 'student') {
        $from_date = $request->input('date_from');
        $to_date = $request->input('date_to');

        //reports
        $permit_summary = DB::table('clearance_students')
          ->select('colleges.name as College', DB::raw('count(clearance_students.id) as Clearance'))
          ->groupBy('colleges.name')
          ->join('colleges', 'clearance_students.college_id', '=', 'colleges.id')
          ->where(['clearance_students.approval_status' => 'approved'])
          ->whereBetween('clearance_students.approved_date', [$from_date, $to_date])
          ->get();

        $this->data['date_from'] = $from_date;
        $this->data['date_to'] = $to_date;
      }

      //reports
      $this->data['report_type'] = $report_type;
      $this->data['permit_summary'] = $permit_summary;
    } else {
      $this->data['date_from'] = null;
      $this->data['date_to'] = null;
      $this->data['report_type'] = null;
      $this->data['permit_summary'] = [];
    }

    //notification
    if (Auth::user()->hasRole("admin")) {

      $students_number = ClearanceStudent::latest('id')->count();
      $staff_number = ClearanceStaff::latest('id')->count();
      $this->data['notifications'] = $students_number + $staff_number;
      $this->data['supervise_notifications'] = 0;
    }
    //render view
    return view('clearance.reports.periodic_permits', ['data' => $this->data]);
  }
}
