<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

use App\Models\Projects\Project;

use Auth;

use App\Models\Calls\CallApplication;
use App\Models\Calls\CallApplicationObjective;
use App\Models\Projects\ProjectActivityReport;
use App\Models\Projects\ProjectFundRequest;
use DB;
use Redirect;

class ProjectsController extends Controller
{
    private $data;

    //constructor
    public function __construct()
    {
        $this->middleware('auth');
    }

    //stats
    public function stats()
    {
        $this->data['title'] = 'Projects Summary';
        //Perms::has_allowed('calls', 'lists');

        //active project
        $active_projects = CallApplication::join('projects', 'call_applications.id', '=', 'projects.application_id')
            ->where('projects.status', '=', 1)->count();
        $this->data['active_projects'] = $active_projects;

        //total funding

        //render view
        return view('projects.stats', $this->data);
    }

    //Project lists
    public function lists()
    {
        $title = 'Projects';
        //Perms::has_allowed('calls', 'lists');

        //projects
        $projects = Project::all();

        //render view
        return view('projects.lists', compact('title', 'projects'));
    }



    /*==========================================================
    MY PROJECT => SPECIFIC FOR STAFF TO VIEW SPECIFIC PROJECT
    ==========================================================*/
    //my projects
    public function index()
    {
        $this->data['title'] = 'My Projects';

        //projects
        $projects = Project::where('created_by', Auth::user()->id)->get();
        $this->data['projects'] = $projects;

        //render view
        return view('projects.index', $this->data);
    }

    //details
    public function show($id)
    {
        //Perms::has_allowed('projects', 'show');
        $this->data['title'] = 'Project details';

        //project
        $project = Project::findOrFail($id);
        $this->data['project'] = $project;

        //objectives
        $objectives = CallApplicationObjective::where(['application_id' => $project->application_id])->get();
        $this->data['objectives'] = $objectives;

        //render view
        return view('projects.show', $this->data);
    }

    //objective activities
    public function activities($id)
    {
        //Perms::has_allowed('projects', 'show');
        $title = 'Activities';

        //project
        $project = Project::findOrFail($id);

        //render view
        return view('projects.activities', compact('title', 'project'));
    }

    //fund requests
    public function fund_requests($id)
    {
        //Perms::has_allowed('projects', 'show');
        $this->data['title'] = 'Project Fund Requests';

        //project
        $project = Project::findOrFail($id);
        $this->data['project'] = $project;

        //render view
        return view('projects.fund_requests', $this->data);
    }

    //listing status
    public function progress_reports($id)
    {
        $this->data['title'] = 'Project Progress Reports';
        //Perms::has_allowed('calls', 'delete');

        //project
        $project = Project::findOrFail($id);
        $this->data['project'] = $project;

        //render view
        return view('projects.progress_reports', $this->data);
    }

    //generate report
    public function generate_report(Request $request, $id)
    {
        $this->data['title'] = 'Generate Report';
        //Perms::has_allowed('calls', 'delete');

        //project
        $project = Project::findOrFail($id);
        $this->data['project'] = $project;

        //post submit
        if (isset($_POST['submit'])) {
            //validation
            $this->validate(
                $request,
                [
                    'report_type' => 'required',
                    'start_at' => 'required_if:report_type,==,date-range',
                    'end_at' => 'required_if:report_type,==,date-range',
                ]
            );

            //report type
            $report_type = $request->input('report_type');

            if ($report_type === 'complete-report') {
                //reports
                $objectives = CallApplicationObjective::select('call_application_objectives.id', 'call_application_objectives.objective')
                    ->join('project_activities', 'call_application_objectives.id', '=', 'project_activities.objective_id')
                    ->join('project_activity_reports', 'project_activities.id', '=', 'project_activity_reports.activity_id')
                    ->join('project_fund_requests', 'project_activities.project_id', '=', 'project_fund_requests.project_id')
                    ->where(['project_activities.project_id' => $project->id, 'project_fund_requests.status' => 'approved'])
                    ->groupBy('call_application_objectives.id')
                    ->orderBy('call_application_objectives.id')
                    ->get();


                $this->data['start_at'] = null;
                $this->data['end_at'] = null;
            } else if ($report_type === 'date-range') {
                $start_at = $request->input('start_at');
                $end_at = $request->input('end_at');

                //reports
                $objectives = CallApplicationObjective::select('call_application_objectives.id', 'call_application_objectives.objective')
                    ->join('project_activities', 'call_application_objectives.id', '=', 'project_activities.objective_id')
                    ->join('project_activity_reports', 'project_activities.id', '=', 'project_activity_reports.activity_id')
                    ->join('project_fund_requests', 'project_activities.project_id', '=', 'project_fund_requests.project_id')
                    ->where(['project_activities.project_id' => $project->id, 'project_fund_requests.status' => 'approved'])
                    ->whereBetween('project_activity_reports.end_at', [$start_at, $end_at])
                    ->groupBy('call_application_objectives.id')
                    ->orderBy('call_application_objectives.id')
                    ->get();

                $this->data['start_at'] = $start_at;
                $this->data['end_at'] = $end_at;
            }

            //total amount spent
            $requests = ProjectFundRequest::select(DB::raw('SUM(amount) as total_amount'))
            ->where(['status' => 'approved'])
            ->get();

            $total_amount_spent = $requests[0]->total_amount;

            //reports
            $this->data['report_type'] = $report_type;
            $this->data['objectives'] = $objectives;
            $this->data['total_amount_spent'] = $total_amount_spent;
        } else {
            $this->data['start_at'] = null;
            $this->data['end_at'] = null;
            $this->data['report_type'] = null;
            $this->data['total_amount_spent'] = 0;
        }
        //render view
        return view('projects.generate_report', $this->data);
    }

    //change status
    function change_status($id)
    {
        $title = 'Change Project Status';
        //Perms::has_allowed('calls', 'delete');

        //project
        $project = Project::findOrFail($id);

        //render view
        return view('projects.change_status', compact('title', 'project'));
    }

    //store request for fund
    public function store_status(Request $request, $id): RedirectResponse
    {
        //project
        $project = Project::findOrFail($id);

        //validation
        $this->validate(
            $request,
            [
                'status' => 'required',
                'attachment' => 'required',
            ],
            [
                'status.required' => 'Status required',
                'attachment.required' => 'Final project report required',
            ]
        );

        //check if project has pending activities
        if ($project->activities) {
            $count = 0;
            foreach ($project->activities as $val) {
                $reports = ProjectActivityReport::where(['activity_id' => $val->id, 'status' => 'on-progress'])->count();

                $count += $reports;
            }
            if ($count > 0) {
                return Redirect::back()->with('danger', 'Update progress for all activities before closing the project');
            }
        } else {
            return Redirect::back()->with('danger', 'Project has no any activity');
        }


        //update project
        $project->status = $request->input('status');
        $project->description = $request->input('description');
        $project->updated_by = Auth::user()->id;

        // Check if an attachment has been uploaded
        if ($request->has('attachment')) {
            // Get image file
            $attachment = $request->file('attachment');

            // Make a image name
            $name = time() . '_' . $attachment->getClientOriginalName() . '.' . $attachment->getClientOriginalExtension();

            //Move Uploaded File
            $attachment->move('assets/uploads/documents/', $name);

            // Set attachment name
            $project->attachment = $name;
        }
        //save            
        $project->save();

        //redirect
        return Redirect::route('projects.index')->with('success', 'Project status changed');
    }
}
