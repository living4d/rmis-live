<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller;
use App\Models\Projects\Project;
use App\Models\Projects\ProjectActivity;
use App\Models\Projects\ProjectActivityReport;
use App\Models\Projects\ProjectActivityReportAttachment;
use App\Models\Projects\ProjectActivityStatus;
use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Redirect;

class ActivityReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $id
     * @return Response
     */
    public function lists($id): Response
    {
        $title = 'Activity Progress Status';
        //Perms::has_allowed('calls', 'delete');

        $project = Project::findOrFail($id);

        //render view
        return view('projects.reports.lists', compact('title', 'project'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(): void
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request): void
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return void
     */
    public function show($id): void
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function edit($id): void
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return void
     */
    public function update(Request $request, $id): void
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return void
     */
    public function destroy($id): void
    {
        //
    }

    //change status
    public function change_status($id)
    {
        $this->data['title'] = 'Activity Progress';
        //Perms::has_allowed('calls', 'delete');

        //activity
        $activity = ProjectActivity::findOrFail($id);
        $this->data['activity'] = $activity;

        //project
        $project = Project::findOrFail($activity->project_id);
        $this->data['project'] = $project;

        //render view
        return view('projects.reports.change_status', $this->data);
    }

    //store request for fund
    public function store_status(Request $request, $id): RedirectResponse
    {
        //activity
        $activity = ProjectActivity::findOrFail($id);

        //validation
        $this->validate(
            $request,
            [
                'status' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'output_produced' => 'required',
                'achieved_outcome' => 'required',
            ],
            [
                'status.required' => 'Status required',
                'start_date' => 'Start date is required',
                'end_date' => 'End date is required',
                'output_produced' => 'Output produced is required',
                'achieved_outcome' => 'Achieved outcome is required',
            ]
        );

        //new activity report
        $activity_report = ProjectActivityReport::firstOrNew(
            ['activity_id' => $activity->id]
        );
        $activity_report->status = $request->input('status');
        $activity_report->start_at = $request->input('start_date');
        $activity_report->end_at = $request->input('end_date');
        $activity_report->output_produced = $request->input('output_produced');
        $activity_report->achieved_outcome = $request->input('achieved_outcome');
        $activity_report->additional_information = $request->input('additional_information');
        $activity_report->created_by = Auth::user()->id;
        $activity_report->save();

        //todo: split output produced

        //todo split outcome achieved

        //upload attachments
        if ($request->has('attachments')) {
            //process for insertion
            for ($i = 0; $i < sizeof($request->file('attachments')); $i++) {
                $attachment = $request->file('attachments')[$i];

                // Make a image name
                $attachment_name = time() . '_' . $attachment->getClientOriginalName() . '.' . $attachment->getClientOriginalExtension();

                //Move Uploaded File
                $attachment->move('assets/uploads/documents/', $attachment_name);

                //save file
                $create_attachment = ProjectActivityReportAttachment::firstOrNew(
                    [
                        'activity_report_id' => $activity_report->id,
                        'filename' => $attachment->getClientOriginalName(),
                    ]
                );
                $create_attachment->attachment = $attachment_name;
                $create_attachment->save();
            }
        }

        //update project status
        $activity = ProjectActivity::findOrFail($id);
        $activity->status = $request->input('status');
        $activity->save();

        //redirect
        return Redirect::route('projects.progress-reports', $activity->project->id)->with('success', 'Progress report changed');
    }

    /*==========================================
    APIs
    ==========================================*/
    public function api_show($id): void
    {
        $activity = ProjectActivity::find($id);

        //activities
        $arr_activity = [
            'project_title' => $activity->project->application->project_title,
            'objective_title' => $activity->objective->objective,
            'title' => $activity->title,
            'start_date' => date('d-m-Y', strtotime($activity->start_date)),
            'end_date' => date('d-m-Y', strtotime($activity->end_date)),
            'expected_output' => $activity->expected_output,
            'expected_outcome' => $activity->expected_outcome,
            'performance_indicator' => $activity->performance_indicator,
            'baseline' => $activity->baseline,
            'outcome_targets' => $activity->outcome_targets,

            //progress
            'status' => $activity->status,
            'output_produced' => $activity->activityReport->output_produced ?? '',
            'achieved_outcome' => $activity->activityReport->achieved_outcome ?? '',
            'additional_information' => $activity->activityReport->additional_information ?? ''
        ];

        //response
        echo json_encode($arr_activity);
    }
}
