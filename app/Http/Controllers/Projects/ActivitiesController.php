<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Projects\Project;
use App\Models\Projects\ProjectActivity;
use App\Models\Calls\CallApplicationObjective;
use App\Models\Projects\ProjectActivityResponsible;
use App\Models\Projects\ProjectActivityStatus;
use App\Models\Projects\ProjectActivityType;
use Redirect;

class ActivitiesController extends Controller
{
    private $data;

    //constructor
    public function __construct()
    {
        $this->middleware('auth');
    }

    //objective activities
    public function lists($id)
    {
        //Perms::has_allowed('projects', 'show');
        $this->data['title'] = 'Activities';

        //project
        $project = Project::findOrFail($id);
        $this->data['project'] = $project;

        //render view
        return view('projects.activities.lists');
    }

    //selected
    public function selected()
    {
        $activity_ids = [];

        //assign evaluator
        if (isset($_POST['request-for-fund'])) {
            if (empty($_POST['id'])) {
                //redirect with message
                return Redirect::back()->with('danger', 'No any activity selected!');
            }

//looping post value
            $i = 1;
            foreach ($_POST['id'] as $activity_id) {
                $activity_ids[$i] = $activity_id;
                $i++;
            }

            //first unset session data
            session()->forget('activity_ids');

            //save data in session
            session(['activity_ids' => $activity_ids]);
            return Redirect::route('fund-requests.create-batch');
        }
    }

    //details
    public function show($id)
    {
        $this->data['title'] = 'Activity Information';
        //Perms::has_allowed('calls', 'delete');

        //activity
        $activity = ProjectActivity::findOrFail($id);
        $this->data['activity'] = $activity;

        //project
        $project = Project::findOrFail($activity->project_id);
        $this->data['project'] = $project;

        //objective
        $objective = CallApplicationObjective::findOrFail($activity->objective_id);
        $this->data['objective'] = $objective;

        //render view
        return view('projects.activities.details', $this->data);
    }

    //create activity for objective
    public function create($project_id)
    {
        $this->data['title'] = 'Register New Activity';

        //project
        $project = Project::findOrFail($project_id);
        $this->data['project'] = $project;

        //objectives
        $objectives = $project->application->objectives;
        $this->data['objectives'] = $objectives;

        //partners
        $partners = $project->application->partners;
        $this->data['partners'] = $partners;

        //activity types
        $activity_types = ProjectActivityType::all();
        $this->data['activity_types'] = $activity_types;

        //render view
        return view('projects.activities.create', $this->data);
    }

    //store activity for objective
    public function store(Request $request, $project_id)
    {
        $remain_fund = floatval(str_replace(",", "", $request->get('remained_fund')));
        $budget_amount = floatval(str_replace(",", "", $request->get('budget_amount')));

        $this->validate(
            $request,
            [
                'objective_id' => 'required',
                'title' => 'required',
                'activity_type_id' => 'required',
                'responsible' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'budget_amount' => 'required|min:0|max:' . $remain_fund,
                'expected_output' => 'required',
                'performance_indicator' => 'required',
                'outcome_targets' => 'required',
            ],
            [
                'objective_id.required' => 'Objective required',
                'title.required' => 'Activity required',
                'activity_type_id.required' => 'Activity type required',
                'responsible.required' => 'Responsible required',
                'start_date.required' => 'Start date required',
                'end_date.required' => 'End date required',
                'budget_amount.required' => 'Budget amount is required',
                'budget_amount.max' => 'Budget for activity should not exceed remained fund',
                'expected_output.required' => 'Expected outcome required',
                'performance_indicator.required' => 'Performance indicator required',
                'outcome_targets.required' => 'Targets required'
            ]
        );

        //register new project
        $activity = ProjectActivity::firstOrNew([
            'project_id' => $project_id,
            'objective_id' => $request->input('objective_id'),
            'activity_type_id' => $request->input('activity_type_id'),
            'title' => $request->input('title')
        ]);
        $activity->start_date = $request->input('start_date');
        $activity->end_date = $request->input('end_date');
        $activity->location = $request->input('location');
        $activity->budget = $budget_amount;
        $activity->remained_fund = $budget_amount;
        $activity->expected_output = $request->input('expected_output');
        $activity->performance_indicator = $request->input('performance_indicator');
        $activity->baseline = $request->input('baseline');
        $activity->outcome_targets = $request->input('outcome_targets');
        $activity->created_by = Auth::user()->id;
        $activity->save();

        //project responsible
        foreach ($_POST['responsible'] as $call_partner_id) {
            $new_update_partner = ProjectActivityResponsible::firstOrNew([
                'activity_id' => $activity->id,
                'call_partner_id' => $call_partner_id
            ]);
            $new_update_partner->save();
        }

        //update remain fund on objective
        $objective = CallApplicationObjective::findOrFail($request->input('objective_id'));
        $objective->remained_fund = $objective->remained_fund - $budget_amount;
        $objective->save();

        //update available fund of overall project
        $project = Project::findOrFail($project_id);
        $project->remained_fund = $project->remained_fund - $budget_amount;
        $project->save();

        //redirect
        if (isset($_POST['save'])) {
            return Redirect::route('projects.activities', $project->id)->with('success', 'New activity registered');
        }

        if (isset($_POST['continue'])) {
            return Redirect::route('activities.create-fund-request', $activity->id);
        }
    }

    //edit activity
    function edit($project_id, $id)
    {
        $this->data['title'] = 'Edit Activity';
        //Perms::has_allowed('calls', 'delete');

        //project
        $project = Project::findOrFail($project_id);
        $this->data['project'] = $project;

        //activity
        $activity = ProjectActivity::findOrFail($id);
        $this->data['activity'] = $activity;

        //objectives
        $objectives = $project->application->objectives;
        $this->data['objectives'] = $objectives;

        //partners
        $partners = $project->application->partners;
        $this->data['partners'] = $partners;

        //activity types
        $activity_types = ProjectActivityType::all();
        $this->data['activity_types'] = $activity_types;

        //render view
        return view('projects.activities.edit', $this->data);
    }

    //update activity
    public function update(Request $request, $project_id, $id)
    {
        $remain_fund = floatval(str_replace(",", "", $request->get('remained_fund')));
        $budget_amount = floatval(str_replace(",", "", $request->get('budget_amount')));

        $this->validate(
            $request,
            [
                'objective_id' => 'required',
                'title' => 'required',
                'activity_type_id' => 'required',
                'responsible' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'budget_amount' => 'required|min:0|max:' . $remain_fund,
                'expected_output' => 'required',
                'performance_indicator' => 'required',
                'outcome_targets' => 'required',
            ],
            [
                'objective_id.required' => 'Objective required',
                'title.required' => 'Activity required',
                'activity_type_id.required' => 'Activity type required',
                'responsible.required' => 'Responsible required',
                'start_date.required' => 'Start date required',
                'end_date.required' => 'End date required',
                'budget_amount.required' => 'Budget amount is required',
                'budget_amount.max' => 'Budget for activity should not exceed remained fund',
                'expected_output.required' => 'Expected outcome required',
                'performance_indicator.required' => 'Performance indicator required',
                'outcome_targets.required' => 'Targets required'
            ]
        );

        //activity
        $activity = ProjectActivity::findOrFail($id);

        //update available fund of overall project
        $project = Project::findOrFail($project_id);
        $project->remained_fund = $project->remained_fund - ($budget_amount - $activity->budget);
        $project->save();

        //update remain fund on objective
        $objective = CallApplicationObjective::findOrFail($request->input('objective_id'));
        $objective->remained_fund = $objective->remained_fund - ($budget_amount - $activity->budget);
        $objective->save();

        //update project activity
        $activity->objective_id = $request->input('objective_id');
        $activity->activity_type_id = $request->input('activity_type_id');
        $activity->title = $request->input('title');
        $activity->start_date = $request->input('start_date');
        $activity->end_date = $request->input('end_date');
        $activity->location = $request->input('location');
        $activity->budget = $budget_amount;
        $activity->remained_fund = $budget_amount;
        $activity->expected_output = $request->input('expected_output');
        $activity->performance_indicator = $request->input('performance_indicator');
        $activity->baseline = $request->input('baseline');
        $activity->outcome_targets = $request->input('outcome_targets');
        $activity->created_by = Auth::user()->id;
        $activity->save();

        //project responsible
        foreach ($_POST['responsible'] as $call_partner_id) {
            $new_update_partner = ProjectActivityResponsible::firstOrNew([
                'activity_id' => $activity->id,
                'call_partner_id' => $call_partner_id
            ]);
            $new_update_partner->save();
        }

        //redirect
        return Redirect::route('projects.activities', $project->id)->with('success', 'Activity updated');
    }

    //delete activity
    public function delete($project_id, $id): \Illuminate\Http\RedirectResponse
    {
        $this->data['title'] = 'Delete';
        //Perms::has_allowed('calls', 'delete');

        //project
        $project = Project::findOrFail($project_id);

        //activity
        $activity = ProjectActivity::findOrFail($id);

        //objective
        $objective = CallApplicationObjective::findOrFail($activity->objective_id);

        //delete project activity
        $activity->delete();

        //delete project activity fund
        $activity->funds()->delete();

        //delete project activity status
        $activity->activityStatus()->delete();

        //update objective budget
        $objective->remained_fund = $objective->remained_fund + $activity->budget;
        $objective->save();

        //redirect
        return Redirect::route('projects.activities', $project->id)->with('success', 'Project deleted');
    }

    /*==========================================
    APIs
    ==========================================*/
    public function api_show($id): void
    {
        $activity = ProjectActivity::find($id);

        //responsible
        $partners = [];

        if ($activity->responsible) {
            foreach ($activity->responsible as $val) {
                if ($val->partner->partner_type === 'INTERNAL') {
                    $partner = $val->partner->user->first_name . ' ' . $val->partner->user->middle_name . ' ' . $val->partner->user->surname;
                }
                elseif ($val->partner->partner_type === 'STUDENT') {
                    $partner = $val->partner->student->first_name . ' ' . $val->partner->student->middle_name . ' ' . $val->partner->student->surname;
                }
                elseif ($val->partner->partner_type === 'EXTERNAL') {
                    $partner = $activity->partner->researcher->full_name;
                }

                array_push($partners, $partner);
            }
        }

        $arr_activity = [
            'project_title' => $activity->project->application->project_title,
            'objective_title' => $activity->objective->objective,
            'title' => $activity->title,
            'activity_type' => $activity->type->name,
            'responsible' => join(', ', $partners),
            'start_date' => date('d-m-Y', strtotime($activity->start_date)),
            'end_date' => date('d-m-Y', strtotime($activity->end_date)),
            'location' => $activity->location,
            'budget' => number_format($activity->budget),
            'justification' => $activity->justification,
            'expected_output' => $activity->expected_output,
            'expected_outcome' => $activity->expected_outcome,
            'performance_indicator' => $activity->performance_indicator,
            'baseline' => $activity->baseline,
            'outcome_targets' => $activity->outcome_targets
        ];

        //response
        echo json_encode($arr_activity);
    }
}
