<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller;
use App\Models\Calls\CallApplicationPartner;
use App\Models\Projects\ProjectFundRequest;
use App\Models\Projects\ProjectFundRequestApproval;
use App\Models\Notification;
use App\Models\Projects\Project;
use App\Models\Projects\ProjectActivity;
use App\Models\Projects\ProjectActivityResponsible;
use App\Models\Projects\ProjectFundRequestActivity;
use App\Models\Projects\ProjectFundRequestAttachment;
use App\Models\Projects\ProjectFundRequestComments;
use App\Models\Settings\NumberSeries;
use App\User;
use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Redirect;

class FundRequestController extends Controller
{
    private $data;

    //constructor
    public function __construct()
    {
        $this->middleware('auth');
    }

    //lists of fund requests
    public function lists()
    {
        $this->data['title'] = "New Fund Requests";

        //project fund requests
        $project_fund_requests = ProjectFundRequest::where(function ($query) {
            $query->where('status', '=', 'new')
                ->orWhere('status', '=', 'approved')
                ->orWhere('status', '=', 'rejected')
                ->orWhere('status', '=', 'partial-approved');
        })
            ->orderBy('created_at', 'DESC')
            ->get();
        $this->data['project_fund_requests'] = $project_fund_requests;

        //render view
        return view('projects.fund_requests.lists', $this->data);
    }

    //id
    public function show($id): void
    {
    }

    //new fund request
    public function create($id)
    {
        $title = 'Request For Fund';
        //Perms::has_allowed('calls', 'delete');

        //activity
        $activity = ProjectActivity::findOrFail($id);

        //render view
        return view('projects.fund_requests.create', compact('title', 'activity'));
    }

    //store request for fund
    public function store(Request $request, $id): RedirectResponse
    {
        //activity
        $activity = ProjectActivity::findOrFail($id);

        //budget submitted
        $remained_fund = floatval(str_replace(",", "", $request->input('remained_fund')));
        $requested_amount = floatval(str_replace(",", "", $request->input('requested_amount')));

        //validation
        $this->validate(
            $request,
            [
                'requested_amount' => 'required|min:0|max:' . $remained_fund,
            ],
            [
                'requested_amount.required' => 'Requested amount is required',
                'requested_amount.max' => 'Requested amount should not exceed remained fund',
            ]
        );

        //request code
        $fund_request_code = $this->generate_request_code();

        //project
        $project_id = $request->input('project_id');
        $project = Project::findOrFail($project_id);

        //activity
        $activity_id = $request->input('activity_id');
        $activity = ProjectActivity::findOrFail($activity_id);

        //save new fund request
        $new_request = new ProjectFundRequest();
        $new_request->project_id = $project->id;
        $new_request->code = $fund_request_code;
        $new_request->amount = $requested_amount;
        $new_request->created_by = Auth::user()->id;
        $new_request->save();

        //create new project fund
        $request_fund_activity = ProjectFundRequestActivity::firstOrNew([
            'project_fund_request_id' => $new_request->id,
            'activity_id' => $activity->id
        ]);
        $request_fund_activity->amount = $requested_amount;
        $request_fund_activity->save();

        //updated activity status for applying fund
        $activity->apply_for_fund = 0;
        $activity->save();

        // Check if an attachment has been uploaded
        if ($request->has('attachments')) {
            //process for insertion
            for ($i = 0; $i < sizeof($request->file('attachments')); $i++) {
                $attachment = $request->file('attachments')[$i];

                // Make attachment name
                $attachment_name = time() . '_' . $attachment->getFilename() . '.' . $attachment->getClientOriginalExtension();

                //Move Uploaded File
                $attachment->move('assets/uploads/documents/', $attachment_name);

                //save file
                $new_attachment = ProjectFundRequestAttachment::firstOrNew(
                    [
                        'project_fund_request_id' => $new_request->id,
                        'attachment_type' => $_POST['attachment_types'][$i],
                        'attachment' => $attachment_name
                    ]
                );
                $new_attachment->save();
            }
        }

        //update fund request code
        $this->update_request_code($fund_request_code);

        //create approval_level
        $approval_levels = [
            'program_coordinator' => 'Program Coordinator',
            'drp' => 'Director of Research and Publication',
            'dvc_research' => 'Deputy Vice Chancellor - Research'
        ];

        foreach ($approval_levels as $key => $val) {
            $new_approval = new ProjectFundRequestApproval();
            $new_approval->project_fund_request_id = $new_request->id;
            $new_approval->approval_level = $key;
            $new_approval->created_by = Auth::user()->id;
            $new_approval->save();
        }

        //construct message
        $message = "New request for fund for <b> {$activity->project->application->project_title} </b> project with registration number {$activity->project->project_reg_number}";
        $message .= "<br/> Login in the system by  <a href=" . url('') . ">click here</a> and recommend/return the request.";
        $message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

        //send notification to program coordinator
        $this->send_notification($activity->project->application->call->programCoordinator, $message);

        //redirect
        return Redirect::route('projects.fund-requests', $activity->project->id)->with('success', 'New request for fund sent, please wait for approval');
    }

    //create fund with batch activities
    function create_batch()
    {
        $title = "Request For Fund";

        //activity_ids
        $activity_ids = session('activity_ids');

        //activities
        $activities = ProjectActivity::whereIn('id', $activity_ids)->get();

        //render view
        return view('projects.fund_requests.create_batch', compact('title', 'activities'));
    }

    //store request for fund
    function store_batch(Request $request)
    {
        //validation
        //budget submitted
        $remained_fund = floatval(str_replace(",", "", $request->input('remained_fund')));
        $total_amount = floatval(str_replace(",", "", $request->input('total_amount')));

        //validation
        $this->validate(
            $request,
            [
                //'total_amount' => 'required|min:0|max:' . $remained_fund,
            ],
            [
                'total_amount.required' => 'Total amount is required',
                'total_amount.max' => 'Total amount of request should not exceed remained fund',
            ]
        );

        //request code
        $fund_request_code = $this->generate_request_code();

        //project
        $project_id = $request->input('project_id');
        $project = Project::findOrFail($project_id);

        //save new fund request
        $new_request = new ProjectFundRequest();
        $new_request->project_id = $project->id;
        $new_request->code = $fund_request_code;
        $new_request->amount = $total_amount;
        $new_request->created_by = Auth::user()->id;
        $new_request->save();

        //activities
        $activity_ids = $request->input('activity_ids');


        foreach ($activity_ids as $k => $activity_id) {
            //create new project fund
            $request_fund_activity = ProjectFundRequestActivity::firstOrNew([
                'project_fund_request_id' => $new_request->id,
                'activity_id' => $activity_id
            ]);
            $request_fund_activity->amount = floatval(str_replace(",", "", $request->input('requested_amount')[$k]));
            $request_fund_activity->save();

            //updated activity status for applying fund
            $activity = ProjectActivity::findOrFail($activity_id);
            $activity->apply_for_fund = 0;
            $activity->save();
        }

        // Check if an attachment has been uploaded
        if ($request->has('attachments')) {
            //process for insertion
            for ($i = 0; $i < sizeof($request->file('attachments')); $i++) {
                $attachment = $request->file('attachments')[$i];

                // Make a image name
                $attachment_name = time() . '_' . $attachment->getFilename() . '.' . $attachment->getClientOriginalExtension();

                //Move Uploaded File
                $attachment->move('assets/uploads/documents/', $attachment_name);

                //save file
                $new_attachment = ProjectFundRequestAttachment::firstOrNew(
                    [
                        'project_fund_request_id' => $new_request->id,
                        'attachment_type' => $_POST['attachment_types'][$i],
                        'attachment' => $attachment_name
                    ]
                );
                $new_attachment->save();
            }
        }

        //update fund request code
        $this->update_request_code($fund_request_code);

        //create approval_level
        $approval_levels = [
            'program_coordinator' => 'Program Coordinator',
            'drp' => 'Director of Research and Publication',
            'dvc_research' => 'Deputy Vice Chancellor - Research'
        ];

        foreach ($approval_levels as $key => $val) {
            $new_approval = new ProjectFundRequestApproval();
            $new_approval->project_fund_request_id = $new_request->id;
            $new_approval->approval_level = $key;
            $new_approval->created_by = Auth::user()->id;
            $new_approval->save();
        }

        //construct message
        $message = "New request for fund for <b> {$project->application->project_title} </b> project with registration number $project->project_reg_number";
        $message .= "<br/> Login in the system by  <a href=" . url('') . ">click here</a> and recommend/return the request.";
        $message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

        //send notification to program coordinator
        $this->send_notification($project->application->call->programCoordinator, $message);

        //redirect
        return Redirect::route('projects.fund-requests', $project->id)->with('success', 'New fund request create, please wait for approval');
    }

    // request
    public function edit($id)
    {
        $title = 'Edit Fund Request';
        //Perms::has_allowed('calls', 'delete');

        //fund request
        $fund_request = ProjectFundRequest::findOrFail($id);

        //activities
        $activities = $fund_request->activities;

        //render view
        return view('projects.fund_requests.edit', compact('title', 'fund_request', 'activities'));
    }

    //update fund request
    function update(Request $request, $id)
    {
        //fund request
        $fund_request = ProjectFundRequest::findOrFail($id);

        //budget submitted
        // $remained_fund = floatval(str_replace(",", "", $request->input('remained_fund')));
        // $requested_amount = floatval(str_replace(",", "", $request->input('requested_amount')));
        $old_budget = $fund_request->amount;

        //validation
        $activity_ids = $request->input('activity_ids'); //activities

        $total_budget = 0;
        foreach ($activity_ids as $k => $val) {
            //calculate total budget
            $total_budget += floatval(str_replace(",", "", $request->input('requested_amount')[$k]));
        }

        //check if total budget exceed remained fund


        //update
        $fund_request->status = 'new';
        $fund_request->amount = $total_budget;
        $fund_request->created_by = Auth::user()->id;
        $fund_request->save();

        foreach ($activity_ids as $k => $val) {
            //update project fund
            $request_fund_activity = ProjectFundRequestActivity::updateOrCreate(
                [
                    'project_fund_request_id' => $fund_request->id,
                    'activity_id' => $val
                ],
                [
                    'amount' => floatval(str_replace(",", "", $request->input('requested_amount')[$k]))
                ]
            );
        }

        // Check if an attachment has been uploaded
        if ($request->has('attachments')) {
            //process for insertion
            for ($i = 0; $i < sizeof($request->file('attachments')); $i++) {
                $attachment = $request->file('attachments')[$i];

                // Make a image name
                $attachment_name = time() . '_' . $attachment->getFilename() . '.' . $attachment->getClientOriginalExtension();

                //Move Uploaded File
                $attachment->move('assets/uploads/documents/', $attachment_name);

                //save file
                $updated_attachment = ProjectFundRequestAttachment::updateOrCreate(
                    [
                        'project_fund_request_id' => $fund_request->id,
                        'attachment_type' => $_POST['attachment_types'][$i],

                    ],
                    [
                        'attachment' => $attachment_name
                    ]
                );
            }
        }

        //my inclusion for update remained fund
        //update available fund of overall project
        $project = Project::findOrFail($fund_request->project_id);
        $project->remained_fund = $project->remained_fund - ($total_budget - $old_budget);
        $project->save();
        //end of my inclusion

        //send notification to program coordinator for approval to approval the fund request
        //program coordinator
        $pc = $fund_request->project->application->call->programCoordinator->first_name . ' ' . $fund_request->project->application->call->programCoordinator->surname;
        $pc_email = $fund_request->project->application->call->programCoordinator->email;
        $pc_phone = $fund_request->project->application->call->programCoordinator->phone;

        //construct email for sending to pc
        $message_id = Notification::generate_message_id();

        //message
        $message = "There is a new fund request for <b> {$fund_request->project->application->project_title} </b> project with registration number {$fund_request->project->project_reg_number}";
        $message .= "<br/> Login in the system by  <a href=" . url('') . ">Click here</a> and approve/reject the request";
        $message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

        //insert notification
        $notification = new Notification(
            [
                'message' => $message,
                'message_id' => $message_id,
                'user' => 'PROGRAM_COORDINATOR',
                'user_id' => $fund_request->project->application->call->programCoordinator->id,
                'email' => $pc_email,
                'phone' => Notification::cast_mobile($pc_phone),
                'app_status' => 'PENDING',
                'sms_status' => 'PENDING',
                'email_status' => 'PENDING',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => Auth::user()->id
            ]
        );
        $notification->save();

        //create array
        $array = ['name' => $pc, 'email' => $pc_email, 'message' => $message];

        if (Notification::send_email($array)) {
            $data = ['email_status' => 'SENT'];
        }
        else {
            $data = ['email_status' => 'REJECTED'];
        }

        Notification::where(['id' => $notification->id])->update($data);

        //redirect
        return Redirect::route('projects.fund-requests', $fund_request->project->id)->with('success', 'Fund request updated, please wait for approval');
    }

    /*==========================================================
    APPROVED FUND REQUESTS ACTIONS
    ==========================================================*/
    //lists of approved fund requests
    function approved()
    {
        $this->data['title'] = "Approved Fund Requests";

        //project fund requests
        $project_fund_requests = ProjectFundRequest::where('status', '=', 'approved')->orderBy('created_at', 'DESC')->get();

        $this->data['project_fund_requests'] = $project_fund_requests;

        //render view
        return view('projects.fund_requests.approved', $this->data);
    }

    //change status
    function change_status($id)
    {
        $title = 'Fund Requests';
        //Perms::has_allowed('calls', 'delete');

        //fund request
        $request = ProjectFundRequest::findOrFail($id);

        //render view
        return view('projects.fund_requests.change_status', compact('title', 'request'));
    }

    //store request for fund
    function store_status(Request $request, $id)
    {

        //validation
        $this->validate(
            $request,
            [
                'status' => 'required',
                'attachment' => 'required',
            ],
            [
                'status.required' => 'Status required',
                'attachment.required' => 'Payment attachment is required',
            ]
        );

        // $request = ProjectFundRequest::findOrFail($id);
        $fund_request = ProjectFundRequest::findOrFail($id);
        $fund_request->payment_status = $request->input('status');
        $fund_request->payment_description = $request->input('description');
        $fund_request->payment_done_by = Auth::user()->id;

        // Check if an attachment has been uploaded
        if ($request->has('attachment')) {
            // Get image file
            $attachment = $request->file('attachment');

            // Make a image name
            $name = time() . '_' . $attachment->getClientOriginalName() . '.' . $attachment->getClientOriginalExtension();

            //Move Uploaded File
            $attachment->move('assets/uploads/documents/', $name);

            // Set attachment name
            $request->payment_attachment = $name;
        }
        //save            
        $fund_request->save();

        //redirect
        return redirect()->back()->with('success', 'Payment status changed');
    }

    //print approved letter
    function approval_letter($id)
    {
        $title = 'Fund Requests';
        //Perms::has_allowed('calls', 'delete');

        //fund request
        $request = ProjectFundRequest::findOrFail($id);

        //fund request activity
        $fund_request_activity = ProjectFundRequestActivity::where(['project_fund_request_id' => $id])->first();
        //fund request responsible for fund management
        $act_resp = ProjectActivityResponsible::where(['activity_id' => $fund_request_activity->activity_id])->first();

        $appl_partner = CallApplicationPartner::whereId($act_resp->call_partner_id)->first();

        //check if responsible exists
        $responsible_count = ProjectActivityResponsible::where(['activity_id' => $fund_request_activity->activity_id])->count();

        if ($responsible_count == 0) {
            $resp_user = 'only_pi';
        } elseif ($appl_partner->partner_type === 'EXTERNAL') {
            $resp_user = 'only_pi';
        } else {
            // $project = Project::whereId($request->project_id);
            $appl_partner = CallApplicationPartner::whereId($act_resp->call_partner_id)->first();
            $resp_user = User::whereId($appl_partner->partner_id)->first();
        }


        //program coordinator
        $pc = $request->project->application->call->programCoordinator;

        //drp
        $drp = User::get_drp();

        //dvc
        $dvc = User::get_dvc_research();

        //render view
        return view('projects.fund_requests.approval_letter', compact('title', 'request', 'pc', 'drp', 'dvc', 'resp_user'));
    }


    //save approval print status (whether printed or not)
    function store_appr_print_status(Request $request, $id)
    {
        //fund request
        $fund_request = ProjectFundRequest::findOrFail($id);

        if ($request->printed_status === 'is_printed') {
            $fund_request->printed_status = 1;
            $fund_request->save();
        } else {
            $fund_request->printed_status = 0;
            $fund_request->save();
        }
        //redirect
        return Redirect::route('fund-requests.lists')->with('success', 'the approval letter is printed');
    }


    function printed()
    {
        return 1;
    }



    /*==========================================================
    APIs => details, approve, award, decline and register project
    ==========================================================*/
    //api to show application details
    function api_details($id)
    {
        //fund request
        $request = ProjectFundRequest::findOrFail($id);

        //activities
        $activities = [];

        if ($request->activities) {
            foreach ($request->activities as $val) {
                array_push($activities, $val->activity->title);
            }
        }

        //attachments
        $attachments = [];

        if ($request->fundRequestAttachments) {
            foreach ($request->fundRequestAttachments as $val) {
                $url = '<a href="' . asset('assets/uploads/documents/' . $val->attachment) . '" class="font-weight-600 text-primary" download>' . $val->attachment_type . '</a>';

                //push
                array_push($attachments, $url);
            }
        }

        //requests
        $arr_fund_request = [
            'project_title' => $request->project->application->project_title,
            'amount' => number_format($request->amount),
            'activity' => join('<br />', $activities),
            'attachments' => join('<br />', $attachments),
            'created_at' => date('d-m-Y H:i', strtotime($request->created_at)),
        ];

        //response
        echo json_encode($arr_fund_request);
    }

    //approval fund request
    public function api_approve($id, $role_name): void
    {
        $fund_request = ProjectFundRequest::findOrFail($id);

        //update fund request approval status to 1
        $request_approval = ProjectFundRequestApproval::where(['project_fund_request_id' => $id, 'approval_level' => $role_name])->first();

        if ($request_approval) {
            //where
            $where = ['project_fund_request_id' => $id, 'approval_level' => $role_name];

            //data
            $data = ['status' => 1, 'updated_by' => Auth::user()->id, 'updated_at' => date('Y-m-d H:i:s')];

            //update fund request approval
            if (ProjectFundRequestApproval::where($where)->update($data)) {
                //update project funding request to partial approve
                $fund_request->status = 'partial-approved';
                $fund_request->save();

                //construct email for sending to pc
                //message id
                $message_id = Notification::generate_message_id();

                //message
                $message = "";

                //send notification for specific role
                //check if role_name = hod
                if ($role_name === 'program_coordinator') {
                    //drp
                    $user = User::get_drp();
                    $full_name = $user->first_name . ' ' . $user->surname;
                    $email = $user->email;
                    $phone = $user->phone;

                    //message
                    $message .= "There is a new fund request for <b> {$fund_request->project->application->project_title} </b> project with registration number {$fund_request->project->project_reg_number}.";
                    $message .= "<br/>The request already approved by Program Coordinator.";
                } else if ($role_name === 'drp') {
                    //dvc_research
                    $user = User::get_dvc_research();
                    $full_name = $user->first_name . ' ' . $user->surname;
                    $email = $user->email;
                    $phone = $user->phone;

                    //message
                    $message .= "There is a new fund request for <b> {$fund_request->project->application->project_title} </b> project with registration number {$fund_request->project->project_reg_number}.";
                    $message .= "<br/>The request already approved by Program Coordinator and Director of Research and Publication(DRP).";
                } else if ($role_name === 'dvc_research') {
                    //send message to pi
                    $pi_message = "<b>The fund request for <b>{$fund_request->project->application->project_title} </b> project with registration number {$fund_request->project->project_reg_number} has been approved by DVC Research.";
                    $pi_message .= "<br/> Login in the system by  <a href=" . url('') . ">Click here</a> to view the status.";
                    $pi_message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

                    //update approval status
                    $apprv = ProjectFundRequestApproval::where(['project_fund_request_id' => $id, 'status' => 0])->count();

                    if ($apprv == 0) {
                        //update project funding request to approve
                        $fund_request->status = 'approved';
                        $fund_request->save();
                    }

                    //send notification to PI
                    $this->send_notification_PI($fund_request, $pi_message);

                    //drp
                    $user = User::get_drp();
                    $full_name = $user->first_name . ' ' . $user->surname;
                    $email = $user->email;
                    $phone = $user->phone;

                    //message
                    $message .= "The fund request for <b> {$fund_request->project->application->project_title} </b> project with registration number {$fund_request->project->project_reg_number} has been successfully approved by DVC Research.";
                }

                //message
                $message .= "<br/> Login in the system by  <a href=" . url('') . ">Click here</a> and recommend/return with comment the fund request.";
                $message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

                //insert notification
                $notification = new Notification(
                    [
                        'message' => $message,
                        'message_id' => $message_id,
                        'user' => strtoupper($role_name),
                        'user_id' => $user->id,
                        'email' => $email,
                        'phone' => Notification::cast_mobile($phone),
                        'app_status' => 'PENDING',
                        'sms_status' => 'PENDING',
                        'email_status' => 'PENDING',
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => Auth::user()->id
                    ]
                );
                $notification->save();

                //create array
                $array = ['name' => $full_name, 'email' => $email, 'message' => $message];

                if (Notification::send_email($array)) {
                    $data = ['email_status' => 'SENT'];
                }
                else {
                    $data = ['email_status' => 'REJECTED'];
                }

                Notification::where(['id' => $notification->id])->update($data);

                //response
                $response['status'] = 'success';
            } else {
                $response['status'] = 'failed';
            }
        } else {
            $response['status'] = 'failed';
        }
        echo json_encode($response);
    }

    //reject fund request
    public function api_reject($id, $role_name, $remarks): void
    {
        $fund_request = ProjectFundRequest::findOrFail($id);

        //update fund request approval status to 0
        $request_approval = ProjectFundRequestApproval::where(['project_fund_request_id' => $id])->get();

        if ($request_approval) {
            //where
            $where = ['project_fund_request_id' => $id];

            //data
            $data = ['status' => 0, 'updated_by' => Auth::user()->id, 'updated_at' => date('Y-m-d H:i:s')];

            //update fund request approval
            if (ProjectFundRequestApproval::where($where)->update($data)) {
                //update project funding request to return
                $fund_request->status = 'rejected';
                //$fund_request->save();

                if ($fund_request->save()) {
                    $fund_request_return_comment = new ProjectFundRequestComments([
                        'project_fund_request_id' => $id,
                        'project_id' => $fund_request->project_id,
                        'comment_from' => $role_name,
                        'remarks' => $remarks,
                        'created_by' => 2729,
                        'created_at' => date('Y-m-d H:i:s'),
                    ]);
                }
                $fund_request_return_comment->save();

                //update remarks based on role name
                ProjectFundRequestApproval::where(['project_fund_request_id' => $id, 'approval_level' => $role_name])->update(['remarks' => $remarks]);

                //pi message
                $pi_message = "<b>The fund request for <b>{$fund_request->project->application->project_title} </b> project with registration number {$fund_request->project->project_reg_number} has returned with remarks.";
                $pi_message .= "<br/> Login in the system by  <a href=" . url('/projects/') . ">Click here</a> and review the request.";
                $pi_message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

                //send notification for specific role
                //check if role_name = hod
                if ($role_name === 'program_coordinator') {
                    //send message to PI
                    $this->send_notification_PI($fund_request, $pi_message);
                } else if ($role_name === 'drp') {
                    //send message to PI
                    $this->send_notification_PI($fund_request, $pi_message);
                } else if ($role_name === 'dvc_research') {
                    //send message to PI
                    $this->send_notification_PI($fund_request, $pi_message);
                }

                //response
                $response['status'] = 'success';
            } else {
                $response['status'] = 'failed';
            }
        } else {
            $response['status'] = 'failed';
        }

        echo json_encode($response);
    }


    //show fund request /open fund request details
    public function show_fund_request($id)
    {
        $this->data['title'] = 'View fund request details';
        $this->data['request'] = ProjectFundRequest::find($id);
        $this->data['remarks'] = ProjectFundRequestComments::where('project_fund_request_id', $id)->get();

        return view('projects.fund_requests.show_fund_request')->with('data', $this->data);
    }


    //send notification to PI
    public function send_notification_PI($fund_request, $message): void
    {
        //construct email for sending to pi
        //message
        $message_id = Notification::generate_message_id();

        //project pi
        //program accountant
        $pi = $fund_request->project->application->pi->first_name . ' ' . $fund_request->project->application->pi->surname;
        $pi_email = $fund_request->project->application->pi->email;
        $pi_phone = $fund_request->project->application->pi->phone;

        //insert notification
        $notification = new Notification(
            [
                'message' => $message,
                'message_id' => $message_id,
                'user' => 'PI',
                'user_id' => $fund_request->project->application->pi->id,
                'email' => $pi_email,
                'phone' => Notification::cast_mobile($pi_phone),
                'app_status' => 'PENDING',
                'sms_status' => 'PENDING',
                'email_status' => 'PENDING',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => Auth::user()->id
            ]
        );
        $notification->save();

        //create array
        $array = ['name' => $pi, 'email' => $pi_email, 'message' => $message];

        if (Notification::send_email($array)) {
            $data = ['email_status' => 'SENT'];
        }
        else {
            $data = ['email_status' => 'REJECTED'];
        }

        Notification::where(['id' => $notification->id])->update($data);
    }

    //send notification to user
    public function send_notification($user, $message): void
    {
        //construct email for sending to pi
        $message_id = Notification::generate_message_id();

        //user
        $name = $user->first_name . ' ' . $user->surname;
        $email = $user->email;
        $phone = $user->phone;

        //insert notification
        $notification = new Notification(
            [
                'message' => $message,
                'message_id' => $message_id,
                'user_id' => $user->id,
                'email' => $email,
                'phone' => Notification::cast_mobile($phone),
                'app_status' => 'PENDING',
                'sms_status' => 'PENDING',
                'email_status' => 'PENDING',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => Auth::user()->id
            ]
        );
        $notification->save();

        //create array
        $array = ['name' => $name, 'email' => $email, 'message' => $message];

        if (Notification::send_email($array)) {
            $data = ['email_status' => 'SENT'];
        }
        else {
            $data = ['email_status' => 'REJECTED'];
        }

        Notification::where(['id' => $notification->id])->update($data);
    }


    /*================================================================
    API CALLS
    ================================================================*/
    //generate request code
    function generate_request_code()
    {
        $number_series = NumberSeries::where('code', 'fundRequestCode')->first();

        //next number
        $current_number = $number_series->next;

        //add_check_digit
        $add_check_digit = $number_series->add_check_digit;

        //next number
        $next_number = $current_number + $add_check_digit;

        //request code
        $request_code = $number_series->prefix . $next_number;

        return $request_code;
    }

    //update last code
    public function update_request_code($code): void
    {
        //last code
        $last_code = substr($code, 2);

        //update number series
        $nm = NumberSeries::where(['code' => 'fundRequestCode'])->first();
        $nm->next = $last_code;
        $nm->save();

        //TODO: check if last number
    }
}
