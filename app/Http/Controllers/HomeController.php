<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\User;
use App\Role;
use App\Student;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    
     //generate user
    //  function autouser()
    //  {
    //      $students = Student::all();
         
    //     //create user to store data
    //     foreach ($students as $student) {

    //         if(empty($student['phone'])){
    //             $student['phone'] = "0717000000";
    //         }

    //         if(empty($student['email'])){
    //             $student['email'] = "student@udsm.ac.tz";
    //         }

    //         $user = new User([
    //             'first_name' => $student['first_name'],
    //             'middle_name' => $student['middle_name'],
    //             'surname' => $student['surname'],
    //             'email' => $student['email'],
    //             'phone' => $student['phone'],
    //             'reg_number' => $student['reg_number'],
    //             'username' => $student['reg_number'],
    //             'password' => Hash::make(12345678)
    //         ]);
    //         $user->save();
    //         $user->roles()->attach(8);

    //     }
       
    //     return redirect('/users')->with('success', 'Students User registered'); //redirect
    //  }
}
