<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Role;
use App\Rules\MatchOldPassword;
use App\User;
use App\Level;
use App\Models\Settings\College;
use App\Models\Settings\Department;
use App\Models\Settings\ResearchArea;
use Hash;
use DB;
use Illuminate\Http\Request;
use Redirect;

class ProfileController extends Controller
{
    private $data;

    public function __construct()
    {
        $this->middleware('auth');
    }

    //profile
    function index()
    {
        $this->data['title'] = 'Profile';

        $user = User::find(auth()->user()->id);

        if (!$user)
            abort('404', 'User does not exist');

        $this->data['user'] = $user;

        //user roles
        $user_roles = \DB::table('role_user')->where('user_id', $user->id)->get();
        $this->data['user_roles'] = $user_roles;

        foreach ($this->data['user_roles'] as $k => $v) {
            $this->data['user_roles'][$k]->role = Role::find($v->role_id);
        }

        //level
        $this->data['level'] = Level::find($user->level_id);

        //college
        $this->data['college'] = College::find($user->college_id);

        //department
        $this->data['department'] = Department::find($user->department_id);

        //research areas
        $research_area_ids = explode(',', $user->research_area_ids);

        $array = [];
        foreach ($research_area_ids as $id) {
            $research_area = ResearchArea::find($id);

            if ($research_area)
                array_push($array, $research_area->name);
        }
        $this->data['research_areas'] = join(', ', $array);

        return view('profile.index', $this->data);
    }

    //edit
    function edit()
    {
        $this->data['title'] = 'Update Profile';

        $user = User::find(auth()->user()->id);

        if (!$user)
            abort('404', 'User does not exist');

        $this->data['user'] = $user;

        //roles
        $this->data['roles'] = Role::all();

        //college
        $this->data['colleges'] = College::where_cond()->get();

        //department
        $this->data['departments'] = Department::where_cond()->get();

        //levels
        $this->data['levels'] = Level::all();

        //research areas
        $this->data['research_areas'] = ResearchArea::all();

        //assigned research areas
        $arr_research_area_ids = [];

        $research_areas_ids = explode(',', $user->research_area_ids);
        if ($research_areas_ids) {
            foreach ($research_areas_ids as $val) {
                array_push($arr_research_area_ids, $val);
            }
        }
        $this->data['arr_research_area_ids'] = $arr_research_area_ids;

        //current roles
        $current_roles = DB::table('role_user')->where('user_id', $user->id)->get();
        $this->data['current_roles'] = $current_roles;

        return view('profile.edit', $this->data);
    }

    //update user
    function update(Request $request, $id)
    {
        //validate
        $this->validate(
            $request,
            [
                'first_name' => 'required|string',
                'surname' => 'required|string',
                'email' => 'required|string|max:255',
                'phone' => 'required|string'
            ],
            [
                'first_name.required' => 'First name is required',
                'surname.required' => 'Surname is required',
                'email.required' => 'Email is required',
            ]
        );

        //check for research areas
        $research_areas = [];
        if (isset($_POST['research_area_ids'])) {
            foreach ($_POST['research_area_ids'] as $research_area_id) {
                array_push($research_areas, $research_area_id);
            }
        }

        //create user to store data
        $user = User::query()->find($id);
        $user->first_name = $request->input('first_name');
        $user->middle_name = $request->input('middle_name');
        $user->surname = $request->input('surname');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->institution = $request->input('institution');
        $user->academic_qualification = $request->input('academic_qualification');
        $user->specialization = $request->input('specialization');
        $user->experience = $request->input('experience');
        $user->gender = $request->input('gender');
        $user->research_area_ids = join(',', $research_areas);

        // Check if a attachment has been uploaded
        if ($request->has('attachment')) {
            // Get image file
            $attachment = $request->file('attachment');

            // Make a image name
            $name = time() . '_' . $attachment->getFilename();

            //Move Uploaded File
            $attachment->move('assets/uploads/cv/', $name);

            // Set cv
            $user->cv = $name;
        }
        if ($request->has('rank'))
            $user->rank = $request->input('rank');

        $user->save(); //save user

        return redirect('/profile/edit')->with('success', 'Profile updated'); //redirect
    }

    //change password
    function change_password()
    {
        $this->data['title'] = 'Change Password';

        return view('profile.change_password', $this->data);
    }

    /**
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store_password(Request $request)
    {
        $this->validate($request, [
            'current_password' => ['required', new MatchOldPassword()],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);

        //update user
        $user = User::find(auth()->user()->id);
        $user->password = Hash::make($request->input('new_password'));
        $user->save();

        return Redirect::route('profile.change-password')->with('success', 'Password change successfully.'); //redirect
    }
}
