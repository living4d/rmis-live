<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Role;
use App\Level;
use App\Perms;
use App\Models\Settings\College;
use App\Models\Settings\Department;
use App\Models\Settings\ResearchArea;
use App\Traits\UploadTrait;
use Redirect;

class UsersController extends Controller
{
    use UploadTrait;

    private $data;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //users
    function lists(Request $request)
    {
        Perms::has_allowed('users', 'lists');
        $this->data['title'] = 'Users';

        //if post        
        if (isset($_POST['filter'])) {
            //get data
            $department_id = $request->get('department_id');
            $keyword = $request->get('keyword');

            //search users
            $users = User::search_all_users($department_id, $keyword);

            if ($users) {
                $this->data['users'] = $users;

                foreach ($this->data['users'] as $key => $value) {
                    //level
                    $level = Level::find($value->level_id);
                    $this->data['users'][$key]->level = $level;

                    //role_user
                    $role_user = DB::table('role_user')->where('user_id', $value->id)->get();

                    $i = 1;
                    $array = [];
                    foreach ($role_user as $role) {
                        $roles = Role::find($role->role_id);
                        array_push($array, strtoupper($roles->name));
                        $i++;
                    }
                    $this->data['users'][$key]->roles = $array;
                }
            } else {
                $this->data['users'] = [];
            }
        } else {
            //users
            $users = User::where_cond()->paginate(100);
            $this->data['users'] = $users;

            foreach ($this->data['users'] as $key => $value) {
                //level
                $level = Level::find($value->level_id);
                $this->data['users'][$key]->level = $level;

                //role_user
                $role_user = DB::table('role_user')->where('user_id', $value->id)->get();

                $i = 1;
                $array = [];
                foreach ($role_user as $role) {
                    $roles = Role::find($role->role_id);
                    array_push($array, strtoupper($roles->name));
                    $i++;
                }
                $this->data['users'][$key]->roles = $array;
            }
        }

        //populate data
        $this->data['departments'] = Department::where_cond()->get();

        //roles
        $this->data['roles'] = Role::all();

        return view('users.lists', $this->data);
    }

    //register user
    function create()
    {
        Perms::has_allowed('users', 'create');
        $this->data['title'] = 'Register New User';

        //roles
        $this->data['roles'] = Role::all();

        //college
        $this->data['colleges'] = College::where_cond()->get();

        //department
        $this->data['departments'] = Department::where_cond()->get();

        //levels
        $this->data['levels'] = Level::all();

        //research areas
        $this->data['research_areas'] = ResearchArea::all();

        return view('users.create', $this->data);
    }

    //store users details
    function store(Request $request)
    {
        //validation
        $this->validate(
            $request,
            [
                'first_name' => 'required|string',
                'surname' => 'required|string',
                'gender' => 'required',
                'email' => 'required|string|max:255|unique:users',
                'phone' => 'required|string',
                'level_id' => 'required',
                'college_id' => 'required_if:level_id,==,2',
                'department_id' => 'required_if:level_id,==,3',
                'institution' => 'required_if:level_id,==,4',
                'academic_qualification' => 'required_if:level_id,==,4',
                'specialization' => 'required_if:level_id,==,4',
                'experience' => 'required_if:level_id,==,4',
                'research_area_ids' => 'required_if:level_id,==,4',
                'role_ids' => 'required',
                'username' => 'required',
                'password' => 'required|string|min:8',
                'password_confirm' => 'required|same:password|min:8'
            ],
            [
                'first_name.required' => 'First name is required',
                'surname.required' => 'Surname is required',
                'gender.required' => 'Gender is required',
                'email.required' => 'Email is required',
                'email.unique' => 'Email must be unique',
                'level_id.required' => 'User level is required',
                'college_id.required_if' => 'College is required',
                'department_id_id.required_if' => 'Department is required',
                'institution.required_if' => 'Institution is required',
                'academic_qualification.required_if' => 'Academic qualification is required',
                'specialization.required_if' => 'Specialization is required',
                'experience.required_if' => 'Experience is required',
                'research_area_ids.required_if' => 'Research area(s) required',
                'role_ids.required' => 'User role(s) required',
                'username.required' => 'Username is required',
                'password.required' => 'Password is required',
                'password.min:8' => 'Password must be 8 or more than character',
                'password_confirm.required' => 'Password confirm is required',
                'password_confirm.same:password' => 'Password confirm must match password'
            ]
        );

        //check for research areas
        $research_areas = [];
        if (isset($_POST['research_area_ids'])) {
            foreach ($_POST['research_area_ids'] as $research_area_id) {
                array_push($research_areas, $research_area_id);
            }
        }

        // Check if a attachment has been uploaded
        $cv = '';
        if ($request->has('attachment')) {
            // Get image file
            $attachment = $request->file('attachment');

            // name
            $name = time() . '_' . $attachment->getFilename() . '.' . $attachment->getClientOriginalExtension();

            //Move Uploaded File
            $attachment->move('assets/uploads/cv/', $name);

            // Set user profile image path in database to filePath
            $cv = $name;
        }

        //create user to store data
        $user = new User([
            'first_name' => $request->input('first_name'),
            'middle_name' => $request->input('middle_name'),
            'surname' => $request->input('surname'),
            'gender' => $request->input('gender'),
            'rank' => $request->input('rank'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'level_id' => $request->input('level_id'),
            'college_id' => $request->input('college_id'),
            'department_id' => $request->input('department_id'),
            'institution' => $request->input('institution'),
            'academic_qualification' => $request->input('academic_qualification'),
            'specialization' => $request->input('specialization'),
            'experience' => $request->input('experience'),
            'research_area_ids' => join(',', $research_areas),
            'cv' => $cv,
            'username' => $request->input('username'),
            'password' => Hash::make($request->input('password'))
        ]);
        $user->save();

        //insert roles
        foreach ($request->input('role_ids') as $role_id) {

            $user->roles()->attach($role_id);
        }

        return Redirect::route('users.lists')->with('success', 'User registered'); //redirect
    }

    //show user
    function show($id)
    {
        Perms::has_allowed('users', 'edit');
        $this->data['title'] = 'User Profile';

        //user
        $user = User::findOrFail($id);
        $this->data['user'] = $user;

        //user roles
        $user_roles = \DB::table('role_user')->where('user_id', $user->id)->get();
        $this->data['user_roles'] = $user_roles;

        foreach ($this->data['user_roles'] as $k => $v) {
            $this->data['user_roles'][$k]->role = Role::find($v->role_id);
        }

        //level
        $this->data['level'] = Level::find($user->level_id);

        //college
        $this->data['college'] = College::find($user->college_id);

        //department
        $this->data['department'] = Department::find($user->department_id);

        //research areas
        $research_area_ids = explode(',', $user->research_area_ids);

        $array = [];
        foreach ($research_area_ids as $id) {
            $research_area = ResearchArea::find($id);

            if ($research_area)
                array_push($array, $research_area->name);
        }
        $this->data['research_areas'] = join(', ', $array);

        return view('users.show', $this->data);
    }

    //edit
    function edit($id)
    {
        Perms::has_allowed('users', 'edit');
        $this->data['title'] = 'Edit User';

        //user
        $user = User::findOrFail($id);
        $this->data['user'] = $user;

        //roles
        $this->data['roles'] = Role::all();

        //college
        $this->data['colleges'] = College::where_cond()->get();

        //department
        $this->data['departments'] = Department::where_cond()->get();

        //levels
        $this->data['levels'] = Level::all();

        //research areas
        $this->data['research_areas'] = ResearchArea::all();

        //assigned research areas
        $arr_research_area_ids = [];

        $research_areas_ids = explode(',', $user->research_area_ids);
        if ($research_areas_ids) {
            foreach ($research_areas_ids as $val) {
                array_push($arr_research_area_ids, $val);
            }
        }
        $this->data['arr_research_area_ids'] = $arr_research_area_ids;

        //current roles
        $current_roles = DB::table('role_user')->where('user_id', $id)->get();
        $this->data['current_roles'] = $current_roles;

        return view('users.edit', $this->data);
    }


    //manage user : edit user without alter username or password
    function manage($id)
    {
        Perms::has_allowed('users', 'edit');
        $this->data['title'] = 'Edit User';

        //user
        $user = User::findOrFail($id);
        $this->data['user'] = $user;

        //roles
        $this->data['roles'] = Role::all();

        //college
        $this->data['colleges'] = College::where_cond()->get();

        //department
        $this->data['departments'] = Department::where_cond()->get();

        //levels
        $this->data['levels'] = Level::all();

        //research areas
        $this->data['research_areas'] = ResearchArea::all();

        //assigned research areas
        $arr_research_area_ids = [];

        $research_areas_ids = explode(',', $user->research_area_ids);
        if ($research_areas_ids) {
            foreach ($research_areas_ids as $val) {
                array_push($arr_research_area_ids, $val);
            }
        }
        $this->data['arr_research_area_ids'] = $arr_research_area_ids;

        //current roles
        $current_roles = DB::table('role_user')->where('user_id', $id)->get();
        $this->data['current_roles'] = $current_roles;

        return view('users.manage', $this->data);
    }


    //update user
    function update(Request $request, $id)
    {
        //validate
        $this->validate(
            $request,
            [
                'first_name' => 'required|string',
                'surname' => 'required|string',
                'gender' => 'required',
                'email' => 'required|string|max:255',
                'phone' => 'required|string',
                // 'level_id' => 'required',
                'college_id' => 'required_if:level_id,==,2',
                'department_id' => 'required_if:level_id,==,3',
                'institution' => 'required_if:level_id,==,4',
                'academic_qualification' => 'required_if:level_id,==,4',
                'specialization' => 'required_if:level_id,==,4',
                'experience' => 'required_if:level_id,==,4',
                'research_area_ids' => 'required_if:level_id,==,4',
                'role_ids' => 'required',
            ],
            [
                'first_name.required' => 'First name is required',
                'surname.required' => 'Surname is required',
                'gender.required' => 'Gender is required',
                'email.required' => 'Email is required',
                'level_id.required' => 'User level is required',
                'college_id.required_if' => 'College is required',
                'department_id_id.required_if' => 'Department is required',
                'institution.required_if' => 'Institution is required',
                'academic_qualification.required_if' => 'Academic qualification is required',
                'specialization.required_if' => 'Specialization is required',
                'experience.required_if' => 'Experience is required',
                'research_area_ids.required_if' => 'Research area(s) required',
                'role_ids.required' => 'User role(s) required',
                'username.required' => 'Username is required',
            ]
        );

        //validate password
        if ($request->input('password') != '') {
            $this->validate($request, [
                'password' => 'required|string|min:8',
                'password_confirm' => 'required|string|min:8|same:password'
            ], [
                'password.required' => 'Password is required',
                'password.min:8' => 'Password must be 8 or more than character',
                'password_confirm.required' => 'Password confirm is required',
                'password_confirm.same:password' => 'Password confirm must match password'
            ]);
        }

        //check for research areas
        $research_areas = [];
        if (isset($_POST['research_area_ids'])) {
            foreach ($_POST['research_area_ids'] as $research_area_id) {
                array_push($research_areas, $research_area_id);
            }
        }

        //create user to store data
        $user = User::query()->find($id);
        $user->first_name = $request->input('first_name');
        $user->middle_name = $request->input('middle_name');
        $user->surname = $request->input('surname');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->gender = $request->input('gender');
        $user->rank = $request->input('rank');
        // $user->level_id = $request->input('level_id');
        $user->college_id = $request->input('college_id');
        $user->department_id = $request->input('department_id');
        $user->institution = $request->input('institution');
        $user->academic_qualification = $request->input('academic_qualification');
        $user->specialization = $request->input('specialization');
        $user->experience = $request->input('experience');
        $user->research_area_ids = join(',', $research_areas);
        $user->username = $request->input('username');

        $old_level = $user->level_id;
        if (Auth::user()->hasRole("admin")) {
            $user->level_id = $request->input('level_id');
        } else {
            $user->level_id = $old_level;
        }

        // Check if a attachment has been uploaded
        if ($request->has('attachment')) {
            // Get image file
            $attachment = $request->file('attachment');

            // name
            $name = time() . '_' . $attachment->getFilename() . '.' . $attachment->getClientOriginalExtension();

            //Move Uploaded File
            $attachment->move('assets/uploads/cv/', $name);

            // Set user profile image path in database to filePath
            $user->cv = $name;
        }

        if ($request->has('password'))
            $user->password = Hash::make($request->input('password'));

        $user->save(); //save user

        //remove role from role_user
        DB::table('role_user')->where('user_id', $id)->delete();

        //insert roles
        foreach ($request->input('role_ids') as $role_id) {
            $user->roles()->attach($role_id);
        }

        return Redirect::route('users.lists')->with('success', 'User updated'); //redirect
    }


    //update user on manage route
    function update_manage(Request $request, $id)
    {
        //validate
        $this->validate(
            $request,
            [
                'first_name' => 'required|string',
                'surname' => 'required|string',
                'gender' => 'required',
                'email' => 'required|string|max:255',
                'phone' => 'required|string',
                // 'level_id' => 'required',
                'college_id' => 'required_if:level_id,==,2',
                'department_id' => 'required_if:level_id,==,3',
                'institution' => 'required_if:level_id,==,4',
                'academic_qualification' => 'required_if:level_id,==,4',
                'specialization' => 'required_if:level_id,==,4',
                'experience' => 'required_if:level_id,==,4',
                'research_area_ids' => 'required_if:level_id,==,4',
                'role_ids' => 'required',
            ],
            [
                'first_name.required' => 'First name is required',
                'surname.required' => 'Surname is required',
                'gender.required' => 'Gender is required',
                'email.required' => 'Email is required',
                'level_id.required' => 'User level is required',
                'college_id.required_if' => 'College is required',
                'department_id_id.required_if' => 'Department is required',
                'institution.required_if' => 'Institution is required',
                'academic_qualification.required_if' => 'Academic qualification is required',
                'specialization.required_if' => 'Specialization is required',
                'experience.required_if' => 'Experience is required',
                'research_area_ids.required_if' => 'Research area(s) required',
                'role_ids.required' => 'User role(s) required',
                'username.required' => 'Username is required',
            ]
        );

        //validate password
        // if ($request->input('password') != '') {
        //     $this->validate($request, [
        //         'password' => 'required|string|min:8',
        //         'password_confirm' => 'required|string|min:8|same:password'
        //     ], [
        //         'password.required' => 'Password is required',
        //         'password.min:8' => 'Password must be 8 or more than character',
        //         'password_confirm.required' => 'Password confirm is required',
        //         'password_confirm.same:password' => 'Password confirm must match password'
        //     ]);
        // }

        //check for research areas
        $research_areas = [];
        if (isset($_POST['research_area_ids'])) {
            foreach ($_POST['research_area_ids'] as $research_area_id) {
                array_push($research_areas, $research_area_id);
            }
        }

        //create user to store data
        $user = User::query()->find($id);
        $user->first_name = $request->input('first_name');
        $user->middle_name = $request->input('middle_name');
        $user->surname = $request->input('surname');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->gender = $request->input('gender');
        $user->rank = $request->input('rank');
        // $user->level_id = $request->input('level_id');
        $user->college_id = $request->input('college_id');
        $user->department_id = $request->input('department_id');
        $user->institution = $request->input('institution');
        $user->academic_qualification = $request->input('academic_qualification');
        $user->specialization = $request->input('specialization');
        $user->experience = $request->input('experience');
        $user->research_area_ids = join(',', $research_areas);
        //$user->username = $request->input('username');

        // Check if a attachment has been uploaded
        if ($request->has('attachment')) {
            // Get image file
            $attachment = $request->file('attachment');

            // name
            $name = time() . '_' . $attachment->getFilename() . '.' . $attachment->getClientOriginalExtension();

            //Move Uploaded File
            $attachment->move('assets/uploads/cv/', $name);

            // Set user profile image path in database to filePath
            $user->cv = $name;
        }

        // if ($request->has('password'))
        //     $user->password = Hash::make($request->input('password'));

        $user->save(); //save user

        //remove role from role_user
        DB::table('role_user')->where('user_id', $id)->delete();

        //insert roles
        foreach ($request->input('role_ids') as $role_id) {
            $user->roles()->attach($role_id);
        }

        return Redirect::route('users.lists')->with('success', 'User updated'); //redirect
    }

    //delete user
    function destroy($id)
    {
        Perms::has_allowed('users', 'delete');
        //client details
        $user = User::query()->find($id);

        if (!$user)
            abort(404);

        //delete user
        $user->delete();

        return Redirect::route('users.lists')->with('danger', 'User deleted');
    }

    // //update user
    // function update_users()
    // {
    //     $users = DB::table('users')
    //         ->select(DB::raw('username, count(username) as c'))
    //         ->orderBy('username', 'asc')
    //         ->groupBy('username')
    //         ->having('c', '>', 1)
    //         ->get();

    //     foreach ($users as $user) {
    //         $sub_users = User::where('username', $user->username)->get();
    //         foreach ($sub_users as $val) {
    //             $us = User::find($val->id);
    //             $us->username = strtolower($us->first_name . '.' . $us->surname);
    //             $us->save();
    //         }
    //     }

    //     echo 'success';
    // }
}
