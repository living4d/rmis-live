<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Perms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

use App\Role;
use App\Perm;

class RolesController extends Controller
{
    private $data;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index()
    {
        $this->data['title'] = 'Roles';
        Perms::has_allowed('roles', 'lists');

        //roles
        $roles = Role::all();
        $this->data['roles'] = $roles;

        return view('roles.lists', $this->data);
    }

    //register user
    function create()
    {
        $this->data['title'] = 'Create New';
        Perms::has_allowed('roles', 'create');

        return view('roles.create', $this->data);
    }

    //store users details
    function store(Request $request)
    {
        //validation
        $this->validate($request,
            [
                'name' => 'required|alpha_dash|string',
                'description' => 'required|string'
            ]);

        //create role to store data
        $role = new Role(
            [
                'name' => $request->input('name'),
                'description' => $request->input('description')
            ]
        );
        $role->save();

        return redirect('roles')->with('success', 'Role created'); //redirect
    }


    //edit
    function edit($id)
    {
        $this->data['title'] = 'Edit';
        Perms::has_allowed('roles', 'edit');

        //role
        $role = Role::findOrFail($id);
        $this->data['role'] = $role;

        return view('roles.edit', $this->data);
    }

    //update user
    public function update(Request $request, $id)
    {
        //validation
        $this->validate($request,
            [
                'name' => 'required|alpha_dash|string',
                'description' => 'required|string'
            ]);

        //create role to store data
        $role = Role::query()->find($id);
        $role->name = $request->input('name');
        $role->description = $request->input('description');

        $role->save(); //save

        return redirect('roles')->with('success', 'Role updated'); //redirect
    }

    //assign perms
    public function assign_perms($id)
    {
        $this->data['title'] = 'Assign Perms';
        Perms::has_allowed('roles', 'assign_perms');

        //role
        $role = Role::findOrFail($id);
        $this->data['role'] = $role;

        //all perms
        $perms = Perm::get_perms_all();
        $this->data['perms'] = $perms;

        //assigned perms
        $assigned_perms = [];
        $perm_role = DB::table("perm_roles")->where('role_id', $id)->first();
        ($perm_role) ? $methods = explode(',', $perm_role->methods) : $methods = [];

        foreach ($methods as $v) {
            array_push($assigned_perms, $v);
        }
        $this->data['assigned_perms'] = $assigned_perms;

        return view('roles.assign_perms', $this->data);
    }

    //store perms
    function store_perms(Request $request, $id)
    {
        $perms = $request->perms;
        $role_id = $id;

        if ($perms) {
            $classes = [];
            foreach ($perms as $perm) {
                //method
                $method = DB::table("perm_methods")->where('id', $perm)->first();

                if (!in_array($method->class_id, $classes))
                    array_push($classes, $method->class_id);
            }

            //perm role
            $perm_role = DB::table("perm_roles")->where('role_id', $role_id)->first();

            if ($perm_role) {
                $classes = join(',', $classes);
                $methods = join(',', $perms);

                DB::update("UPDATE perm_roles SET classes='$classes', methods='$methods' WHERE role_id=:role_id", ['role_id' => $role_id]);
            } else {
                DB::insert("INSERT INTO perm_roles (role_id, classes, methods) VALUES (?,?,?)", [$role_id, join(',', $classes), join(',', $perms)]);
            }

            return redirect('roles/assign_perms/' . $role_id)->with('success', 'Permission updated');
        } else {
            return redirect('roles/assign_perms/' . $role_id)->with('failed', 'Failed to update perms');
        }
    }
}
