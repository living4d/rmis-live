<?php

/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 08/04/2020
 * Time: 17:31
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Auth;
use Hash;
use App\User;

class DashboardController extends Controller
{
    private $data;

    //constructor
    public function __construct()
    {
        $this->middleware('auth');
    }

    //dashboard
    function index()
    {
        $this->data['title'] = 'Statistics';

        if (Auth::user()->first_login == 1) {
            return redirect('change-password');
        }

        return view('dashboard', $this->data);
    }

    //change password
    function change_password()
    {
        $this->data['title'] = 'Change Password';

        return view('change_password', $this->data);
    }

    /**
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store_password(Request $request)
    {
        $this->validate($request, [
            'current_password' => ['required', new MatchOldPassword()],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);

        //update user
        $user = User::find(auth()->user()->id);
        $user->password = Hash::make($request->input('new_password'));
        $user->save();

        //update first login status
        User::where(['id' => Auth::user()->id])->update(['first_login' => 2]);

        return redirect('dashboard'); //redirect to dashboard
    }
}
