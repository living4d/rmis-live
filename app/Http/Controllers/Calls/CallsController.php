<?php

namespace App\Http\Controllers\Calls;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Settings\College;
use App\Models\Settings\Department;
use App\Models\Settings\Cluster;
use App\Models\Settings\ResearchArea;
use App\Models\Calls\Call;
use App\Models\Calls\CallApplication;
use App\Models\Calls\CallApplicationEvaluator;
use App\Models\Calls\CallApplicationObjective;
use App\Models\Calls\CallApplicationResult;
use App\Models\Calls\CallCategory;
use App\Models\Calls\CallCluster;
use App\Models\Calls\CallInvitation;

use App\User;
use App\Perms;
use App\Models\Notification;

use App\Traits\UploadTrait;

class CallsController extends Controller
{
    use UploadTrait;
    private $data;

    //constructor
    public function __construct()
    {
        $this->middleware('auth');
    }

    //stats
    public function stats()
    {
        $this->data['title'] = 'Calls Summary';
        Perms::has_allowed('calls', 'lists');

        //number of calls
        $this->data['number_of_calls'] = Call::count();

        //number of applications
        $this->data['number_of_applications'] = CallApplication::number_of_complete_applications();

        //won application
        $this->data['number_of_won_applications'] = CallApplication::number_of_won_applications();


        //render view
        return view('calls.stats', $this->data);
    }

    //lists
    public function index()
    {
        $this->data['title'] = 'Calls';
        Perms::has_allowed('calls', 'lists');

        //calls
        $calls = Call::orderBy('created_at', 'DESC')->get();
        $this->data['calls'] = $calls;

        foreach ($calls as $k => $call) {
            //my applications
            $my_applications = CallApplication::where(['call_id' => $call->id, 'principal_investigator' => Perms::get_current_user_id()])->count();
            $this->data['calls'][$k]->my_applications = $my_applications;

            //staff applications
            $this->data['calls'][$k]->applications = CallApplication::count_all($call->id);
        }

        //render view
        return view('calls.lists', $this->data);
    }

    //create
    public function create()
    {
        Perms::has_allowed('calls', 'create');

        $this->data['title'] = 'Register';

        //clusters
        $clusters = Cluster::all('id', 'name');
        $this->data['clusters'] = $clusters;

        if ($this->data['clusters']) {
            foreach ($this->data['clusters'] as $k => $v) {
                $this->data['clusters'][$k]->research_areas = ResearchArea::where('cluster_id', $v->id)->get();
            }
        }

        //program coordinator
        $this->data['program_coordinators'] = User::select('users.id', 'users.first_name', 'users.middle_name', 'users.surname')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where(['roles.name' => 'program_coordinator'])
            ->orderBy('users.first_name')
            ->get();


        //program accountant
        $this->data['program_accountants'] = User::select('users.id', 'users.first_name', 'users.middle_name', 'users.surname')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where(['roles.name' => 'program_accountant'])
            ->orderBy('users.first_name')
            ->get();

        return view('calls.create', $this->data);
    }

    //store
    public function store(Request $request)
    {
        //validation
        $this->validate($request, Call::rules(), Call::messages());

        //deadline
        $deadline = $request->input('date') . ' ' . $request->input('time');

        //create object to store data
        $call = new Call([
            'title' => $request->input('name'),
            'introduction' => $request->input('introduction'),
            'duration' => $request->input('duration'),
            'deadline' => $deadline,
            'team_composition' => $request->input('team_composition'),
            'eligibility' => $request->input('eligibility'),
            'deliverable' => $request->input('deliverable'),
            'evaluation_criteria' => $request->input('evaluation_criteria'),
            'application_details' => $request->input('application_details'),
            'program_coordinator' => $request->input('program_coordinator'),
            'program_accountant' => $request->input('program_accountant'),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => Perms::get_current_user_id()
        ]);

        if ($call->save()) {
            //call category
            if (isset($_POST['call_category'])) {
                for ($i = 0; $i < sizeof($_POST['call_category']); $i++) {
                    if ($_POST['call_category'][$i] != '') {
                        //budget ceiling
                        $budget_ceiling = floatval(str_replace(",", "", $_POST['budget_ceiling'][$i]));

                        $call_category = new CallCategory(
                            [
                                'call_id' => $call->id,
                                'title' => $_POST['call_category'][$i],
                                'budget_ceiling' => $budget_ceiling,
                                // 'description' => $_POST['call_category_desc'][$i]
                                'description' => $_POST['call_category_desc']
                            ]
                        );
                        $call_category->save();
                    }
                }
            }

            //clusters
            if (isset($_POST['cluster_ids'])) {
                foreach ($_POST['cluster_ids'] as $cluster_id) {
                    //insert into call research area
                    $call_cluster = new CallCluster([
                        'call_id' => $call->id,
                        'cluster_id' => $cluster_id
                    ]);
                    $call_cluster->save(); //save
                }
            }
            return redirect('calls')->with('success', 'New call created');
        }

        return redirect('calls')->with('danger', 'Failed to create new call');
    }

    //details
    public function show($id)
    {
        Perms::has_allowed('calls', 'show');
        $this->data['title'] = 'Call Information';

        //call
        $call = Call::findOrFail($id);
        $this->data['call'] = $call;

        //render view
        return view('calls.show', $this->data);
    }

    //invitations
    public function invitations($id)
    {
        //Perms::has_allowed('calls', 'show');
        $this->data['title'] = 'Call Invitations';

        //call
        $call = Call::findOrFail($id);
        $this->data['call'] = $call;

        //render view
        return view('calls.invitations', $this->data);
    }

    //edit
    public function invite($id)
    {
        $this->data['title'] = 'Invite Staff';
        Perms::has_allowed('calls', 'invite');

        //call
        $call = Call::findOrFail($id);
        $this->data['call'] = $call;

        //colleges
        $colleges = College::all();

        $this->data['colleges'] = $colleges;

        foreach ($this->data['colleges'] as $k => $v) {
            $departments = Department::where('college_id', $v->id)->get();
            $this->data['colleges'][$k]->departments = $departments;
        }

        return view('calls.invite', $this->data);
    }

    //store all invited users
    public function store_invite(Request $request, $id)
    {
        //call
        $call = Call::findOrFail($id);

        //validation
        $this->validate(
            $request,
            ['college_ids' => 'required'],
            ['college_ids.required' => 'College is required']
        );

        //message
        $message = '<b>' . $call->title . '</b> has been published, to apply for this call please click the link below.';
        $message .= '<br/><a href="' . url('calls/' . $call->id) . '">' . url('calls/' . $call->id) . '</a>';
        $message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

        //college
        $college_ids = $request->college_ids;
        $department_ids = $request->department_ids;
        $array = ['college_ids' => $college_ids, 'department_ids' => $department_ids];

        //recipients
        $recipients = Notification::get_recipients('invitation', $array);

        if ($recipients) {
            //insert or update call invitation
            foreach ($recipients as $val) {
                //check if exist
                $call_inv = CallInvitation::where(['call_id' => $id, 'user_id' => $val])->first();

                if ($call_inv) {
                    $call_inv->iteration = $call_inv->iteration + 1;
                    $call_inv->save();
                } else {
                    $new = new CallInvitation();
                    $new->call_id = $id;
                    $new->user_id = $val;
                    $new->iteration = 1;
                    $new->created_by = Perms::get_current_user_id();
                    $new->created_at = date('Y-m-d H:i:s');
                    $new->save();
                }
            }

            //send notification
            $message_id = Notification::insert_notifications($recipients, $message);

            //redirect with message
            return redirect(route('calls.invitations', $id))->with('success', 'Invitations notification sent');
        }

//redirect with message
        return redirect(route('calls.invitations', $id))->with('danger', 'Failed to send invitations notification');
    }

    //evaluators
    function evaluators($id)
    {
        $this->data['title'] = 'Call Evaluators';
        //Perms::has_allowed('calls', 'invite');

        //call
        $call = Call::findOrFail($id);
        $this->data['call'] = $call;

        //evaluators
        $this->data['users'] = User::select('users.id', 'users.first_name', 'users.middle_name', 'users.surname')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where(['roles.name' => 'evaluators'])
            ->orderBy('users.first_name')
            ->get();

        //render view
        return view('calls.evaluators', $this->data);
    }

    //applications
    function applications(Request $request, $id)
    {
        $this->data['title'] = 'Call Applications';
        Perms::has_allowed('calls', 'applications');

        //call
        $call = Call::findOrFail($id);
        $this->data['call'] = $call;

        //if post        
        if (isset($_POST['filter'])) {
            $department_id = $request->input('department_id');
            $cluster_id = $request->input('cluster_id');
            $keyword = $request->input('keyword');
            $start_at = $request->input('start_at');
            $end_at = $request->input('end_at');

            $this->data['call_id'] = $id;
            $this->data['department_id'] = $department_id;
            $this->data['cluster_id'] = $cluster_id;
            $this->data['keyword'] = $keyword;
            $this->data['start_at'] = $start_at;
            $this->data['end_at'] = $end_at;

            //count apps
            $this->data['count_apps'] = CallApplication::count_all($id, $department_id, $cluster_id, $start_at, $end_at);

            //get all apps
            $this->data['call_apps'] = CallApplication::get_all($id, $department_id, $cluster_id, $start_at, $end_at);
        } else {
            $this->data['call_id'] = $id;
            $this->data['department_id'] = null;
            $this->data['cluster_id'] = null;
            $this->data['keyword'] = null;
            $this->data['start_at'] = null;
            $this->data['end_at'] = null;

            //count apps
            $this->data['count_apps'] = CallApplication::count_all($id);

            //get all apps
            $this->data['call_apps'] = CallApplication::get_all($id);
        }

        foreach ($this->data['call_apps'] as $k => $val) {
            $this->data['call_apps'][$k]->call_evaluation_result = CallApplicationResult::where('application_id', '=', $val->call_app_id)->first();
            $this->data['call_apps'][$k]->evaluators = CallApplicationEvaluator::where('application_id', '=', $val->call_app_id)->get();
            $this->data['call_apps'][$k]->app_budgets = CallApplicationObjective::where('application_id', '=', $val->call_app_id)->sum('budget');
        }

        //department
        $departments = Department::where_cond();
        $this->data['departments'] = $departments->get();

        //clusters
        $call_clusters = CallCluster::where('call_id', '=', $id)->get();
        $this->data['call_clusters'] = $call_clusters;

        //render view
        return view('calls.applications', $this->data);
    }

    //winners
    public function winners($id)
    {
        $this->data['title'] = 'Winners';
        //Perms::has_allowed('calls', 'invite');

        //call
        $call = Call::findOrFail($id);
        $this->data['call'] = $call;

        //render view
        return view('calls.winners', $this->data);
    }


    //show applications
    public function show_apps($id)
    {
        //call
        $call = Call::find($id);

        if (!$call) {
            abort(404);
        }

        $this->data['call'] = $call;

        //call applications
        $call_applications = CallApplication::where_cond();
        $call_applications = $call_applications
            ->select('call_applications.id', 'call_applications.principal_investigator', 'call_applications.project_title', 'call_applications.created_at')
            ->where(['call_applications.call_id' => $id, 'call_applications.status' => 'COMPLETE'])->get();
        $this->data['call_applications'] = $call_applications;


        //render view
        return view('calls.show_apps', $this->data);
    }

    //show winners
    function show_winners($id)
    {
        //call
        $call = Call::find($id);

        if (!$call) {
            abort(404);
        }

        $this->data['call'] = $call;

        //render views
        return view('calls.show_winners', $this->data);
    }

    //edit
    public function edit($id)
    {
        $this->data['title'] = 'Edit';
        Perms::has_allowed('calls', 'edit');

        //call
        $call = Call::findOrFail($id);
        $this->data['call'] = $call;

        //clusters
        $clusters = Cluster::all('id', 'name');
        $this->data['clusters'] = $clusters;

        if ($this->data['clusters']) {
            foreach ($this->data['clusters'] as $k => $v) {
                $this->data['clusters'][$k]->research_areas = ResearchArea::where('cluster_id', $v->id)->get();
            }
        }

        //assigned categories
        $call_categories = CallCategory::where('call_id', $id)->get();
        $this->data['call_categories'] = $call_categories;

        //assigned cluster ids
        $call_clusters = CallCluster::where(['call_id' =>  $id])->get();

        $arr_cluster_ids = [];
        if ($call_clusters) {
            foreach ($call_clusters as $val) {
                array_push($arr_cluster_ids, $val->cluster_id);
            }
        }
        $this->data['arr_cluster_ids'] = $arr_cluster_ids;

        //program coordinator
        $this->data['program_coordinators'] = User::select('users.id', 'users.first_name', 'users.middle_name', 'users.surname')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where(['roles.name' => 'program_coordinator'])
            ->orderBy('users.first_name')
            ->get();


        //program accountant
        $this->data['program_accountants'] = User::select('users.id', 'users.first_name', 'users.middle_name', 'users.surname')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where(['roles.name' => 'program_accountant'])
            ->orderBy('users.first_name')
            ->get();

        //render view
        return view('calls.edit', $this->data);
    }

    //update
    public function update(Request $request, $id)
    {
        //validation
        $this->validate($request, Call::rules(), Call::messages());

        //deadline
        $deadline = $request->input('date') . ' ' . $request->input('time');

        //create object to store data
        $call = Call::query()->find($id);
        $call->title = $request->input('name');
        $call->introduction = $request->input('introduction');
        $call->duration = $request->input('duration');
        $call->deadline = $deadline;
        $call->team_composition = $request->input('team_composition');
        $call->eligibility = $request->input('eligibility');
        $call->deliverable = $request->input('deliverable');
        $call->evaluation_criteria = $request->input('evaluation_criteria');
        $call->application_details = $request->input('application_details');
        $call->program_coordinator = $request->input('program_coordinator');
        $call->program_accountant = $request->input('program_accountant');

        if ($call->save()) {
            //insert call call category
            if (isset($_POST['call_category'])) {
                for ($i = 0; $i < sizeof($_POST['call_category']); $i++) {
                    if ($_POST['call_category'][$i] != '') {
                        //budget ceiling
                        $budget_ceiling = floatval(str_replace(",", "", $_POST['budget_ceiling'][$i]));

                        //check if call category exists
                        $call_category = CallCategory::where(['call_id' => $id, 'title' => $_POST['call_category'][$i]])->first();

                        if (!$call_category) {
                            //insert new call category
                            $call_category = new CallCategory(
                                [
                                    'call_id' => $call->id,
                                    'title' => $_POST['call_category'][$i],
                                    'budget_ceiling' => $budget_ceiling,
                                    'description' => $_POST['call_category_desc'][$i]
                                ]
                            );
                            $call_category->save();
                        } else {
                            //update call category
                            $call_category->title = $_POST['call_category'][$i];
                            $call_category->budget_ceiling = $budget_ceiling;
                            $call_category->description = $_POST['call_category_desc'][$i];
                            $call_category->save();
                        }
                    }
                }
            }

            //delete call clusters
            CallCluster::where(['call_id' => $id])->delete();

            //clusters
            if (isset($_POST['cluster_ids'])) {
                foreach ($_POST['cluster_ids'] as $cluster_id) {
                    //insert into call research area
                    $call_cluster = new CallCluster([
                        'call_id' => $call->id,
                        'cluster_id' => $cluster_id
                    ]);
                    $call_cluster->save(); //save
                }
            }

            //update
            return redirect('calls/' . $id . '#information')->with('success', 'Call updated');
        }

        return redirect('calls/' . $id . '#information')->with('danger', 'Failed to update call');
    }

    //delete
    public function destroy($id)
    {
        $this->data['title'] = 'Delete';
        Perms::has_allowed('calls', 'delete');

        //call
        $call = Call::findOrFail($id);

        //delete
        $call->delete($id);

        //delete call application
        CallApplication::where(['call_id' => $id])->delete();

        //delete call category
        CallCategory::where(['call_id' => $id])->delete();

        //delete call cluster
        CallCluster::where(['call_id' => $id])->delete();

        //delete call invitation
        CallInvitation::where(['call_id' => $id])->delete();

        //call application
        CallApplication::where(['call_id' => $id])->delete();

        return redirect('calls')->with('danger', 'Call deleted');
    }

    //close app
    public function close($id, $action)
    {
        //call
        $call = Call::findOrFail($id);

        if ($action === 'close') {
            $call->status = 'closed';
        }
        elseif ($action = 'cancel') {
            $call->status = 'cancelled';
        }

        //save 
        $call->save();

        //redirect
        if ($action === 'close') {
            return redirect('calls')->with('danger', 'Call closed');
        }

        if ($action = 'cancel') {
            return redirect('calls')->with('danger', 'Call cancelled');
        }
    }

    /*==================API FUNCTIONS =======================*/
    //assign evaluators
    public function assign_evaluators(Request $request, $call_id): void
    {
        $cluster_id = $request->input('cluster_id');
        $evaluator_ids = $request->input('evaluators');

        //get application from cluster_id
        $apps = CallApplication::where(['call_id' => $call_id, 'call_cluster_id' => $cluster_id])->get();

        if ($apps) {
            foreach ($apps as $app) {
                foreach ($evaluator_ids as $evaluator_id) {
                    //todo : check if evaluator exists ??
                    $call_app_evaluator = CallApplicationEvaluator::where(['application_id' => $app->id, 'evaluator_id' => $evaluator_id])->first();

                    if ($call_app_evaluator) {
                        //do nothing
                    } else {
                        //insert
                        $call_evaluator = new CallApplicationEvaluator([
                            'call_id' => $call_id,
                            'application_id' => $app->id,
                            'evaluator_id' => $evaluator_id,
                            'created_by' => Perms::get_current_user_id(),
                            'created_at' => date('Y-m-d H:i:s')
                        ]);
                        $call_evaluator->save(); //save
                    }
                }
            }
            //response
            $response['status'] = 'success';
        } else {
            //failed
            $response['status'] = 'failed';
        }
        echo json_encode($response);
    }

    //drop evaluators
    public function drop_evaluators(Request $request, $call_id): void
    {
        $cluster_id = $request->input('cluster_id');
        $evaluator_ids = $request->input('evaluators');

        //get application from cluster_id
        $apps = CallApplication::where(['call_id' => $call_id, 'call_cluster_id' => $cluster_id])->get();

        if ($apps) {
            foreach ($apps as $app) {
                foreach ($evaluator_ids as $evaluator_id) {
                    //todo : check if evaluator exists ??
                    $delete = CallApplicationEvaluator::where(['application_id' => $app->id, 'evaluator_id' => $evaluator_id])->delete();
                }
            }
            //response
            $response['status'] = 'success';
        } else {
            //failed
            $response['status'] = 'failed';
        }
        echo json_encode($response);
    }

    //get application evaluators
    public function assigned_evaluators($call_id, $cluster_id): void
    {
        $apps = CallApplication::where(['call_id' => $call_id, 'call_cluster_id' => $cluster_id])->get();

        $evaluators = [];
        if ($apps) {
            foreach ($apps as $app) {
                //find application evaluators
                $app_evaluators = CallApplicationEvaluator::select('evaluator_id')
                    ->where('application_id', '=', $app->id)
                    ->get();

                foreach ($app_evaluators as $val) {
                    if (!in_array($val->evaluator_id, $evaluators)) {
                        array_push($evaluators, $val->evaluator_id);
                    }
                }
            }
        }

        //display evaluators in html
        if ($evaluators) {
            $evaluatorArr = [];
            foreach ($evaluators as $val) {
                $user = User::find($val);

                $evaluatorArr[] =
                    [
                        'id' => $user->id,
                        'name' => $user->first_name . ' ' . substr($user->middle_name, 1) . ' ' . $user->surname
                    ];
            }
            //response
            $response['status'] = 'success';
            $response['evaluators'] = $evaluatorArr;
        } else {
            $response['status'] = 'failed';
        }
        echo json_encode($response);
    }

    //cluster from call
    public function get_clusters_from_call($call_id): void
    {
        $call_clusters = CallCluster::where('call_id', '=', $call_id)->get();

        if ($call_clusters) {
            echo '<option value="">Clusters</option>';
            foreach ($call_clusters as $val) {
                echo '<option value="' . $val->cluster_id . '">' . $val->cluster->name . '</option>';
            }
        } else {
            echo '<option value="">-- No clusters --</option>';
        }
    }
}
