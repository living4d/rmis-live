<?php

namespace App\Http\Controllers\Calls;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

use App\Models\Settings\College;
use App\Models\Settings\Department;
use App\Models\Settings\Cluster;
use App\Models\Settings\ResearchArea;
use App\Models\Settings\ResearchType;
use App\Models\Calls\Call;
use App\Models\Calls\CallCategory;
use App\Models\Calls\CallCluster;
use App\Models\Calls\CallApplication;
use App\Models\Calls\CallApplicationPartner;
use App\Models\Calls\CallApplicationApproval;
use App\Models\Calls\CallApplicationObjective;
use App\Models\Calls\CallApplicationEvaluator;
use App\Models\Calls\CallApplicationEvaluation;
use App\Models\Calls\CallApplicationResult;
use App\Models\Projects\Project;
use App\Models\Researcher;
use App\Models\Student;
use App\Models\Notification;
use Auth;
use App\User;
use App\Perms;
use App\Rules\MaxWordsRule;
use App\Traits\UploadTrait;
use Redirect;

class CallApplicationsController extends Controller
{
    use UploadTrait;
    private $data;

    //constructor
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**===========APPLICATION FOR A CALL =================**/
    //check if uncompleted application
    public function apply($id)
    {
        //call
        Perms::has_allowed('call_applications', 'apply');
        $call = Call::findOrFail($id);

        if ($call->status === 'closed') {
            return redirect('calls')->with('danger', 'Call already closed, you can not apply it');
        }

        //user
        $user_id = Perms::get_current_user_id();
//
//        //logged user
//        $user = User::findOrFail($user_id);

        //check if uncompleted application
        $call_app = CallApplication::where(['call_id' => $id, 'created_by' => $user_id, 'status' => 'INCOMPLETE'])->first();

        if ($call_app) {
            if ($call_app->completion == 0) {
                return Redirect::route('applications.step-1', [$id, $call_app->id])->with('info', 'Continue with call application');
            }
            if ($call_app->completion == 1 && $call_app->application_grant_category === 'innovation_grant') {
                return Redirect::route('applications.innov_step-2', [$id, $call_app->id])->with('info', 'Continue with call application innovation category');
            }

            if ($call_app->completion == 1 && $call_app->application_grant_category !== 'innovation_grant') {
                return Redirect::route('applications.step-2', [$id, $call_app->id])->with('info', 'Continue with call application');
            }

            if ($call_app->completion == 2) {
                return Redirect::route('applications.step-3', [$id, $call_app->id])->with('info', 'Continue with call application');
            }

            if ($call_app->completion == 3) {
                return Redirect::route('applications.preview', [$call_app->id])->with('success', 'Preview and finish call application');
            }
        } else {
            return Redirect::route('applications.step-1', $id);
        }
    }

    //apply for proposal
    function apply_step1($id, $app_id = null)
    {
        Perms::has_allowed('call_applications', 'apply');
        $this->data['title'] = 'Apply For Call';

        //call
        $call = Call::findOrFail($id);

        if ($call->status === 'closed') {
            return redirect('calls')->with('danger', 'Call already closed, you can not apply it');
        }

        $this->data['call'] = $call;

        //logged user
        $user = User::findOrFail(Auth::user()->id);
        $this->data['user'] = $user;

        //call application
        if ($app_id != null) {
            //app_id
            $this->data['app_id'] = $app_id;

            //call application
            $call_app = CallApplication::findOrFail($app_id);
            $this->data['call_app'] = $call_app;

            //assigned researchers (internal)
            $internal_partners = CallApplicationPartner::where(['call_id' => $id, 'application_id' => $app_id, 'partner_type' => 'INTERNAL'])
                ->pluck('partner_id')->toArray();

            $this->data['arr_internal_partners'] = $internal_partners;

            //assigned researcher (students)
            $student_partners = CallApplicationPartner::where(['call_id' => $id, 'application_id' => $app_id, 'partner_type' => 'STUDENT'])
                ->pluck('partner_id')->toArray();

            $this->data['arr_student_partners'] = $student_partners;

            //assigned researcher (external)
            $external_partners = CallApplicationPartner::where(['call_id' => $id, 'application_id' => $app_id, 'partner_type' => 'EXTERNAL'])
                ->pluck('partner_id')->toArray();

            $this->data['arr_external_partners'] = $external_partners;
        } else {
            //app_id
            $this->data['app_id'] = null;

            //call app
            $this->data['call_app'] = [];
            $this->data['arr_internal_partners'] = [];
            $this->data['arr_student_partners'] = [];
            $this->data['arr_external_partners'] = [];
        }

        //college
        $college = College::find($user->college_id);
        $this->data['college'] = $college;

        //department
        $department = Department::find($user->department_id);
        $this->data['department'] = $department;

        //faculty
        $this->data['faculty'] = [];

        //select only staff
        $this->data['users'] = User::select('users.id', 'users.first_name', 'users.middle_name', 'users.surname')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where(['roles.name' => 'staff'])
            ->orderBy('users.first_name')
            ->get();

        //external researchers
        $researchers = Researcher::all();
        $this->data['researchers'] = $researchers;

        //students
        $students = Student::all();
        $this->data['students'] = $students;

        return view('call_applications.apply_step1', $this->data);
    }

    //save app step one
    function save_step1(Request $request, $id, $app_id = null)
    {
        //validation
        $this->validate(
            $request,
            ['project_title' => 'required|string', 'application_grant_category' => 'required|string'],
            ['project_title.required' => 'Project title is required', 'application_grant_category.required' => 'Application grant category must be selected'],
        );

        //call
//        $call = Call::find($id);

        //call app_id
//        $call_app_id = ''

        //check for completion
        if (isset($_POST['save'])) {
            $completion = 0;
        }
        else if (isset($_POST['continue'])) {
            $completion = 1;
        }

        //check call application
        if ($app_id != null) {
            $call_app = CallApplication::find($app_id);

            if ($call_app) {
                $call_app->project_title = $request->input('project_title');
                $call_app->completion = $completion;
                $call_app->created_at = date('Y-m-d H:i:s');
                $call_app->created_by = Perms::get_current_user_id();
                $call_app->application_grant_category = $request->input('application_grant_category');
                $call_app->save();

                //call app id
                $call_app_id = $app_id;
            } else {
                $call_app = new CallApplication([
                    'call_id' => $id,
                    'principal_investigator' => $request->input('user_id'),
                    'project_title' => $request->input('project_title'),
                    'completion' => $completion,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => Perms::get_current_user_id(),
                    'application_grant_category' => $request->input('application_grant_category')
                ]);
                $call_app->save();

                //call app id
                $call_app_id = $call_app->id;
            }
        } else {
            $call_app = new CallApplication([
                'call_id' => $id,
                'principal_investigator' => $request->input('user_id'),
                'project_title' => $request->input('project_title'),
                'completion' => $completion,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => Perms::get_current_user_id(),
                'application_grant_category' => $request->input('application_grant_category')
            ]);
            $call_app->save();

            //call app id
            $call_app_id = $call_app->id;
        }

        //insert internal partners
        if (isset($_POST['partner_ids'])) {
            //delete partner if exists
            CallApplicationPartner::where(['call_id' => $id, 'application_id' => $call_app_id, 'partner_type' => 'INTERNAL'])->delete();

            //iterate
            foreach ($_POST['partner_ids'] as $partner_id) {
                $user = User::find($partner_id);

                //insert partner
                $call_partner = new CallApplicationPartner([
                    'call_id' => $id,
                    'application_id' => $call_app_id,
                    'partner_id' => $partner_id,
                    'partner_phone' => $user->phone,
                    'partner_email' => $user->email,
                    'partner_type' => 'INTERNAL'
                ]);
                $call_partner->save();
            }
        }

        //insert partner students
        if (isset($_POST['student_ids'])) {
            //delete partner if exists
            CallApplicationPartner::where(['call_id' => $id, 'application_id' => $call_app_id, 'partner_type' => 'STUDENT'])->delete();

            //iterate 
            foreach ($_POST['student_ids'] as $student_id) {
                $student = Student::find($student_id);

                //insert partner
                $call_partner = new CallApplicationPartner([
                    'call_id' => $id,
                    'application_id' => $call_app_id,
                    'partner_id' => $student_id,
                    'partner_phone' => $student->phone,
                    'partner_email' => $student->email,
                    'partner_type' => 'STUDENT'
                ]);
                $call_partner->save();
            }
        }

        //insert external
        //1. insert through dropdown
        if (isset($_POST['researcher_ids'])) {
            //delete partner if exists
            CallApplicationPartner::where(['call_id' => $id, 'application_id' => $call_app_id, 'partner_type' => 'EXTERNAL'])->delete();

            //iterate
            foreach ($_POST['researcher_ids'] as $researcher_id) {
                $researcher = Researcher::find($researcher_id);

                //insert partner
                $call_partner = new CallApplicationPartner([
                    'call_id' => $id,
                    'application_id' => $call_app_id,
                    'partner_id' => $researcher_id,
                    'partner_phone' => $researcher->phone,
                    'partner_email' => $researcher->email,
                    'partner_type' => 'EXTERNAL'
                ]);
                $call_partner->save();
            }
        }

        //2. insert through add more
        if (isset($_POST['full_name']) && $_POST['email']) {
            for ($j = 0; $j < sizeof($_POST['full_name']); $j++) {
                $researcher = new Researcher([
                    'full_name' => $_POST['full_name'][$j],
                    'phone' => $_POST['phone'][$j],
                    'email' => $_POST['email'][$j],
                    'created_at' => date('Y-m-d H:i:s')
                ]);
                $researcher->save();

                //insert application partner
                $call_partner = new CallApplicationPartner([
                    'call_id' => $id,
                    'application_id' => $call_app_id,
                    'partner_id' => $researcher->id,
                    'partner_type' => 'EXTERNAL'
                ]);
                $call_partner->save();
            }
        }

        //redirect
        if (isset($_POST['save'])) {
            return redirect('calls')->with('success', 'Call application saved, continue later');
        }

        if (isset($_POST['continue'])) {
            if ($call_app->application_grant_category === 'innovation_grant') {
                return redirect(route('applications.innov_step-2', [$id, $call_app_id]))->with('success', 'Call application saved, continue with application innovation category');
            }

            if ($call_app->application_grant_category === 'research_grant') {
                return redirect(route('applications.step-2', [$id, $call_app_id]))->with('success', 'Call application saved, continue with application');
            }
        }
        return redirect(route('applications.step-2', [$id, $call_app_id]))->with('success', 'Call application saved, continue with application');
    }

    //apply step two
    function apply_step2($id, $app_id)
    {
        Perms::has_allowed('call_applications', 'apply');
        $this->data['title'] = 'Apply For Call';

        //call
        $call = Call::findOrFail($id);

        if ($call->status === 'closed') {
            return redirect('calls')->with('danger', 'Call already closed, you can not apply it');
        }

        $this->data['call'] = $call;

        //app_id
        $call_app = CallApplication::findOrFail($app_id);
        $this->data['call_app'] = $call_app;

        //logged user
        $user = User::find(Perms::get_current_user_id());

        if (!$user) {
            abort(400, 'User does not exit');
        }

        $this->data['user'] = $user;

        //call categories
        $call_categories = CallCategory::where('call_id', $id)->get();
        $this->data['call_categories'] = $call_categories;

        //research types
        $this->data['research_types'] = ResearchType::all();

        //clusters
        $clusters = CallCluster::where('call_id', $id)->get();

        if ($clusters) {
            $this->data['clusters'] = $clusters;
            $arr_clusters = [];
            foreach ($this->data['clusters'] as $k => $cl) {
                $arr_clusters[] = $cl->cluster_id;
                $this->data['clusters'][$k]->cluster = Cluster::find($cl->cluster_id);
            }
            //research areas
            $research_areas = ResearchArea::whereIn('cluster_id', $arr_clusters)->get();
            $this->data['research_areas'] = $research_areas;
        }

        //specific objective
        $objectives = CallApplicationObjective::where('application_id', '=', $app_id)->get();
        $this->data['objectives'] = $objectives;

        return view('call_applications.apply_step2', $this->data);
    }

    //save application step 2
    function save_step2(Request $request, $id, $app_id)
    {
        //validation
        if (isset($_POST['continue'])) {
            $this->validate($request, CallApplication::rules(), CallApplication::messages());
        }

        //call application
        $call_application = CallApplication::find($app_id);
        $call_application->call_category_id = $request->input('call_category_id');
        // $call_application->research_type_id = $request->input('research_type_id');
        $call_application->research_type_id = 3;
        $call_application->call_cluster_id = $request->input('cluster_id');
        $call_application->call_research_area_id = $request->input('research_area_id');
        $call_application->summary = $request->input('summary');
        $call_application->introduction = $request->input('introduction');
        $call_application->problem_statement = $request->input('problem_statement');
        $call_application->rationale = $request->input('rationale');
        $call_application->general_objective = $request->input('general_objective');
        $call_application->literature_review = $request->input('literature_review');
        $call_application->methodology = $request->input('methodology');
        $call_application->research_output = $request->input('research_output');
        $call_application->sustainability_measures = $request->input('sustainability_measures');
        $call_application->management_structure = $request->input('management_structure');
        $call_application->implementation_plan = $request->input('implementation_plan');

        //check for completion
        if (isset($_POST['save'])) {
            $call_application->completion = 1;
        }
        elseif (isset($_POST['continue'])) {
            $call_application->completion = 2;
        }

        //save
        if ($call_application->save()) {
            //todo: insert objectives
            if (isset($_POST['specific_objective'])) {
                //delete objectives if exists
                CallApplicationObjective::where(['call_id' => $id, 'application_id' => $app_id])->delete();

                //process for insertion
                for ($h = 0; $h < sizeof($_POST['specific_objective']); $h++) {
                    $app_objective = new CallApplicationObjective(
                        [
                            'call_id' => $id,
                            'application_id' => $call_application->id,
                            'objective' => $_POST['specific_objective'][$h]
                        ]
                    );
                    $app_objective->save();
                }
            }
            //redirect
            if (isset($_POST['save'])) {
                return redirect('calls')->with('success', 'Call application saved, continue later');
            }

            if (isset($_POST['continue'])) {
                return redirect()->route('applications.step-3', [$id, $app_id])->with('success', 'Call application saved');
            }
        } else {
            return redirect()->route('applications.step-2', [$id, $app_id])->with('danger', 'Failed to save call application');
        }
    }


    //apply step two
    function apply_innov_step2($id, $app_id)
    {
        Perms::has_allowed('call_applications', 'apply');
        $this->data['title'] = 'Apply For Call';

        //call
        $call = Call::findOrFail($id);

        if ($call->status === 'closed') {
            return redirect('calls')->with('danger', 'Call already closed, you can not apply it');
        }

        $this->data['call'] = $call;

        //app_id
        $call_app = CallApplication::findOrFail($app_id);
        $this->data['call_app'] = $call_app;

        //logged user
        $user = User::find(Perms::get_current_user_id());

        if (!$user) {
            abort(400, 'User does not exit');
        }

        $this->data['user'] = $user;

        //call categories
        $call_categories = CallCategory::where('call_id', $id)->get();
        $this->data['call_categories'] = $call_categories;

        //research types
        $this->data['research_types'] = ResearchType::all();

        //clusters
        $clusters = CallCluster::where('call_id', $id)->get();

        if ($clusters) {
            $this->data['clusters'] = $clusters;
            $arr_clusters = [];
            foreach ($this->data['clusters'] as $k => $cl) {
                $arr_clusters[] = $cl->cluster_id;
                $this->data['clusters'][$k]->cluster = Cluster::find($cl->cluster_id);
            }
            //research areas
            $research_areas = ResearchArea::whereIn('cluster_id', $arr_clusters)->get();
            $this->data['research_areas'] = $research_areas;
        }

        //specific objective
        $objectives = CallApplicationObjective::where('application_id', '=', $app_id)->get();
        $this->data['objectives'] = $objectives;

        return view('call_applications.innovation.apply_step2', $this->data);
    }

    //save application innovation step 2
    function save_innov_step2(Request $request, $id, $app_id)
    {
        //validation
        if (isset($_POST['continue'])){
            //validation
            $this->validate(
                $request,
                [
                    'call_category_id' => 'required',
                    'cluster_id' => 'required',
                    'research_area_id' => 'required',
                    'summary' => ['required', new MaxWordsRule(300)],
                    'introduction' => 'required|string',
                    'rationale' => 'required|string',
                    'general_objective' => 'required|string',
                    'specific_objective.*' => 'required',
                    'research_output' => 'required|string',
                    'sustainability_measures' => 'required|string',
                    'potential_benefit' => 'required|string',
                ],
                [
                    'cluster_id.required' => 'Cluster is required',
                    'research_area_id.required' => 'Research area is required',
                    'introduction.required' => 'Project Description & Purpose is required',
                    'rationale.required' => 'Intellectual Property Issues are required',
                    'general_objective.required' => 'Project Plan and Budget are required',
                    'research_output.required' => 'Product/Process/Service is required',
                    'sustainability_measures.required' => 'Project Risks and Mitigation Plan is required',
                    'potential_benefit.required' => 'Potential National Benefit of the Outcome of the Project is required',
                ]
            );
        }

        //call application
        $call_application = CallApplication::find($app_id);
        $call_application->call_category_id = $request->input('call_category_id');
        // $call_application->research_type_id = $request->input('research_type_id');
        $call_application->research_type_id = 4;
        $call_application->call_cluster_id = $request->input('cluster_id');
        $call_application->call_research_area_id = $request->input('research_area_id');
        $call_application->summary = $request->input('summary');
        $call_application->introduction = $request->input('introduction');
        // $call_application->problem_statement = $request->input('problem_statement');
        $call_application->problem_statement = '<p>N/A</p>';
        $call_application->rationale = $request->input('rationale');
        $call_application->general_objective = $request->input('general_objective');
        // $call_application->literature_review = $request->input('literature_review');
        $call_application->literature_review = '<p>N/A</p>';
        // $call_application->methodology = $request->input('methodology');
        $call_application->methodology = '<p>N/A</p>';
        $call_application->research_output = $request->input('research_output');
        $call_application->sustainability_measures = $request->input('sustainability_measures');
        // $call_application->management_structure = $request->input('management_structure');
        $call_application->management_structure = '<p>N/A</p>';
        $call_application->dissemination_plan = '<p>N/A</p>';
        $call_application->capability = '<p>N/A</p>';
        // $call_application->implementation_plan = $request->input('implementation_plan');
        $call_application->implementation_plan = '<p>N/A</p>';
        $call_application->potential_benefit = $request->input('potential_benefit');

        //check for completion
        if (isset($_POST['save'])) {
            $call_application->completion = 1;
            // dd('this is save stage 2');
        } else if (isset($_POST['continue'])) {
            $call_application->completion = 2;
            // dd('this is continue stage 2');
        }
        //save
        if ($call_application->save()) {
            //todo: insert objectives
            if (isset($_POST['specific_objective'])) {
                //delete objectives if exists
                CallApplicationObjective::where(['call_id' => $id, 'application_id' => $app_id])->delete();

                //process for insertion
                for ($h = 0; $h < sizeof($_POST['specific_objective']); $h++) {
                    $app_objective = new CallApplicationObjective(
                        [
                            'call_id' => $id,
                            'application_id' => $call_application->id,
                            'objective' => $_POST['specific_objective'][$h]
                        ]
                    );
                    $app_objective->save();
                }
            }
            //redirect
            if (isset($_POST['save'])) {
                return redirect('calls')->with('success', 'Call application saved, continue later');
            }

            if (isset($_POST['continue'])) {
                return redirect()->route('applications.step-3', [$id, $app_id])->with('success', 'Call application saved');
            }
        } else {
            return redirect()->route('applications.step-2', [$id, $app_id])->with('danger', 'Failed to save call application');
        }
    }

    //apply step 3
    function apply_step3($id, $app_id)
    {
        Perms::has_allowed('call_applications', 'apply');
        $this->data['title'] = 'Apply For Call';

        //call
        $call = Call::findOrFail($id);

        if ($call->status === 'closed') {
            return redirect('calls')->with('danger', 'Call already closed, you can not apply it');
        }

        $this->data['call'] = $call;

        //app_id
        $call_app = CallApplication::findOrFail($app_id);
        $this->data['call_app'] = $call_app;

        //logged user
        $user = User::find(Auth::user()->id);
        $this->data['user'] = $user;

        //call category
        $this->data['call_category'] = CallCategory::find($call_app->call_category_id);

        //render view
        return view('call_applications.apply_step3', $this->data);
    }

    //save application step 3
    function save_step3(Request $request, $id, $app_id)
    {
        //call
        $call = Call::findOrFail($id);

        //app_id
        $call_app = CallApplication::findOrFail($app_id);

        //call category
        $call_category = CallCategory::find($call_app->call_category_id);

        //validate amount entered
        if (isset($_POST['amount'])) {
            for ($i = 0; $i < sizeof($_POST['amount']); $i++) {
                //check if amount is null
                if ($_POST['amount'][$i] != null) {
                    $amount = floatval(str_replace(",", "", $_POST['amount'][$i]));
                    //check if numeric
                    if (!is_numeric($amount)) {
                        return redirect('applications.step-3', [$id, $app_id])->with('danger', 'Amount in row ' . $i . ' should be numeric');
                        //exit();
                    }
                }
            }
        }

        //get summation of budget
        $sum = 0;
        if (isset($_POST['amount'])) {
            for ($i = 0; $i < sizeof($_POST['amount']); $i++) {
                //check if amount is null
                if ($_POST['amount'][$i] != null) {
                    $amount = floatval(str_replace(",", "", $_POST['amount'][$i]));
                    $sum += $amount;
                }
            }
        }
        //todo: calculate institution fees => this for external calls

        //check if sum exceed budget of category
        if ($sum > $call_category->budget_ceiling) {
            return redirect('applications.step-3', [$id, $app_id])->with('danger', 'Summation of budget exceed budget ceiling of call category');
        }

//validate
        if (isset($_POST['continue'])) {
            $this->validate($request, [
                'attachment' => 'required',
                // 'cv_attachment' => 'required',
            ]);
        }

        //call application
        $call_application = CallApplication::find($app_id);
        $call_application->source_fund = $request->input("source_fund");

        // Check if an attachment has been uploaded
        if ($request->has('attachment')) {
            // Get image file
            $attachment = $request->file('attachment');

            // Make a image name
            $name = time() . '_' . $attachment->getFilename() . '.' . $attachment->getClientOriginalExtension();

            //Move Uploaded File
            $attachment->move('assets/uploads/documents/', $name);

            // Set attachment name
            $call_application->attachment = $name;
        }

        // Check if a CV attachment has been uploaded
        if ($request->has('cv_attachment')) {
            // Get image file
            $cv_attachment = $request->file('cv_attachment');

            // Make a image name
            $cv_name = time() . '_' . $cv_attachment->getFilename() . '.' . $cv_attachment->getClientOriginalExtension();

            //Move Uploaded File
            $cv_attachment->move('assets/uploads/documents/', $cv_name);

            // Set attachment name
            $call_application->cv_attachment = $cv_name;
        }

        //update completion status
        if (isset($_POST['save'])) {
            $call_application->completion = 2;
        }
        else if (isset($_POST['continue'])) {
            $call_application->completion = 3;
        }

        //save
        if ($call_application->save()) {
            //application budget
            if (isset($_POST['objective']) && isset($_POST['amount'])) {
                //process for insertion
                for ($i = 0; $i < sizeof($_POST['objective']); $i++) {
                    //check if amount is null
                    if ($_POST['amount'][$i] != null) {
                        $amount = floatval(str_replace(",", "", $_POST['amount'][$i]));
                        $obj_id = $_POST['objective'][$i];

                        //update call objectives
                        $app_objective = CallApplicationObjective::find($obj_id);
                        $app_objective->budget = $amount;
                        $app_objective->remained_fund = $amount;
                        $app_objective->save();
                    }
                }
            }

            //redirect
            if (isset($_POST['save'])) {
                return redirect('calls')->with('success', 'Call application saved, continue later');
            }

            if (isset($_POST['continue'])) {
                return redirect()->route('applications.preview', $app_id)->with('success', 'Call application saved');
            }
        } else {
            return redirect()->route('applications.step-3', [$id, $app_id])->with('danger', 'Failed to save application');
        }
    }

    //preview application
    function preview($app_id)
    {
        $this->data['title'] = 'Call Application Preview';

        //app_id
        $call_app = CallApplication::findOrFail($app_id);
        $this->data['call_app'] = $call_app;

        //call
        $call = Call::findOrFail($call_app->call_id);
        $this->data['call'] = $call;

        //principal investigator
        $this->data['investigator'] = User::findOrFail($call_app->principal_investigator);

        //internal research partners
        $partners = CallApplicationPartner::where(['application_id' => $app_id, 'partner_type' => 'INTERNAL'])->get();
        $this->data['partners'] = $partners;
        foreach ($partners as $k => $v) {
            $this->data['partners'][$k]->partner = User::find($v->partner_id);
        }

        //students
        $students = CallApplicationPartner::where(['application_id' => $app_id, 'partner_type' => 'STUDENT'])->get();
        $this->data['students'] = $students;
        foreach ($students as $k => $v) {
            $this->data['students'][$k]->partner = Student::find($v->partner_id);
        }

        //external research partners
        $external_partners = CallApplicationPartner::where(['application_id' => $app_id, 'partner_type' => 'EXTERNAL'])->get();
        $this->data['external_partners'] = $external_partners;
        foreach ($external_partners as $k => $v) {
            $this->data['external_partners'][$k]->partner = Researcher::find($v->partner_id);
        }

        //call category
        $this->data['call_category'] = CallCategory::findOrFail($call_app->call_category_id);

        //research type
        $this->data['research_type'] = ResearchType::findOrFail($call_app->research_type_id);

        //cluster
        $this->data['cluster'] = Cluster::findOrFail($call_app->call_cluster_id);

        //research area
        $this->data['research_area'] = ResearchArea::findOrFail($call_app->call_research_area_id);

        //specific objectives
        $this->data['objectives'] = CallApplicationObjective::where(['application_id' => $app_id])->get();

        //render view
        if ($call_app->application_grant_category === 'innovation_grant') {
            return view('call_applications.innovation.preview', $this->data);
        }

        return view('call_applications.preview', $this->data);
    }

    //cancel app
    public function cancel($app_id)
    {
        $this->data['title'] = 'Cancel app';

        //call app
        $call_app = CallApplication::findOrFail($app_id);

        //calls
        $call = Call::findOrFail($call_app->call_id);

        //update app
        $call_app->status = 'CANCELED';
        $call_app->save();

        //redirect
        return redirect('calls/' . $call->id)->with('danger', 'Call application canceled');
    }

    //confirm app
    public function confirm($app_id): RedirectResponse
    {
        $this->data['title'] = 'Confirm call application';

        //app_id
        $call_app = CallApplication::findOrFail($app_id);

        //call
        $call = Call::findOrFail($call_app->call_id);

        //check if deadline is greater than application time
        $deadline = strtotime($call->deadline);
        $app_date = strtotime(date('Y-m-d H:i:s'));

        if ($app_date > $deadline) {
            //redirect
            return Redirect::route('applications.preview', $app_id)->with('danger', 'Application exceed deadline, Please cancel the call');
        }

//call application
        $call_app->status = 'COMPLETE';
        $call_app->completion = 4;
        $call_app->created_at = date('Y-m-d H:i:s');
        $call_app->save();

        //todo: call application approvals => this for internal call only | improve for external calls application
        $approval_levels = ['hod' => 'HoD', 'principal' => 'Principal', 'drp' => 'DRP'];

        foreach ($approval_levels as $key => $level) {
            $call_approval = new CallApplicationApproval([
                'application_id' => $app_id,
                'approval_level' => $key,
                'status' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => Perms::get_current_user_id()
            ]);
            $call_approval->save();
        }

        //todo: send notification to HoD to notify on call to approve
        //pi
        $pi = $call_app->pi->first_name . '' . $call_app->pi->middle_name . ' ' . $call_app->pi->surname;

        //hod details
        $hod = User::get_hod($call_app->pi->department_id);

        if ($hod) {
            //construct message
            $message_id = Notification::generate_message_id();

            //message
            $message = "$pi has applied for a call $call->title with the title <b>$call_app->project_title</b>";
            $message .= "<br/>Please log in to the system by <a href=" . url('') . ">click here</a>to approve the application.";

            //save notification
            $notification = new Notification(
                [
                    'message' => $message,
                    'message_id' => $message_id,
                    'user_id' => $hod->id,
                    'user' => "HoD",
                    'email' => $hod->email,
                    'phone' => Notification::cast_mobile($hod->phone),
                    'app_status' => 'PENDING',
                    'sms_status' => 'PENDING',
                    'email_status' => 'PENDING',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => Auth::user()->id
                ]
            );
            $notification->save();

            //create array
            $array = ['name' => $hod->first_name . ' ' . $hod->surname, 'email' => $hod->email, 'message' => $message];

            if (Notification::send_email($array)) {
                $data = ['email_status' => 'SENT'];
            }
            else {
                $data = ['email_status' => 'REJECTED'];
            }

            Notification::where(['id' => $notification->id])->update($data);
        }

        //construct message to send to partners
        else {
        $message_id = Notification::generate_message_id();

        //message
        $message = "<b>$pi</b> invite you to participate in a call applied through University of Dar es Salaam Research Management portal.<br/>";
        $message .= "To confirm your participation please click the link below.<br/>";
        $message .= '<a href="' . route('proposals.confirm', $call_app->id) . '">Click here</a>';
        $message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

        //partners
        $call_partners = CallApplicationPartner::where(['application_id' => $app_id])->get();

        if ($call_partners) {
            foreach ($call_partners as $val) {
                $notification = new Notification(
                    [
                        'message' => $message,
                        'message_id' => $message_id,
                        'user_id' => $val->partner_id,
                        'user' => $val->partner_type,
                        'email' => $val->partner_email,
                        'phone' => Notification::cast_mobile($val->partner_phone),
                        'app_status' => 'PENDING',
                        'sms_status' => 'PENDING',
                        'email_status' => 'PENDING',
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => Perms::get_current_user_id()
                    ]
                );
                $notification->save();
            }
            }
        }

        //redirect
        return Redirect::route('applications.success', $app_id);
    }

    //success on apply proposal
    public function success($app_id)
    {
        $this->data['title'] = 'Success';

        //app_id
        $call_app = CallApplication::findOrFail($app_id);
        $this->data['call_app'] = $call_app;

        //call
        $call = Call::findOrFail($call_app->call_id);
        $this->data['call'] = $call;

        return view('call_applications.success', $this->data);
    }


    /**==================MANAGEMENT OF CALL APPLICATION ======================**/
    //application lists
    public function lists(Request $request)
    {
        $this->data['title'] = 'My Applications';

        //if post        
        if (isset($_POST['filter'])) {
            $call_id = $request->input('call_id');
            $this->data['call_id'] = $call_id;

            //count apps
            $this->data['count_apps'] = CallApplication::count_staff_applications($call_id);

            //get all apps
            $this->data['call_apps'] = CallApplication::get_staff_applications($call_id);
        } else {
            //count apps
            $this->data['count_apps'] = CallApplication::count_staff_applications();

            //get all apps
            $this->data['call_apps'] = CallApplication::get_staff_applications();
        }

        foreach ($this->data['call_apps'] as $k => $val) {
            $this->data['call_apps'][$k]->call_evaluation_result = CallApplicationResult::where('application_id', '=', $val->call_app_id)->first();
            $this->data['call_apps'][$k]->app_budgets = CallApplicationObjective::where('application_id', '=', $val->call_app_id)->sum('budget');
        }

        //calls
        $this->data['calls'] = Call::orderBy('created_at', 'DESC')->get();

        //render view
        return view('call_applications.lists', $this->data);
    }

    //details
    public function details($app_id)
    {
        Perms::has_allowed('call_applications', 'details');
        $this->data['title'] = 'Call Application details';

        //app_id
        $call_app = CallApplication::findOrFail($app_id);
        $this->data['call_app'] = $call_app;

        //call
        $call = Call::findOrFail($call_app->call_id);
        $this->data['call'] = $call;

        //internal research partners
        $partners = CallApplicationPartner::where(['application_id' => $app_id, 'partner_type' => 'INTERNAL'])->get();
        $this->data['partners'] = $partners;
        foreach ($partners as $k => $v) {
            $this->data['partners'][$k]->partner = User::find($v->partner_id);
        }

        //students
        $students = CallApplicationPartner::where(['application_id' => $app_id, 'partner_type' => 'STUDENT'])->get();
        $this->data['students'] = $students;
        foreach ($students as $k => $v) {
            $this->data['students'][$k]->partner = Student::find($v->partner_id);
        }

        //external research partners
        $external_partners = CallApplicationPartner::where(['application_id' => $app_id, 'partner_type' => 'EXTERNAL'])->get();
        $this->data['external_partners'] = $external_partners;
        foreach ($external_partners as $k => $v) {
            $this->data['external_partners'][$k]->partner = Researcher::find($v->partner_id);
        }

        //render view
        if ($call_app->application_grant_category === 'innovation_grant') {
            return view('call_applications.innovation.details', $this->data);
        }

        return view('call_applications.details', $this->data);
    }

    //edit
    public function edit($app_id)
    {
        //call app
        $call_app = CallApplication::findOrFail($app_id);
        $this->data['call_app'] = $call_app;

        //call
        $call = Call::findOrFail($call_app->call_id);
        $this->data['call'] = $call;

        return view('call_applications.edit', $this->data);
    }

    //update
    public function update(Request $request)
    {
        $this->validate(
            $request,
            ['specific_objective.*' => 'required'],
            ['specific_objective.*.required' => 'Specific objective is required']
        );

        //post data
        $call_id = $request->input('call_id');
        $app_id = $request->input('call_app_id');

        //todo: insert objectives
        if (isset($_POST['specific_objective'])) {
            //delete objectives if exists
            CallApplicationObjective::where(['call_id' => $call_id, 'application_id' => $app_id])->delete();

            //process for insertion
            for ($h = 0; $h < sizeof($_POST['specific_objective']); $h++) {
                $app_objective = new CallApplicationObjective(
                    [
                        'call_id' => $call_id,
                        'application_id' => $app_id,
                        'objective' => $_POST['specific_objective'][$h]
                    ]
                );
                $app_objective->save();
            }
        }
        //redirect 
        return redirect(route('calls.applications', $call_id))->with('success', 'Call application updated');
    }

    //delete
    public function delete($app_id)
    {
        Perms::has_allowed('call_applications', 'delete');
        $this->data['title'] = 'Delete';

        //call application
        $call_app = CallApplication::findOrFail($app_id);

        //call
        $call = Call::findOrFail($call_app->call_id);

        //delete
        $call_app->delete();

        //delete application objective
        CallApplicationObjective::where('application_id', '=', $app_id)->delete();

        //delete application evaluations
        CallApplicationEvaluation::where('application_id', '=', $app_id)->delete();

        //delete application evaluators
        CallApplicationEvaluator::where('application_id', '=', $app_id)->delete();

        //delete application approvals
        CallApplicationApproval::where('application_id', '=', $app_id)->delete();

        //delete application partners
        CallApplicationPartner::where('application_id', '=', $app_id)->delete();

        //redirect
        return redirect(route('calls.applications', $call->id))->with('danger', 'Call application deleted');
    }

    //assign evaluators
    public function assign_evaluators($call_id, $app_id)
    {
        $this->data['title'] = 'Assign evaluators';
        Perms::has_allowed('call_applications', 'assign_evaluators');

        //call
        $call = Call::find($call_id);

        if (!$call) {
            abort(404);
        }

        $this->data['call'] = $call;

        //app_id
        $call_app = CallApplication::find($app_id);

        if (!$call_app)
            abort(404, 'Call application does not exist');

        $this->data['call_app'] = $call_app;

        //principal investigator
        $this->data['investigator'] = User::findOrFail($call_app->principal_investigator);

        //evaluators
        $this->data['users'] = User::select('users.id', 'users.first_name', 'users.middle_name', 'users.surname')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where(['roles.name' => 'evaluators'])
            ->orderBy('users.first_name')
            ->get();

        return view('call_applications.assign_evaluators', $this->data);
    }

    //store evaluators
    public function store_evaluators(Request $request, $call_id, $app_id)
    {
        //validation
        $this->validate(
            $request,
            ['evaluator_ids' => 'required'],
            ['evaluator_ids.required' => 'Evaluator(s) required']
        );

        //insert evaluators
        $evaluators = $request->input('evaluator_ids');

        foreach ($evaluators as $evaluator_id) {
            //todo : check if evaluator exists ??
            $call_app_evaluator = CallApplicationEvaluator::where(['call_id' => $call_id, 'application_id' => $app_id, 'evaluator_id' => $evaluator_id])->first();

            if ($call_app_evaluator) {
                return redirect('call_applications/details/' . $call_id . '/' . $app_id . '/#evaluators')->with('success', 'Evaluators assigned');
            }

//insert
            $call_evaluator = new CallApplicationEvaluator([
                'call_id' => $call_id,
                'application_id' => $app_id,
                'evaluator_id' => $evaluator_id,
                'created_by' => Perms::get_current_user_id(),
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $call_evaluator->save();
        }

        //redirect
        return redirect('call_applications/details/' . $call_id . '/' . $app_id . '/#evaluators')->with('success', 'Evaluators assigned');
    }

    //drop evaluator
    public function drop_evaluator($call_id, $app_id, $ev_id)
    {
        $this->data['title'] = 'Delete evaluator';
        Perms::has_allowed('call_applications', 'drop_evaluator');

        //call
        $call = Call::find($call_id);

        if (!$call) {
            abort(404);
        }

        //app_id
        $call_app = CallApplication::find($app_id);

        if (!$call_app) {
            abort(404, 'Call application does not exist');
        }

        //delete evaluator
        CallApplicationEvaluator::query()->delete($ev_id);

        //redirect
        return redirect('/call_applications/details/' . $call_id . '/' . $app_id)->with('danger', 'Evaluator dropped');
    }

    /*=========================CALL APPLICATIONS BATCH ACTIONS ======================*/
    //selected
    public function selected()
    {
        $app_ids = [];

        //assign evaluator
        if (isset($_POST['assign_evaluator'])) {
            if (empty($_POST['id'])) {
                //redirect with message
                return redirect('call_applications/lists')->with('message', 'No call application selected');
            }

//looping post value
            $i = 1;
            foreach ($_POST['id'] as $app_id) {
                $app_ids[$i] = $app_id;
                $i++;
            }

            //first unset session data
            session()->forget('app_ids');

            //save data in session
            session(['app_ids' => $app_ids]);
            return redirect('call_applications/batch_assign_evaluators');
        }

        //delete app
        if (isset($_POST['delete_app'])) {
            if (empty($_POST['id'])) {
                //redirect with message
                return redirect('call_applications/lists')->with('message', 'No call application selected');
            }

//looping post value
            $i = 1;
            foreach ($_POST['id'] as $app_id) {
                $app_ids[$i] = $app_id;
                $i++;
            }

            //first unset session data
            session()->forget('app_ids');

            //save data in session
            session(['app_ids' => $app_ids]);
            return redirect('call_applications/batch_delete');
        }
    }

    //assign evaluators in batch
    public function batch_assign_evaluators()
    {
        $this->data['title'] = "Assign Evaluator";
        Perms::has_allowed('call_applications', 'assign_evaluators');

        //app_ids
        $app_ids = session('app_ids');

        //call applications
        $call_applications = CallApplication::whereIn('id', $app_ids)->get();
        $this->data['call_applications'] = $call_applications;

        //evaluators
        $this->data['users'] = User::select('users.id', 'users.first_name', 'users.middle_name', 'users.surname')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where(['roles.name' => 'evaluators'])
            ->orderBy('users.first_name')
            ->get();

        //render view
        return view('call_applications.batch_assign_evaluators', $this->data);
    }

    //store batch evaluators
    public function batch_store_evaluators(Request $request)
    {
        //validation
        $this->validate(
            $request,
            ['evaluator_ids' => 'required'],
            ['evaluator_ids.required' => 'Evaluator(s) required']
        );

        //insert evaluators
        $evaluators = $request->input('evaluator_ids');

        foreach ($evaluators as $evaluator_id) {
            //app_ids
            $app_ids = session('app_ids');

            //iterate app_ids
            foreach ($app_ids as $app_id) {
                $app = CallApplication::find($app_id);

                //todo : check if evaluator exists ??
                $call_app_evaluator = CallApplicationEvaluator::where(['application_id' => $app_id, 'evaluator_id' => $evaluator_id])->first();

                if ($call_app_evaluator) {
                    return redirect('call_applications/lists')->with('success', 'Evaluators Exists');
                }

//insert
                $call_evaluator = new CallApplicationEvaluator([
                    'call_id' => $app->call_id,
                    'application_id' => $app_id,
                    'evaluator_id' => $evaluator_id,
                    'created_by' => Perms::get_current_user_id(),
                    'created_at' => date('Y-m-d H:i:s')
                ]);

                $call_evaluator->save();
            }
        }
        //forget session
        session()->forget('app_ids');

        //redirect
        return redirect('call_applications/lists')->with('success', 'Evaluators assigned');
    }

    //delete
    public function batch_delete()
    {
        $this->data['title'] = 'Delete';
        Perms::has_allowed('call_applications', 'delete');

        //app_ids
        $app_ids = session('app_ids');

        foreach ($app_ids as $app_id) {
            //call app
            $call_app = CallApplication::find($app_id);
            $call_app->delete();
        }

        //forget session
        session()->forget('app_ids');

        //redirect
        return redirect('call_applications/lists')->with('danger', 'Call application deleted');
    }



    /*==========================================================
    APIs => details, approve, award, decline and register project
    ==========================================================*/
    //api to show application details
    public function api_details($id): void
    {
        $app = CallApplication::find($id);

        //budget
        $budget = CallApplicationObjective::where('application_id', '=', $id)->sum('budget');

        //evaluation grade
        $evaluation_result = CallApplicationResult::where('application_id', '=', $id)->first();

        $arr_app = [
            'title' => $app->project_title,
            'introduction' => $app->introduction,
            'general_objective' => $app->general_objective,
            'project_number' => $this->create_project_number($id),
            'pi' => $app->pi->first_name . ' ' . $app->pi->middle_name . ' ' . $app->pi->surname,
            'category' => $app->callCategory->title,
            'submitted_date' => date('d-m-Y H:i', strtotime($app->created_at)),
            'budget' => number_format($budget),
            'evaluation_grade' => $evaluation_result ? round($evaluation_result->avg_points, 2) : '0',
            'winner' => $app->winner
        ];

        //response
        echo json_encode($arr_app);
    }

    //approval call application
    public function api_approve($app_id, $role_name): void
    {
        //app_id
        $call_app = CallApplication::findOrFail($app_id);

        //call
        $call = Call::findOrFail($call_app->call_id);

        //update call approval status to 1
        $call_approval = CallApplicationApproval::where(['application_id' => $app_id, 'approval_level' => $role_name])->first();

        if ($call_approval) {
            //where
            $where = ['application_id' => $app_id, 'approval_level' => $role_name];

            //data
            $data = ['status' => 1, 'updated_by' => Perms::get_current_user_id(), 'updated_at' => date('Y-m-d H:i:s')];

            //update call application approval
            if (CallApplicationApproval::where($where)->update($data)) {
                //send notification for specific role 
                //pi
                $pi = $call_app->pi->first_name . '' . $call_app->pi->middle_name . ' ' . $call_app->pi->surname;

                //check if role_name = hod
                if ($role_name === 'hod') {
                    $principal = User::get_principal($call_app->pi->college_id);

                    if ($principal) {
                        //construct message
                        $message_id = Notification::generate_message_id();

                        //message
                        $message = "$pi has applied for a call $call->title with the title <b>$call_app->project_title</b> and approved by Head of Department (HoD)";
                        $message .= "<br/>Please log in to the system by <a href=" . url('') . ">click here</a>to approve the application.";

                        //save notification
                        $notification = new Notification(
                            [
                                'message' => $message,
                                'message_id' => $message_id,
                                'user_id' => $principal->id,
                                'user' => "Principal",
                                'email' => $principal->email,
                                'phone' => Notification::cast_mobile($principal->phone),
                                'app_status' => 'PENDING',
                                'sms_status' => 'PENDING',
                                'email_status' => 'PENDING',
                                'created_at' => date('Y-m-d H:i:s'),
                                'created_by' => Auth::user()->id
                            ]
                        );
                        $notification->save();

                        //create array
                        $array = ['name' => $principal->first_name . ' ' . $principal->surname, 'email' => $principal->email, 'message' => $message];

                        if (Notification::send_email($array)) {
                            $data = ['email_status' => 'SENT'];
                        }
                        else {
                            $data = ['email_status' => 'REJECTED'];
                        }

                        Notification::where(['id' => $notification->id])->update($data);
                    }
                } else if ($role_name === 'principal') {
                    $drp = User::get_drp();

                    if ($drp) {
                        //construct message
                        $message_id = Notification::generate_message_id();

                        //message
                        $message = "$pi has applied for a call $call->title with the title <b>$call_app->project_title</b> and approved by College Principal";
                        $message .= "<br/>Please log in to the system by  <a href=" . url('') . ">click here</a> to approve the application.";

                        //save notification
                        $notification = new Notification(
                            [
                                'message' => $message,
                                'message_id' => $message_id,
                                'user_id' => $drp->id,
                                'user' => "DRP",
                                'email' => $drp->email,
                                'phone' => Notification::cast_mobile($drp->phone),
                                'app_status' => 'PENDING',
                                'sms_status' => 'PENDING',
                                'email_status' => 'PENDING',
                                'created_at' => date('Y-m-d H:i:s'),
                                'created_by' => Auth::user()->id
                            ]
                        );
                        $notification->save();

                        //create array
                        $array = ['name' => $drp->first_name . ' ' . $drp->surname, 'email' => $drp->email, 'message' => $message];

                        if (Notification::send_email($array)) {
                            $data = ['email_status' => 'SENT'];
                        } else {
                            $data = ['email_status' => 'REJECTED'];
                        }

                        Notification::where(['id' => $notification->id])->update($data);
                    }
                }

                //update approval status
                $apprv = CallApplicationApproval::where(['application_id' => $app_id, 'status' => 0])->count();

                if ($apprv == 0) {
                    //update application approval = 1
                    $call_app->approval_status = 1;
                    $call_app->save();

                    //send notification to evaluators
                    $evaluators = CallApplicationEvaluator::where(['application_id' => $app_id]);

                    if ($evaluators->count() > 0) {
                        $evaluators = $evaluators->get(); //list of evaluators

                        //construct message
                        $message_id = Notification::generate_message_id();

                        //message
                        $message = "You have assigned to evaluate <b>$call->title</b> call and application with title <b>$call_app->project_title</b> already approved in all levels";
                        $message .= " <p>Please log in to the system by  <a href=" . url('/clearance/process') . ">click here</a> to evaluate application.</p>";

                        foreach ($evaluators  as $val) {
                            //save notification
                            $notification = new Notification(
                                [
                                    'message' => $message,
                                    'message_id' => $message_id,
                                    'user_id' => $val->user->id,
                                    'user' => "EVALUATOR",
                                    'email' => $val->user->email,
                                    'phone' => Notification::cast_mobile($val->apply_step3user->phone),
                                    'app_status' => 'PENDING',
                                    'sms_status' => 'PENDING',
                                    'email_status' => 'PENDING',
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'created_by' => Perms::get_current_user_id()
                                ]
                            );
                            $notification->save();
                        }
                    }
                }

                //response
                $response['status'] = 'success';
            } else {
                $response['status'] = 'failed';
            }
        } else {
            $response['status'] = 'failed';
        }
        echo json_encode($response);
    }

    //api award
    public function api_award($id): void
    {
        //app_id
        $app = CallApplication::find($id);

        ///call
        $call = Call::find($app->call_id);

        //change status
        $app->winner = 1;
        $app->changed_at = date('Y-m-d');
        $app->changed_by = Perms::get_current_user_id();

        if ($app->save()) {
            //send notification to principal investigator
            $pi = "Principal Investigator";
            if ($app->pi) {
                $pi = $app->pi->first_name . ' ' . $app->pi->middle_name . ' ' . $app->pi->surname;
            }

            //message
            $message_id = Notification::generate_message_id();

            //message
            $message = '<p>Conglatulations, You have been choosen among the winners for <b>' . $call->title . '</b> call</p>';
            $message .= " <p>Please log in to the system to register the project.</p>";

            //save notification
            $notification = new Notification(
                [
                    'message' => $message,
                    'message_id' => $message_id,
                    'user_id' => $app->pi->id,
                    'user' => "PI",
                    'email' => $app->pi->email,
                    'phone' => Notification::cast_mobile($app->pi->phone),
                    'app_status' => 'PENDING',
                    'sms_status' => 'PENDING',
                    'email_status' => 'PENDING',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => Perms::get_current_user_id()
                ]
            );
            $notification->save();

            //create array
            $array = ['name' => $pi, 'email' => $app->pi->email, 'message' => $message];

            if (Notification::send_email($array)) {
                $data = ['email_status' => 'SENT'];
            }
            else {
                $data = ['email_status' => 'REJECTED'];
            }

            Notification::where(['id' => $notification->id])->update($data);

            //response
            $response['status'] = 'success';
        } else {
            $response['status'] = 'failed';
        }

        echo json_encode($response);
    }

    //decline
    public function api_decline($id): void
    {
        //app_id
        $app = CallApplication::find($id);

        ///call
        $call = Call::find($app->call_id);

        //change status
        $app->winner = 0;
        $app->changed_at = date('Y-m-d');
        $app->changed_by = Perms::get_current_user_id();

        if ($app->save()) {
            //send notification to principal investigator
            $pi = "Principal Investigator";
            if ($app->pi) {
                $pi = $app->pi->first_name . ' ' . $app->pi->middle_name . ' ' . $app->pi->surname;
            }

            //message
            $message_id = Notification::generate_message_id();

            //message
            $message = '<p>Sorry to inform that your call application for <b>' . $call->title . '</b> has been DECLINED</p>';
            $message .= " <p>Please try to apply the another call when published.</p>";

            //save notification
            $notification = new Notification(
                [
                    'message' => $message,
                    'message_id' => $message_id,
                    'user_id' => $app->pi->id,
                    'user' => "PI",
                    'email' => $app->pi->email,
                    'phone' => Notification::cast_mobile($app->pi->phone),
                    'app_status' => 'PENDING',
                    'sms_status' => 'PENDING',
                    'email_status' => 'PENDING',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => Perms::get_current_user_id()
                ]
            );
            $notification->save();

            //create array
            $array = ['name' => $pi, 'email' => $app->pi->email, 'message' => $message];

            if (Notification::send_email($array)) {
                $data = ['email_status' => 'SENT'];
            }
            else {
                $data = ['email_status' => 'REJECTED'];
            }

            Notification::where(['id' => $notification->id])->update($data);

            //response
            $response['status'] = 'success';
        } else {
            $response['status'] = 'failed';
        }

        echo json_encode($response);
    }

    //register project
    public function api_register_project($id): void
    {
        //app_id
        $app = CallApplication::find($id);

        //insert into project table
        $project = Project::where(['application_id' => $app->id])->first();

        if (!$project) {
            $budget = CallApplicationObjective::where('application_id', '=', $app->id)->sum('budget');

            //create new project
            $new_project = new Project();
            $new_project->application_id = $app->id;
            $new_project->project_reg_number = $this->create_project_number($app->id);
            $new_project->total_fund = $budget;
            $new_project->remained_fund = $budget;
            $new_project->start_at = date('Y-m-d');
            $new_project->created_by = Auth::user()->id;
            $new_project->save();

            //response
            $response['status'] = 'success';
        } else {
            $response['status'] = 'failed';
        }

        echo json_encode($response);
    }


    //callback function => create project number
    public function create_project_number($app_id): string
    {
        $app = CallApplication::findOrFail($app_id);

        //user college and department
        $college = $app->pi->college->short_name;
        $department = $app->pi->department->short_name;

        //registration date
        $reg_at = date('Y');

        //serial
        //check last inserted number
        $project = Project::where(['application_id' => $app_id])->orderBy('id', 'DESC')->first();

        if ($project) {
            $start_value = substr($project->project_number, -3);

            #serial
            $serial = sprintf("%03d", $start_value++);
        } else {
            $serial = '001';
        }

        return $college . '-' . $department . $reg_at . $serial;
    }
}
