<?php

namespace App\Http\Controllers\Calls;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

use App\Models\Calls\Call;
use App\Models\Calls\CallApplication;
use App\Models\Settings\EvaluationCriteria;
use App\Models\Settings\EvaluationSubCriteria;
use App\Models\Calls\CallApplicationEvaluation;
use App\Models\Calls\CallApplicationEvaluator;
use App\Models\Calls\CallApplicationResult;

use App\Perms;

use PDF;

class CallEvaluationsController extends Controller
{
    private $data;

    //constructor
    public function __construct()
    {
        $this->middleware('auth');
    }

    //application lists
    public function lists(Request $request)
    {
        Perms::has_allowed('call_evaluations', 'records');
        $this->data['title'] = 'Grant Evaluations Records';

        //if post        
        if (isset($_POST['filter'])) {
            $call_id = $request->input('call_id');
            $cluster_id = $request->input('cluster_id');
            $keyword = $request->input('keyword');
            $start_at = $request->input('start_at');
            $end_at = $request->input('end_at');

            $this->data['call_id'] = $call_id;
            $this->data['cluster_id'] = $cluster_id;
            $this->data['keyword'] = $keyword;
            $this->data['start_at'] = $start_at;
            $this->data['end_at'] = $end_at;

            //count apps
            $this->data['count_apps'] = CallApplication::count_all($call_id, $cluster_id, $keyword, $start_at, $end_at);

            //get all apps
            $this->data['call_apps'] = CallApplication::get_all($call_id, $cluster_id, $keyword, $start_at, $end_at);
        } else {
            $this->data['call_id'] = null;
            $this->data['cluster_id'] = null;
            $this->data['keyword'] = null;
            $this->data['start_at'] = null;
            $this->data['end_at'] = null;

            //count apps
            $this->data['count_apps'] = CallApplication::count_all();

            //get all apps
            $this->data['call_apps'] = CallApplication::get_all();
        }

        //calls
        $this->data['calls'] = Call::orderBy('created_at', 'DESC')->get();

        //render view
        return view('call_evaluations.lists', $this->data);
    }

    //evaluation records
    public function records($call_id, $app_id)
    {
        Perms::has_allowed('call_evaluations', 'records');
        $this->data['title'] = 'Evaluations Records';

        //call
        $call = Call::findOrFail($call_id);

        //call app
        $call_app = CallApplication::findOrFail($app_id);

        $this->data['call'] = $call;
        $this->data['call_app'] = $call_app;

        //evaluation module
        $evaluation_criteria = EvaluationCriteria::all();
        $this->data['evaluation_criteria'] = $evaluation_criteria;

        foreach ($this->data['evaluation_criteria'] as $k => $v) {
            $this->data['evaluation_criteria'][$k]->sub_criteria = EvaluationSubCriteria::where('criteria_id', $v->id)->get();
        }

        //render view
        return view('call_evaluations.records', $this->data);
    }

    //create evaluation
    public function create($call_id, $app_id, $criteria_id)
    {
        Perms::has_allowed('call_evaluations', 'create');
        $this->data['title'] = 'Evaluation Tool';

        //call
        $call = Call::findOrFail($call_id);

        //call app
        $call_app = CallApplication::findOrFail($app_id);

        $this->data['call'] = $call;
        $this->data['call_app'] = $call_app;

        //criteria id
        $this->data['criteria_id'] = $criteria_id;

        //query evaluation criteria
        $ev_criteria = EvaluationCriteria::where('id', $criteria_id)->get();
        $this->data['ev_criteria'] = $ev_criteria;

        //evaluation contents
        $ev_content = CallApplicationEvaluation::get_evaluation_content($ev_criteria[0]->action, $app_id);
        $this->data['ev_content'] = $ev_content;

        foreach ($this->data['ev_criteria'] as $k => $v) {
            $this->data['ev_criteria'][$k]->sub_criteria = EvaluationSubCriteria::where('criteria_id', $v->id)->get();
        }

        //render view
        return view('call_evaluations.create', $this->data);
    }

    //store
    public function store(Request $request): RedirectResponse
    {
        //validation
        $this->validate($request, CallApplicationEvaluation::rules(), CallApplicationEvaluation::messages());

        //inputs
        $call_id = $request->input('call_id');
        $app_id = $request->input('app_id');

        if (isset($_POST['score'])) {
            for ($i = 0; $i < sizeof($_POST['score']); $i++) {
                if ($_POST['score'][$i] != '') {
                    //todo : check if evaluation exists
                    $call_id = $request->input('call_id');
                    $app_id = $request->input('app_id');
                    $criteria_id = $request->input('criteria_id');
                    $sub_criteria_id = $_POST['sub_criteria_id'][$i];

                    $app_eval = CallApplicationEvaluation::where(
                        [
                            'call_id' => $call_id,
                            'application_id' => $app_id,
                            'criteria_id' => $criteria_id,
                            'sub_criteria_id' => $sub_criteria_id,
                            'evaluator_id' => Perms::get_current_user_id()
                        ]
                    )->first();

                    //points
                    $points = $_POST['score'][$i] * $_POST['weight'][$i];

                    if ($app_eval) {
                        //update
                        $app_eval->score = $_POST['score'][$i];
                        $app_eval->points = $points;
                        $app_eval->comments = $_POST['comments'][$i];
                        $app_eval->save();
                    } else {
                        //insert
                        $app_evaluation = new CallApplicationEvaluation(
                            [
                                'call_id' => $call_id,
                                'application_id' => $app_id,
                                'criteria_id' => $criteria_id,
                                'sub_criteria_id' => $sub_criteria_id,
                                'score' => $_POST['score'][$i],
                                'points' => $points,
                                'comments' => $_POST['comments'][$i],
                                'evaluator_id' => Perms::get_current_user_id(),
                                'created_at' => date('Y-m-d')
                            ]
                        );
                        $app_evaluation->save();
                    }
                }
            }
        }
        //redirect 
        return redirect()->route('evaluations.records', [$call_id, $app_id])->with('success', 'Evaluation saved');
    }

    //process results
    public function confirm_results($call_id, $app_id): RedirectResponse
    {
        //call
        $call = Call::findOrFail($call_id);

        //app_id
        $call_app = CallApplication::findOrFail($app_id);

        //call application evaluators
        $app_evaluator = CallApplicationEvaluator::where(['call_id' => $call_id, 'application_id' => $app_id, 'evaluator_id' => Perms::get_current_user_id()])->first();

        if ($app_evaluator) {
            //todo: validate application evaluation

            //update total points
            $points = CallApplicationEvaluation::getSummationPoints($call_id, $app_id, Perms::get_current_user_id());

            //update application evaluator
            $app_evaluator->status = 'COMPLETE';
            $app_evaluator->points = $points;
            $app_evaluator->updated_by = Perms::get_current_user_id();
            $app_evaluator->updated_at = date('Y-m-d H:i:s');
            $app_evaluator->save();

            //todo: check if all complete to process final exams
            $incomplete = CallApplicationEvaluator::where(['call_id' => $call_id, 'application_id' => $app_id, 'status' => 'INCOMPLETE'])->count();
            if ($incomplete === 0) {
                //update evaluation status = complete (2)
                CallApplication::where(['id' => $app_id])->update(['eval_status' => 2]);

                //evaluation results for all evaluators => average points
                $avg_points = CallApplicationEvaluation::getAveragePoints($call_id, $app_id);

                //insert evaluation results
                $app_res = new CallApplicationResult(
                    [
                        'call_id' => $call_id,
                        'application_id' => $app_id,
                        'avg_points' => $avg_points
                    ]
                );
                $app_res->save();
            } else {
                //update evaluation status = incomplete (1)
                CallApplication::where(['id' => $app_id])->update(['eval_status' => 1]);
            }
            //redirect
            return redirect()->route('evaluations.records', [$call_id, $app_id])->with('success', 'Evaluation results confirmed');
        }

        return redirect()->route('evaluations.records', [$call_id, $app_id])->with('danger', 'Failed to confirm evaluation, you are not assigned to evaluate this application.');
    }

    //set the winner manually after the manual evaluation process and approval
    public function set_winner($call_id, $app_id): RedirectResponse
    {
        //call
        $call = Call::findOrFail($call_id);

        //app_id
        $call_app = CallApplication::findOrFail($app_id);

        //set the project as winner
        if ($call_app) {
            $call_app->approval_status = 1;
            $call_app->eval_status = 2;
            $call_app->winner = 1;
            $call_app->updated_at = date('Y-m-d H:i:s');
            $call_app->save();
        } else {
            //update evaluation status = incomplete (1)
            CallApplication::where(['id' => $app_id])->update(['eval_status' => 1]);
        }
        //redirect
        return redirect()->route('calls.applications', [$call_id])->with('success', 'Winner is successful set');
    }
}
