<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Settings\ResearchArea;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Models\Settings\Cluster;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class ClustersController extends Controller
{
    private $data;

    /**
     * CollegesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $this->data['title'] = 'Clusters';

        $clusters = Cluster::all();
        $this->data['clusters'] = $clusters;

        return view('settings.clusters.lists', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $this->data['title'] = 'Create New';

        return view('settings.clusters.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        //validation
        $this->validate($request, Cluster::rules(), Cluster::messages());

        //create object
        $cluster = new Cluster();
        $cluster->fill($request->all());
        $cluster->save();

        return redirect('/clusters')->with('success', 'New cluster created'); //redirect
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $this->data['title'] = 'Edit';

        //role
        $cluster = Cluster::query()->find($id);
        if (!$cluster)
            abort(404);

        $this->data['cluster'] = $cluster;

        return view('settings.clusters.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $this->validate($request, Cluster::rules(), Cluster::messages());

        //create role to store data
        $cluster = Cluster::query()->find($id);
        $cluster->fill($request->all());
        $cluster->save(); //save

        return redirect('/clusters')->with('success', 'Cluster updated'); //redirect
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    //display research area
    function research_areas($cluster_id)
    {
        $research_areas = ResearchArea::where(['cluster_id' => $cluster_id])->get();

        if ($research_areas) {
            echo '<option value="">-- Select --</option>';
            foreach ($research_areas as $area) {
                echo '<option value="' . $area->id . '">' . $area->name . '</option>';
            }
        } else {
            echo '<option value="">-- Select --</option>';
        }
    }
}
