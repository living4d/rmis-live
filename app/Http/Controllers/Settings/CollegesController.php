<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Settings\College;
use App\Models\Settings\Department;

class CollegesController extends Controller
{
    private $data;

    /**
     * CollegesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource
     */
    public function index()
    {
        $this->data['title'] = 'Colleges';

        //colleges
        $colleges = College::where_cond()->get();
        $this->data['colleges'] = $colleges;

        return view('settings.colleges.lists', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->data['title'] = "Register New College";

        return view('settings.colleges.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     */
    public function store(Request $request)
    {
        $this->validate($request, College::rules(), College::messages());

        $college = new College();
        $college->fill($request->all());
        $college->save();

        return redirect('/colleges')->with('success', 'College registered');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     */
    public function edit($id)
    {
        $this->data['title'] = "Edit College";

        $college = College::find($id);

        if (!$college)
            abort(404, 'College does not exist');

        $this->data['college'] = $college;

        return view('settings.colleges.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     */
    public function update(Request $request, $id)
    {
        //validation
        $this->validate($request, College::rules(), College::messages());

        //update
        $college = College::find($id);
        $college->fill($request->all());
        $college->save(); //save

        return redirect('/colleges')->with('success', 'Colleges updated'); //redirect
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //get departments
    function departments($college_id)
    {
        $departments = Department::where(['college_id' => $college_id])->get();

        if ($departments) {
            echo '<option value="">-- Select --</option>';
            foreach ($departments as $dp) {
                echo '<option value="' . $dp->id . '">' . $dp->name . '</option>';
            }
        } else {
            echo '<option value="">-- Select --</option>';
        }
    }
}
