<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Settings\Content;
use App\Perms;

class ContentsController extends Controller
{
    private $data;

    //constructor
    public function __construct()
    {
        $this->middleware('auth');
    }

    //t content
    function index()
    {
        $this->data['title'] = "Edit Content";

        $content = Content::find(1);

        if (!$content)
            abort(404, 'Content not exist');

        $this->data['content'] = $content;

        return view('settings.contents.index', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     */
    public function update(Request $request, $id)
    {
        //validation
        $this->validate($request, Content::rules(), Content::messages());

        //update
        $content = Content::find($id);
        $content->title = $request->input('name');
        $content->content = $request->input('content');
        $content->updated_by = Perms::get_current_user_id();
        $content->updated_at = date('Y-m-d H:i:s');
        $content->save(); //save

        return redirect('/contents')->with('success', 'Content updated'); //redirect
    }
}
