<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Settings\College;
use App\Models\Settings\Department;

class DepartmentsController extends Controller
{
    private $data;

    /**
     * constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['title'] = 'Departments';

        //colleges
        $departments = Department::all();
        $this->data['departments'] = $departments;

        foreach ($this->data['departments'] as $k => $v) {
            $this->data['departments'][$k]->college = College::findOrFail($v->college_id);
        }

        return view('settings.departments.lists', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = "Register New Department";

        //colleges
        $this->data['colleges'] = College::where_cond()->get();

        return view('settings.departments.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Department::rules(), Department::messages());

        $department = new Department();
        $department->fill($request->all());
        $department->save();

        return redirect('/departments')->with('success', 'Department registered');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['title'] = "Edit Department";

        $department = Department::find($id);

        if (!$department)
            abort(404, 'Department does not exist');

        $this->data['department'] = $department;

        //colleges
        $this->data['colleges'] = College::where_cond()->get();

        return view('settings.departments.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $this->validate($request, Department::rules(), Department::messages());

        //update
        $department = Department::find($id);
        $department->fill($request->all());
        $department->save(); //save

        return redirect('/departments')->with('success', 'Department updated'); //redirect
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
