<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use App\Models\Settings\ResearchArea;
use App\Models\Settings\Cluster;

class ResearchAreasController extends Controller
{
    private $data;

    /**
     * CollegesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['title'] = 'Research Areas';

        $research_areas = ResearchArea::all();
        $this->data['research_areas'] = $research_areas;

        foreach ($this->data['research_areas'] as $k => $v) {
            $this->data['research_areas'][$k]->cluster = Cluster::query()->find($v->cluster_id);
        }

        return view('settings.research_areas.lists', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = 'Create New';

        //clusters
        $this->data['clusters'] = Cluster::all();

        return view('settings.research_areas.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $this->validate($request, ResearchArea::rules(), ResearchArea::messages());

        //create object
        $research_area = new ResearchArea();
        $research_area->fill($request->all());
        $research_area->save();

        return redirect('/research_areas')->with('success', 'Research area created');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['title'] = 'Edit';

        //role
        $research_area = ResearchArea::find($id);

        if (!$research_area)
            abort(404);

        $this->data['research_area'] = $research_area;

        //clusters
        $this->data['clusters'] = Cluster::all();

        return view('settings.research_areas.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $this->validate($request, ResearchArea::rules(), ResearchArea::messages());

        //create role to store data
        $research_area = ResearchArea::find($id);
        $research_area->fill($request->all());
        $research_area->save(); //save

        return redirect('/research_areas')->with('success', 'Research area updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
