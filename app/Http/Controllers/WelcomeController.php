<?php

namespace App\Http\Controllers;

use App\CallCategory;
use Illuminate\Http\Request;

use App\Models\Calls\Call;
use App\Models\Settings\Content;

class WelcomeController extends Controller
{
    private $data;

    //default page
    function index()
    {
        $this->data['title'] = 'Research Management Information System (RMIS)';

        //content
        $content = Content::find(1);
        $this->data['content'] = $content;

        //calls
        $calls = Call::where(['status' => 'active'])->orderBy('created_at', 'DESC')->get();
        $this->data['calls'] = $calls;

        return view('index', $this->data);
    }
}
