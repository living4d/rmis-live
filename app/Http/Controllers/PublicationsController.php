<?php

namespace App\Http\Controllers;

use App\Exports\Vet_listExport;
use App\Imports\PublicationsVettedApprovedImport;
use App\Models\Clearances\ClearanceStaff;
use App\Models\Clearances\ClearanceStudent;
use App\Models\Notification;
use App\Models\Publications\PublicationCommittee;
use App\Models\Publications\Publication;
use App\Models\Publications\PublicationBatch;
use App\Models\Publications\PublicationType;
use App\Models\Publications\PublicationVettingApprovedRecommendation;
use App\Models\Settings\College;
use App\Models\Settings\Department;
use App\Perms;
use App\Models\Publications\PublicationFlow;
use App\Models\Publications\PublicationLevel;
use App\Models\Publications\PublicationRecommendation;
use App\Models\Publications\PublicationReview;
use App\Models\Publications\PublicationReviewer;
use App\Role;
use App\User;
use Auth;
use Illuminate\Http\Request;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class PublicationsController extends Controller
{
    private $data;

    //constructor
    public function __construct()
    {
        $this->middleware('auth');
    }

    //brief page on publication
    public function brief()
    {
        $this->data['title'] = 'Publication Description';

        return view('publications.brief');
    }


    //summary
    public function stats()
    {
        $this->data['title'] = 'Publications Summary';
        $dept_id = Auth::user()->department_id;
        $coll_id = Auth::user()->college_id;
        $today = date('Y-m-d');
        $open_batches = PublicationBatch::where('status', 'open')->get();
        $closed_batches = PublicationBatch::where('status', 'closed')->get();
        $open_batch_count = 0;
        $closed_batches_count = 0;

        if (Auth::user()->hasRole("staff")) {

            //vetted
            $vetted_publications = Publication::where('created_by', Auth::user()->id)
                ->where('level', '10')->count();
            $this->data['vetted'] = $vetted_publications;

            //waiting for vetting
            $publications_pending = Publication::where('created_by', Auth::user()->id)
                ->where('level', '<', 10)->get();
            foreach ($closed_batches as $closed_batch){
                foreach ($publications_pending as $item){
                    if ($item->batch_id === $closed_batch->id){
                        $closed_batches_count = $closed_batches_count + 1;
                    }
                }
            }
            $this->data['pending'] = $closed_batches_count;

            //in open batch
            $publications_in_open = Publication::where('created_by', Auth::user()->id)
                ->where('level', '<', 10)->get();
            foreach ($open_batches as $open_batch){
                foreach ($publications_in_open as $item){
                    if ($item->batch_id === $open_batch->id){
                        $open_batch_count = $open_batch_count + 1;
                    }
                }
            }
            $this->data['in_open'] = $open_batch_count;
        }

        if (Auth::user()->hasRole("hod")) {

            $this->data['notifications'] = Publication::where('vetting_status', 'hod')
                ->where('department_id', $dept_id)
                ->oldest('id')->count();
        }

        if (Auth::user()->hasRole("principal")) {
            $this->data['notifications'] = Publication::where('vetting_status', 'principal')
                ->where('college_id', $coll_id)
                ->oldest('id')->count();
        }

        if (Auth::user()->hasRole("drp")) {
            $this->data['notifications'] = Publication::where('vetting_status', 'drp')->oldest('id')->count();
        }

        if (Auth::user()->hasRole("dvc")) {
            $this->data['notifications'] = Publication::where('level', '4')->oldest('id')->count();
        }

        if (Auth::user()->hasRole("vc")) {
            $this->data['notifications'] = Publication::where('level', '5')->oldest('id')->count();
        }

        if (Auth::user()->hasRole("admin")) {
            //vetted
            $vetted_publications = Publication::where('level', '10')->count();
            $this->data['vetted'] = $vetted_publications;

            //waiting for vetting
            $publications_pending = Publication::where('level', '<', 10)->get();
            foreach ($closed_batches as $closed_batch){
                foreach ($publications_pending as $item){
                    if ($item->batch_id === $closed_batch->id){
                        $closed_batches_count = $closed_batches_count + 1;
                    }
                }
            }
            $this->data['pending'] = $closed_batches_count;

            //in open batch
            $publications_in_open = Publication::where('level', '<', 10)->get();
            foreach ($open_batches as $open_batch){
                foreach ($publications_in_open as $item){
                    if ($item->batch_id === $open_batch->id){
                        $open_batch_count = $open_batch_count + 1;
                    }
                }
            }
            $this->data['in_open'] = $open_batch_count;

            $this->data['notifications'] = Publication::oldest('id')->count();
        }

        //render view
        return view('publications.stats')->with('data', $this->data);
    }

    //lists of publications
    public function index()
    {
        Perms::has_allowed('publications', 'lists');
        $this->data['title'] = 'Publications';
        $this->data['table'] = 'publications';
        $dept_id = Auth::user()->department_id;

        $coll_id = Auth::user()->college_id;
        // $coll = College::find($coll_id);

//        $this->data['publications'] = Publication::where('created_by', Auth::user()->id)->oldest('id')->get();
        $this->data['publications'] = Publication::where('created_by', Auth::user()->id)->latest('id')->get();
        foreach ($this->data['publications'] as $key => $value) {
            $this->data['publications'][$key]->type = PublicationType::where('id', $value->publication_type)->first();
        }

        if (Auth::user()->hasRole("hod")) {

            $this->data['notifications'] = Publication::where('vetting_status', 'hod')
                ->where('department_id', $dept_id)
                ->oldest('id')->count();
        }

        if (Auth::user()->hasRole("principal")) {
            $this->data['notifications'] = Publication::where('vetting_status', 'principal')
                ->where('college_id', $coll_id)
                ->oldest('id')->count();
        }

        if (Auth::user()->hasRole("drp")) {
            $this->data['notifications'] = Publication::where('vetting_status', 'drp')->oldest('id')->count();
        }

        if (Auth::user()->hasRole("dvc")) {
            // $this->data['notifications'] = Publication::where('vetting_status','Recommended')
            // ->orWhere('vetting_status','Not Recommended')->oldest('id')->count();
            $this->data['notifications'] = Publication::where('level', '4')->oldest('id')->count();
        }

        if (Auth::user()->hasRole("vc")) {
            // $this->data['notifications'] = Publication::where('vetting_status','Recommended')
            // ->orWhere('vetting_status','Not Recommended')->oldest('id')->count();
            $this->data['notifications'] = Publication::where('level', '5')->oldest('id')->count();
        }

        if (Auth::user()->hasRole("admin")) {
            $this->data['notifications'] = Publication::oldest('id')->count();
        }

        return view('publications.index')->with('data', $this->data);
    }

    //on working
    public function process_publications()
    {
        $this->data['title'] = 'Publications to approve';
        $this->data['table'] = 'process';
        $dept_id = Auth::user()->department_id;

        $coll_id = Auth::user()->college_id;

        if (Auth::user()->hasRole("hod")) {
            $this->data['publications'] = Publication::where('vetting_status', 'hod')
                ->where('department_id', $dept_id)
                ->oldest('id')->get();
            foreach ($this->data['publications'] as $key => $value) {
                $this->data['publications'][$key]->type = PublicationType::where('id', $value->publication_type)->first();
            }

            $this->data['notifications'] = Publication::where('vetting_status', 'hod')
                ->where('department_id', $dept_id)
                ->oldest('id')->count();
        }

        if (Auth::user()->hasRole("principal")) {
            $this->data['publications'] = Publication::where('vetting_status', 'principal')
                ->where('college_id', $coll_id)
                ->oldest('id')->get();

            foreach ($this->data['publications'] as $key => $value) {
                $this->data['publications'][$key]->type = PublicationType::where('id', $value->publication_type)->first();
            }

            $this->data['notifications'] = Publication::where('vetting_status', 'principal')
                ->where('college_id', $coll_id)
                ->oldest('id')->count();
        }

        if (Auth::user()->hasRole("drp")) {
            $this->data['publications'] = Publication::where('vetting_status', 'drp')->oldest('id')->get();

            foreach ($this->data['publications'] as $key => $value) {
                $this->data['publications'][$key]->type = PublicationType::where('id', $value->publication_type)->first();
            }

            $this->data['notifications'] = Publication::where('vetting_status', 'drp')->oldest('id')->count();
        }

        if (Auth::user()->hasRole("dvc")) {
            $this->data['publications'] = Publication::where('level', '4')->oldest('id')->get();

            foreach ($this->data['publications'] as $key => $value) {
                $this->data['publications'][$key]->type = PublicationType::where('id', $value->publication_type)->first();
            }
            $this->data['notifications'] = Publication::where('level', '4')->oldest('id')->count();
        }

        if (Auth::user()->hasRole("vc")) {
            $this->data['publications'] = Publication::where('level', '5')->oldest('id')->get();

            foreach ($this->data['publications'] as $key => $value) {
                $this->data['publications'][$key]->type = PublicationType::where('id', $value->publication_type)->first();
            }
            $this->data['notifications'] = Publication::where('level', '5')->oldest('id')->count();
        }

        if (Auth::user()->hasRole("admin")) {
            $this->data['publications'] = Publication::latest('id')->get();

            foreach ($this->data['publications'] as $key => $value) {
                $this->data['publications'][$key]->type = PublicationType::where('id', $value->publication_type)->first();
            }

            $this->data['notifications'] = Publication::oldest('id')->count();
        }
        return view('publications.index')->with('data', $this->data);
    }

    //register new publication
    public function register()
    {
        Perms::has_allowed('publications', 'register');

        $this->data['title'] = 'Register Publication';
        $this->data['publication_types'] = PublicationType::pluck('name', 'id');
        $this->data['user'] = User::where('id', Auth::user()->id)->first();
        $this->data['dept'] = Department::where('id', $this->data['user']->department_id)->first();
        $this->data['college'] = College::where('id', $this->data['user']->college_id)->first();
        $this->data['batches'] = PublicationBatch::where(['status' => 'open'])->get();

        //college selection pool
        $colleges = College::all();
        $this->data['college_pool'] = $colleges;

        //department selection pool
        $departments = Department::all();
        $this->data['dept_pool'] = $departments;

        //notification defined: this expected to remove bug when hod register publication
        if (Auth::user()->hasRole("hod")) {

            $this->data['notifications'] = Publication::where('vetting_status', 'hod')
                ->where('department_id', $this->data['dept']['id'])
                ->oldest('id')->count();
        }

        if (Auth::user()->hasRole("principal")) {

            $this->data['notifications'] = Publication::where('vetting_status', 'principal')
                ->where('college_id', $this->data['college']['id'])
                ->oldest('id')->count();
        }

        //check if there is open batch
        $open_batch_count = PublicationBatch::where(['status' => 'open'])->count();

        if ($open_batch_count == 0) {
            return redirect()->route('publications')->with('danger', 'No open batch, you cannot submit publication now, please try later!!');
        }
        return view('publications.register')->with('data', $this->data);
    }

    //store new publication
    public function store_publication(Request $request)
    {
        //deal with uploaded file
        if ($request->get('accessibility') === "online") {
            $this->validate($request, ['link' => 'required']);

            $publication_file = $request->file('publication_attachment');
            if ($publication_file) {
                $destinationPath = 'updloads/'; // upload path
                $publication_filename = date('YmdHis') . "pubcopy" . "." . $publication_file->getClientOriginalExtension();
                $publication_file->move($destinationPath, $publication_filename);
            } else {
                $publication_filename = "";
            }
        } else {
            $this->validate($request, ['publication_attachment' => 'required|mimes:pdf']);

            $publication_file = $request->file('publication_attachment');
            if ($publication_file) {
                $destinationPath = 'updloads/'; // upload path
                $publication_filename = date('YmdHis') . "pubcopy" . "." . $publication_file->getClientOriginalExtension();
                $publication_file->move($destinationPath, $publication_filename);
            }
        }

        $this->validate($request, [
            'names' => 'required',
            'year' => 'required',
            'category' => 'required',
            'batch_id' => 'required',
            'publication_type' => 'required',
            'publication_title' => 'required',
            'index' => 'required',
            'processing_charge' => 'required',
            'accessibility' => 'required',
            'publisher' => 'required',
        ]);

        if ($request->get('publication_type') == "1") {
            $this->validate($request, [
                'isbn' => 'required|string|min:6|max:20',
            ]);
        }

        if ($request->get('publication_type') == "2") {
            $this->validate($request, [
                'book_author' => 'required',
                'book_title' => 'required',
                'isbn' => 'required|string|min:6|max:20',
            ]);
        }

        if ($request->get('publication_type') == "3") {
            $this->validate($request, [
                'journal_name' => 'required',
                'issue' => 'required',
                'volume' => 'required',
                'issn' => 'max:20',
                'eissn' => 'required|string|min:6|max:20',
            ]);
        }

        if ($request->get('publication_type') == "4") {
            $this->validate($request, [
                'conference_publication_name' => 'required',
                'volume' => 'required',
                'issn' => 'required|string|min:6|max:20',
                'eissn' => 'max:20',
            ]);
        }

        if ($request->get('publication_type') == "5") {

            $this->validate($request, [
                'consultancy_reg_number' => 'required',
                'receipt_attachment' => 'required|mimes:pdf',
            ]);

            $rcpt_file = $request->file('receipt_attachment');
            if ($rcpt_file) {
                $destinationPath = 'updloads/'; // upload path
                $rcpt_filename = date('YmdHis') . "consrcpt" . "." . $rcpt_file->getClientOriginalExtension();
                $rcpt_file->move($destinationPath, $rcpt_filename);
            }
        }

        if ($request->get('processing_charge') === "yes") {
            $this->validate($request, [
                'currency' => 'required',
                'amount' => 'required|numeric',
            ]);
        }

        $publication = new Publication();
        $publication->names = $request->get('names');
        $publication->rank = $request->get('rank');
        $publication->department_id = $request->get('department_id');
        $publication->college_id = $request->get('college_id');
        $publication->year = $request->get('year');
        $publication->gender = $request->get('gender');
        $publication->category = $request->get('category');
        $publication->batch_id = $request->get('batch_id');
        $publication->publication_type = $request->get('publication_type');
        $publication->publication_title = $request->get('publication_title');
        $publication->publisher = $request->get('publisher');
        $publication->volume = $request->get('volume');
        $publication->issue = $request->get('issue');
        $publication->pages = $request->get('pages');
        $publication->city = $request->get('city');
        $publication->book_author = $request->get('book_author');
        $publication->book_title = $request->get('book_title');
        $publication->journal_name = $request->get('journal_name');
        $publication->conference_publication_name = $request->get('conference_publication_name');
        $publication->accessibility = $request->get('accessibility');
        $publication->index = $request->get('index');
        $publication->link = $request->get('link');
        $publication->processing_charge = $request->get('processing_charge');
        $publication->currency = $request->get('currency');
        $publication->amount = $request->get('amount');
        $publication->consultancy_reg_number = $request->get('consultancy_reg_number');
        $publication->isbn = $request->get('isbn');
        $publication->issn = $request->get('issn');
        $publication->eissn = $request->get('eissn');
        $publication->vetting_status = "hod";
        $publication->level = "1";
        $publication->attach_doc = $publication_filename;
        $publication->created_by = Auth::user()->id;

        //save receipt attachment
        if ($request->get('publication_type') == "5") {

            if (!empty($request->file('receipt_attachment'))) {
                $publication->consultancy_receipt = $rcpt_filename;
            }
        }
        $publication->save();

        $flow = new PublicationFlow();
        $flow->publication_id = $publication->id;
        $flow->publication_level_id = PublicationLevel::where('step', 1)->first()->id;
        $flow->status = "NEW";
        $flow->save();

        //construct notification message to hod
        $message = "The publication from <b><i> $publication->names</i></b> with title <b>$publication->publication_title</b>, has been submitted for vetting.";
        $message .= "<br/> Login in the system by  <a href=" . url('/clearance/process') . ">click here</a> and act on the submission.";
        $message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

        //extract recipient user by role
        $roleHoD = Role::where('name', 'hod')->first();
        $user_hod = DB::table('users')
            ->join('publications', 'users.department_id', '=', 'publications.department_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleHoD->id)
            ->where('publications.department_id', '=', $publication->department_id)
            ->select('users.id')
            ->first();
        //send notification to hod
        $this->send_notification($user_hod->id, $message);

        return redirect()->route('publications')->with('success', 'You have successfully
        submitted your publication to HoD');
    }

    //open publication
    public function show_publication($id)
    {
        $this->data['title'] = 'View publication';
        $this->data['publication'] = Publication::find($id);
        $this->data['review'] = PublicationReview::where('publication_id', $id)->first();
        $this->data['type'] = PublicationType::where('id', $this->data['publication']['publication_type'])->first();
        $this->data['recommendations'] = PublicationRecommendation::where('publication_id', $id)->get();
        $this->data['media_quality'] = PublicationRecommendation::where('publication_id', $id)
            ->where('from', 'drp')->first();

        //check logged user if is assigned to publications
        $dept_id = Auth::user()->department_id;
        $dept = Department::find($dept_id);

        $coll_id = Auth::user()->college_id;
        $coll = College::find($coll_id);

        //to display college and department from ID's
        $college = College::whereId([$this->data['publication']->college_id])->first();
        $this->data['college'] = $college;
        $department = Department::whereId([$this->data['publication']->department_id])->first();
        $this->data['department'] = $department;

        $user_id = Auth::user()->id;
        $this->data['commit'] = PublicationCommittee::where('publication_id', $id)->where('user_id', $user_id)->first();

        if (Auth::user()->hasRole("hod")) {
            $this->data['notifications'] = Publication::where('vetting_status', 'hod')
                ->where('department_id', $dept_id)
                ->oldest('id')->count();
        }

        if (Auth::user()->hasRole("principal")) {
            $this->data['notifications'] = Publication::where('vetting_status', 'principal')
                ->where('college_id', $coll_id)
                ->oldest('id')->count();
        }

        if (Auth::user()->hasRole("drp")) {
            $this->data['notifications'] = Publication::where('vetting_status', 'drp')->oldest('id')->count();
        }

        if (Auth::user()->hasRole("dvc")) {
            // $this->data['notifications'] = Publication::where('vetting_status','Recommended')
            // ->orWhere('vetting_status','Not Recommended')->oldest('id')->count();
            $this->data['notifications'] = Publication::where('level', '4')->oldest('id')->count();
        }

        if (Auth::user()->hasRole("vc")) {
            // $this->data['notifications'] = Publication::where('vetting_status','Recommended')
            // ->orWhere('vetting_status','Not Recommended')->oldest('id')->count();
            $this->data['notifications'] = Publication::where('level', '5')->oldest('id')->count();
        }

        if (Auth::user()->hasRole("admin")) {
            $this->data['notifications'] = Publication::oldest('id')->count();
        }
//        dd($this->data['publication']['recommendation']);
        return view('publications.show')->with('data', $this->data);
    }

    //forward publication to principal
    public function push_principal($id)
    {
        //  dd($id);
        $publication = Publication::find($id);

        $publication->vetting_status = "Principal";
        $publication->level = "2";
        $publication->save();

        //update previous publication flow
        PublicationFlow::where('publication_id', $publication->id)
            ->where('status', "NEW")->update(['status' => "APPROVED"]);

        //save new flow
        $flow = new PublicationFlow();
        $flow->publication_id = $publication->id;
        $flow->publication_level_id = PublicationLevel::where('step', 2)->first()->id;
        $flow->status = "NEW";
        $flow->save();

        return redirect()->route('publications');
    }

    //forward publication to principal
    public function push_drp($id)
    {
        $publication = Publication::find($id);
        $publication->vetting_status = "DRP";
        $publication->level = "3";
        $publication->save();

        //update previous publication flow
        PublicationFlow::where('publication_id', $publication->id)
            ->where('status', "NEW")->update(['status' => "APPROVED"]);

        //save new flow
        $flow = new PublicationFlow();
        $flow->publication_id = $publication->id;
        $flow->publication_level_id = PublicationLevel::where('step', 3)->first()->id;
        $flow->status = "NEW";
        $flow->save();

        return redirect()->route('publications');
    }

    //push all publications at drp level
    public function upc()
    {
        $this->data['publications'] = Publication::where('vetting_status', 'drp')->latest('id')->get();

        foreach ($this->data['publications'] as $pub) {
            $publication = Publication::find($pub->id);
            $publication->vetting_status = "UPC";
            $publication->level = "4";
            $publication->save();
        }

        return redirect()->route('publications');
    }

    //push all publications at drp level
    public function assign_commit()
    {
        $publications = Publication::where('vetting_status', 'upc')->latest('id')->get();
        $user = User::whereHas('roles', function ($q) {
            $q->where('name', 'manager');
        })->first();

        foreach ($publications as $pub) {
            $assign = new PublicationCommittee();
            $assign->user_id = $user['id'];
            $assign->publication_id = $pub['id'];
            $assign->save();
        }

        return redirect()->route('publications');
    }

    //get vet for committee member panel
    public function vet_panel($id)
    {
        $this->data['title'] = 'Vetting decision';
        return view('publications.vet_panel')->with('id', $id);
    }

    //save vetted
    public function save_vetted(Request $request, $id)
    {
        $publication = PublicationCommittee::find($id);

        $publication->vetting_status = $request->get('vetting_status');
        // $publication->level = "3";
        $publication->comments = $request->get('recomendation');
        $publication->peer_review = $request->get('peer_review');
        $publication->international_editor_board = $request->get('international_editor_board');
        $publication->save();

        return redirect()->route('publications');
    }

    //get board panel
    public function board_panel($id)
    {
        $this->data['title'] = 'Board decision';
        return view('publications.board_panel')->with('id', $id);
    }

    //get board panel
    public function save_decisions(Request $request, $id)
    {
        $publication = Publication::find($id);

        $publication->vetting_status = $request->get('vetting_status');
        $publication->level = "4";
        $publication->meeting_date = $request->get('meeting_date');
        $publication->peer_review = $request->get('peer_review');
        $publication->international_editor_board = $request->get('international_editor_board');
        $publication->save();

        $reco = new PublicationRecommendation();

        $reco->publication_id = $id;
        $reco->from = "drp";
        $reco->comments = $request->get('recommendation');
        $reco->save();

        return redirect()->route('publications')->with('success', 'You have successfully submitted vetting results');
    }

    //get forward recommendations panel
    public function forward_panel($id)
    {
        $this->data['title'] = 'Forward with recommendations';
        return view('publications.forward_panel')->with('id', $id);
    }

    //store forward recommendations panel
    public function save_recomend(Request $request, $id)
    {

        $reco = new PublicationRecommendation();
        $publication = Publication::find($id);

        //validate the added site
        $this->validate(
            $request,
            [
                'recommendation' => 'required', //the recommendations must be filled in each stage
            ],
            [
                'recommendation.required' => 'The Recommendation Comment must be filled!',
            ]
        );

        // $next = " ";
        if ($publication->level == "1") {
            $publication->level = "2";
            $publication->vetting_status = "principal";
            // $next = "principal";

            $reco->publication_id = $id;
            $reco->from = "hod";
            $reco->comments = $request->get('recommendation');
        } elseif ($publication->level == "2") {
            $publication->level = "3";
            $publication->vetting_status = "drp";
            //$next = "drp";

            $reco->publication_id = $id;
            $reco->from = "principal";
            $reco->comments = $request->get('recommendation');
        } elseif ($publication->level == "4") {
            $publication->level = "5";
            // $publication->vetting_status = "drp";
            //$next = "vc";

            $reco->publication_id = $id;
            $reco->from = "dvc";
            $reco->comments = $request->get('recommendation');
        }

        $publication->save();
        $reco->save();

        //extract the user by role as recipients of notification email
        $roleDRP = Role::where('name', 'drp')->first();
        $rolePrincipal = Role::where('name', 'principal')->first();
        $roleHoD = Role::where('name', 'hod')->first();

        $user_drp = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleDRP->id)
            ->select('users.id')
            ->first();

        $user_principal = DB::table('users')
            ->join('publications', 'users.college_id', '=', 'publications.college_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $rolePrincipal->id)
            ->where('publications.college_id', '=', $publication->college_id)
            ->select('users.id')
            ->first();

        $user_hod = DB::table('users')
            ->join('publications', 'users.department_id', '=', 'publications.department_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleHoD->id)
            ->where('publications.department_id', '=', $publication->department_id)
            ->select('users.id')
            ->first();

        //construct forward notification message
        $message = "The publicatin submission from <b><i> $publication->names</i></b> with publication title <b>$publication->publication_title</b>, has been forwarded to you for recommendation.";
        $message .= "<br/> Please log in the system by  <a href=" . url('/clearance/process') . ">click here</a> and act on the submission.";
        $message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

        //send notification to respective person
        if ($publication->level == "3") {
            $this->send_notification($user_drp->id, $message);
        } else if ($publication->level == "2") {
            $this->send_notification($user_principal->id, $message);
        } else {
            $this->send_notification($user_hod->id, $message);
        }

        return redirect()->route('publications')->with('success', 'You have successfully forwarded publication');
    }

    //get reverse recommendations panel
    public function back_panel($id)
    {
        $this->data['title'] = 'Board decision';
        return view('publications.recommend_panel')->with('id', $id);
    }

    //store reverse recommendation and change status
    public function save_recomendation(Request $request, $id)
    {
        $publication = Publication::find($id);
        $reco = new PublicationRecommendation();

        //validate the added site
        $this->validate(
            $request,
            [
                'recomendation' => 'required', //the reverse recommendations must be filled in each stage
            ],
            [
                'recomendation.required' => 'The Reverse Comment must be filled!',
            ]
        );

        if ($publication->level == "1") {
            $publication->vetting_status = "rejected";
            $publication->level = "0";
            $prev = "Staff";

            $reco->publication_id = $id;
            $reco->from = "hod";
            $reco->comments = $request->get('recomendation');
        } elseif ($publication->level == "2") {
            $publication->vetting_status = "hod";
            $publication->level = "1";
            $prev = "HOD";

            $reco->publication_id = $id;
            $reco->from = "principal";
            $reco->comments = $request->get('recomendation');
        } elseif ($publication->level == "3") {
            $publication->vetting_status = "principal";
            $publication->level = "2";
            $prev = "Principal";

            $reco->publication_id = $id;
            $reco->from = "drp";
            $reco->comments = $request->get('recomendation');
        }
        $publication->save();
        $reco->save();

        //extract the user by role as recipients of notification email
        $rolePrincipal = Role::where('name', 'principal')->first();
        $roleHoD = Role::where('name', 'hod')->first();

        $user_principal = DB::table('users')
            ->join('publications', 'users.college_id', '=', 'publications.college_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $rolePrincipal->id)
            ->where('publications.college_id', '=', $publication->college_id)
            ->select('users.id')
            ->first();

        $user_hod = DB::table('users')
            ->join('publications', 'users.department_id', '=', 'publications.department_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_user.role_id', '=', $roleHoD->id)
            ->where('publications.department_id', '=', $publication->department_id)
            ->select('users.id')
            ->first();

        //construct reverse notification message
        $message = "The publicatin submission from <b><i> $publication->names</i></b> with publication title <b>$publication->publication_title</b>, has been reversed.";
        $message .= "<br/> Please log in the system by  <a href=" . url('/clearance/process') . ">click here</a> and act on the submission.";
        $message .= " <p>If the link is not clickable, kindly copy it and paste onto browser's address bar.</p>";

        //send notification to respective person
        if ($publication->level == "2") {
            $this->send_notification($user_principal->id, $message);
        } else if ($publication->level == "1") {
            $this->send_notification($user_hod->id, $message);
        } else {
            $this->send_notification($publication->created_by, $message);
        }

        return redirect()->route('publications')->with('success', 'You have successfully
        returned publication to ' . $prev);
    }

    //vc approval
    public function senate_approve($id)
    {
        $publication = Publication::find($id);
        $publication->remark = "approved";
        $publication->approved_date = date('Y-m-d H:i:s');
        $publication->level = "6";
        $publication->save();
        return redirect()->route('generate_certificate', ['id' => $id]);
    }

    //vc disapprove
    public function vc_disapprove($id)
    {
        $publication = Publication::find($id);
        $publication->remark = "rejected";
        $publication->approved_date = date('Y-m-d H:i:s');
        $publication->level = "6";
        $publication->save();

        return redirect()->route('publications');
    }

    //publication certificate
    public function publication_certificate($id)
    {
        $this->data['title'] = 'Certificate';

        $publication = Publication::find($id);

        $this->data['publication'] = $publication;
        $this->data['type'] = PublicationType::where('id', $this->data['publication']['publication_type'])->first();

        $this->data['upc_recomendation'] = PublicationRecommendation::where('publication_id', $id)
            ->where('from', 'drp')->first();

        $this->data['drp'] = User::whereHas('roles', function ($q) {
            $q->where('name', 'drp');
        })->first();

        $this->data['vc'] = User::whereHas('roles', function ($q) {
            $q->where('name', 'vc');
        })->first();

        $user = User::find($publication->created_by);
        $this->data['user'] = $user;
        return view('publications.certificate')->with('data', $this->data);
    }

    //get assign reviewers panel
    public function assign_panel($id)
    {
        Perms::has_allowed('publications', 'assign_panel');

        $this->data['title'] = 'Assign reviewers';
        $this->data['publication'] = Publication::find($id);
        $this->data['users'] = User::whereHas('roles', function ($q) {
            $q->where('name', 'staff');
        })->orderBy('first_name', 'asc')->get();

        return view('publications.assign_panel')->with('data', $this->data);
    }

    //get assign reviewers panel
    public function save_reviewer(Request $request, $id)
    {
        //first reviewer
        $review = new PublicationReviewer();
        $review->publication_id = $id;
        $review->user_id = $request->get('reviewer1');
        $review->save();

        //second reviewer
        $review = new PublicationReviewer();
        $review->publication_id = $id;
        $review->user_id = $request->get('reviewer2');
        $review->save();

        $publication = Publication::find($id);
        $publication->review_status = "review";
        $publication->save();

        return redirect()->route('publications')->with('success', 'You have successfully
        assigned publication reviewers');
    }

    //get review tool(form)
    public function review_tool($id)
    {
        Perms::has_allowed('publications', 'review_tool');

        $this->data['title'] = 'reviewer publication';
        $this->data['publication'] = Publication::find($id);

        return view('publications.review_form')->with('data', $this->data);
    }

    //save reviews results
    public function save_review(Request $request, $id)
    {
        $review = new PublicationReview();
        $review->publication_id = $id;
        $review->user_id = Auth::user()->id;
        $review->coverage = $request->get('mark1');
        $review->originality = $request->get('mark2');
        $review->contribution = $request->get('mark3');
        $review->relevance_to_academic = $request->get('mark4');
        $review->relevance_to_individual = $request->get('mark5');
        $review->presentation = $request->get('mark6');
        $review->overall = $request->get('overall');
        $review->general = $request->get('general');
        $review->promotion = $request->get('promo');
        $review->comment = $request->get('comment');
        $review->status = "reviewed";
        $review->save();

        $pub = Publication::find($id);
        $pub->review_status = "reviewed";
        $pub->save();

        return redirect()->route('publications')->with('success', 'You have successfully
        submitted publication review results');
    }

    //edit
    public function edit($id)
    {
        // Perms::has_allowed('publications', 'edit');

        $this->data['title'] = 'Editing publication';
        $publication = Publication::find($id);

        //identify author college
        $author_college = College::whereId($publication->college_id)->first();
        $this->data['author_college'] = $author_college;

        //identify author department
        $author_dept = Department::whereId($publication->department_id)->first();
        $this->data['author_dept'] = $author_dept;

        $this->data['pub_type'] = PublicationType::find($publication->publication_type);

        $this->data['publication'] = $publication;
        // $this->data['publication_types'] = PublicationType::pluck('name', 'id');
        $this->data['publication_types'] = PublicationType::all();

        if (Auth::user()->hasRole("admin")) {
            $this->data['notifications'] = Publication::oldest('id')->count();
        }

        if ($this->data['publication']->vetting_status === "hod" || $this->data['publication']->vetting_status === "NEW" || $this->data['publication']->vetting_status === "rejected") {
            return view('publications.edit')->with('data', $this->data);
        }
        return redirect('/publications')->with('danger', 'Cant be edited,Publication is already on progress');
    }

    //delete
    public function delete($id)
    {
        Perms::has_allowed('publications', 'delete');

        $publication = Publication::find($id);
        if ($publication->vetting_status === "hod" || $publication->vetting_status === "NEW" || $publication->vetting_status === "rejected") {
            $publication->delete($id);

            return redirect('/publications')->with('success', 'Publication is deleted');
        }
        return redirect('/publications')->with('danger', 'Cant be deleted,Publication is already on progress');
    }

    //store edited publication
    public function update_publication(Request $request, $id)
    {
        //deal with uploaded file
        if (!empty($request->file('publication_attachment'))) {
            $this->validate($request, ['publication_attachment' => 'required|mimes:pdf']);
        }

        if ($request->get('accessibility') === "online") {
            $this->validate($request, ['link' => 'required']);

            $pub_file = $request->file('publication_attachment');
            if ($pub_file) {
                $destinationPath = 'updloads/'; // upload path
                $pub_filename = date('YmdHis') . "pubcopy" . "." . $pub_file->getClientOriginalExtension();
                $pub_file->move($destinationPath, $pub_filename);
            } else {
                $pub_filename = "";
            }
        } else {
            //$this->validate($request, ['publication_attachment' => 'required|mimes:pdf']);

            $pub_file = $request->file('publication_attachment');
            if ($pub_file) {
                $destinationPath = 'updloads/'; // upload path
                $pub_filename = date('YmdHis') . "pubcopy" . "." . $pub_file->getClientOriginalExtension();
                $pub_file->move($destinationPath, $pub_filename);
            }
        }

        $this->validate($request, [
            'year' => 'required',
            'category' => 'required',
            'publication_type' => 'required',
            'publication_title' => 'required',
            'index' => 'required',
            'processing_charge' => 'required',
            'accessibility' => 'required',
            'publisher' => 'required',
        ]);

        if ($request->get('publication_type') == "1") {
            $this->validate($request, [
                'isbn' => 'required',
            ]);
        }

        if ($request->get('publication_type') == "2") {
            $this->validate($request, [
                'book_author' => 'required',
                'book_title' => 'required',
                'isbn' => 'required',
            ]);
        }

        if ($request->get('publication_type') == "3") {
            $this->validate($request, [
                'journal_name' => 'required',
                'issue' => 'required',
                'volume' => 'required',
                'eissn' => 'required',
            ]);
        }

        if ($request->get('publication_type') == "4") {
            $this->validate($request, [
                'conference_publication_name' => 'required',
                'volume' => 'required',
                'issn' => 'required',
            ]);
        }

        if ($request->get('publication_type') == "5") {

            $this->validate($request, [
                'consultancy_reg_number' => 'required',
                //'receipt_attachment' => 'required|mimes:pdf',
            ]);

            $rcpt_files = $request->file('receipt_attachment');
            if ($rcpt_files) {
                $destinationPath = 'updloads/'; // upload path
                $rcpt_filename = date('YmdHis') . "consrcpt" . "." . $rcpt_files->getClientOriginalExtension();
                $rcpt_files->move($destinationPath, $rcpt_filename);
            }
        }

        if ($request->get('processing_charge') === "yes") {
            $this->validate($request, [
                'currency' => 'required',
                'amount' => 'required',
            ]);
        }

        $publication = Publication::find($id);
        $publication->year = $request->get('year');
        $publication->category = $request->get('category');
        $publication->publication_type = $request->get('publication_type');
        $publication->publication_title = $request->get('publication_title');
        $publication->publisher = $request->get('publisher');
        $publication->volume = $request->get('volume');
        $publication->issue = $request->get('issue');
        $publication->pages = $request->get('pages');
        $publication->city = $request->get('city');
        $publication->book_author = $request->get('book_author');
        $publication->book_title = $request->get('book_title');
        $publication->isbn = $request->get('isbn');
        $publication->issn = $request->get('issn');
        $publication->eissn = $request->get('eissn');
        $publication->journal_name = $request->get('journal_name');
        $publication->conference_publication_name = $request->get('conference_publication_name');
        $publication->accessibility = $request->get('accessibility');
        $publication->index = $request->get('index');
        $publication->link = $request->get('link');
        $publication->processing_charge = $request->get('processing_charge');
        $publication->currency = $request->get('currency');
        $publication->amount = $request->get('amount');
        $publication->updated_by = Auth::user()->id;

        //save publication attachment
        if (!empty($request->file('publication_attachment'))) {
            $publication->attach_doc = $pub_filename;
        }

        //save receipt attachment
        if ($request->get('publication_type') == "5") {

            if (!empty($request->file('receipt_attachment'))) {
                $publication->consultancy_receipt = $rcpt_filename;
            }
        }

        $publication->save();

        return redirect()->route('publications')->with('success', 'You have successfully
        updated your publication');
    }

    public function reportPublication()
    {

        $data = Publication::all();

        $dept_id = Auth::user()->department_id;
        $dept = Department::find($dept_id);

        $coll_id = Auth::user()->college_id;
        $coll = College::find($coll_id);

        if (Auth::user()->hasRole("hod")) {
            $this->data['notifications'] = Publication::where('vetting_status', 'hod')
                ->where('department_id', $dept['id'])
                ->oldest('id')->count();
        }

        if (Auth::user()->hasRole("principal")) {
            $this->data['notifications'] = Publication::where('vetting_status', 'principal')
                ->where('college_id', $coll['id'])
                ->oldest('id')->count();
        }

        if (Auth::user()->hasRole("drp")) {
            $this->data['notifications'] = Publication::where('vetting_status', 'drp')->oldest('id')->count();
        }

        if (Auth::user()->hasRole("dvc")) {
            $this->data['notifications'] = Publication::where('level', '4')->oldest('id')->count();
        }

        if (Auth::user()->hasRole("vc")) {
            $this->data['notifications'] = Publication::where('level', '5')->oldest('id')->count();
        }

        if (Auth::user()->hasRole("admin")) {
            $this->data['notifications'] = Publication::oldest('id')->count();
        }

        // return($data);
        $proff = Publication::where('category', 'Professorial')->count();
        $nonProff = Publication::where('category', 'Non-Professorial')->count();
        return (view('./publications.reports.report_publication', compact("data", "proff", "nonProff")));
    }


    //generate matrix for vetting list
    function vet_list_select(Request $request)
    {
        $this->data['title'] = 'Publs Vet Matrix';

        //batch for matrix should be closed
        $this->data['batches'] = PublicationBatch::where(['status' => 'closed'])->get();

        //post submit
        if (isset($_POST['submit'])) {
            //validation
            $this->validate(
                $request,
                [
                    'report_type' => 'required',
                    'batch_id' => 'required_if:report_type,==,batch',
                ]
            );

            //report type
            $report_type = $request->input('report_type');

            if ($report_type === 'complete-report') {
                //reports
                $vet_list = Publication::select(
                    'publications.id',
                    'publications.category',
                    'colleges.name AS College',
                    'departments.name AS Department',
                    'publications.names AS Name',
                    'publications.publication_title',
                    'publications.year',
                    'publication_types.name AS publication_type',
                    'publications.journal_name',
                    'publications.conference_publication_name',
                    'publications.book_title',
                    'publications.publisher',
                    'publications.volume',
                    'publications.issue',
                    'publications.pages',
                    'publications.isbn',
                    'publications.issn',
                    'publications.eissn',
                    'publications.consultancy_reg_number',
                    'publications.accessibility',
                    'publications.index',
                    'publications.link',
                    'publications.processing_charge',
                    'publications.currency',
                    'publications.amount',
                    'publications.batch_id'
                )
                    ->join('publication_types', 'publications.publication_type', '=', 'publication_types.id')
                    ->join('colleges', 'publications.college_id', '=', 'colleges.id')
                    ->join('departments', 'publications.department_id', '=', 'departments.id')
                    // ->where(['publications.level' => 3])
                    ->orderBy('publications.category', 'asc')
                    ->orderBy('colleges.name', 'asc')
                    ->orderBy('departments.name', 'asc')
                    ->orderBy('publications.names', 'asc')
                    ->orderBy('publication_types.name', 'asc')
                    ->get();

                $this->data['start_at'] = null;
                $this->data['end_at'] = null;
            } else if ($report_type === 'batch') {
                $batch = $request->input('batch_id');
                $category = $request->input('category');

                //reports
                $vet_list = Publication::select(
                    'publications.id',
                    'publications.category',
                    'colleges.name AS College',
                    'departments.name AS Department',
                    'publications.names AS Name',
                    'publications.publication_title',
                    'publications.year',
                    'publication_types.name AS publication_type',
                    'publications.journal_name',
                    'publications.conference_publication_name',
                    'publications.book_title',
                    'publications.publisher',
                    'publications.volume',
                    'publications.issue',
                    'publications.pages',
                    'publications.isbn',
                    'publications.issn',
                    'publications.eissn',
                    'publications.consultancy_reg_number',
                    'publications.accessibility',
                    'publications.index',
                    'publications.link',
                    'publications.processing_charge',
                    'publications.currency',
                    'publications.amount',
                    'publications.batch_id'
                )
                    ->join('publication_types', 'publications.publication_type', '=', 'publication_types.id')
                    ->join('colleges', 'publications.college_id', '=', 'colleges.id')
                    ->join('departments', 'publications.department_id', '=', 'departments.id')
                    // ->where(['publications.category' => $category, 'publications.level' => 3, 'publications.batch_id' => $batch])
                    ->where(['publications.category' => $category, 'publications.batch_id' => $batch])
                    ->orderBy('publications.category', 'asc')
                    ->orderBy('colleges.name', 'asc')
                    ->orderBy('departments.name', 'asc')
                    ->orderBy('publications.names', 'asc')
                    ->orderBy('publication_types.name', 'asc')
                    ->get();

                $this->data['batch'] = $batch;
                $this->data['category'] = $category;
            }

            //reports
            $this->data['report_type'] = $report_type;
            $this->data['vet_list'] = $vet_list;
        } else {
            $this->data['start_at'] = null;
            $this->data['end_at'] = null;
            $this->data['report_type'] = null;
            $this->data['vet_list'] = [];
        }

        //notification
        if (Auth::user()->hasRole("admin")) {
            $this->data['notifications'] = Publication::oldest('id')->count();
        }
        //render view
        return view('publications.reports.generate_vet_list', ['data' => $this->data]);
    }


    /*
     * This function is for displaying publications in a batch to be updated.
     * is used mainly for vetting feedback
     */
    function vetted_list_import()
    {
        $this->data['title'] = 'Vetted Import';

        //code to display target db contents
        $this->data['verdict'] = PublicationVettingApprovedRecommendation::latest('publication_id')->get();

        //notification
        if (Auth::user()->hasRole("admin")) {
            $this->data['notifications'] = Publication::oldest('id')->count();
        }
        //render view
        return view('publications.imports.vet_import')->with('data', $this->data);
    }


    /*
     * This function is for importing vetting results and updating the publications database.
     * is used mainly for vetting feedback
     */
    function store_vet_excel(Request $request)
    {
        $request->validate([
            'select_file' => 'required|mimes:xls,xlsx'
        ]);
        $file = $request->file('select_file')->store('imports');
        Excel::import(new PublicationsVettedApprovedImport, $file);

        /* Deal with bulk recommendation assignment */
        $publications = Publication::all(); //identify the recently uploaded record
        $imported = PublicationVettingApprovedRecommendation::all();
        foreach ($publications as $publication){
            if (empty($publication->recommendation)){ //take care of unfilled recommendation only
                foreach ($imported as $imported_record){
                    if ($publication->id === $imported_record->publication_id){
                        $publication->recommendation = $imported_record->recommendation;
                        $publication->save();
                    }
                }
            }
        }
        return back()->with('success', 'Excel file imported successfully and Publications Recommendations updated.');
    }


    public function export()
    {
        return Excel::download(new Vet_listExport, 'invoices.xlsx');
    }


    //list publication types
    public function publication_type()
    {
        $this->data['title'] = 'Publication type';

        //publication types
        $types = PublicationType::all();
        $this->data['pub_type'] = $types;

        if (Auth::user()->hasRole("admin")) {
            $this->data['notifications'] = Publication::oldest('id')->count();
        }

        return view('publications.publication_types', ['data' => $this->data]);
    }

    public function publication_type_create()
    {
        $this->data['title'] = "Register New Type";

        if (Auth::user()->hasRole("admin")) {
            $this->data['notifications'] = Publication::oldest('id')->count();
        }

        return view('publications.publication_types.create', ['data' => $this->data]);
    }

    public function publ_type_store(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required'
        ]);

        $publ_type = new PublicationType();
        $publ_type->id = $request->get('id');
        $publ_type->name = $request->get('name');
        $publ_type->save();

        return redirect('/publications/type')->with('success', 'publication type registered');
    }


    public function publication_type_edit($id)
    {
        $this->data['title'] = "Edit Publication Type";

        $pub_type = PublicationType::find($id);
        $this->data['pub_type'] = $pub_type;

        if (!$pub_type)
            abort(404, 'Type does not exist');

        if (Auth::user()->hasRole("admin")) {
            $this->data['notifications'] = Publication::oldest('id')->count();
        }
        return view('publications.publication_types.edit', ['data' => $this->data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     */
    public function publications_type_update(Request $request, $id)
    {
        //validation
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required'
        ]);

        //update
        $publ_type = PublicationType::find($id);
        $publ_type->id = $request->get('id');
        $publ_type->name = $request->get('name');
        $publ_type->save();

        return redirect('/publications/type')->with('success', 'publication type updated'); //redirect
    }


    //list of batches
    public function batches()
    {
        $this->data['title'] = 'Publication type';

        //batches
        $batches = PublicationBatch::all();
        $this->data['batch'] = $batches;

        if (Auth::user()->hasRole("admin")) {
            $this->data['notifications'] = Publication::oldest('id')->count();
        }
        return view('publications.batches.list', ['data' => $this->data]);
    }

    public function batch_create()
    {
        $this->data['title'] = "Create New Batch";

        if (Auth::user()->hasRole("admin")) {
            $this->data['notifications'] = Publication::oldest('id')->count();
        }

        return view('publications.batches.create', ['data' => $this->data]);
    }

    public function batch_store(Request $request)
    {
        $this->validate($request, [
            'batch_number' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);

        $batch = new PublicationBatch();
        $batch->batch_number = $request->get('batch_number');
        $batch->start_date = $request->get('start_date');
        $batch->end_date = $request->get('end_date');
        $batch->status = 'open';
        $batch->created_at = date('Y-m-d H:i:s');
        $batch->created_by = Auth::user()->id;
        $batch->updated_at = date('Y-m-d H:i:s');
        $batch->updated_by = Auth::user()->id;
        $batch->save();

        return redirect('/publications/batches/')->with('success', 'batch created');
    }


    public function batch_edit($id)
    {
        $this->data['title'] = "Edit Batch";

        $batch = PublicationBatch::find($id);
        $this->data['batch'] = $batch;

        if (!$batch)
            abort(404, 'Batch does not exist');

        if (Auth::user()->hasRole("admin")) {
            $this->data['notifications'] = Publication::oldest('id')->count();
        }

        return view('publications.batches.edit', ['data' => $this->data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     */
    public function batch_update(Request $request, $id)
    {
        //validation
        $this->validate($request, [
            'batch_number' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);

        //update
        $batch = PublicationBatch::find($id);
        $batch->batch_number = $request->get('batch_number');
        $batch->start_date = $request->get('start_date');
        $batch->end_date = $request->get('end_date');
        $batch->updated_at = date('Y-m-d H:i:s');
        $batch->updated_by = Auth::user()->id;
        $batch->save();

        return redirect('/publications/batches')->with('success', 'batch updated'); //redirect
    }


    public function batch_publ_status_edit($id)
    {
        $this->data['title'] = "Edit Batch";

        $batch = PublicationBatch::find($id);
        $this->data['batch'] = $batch;

        if (!$batch)
            abort(404, 'Batch does not exist');

        if (Auth::user()->hasRole("admin")) {
            $this->data['notifications'] = Publication::oldest('id')->count();
        }

        return view('publications.batches.update_batch_publications_status', ['data' => $this->data]);
    }


    public function batch_publications_status_update(Request $request, $id)
    {
        //validation
        $this->validate($request, [
            'status' => 'required'
        ]);

        //update
        $batch = PublicationBatch::find($id);
        $batch->status = $request->get('status');
        $batch->updated_at = date('Y-m-d H:i:s');
        $batch->updated_by = Auth::user()->id;

        if ($batch->save()) {
            if ($batch->status === 'open') {
                return redirect('/publications/batches')->with('success', 'batch updated'); //redirect
            } elseif ($batch->status === 'closed') {
                //it is closed, publication state to change to closed
                return redirect('/publications/batches')->with('success', 'batch closed'); //redirect
            } else {
                //it is vetted, publication status to change to vetted
                $vetted_publications = Publication::where(['batch_id' => $batch->id])->get();

                foreach ($vetted_publications as $vetted_publ) {
                    $vetted_publ->level = 10;
                    $vetted_publ->vetting_status = 'completed';
                    $vetted_publ->save();
                }
                return redirect('/publications/batches')->with('success', 'batch updated'); //redirect
            }
        }
    }


    //send notification to user
    function send_notification($user_id, $message)
    {
        $user = User::find($user_id);

        //construct email for sending
        $message_id = Notification::generate_message_id();

        //user
        $name = $user->first_name . ' ' . $user->surname;
        $email = $user->email;
        $phone = $user->phone;

        //insert notification
        $notification = new Notification(
            [
                'message' => $message,
                'message_id' => $message_id,
                'user_id' => $user->id,
                'email' => $email,
                'phone' => Notification::cast_mobile($phone),
                'app_status' => 'PENDING',
                'sms_status' => 'PENDING',
                'email_status' => 'PENDING',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => Auth::user()->id
            ]
        );
        $notification->save();

        //create array
        $array = ['name' => $name, 'email' => $email, 'message' => $message];

        if (Notification::send_email($array))
            $data = ['email_status' => 'SENT'];
        else
            $data = ['email_status' => 'REJECTED'];

        Notification::where(['id' => $notification->id])->update($data);
    }
}
