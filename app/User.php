<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Auth;

use App\Models\Settings\College;
use App\Models\Settings\Department;

/**
 * App\User
 *
 * @property int $id
 * @property string $first_name
 * @property string|null $middle_name
 * @property string|null $surname
 * @property string $phone
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $username
 * @property string $password
 * @property int|null $level_id
 * @property string|null $college_id
 * @property string|null $department_id
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCollegeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUsername($value)
 * @mixin \Eloquent
 * @property string|null $institution
 * @property string|null $academic_qualification
 * @property string|null $specialization
 * @property string|null $experience
 * @property string|null $research_area_ids
 * @property string|null $cv
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAcademicQualification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereExperience($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereInstitution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereResearchAreaIds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSpecialization($value)
 */
class User extends Authenticatable
{
    use Notifiable;

    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //where condition
    static function where_cond()
    {
        $users = static::orderBy('created_at');

        if (!Auth::user()->hasRole('admin') || !Auth::user()->hasRole('manager')) {
            $user_id = Auth::user()->id;

            $user = User::query()->find($user_id);

            if ($user->level_id == 2)
                $users = $users->where('users.college_id', $user->college_id);
            else if ($user->level_id == 3)
                $users = $users->where('users.department_id', $user->department_id);
        }

        return $users;
    }


    //search all users
    public static function search_all_users($department_id = null, $keyword = null)
    {
        //users
        $users = static::where_cond();

        if ($department_id != null)
            $users = $users->where('users.department_id', $department_id);


        if ($keyword != null) {
            $users = $users->where(function ($query) use ($keyword) {
                $query->orWhere('first_name', 'LIKE', "%$keyword%")
                    ->orWhere('middle_name', 'LIKE', "%$keyword%")
                    ->orWhere('surname', 'LIKE', "%$keyword%");
            });
        }

        return $users->paginate(100);
    }

    //department
    public function college()
    {
        return $this->belongsTo(College::class);
    }

    //department
    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    //roles
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * @param string|array $roles
     * @return bool
     */
    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) ||
                abort(401, 'This action is unauthorized.');
        }

        return $this->hasRole($roles) ||
            abort(401, 'This action is unauthorized.');
    }

    /**
     * Check multiple roles
     * @param array $roles
     * @return bool
     */
    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    /**
     * Check one role
     * @param string $role
     * @return bool
     */
    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }

    //get hod
    public static function get_hod($dep_id)
    {
        $user = DB::table('users')
            ->select('users.id', 'users.first_name', 'users.middle_name', 'users.surname', 'users.email', 'users.phone', 'users.rank')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where(['users.department_id' => $dep_id])
            ->where(['roles.name' => 'hod'])
            ->first();
        return $user;
    }

    //get principal
    public static function get_principal($col_id)
    {
        $user = DB::table('users')
            ->select('users.id', 'users.first_name', 'users.middle_name', 'users.surname', 'users.email', 'users.phone', 'users.rank')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where(['users.college_id' => $col_id])
            ->where(['roles.name' => 'principal'])
            ->first();
        return $user;
    }

    //get drp
    public static function get_drp()
    {
        $user = DB::table('users')
            ->select('users.id', 'users.first_name', 'users.middle_name', 'users.surname', 'users.email', 'users.phone', 'users.rank')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where(['roles.name' => 'drp'])
            ->first();
        return $user;
    }

    //get dvc research
    public static function get_dvc_research()
    {
        $user = DB::table('users')
            ->select('users.id', 'users.first_name', 'users.middle_name', 'users.surname', 'users.email', 'users.phone', 'users.rank')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where(['roles.name' => 'dvc_research'])
            ->first();
        return $user;
    }
}
