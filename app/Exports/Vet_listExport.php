<?php

namespace App\Exports;

use App\Models\Publications\Publication;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Http\Controllers\PublicationsController;

// class Vet_listExport implements FromCollection
// {
//     /**
//     * @return \Illuminate\Support\Collection
//     */
//     public function collection()
//     {
//         return Publication::all();
//     }
// }


class Vet_listExport implements FromView
{
    public function view(): View
    {



        $vet_list = Publication::select(
            'publications.category',
            'colleges.name AS College',
            'departments.name AS Department',
            'publications.names AS Name',
            'publications.publication_title',
            'publications.year',
            'publication_types.name AS publication_type',
            'publications.media_name',
            'publications.journal_name',
            'publications.conference_publication_name',
            'publications.book_title',
            'publications.publisher',
            'publications.volume',
            'publications.issue',
            'publications.pages',
            'publications.accessibility',
            'publications.index',
            'publications.link',
            'publications.processing_charge',
            'publications.currency',
            'publications.amount'
        )
            ->join('publication_types', 'publications.publication_type', '=', 'publication_types.id')
            ->join('colleges', 'publications.college_id', '=', 'colleges.id')
            ->join('departments', 'publications.department_id', '=', 'departments.id')
            ->where(['publications.category' => 'Non-Professorial', 'publications.level' => 3])
            //->groupBy('call_application_objectives.id')
            ->orderBy('publications.category', 'asc')
            ->orderBy('colleges.name', 'asc')
            ->orderBy('departments.name', 'asc')
            ->orderBy('publications.names', 'asc')
            ->get();




        return view('publications.reports.generate_non_prof_vet_list', [
            'invoices' => Publication::all(),
            'data' => $vet_list
            // 'invoices' => $vet_list
        ]);
    }
}
