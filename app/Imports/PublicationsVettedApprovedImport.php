<?php

namespace App\Imports;

use App\Models\Publications\PublicationVettingApprovedRecommendation;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Throwable;

class PublicationsVettedApprovedImport implements ToModel, WithHeadingRow, SkipsOnError, WithValidation
{
    use SkipsErrors;
    /**
    * @param array $row
    *
    * @return Model|null
    */
    public function model(array $row)
    {
        return new PublicationVettingApprovedRecommendation([
            'publication_id' => $row['publication_id'],
            'category' => $row['category'],
            'publication_title' => $row['publication_title'],
            'batch' => $row['batch'],
            'recommendation' => $row['recommendation']
        ]);
    }

    /**
     * @param Throwable $e
     * @return void
     */
    public function onError(Throwable $e)
    {
        // TODO: Implement onError() method.
    }

    public function rules(): array
    {
        return [
          '*.publication_id' => ['unique:publication_vetting_approved_recommendations,publication_id']
        ];
    }
}
