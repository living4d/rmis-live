<?php

namespace App\Imports;

use App\Models\TestRecommendation;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Throwable;

class TestsImport implements ToModel, WithHeadingRow, SkipsOnError, WithValidation
{
    use SkipsErrors;
    /**
    * @param array $row
    *
    * @return Model|null
    */
    public function model(array $row)
    {
        return new TestRecommendation([
            'id' => $row['id'],
            'name' => $row['name'],
            't_status' => $row['t_status']
        ]);
    }

    /**
     * @param Throwable $e
     * @return void
     */
    public function onError(Throwable $e)
    {
        // TODO: Implement onError() method.
    }

    public function rules(): array
    {
        return [
          '*.id' => ['unique:test_recommendations,id']
        ];
    }
}
