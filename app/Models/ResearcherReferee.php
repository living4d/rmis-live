<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ResearcherReferee
 *
 * @property int $id
 * @property int $researcher_id
 * @property string|null $name
 * @property string|null $title
 * @property string|null $email
 * @property string|null $phone
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearcherReferee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearcherReferee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearcherReferee query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearcherReferee whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearcherReferee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearcherReferee whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearcherReferee wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearcherReferee whereResearcherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearcherReferee whereTitle($value)
 * @mixin \Eloquent
 */
class ResearcherReferee extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
}
