<?php

namespace App\Models\Clearances;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ResearchSite
 *
 * @property int $id
 * @property int|null $clearance_student_id
 * @property int|null $clearance_staff_id
 * @property string|null $location_site
 * @property string|null $organization
 * @property string|null $title
 * @property string|null $address
 * @property string|null $region
 * @property string|null $district
 * @property string|null $ward
 * @property string|null $village
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearchSite newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearchSite newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearchSite query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearchSite whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearchSite whereClearanceStaffId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearchSite whereClearanceStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearchSite whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearchSite whereDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearchSite whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearchSite whereLocationSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearchSite whereOrganization($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearchSite whereRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearchSite whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearchSite whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearchSite whereVillage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearchSite whereWard($value)
 * @mixin \Eloquent
 */
class ResearchSite extends Model
{
    public $table = 'research_sites';
}
