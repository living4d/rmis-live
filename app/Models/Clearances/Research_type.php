<?php

namespace App\Models\Clearances;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Research_type
 *
 * @property int $id
 * @property string|null $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Research_type newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Research_type newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Research_type query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Research_type whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Research_type whereName($value)
 * @mixin \Eloquent
 */
class Research_type extends Model
{
    //
}
