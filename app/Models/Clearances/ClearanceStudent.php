<?php

namespace App\Models\Clearances;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ClearanceStudent
 *
 * @property int $id
 * @property string|null $names
 * @property string|null $gender
 * @property string|null $nationality
 * @property string|null $reg_no
 * @property string $year_entry
 * @property int|null $department
 * @property string|null $degree_program
 * @property string|null $duration
 * @property string|null $program_mode
 * @property string|null $contact
 * @property int|null $supervisor
 * @property string|null $research_title
 * @property int|null $research_type
 * @property string|null $commencement_date
 * @property string|null $completion_date
 * @property float|null $research_budget
 * @property string|null $fund_source
 * @property string|null $sponsor_name
 * @property string|null $proposal_attachment
 * @property string|null $delivery_mode
 * @property string|null $level
 * @property string|null $approval_status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereApprovalStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereCommencementDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereCompletionDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereDegreeProgram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereDeliveryMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereDepartment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereFundSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereNames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereNationality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereProgramMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereProposalAttachment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereRegNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereResearchBudget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereResearchTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereResearchType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereSponsorName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereSupervisor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStudent whereYearEntry($value)
 * @mixin \Eloquent
 */
class ClearanceStudent extends Model
{
    public $table = 'clearance_students';
}
