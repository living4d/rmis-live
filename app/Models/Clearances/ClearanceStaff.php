<?php

namespace App\Models\Clearances;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ClearanceStaff
 *
 * @property int $id
 * @property string|null $names
 * @property string|null $title
 * @property string|null $department
 * @property string|null $academic_unit
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $project_reg_no
 * @property string|null $research_title
 * @property string|null $research_type
 * @property string|null $start_date
 * @property string|null $end_date
 * @property float|null $fund_amount
 * @property string|null $fund_source
 * @property string|null $fund_name
 * @property string|null $other_researcher
 * @property string|null $agreement_attachment
 * @property string|null $level
 * @property string|null $approval_status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereAcademicUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereAgreementAttachment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereApprovalStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereDepartment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereFundAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereFundName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereFundSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereNames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereOtherResearcher($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereProjectRegNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereResearchTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereResearchType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceStaff whereUpdatedBy($value)
 * @mixin \Eloquent
 */
class ClearanceStaff extends Model
{
    public $table = 'clearance_staffs';
}
