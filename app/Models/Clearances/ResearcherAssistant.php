<?php

namespace App\Models\Clearances;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ResearcherAssistant
 *
 * @property int $id
 * @property int|null $site_id
 * @property string|null $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearcherAssistant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearcherAssistant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearcherAssistant query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearcherAssistant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearcherAssistant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearcherAssistant whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearcherAssistant whereSiteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ResearcherAssistant whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ResearcherAssistant extends Model
{
    public $table = 'assistants';
}
