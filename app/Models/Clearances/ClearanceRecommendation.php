<?php

namespace App\Models\Clearances;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ClearanceRecommendation
 *
 * @property int $id
 * @property int|null $clearance_student_id
 * @property int|null $clearance_staff_id
 * @property string|null $comment
 * @property string|null $from
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceRecommendation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceRecommendation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceRecommendation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceRecommendation whereClearanceStaffId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceRecommendation whereClearanceStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceRecommendation whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceRecommendation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceRecommendation whereFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceRecommendation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClearanceRecommendation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ClearanceRecommendation extends Model
{
    public $table = 'clearance_recommendations';
}
