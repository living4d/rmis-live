<?php

namespace App\Models\Clearances;

use Illuminate\Database\Eloquent\Model;

/**
 * App\AssignedResearcher
 *
 * @property int $id
 * @property int|null $researcher_id
 * @property int|null $site_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\AssignedResearcher newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\AssignedResearcher newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\AssignedResearcher query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\AssignedResearcher whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\AssignedResearcher whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\AssignedResearcher whereResearcherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\AssignedResearcher whereSiteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\AssignedResearcher whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AssignedResearcher extends Model
{
    public $table = 'assigned_researchers';
}
