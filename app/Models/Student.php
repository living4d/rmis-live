<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Student
 *
 * @property int $id
 * @property string|null $first_name
 * @property string|null $middle_name
 * @property string|null $surname
 * @property string|null $gender
 * @property string|null $nationality
 * @property string|null $reg_number
 * @property string|null $year_of_entry
 * @property string|null $department
 * @property string|null $college
 * @property string|null $program
 * @property int|null $duration
 * @property string|null $phone
 * @property string|null $email
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereCollege($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereDepartment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereNationality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereProgram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereRegNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereYearOfEntry($value)
 * @mixin \Eloquent
 */
class Student extends Model
{
    //
}
