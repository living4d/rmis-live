<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Cluster
 *
 * @property int $id
 * @property string|null $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Cluster newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Cluster newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Cluster query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Cluster whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Cluster whereName($value)
 * @mixin \Eloquent
 */
class Cluster extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    //has many Relationship
    function researchAreas()
    {
        return $this->hasMany(ResearchArea::class, 'cluster_id');
    }

    //casts
    protected $casts = [
        'name' => 'string',
    ];

    //rules
    public static function rules(): array
    {
        return [
            'name' => 'required|string'
        ];
    }

    //messages
    public static function messages(): array
    {
        return [
            'name.required' => 'Cluster is required',
        ];
    }
}
