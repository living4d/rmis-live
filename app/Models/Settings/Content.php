<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Department
 *
 * @property int $id
 * @property string $title
 * @property string|null $content
 * @property int|null $updated_by
 * @property date|null updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Content query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Content whereCollegeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Content whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Content whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Content whereShortName($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Content newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Content newQuery()
 */

class Content extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    //casts
    protected $casts = [
        'title' => 'string',
        'content' => 'string'
    ];

    //rules
    public static function rules(): array
    {
        return [
            'name' => 'bail|required|string',
            'content' => 'required|string'
        ];
    }

    //messages
    public static function messages(): array
    {
        return [
            'name.required' => 'Title is required',
            'content.required' => 'Content is required'
        ];
    }
}
