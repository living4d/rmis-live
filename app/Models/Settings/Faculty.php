<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Faculty
 *
 * @property int $id
 * @property string $name
 * @property int|null $department_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Faculty newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Faculty newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Faculty query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Faculty whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Faculty whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Faculty whereName($value)
 * @mixin \Eloquent
 */
class Faculty extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
}
