<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

/**
 * App\EvaluationCriteria
 *
 * @property int $id
 * @property string $attribute
 * @property int|null $percentage
 * @property float|null $weight
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\EvaluationCriteria query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\EvaluationCriteria whereAttribute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\EvaluationCriteria whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\EvaluationCriteria wherePercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\EvaluationCriteria whereWeight($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\EvaluationCriteria newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\EvaluationCriteria newQuery()
 */
class EvaluationCriteria extends Model
{
    protected $table = 'evaluation_criteria';
    protected $guarded = ['id'];
    public $timestamps = false;
}
