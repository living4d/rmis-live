<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;

/**
 * App\Department
 *
 * @property int $id
 * @property string $name
 * @property string|null $short_name
 * @property int|null $college_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Department query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Department whereCollegeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Department whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Department whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Department whereShortName($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\Department newQuery()
 */
class Department extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    //where condition
    static function where_cond()
    {
        $departments = static::orderBy('name', 'ASC');

        if (!Auth::user()->hasRole('admin') || !Auth::user()->hasRole('manager')) {
            $user_id = Auth::user()->id;

            $user = User::find($user_id);

            if ($user->level_id == 2)
                $departments = $departments->where('departments.college_id', $user->college_id);
            else if ($user->level_id == 3)
                $departments = $departments->where('departments.id', $user->department_id);
        }

        return $departments;
    }

    //casts
    protected $casts = [
        'name' => 'string',
        'short_name' => 'string',
        'college_id' => 'int'
    ];

    //rules
    public static function rules(): array
    {
        return [
            'name' => 'bail|required|string',
            'short_name' => 'bail|required|string',
            'college_id' => 'required'
        ];
    }

    //messages
    public static function messages(): array
    {
        return [
            'name.required' => 'Department name is required',
            'short_name.required' => 'Short name is required',
            'college_id.required' => 'College is required'
        ];
    }
}
