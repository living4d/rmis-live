<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class NumberSeries extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
}
