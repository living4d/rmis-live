<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Research_type
 *
 * @property int $id
 * @property string|null $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\ResearchType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\ResearchType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\ResearchType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\ResearchType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\ResearchType whereName($value)
 * @mixin \Eloquent
 */
class ResearchType extends Model
{
    //
}
