<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;

/**
 * App\College
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $short_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\College query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\College whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\College whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\College whereShortName($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\College newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\College newQuery()
 */
class College extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    //where condition
    static function where_cond()
    {
        $colleges = static::orderBy('name', 'ASC');

        if (!Auth::user()->hasRole('admin') || !Auth::user()->hasRole('manager')) {
            $user_id = Auth::user()->id;

            $user = User::find($user_id);

            if ($user->level_id == 2)
                $colleges = $colleges->where('colleges.id', $user->college_id);
        }

        return $colleges;
    }

    //casts
    protected $casts = [
        'name' => 'string',
        'short_name' => 'string'
    ];

    //rules
    public static function rules(): array
    {
        return [
            'name' => 'bail|required|string',
            'short_name' => 'bail|required|string'
        ];
    }

    //messages
    public static function messages(): array
    {
        return [
            'name.required' => 'College name is required',
            'short_name.required' => 'Short name is required'
        ];
    }
}
