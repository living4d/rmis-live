<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

/**
 * App\EvaluationSubCriteria
 *
 * @property int $id
 * @property int $criteria_id
 * @property string $sub_criteria
 * @property float|null $weight
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\EvaluationSubCriteria newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\EvaluationSubCriteria newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\EvaluationSubCriteria query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\EvaluationSubCriteria whereCriteriaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\EvaluationSubCriteria whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\EvaluationSubCriteria whereSubCriteria($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\EvaluationSubCriteria whereWeight($value)
 * @mixin \Eloquent
 */
class EvaluationSubCriteria extends Model
{
    protected $table = 'evaluation_sub_criteria';
    protected $guarded = ['id'];
    public $timestamps = false;
}
