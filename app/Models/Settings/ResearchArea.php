<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Research_area
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $cluster_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\ResearchArea newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\ResearchArea newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\ResearchArea query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\ResearchArea whereClusterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\ResearchArea whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings\ResearchArea whereName($value)
 * @mixin \Eloquent
 */
class ResearchArea extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    //casts
    protected $casts = [
        'name' => 'string',
        'cluster_id' => 'int'
    ];

    //rules
    public static function rules(): array
    {
        return [
            'name' => 'required|string',
            'cluster_id' => 'required|int'
        ];
    }

    //messages
    public static function messages(): array
    {
        return [
            'name.required' => 'Research area is required',
            'cluster_id.required' => 'Cluster is required'
        ];
    }
}
