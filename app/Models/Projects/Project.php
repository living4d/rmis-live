<?php

namespace App\Models\Projects;

use Illuminate\Database\Eloquent\Model;
use App\Models\Calls\CallApplication;

class Project extends Model
{
    protected $guarded = ['id'];

    //check if project available
    public static function is_project_available($app_id)
    {
        $project = Project::where('application_id', '=', $app_id)->first();

        if ($project)
            return true;
        else
            return false;
    }

    //belong relationship
    public function application()
    {
        return $this->belongsTo(CallApplication::class, 'application_id');
    }

    //project activities
    public function activities()
    {
        return $this->hasMany(ProjectActivity::class, 'project_id');
    }

    //fund requests
    function fundRequests()
    {
        return $this->hasMany(ProjectFundRequest::class, 'project_id')->orderBy('created_at', 'DESC');
    }
}
