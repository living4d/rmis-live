<?php

namespace App\Models\Projects;

use Illuminate\Database\Eloquent\Model;

class ProjectActivityReportAttachment extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    function activityReport()
    {
        return $this->belongsTo(ProjectActivityReport::class, 'activity_report_id');
    }
}
