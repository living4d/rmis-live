<?php

namespace App\Models\Projects;

use Illuminate\Database\Eloquent\Model;

class ProjectFundRequestActivity extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    //belong to
    function projectFundRequest()
    {
        return $this->belongsTo(ProjectFundRequest::class, 'project_fund_request_id');
    }

    //activity
    function activity()
    {
        return $this->belongsTo(ProjectActivity::class, 'activity_id');
    }
}
