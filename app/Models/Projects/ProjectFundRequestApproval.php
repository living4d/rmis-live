<?php

namespace App\Models\Projects;

use Illuminate\Database\Eloquent\Model;

class ProjectFundRequestApproval extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    //belong to
    function projectFundRequest()
    {
        return $this->belongsTo(ProjectFundRequest::class, 'project_fund_request_id');
    }
}
