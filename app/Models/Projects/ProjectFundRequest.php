<?php

namespace App\Models\Projects;

use Illuminate\Database\Eloquent\Model;

class ProjectFundRequest extends Model
{
    protected $guarded = ['id'];
    public $timestamps = true;

    //belong to
    function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    //activities
    function activities()
    {
        return $this->hasMany(ProjectFundRequestActivity::class, 'project_fund_request_id');
    }


    //attachments
    function fundRequestAttachments()
    {
        return $this->hasMany(ProjectFundRequestAttachment::class, 'project_fund_request_id');
    }

    //approvals
    function fundRequestApprovals()
    {
        return $this->hasMany(ProjectFundRequestApproval::class, 'project_fund_request_id');
    }
}
