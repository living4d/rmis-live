<?php

namespace App\Models\Projects;

use App\Models\Projects\ProjectFundRequestApproval;
use Illuminate\Database\Eloquent\Model;

class ProjectFundRequestComments extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    //belong to
    function projectFundRequestApproval()
    {
        return $this->belongsTo(ProjectFundRequestApproval::class, 'project_fund_request_id');
    }

    //activity
    function projectFundRequests()
    {
        return $this->belongsTo(projectFundRequests::class, 'project_id');
    }
}
