<?php

namespace App\Models\Projects;

use Illuminate\Database\Eloquent\Model;

class ProjectActivityReport extends Model
{
    protected $guarded = ['id'];
    public $timestamps = true;

    //belong to
    function activity()
    {
        return $this->belongsTo(ProjectActivity::class, 'activity_id');
    }

    function attachments()
    {
        return $this->hasMany(ProjectActivityReportAttachment::class, 'activity_report_id');
    }
}
