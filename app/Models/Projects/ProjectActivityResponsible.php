<?php

namespace App\Models\Projects;

use App\Models\Calls\CallApplicationPartner;
use App\User;
use Illuminate\Database\Eloquent\Model;

class ProjectActivityResponsible extends Model
{
    public $table = 'project_activity_responsible';
    protected $guarded = ['id'];
    public $timestamps = false;

    //activity
    function activity()
    {
        return $this->belongsTo(ProjectActivity::class, 'activity_id');
    }

    //user
    function partner()
    {
        return $this->belongsTo(CallApplicationPartner::class, 'call_partner_id');
    }
}
