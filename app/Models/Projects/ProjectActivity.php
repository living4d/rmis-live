<?php

namespace App\Models\Projects;

use App\Models\Calls\CallApplicationObjective;
use App\Models\Calls\CallApplicationPartner;
use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 */
class ProjectActivity extends Model
{
    protected $guarded = ['id'];
    public $timestamps = true;

    //responsible
    function responsible()
    {
        return $this->hasMany(ProjectActivityResponsible::class, 'activity_id');
    }

    //project
    function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    //objective
    function objective()
    {
        return $this->belongsTo(CallApplicationObjective::class, 'objective_id');
    }

    //activity type
    function type()
    {
        return $this->belongsTo(ProjectActivityType::class, 'activity_type_id');
    }

    //fund requests
    function funds()
    {
        return $this->hasMany(ProjectActivityFund::class, 'activity_id');
    }

    //activity reports
    function activityReport()
    {
        return $this->hasOne(ProjectActivityReport::class, 'activity_id');
    }
}
