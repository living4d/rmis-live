<?php

namespace App\Models\Projects;

use Illuminate\Database\Eloquent\Model;

class ProjectActivityType extends Model
{
    public $table = 'project_activity_types';
    protected $guarded = ['id'];
    public $timestamps = false;
}
