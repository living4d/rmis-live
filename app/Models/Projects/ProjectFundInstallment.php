<?php

namespace App\Models\Projects;

use Illuminate\Database\Eloquent\Model;

class ProjectFundInstallment extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
}
