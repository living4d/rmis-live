<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Researcher
 *
 * @property int $id
 * @property string $type
 * @property string $full_name
 * @property string|null $title
 * @property string|null $institution
 * @property string|null $phone
 * @property string $email
 * @property string|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Researcher newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Researcher newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Researcher query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Researcher whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Researcher whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Models\Researcher whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Researcher whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Researcher whereInstitution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Researcher wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Researcher whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Researcher whereType($value)
 * @mixin \Eloquent
 */
class Researcher extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
}
