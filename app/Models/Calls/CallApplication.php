<?php

namespace App\Models\Calls;

use App\Models\Settings\ResearchArea;
use App\Models\Settings\ResearchType;
use App\Models\Settings\Cluster;
use App\Rules\MaxWordsRule;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use DB;

/**
 * App\Models\Calls\CallApplication
 *
 * @property int $id
 * @property int $call_id
 * @property int $principal_investigator
 * @property string|null $project_title
 * @property int|null $call_category_id
 * @property int|null $research_type_id
 * @property int|null $call_cluster_id
 * @property int|null $call_research_area_id
 * @property string|null $introduction
 * @property string|null $problem_statement
 * @property string|null $rationale
 * @property string|null $general_objective
 * @property string|null $literature_review
 * @property string|null $methodology
 * @property string|null $research_output
 * @property string|null $capability
 * @property string|null $sustainability_measures
 * @property string|null $management_structure
 * @property string|null $implementation_plan
 * @property string|null $source_fund
 * @property string|null $attachment
 * @property int|null $completion
 * @property string|null $status
 * @property int|null $created_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereAttachment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereCallCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereCallClusterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereCallId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereCallResearchAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereCapability($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereCompletion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereGeneralObjective($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereImplementationPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereIntroduction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereLiteratureReview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereManagementStructure($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereMethodology($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication wherePrincipalInvestigator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereProblemStatement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereProjectTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereRationale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereResearchOutput($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereResearchTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereSourceFund($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereSustainabilityMeasures($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplication whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CallApplication extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    //where condition
    public static function where_cond()
    {
        $user_id = Auth::user()->id; //user id
        $user = User::find($user_id); //user

        //call application
        $call_apps = static::orderBy('calls.created_at', 'DESC');

        if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('manager') || Auth::user()->hasRole('DHRA') || Auth::user()->hasRole('drp') || Auth::user()->hasRole('DVC') || Auth::user()->hasRole('VC')) {
            //do nothing
        } else if (Auth::user()->hasRole('principal')) {
            //todo: filter per college
            $call_apps = $call_apps->where('users.college_id', $user->college_id);
        } else if (Auth::user()->hasRole('hod')) {
            //filter per department
            $call_apps = $call_apps->where('users.department_id', $user->department_id);
        } else if (Auth::user()->hasRole('evaluators')) {
            //show only assigned call application
            $call_apps = $call_apps->join('call_application_evaluators', 'call_applications.id', '=', 'call_application_evaluators.application_id')
                ->where('call_application_evaluators.evaluator_id', '=', $user->id);
        } else if (Auth::user()->hasRole('program_coordinator')) {
            $call_apps = $call_apps->where('calls.program_coordinator', $user->id);
        } else if (Auth::user()->hasRole('program_accountant')) {
            $call_apps = $call_apps->where('calls.program_accountant', $user->id);
        } else if (Auth::user()->hasRole('staff')) {
            //filter per user
            $call_apps = $call_apps->where('call_applications.principal_investigator', $user->id);
        }

        return $call_apps;
    }


    //count call applications
    public static function count_all($call_id = null, $department_id = null, $cluster_id = null, $start_at = null, $end_at = null)
    {
        $call_applications = static::where_cond();

        if ($call_id != null)
            $call_applications = $call_applications->where('calls.id', $call_id);

        if ($department_id != null)
            $call_applications = $call_applications->where('users.department_id', $department_id);

        if ($cluster_id != null)
            $call_applications = $call_applications->where('call_applications.call_cluster_id', '=', $cluster_id);

        if ($start_at != null && $end_at != null) {
            $start_at = date('Y-m-d', strtotime($start_at));
            $end_at = date('Y-m-d', strtotime($end_at));

            $call_applications = $call_applications->whereBetween('call_applications.created_at', [$start_at, $end_at]);
        }

        return $call_applications
            ->select('calls.id as call_id', 'calls.title as call_title', 'call_applications.id as call_app_id', 'call_applications.principal_investigator', 'call_applications.project_title', 'call_applications.approval_status',  'call_applications.created_at')
            ->join('users', 'users.id', '=', 'call_applications.principal_investigator')
            ->join('calls', 'calls.id', '=', 'call_applications.call_id')
            ->where(['call_applications.status' => 'COMPLETE'])->count();
    }

    //search call applications
    public static function get_all($call_id = null, $department_id = null, $cluster_id = null, $start_at = null, $end_at = null)
    {
        $call_applications = static::where_cond();

        if ($call_id != null)
            $call_applications = $call_applications->where('calls.id', $call_id);

        if ($department_id != null)
            $call_applications = $call_applications->where('users.department_id', $department_id);

        if ($cluster_id != null)
            $call_applications = $call_applications->where('call_applications.call_cluster_id', '=', $cluster_id);

        if ($start_at != null && $end_at != null) {
            $start_at = date('Y-m-d', strtotime($start_at));
            $end_at = date('Y-m-d', strtotime($end_at));

            $call_applications = $call_applications->whereBetween('call_applications.created_at', [$start_at, $end_at]);
        }

        return $call_applications
            ->select('calls.id as call_id', 'calls.title as call_title', 'call_applications.id as call_app_id', 'call_applications.principal_investigator', 'call_applications.project_title', 'call_applications.approval_status', 'call_applications.eval_status', 'call_applications.winner', 'call_applications.created_at')
            ->join('users', 'users.id', '=', 'call_applications.principal_investigator')
            ->join('calls', 'calls.id', '=', 'call_applications.call_id')
            ->orderBy('calls.created_at', 'DESC')
            ->orderBy('call_applications.created_at', 'ASC')
            ->where(['call_applications.status' => 'COMPLETE'])->paginate(500);
    }

    //count staff applications
    public static function count_staff_applications($call_id = null)
    {
        $user_id = Auth::user()->id; //user id

        //call application
        $call_apps = static::orderBy('calls.created_at', 'DESC');

        if ($call_id != null)
            $call_apps = $call_apps->where('calls.id', $call_id);

        return $call_apps
            ->join('calls', 'calls.id', '=', 'call_applications.call_id')
            ->orderBy('calls.created_at', 'DESC')
            ->orderBy('call_applications.created_at', 'ASC')
            ->where(['call_applications.status' => 'COMPLETE', 'call_applications.principal_investigator' => $user_id])->count();
    }


    //get staff applications
    public static function get_staff_applications($call_id = null)
    {
        $user_id = Auth::user()->id; //user id

        //call application
        $call_apps = static::orderBy('calls.created_at', 'DESC');

        if ($call_id != null)
            $call_apps = $call_apps->where('calls.id', $call_id);

        return $call_apps
            ->select('calls.id as call_id', 'calls.title as call_title', 'call_applications.id as call_app_id', 'call_applications.principal_investigator', 'call_applications.project_title', 'call_applications.approval_status', 'call_applications.eval_status', 'call_applications.winner', 'call_applications.created_at')
            ->join('calls', 'calls.id', '=', 'call_applications.call_id')
            ->orderBy('calls.created_at', 'DESC')
            ->orderBy('call_applications.created_at', 'ASC')
            ->where(['call_applications.status' => 'COMPLETE', 'call_applications.principal_investigator' => $user_id])->paginate(500);
    }

    /*=====================================================
    Dashboard queries
    =====================================================*/
    //number of complete applications
    public static function number_of_complete_applications()
    {
        $call_applications = static::where_cond();

        return $call_applications
            ->select('calls.id as call_id', 'calls.title as call_title', 'call_applications.id as call_app_id', 'call_applications.principal_investigator', 'call_applications.project_title', 'call_applications.created_at')
            ->join('users', 'users.id', '=', 'call_applications.principal_investigator')
            ->join('calls', 'calls.id', '=', 'call_applications.call_id')
            ->where(['call_applications.status' => 'COMPLETE'])->count();
    }

    //number of won application
    public static function number_of_won_applications()
    {
        $call_applications = static::where_cond();

        return $call_applications
            ->select('calls.id as call_id', 'calls.title as call_title', 'call_applications.id as call_app_id', 'call_applications.principal_investigator', 'call_applications.project_title', 'call_applications.created_at')
            ->join('users', 'users.id', '=', 'call_applications.principal_investigator')
            ->join('calls', 'calls.id', '=', 'call_applications.call_id')
            ->where(['call_applications.status' => 'COMPLETE', 'call_applications.winner' => 1])->count();
    }

    //rules
    public static function rules(): array
    {
        return [
            'call_category_id' => 'required',
            // 'research_type_id' => 'required',
            'cluster_id' => 'required',
            'research_area_id' => 'required',
            'summary' => ['required', new MaxWordsRule(300)],
            'introduction' => 'required|string',
            'problem_statement' => 'required|string',
            'rationale' => 'required|string',
            'general_objective' => 'required|string',
            'specific_objective.*' => 'required',
            'literature_review' => ['required', new MaxWordsRule()],
            'methodology' => 'required|string',
            'research_output' => 'required|string',
            'sustainability_measures' => 'required|string',
            'management_structure' => 'required|string',
            'implementation_plan' => 'required|string',
            // 'dissemination_plan' => 'required|string'
        ];
    }

    //messages
    public static function messages(): array
    {
        return [
            'call_category_id.required' => 'Call category is required',
            // 'research_type_id.required' => 'Research type is required',
            'cluster_id.required' => 'Call cluster is required',
            'research_area_id.required' => 'Research area is required',
            'summary.required' => 'Summary is required',
            'introduction.required' => 'Introduction is required',
            'problem_statement.required' => 'Problem statement is required',
            'rationale.required' => 'Rationale is required',
            'general_objective.required' => 'General objective is required',
            'specific_objective.*.required' => 'Specific objective is required',
            'literature_review.required' => 'Literature review is required',
            'methodology.required' => 'Methodology is required',
            'research_output.required' => 'Expected research output is required',
            'sustainability_measures' => 'Sustainability measures is required',
            'management_structure.required' => 'Management structure is required',
            'implementation_plan.required' => 'Implementation plan is required',
            // 'dissemination_plan' => 'Implementation plan is required for innovation projects'
        ];
    }

    //call
    public function call()
    {
        return $this->belongsTo(Call::class, 'call_id');
    }

    //application category
    public function callCategory()
    {
        return $this->belongsTo(CallCategory::class, 'call_category_id');
    }

    //research type
    public function researchType()
    {
        return $this->belongsTo(ResearchType::class, 'research_type_id');
    }

    //call cluster
    public function  cluster()
    {
        return $this->belongsTo(Cluster::class, 'call_cluster_id');
    }

    //research area
    public function researchArea()
    {
        return $this->belongsTo(ResearchArea::class, 'call_research_area_id');
    }

    //objectives
    public function objectives()
    {
        return $this->hasMany(CallApplicationObjective::class, 'application_id');
    }

    //budgets
    public function budgets()
    {
        return $this->hasMany(CallApplicationBudget::class, 'application_id');
    }

    //belong relationship
    public function pi()
    {
        return $this->belongsTo(User::class, 'principal_investigator');
    }

    //partners
    public function partners()
    {
        return $this->hasMany(CallApplicationPartner::class, 'application_id');
    }

    //hasMany call application evaluators
    public function evaluators()
    {
        return $this->hasMany(CallApplicationEvaluator::class, 'application_id');
    }

    //call evaluation results
    public function callEvaluationResult()
    {
        return $this->hasOne(callEvaluationResult::class, 'application_id');
    }

    //get application evaluators
    public static function get_evaluators($call_id, $cluster_id)
    {
        $apps = static::where(['call_id' => $call_id, 'call_cluster_id' => $cluster_id])->get();

        $evaluators = [];
        if ($apps) {
            foreach ($apps as $app) {
                //find application evaluators
                $app_evaluators = DB::table('call_application_evaluators')
                    ->select('evaluator_id')
                    ->where('application_id', '=', $app->id)
                    ->get();

                foreach ($app_evaluators as $val) {
                    if (!in_array($val->evaluator_id, $evaluators))
                        array_push($evaluators, $val->evaluator_id);
                }
            }
        }

        //display evaluators in html
        if ($evaluators) {
            $i = 1;
            foreach ($evaluators as $val) {
                $user = User::find($val);
                echo $i . '. ' . $user->first_name . ' ' . substr($user->middle_name, 1) . ' ' . $user->surname;
                echo '<br />';
                $i++;
            }
        } else {
            echo 'No evaluator';
        }
    }
}
