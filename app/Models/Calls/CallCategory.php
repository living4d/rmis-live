<?php

namespace App\Models\Calls;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Calls\CallCategory
 *
 * @property int $id
 * @property int $call_id
 * @property string|null $title
 * @property string|null $description
 * @property string $currency
 * @property float $budget_ceiling
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallCategory whereBudgetCeiling($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallCategory whereCallId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallCategory whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallCategory whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallCategory whereTitle($value)
 * @mixin \Eloquent
 */
class CallCategory extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
}
