<?php

namespace App\Models\Calls;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Calls\CallApplicationResult
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationResult newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationResult newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationResult query()
 * @mixin \Eloquent
 */

class CallApplicationResult extends Model
{
    protected $table = 'call_evaluation_results';
    protected $guarded = ['id'];
    public $timestamps = false;
}
