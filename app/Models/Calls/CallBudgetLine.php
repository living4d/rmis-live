<?php

namespace App\Models\Calls;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Calls\CallBudgetLine
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallBudgetLine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallBudgetLine newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallBudgetLine query()
 * @mixin \Eloquent
 */
class CallBudgetLine extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
}
