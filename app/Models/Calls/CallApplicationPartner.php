<?php

namespace App\Models\Calls;

use App\Models\Researcher;
use App\Models\Student;
use App\User;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Calls\CallApplicationPartner
 *
 * @property int $id
 * @property int $call_id
 * @property int $application_id
 * @property int $partner_id
 * @property string $partner_type
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationPartner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationPartner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationPartner query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationPartner whereApplicationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationPartner whereCallId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationPartner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationPartner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationPartner wherePartnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationPartner wherePartnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationPartner whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationPartner whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CallApplicationPartner extends Model
{
    protected $guarded = ['id'];

    //belong relationship
    public function user()
    {
        return $this->belongsTo(User::class, 'partner_id');
    }

    //belong relationship
    public function student()
    {
        return $this->belongsTo(Student::class, 'partner_id');
    }

    //belong relationship
    public function researcher()
    {
        return $this->belongsTo(Researcher::class, 'partner_id');
    }
}
