<?php

namespace App\Models\Calls;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Calls\CallApplicationApproval
 *
 * @property int $id
 * @property string $call_id
 * @property string $application_id
 * @property string $approval_level_id
 * @property int|null $status
 * @property int|null $created_by
 * @property string|null $created_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApproval newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApproval newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApproval query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApproval whereApplicationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApproval whereApprovalLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApproval whereCallId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApproval whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApproval whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApproval whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApproval whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApproval whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApproval whereUpdatedBy($value)
 * @mixin \Eloquent
 */
class CallApplicationApproval extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
}
