<?php

namespace App\Models\Calls;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Calls\CallResearcher
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallResearcher newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallResearcher newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallResearcher query()
 * @mixin \Eloquent
 */
class CallResearcher extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
}
