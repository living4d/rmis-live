<?php

namespace App\Models\Calls;

use App\Models\Settings\Cluster;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Calls\CallCluster
 *
 * @property int $id
 * @property int $call_id
 * @property int $cluster_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallCluster newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallCluster newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallCluster query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallCluster whereCallId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallCluster whereClusterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallCluster whereId($value)
 * @mixin \Eloquent
 */
class CallCluster extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    //belong relationship
    public function cluster()
    {
        return $this->belongsTo(Cluster::class, 'cluster_id');
    }
}
