<?php

namespace App\Models\Calls;

use Illuminate\Database\Eloquent\Model;

use DB;
use Auth;
use User;


/**
 * App\Models\Calls\CallApplicationEvaluation
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationEvaluation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationEvaluation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationEvaluation query()
 * @mixin \Eloquent
 */
class CallApplicationEvaluation extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    //where condition
    public static function where_cond()
    {
    }


    //calculate summation of points
    public static function getSummationPoints($call_id, $app_id, $evaluator_id)
    {
        $call_eval = DB::table('call_application_evaluations')
            ->where(['call_id' => $call_id, 'application_id' => $app_id, 'evaluator_id' => $evaluator_id])->get();

        $total_points = 0;
        if ($call_eval) {
            foreach ($call_eval as $val) {
                $total_points += $val->points;
            }
        }
        return $total_points;
    }

    //calculate average points
    public static function getAveragePoints($call_id, $app_id)
    {
        $call_eval = DB::table('call_application_evaluators')
            ->where(['call_id' => $call_id, 'application_id' => $app_id])->get();

        $avg_points = 0;
        if ($call_eval) {
            $i = 0;
            $total_points = 0;
            foreach ($call_eval as $val) {
                $total_points += $val->points;

                $i++;
            }
            $avg_points = ($total_points / $i);
        }
        return $avg_points;
    }

    //rules
    public static function rules(): array
    {
        return [
            'score.*' => 'required|numeric|gte:0|lte:100',
            //'comments.*' => 'required'
        ];
    }

    //messages
    public static function messages(): array
    {
        return [
            'score.*.required' => 'Score required',
            'score.*.numeric' => 'Score must be required',
            'score.*.gte:0' => 'Score should be greater than or equal',
            'score.*.lte:100' => 'Score should be less than or equal',
            //'comments.*.required' => 'Comments required'
        ];
    }

    //get evaluation content
    public static function get_evaluation_content($action, $app_id)
    {
        $content = '';

        //call app
        $call_app = CallApplication::where(['id' => $app_id])->first();

        //project title
        if ($action === 'title') {
            if ($call_app) {
                $content .= "<h3 class='font-weight-bold'>Project Title</h3>";
                $content .= "<p>$call_app->project_title</p>";
            } else {
                $content .= "No project title";
            }
        }

        //project summary
        if ($action === 'summary') {
            if ($call_app) {
                $content .= "<h3 class='font-weight-bold'>Summary</h3>";
                $content .= "<p>$call_app->summary</p>";
            } else {
                $content .= "No project summary";
            }
        }

        //project introduction
        if ($action === 'introduction') {
            if ($call_app) {
                $content .= "<h3 class='font-weight-bold'>Introduction</h3>";
                $content .= "<p>$call_app->introduction</p>";
            } else {
                $content .= "No project introduction";
            }
        }

        //project problem_statement
        if ($action === 'problem_statement') {
            if ($call_app) {
                $content .= "<h3 class='font-weight-bold'>Problem Statement</h3>";
                $content .= "<p>$call_app->problem_statement</p>";
            } else {
                $content .= "No project problem statement";
            }
        }

        //objectives
        if ($action === 'objectives') {
            if ($call_app) {
                //general objective
                $content .= "<h3 class='font-weight-bold'>General Objective</h3>";
                $content .= "<p>$call_app->general_objective</p>";
                $content .= "<div class='mt-2'></div>";

                //specific objectives
                $content .= "<h4 class='font-weight-bold'>Specific Objective</h4>";
                $objectives = CallApplicationObjective::where(['application_id' => $app_id])->get();
                if ($objectives) {
                    $i = 1;
                    foreach ($objectives as $v) {
                        $content .= "<p>$i. $v->objective</p>";
                        $i++;
                    }
                }
            }
        }

        //literature review
        if ($action === 'literature_review') {
            if ($call_app) {
                $content .= "<h3 class='font-weight-bold'>Literature Review</h3>";
                $content .= "<p>$call_app->literature_review</p>";
            } else {
                $content .= 'No literature review';
            }
        }

        //methodology
        if ($action === 'methodology') {
            if ($call_app) {
                $content .= "<h3 class='font-weight-bold'>Methodology</h3>";
                $content .= "<p>$call_app->methodology</p>";
            } else {
                $content .= 'No methodology';
            }
        }

        //research_output
        if ($action === 'research_output') {
            if ($call_app) {
                $content .= "<h3 class='font-weight-bold'>Research Output</h3>";
                $content .= "<p>$call_app->research_output</p>";
            } else {
                $content .= 'No research output';
            }
        }

        //project budget
        if ($action === 'budget') {
            if ($call_app) {
                $call_category = CallCategory::find($call_app->call_category_id);

                $currency = ($call_category) ? $call_category->currency : 'TZS';
                $budget = ($call_category) ? number_format($call_category->budget_ceiling) : 0;

                $content .= "<h3 class='font-weight-bold'>Project Budget</h3>";
                $content .= "<p><strong>Budget Ceiling : $currency $budget</strong></p>";
                $call_app_budgets = CallApplicationObjective::where(['application_id' => $app_id])->get();

                if ($call_app_budgets) {
                    $content .= "<table width='100%' class='table table-bordered'>
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th width='75%'>Budget</th>
                                                <th>Amount (TZS)</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
                    $i = 1;
                    $sum = 0;
                    foreach ($call_app_budgets as $val) {
                        $budget = number_format($val->budget);
                        $content .= "<tr>
                                                <td>$i</td>
                                                <td>$val->objective</td>
                                                <td>$budget</td>
                                            </tr>";
                        $sum += $val->budget;
                        $i++;
                    }
                    $sum = number_format($sum);
                    $content .= "<tr>
                                                <td></td>
                                                <td><b>TOTAL</b></td>
                                                <td><b>$sum<b></td>
                                            </tr>
                                        </tbody>
                                    </table>";
                }
            } else {
                $content .= 'No budget indicated';
            }
        }

        //capability
        if ($action === 'capability') {
            if ($call_app) {
                $content .= "<h3 class='font-weight-bold'>Capability</h3>";
                $content .= "<p>$call_app->capacity</p>";
            } else {
                $content .= 'No project capacity';
            }
        }

        return $content;
    }
}
