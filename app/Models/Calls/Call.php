<?php

namespace App\Models\Calls;

use Illuminate\Database\Eloquent\Model;
use App\User;

/**
 * App\Models\Calls\Call
 *
 * @property int $id
 * @property string $title
 * @property string|null $introduction
 * @property string|null $team_composition
 * @property string|null $eligibility
 * @property string|null $duration
 * @property string|null $deliverable
 * @property string|null $evaluation_criteria
 * @property string|null $application_details
 * @property string|null $deadline
 * @property string|null $status
 * @property int|null $created_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property int|null $updated_by
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call whereApplicationDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call whereDeadline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call whereDeliverable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call whereEligibility($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call whereEvaluationCriteria($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call whereIntroduction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call whereTeamComposition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\Call whereUpdatedBy($value)
 * @mixin \Eloquent
 */
class Call extends Model
{
    protected $guarded = ['id'];

    //call categories
    public function callCategories()
    {
        return $this->hasMany(CallCategory::class, 'call_id');
    }

    //call cluster
    public function callClusters()
    {
        return $this->hasMany(CallCluster::class, 'call_id');
    }

    //call invitations
    public function callInvitations()
    {
        return $this->hasMany(CallInvitation::class, 'call_id');
    }

    //program coordinator
    public function programCoordinator()
    {
        return $this->belongsTo(User::class, 'program_coordinator');
    }

    //program Accountant
    public function programAccountant()
    {
        return $this->belongsTo(User::class, 'program_accountant');
    }

    //rules
    public static function rules(): array
    {
        return [
            'name' => 'required|string',
            'introduction' => 'required|string',
            'call_category.*' => 'required',
            'budget_ceiling.*' => 'required',
            'call_category_desc.*' => 'required',
            'application_details' => 'required',
            'duration' => 'required|string',
            'team_composition' => 'required|string',
            'eligibility' => 'required|string',
            'deliverable' => 'required|string',
            'evaluation_criteria' => 'required|string',
            'cluster_ids' => 'required',
            'date' => 'required|string',
            'time' => 'required|string',
        ];
    }

    //messages
    public static function messages(): array
    {
        return [
            'name.required' => 'Title of call is required',
            'introduction.required' => 'Call introduction is required',
            'call_category.*.required' => 'Call category is required',
            'budget_ceiling.*.required' => 'Budget ceiling is required',
            'call_category_desc.*.required' => 'Call category description is required',
            'application_details.required' => 'Application details required',
            'duration.required' => 'Project duration is required',
            'team_composition.required' => 'Team composition is required',
            'eligibility.required' => 'Eligibility is required',
            'deliverable.required' => 'Deliverable is required',
            'evaluation_criteria.required' => 'Evaluation criteria is required',
            'cluster_ids.required' => 'Research area is required',
            'date.required' => 'Deadline date is required',
            'time.required' => 'Deadline time is required',
        ];
    }
}
