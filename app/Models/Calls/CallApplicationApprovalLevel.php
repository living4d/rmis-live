<?php

namespace App\Models\Calls;

use Auth;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Calls\CallApplicationApprovalLevel
 *
 * @property int $id
 * @property string|null $level
 * @property string|null $ordering
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApprovalLevel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApprovalLevel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApprovalLevel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApprovalLevel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApprovalLevel whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationApprovalLevel whereOrdering($value)
 * @mixin \Eloquent
 */
class CallApplicationApprovalLevel extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    //show operation
    public static function show_operation($call_id, $app_id)
    {
        //call approval levels
        $levels = [];

        $call_approval_levels = CallApplicationApprovalLevel::orderBy('ordering', 'ASC')->get();
        foreach ($call_approval_levels as $value) {
            array_push($levels, strtolower($value->level));
        }

        //logged user roles
        $roles = Auth::user()->roles;

        $serial = 1;
        foreach ($roles as $role) {
            $key = array_search($role->name, $levels);

            if ($key !== false) {
                //1. get level_id from the approval_levels
                $call_approval_level = CallApplicationApprovalLevel::where(['level' => $role->name])->first();

                //2. if exists in role check key of actor from student actors => done
                //3. then check if actor is already approve student request from progress table
                $call_approval = CallApplicationApproval::where(['application_id' => $app_id, 'approval_level_id' => $call_approval_level->id, 'status' => 1])->first();

                if ($call_approval) {
                    //check if there is more than role
                    if (count($roles) > 1)
                        $serial++; //increment to another role
                } else {
                    //if key == 0 means first to approve
                    if ($key == 0) {
                        echo '<a href="' . route('applications.approve', [$call_id, $app_id, $call_approval_level->id]).'" class="approve btn btn-outline-success btn-xs"><i class="fa fa-check-square fa-2x text-success"></i></a>';
                    } else {
                        $prev_key = $key - 1; //previous key
                        $prev_role = $levels[$prev_key]; //previous role

                        //4.find prev approval level id
                        $prev_call_approval_level = CallApplicationApprovalLevel::where(['level' => $prev_role])->first();

                        //4. check the previous actor to see the current role should follow for approval
                        $prev_call_approval = CallApplicationApproval::where(['application_id' => $app_id, 'approval_level_id' => $prev_call_approval_level->id, 'status' => 1])->first();

                        if ($prev_call_approval)
                            echo '<a href="' . route('applications.approve', [$call_id, $app_id, $call_approval_level->id]) . '" class="approve btn btn-outline-success btn-xs"><i class="fa fa-check-square fa-2x text-success"></i></a>';
                    }
                }
            }
        }
    }


}
