<?php

namespace App\Models\Calls;

use Illuminate\Database\Eloquent\Model;

use Auth;
use App\User;
use DB;


/**
 * App\Models\Calls\CallApplicationEvaluator
 *
 * @property int $id
 * @property int $call_id
 * @property int $application_id
 * @property int $evaluator_id
 * @property int $created_by
 * @property string|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationEvaluator newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationEvaluator newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationEvaluator query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationEvaluator whereApplicationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationEvaluator whereCallId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationEvaluator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationEvaluator whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationEvaluator whereEvaluatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationEvaluator whereId($value)
 * @mixin \Eloquent
 */
class CallApplicationEvaluator extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    //where condition
    public static function where_cond()
    {
        $app_evaluators = static::select('call_application_evaluators.call_id');

        if (Auth::user()->hasRole('evaluators')) {
            $user_id = Auth::user()->id;

            $user = User::find($user_id);

            $app_evaluators = $app_evaluators->where('call_application_evaluators.evaluator_id', $user->id);
        }

        return $app_evaluators;
    }

    //get all 
    public static function getLists()
    {
        return DB::table('call_application_evaluators')->get();
    }

    //belong relationship
    public function user()
    {
        return $this->belongsTo(User::class, 'evaluator_id');
    }

    //is assigned a call??
    public static function is_assigned_app($app_id)
    {
        //call application evaluators
        $app_evaluator = static::where(['application_id' => $app_id, 'evaluator_id' => Auth::user()->id])->count();

        if ($app_evaluator > 0)
            return true;
        else
            return false;
    }
}
