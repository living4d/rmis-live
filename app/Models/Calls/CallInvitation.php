<?php

namespace App\Models\Calls;

use Illuminate\Database\Eloquent\Model;
use App\User;


/**
 * App\Models\Calls\CallInvite
 *
 * @property int $id
 * @property int $call_id
 * @property int $college_id
 * @property string|null $department_ids
 * @property int|null $status
 * @property int|null $created_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallInvite newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallInvite newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallInvite query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallInvite whereCallId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallInvite whereCollegeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallInvite whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallInvite whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallInvite whereDepartmentIds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallInvite whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallInvite whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallInvite whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CallInvitation extends Model
{
    public $table = 'call_invitations';
    protected $guarded = ['id'];
    public $timestamps = false;

    //belong to
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
