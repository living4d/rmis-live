<?php

namespace App\Models\Calls;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Calls\CallApplicationResearcher
 *
 * @property int $id
 * @property int $call_id
 * @property int|null $application_id
 * @property int $researcher_id
 * @property string|null $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationResearcher newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationResearcher newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationResearcher query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationResearcher whereApplicationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationResearcher whereCallId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationResearcher whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationResearcher whereResearcherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationResearcher whereStatus($value)
 * @mixin \Eloquent
 */
class CallApplicationResearcher extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
}
