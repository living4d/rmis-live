<?php

namespace App\Models\Calls;

use Illuminate\Database\Eloquent\Model;

/**
 * App\BudgetLine
 *
 * @property int $id
 * @property string $name
 * @property int|null $active
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\BudgetLine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\BudgetLine newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\BudgetLine query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\BudgetLine whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\BudgetLine whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\BudgetLine whereName($value)
 * @mixin \Eloquent
 */
class BudgetLine extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
}
