<?php

namespace App\Models\Calls;

use App\Models\Projects\ProjectActivity;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Calls\CallApplicationObjective
 *
 * @property int $id
 * @property int $call_id
 * @property int $application_id
 * @property string|null $objective
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationObjective newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationObjective newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationObjective query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationObjective whereApplicationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationObjective whereCallId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationObjective whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationObjective whereObjective($value)
 * @mixin \Eloquent
 */
class CallApplicationObjective extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    //project activities
    public function activities()
    {
        return $this->hasMany(ProjectActivity::class, 'objective_id');
    }
}
