<?php

namespace App\Models\Calls;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Calls\CallApplicationBudget
 *
 * @property int $id
 * @property int $call_id
 * @property int $application_id
 * @property int $budget_line_id
 * @property string|null $amount
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationBudget newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationBudget newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationBudget query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationBudget whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationBudget whereApplicationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationBudget whereBudgetLineId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationBudget whereCallId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calls\CallApplicationBudget whereId($value)
 * @mixin \Eloquent
 */
class CallApplicationBudget extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;


    //budget line
    public function budgetLine()
    {
        return $this->belongsTo(BudgetLine::class, 'budget_line_id');
    }
}
