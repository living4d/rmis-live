<?php

namespace App\Models;

use App\Models\Calls\CallApplicationPartner;
use Illuminate\Database\Eloquent\Model;
use App\Perms;
use App\Role;
use App\User;
use DB;
use Mail;
use Auth;

/**
 * App\Notification
 *
 * @property int $id
 * @property string $message
 * @property string $message_id
 * @property string $user
 * @property int $user_id
 * @property string|null $email
 * @property string|null $app_status
 * @property string|null $sms_status
 * @property string|null $email_status
 * @property string|null $created_at
 * @property int|null $created_by
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereAppStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereEmailStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereMessageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereSmsStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $subject
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Notification whereSubject($value)
 */
class Notification extends Model
{
    public $table = 'notification';
    protected $guarded = ['id'];
    public $timestamps = false;

    //get recipients by actions
    public static function get_recipients($action, $array)
    {
        $recipients = [];

        //call inivitation
        if ($action == 'invitation' || $action == 'notify') {
            $college_ids = $array['college_ids'];
            $department_ids = $array['department_ids'];

            if ($college_ids != null) {
                foreach ($college_ids as $college_id) {
                    if ($department_ids != null) {
                        foreach ($department_ids as $department_id) {
                            $users = User::where(['college_id' => $college_id, 'department_id' => $department_id])->get();
                            foreach ($users as $user) {
                                array_push($recipients, $user->id);
                            }
                        }
                    } else {
                        $users = User::where('college_id', $college_id)->get();
                        foreach ($users as $user) {
                            array_push($recipients, $user->id);
                        }
                    }
                }
            }
        }

        //call application consent
        if ($action == 'call_consent') {
            $partner_type = $array['partner_type'];
            $call_id = $array['call_id'];
            $app_id = $array['app_id'];

            //internal
            if($partner_type == 'INTERNAL'){
                $call_partners = CallApplicationPartner::where(['call_id' => $call_id, 'application_id' => $app_id, 'partner_type' => $partner_type])->get();

                if($call_partners){
                    foreach($call_partners as $val){
                        $user = User::find($val->partner_id);
                        array_push($recipients, $user->id);
                    }
                }
            }
        }
        return $recipients;
    }

    /**
     * @param $recipients
     * @param $message
     */
    public static function insert_notifications($recipients, $message)
    {
        $message_id = self::generate_message_id();

        if ($recipients) {
            foreach ($recipients as $k => $v) {
                $user = User::find($v);

                //insert notification
                DB::table('notification')->insert(
                    [
                        'message' => $message,
                        'message_id' => $message_id,
                        'user_id' => $user->id,
                        'email' => $user->email,
                        'phone' => self::cast_mobile($user->phone),
                        'app_status' => 'PENDING',
                        'sms_status' => 'PENDING',
                        'email_status' => 'PENDING',
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => Perms::get_current_user_id()
                    ]
                );
            }
        }

        return $message_id;
    }

    //send email
    public static function send_email($array)
    {
        $to_name = $array['name'];
        $to_email = $array['email'];
        $subject = 'Research Information Management  System (RIMS)';

        //post data
        $data = array("name" => $to_name, "body" => $array['message']);

        //send email
        Mail::send(
            ['html' => 'emails.mail'],
            $data,
            function ($message) use ($to_name, $to_email, $subject) {
                $message->to($to_email, $to_name)
                    ->subject($subject);
                $message->from('multics.tz@gmail.com', 'Research Information Management System (RIMS)');
            }
        );

        //check if mail failure
        if (count(Mail::failures()) > 0)
            return false;
        else
            return true;
    }

    //show notification
    public static function show_notification()
    {
        $user_id = Perms::get_current_user_id();

        $notification = DB::table('notification')->where(['user_id' => $user_id, 'app_status' => 'PENDING'])->count();

        if ($notification > 0) {
            return $notification;
        } else {
            return '';
        }
    }

    //generate message_id
    public static function generate_message_id()
    {
        //the characters you want in your id
        $characters = '123456789ABCDEFGHJKLMNPQRSTUVWXYZ';
        $max = strlen($characters) - 1;
        $string = '';

        for ($i = 0; $i <= 10; $i++) {
            $string .= $characters[mt_rand(0, $max)];
        }

        return $string;
    }

    //remove 0 and + on start of mobile
    public static function cast_mobile($mobile)
    {
        $code = '255';
        if (preg_match("~^0\d+$~", $mobile)) {
            return  $code . substr($mobile, 1);
        } else {
            return $mobile;
        }
    }
}
