<?php

namespace App\Models\Publications;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PublicationLevel
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $step
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationLevel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationLevel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationLevel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationLevel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationLevel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationLevel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationLevel whereStep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationLevel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PublicationLevel extends Model
{
    public $table = 'publication_levels';
}
