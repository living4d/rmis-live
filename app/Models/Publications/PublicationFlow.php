<?php

namespace App\Models\Publications;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PublicationFlow
 *
 * @property int $id
 * @property int|null $publication_id
 * @property int|null $publication_level_id
 * @property string|null $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationFlow newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationFlow newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationFlow query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationFlow whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationFlow whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationFlow wherePublicationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationFlow wherePublicationLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationFlow whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationFlow whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PublicationFlow extends Model
{
    public $table = 'publication_flows';
}
