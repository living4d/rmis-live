<?php

namespace App\Models\Publications;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PublicationReviewer
 *
 * @property int $id
 * @property int|null $publication_id
 * @property int|null $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationReviewer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationReviewer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationReviewer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationReviewer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationReviewer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationReviewer wherePublicationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationReviewer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationReviewer whereUserId($value)
 * @mixin \Eloquent
 */
class PublicationReviewer extends Model
{
    public $table = 'publication_reviewers';
}
