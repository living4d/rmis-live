<?php

namespace App\Models\Publications;

use Illuminate\Database\Eloquent\Model;

class PublicationCommittee extends Model
{
    public $table = 'publications_committee';
}
