<?php

namespace App\Models\Publications;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Publication
 *
 * @property int $id
 * @property string|null $names
 * @property string|null $department
 * @property string|null $rank
 * @property string|null $year
 * @property int|null $publication_type
 * @property string|null $media_name
 * @property string|null $publication_title
 * @property string|null $publisher
 * @property string|null $volume
 * @property string|null $issue
 * @property string|null $pages
 * @property string|null $city
 * @property string|null $book_author
 * @property string|null $book_title
 * @property string|null $journal_name
 * @property string|null $conference_publication_name
 * @property string|null $accessibility
 * @property string|null $index
 * @property string|null $link
 * @property string|null $processing_charge
 * @property string|null $currency
 * @property float|null $amount
 * @property string|null $peer_review
 * @property string|null $points
 * @property string|null $vetting_status
 * @property string|null $remark
 * @property string|null $review_status
 * @property string|null $level
 * @property string|null $attach_doc
 * @property string|null $international_editor_board
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereAccessibility($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereAttachDoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereBookAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereBookTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereConferencePublicationName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereDepartment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereIndex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereInternationalEditorBoard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereIssue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereJournalName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereMediaName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereNames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication wherePages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication wherePeerReview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereProcessingCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication wherePublicationTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication wherePublicationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication wherePublisher($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereRank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereReviewStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereVettingStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereVolume($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Publication whereYear($value)
 * @mixin \Eloquent
 */
class Publication extends Model
{
    //
}
