<?php

namespace App\Models\Publications;

use Illuminate\Database\Eloquent\Model;

class PublicationReview extends Model
{
    public $table = 'publication_reviews';
}
