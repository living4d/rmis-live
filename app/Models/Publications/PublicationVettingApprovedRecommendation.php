<?php

namespace App\Models\Publications;

use Illuminate\Database\Eloquent\Model;

class PublicationVettingApprovedRecommendation extends Model
{
    protected $guarded = [];
}
