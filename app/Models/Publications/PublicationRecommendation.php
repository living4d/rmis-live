<?php

namespace App\Models\Publications;

use Illuminate\Database\Eloquent\Model;

class PublicationRecommendation extends Model
{
    public $table = 'publication_recommendations';
}
