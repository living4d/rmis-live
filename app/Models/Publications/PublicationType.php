<?php

namespace App\Models\Publications;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PublicationType
 *
 * @property int $id
 * @property string|null $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PublicationType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PublicationType extends Model
{
    public $table = 'publication_types';
}
