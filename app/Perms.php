<?php

/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 17/04/2020
 * Time: 05:11
 */

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Perms
{
    //get current logged user
    public static function get_current_user_id()
    {
        return Auth::user()->id;
    }

    //get current user level_id
    public static function get_current_user_level_id()
    {
        $user_id = Auth::user()->id;
        $user = User::find($user_id);

        return $user->level_id;
    }

    //perm class
    public static function perm_class($controller)
    {
        //get perm class
        $class = DB::table("perm_classes")->where("class", $controller)->first();

        if ($class) {
            $roles = Auth::user()->roles;

            $class_array = [];
            foreach ($roles as $role) {
                //perm_role
                $perm_role = DB::table("perm_roles")->where("role_id", $role->id)->first();

                if ($perm_role) {
                    $classes = explode(',', $perm_role->classes);

                    if (in_array($class->id, $classes))
                        array_push($class_array, $class->id);
                } else {
                    return false;
                }
            }
            //check for class array
            if ($class_array)
                return true;
            else
                return false;
        } else {
            return false;
        }
    }

    //perm methods
    public static function perm_methods($controller, $perm_slug)
    {
        //get perm class
        $class = DB::table("perm_classes")->where("class", $controller)->first();

        if ($class) {
            //method
            $method = DB::table("perm_methods")->where(['class_id' => $class->id, 'method' => $perm_slug])->first();

            if ($method) {
                $roles = Auth::user()->roles;

                $method_array = [];
                foreach ($roles as $role) {
                    //perm_role
                    $perm_role = DB::table("perm_roles")->where("role_id", $role->id)->first();

                    if ($perm_role) {
                        $perms = explode(',', $perm_role->methods);

                        if (in_array($method->id, $perms))
                            array_push($method_array, $method->id);
                    } else {
                        return false;
                    }
                }
                //check for method array
                if ($method_array)
                    return true;
                else
                    return false;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //check if user allowed
    public static function has_allowed($controller, $method)
    {
        if (!Perms::perm_methods($controller, $method)) {
            abort(403, 'Unauthorized action.');
        }
    }

    //function
    public static function manage_view()
    {
        if (Auth::user()->hasRole('admin') ||
            Auth::user()->hasRole('hod') || Auth::user()->hasRole('principal') || Auth::user()->hasRole('DHRA')
            || Auth::user()->hasRole('drp') || Auth::user()->hasRole('DVC') || Auth::user()->hasRole('VC')
        ) {
            return true;
        } elseif (Auth::user()->hasRole('staff')) {
            return false;
        }
    }
}
