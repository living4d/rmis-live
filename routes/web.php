<?php

use App\Models\Calls\Call;
use App\Models\Settings\Content;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', function () {
    //content
    $content = Content::find(1);
    $this->data['content'] = $content;

    //calls
    $calls = Call::where(['status' => 'active'])->orderBy('created_at', 'DESC')->get();
    $this->data['calls'] = $calls;

    //render view
    return view('auth.login', $this->data);
});

//login
Route::get('login', 'Auth\LoginController@show_login_form')->name('login');
Route::post('login', 'Auth\LoginController@authenticate')->name('login');
Route::get('logout', 'Auth\LoginController@logout');

//dashboard
Route::get('dashboard', 'DashboardController@index')->middleware('auth');
Route::get('home', 'DashboardController@index')->middleware('auth');


//change password on first login
Route::get('change-password', 'DashboardController@change_password')->middleware('auth');
Route::post('change-password', 'DashboardController@store_password')->middleware('auth');

//proposals
Route::get('proposals', 'ProposalsController@index');
Route::get('proposals/details/{id}', 'ProposalsController@details');
Route::get('proposals/{app_id}/confirm/', 'ProposalsController@confirm')->name('proposals.confirm');
Route::post('proposals/{app_id}/save-confirmation', 'ProposalsController@save_confirmation')->name('proposals.save-confirmation');

//calls
Route::get('calls/stats', 'Calls\CallsController@stats')->name('calls.stats');
Route::get('calls/close', 'Calls\CallsController@close')->name('calls.close');

//projects
Route::get('projects/stats', 'Projects\ProjectsController@stats')->name('projects.stats');
Route::get('projects/lists', 'Projects\ProjectsController@lists')->name('projects.lists');

//fund requests
Route::get('fund-requests/lists', 'Projects\FundRequestController@lists')->name('fund-requests.lists');
Route::get('fund-requests/approved', 'Projects\FundRequestController@approved')->name('fund-requests.approved');
Route::get('fund-requests/{id}/change-status', 'Projects\FundRequestController@change_status')->name('fund-requests.change-status');
Route::post('fund-requests/{id}/store-status', 'Projects\FundRequestController@store_status')->name('fund-requests.store-status');

Route::get('fund-requests/create-batch', 'Projects\FundRequestController@create_batch')->name('fund-requests.create-batch');
Route::post('fund-requests/store-batch', 'Projects\FundRequestController@store_batch')->name('fund-requests.store-batch');


//user
Route::get('users/lists', 'Users\UsersController@lists')->name('users.lists');
Route::post('users/lists', 'Users\UsersController@lists')->name('users.lists');

//routes
Route::resources(
    [
        'roles' => 'Users\RolesController',
        'users' => 'Users\UsersController',
        'calls' => 'Calls\CallsController',
        // 'call_categories' => 'Calls\CallCategoriesController',
        'projects' => 'Projects\ProjectsController',
        'colleges' => 'Settings\CollegesController',
        'departments' => 'Settings\DepartmentsController',
        'clusters' => 'Settings\ClustersController',
        'research_areas' => 'Settings\ResearchAreasController',
    ]
);

//call information
Route::get('calls/{id}/invitations', ['as' => 'calls.invitations', 'uses' => 'Calls\CallsController@invitations']);
Route::get('calls/{id}/invite-staff', 'Calls\CallsController@invite')->name('calls.invite');
Route::post('calls/{id}/store-invite', 'Calls\CallsController@store_invite')->name('calls.store-invite');

Route::get('calls/{id}/evaluators', ['as' => 'calls.evaluators', 'uses' => 'Calls\CallsController@evaluators']);
Route::get('calls/{id}/{cluster_id}/assigned_evaluators', 'Calls\CallsController@assigned_evaluators');
Route::post('calls/{id}/assign_evaluators', 'Calls\CallsController@assign_evaluators');
Route::post('calls/{id}/drop_evaluators', 'Calls\CallsController@drop_evaluators');
Route::get('calls/{id}/applications', ['as' => 'calls.applications', 'uses' => 'Calls\CallsController@applications']);
Route::post('calls/{id}/applications', ['as' => 'calls.applications', 'uses' => 'Calls\CallsController@applications']);
Route::get('calls/{id}/winners', ['as' => 'calls.winners', 'uses' => 'Calls\CallsController@winners']);

//calls
Route::get('calls/{id}/delete', ['as' => 'calls.delete', 'uses' => 'Calls\CallsController@destroy']);
Route::get('calls/close/{id}/{action}', ['as' => 'calls.close', 'uses' => 'Calls\CallsController@close']);
Route::get('calls/get_clusters_from_call/{id}', 'Calls\CallsController@get_clusters_from_call');

//apply
Route::get('applications/{id}/apply', 'Calls\CallApplicationsController@apply')->name('applications.apply');

//step 1
Route::get('applications/step-1/{id}/{app_id?}', 'Calls\CallApplicationsController@apply_step1')->name('applications.step-1');
Route::post('applications/save-step-1/{id}/{app_id?}', 'Calls\CallApplicationsController@save_step1')->name('applications.save-step-1');

//step 2
Route::get('applications/step-2/{id}/{app_id}', 'Calls\CallApplicationsController@apply_step2')->name('applications.step-2');
Route::put('applications/save-step-2/{id}/{app_id}', 'Calls\CallApplicationsController@save_step2')->name('applications.save-step-2');

//step 2 innovation app
Route::get('applications/innovStep-2/{id}/{app_id}', 'Calls\CallApplicationsController@apply_innov_step2')->name('applications.innov_step-2');
Route::put('applications/save-innovStep-2/{id}/{app_id}', 'Calls\CallApplicationsController@save_innov_step2')->name('applications.save-innov-step-2');

//step 3
Route::get('applications/step-3/{id}/{app_id}', 'Calls\CallApplicationsController@apply_step3')->name('applications.step-3');
Route::put('applications/save-step-3/{id}/{app_id}', 'Calls\CallApplicationsController@save_step3')->name('applications.save-step-3');

//preview, cancel
Route::get('applications/{app_id}/preview', 'Calls\CallApplicationsController@preview')->name('applications.preview');
Route::get('applications/{app_id}/cancel', 'Calls\CallApplicationsController@cancel')->name('applications.cancel');
Route::get('applications/{app_id}/confirm', 'Calls\CallApplicationsController@confirm')->name('applications.confirm');
Route::get('applications/{app_id}/success', 'Calls\CallApplicationsController@success')->name('applications.success');

//call application
Route::get('applications/lists', 'Calls\CallApplicationsController@lists')->name('applications.lists');
Route::post('applications/lists', 'Calls\CallApplicationsController@lists')->name('applications.lists');

Route::get('applications/{app_id}/details', 'Calls\CallApplicationsController@details')->name('applications.details');
Route::get('applications/{app_id}/edit', 'Calls\CallApplicationsController@edit')->name('applications.edit');
Route::put('applications/update', 'Calls\CallApplicationsController@update')->name('applications.update');
Route::get('applications/{app_id}/delete', 'Calls\CallApplicationsController@delete')->name('applications.delete');
Route::get('applications/{app_id}/assign-evaluators', 'Calls\CallApplicationsController@assign_evaluators')->name('applications.assign-evaluators');
Route::post('applications/{app_id}/store-evaluators', 'Calls\CallApplicationsController@store_evaluators')->name('applications.store-evaluators');
Route::get('applications/{app_id}/{ev_id}/drop-evaluator', 'Calls\CallApplicationsController@drop_evaluator')->name('applications.drop-evaluator');

//award and decline
Route::get('applications/{id}/api_details', 'Calls\CallApplicationsController@api_details');
Route::get('applications/{app_id}/{role_name}/api_approve', 'Calls\CallApplicationsController@api_approve');
Route::get('applications/{app_id}/api_award', 'Calls\CallApplicationsController@api_award');
Route::get('applications/{app_id}/api_decline', 'Calls\CallApplicationsController@api_decline');
Route::get('applications/{app_id}/api_register_project', 'Calls\CallApplicationsController@api_register_project');

//batch actions
Route::post('call_applications/selected', 'Calls\CallApplicationsController@selected');
Route::get('call_applications/batch_assign_evaluators', 'Calls\CallApplicationsController@batch_assign_evaluators');
Route::post('call_applications/batch-store-evaluators', 'Calls\CallApplicationsController@batch_store_evaluators');
Route::get('call_applications/batch_delete', 'Calls\CallApplicationsController@batch_delete');

//call application evaluations
// Route::get('call_evaluations/lists', 'Calls\CallEvaluationsController@lists');
// Route::post('call_evaluations/lists', 'Calls\CallEvaluationsController@lists');

Route::get('evaluations/{call_id}/{app_id}/records', 'Calls\CallEvaluationsController@records')->name('evaluations.records');
Route::get('evaluations/{call_id}/{app_id}/winner', 'Calls\CallEvaluationsController@set_winner')->name('evaluations.manual_winner');//to set winners

Route::get('evaluations/{call_id}/{app_id}/{criteria_id}/create', 'Calls\CallEvaluationsController@create')->name('evaluations.create');
Route::post('evaluations/store', 'Calls\CallEvaluationsController@store')->name('evaluations.store');
Route::get('evaluations/{call_id}/{app_id}/confirm-results', 'Calls\CallEvaluationsController@confirm_results')->name('evaluations.confirm-results');

Route::get('call_evaluations/clusters/{call_id}', 'Calls\CallEvaluationsController@clusters');
Route::get('call_evaluations/cluster_results/{call_id}/{cluster_id}', 'Calls\CallEvaluationsController@cluster_results');
Route::post('call_evaluations/selected', 'Calls\CallEvaluationsController@selected');
Route::get('call_evaluations/batch_winners', 'Calls\CallEvaluationsController@batch_winners');
Route::get('call_evaluations/export-pdf/{call_id}/{cluster_id}', 'Calls\CallEvaluationsController@export_pdf');

//projects
Route::get('projects/{id}/activities', 'Projects\ProjectsController@activities')->name('projects.activities');
Route::get('projects/{id}/fund-requests', 'Projects\ProjectsController@fund_requests')->name('projects.fund-requests');
Route::get('projects/{id}/progress-reports', 'Projects\ProjectsController@progress_reports')->name('projects.progress-reports');
Route::any('projects/{id}/generate-report', 'Projects\ProjectsController@generate_report')->name('projects.generate-report');
Route::any('projects/{id}/change-status', 'Projects\ProjectsController@change_status')->name('projects.change-status');
Route::any('projects/{id}/store-status', 'Projects\ProjectsController@store_status')->name('projects.store-status');

//activities
Route::post('activities/selected', 'Projects\ActivitiesController@selected')->name('activities.selected');
Route::get('activities/{id}/api_show', 'Projects\ActivitiesController@api_show');
Route::get('activities/{project_id}/{objective_id}/lists', 'Projects\ActivitiesController@lists')->name('activities.lists');
Route::get('activities/{project_id}/create', 'Projects\ActivitiesController@create')->name('activities.create');
Route::post('activities/{project_id}/store', 'Projects\ActivitiesController@store')->name('activities.store');
Route::get('activities/{project_id}/{id}/edit', 'Projects\ActivitiesController@edit')->name('activities.edit');
Route::put('activities/{project_id}/{id}/update', 'Projects\ActivitiesController@update')->name('activities.update');

Route::get('activities/{activity_id}/fund-requests', 'Projects\ActivitiesController@fund_requests')->name('activities.fund-requests');
Route::get('activities/{project_id}/{activity_id}/delete', 'Projects\ActivitiesController@delete')->name('activities.delete');
Route::post('activities/{project_id}/{activity_id}/request-fund', 'Projects\ActivitiesController@store_request_fund')->name('activities.request-fund');

//activity reports
Route::get('activity-reports/{id}/api_show', 'Projects\ActivityReportsController@api_show');
Route::get('activity-reports/{activity_id}/change-status', 'Projects\ActivityReportsController@change_status')->name('activity-reports.change-status');
Route::post('activity-reports/{activity_id}/store-status', 'Projects\ActivityReportsController@store_status')->name('activity-reports.store-status');

//fund requests
Route::get('fund-requests/{activity_id}/create', 'Projects\FundRequestController@create')->name('fund-requests.create');
Route::post('fund-requests/{activity_id}/store', 'Projects\FundRequestController@store')->name('fund-requests.store');

Route::get('fund-requests/{id}/edit', 'Projects\FundRequestController@edit')->name('fund-requests.edit');
Route::put('fund-requests/{id}/update', 'Projects\FundRequestController@update')->name('fund-requests.update');

Route::get('fund-requests/{id}/api_details', 'Projects\FundRequestController@api_details');
Route::get('fund-requests/{id}/{role_name}/api_approve', 'Projects\FundRequestController@api_approve');
Route::get('fund-requests/{id}/{role_name}/{remarks}/api_reject', 'Projects\FundRequestController@api_reject');
Route::get('fund-requests/{id}/approval-letter', 'Projects\FundRequestController@approval_letter')->name('fund-requests.approval-letter');

//my inclusion in fund request
Route::get('fund-requests/{id}/show', 'Projects\FundRequestController@show_fund_request')->name('show_fund_request')->middleware('auth');
Route::post('fund-requests/{id}/appr_printed', 'Projects\FundRequestController@store_appr_print_status')->name('store_approval_printed_status')->middleware('auth');

//notification
Route::get('notification', 'NotificationController@index');
Route::get('notification/outbox', 'NotificationController@outbox');
Route::any('notification/send', 'NotificationController@send');
Route::any('notification/store-message', 'NotificationController@store_message');
Route::get('notification/send-email', 'NotificationController@send_email');
Route::get('notification/change-status/{id}', 'NotificationController@change_status');

//front contents
Route::get('contents', 'Settings\ContentsController@index');
Route::put('contents/update/{id}', 'Settings\ContentsController@update');

//roles
Route::get('/roles/assign_perms/{id}', 'Users\RolesController@assign_perms');
Route::post('/roles/store_perms/{id}', 'Users\RolesController@store_perms');

//users
Route::get('users/{id}/delete', ['as' => 'users.delete', 'uses' => 'Users\UsersController@destroy']);

//users my inclusion
Route::get('users/{id}/manage', ['as' => 'users.manage', 'uses' => 'Users\UsersController@manage']);
Route::put('users/{id}/update_manage', ['as' => 'users.update_manage', 'uses' => 'Users\UsersController@update_manage']);

//profile
Route::get('profile', 'Users\ProfileController@index');
Route::get('profile/edit', 'Users\ProfileController@edit')->name('profile.edit');
Route::put('profile/{id}/update', 'Users\ProfileController@update')->name('profile.update');
Route::get('profile/change-password', 'Users\ProfileController@change_password')->name('profile.change-password');
Route::post('profile/change-password', 'Users\ProfileController@store_password')->name('profile.change-password');

//Password Reset routes
Route::get('password/forgot', 'Auth\ResetPasswordController@showForgotForm')->name('forgot.password.form');
Route::post('password/forgot', 'Auth\ResetPasswordController@sendResetLink')->name('forgot.password.link');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('reset.password.form');
Route::post('password/reset', 'Auth\ResetPasswordController@resetPassword')->name('reset.password');

//colleges
Route::get('colleges/departments/{id}', 'Settings\CollegesController@departments');

//research area
Route::get('clusters/research_areas/{cluster_id}', 'Settings\ClustersController@research_areas');


//clearance
Route::get('clearance/stats', 'ClearanceController@stats')->name('clearance.stats');
Route::get('clearance', 'ClearanceController@index')->name('clearance')->middleware('auth');
Route::get('clearance/staff', 'ClearanceController@staff')->middleware('auth');
Route::get('clearance/ethical', 'ClearanceController@ethical')->middleware('auth');
Route::get('clearance/students', 'ClearanceController@students')->middleware('auth');
Route::get('clearance/brief', 'ClearanceController@brief')->middleware('auth');
Route::get('clearance/brief1', 'ClearanceController@brief_staff')->middleware('auth');
Route::get('clearance/store_student_clearance', 'ClearanceController@store_student_clearance')->middleware('auth');
Route::get('clearance/store_staff_clearance', 'ClearanceController@store_staff_clearance')->middleware('auth');
Route::get('clearance/store_ethical', 'ClearanceController@store_ethical')->middleware('auth');
Route::get('clearance/ethical/{id}', 'ClearanceController@show_ethical_clearance')->name('show_ethical_clearance')->middleware('auth');
Route::get('clearance/panel2/{id}', 'ClearanceController@recommendation_panel_ethical')->name('recommendation_panel_ethical')->middleware('auth');

Route::get('clearance/ethical/save_recommend/{id}', 'ClearanceController@save_recommendation_ethical')->name('save_recommendation_ethical')->middleware('auth');
Route::get('clearance/approve_ethical/{id}', 'ClearanceController@approve_ethical_clearance')->name('approve_ethical_clearance')->middleware('auth');
Route::get('ethical/list', 'ClearanceController@list_ethical_clearance')->name('list_ethical_clearance')->middleware('auth');
Route::get('clearance/process', 'ClearanceController@process_clearance')->name('process_clearance')->middleware('auth');
Route::get('clearance/processHolderSupervisions', 'ClearanceController@process_clearance_holder_supervise')->name('process_clearance_holder_supervise')->middleware('auth');

Route::get('clearance/edit/{id}', 'ClearanceController@edit_student_clearance')->name('edit_student_clearance')->middleware('auth');
Route::get('clearance/add_std_site/{id}/create', 'ClearanceController@add_student_site')->name('add_student_clearance_site_edit')->middleware('auth');
Route::post('clearance/store_added_site/{id}', 'ClearanceController@store_added_site')->name('store_added_site')->middleware('auth');
Route::get('clearance/edit_staff/{id}', 'ClearanceController@edit_staff_clearance')->name('edit_staff_clearance')->middleware('auth');
Route::get('clearance/add_staff_site/{id}/create', 'ClearanceController@add_staff_site')->name('add_staff_clearance_site')->middleware('auth');
Route::post('clearance/store_staff_added_site/{id}', 'ClearanceController@store_staff_added_site')->name('store_staff_added_site')->middleware('auth');
Route::post('clearance/update/student_clearance/{id}', 'ClearanceController@update_student_clearance')->middleware('auth');
Route::post('clearance/update/staff_clearance/{id}', 'ClearanceController@update_staff_clearance')->middleware('auth');

//edit clearance forms by admin
Route::get('clearance/edit/admin/{id}', 'ClearanceController@edit_std_clearance_admin')->name('edit_std_clearance_admin')->middleware('auth');
Route::post('clearance/update/std_clearance/admin/{id}', 'ClearanceController@update_std_clearance_admin')->middleware('auth');
Route::get('clearance/editstaff/admin/{id}', 'ClearanceController@edit_staff_clearance_admin')->name('edit_staff_clearance_admin')->middleware('auth');
Route::post('clearance/update/staff_clearance/admin/{id}', 'ClearanceController@update_staff_clearance_admin')->middleware('auth');

//clear researchers field on edit
Route::get('clearance/staff/clr_researchers/{id}', 'ClearanceController@clear_staff_clearance_researchers')->name('clr_st_researchers')->middleware('auth');

Route::get('clearance/{id}', 'ClearanceController@show_student_clearance')->name('show_student_clearance')->middleware('auth');
Route::get('clearance/panel/{id}', 'ClearanceController@recommendation_panel')->name('recommendation_panel')->middleware('auth');
Route::get('clearance/save_recommendation/{id}', 'ClearanceController@save_recommendation')->name('save_recommendation')->middleware('auth');
Route::get('clearance/approve_student/{id}', 'ClearanceController@approve_student_clearance')->name('approve_student_clearance')->middleware('auth');
Route::get('clearance/letters/{id}', 'ClearanceController@student_clearance_letter')->name('student_clearance_letter')->middleware('auth');

Route::get('clearance/ref_letters/{id}', 'ClearanceController@ref_student_clearance_letter')->name('ref_student_clearance_letter')->middleware('auth');

Route::get('clearance/staff/{id}', 'ClearanceController@show_staff_clearance')->name('show_staff_clearance')->middleware('auth');
Route::get('clearance/panel1/{id}', 'ClearanceController@recommendation_panel_staff')->name('recommendation_panel_staff')->middleware('auth');
Route::get('clearance/save_recommends/{id}', 'ClearanceController@save_recommendation_staff')->name('save_recommendation_staff')->middleware('auth');
Route::get('clearance/approve_staff/{id}', 'ClearanceController@approve_staff_clearance')->name('approve_staff_clearance')->middleware('auth');
Route::get('clearance/letter/{id}', 'ClearanceController@staff_clearance_letter')->name('staff_clearance_letter')->middleware('auth');

Route::get('clearance/return/student/{id}', 'ClearanceController@back_panel_student')->name('back_panel_student')->middleware('auth');
Route::get('clearance/store_rev/{id}', 'ClearanceController@save_reverse_recomendation')->name('save_return_recomendation_student')->middleware('auth');

Route::get('clearance/back/{id}', 'ClearanceController@back_panel')->name('return_panel')->middleware('auth');
Route::get('clearance/return_recommend/{id}', 'ClearanceController@save_return_recomendation')->name('save_return_recomendation')->middleware('auth');

Route::get('districts/{id}', 'ClearanceController@getdistricts');
Route::get('wards/{id}', 'ClearanceController@getwards');
Route::get('villages/{id}', 'ClearanceController@getvillages');

//clearance reports
Route::any('report/permitReport/', 'ClearanceReportController@clearance_report')->name('permit_report')->middleware('auth');

//letters staff
Route::get('site/ras/letter/{id}', 'ClearanceController@ras_letter')->name('ras_letter')->middleware('auth');
Route::get('site/das/letter/{id}', 'ClearanceController@das_letter')->name('das_letter')->middleware('auth');
Route::get('site/org/letter/{id}', 'ClearanceController@org_letter')->name('org_letter')->middleware('auth');
Route::get('site/twmc/letter/{id}', 'ClearanceController@twmc_letter')->name('twmc_letter')->middleware('auth');

//letters student
Route::get('site/rasst/letter/{id}', 'ClearanceController@ras_student_letter')->name('ras_student_letter')->middleware('auth');
Route::get('site/dasst/letter/{id}', 'ClearanceController@das_student_letter')->name('das_student_letter')->middleware('auth');
Route::get('site/orgst/letter/{id}', 'ClearanceController@org_student_letter')->name('org_student_letter')->middleware('auth');
Route::get('site/twmcst/letter/{id}', 'ClearanceController@twmc_student_letter')->name('twmc_student_letter')->middleware('auth');


//referred letters student
Route::get('site/rasstref/letter/{id}', 'ClearanceController@ref_ras_student_letter')->name('ref_ras_student_letter')->middleware('auth');
Route::get('site/dasstref/letter/{id}', 'ClearanceController@ref_das_student_letter')->name('ref_das_student_letter')->middleware('auth');
Route::get('site/orgstref/letter/{id}', 'ClearanceController@ref_org_student_letter')->name('ref_org_student_letter')->middleware('auth');
Route::get('site/twmcstref/letter/{id}', 'ClearanceController@ref_twmc_student_letter')->name('ref_twmc_student_letter')->middleware('auth');

//publications
Route::get('publications/stats', 'PublicationsController@stats')->name('publications.stats');
Route::get('publications', 'PublicationsController@index')->name('publications')->middleware('auth');
Route::get('publications/register', 'PublicationsController@register')->middleware('auth');
Route::get('publications/brief', 'PublicationsController@brief')->name('publication_brief')->middleware('auth');
Route::get('publications/store', 'PublicationsController@store_publication');
Route::get('publication/{id}', 'PublicationsController@show_publication')->name('show_publication')->middleware('auth');
Route::get('publication/principal/{id}', 'PublicationsController@push_principal')->name('push_principal')->middleware('auth');
Route::get('publication/drp/{id}', 'PublicationsController@push_drp')->name('push_drp')->middleware('auth');

Route::get('pub/upc', 'PublicationsController@upc')->name('upc')->middleware('auth');
Route::get('pub/commit', 'PublicationsController@assign_commit')->name('assign_commit')->middleware('auth');

Route::get('publication/board/{id}', 'PublicationsController@board_panel')->name('board_panel')->middleware('auth');
Route::get('publication/back/{id}', 'PublicationsController@back_panel')->name('back_panel')->middleware('auth');
Route::get('publication/back_recomend/{id}', 'PublicationsController@save_recomendation')->name('save_recomendation')->middleware('auth');
Route::get('publication/decision/{id}', 'PublicationsController@save_decisions')->name('board_decisions')->middleware('auth');
Route::get('publication/assign/{id}', 'PublicationsController@assign_panel')->name('assign_panel')->middleware('auth');
Route::get('publication/save_reviewer/{id}', 'PublicationsController@save_reviewer')->name('save_reviewer')->middleware('auth');
Route::get('publication/review/{id}', 'PublicationsController@review_tool')->name('review_tool')->middleware('auth');
Route::get('publication/save_review/{id}', 'PublicationsController@save_review')->name('save_review')->middleware('auth');
Route::get('publication/vet/{id}', 'PublicationsController@vet_panel')->name('vet_panel')->middleware('auth');
Route::get('publication/vetted/{id}', 'PublicationsController@save_vetted')->name('save_vetted')->middleware('auth');
Route::get('publication/forward/{id}', 'PublicationsController@forward_panel')->name('forward_panel')->middleware('auth');
Route::get('publication/save_forward/{id}', 'PublicationsController@save_recomend')->name('save_recomend')->middleware('auth');
Route::get('publication/senate/{id}', 'PublicationsController@senate_approve')->name('senate_approve')->middleware('auth');
Route::get('publication/vc/{id}', 'PublicationsController@vc_disapprove')->name('vc_disapprove')->middleware('auth');
Route::get('generate/certificate/{id}', 'PublicationsController@publication_certificate')->name('generate_certificate')->middleware('auth');

Route::get('publication/edit/{id}', 'PublicationsController@edit')->name('edit_publication')->middleware('auth');
Route::get('publication/delete/{id}', 'PublicationsController@delete')->name('delete_publication')->middleware('auth');
Route::get('publication/update/{id}', 'PublicationsController@update_publication')->name('update_publication')->middleware('auth');

Route::get('publications/process', 'PublicationsController@process_publications')->name('process_publications')->middleware('auth');

//Publication reports
Route::get('publications/pubReports', 'PublicationsController@reportPublication')->name('publications.report_publication');
Route::any('publications/pubVetList/', 'PublicationsController@vet_list_select')->name('publications.vet_list')->middleware('auth');
Route::any('publications/publVet/export/', 'PublicationsController@export')->name('publications.publ_vet_expo')->middleware('auth');

//Publication vetted import
Route::get('/publications/vets', 'PublicationsController@vetted_list_import')->name('publications.vetted_list')->middleware('auth');
Route::post('/publications/pubVetImport/', 'PublicationsController@store_vet_excel')->name('publications.vetted_import')->middleware('auth');

//Test routes
Route::get('/tests', 'TestsImportController@list'); //show the list of tests
Route::get('/tests/import', 'TestsImportController@show'); //SHOW import form and list
//Route::post('/tests/import', 'TestsImportController@store');
Route::post('/tests/importtests', 'TestsImportController@store')->name('import_test');

//publication types
Route::get('publications/type', 'PublicationsController@publication_type')->name('publications.type');
Route::get('publications/types/create', 'PublicationsController@publication_type_create')->name('publications.type.create');
Route::post('publications/types/store', 'PublicationsController@publ_type_store')->name('publications.publ_type_store');
Route::get('publications/types/edit/{id}', 'PublicationsController@publication_type_edit')->name('publications.type.edit');
Route::put('publications/types/update/{id}', 'PublicationsController@publications_type_update')->name('publications.type.update');

//Publication batches
Route::get('publications/batches', 'PublicationsController@batches')->name('publications.batches');
Route::get('publications/batches/create', 'PublicationsController@batch_create')->name('publications.batch.create');
Route::post('publications/batches/store', 'PublicationsController@batch_store')->name('batch.store');
Route::get('publications/batch/{id}/edit', 'PublicationsController@batch_edit')->name('batch.edit');
Route::put('publications/batch/{id}/update', 'PublicationsController@batch_update')->name('batch.update');
Route::get('publications/batch/{id}/status/edit', 'PublicationsController@batch_publ_status_edit')->name('batch.pupl.status.edit');
Route::put('publications/batch/{id}/status/update', 'PublicationsController@batch_publications_status_update')->name('batch.publications.status.update');
