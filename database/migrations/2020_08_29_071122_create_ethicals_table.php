<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEthicalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ethicals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('names', 50)->nullable();
            $table->string('title', 50)->nullable();
            $table->string('college', 50)->nullable();
            $table->string('department', 50)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('phone', 50)->nullable();
            $table->string('project_reg_no', 50)->nullable();
            $table->text('research_title')->nullable();
            $table->string('research_type', 50)->nullable();
            $table->text('background')->nullable();
            $table->string('design')->nullable();
            $table->text('objective')->nullable();
            $table->text('hypotheses')->nullable();
            $table->integer('region_id')->nullable();
            $table->integer('district_id')->nullable();
            $table->integer('ward_id')->nullable();
            $table->integer('village_id')->nullable();
            $table->text('collection')->nullable();
            $table->text('procedures')->nullable();
            $table->text('adverse')->nullable();
            $table->text('precaution')->nullable();
            $table->text('procedures_discomfort')->nullable();
            $table->text('degree_discomfort')->nullable();
            $table->text('recruited')->nullable();
            $table->string('payment_participant')->nullable();
            $table->string('confidentiality')->nullable();
            $table->text('investigation_qn')->nullable();
            $table->text('investigation_detail')->nullable();
            $table->string('expertise')->nullable();
            $table->string('situation_type')->nullable();
            $table->string('env')->nullable();
            $table->string('negative_impact')->nullable();

            $table->string('english_attachment',191)->nullable();
            $table->string('swahili_attachment',191)->nullable();
            $table->string('recruite_attachment',191)->nullable();
            $table->string('consent_attachment',191)->nullable();

            $table->string('level');
            $table->string('approval_status');
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ethicals');
    }
}
