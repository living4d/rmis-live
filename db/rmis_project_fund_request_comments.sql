-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: 192.168.10.10    Database: rmis
-- ------------------------------------------------------
-- Server version	8.0.26-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `project_fund_request_comments`
--

DROP TABLE IF EXISTS `project_fund_request_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_fund_request_comments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `project_fund_request_id` int NOT NULL,
  `project_id` int NOT NULL,
  `comment_from` varchar(45) NOT NULL,
  `remarks` text NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_fund_request_comments`
--

LOCK TABLES `project_fund_request_comments` WRITE;
/*!40000 ALTER TABLE `project_fund_request_comments` DISABLE KEYS */;
INSERT INTO `project_fund_request_comments` VALUES (1,400,400,'2000','teste remarkado',NULL,NULL),(2,500,500,'500','teste remarkado',NULL,NULL),(3,600,500,'500','teste remarkado',NULL,NULL),(4,700,29,'500','teste remarkado',NULL,NULL),(5,800,29,'drp','teste remarkado',NULL,NULL),(6,800,29,'program_coordinator','teste remarkado',NULL,NULL),(7,3,29,'program_coordinator','naskia kelele huko',NULL,NULL),(8,3,29,'program_coordinator','mnyama amefanya yake',NULL,NULL),(9,3,29,'program_coordinator','sasa mbele kwa mbele',2729,'2022-03-10 17:16:18');
/*!40000 ALTER TABLE `project_fund_request_comments` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-10 20:31:51
