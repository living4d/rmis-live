@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <h4>Research Grant Evaluation Results</h4>
                        <ul>
                            <li><a href="#" title="">Dashboard</a></li>
                            <li><a href="{{url('calls')}}" title=""> Calls</a></li>
                            <li><a href="{{url('calls/'.$call->id)}}"> {{$call->title}}</a></li>
                            <li><a href="{{url('call_evaluations/clusters/'.$call->id)}}"> Research Grant Clusters</a></li>
                            <li>{{$call->title}}</li>
                        </ul>
                    </div><!-- Page Title -->
                </div>
                <!--./col-md-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    @include('calls.sidebar_menu')
                </div>
                <!--col-md-2 -->
                <div class="col-lg-10 col-md-10">
                    <h4>Research Grant: {{ $call->title}}</h4>
                    <h4>Cluster: {{$cluster->name}}</h4>
                    <hr />
                    <div class="mt-3"></div>

                    @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div><br />
                    @elseif(session()->get('danger'))
                    <div class="alert alert-danger">
                        {{ session()->get('danger') }}
                    </div><br />
                    @endif

                    @if (isset($call_results) && count($call_results) > 0)
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="float-right">
                                <a target="_blank" title="Export pdf" class="btn btn-outline-primary btn-sm"
                                    href="#">
                                    <i class="fa fa-file-pdf-o"></i> Export PDF</a>
                            </div>
                        </div>
                    </div>

                    {{ Form::open(['url' => url('call_evaluations/selected'), 'method' => 'POST', 'class' => 'form-horizontal']) }}
                    {{ Form::token() }}
                    {{Form::hidden('call_id', $call->id, ['id' => 'call_id'])}}
                    {{Form::hidden('cluster_id', $cluster->id, ['id' => 'cluster_id'])}}

                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th width="5%"><input type="checkbox" id="select_all" /></th>
                                <th width="3%">#</th>
                                <th width="44%">Project Title</th>
                                <th width="16%">PI</th>
                                <th width="10%">Submitted Date</th>
                                <th width="10%">Budget</th>
                                <th width="8%">Points</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php($serial = 1)
                            @foreach($call_results as $val)
                            @php($class = "")
                            @php($checked = "")

                            @if($val->winner == 1)
                            @php($class = "success")
                            @php($checked = "checked")
                            @endif
                            <tr class="{{$class}}">
                                <td>
                                    <input type="checkbox" class="checkbox" id="id" name="id[]" value="{{ $val->id }}"
                                        {{$checked}}>
                                </td>
                                <td>{{$serial}}</td>
                                <td><span class="font-weight-600">{{$val->project_title}}</span></td>
                                <td>{{$val->pi->first_name.' '. substr($val->pi->middle_name,0,1).' '.$val->pi->surname}}</td>
                                <td>{{ date('d-m-Y H:i', strtotime($val->created_at)) }}</td>
                                <td>{{ number_format($val->budget)}}</td>
                                <td>{{ number_format($val->avg_points, 2) }}</td>
                            </tr>
                            @php ($serial++)
                            @endforeach
                        </tbody>
                    </table>
                    <table class="table table-striped table-hover">
                        <tr>
                            <td colspan="5">
                                <b style="font-size: 14px;">With selected : </b>
                                <button type="submit" name="winners" class="btn btn-primary btn-sm"> Winners</button>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5"></td>
                        </tr>
                    </table>
                    {{ Form::close() }}
                    @else
                    <div class="alert alert-danger"> No any results found.</div>
                    @endif
                </div>
                <!--./col-lg-9 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection