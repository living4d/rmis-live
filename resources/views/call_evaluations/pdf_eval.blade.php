<table>
    <tr>
        <td width="100%" align="center">
            <img src="{{asset('assets/img/logo_ud.png')}}" width="75" />
            <h3>UNIVERSITY OF DAR ES SALAAM</h3>
            <h4>RESEARCH MANAGEMENT INFORMATION SYSTEM (RMIS)</h4>
        </td>
    </tr>
</table>
<br /><br />
<table width="100%">
    <tr bgcolor="#EEEEEE">
        <th width="5%"
            style="height:20px; border: 0.5px solid #808080; text-align:left; font-size: 70%; font-weight: bold; color: #212121;">
            &nbsp; #
        </th>
        <th width="50%"
            style="height:20px; border: 0.5px solid #808080; text-align:left; font-size: 70%; font-weight: bold; color: #212121;">
            Project Title</th>
        <th width="20%"
            style="height:20px; border: 0.5px solid #808080; text-align:left; font-size: 70%; font-weight: bold; color: #212121;">
            Principal Investigator</th>
        <th width="10%"
            style="height:20px; border: 0.5px solid #808080; text-align:left; font-size: 70%; font-weight: bold; color: #212121;">
            Budget</th>
        <th width="10%"
            style="height:20px; border: 0.5px solid #808080; text-align:left; font-size: 70%; font-weight: bold; color: #212121;">
            Points</th>
    </tr>

    @php($serial = 1)
    @foreach($call_results as $val)
    <tr>
        <td width="5%" valign="top"
            style="text-align:left; height:20px; border: 0.5px solid #808080; font-size: 75%; color: #212121;">
            &nbsp;{{$serial}}</td>
        <td width="50%" valign="top"
            style="text-align:left; height:20px; border: 0.5px solid #808080; font-size: 75%; color: #212121;">
            <span class="font-weight-600">&nbsp;{{$val->project_title}}</span></td>
        <td width="20%" valign="top"
            style="text-align:left; height:20px; border: 0.5px solid #808080; font-size: 75%; color:#212121;">
            &nbsp;{{$val->user->first_name.' '.$val->user->middle_name.' '.$val->user->surname}}</td>
        <td width="10%" valign="top"
            style="text-align:left; height:20px; border: 0.5px solid #808080; font-size: 75%; color: #212121;">
            &nbsp;{{ number_format($val->budget)}}</td>
        <td width="10%" valign="top"
            style="text-align:left; height:20px; border: 0.5px solid #808080; font-size: 75%; color: #212121;">
            &nbsp;{{$val->results}}</td>
    </tr>
    @php ($serial++)
    @endforeach

</table>