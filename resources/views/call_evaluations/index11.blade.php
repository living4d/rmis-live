@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <h4>Evaluations</h4>
                        <ul>
                            <li><a href="#" title="">Dashboard</a></li>
                            <li>Evaluations</li>
                        </ul>
                    </div><!-- Page Title -->
                </div>
                <!--./col-md-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    @include('calls.sidebar_menu')
                </div>
                <!--col-md-2 -->
                <div class="col-lg-10 col-md-10">
                    @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div><br />
                    @elseif(session()->get('danger'))
                    <div class="alert alert-danger">
                        {{ session()->get('danger') }}
                    </div><br />
                    @endif

                    @if (isset($app_evaluators) && $app_evaluators)
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th width="3%">#</th>
                                <th width="30%">Call Title</th>
                                <th width="50%">Project Title</th>
                                <th width="10%">Status</th>
                                <th width="5%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php($serial = 1)
                            @foreach($app_evaluators as $val)
                            <tr>
                                <td rowspan="{{count($val->call_apps)}}">{{$serial}}</td>
                                <td rowspan="{{count($val->call_apps)}}"><span
                                        class="font-weight-600">{{$val->call->title}}</span></td>
                                <td><span class="font-weight-600">{{$val->call_apps[0]->project_title}}</span></td>
                                <td>
                                    @if($val->call_apps[0]->eval_status == 0)
                                    <label class="badge badge-danger">NOT STARTED</label>
                                    @elseif($val->call_apps[0]->eval_status == 1)
                                    <label class="badge badge-warning">INCOMPLETE</label>
                                    @elseif($val->call_apps[0]->eval_status == 2)
                                    <label class="badge badge-success">COMPLETE</label>
                                    @endif
                                </td>
                                <td>
                                    <div class="text-center">
                                        <a
                                            href="{{ url('call_evaluations/records/'.$val->call->id.'/'.$val->call_apps[0]->id)}}">
                                            <i class="fa fa-folder-open text-primary"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @for($i = 1; $i < sizeof($val->call_apps); $i++)
                                <tr>
                                    <td><span class="font-weight-600">{{$val->call_apps[$i]->project_title}}</span></td>
                                    <td>
                                        @if($val->call_apps[$i]->eval_status == 0)
                                        <label class="badge badge-danger">NOT STARTED</label>
                                        @elseif($val->call_apps[$i]->eval_status == 1)
                                        <label class="badge badge-warning">INCOMPLETE</label>
                                        @elseif($val->call_apps[$i]->eval_status == 2)
                                        <label class="badge badge-success">COMPLETE</label>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="text-center">
                                            <a
                                                href="{{ url('call_evaluations/records/'.$val->call->id.'/'.$val->call_apps[$i]->id)}}">
                                                <i class="fa fa-folder-open text-primary"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endfor
                                <!--./endfor -->

                                @php ($serial++)
                                @endforeach
                        </tbody>
                    </table>
                    @else
                    <div class="alert alert-danger"> No any call application found.</div>
                    @endif
                </div>
                <!--./col-lg-9 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection