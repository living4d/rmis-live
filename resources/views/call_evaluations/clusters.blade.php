@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <h4>Research Grant Clusters</h4>
                        <ul>
                            <li><a href="#" title="">Dashboard</a></li>
                            <li><a href="{{url('calls')}}" title=""> Calls</a></li>
                            <li><a href="{{url('calls/'.$call->id)}}"> {{$call->title}}</a></li>
                            <li>Research Grant Clusters</li>
                        </ul>
                    </div><!-- Page Title -->
                </div>
                <!--./col-md-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    @include('calls.sidebar_menu')
                </div>
                <!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    <div class="pure-form">
                        <h4 class="font-weight-bold">Call Clusters</h4>

                        <div class="mt-2 listing">
                            @if(isset($call_clusters) && $call_clusters)
                            <ul>
                                @foreach ($call_clusters as $val)
                                <li>
                                    <a href="{{ url('call_evaluations/cluster_results/'.$call->id.'/'.$val->cluster->id)}}">
                                        <i class="fa fa-angle-double-right"></i> {{ $val->cluster->name }}</a>
                                </li>
                                @endforeach
                            </ul>
                            @else
                            <div class="alert alert-danger">No any cluster found</div>
                            @endif
                        </div>
                        <!--./listing -->

                    </div>
                    <!--./pure-form -->
                </div>
                <!--./col-lg-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection