@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Research Grants Applications</h4>
                            <ul>
                                <li><a href="#" title="">Dashboard</a></li>
                                <li>Research Grants Applications</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div>
                    <!--col-md-2 -->
                    <div class="col-lg-10 col-md-10">
                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <div class="row mt-2">
                            <div class="col-md-12">
                                <div class="search-form">
                                    {!! Form::open(['url' => 'call_evaluations/lists', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
                                    {{ Form::token() }}

                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-12">
                                            <div class="form-group">
                                                @php $_option = []; @endphp
                                                @foreach ($calls as $val)
                                                    @php $_option[$val->id] = $val->title; @endphp
                                                @endforeach
                                                @php $_option = ['' => 'Research Grant'] + $_option; @endphp
                                                {{ Form::select('call_id', $_option, old('call_id', $call_id), ['class="form-control chosen-select" id="fiter_call_id" ']) }}
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-md-3 -->

                                        <div class="col-md-6 col-sm-6 col-12">
                                            <div class="form-group">
                                                @php
                                                    $_option = ['' => 'Clusters'];
                                                @endphp
                                                {{ Form::select('cluster_id', $_option, old('cluster_id', $cluster_id), ['class="form-control" id="filter_cluster_id" ']) }}
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-md-3 -->

                                        <div class="col-md-6 col-sm-6 col-12">
                                            <div class="form-group">
                                                {{ Form::text('keyword', old('keyword'), ['placeholder="Project Title"', 'class="form-control"']) }}
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-md-3 -->

                                        <div class="col-md-3 col-sm-3 col-12">
                                            <div class="form-group">
                                                {{ Form::date('start_at', old('start_at'), ['class' => 'form-control', 'placeholder' => 'Start Date']) }}
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-md-3 -->

                                        <div class="col-md-3 col-sm-3 col-12">
                                            <div class="form-group">
                                                {{ Form::date('end_at', old('end_at'), ['class' => 'form-control', 'placeholder' => 'End Date']) }}
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-md-3 -->
                                    </div>
                                    <!--./row -->

                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <div class="float-right">
                                                <div class="form-group">
                                                    <button type="submit" name="filter" class="btn btn-secondary btn-xs">
                                                        <i class="fa fa-search"></i> Filter
                                                    </button>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./float-right -->
                                        </div>
                                        <!--./col-md-->
                                    </div>
                                    <!--./row -->
                                    {{ Form::close() }}
                                </div>
                                <!--./search-form -->
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->

                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-4">
                                <div class="font-weight-600 text-large">
                                    {{ number_format($count_apps) }} Grant Applications
                                </div>

                            </div>
                            <!--./col-md-4 -->

                            <div class="col-md-8">
                                <div class="float-right">
                                    <a title="Export PDF" class="btn btn-outline-primary btn-sm text-small" href="#">
                                        <i class="fa fa-file-pdf-o"></i> PDF</a>

                                    <a title="Export xlsx" class="btn btn-outline-info btn-sm text-small"
                                        href="{{ url('#') }}">
                                        <i class="fa fa-file-excel-o"></i> XLS</a>
                                </div>
                                <!--./float-right -->
                            </div>
                            <!--./col-md-2 -->
                        </div>
                        <!--./row -->

                        @if (isset($call_apps) && $call_apps)
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th width="3%">#</th>
                                        <th width="30%">Call Title</th>
                                        <th width="40%">Project Title</th>
                                        <th width="10%">Submitted</th>
                                        <th width="10%">Evaluation</th>
                                        <th style="width: 40px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @php($serial = 1)
                                        @foreach ($call_apps as $app)
                                            <tr>
                                                <td align="center">{{ $serial }}</td>
                                                <td>{!! $app->call_title !!}</td>
                                                <td> {!! $app->project_title !!}</td>
                                                <td>{{ date('d-m-Y H:i', strtotime($app->created_at)) }}</td>
                                                <td>
                                                    <div class="text-center">
                                                        @if ($app->eval_status == 0)
                                                            <span class="badge badge-pill badge-danger">NOT STARTED</span>
                                                        @elseif($app->eval_status == 1)
                                                            <span class="badge badge-pill badge-warning">INCOMPLETE</span>
                                                        @elseif($app->eval_status == 2)
                                                            <span class="badge badge-pill badge-success">COMPLETE</span>
                                                        @endif
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="text-center">
                                                        @if (\App\Perms::perm_methods('call_applications', 'details'))
                                                            <a href="{{ url('call_applications/details/' . $app->call_id . '/' . $app->call_app_id . '/#evaluations') }}"
                                                                title="Evaluations Results"><i
                                                                    class="fa fa-folder-open text-primary"></i>
                                                            </a>&nbsp;
                                                        @endif

                                                        @if (\App\Perms::perm_methods('call_evaluations', 'records'))
                                                            <a
                                                                href="{{ url('call_evaluations/records/' . $app->call_id . '/' . $app->call_app_id) }}">
                                                                <i class="fa fa-check-square-o text-secondary"></i>
                                                            </a>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                            @php($serial++)
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="pull-right">
                                        {{ $call_apps->links() }}
                                    </div>
                                    <!--./pull-right -->
                                @else
                                    <div class="alert alert-danger"> No any call application found.</div>
                                @endif
                            </div>
                            <!--./col-lg-9 -->
                        </div>
                        <!--./row -->
                    </div>
                    <!--./container -->
                </div>
                <!--./page-content -->
            </section>
        @endsection
