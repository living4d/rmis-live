@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <h4>Evaluation Tool</h4>
                        <ul>
                            <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="{{ url('calls') }}" title=""> Research Calls</a></li>
                            <li><a href="{{ route('calls.applications', $call->id) }}"> Applications</a></li>
                            <li><a href="{{ route('evaluations.records', [$call->id, $call_app->id])}}">Records</a></li>
                            <li>Tool</li>
                        </ul>
                    </div><!-- Page Title -->
                </div>
                <!--./col-md-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    @include('calls.sidebar_menu')
                </div>
                <!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    <div class="pure-form">
                        {!! $ev_content !!}

                        <div class="mt-4"></div>
                        <h5 class="font-weight-bold text-uppercase">Evaluation Tool</h5>
                        <hr/>

                        @if ($errors->any())
                        <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                {{ $error }}<br/>
                                @endforeach 
                        </div>
                        @endif

                        @if(session()->get('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div><br />
                        @elseif (session()->get('danger'))
                        <div class="alert alert-danger">
                            {{ session()->get('danger') }}
                        </div>
                        @endif

                        <span id="errmsg" style="color: red;"></span>
                        {{ Form::open(['url' => route('evaluations.store'), 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'formEv']) }}
                        {{ Form::token() }}
                        {{Form::hidden('call_id', $call->id, ['id' => 'call_id'])}}
                        {{Form::hidden('app_id', $call_app->id, ['id' => 'app_id'])}}
                        {{Form::hidden('criteria_id', $criteria_id, ['id' => 'criteria_id'])}}

                        <table class="table table-bordered">
                            <tr>
                                <th width="3%">No</th>
                                <th width="16%">Criteria</th>
                                <th width="8%">Weight</th>
                                <th width="24%">Sub criteria</th>
                                <th width="10%">Weight</th>
                                <th width="10%">Score/100 (%)</th>
                                <th width="24%">Comments</th>
                            </tr>

                            @php($serial = 1)
                            @foreach($ev_criteria as $value)
                            {{Form::hidden('weight[]', $value->sub_criteria[0]->weight, ['id' => 'weight'])}}
                            <tr>
                                <td rowspan="{{count($value->sub_criteria)}}">{{$serial}}</td>
                                <td rowspan="{{count($value->sub_criteria)}}">
                                    {{$value->attribute}}</td>
                                <td rowspan="{{count($value->sub_criteria)}}">
                                    {{$value->weight}}</td>
                                <td>{{$value->sub_criteria[0]->sub_criteria}}</td>
                                <td>{{$value->sub_criteria[0]->weight}}</td>
                                <td>
                                    {{Form::text('score[]', old('score[]'), ['class' => 'form-control score', 'percent' => $value->percentage])}}
                                </td>

                                <td>
                                    {{Form::textarea('comments[]', old('comments[]'), ['class' =>'form-control comments', "rows" => 3])}}
                                </td>
                                {{Form::hidden('sub_criteria_id[]', $value->sub_criteria[0]->id, ['id' => 'sub_criteria_id'])}}
                            </tr>

                            @for($i = 1; $i < sizeof($value->sub_criteria); $i++)
                            {{Form::hidden('weight[]', $value->sub_criteria[$i]->weight, ['id' => 'weight'])}}
                                <tr>
                                    <td>{{$value->sub_criteria[$i]->sub_criteria}}</td>
                                    <td>{{$value->sub_criteria[$i]->weight}}</td>
                                    <td>
                                        {{Form::text('score[]', old('score[]'), ['class' => 'form-control score', 'percent' => $value->percentage])}}
                                    </td>
                                    <td>
                                        {{Form::textarea('comments[]', old('comments[]'), ['class' =>'form-control comments', "rows" => 3])}}
                                    </td>
                                    {{Form::hidden('sub_criteria_id[]', $value->sub_criteria[$i]->id, ['id' => 'sub_criteria_id'])}}
                                </tr>
                                @endfor
                                @php($serial++)
                                @endforeach
                        </table>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="float-right">
                                    <div class="form-group">
                                        <button type="submit" id="btnEv"
                                            class="btn btn-secondary btn-sm font-weight-600">Evaluate</button>

                                        <a href="{{ route('evaluations.records', [$call->id, $call_app->id])}}"
                                            class="btn btn-danger btn-sm font-weight-600">Cancel</a>  
                                    </div>
                                </div>
                            </div>
                            <!--./col-lg-12 -->
                        </div>
                        <!--./row -->
                        {{ Form::close() }}
                    </div>
                    <!--./pure-form -->
                </div>
                <!--./col-lg-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection