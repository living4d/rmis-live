@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Call Evaluation</h4>
                            <ul>
                                <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('calls') }}" title=""> Research Calls</a></li>
                                <li><a href="{{ route('calls.applications', $call->id) }}"> Applications</a></li>
                                <li>Call Evaluation</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div>
                    <!--col-md-2 -->
                    <div class="col-lg-10 col-md-10">
                        <h5 class="font-weight-bold text-uppercase">{{ $call_app->project_title }}</h5>
                        <hr />

                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th width="3%">No</th>
                                <th width="14%">Criteria</th>
                                <th width="8%">Weight</th>
                                <th width="30%">Sub criteria</th>
                                <th width="8%">Weight</th>
                                <th width="8%">Score/100 (%)</th>
                                <th width="8%">Weighted Score (%) </th>
                                <th width="14%">Comments</th>
                                <th style="width: 16px;"></th>
                            </tr>

                            @php($serial = 1)
                                @php($sum_percent = 0)
                                    @php($sum_weight = 0)
                                        @php($sum_score = 0)
                                            @php($sum_points = 0)

                                                @foreach ($evaluation_criteria as $value)
                                                    @php($eval = \App\Models\Calls\CallApplicationEvaluation::where([
                                                        'application_id' => $call_app->id,
                                                        'criteria_id' => $value->sub_criteria[0]->criteria_id,
                                                        'sub_criteria_id' => $value->sub_criteria[0]->id,
                                                        'evaluator_id' => \App\Perms::get_current_user_id()])->first())
                                                        <tr>
                                                            <td width="3%" rowspan="{{ count($value->sub_criteria) }}">{{ $serial }}</td>
                                                            <td width="14%" rowspan="{{ count($value->sub_criteria) }}">
                                                                {{ $value->attribute }}</td>
                                                            <td width="8%" rowspan="{{ count($value->sub_criteria) }}">
                                                                {{ $value->weight }}</td>
                                                            <td width="30%">{{ $value->sub_criteria[0]->sub_criteria }}</td>
                                                            <td width="8%">{{ $value->sub_criteria[0]->weight }}</td>
                                                            <td width="8%">
                                                                @if ($eval)
                                                                    <span>{{ $eval->score }}</span>
                                                                @endif
                                                            </td>

                                                            <td width="8%">
                                                                @if ($eval)
                                                                    <span>{{ $eval->points }}</span>
                                                                @endif
                                                            </td>
                                                            <td width="16%">
                                                                @if ($eval)
                                                                    {{ $eval->comments }}
                                                                @endif
                                                            </td>
                                                            <td rowspan="{{ count($value->sub_criteria) }}">
                                                                @if (\App\Perms::perm_methods('call_evaluations', 'create'))
                                                                    @if (($call_app->eval_status == 0 || $call_app->eval_status == 1) && \App\Models\Calls\CallApplicationEvaluator::is_assigned_app($call_app->id))
                                                                        <a href="{{ route('evaluations.create', [$call->id, $call_app->id, $value->id]) }}"
                                                                            title="Evaluate" class="btn btn-outline-primary btn-xs">
                                                                            <i class="fa fa-pencil text-primary"></i>
                                                                        </a>
                                                                    @endif
                                                                @endif
                                                            </td>
                                                        </tr>

                                                        @for ($i = 1; $i < sizeof($value->sub_criteria); $i++)
                                                            @php($eval2 = \App\Models\Calls\CallApplicationEvaluation::where([
                                                                'application_id' => $call_app->id,
                                                                'criteria_id' => $value->sub_criteria[$i]->criteria_id,
                                                                'sub_criteria_id' => $value->sub_criteria[$i]->id,
                                                                'evaluator_id' => \App\Perms::get_current_user_id()])->first())
                                                                <tr>
                                                                    <td>{{ $value->sub_criteria[$i]->sub_criteria }}</td>
                                                                    <td>{{ $value->sub_criteria[$i]->weight }}</td>
                                                                    <td width="5%">
                                                                        @if ($eval2)
                                                                            <span>{{ $eval2->score }}</span>
                                                                        @endif
                                                                    </td>
                                                                    <td width="5%">
                                                                        @if ($eval2)
                                                                            <span>{{ $eval2->points }}</span>
                                                                        @endif
                                                                    </td>
                                                                    <td width="30%">
                                                                        @if ($eval)
                                                                            {{ $eval2->comments }}
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                @php($sum_weight += $value->sub_criteria[$i]->weight)

                                                                    <!--summation of scores and points -->
                                                                    @if ($eval2)
                                                                        @php($sum_score += $eval2->score)
                                                                            @php($sum_points += $eval2->points)
                                                                            @endif

                                                                            @php($sum_points += $value->sub_criteria[$i]->points)
                                                                            @endfor
                                                                            @php($serial++)
                                                                                @php($sum_percent += $value->percentage)
                                                                                    @php($sum_weight += $value->sub_criteria[0]->weight)

                                                                                        <!--summation of scores and points-->
                                                                                        @if ($eval)
                                                                                            @php($sum_score += $eval->score)
                                                                                                @php($sum_points += $eval->points)
                                                                                                @endif
                                                                                            @endforeach

                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td><b>Score</b></td>
                                                                                                <td>{{ $sum_weight }}</td>
                                                                                                <td></td>
                                                                                                <td>{{ $sum_points }}</td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                        </table>

                                                                                        @if (\App\Perms::perm_methods('call_evaluations', 'confirm_results'))
                                                                                            @if (($call_app->eval_status == 0 || $call_app->eval_status == 1) && \App\Models\Calls\CallApplicationEvaluator::is_assigned_app($call_app->id))
                                                                                                <div class="row">
                                                                                                    <div class="col-md-12">
                                                                                                        <div class="pull-right">
                                                                                                            <div class="form-group">
                                                                                                                <a href="{{ route('evaluations.confirm-results', [$call->id, $call_app->id]) }}"
                                                                                                                    class="btn btn-primary btn-sm font-weight-600">Confirm
                                                                                                                    Evaluations <i class="fa fa-angle-double-right"></i></a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <!--./col-lg-12 -->
                                                                                                </div>
                                                                                                <!--./row -->
                                                                                            @endif
                                                                                        @endif
                                                                                    </div>
                                                                                    <!--./col-lg-9 -->
                                                                                </div>
                                                                                <!--./row -->
                                                                            </div>
                                                                            <!--./container -->
                                                                        </div>
                                                                        <!--./page-content -->
                                                                    </section>
                                                                @endsection
