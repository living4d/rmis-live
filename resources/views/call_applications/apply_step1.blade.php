@extends('layouts.admin_app')
@section('content')

    @include('call_applications.add_more_researcher')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h4>Call Application</h4>
                            <ul>
                                <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('calls') }}">Calls</a></li>
                                <li>Call Application</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <!-- sidebar -->
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat pure-form">
                            <!--./card-header -->
                            <div class="card-body">
                                <h5 class="text-uppercase">{{ $call->title }}</h5>
                                <hr />

                                <div class="stepwizard">
                                    <div class="stepwizard-row setup-panel">
                                        <div class="stepwizard-step col-xs-3">
                                            <a href="#step-1" type="button"
                                                class="btn btn-info btn-circle text-white font-weight-bold">1</a>
                                        </div>
                                        <div class="stepwizard-step col-xs-3">
                                            <a href="#step-2" type="button"
                                                class="btn btn-danger btn-circle text-white font-weight-bold"
                                                disabled="disabled">2</a>
                                        </div>
                                        <div class="stepwizard-step col-xs-3">
                                            <a href="#step-3" type="button"
                                                class="btn btn-danger btn-circle text-white font-weight-bold"
                                                disabled="disabled">3</a>
                                        </div>
                                        <div class="stepwizard-step col-xs-3">
                                            <a href="#step-4" type="button"
                                                class="btn btn-danger btn-circle text-white font-weight-bold"
                                                disabled="disabled">4</a>
                                        </div>
                                    </div>
                                </div>
                                <!--./stepwizard -->

                                <div class="form mt-4">
                                    @if (session()->get('success'))
                                        <div class="alert alert-success">
                                            {{ session()->get('success') }}
                                        </div>
                                    @elseif (session()->get('info'))
                                        <div class="alert alert-info">
                                            {{ session()->get('info') }}
                                        </div>
                                    @elseif (session()->get('danger'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('danger') }}
                                        </div>
                                    @endif

                                    {{ Form::open(['url' => route('applications.save-step-1', [$call->id, $app_id]), 'method="POST" class="form-horizontal"']) }}
                                    {{ Form::token() }}
                                    {{ Form::hidden('url', url(''), ['id' => 'url']) }}
                                    {{ Form::hidden('user_id', $user->id, ['id' => 'user_id']) }}
                                    {{ Form::hidden('call_id', $call->id, ['id' => 'call_id']) }}

                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5>Step 1/4: Basic Information</h5>
                                        </div>
                                        <!--./col-md-12 -->
                                    </div>
                                    <!--./row -->

                                    <div class="row mt-2">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Title of the Project<span
                                                        style="color: red;">*</span></label>
                                                {{ Form::textarea('project_title', old('project_title', $call_app ? $call_app->project_title : ''), ['class="form-control" rows="2" placeholder="Write title..."']) }}
                                                <span class="text-danger">{{ $errors->first('project_title') }}</span>
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-lg-12 -->
                                    </div>
                                    <!--./row -->


                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Principal Investigator</label>
                                                {{ Form::text('principal_investigator', $user->first_name . ' ' . $user->middle_name . ' ' . $user->surname, ['readonly' => '', 'class' => 'form-control']) }}
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-lg-4 -->

                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Phone</label>
                                                {{ Form::text('phone', $user->phone, ['readonly' => '', 'class' => 'form-control']) }}
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-lg-4 -->

                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Email</label>
                                                {{ Form::text('email', $user->email, ['readonly' => '', 'class' => 'form-control', 'class' => 'form-control']) }}
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-lg-4 -->
                                    </div>
                                    <!--./row -->

                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>College/School/Institute</label>
                                                {{ Form::text('college', $college ? $college->name : 'No college', ['readonly' => '', 'class' => 'form-control']) }}
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-lg-4 -->

                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Department</label>
                                                {{ Form::text('department', $department ? $department->name : 'No department', ['readonly' => '', 'class' => 'form-control']) }}
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-lg-4 -->

                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Faculty</label>
                                                {{ Form::text('faculty', $faculty ? $faculty->name : 'No faculty', ['readonly' => '', 'class' => 'form-control']) }}
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-lg-4 -->
                                    </div>
                                    <!--./row -->

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <p class="font-weight-600" style="color: red;">Selection of team composition
                                                (Research Partners) should consider reflections on interdisciplinary
                                                work, team work, gender mix (at least 30% female) and junior-senior mix (at
                                                least 30% junior stass)</p>
                                            <p class="font-weight-600" style="color: red;">Note: This criterion will be used
                                                in
                                                evaluation process.</p>
                                        </div>
                                        <!--./col-md-12 -->
                                    </div>
                                    <!--./row -->

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Research Partner (UDSM Staff)</label>
                                                @php($_option = [])
                                                    @foreach ($users as $us)
                                                        @php($_option[$us->id] = $us->first_name . ' ' . $us->middle_name . ' '
                                                            . $us->surname)
                                                        @endforeach
                                                        {{ Form::select('partner_ids[]', $_option, old('partner_ids[]', $arr_internal_partners), ['data-placeholder="   -- Select --" class="form-control chosen-select" multiple']) }}
                                                    </div>
                                                    <!--./form-group -->
                                                </div>
                                                <!--./col-lg-12 -->
                                            </div>
                                            <!--./row -->

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label>Research Partner (UDSM Students, If any)</label>
                                                        @php($_option = [])
                                                            @foreach ($students as $us)
                                                                @php($_option[$us->id] = $us->first_name . ' ' . $us->middle_name . ' '
                                                                    . $us->surname)
                                                                @endforeach
                                                                {{ Form::select('student_ids[]', $_option, old('student_ids[]', $arr_student_partners), ['data-placeholder="   -- Select --" class="form-control chosen-select" multiple']) }}
                                                            </div>
                                                            <!--./form-group -->
                                                        </div>
                                                        <!--./col-lg-12 -->
                                                    </div>
                                                    <!--./row -->

                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label>Research Partner (Non UDSM Staff, If any)</label>
                                                                @php($_option = [])
                                                                    @foreach ($researchers as $us)
                                                                        @php($_option[$us->id] = $us->full_name)
                                                                        @endforeach
                                                                        {{ Form::select('researcher_ids[]', $_option, old('researcher_ids[]', $arr_external_partners), ['data-placeholder="   -- Select --" class="form-control chosen-select" multiple']) }}
                                                                    </div>
                                                                    <!--./form-group -->
                                                                </div>
                                                                <!--./col-lg-12 -->
                                                            </div>
                                                            <!--./row -->

                                                            <div class="row field_wrapper"></div>
                                                            <!--./row -->
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="pull-right">
                                                                        <a href="javascript:void(0);" class="add_button btn btn-info btn-sm"
                                                                            title="Add Researcher"><i class="fa fa-plus fa-lg"></i> Add More</a>
                                                                    </div>
                                                                    <!--./pull-right -->
                                                                </div>
                                                                <!--./col-lg-12 -->
                                                            </div>
                                                            <!--./row -->


                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                    <label>Application Grant Category</label>
                                                                        {{Form::select('application_grant_category', ['' => 'Select Application Grant Category',
                                                                            'research_grant' => 'Multidisciplinary Research Grant',
                                                                            'innovation_grant' => 'Innovation or Commercialization Grant'], old('application_grant_category'), ['class'=> 'form-control'])}}
                                                                            <span class="text-danger">{{ $errors->first('application_grant_category') }}</span>
                                                                    </div>
                                                                <!--./form-group -->
                                                                </div>
                                                                <!--./col-lg-12 -->
                                                            </div>
                                                            <!--./row -->


                                                            <div class="mt-4"></div>
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="form-group float-right">
                                                                        <a href="{{ url('calls') }}"
                                                                            class="btn btn-danger font-weight-600">Cancel</a>

                                                                        <button type="submit" name="save"
                                                                            class="btn btn-secondary font-weight-600">Save and
                                                                            Exit</button>

                                                                        <button type="submit" name="continue"
                                                                            class="btn btn-success font-weight-600">Save and
                                                                            Continue <i class="fa fa-chevron-right"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--./row -->
                                                            {{ Form::close() }}
                                                        </div><!-- ./form -->
                                                    </div>
                                                    <!--./card-body -->
                                                </div>
                                                <!--./card -->
                                            </div>
                                            <!--./col-lg-12 -->
                                        </div>
                                        <!--./row -->
                                    </div>
                                    <!--./container -->
                                </div>
                                <!--./page-content -->
                            </section>
                        @endsection
