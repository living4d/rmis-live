@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Research Calls Applications</h4>
                            <ul>
                                <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('calls') }}" title=""> Research Calls</a></li>
                                <li><a href="{{ route('calls.applications', $call->id) }}"> Applications</a></li>
                                <li>{{ $call->title }}</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <!--./tabs -->
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="information-tab" data-toggle="tab" href="#information"
                                    role="tab" aria-controls="information" aria-selected="true">Application</a>
                            </li>

                            @if (\App\Perms::perm_methods('calls', 'evaluators'))
                                <li class="nav-item">
                                    <a class="nav-link" id="invitation-tab" data-toggle="tab" href="#evaluators" role="tab"
                                        aria-controls="invitation" aria-selected="false">Evaluators</a>
                                </li>
                            @endif
                        </ul>
                        <!--./tab-lists -->

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="information" role="tabpanel"
                                aria-labelledby="information-tab">
                                <div class="row mt-2">
                                    <div class="col-md-10">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="25%">
                                                    <span class="font-weight-bold">Project Title</span>
                                                </th>
                                                <td colspan="3">
                                                    <span
                                                        class="text-uppercase font-weight-bold">{{ $call_app->project_title }}</span>
                                                </td>
                                            </tr>
                                            @if (!Auth::user()->hasRole('evaluators'))
                                                <tr>
                                                    <th>
                                                        <span class="font-weight-bold">Principal Investigator</span>
                                                    </th>
                                                    <td colspan="3">
                                                        {{ $call_app->pi->first_name . ' ' . $call_app->pi->middle_name . ' ' . $call_app->pi->surname }}
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">Internal Partners (UDSM
                                                            Staff)</span>
                                                    </th>
                                                    <td colspan="3">
                                                        @php $i = 1; @endphp
                                                        @foreach ($partners as $v)
                                                            {{ $i . '. ' . $v->partner->first_name . ' ' . $v->partner->middle_name . ' ' . $v->partner->surname }}
                                                            <br />
                                                            @php $i++; @endphp
                                                        @endforeach
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">Research Partner (UDSM
                                                            Students)</span>
                                                    </th>
                                                    <td colspan="3">
                                                        @php $i = 1; @endphp
                                                        @foreach ($students as $v)
                                                            {{ $i . '. ' . $v->partner->first_name . ' ' . $v->partner->middle_name . ' ' . $v->partner->surname }}
                                                            <br />
                                                            @php $i++; @endphp
                                                        @endforeach
                                                    </td>

                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">External Partners (Non-UDSM
                                                            Staff)</span>
                                                    </th>
                                                    <td colspan="3">
                                                        @php $i = 1; @endphp
                                                        @foreach ($external_partners as $v)
                                                            {!! $i . '. ' . $v->partner->full_name !!}
                                                            <br />
                                                            @php $i++; @endphp
                                                        @endforeach
                                                    </td>
                                                </tr>
                                            @endif

                                            <tr>
                                                <th><span class="font-weight-bold">Call Category</span></th>
                                                <td>
                                                    @if ($call_app->callCategory)
                                                        {{ $call_app->callCategory->title }}
                                                    @endif
                                                </td>

                                                <th><span class="font-weight-bold">Research Type</span></th>
                                                <td>
                                                    @if ($call_app->researchType)
                                                        {{ $call_app->researchType->name }}
                                                    @endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <th><span class="font-weight-bold">Call Cluster</span></th>
                                                <td>
                                                    @if ($call_app->cluster)
                                                        {{ $call_app->cluster->name }}
                                                    @endif
                                                </td>

                                                <th><span class="font-weight-bold">Research Area</span></th>
                                                <td>
                                                    @if ($call_app->researchArea)
                                                        {{ $call_app->researchArea->name }}
                                                    @endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <th><span class="font-weight-bold">Introduction</span></th>
                                                <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->introduction) !!}</td>
                                            </tr>

                                            <tr>
                                                <th><span class="font-weight-bold">Problem Statement</span></th>
                                                <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->problem_statement) !!}</td>
                                            </tr>

                                            <tr>
                                                <th><span class="font-weight-bold">Rationale</span></th>
                                                <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->rationale) !!}</td>
                                            </tr>

                                            <tr>
                                                <th><span class="font-weight-bold">General Objective</span></th>
                                                <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->general_objective) !!}</td>
                                            </tr>

                                            <tr>
                                                <th><span class="font-weight-bold">Specific Objective</span></th>
                                                <td colspan="3">
                                                    @php $j = 1; @endphp
                                                    @foreach ($call_app->objectives as $v)
                                                        {{ $j . '. ' . $v->objective }}<br />
                                                        @php $j++; @endphp
                                                    @endforeach
                                                </td>
                                            </tr>

                                            <tr>
                                                <th><span class="font-weight-bold">Literature Review</span></th>
                                                <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->literature_review) !!}</td>
                                            </tr>

                                            <tr>
                                                <th><span class="font-weight-bold">Methodology</span></th>
                                                <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->methodology) !!}</td>
                                            </tr>

                                            <tr>
                                                <th><span class="font-weight-bold">Research Output</span></th>
                                                <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->research_output) !!}</td>
                                            </tr>

                                            <tr>
                                                <th>
                                                    <span class="font-weight-bold">Sustainability Measures</span>
                                                </th>
                                                <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->sustainability_measures) !!}</td>
                                            </tr>

                                            <tr>
                                                <th><span class="font-weight-bold">Management Structure</span></th>
                                                <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->management_structure) !!}</td>
                                            </tr>

                                            <tr>
                                                <th><span class="font-weight-bold">Implementation Plan</span></th>
                                                <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->implementation_plan) !!}</td>
                                            </tr>

                                            <tr>
                                                <th><span class="font-weight-bold">Source Fund</span></th>
                                                <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->source_fund) !!}</td>
                                            </tr>

                                            <tr>
                                                <th><span class="font-weight-bold">Proposal Attachment</span></th>
                                                <td >
                                                    @if (file_exists(public_path() . '/assets/uploads/documents/' . $call_app->attachment))
                                                        <a class="btn btn-primary btn-sm"
                                                            href="{{ asset('assets/uploads/documents/' . $call_app->attachment) }}"
                                                            download><i class="fa fa-download"></i> Download Proposal</a>
                                                    @endif
                                                </td>
                                                <th><span class="font-weight-bold">CV Attachment</span></th>
                                                <td >
                                                    @if (file_exists(public_path() . '/assets/uploads/documents/' . $call_app->cv_attachment))
                                                        <a class="btn btn-primary btn-sm"
                                                            href="{{ asset('assets/uploads/documents/' . $call_app->cv_attachment) }}"
                                                            download><i class="fa fa-download"></i> Download CV</a>
                                                    @endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <th><span class="font-weight-bold">Budget Summary</span></th>
                                                <td colspan="3">
                                                    <h5>
                                                        @if ($call_app->callCategory)
                                                            Budget Ceiling :
                                                            {{ $call_app->callCategory->currency . ' ' . number_format($call_app->callCategory->budget_ceiling) }}
                                                        @endif


                                                        <table width="100%" class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th width="75%">Budget Line</th>
                                                                    <th>Amount (TZS)</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @php $i = 1; @endphp
                                                                @php $sum = 0; @endphp
                                                                @foreach ($call_app->objectives as $value)
                                                                    <tr>
                                                                        <td>{{ $i . '. ' . $value->objective }}
                                                                        </td>
                                                                        <td>{{ number_format($value->budget) }}</td>
                                                                    </tr>
                                                                    @php $sum += $value->budget; @endphp
                                                                    @php $i++; @endphp
                                                                @endforeach
                                                                <tr>
                                                                    <td><b>Total budget</b></td>
                                                                    <td><span id="sum">{{ number_format($sum) }}</span>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </h5>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th><span class="font-weight-bold">Application Date</span></th>
                                                <td colspan="3">
                                                    {{ date('l, jS F, Y H:i:s', strtotime($call_app->created_at)) }}
                                                </td>
                                            </tr>

                                        </table>
                                    </div>
                                    <!--./col-md-10 -->

                                    <div class="col-md-2">
                                        <div class="row">
                                            @if (\App\Perms::perm_methods('call_applications', 'edit'))
                                                <div class="col-md-12 mt-3">
                                                    <a href="{{ route('applications.edit', [$call_app->id]) }}"
                                                        class="btn btn-secondary btn-block text-medium">
                                                        <i class="fa fa-pencil"></i> Edit</a>
                                                </div>
                                                <!--./col-md-12 -->
                                            @endif

                                            @if (\App\Perms::perm_methods('call_applications', 'assign_evaluators'))
                                                <div class="col-md-12 mt-3">
                                                    <a href="{{ route('applications.assign-evaluators', [$call_app->id]) }}"
                                                        class="btn btn-info btn-block text-medium">
                                                        <i class="fa fa-users"></i> Assign Evaluators</a>
                                                </div>
                                                <!--./col-md-12 -->
                                            @endif
                                        </div>
                                        <!--./row -->
                                    </div>
                                    <!--./col-md-2 -->
                                </div>
                                <!--./row -->
                            </div>
                            <!--./information -->

                            <div class="tab-pane fade" id="evaluators" role="tabpanel" aria-labelledby="invitation-tab">
                                <div class="row p-2">
                                    <div class="col-md-12">

                                        <div class="row">
                                            @if (\App\Perms::perm_methods('call_applications', 'assign_evaluators'))
                                                <div class="col-md-12 mt-3">
                                                    <a href="{{ route('applications.assign-evaluators', [$call->id, $call_app->id]) }}"
                                                        class="btn btn-info btn-sm text-medium">
                                                        <i class="fa fa-users"></i> Assign Evaluators</a>
                                                </div>
                                                <!--./col-md-12 -->
                                            @endif
                                        </div>
                                        <!--./row -->

                                        <!--evaluators -->
                                        @if (isset($call_app->evaluators) && $call_app->evaluators)
                                            <table class="table table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th width="3%">#</th>
                                                        <th width="30%">Name</th>
                                                        <th width="25%">Institution</th>
                                                        <th width="18%">Contacts</th>
                                                        <th width="15%">Assigned At</th>
                                                        <th style="width: 20px;">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $serial = 1; @endphp
                                                    @foreach ($call_app->evaluators as $values)
                                                        <tr>
                                                            <td>{{ $serial }}</td>
                                                            <td>{{ $values->user->first_name . ' ' . $values->user->middle_name . ' ' . $values->user->surname }}
                                                            </td>
                                                            <td>{{ $values->user->institution }}</td>
                                                            <td>{!! $values->user->email . '<br />' . $values->user->phone !!}</td>
                                                            <td>{{ date('d-m-Y', strtotime($values->created_at)) }}
                                                            </td>
                                                            <td>
                                                                @if (\App\Perms::perm_methods('call_applications', 'drop_evaluator'))
                                                                    <a href="{{ route('applications.drop_evaluator', [$call->id, $call_app->id, $values->id]) }}"
                                                                        title="Delete" class="btn btn-danger btn-xs"
                                                                        onClick="return confirm('Drop evaluator?')">
                                                                        <i class="fa fa-trash text-danger"></i>
                                                                    </a>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @php $serial++; @endphp
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <div class="alert alert-danger mt-2"> No any assigned evaluator.
                                            </div>
                                        @endif

                                    </div>
                                    <!--./col-md-12 -->
                                </div>
                                <!--./row -->
                            </div>
                            <!--./evaluators -->
                        </div>
                        <!--./tab-content -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
    <script>
        $(function() {
            var hash = window.location.hash;
            hash && $('ul.nav a[href="' + hash + '"]').tab('show');
        });
    </script>
@endsection
