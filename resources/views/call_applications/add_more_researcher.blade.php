<script type="text/javascript">
    $(document).ready(function () {
        let maxField = 5; //Input fields increment limitation
        let addButton = $('.add_button'); //Add button selector
        let wrapper = $('.field_wrapper'); //Input field wrapper
        let fieldHTML = '<div class="col-lg-4 col-md-4">' +
            '<div class="form-group">' +
            '<label>Full name</label>' +
            '<input type="text" id="full_name[]" name="full_name[]" placeholder="Write full name..." class="form-control">' +
            '</div>' +
            '</div>' +
            '<div class="col-lg-4 col-md-4">' +
            '<div class="form-group">' +
            '<label>Email</label>' +
            '<input type="text" id="email[]" name="email[]" placeholder="Write email address..." class="form-control">' +
            '</div>' +
            '</div>' +
            '<div class="col-lg-4 col-md-4">' +
            '<div class="form-group">' +
            '<label>Phone</label>' +
            '<input type="text" id="phone[]" name="phone[]" placeholder="Write phone..." class="form-control">' +
            '</div>' +
            '</div>';

        let x = 1; //Initial field counter is 1
        //Once add button is clicked
        $(addButton).click(function () {
            //Check maximum number of input fields
            if (x < maxField) {
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
            }
        });

        //Once remove button is clicked
        $(wrapper).on('click', '.remove_button', function (e) {
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });
    });



</script>