@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="page-title">
                        <h4>Call Application</h4>
                        <ul>
                            <li><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="{{ url('calls') }}" title=""> Research Calls</a></li>
                            <li>Call Application</li>
                        </ul>
                    </div><!-- Page Title -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <!-- sidebar -->
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat pure-form">
                            <div class="card-body">
                                <h5 class="text-uppercase">{{ $call->title }}</h5>
                                <hr />
                                
                                <div class="alert alert-success">
                                    <p class="text-large font-weight-600">Aplication for the call "{{ $call->title }}"
                                        has
                                        been completed successfully. <br /><br />Please visit the portal frequently to see the
                                        status of your application. </p>
                                </div>
                            </div>
                            <!--./card-body -->
                        </div>
                        <!--./card -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
