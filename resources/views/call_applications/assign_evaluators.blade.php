@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Assign Evaluators</h4>
                            <ul>
                                <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{url('calls')}}" title=""> Research Calls</a></li>
                                <li><a href="{{url('calls/'.$call->id)}}" title=""> Call
                                        Applications</a></li>
                                <li>{{$call->title}}</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div><!--./col-md-12 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div><!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat">
                            <div class="card-body">
                                <h5 class="text-uppercase">Assign Evaluators</h5>
                                <hr />

                                @if(session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div><br/>
                                @endif

                                {{ Form::open(['url' => route('applications.store-evaluators', [$call->id, $call_app->id]), 'method' => 'POST', 'class' => 'form-horizontal']) }}
                                {{ Form::token() }}

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Title of the Research Project<span
                                                        style="color: red;">*</span></label>
                                            {{Form::textarea('project_title', $call_app->project_title, ['class="form-control" rows="3" readonly=""'])}}
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Principal Investigator</label>
                                            {{Form::text('principal_investigator', $investigator->first_name.' '.$investigator->surname, ['class="form-control" readonly=""'])}}
                                        </div>
                                        <!--./form-group -->
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Call Evaluators</label>
                                            @php ($_option = [])
                                            @foreach($users as $us)
                                                @php ($_option[$us->id] = $us->first_name.' '.$us->middle_name.' '.$us->surname)
                                            @endforeach
                                            {{Form::select('evaluator_ids[]', $_option, old('evaluator_ids[]'), ['data-placeholder="   -- Select --" class="form-control chosen-select" multiple'])}}
                                            <span class="text-danger">{{ $errors->first('evaluator_ids') }}</span>
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-secondary btn-sm font-weight-600">Assign</button>
                                            <a href="{{ route('applications.details', [$call->id, $call_app->id]) }}"
                                                class="btn btn-danger btn-sm font-weight-600">Cancel</a>
                                        </div>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->
                                {{ Form::close() }}
                            </div><!--./card-body -->
                        </div><!--./card -->
                    </div><!--./col-lg-12 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
@endsection