@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>My Applications</h4>
                            <ul>
                                <li><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li>My Applications</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div>

                    <!--col-md-2 -->
                    <div class="col-lg-10 col-md-10">
                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif
                        <div class="messages"></div>

                        <div class="row mt-2">
                            <div class="col-md-12">
                                <div class="search-form">
                                    {!! Form::open(['url' => route('applications.lists'), 'method' => 'POST', 'class' => 'form-horizontal']) !!}
                                    {{ Form::token() }}

                                    <div class="row">
                                        <div class="col-md-10 col-sm-10 col-12">
                                            <div class="form-group">
                                                @php $_option = []; @endphp
                                                @foreach ($calls as $val)
                                                    @php $_option[$val->id] = $val->title; @endphp
                                                @endforeach
                                                @php $_option = ['' => '-- Research Calls --'] + $_option; @endphp
                                                {{ Form::select('call_id', $_option, old('call_id'), ['class="form-control chosen-select" id="fiter_call_id" ']) }}
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-md-10 -->

                                        <div class="col-md-2 col-sm-2 col-12">
                                            <div class="form-group">
                                                <button type="submit" name="filter" class="btn btn-secondary btn-sm">
                                                    <i class="fa fa-search"></i> Filter
                                                </button>
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-md-2-->
                                    </div>
                                    <!--./row -->
                                    {{ Form::close() }}
                                </div>
                                <!--./search-form -->
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->

                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-4">
                                <div class="font-weight-600 text-large">
                                    {{ number_format($count_apps) }} Call Applications
                                </div>

                            </div>
                            <!--./col-md-4 -->

                            <div class="col-md-8">
                                <div class="float-right">
                                    <a title="Export PDF" class="btn btn-outline-primary btn-sm text-small" href="#">
                                        <i class="fa fa-file-pdf-o"></i> PDF</a>

                                    <a title="Export xlsx" class="btn btn-outline-info btn-sm text-small"
                                        href="{{ url('#') }}">
                                        <i class="fa fa-file-excel-o"></i> XLS</a>
                                </div>
                                <!--./float-right -->
                            </div>
                            <!--./col-md-2 -->
                        </div>
                        <!--./row -->

                        @if (isset($call_apps) && $call_apps)
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th width="3%">#</th>
                                        <th width="20%">Call Title</th>
                                        <th width="28%">Project Title</th>
                                        <th width="10%">Budget</th>
                                        <th width="8%">Submitted</th>
                                        <th width="8%">Approval</th>
                                        <th width="8%">Evaluation Status</th>
                                        <th width="8%">Evaluation Grade</th>
                                        <th style="width: 60px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @php($serial = 1)
                                        @foreach ($call_apps as $app)
                                            <tr>
                                                <td>{{ $serial }}</td>
                                                <td>{!! $app->call_title !!}</td>
                                                <td> {!! $app->project_title !!}</td>
                                                <td>{{ number_format($app->app_budgets) }}</td>
                                                <td>{{ date('d-m-Y H:i', strtotime($app->created_at)) }}</td>
                                                <td>{{ show_application_approval_status($app->call_app_id) }}
                                                </td>
                                                <td>
                                                    <div class="text-center">
                                                        @if ($app->eval_status == 0)
                                                            <span class="badge badge-pill badge-danger">NOT
                                                                STARTED</span>
                                                        @elseif($app->eval_status == 1)
                                                            <span class="badge badge-pill badge-warning">ON
                                                                PROGRESS</span>
                                                        @elseif($app->eval_status == 2)
                                                            <span class="badge badge-pill badge-success">COMPLETE</span>
                                                        @endif
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="text-center">
                                                        {!! $app->call_evaluation_result ? round($app->call_evaluation_result->avg_points, 2) : '0' !!}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="text-center">
                                                        @if (\App\Perms::perm_methods('call_applications', 'details'))
                                                            <a href="{{ route('applications.details', $app->call_app_id) }}"
                                                                class="btn btn-outline-primary btn-xs" title="Show details"><i
                                                                    class="fa fa-folder-open text-primary"></i>
                                                            </a>
                                                        @endif

                                                        @if (($app->winner == 1) & !\App\Models\Projects\Project::is_project_available($app->call_app_id))
                                                            <a href="#projectModal" data-backdrop="static" data-keyboard="false"
                                                                data-toggle="modal" data-target="#projectModal"
                                                                data-app-id="{{ $app->call_app_id }}" data-toggle="tooltip"
                                                                data-placement="bottom" title="Register Project"
                                                                id="btnRegisterModal" class="btn btn-outline-success btn-xs">
                                                                <i class="fa fa-plus-square text-success"></i>
                                                            </a>
                                                        @endif
                                                    </div>
                                                    <!--./text-center -->
                                                </td>
                                            </tr>
                                            @php($serial++)
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="pull-right">
                                        {{ $call_apps->links() }}
                                    </div>
                                    <!--./pull-right -->
                                @else
                                    <div class="alert alert-danger"> No any call application found.</div>
                                @endif
                            </div>
                            <!--./col-lg-9 -->
                        </div>
                        <!--./row -->
                    </div>
                    <!--./container -->
                </div>
                <!--./page-content -->
            </section>

            <!-- Modal -->
            <div class="modal fade" id="projectModal" tabindex="-1" role="dialog" aria-labelledby="projectModal" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title font-weight-600">Project Information</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!--./model-header-->

                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="notificationBar"></div>
                                    <span id="errorMsg-2" class="text-danger"></span>

                                    <table class="table table-striped table-bordered table-sm">
                                        <tr>
                                            <th width="20%"><span class="font-weight-bold">Project Title</span></th>
                                            <td colspan="3"><span id="projectTitle"></span></td>
                                        </tr>

                                        <tr>
                                            <th><span class="font-weight-bold">Project Number</span></th>
                                            <td colspan="3"><span id="projectNumber"></span></td>
                                        </tr>
                                    </table>
                                </div>
                                <!--./col-md-12 -->

                                <div class="col-md-12">
                                    <div class="float-left">
                                        <button type="button" id="btn-register-project" class="btn btn-success font-weight-600"><i
                                                class="fa fa-plus-square"></i> Register Project</button>
                                    </div>
                                    <!--./float-right -->
                                </div>
                                <!--./col -->
                            </div>
                            <!--./row -->
                        </div>
                        <!--./model-body-->

                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                                Close
                            </button>
                        </div>
                        <!--./model-footer-->
                    </div>
                </div>
            </div>
            <!--./modal-->
        @endsection
