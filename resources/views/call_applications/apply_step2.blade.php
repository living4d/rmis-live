@extends('layouts.admin_app')
@section('content')

    @include('call_applications.add_more_objective')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h4>Call Application</h4>
                            <ul>
                                <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('calls') }}"> Calls</a></li>
                                <li>Call Application</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <!-- sidebar -->
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat pure-form">
                            <!--./card-header -->
                            <div class="card-body">
                                <h5 class="text-uppercase">{{ $call->title }}</h5>
                                <hr />

                                <div class="stepwizard">
                                    <div class="stepwizard-row setup-panel">
                                        <div class="stepwizard-step col-xs-3">
                                            <a href="#step-1" type="button"
                                                class="btn btn-success btn-circle text-white font-weight-bold">1</a>
                                        </div>
                                        <div class="stepwizard-step col-xs-3">
                                            <a href="#step-2" type="button"
                                                class="btn btn-info btn-circle text-white font-weight-bold"
                                                disabled="disabled">2</a>
                                        </div>
                                        <div class="stepwizard-step col-xs-3">
                                            <a href="#step-3" type="button"
                                                class="btn btn-danger btn-circle text-white font-weight-bold"
                                                disabled="disabled">3</a>
                                        </div>
                                        <div class="stepwizard-step col-xs-3">
                                            <a href="#step-4" type="button"
                                                class="btn btn-danger btn-circle text-white font-weight-bold"
                                                disabled="disabled">4</a>
                                        </div>
                                    </div>
                                </div>
                                <!--./stepwizard -->

                                <div class="form mt-4">
                                    @if (session()->get('success'))
                                        <div class="alert alert-success">
                                            {{ session()->get('success') }}
                                        </div>
                                    @elseif (session()->get('info'))
                                        <div class="alert alert-info">
                                            {{ session()->get('info') }}
                                        </div>
                                    @elseif (session()->get('danger'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('danger') }}
                                        </div>
                                    @endif

                                    {{ Form::open(['url' => route('applications.save-step-2', [$call->id, $call_app->id]), 'method="POST"']) }}
                                    @method('PUT')
                                    {{ Form::token() }}
                                    {{ Form::hidden('url', url(''), ['id' => 'url']) }}
                                    {{ Form::hidden('user_id', $user->id, ['id' => 'user_id']) }}
                                    {{ Form::hidden('call_id', $call->id, ['id' => 'call_id']) }}

                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5>Step 2/4: Proposal</h5>
                                        </div>
                                        <!--./col-md-12 -->
                                    </div>
                                    <!--./row -->

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Title of the Project<span
                                                        style="color: red;">*</span></label>
                                                {{ Form::textarea('project_title', $call_app->project_title, ['class="form-control" rows="3" readonly=""']) }}
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-lg-12 -->
                                    </div>
                                    <!--./row -->

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Call Category<span style="color: red;">*</span></label>
                                                <br /><small><i>Note: Select innovation and commercialization of technology <b>only if</b> it is innovation grant (as selected in the previous page); otherwise, for research grant please select either of the three categories (Natural and Applied Science, ICT, or the Social sciences)</i></small>
                                                @php($_option = [])
                                                    @foreach ($call_categories as $val)
                                                        @php($_option[$val->id] = $val->title)
                                                        @endforeach
                                                        {{ Form::select('call_category_id', ['' => '-- Select --'] + $_option, old('call_category_id', $call_app->call_category_id), ['class' => 'form-control', 'id' => 'call_category_id']) }}
                                                        <span class="text-danger">{{ $errors->first('call_category_id') }}</span>
                                                    </div>
                                                    <!--./form-group -->
                                                </div>
                                                <!--./col-lg-6 -->

                                                {{-- <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Type of Research<span style="color: red;">*</span></label>
                                                        @php($_option = [])
                                                            @foreach ($research_types as $dp)
                                                                @php($_option[$dp->id] = $dp->name)
                                                                @endforeach
                                                                {{ Form::select('research_type_id', ['' => 'Select type of research'] + $_option, old('research_type_id', $call_app->research_type_id), ['class' => 'form-control', 'id' => 'research_type_id']) }}
                                                                <span class="text-danger">{{ $errors->first('research_type_id') }}</span>
                                                            </div>
                                                            <!--./form-group -->
                                                        </div> --}}
                                                        <!--./col-lg-12 -->
                                                    </div>
                                                    <!--./row -->

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label>Cluster <span style="color: red;">*</span></label>
                                                                @php($_option = [])
                                                                    @foreach ($clusters as $val)
                                                                        @php($_option[$val->cluster->id] = $val->cluster->name)
                                                                        @endforeach
                                                                        {{ Form::select('cluster_id', ['' => '-- Select --'] + $_option, old('cluster_id', $call_app->call_cluster_id), ['class' => 'form-control', 'id' => 'cluster_id', 'onchange' => 'suggest_research_areas();']) }}
                                                                        <span class="text-danger">{{ $errors->first('cluster_id') }}</span>
                                                                    </div>
                                                                    <!--./form-group -->
                                                                </div>
                                                                <!--./col-lg-12 -->

                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <label>Research Area <span style="color: red;">*</span></label>
                                                                        @php($_option = [])
                                                                            @foreach ($research_areas as $val)
                                                                                @php($_option[$val->id] = $val->name)
                                                                                @endforeach
                                                                                {{ Form::select('research_area_id', ['' => '--Select --'] + $_option, old('research_area_id', $call_app->call_research_area_id), ['class' => 'form-control', 'id' => 'research_area_id']) }}
                                                                                <span class="text-danger">{{ $errors->first('research_area_id') }}</span>
                                                                            </div>
                                                                            <!--./form-group -->
                                                                        </div>
                                                                        <!--./col-lg-12 -->
                                                                    </div>
                                                                    <!--./row -->

                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">
                                                                                <label>Summary <span style="color: red;">*</span></label>
                                                                                {{ Form::textarea('summary', old('summary', $call_app->summary), ['class="form-control" id="summary" rows="5" placeholder="Write summary..."']) }}
                                                                                <script>
                                                                                    CKEDITOR.replace('summary');
                                                                                </script>
                                                                                <span class="text-danger">{{ $errors->first('summary') }}</span>
                                                                            </div>
                                                                            <!--./form-group -->
                                                                        </div>
                                                                        <!--./col-lg-12 -->
                                                                    </div>
                                                                    <!--./row -->

                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">
                                                                                <label>Introduction <span style="color: red;">*</span></label>
                                                                                {{ Form::textarea('introduction', old('introduction', $call_app->introduction), ['class="form-control" id="introduction" rows="5" placeholder="Write introduction..."']) }}
                                                                                <script>
                                                                                    CKEDITOR.replace('introduction');
                                                                                </script>
                                                                                <span class="text-danger">{{ $errors->first('introduction') }}</span>
                                                                            </div>
                                                                            <!--./form-group -->
                                                                        </div>
                                                                        <!--./col-lg-12 -->
                                                                    </div>
                                                                    <!--./row -->

                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">
                                                                                <label> Problem Statement<span style="color: red;">*</span></label>
                                                                                {{ Form::textarea('problem_statement', old('problem_statement', $call_app->problem_statement), ['class="form-control" id="problem_statement" rows="5" placeholder="Write problem statement..."']) }}
                                                                                <script>
                                                                                    CKEDITOR.replace('problem_statement');
                                                                                </script>
                                                                                <span
                                                                                    class="text-danger">{{ $errors->first('problem_statement') }}</span>
                                                                            </div>
                                                                            <!--./form-group -->
                                                                        </div>
                                                                        <!--./col-lg-12 -->
                                                                    </div>
                                                                    <!--./row -->

                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">
                                                                                <label>Rationale <span style="color: red;">*</span></label>
                                                                                {{ Form::textarea('rationale', old('rationale', $call_app->rationale), ['class="form-control" id="rationale" rows="5" placeholder="Write rationale..."']) }}
                                                                                <script>
                                                                                    CKEDITOR.replace('rationale');
                                                                                </script>
                                                                                <span class="text-danger">{{ $errors->first('rationale') }}</span>
                                                                            </div>
                                                                            <!--./form-group -->
                                                                        </div>
                                                                        <!--./col-lg-12 -->
                                                                    </div>
                                                                    <!--./row -->

                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">
                                                                                <label>General Objective<span style="color: red;">*</span></label>
                                                                                {{ Form::textarea('general_objective', old('general_objective', $call_app->general_objective), ['class="form-control" id="general_objective" rows="5" placeholder="Write general objective..."']) }}
                                                                                <script>
                                                                                    CKEDITOR.replace('general_objective');
                                                                                </script>
                                                                                <span
                                                                                    class="text-danger">{{ $errors->first('general_objective') }}</span>
                                                                            </div>
                                                                            <!--./form-group -->
                                                                        </div>
                                                                        <!--./col-lg-12 -->
                                                                    </div>
                                                                    <!--./row -->

                                                                    <div class="row obj_wrapper">
                                                                        @if (isset($objectives) && count($objectives))
                                                                            @foreach ($objectives as $val)
                                                                                <div class="col-lg-12 col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label>Specific Objective <span style="color: red;">*</span></label>
                                                                                        {{ Form::text('specific_objective[]', old('specific_objective[]', $val->objective), ['class="form-control" placeholder="Write specific objective..." autocomplete="off"']) }}
                                                                                        <span
                                                                                            class="text-danger">{{ $errors->first('specific_objective') }}</span>
                                                                                    </div>
                                                                                    <!--./form-group -->
                                                                                </div>
                                                                                <!--./col-lg-12 -->
                                                                            @endforeach
                                                                        @else
                                                                            <div class="col-lg-12 col-md-12">
                                                                                <div class="form-group">
                                                                                    <label>Specific Objective <span style="color: red;">*</span></label>
                                                                                    {{ Form::text('specific_objective[]', old('specific_objective[]'), ['class="form-control" placeholder="Write specific objective..." autocomplete="off"']) }}
                                                                                    <span
                                                                                        class="text-danger">{{ $errors->first('specific_objective') }}</span>
                                                                                </div>
                                                                                <!--./form-group -->
                                                                            </div>
                                                                            <!--./col-lg-12 -->
                                                                        @endif
                                                                    </div>
                                                                    <!--./row -->

                                                                    <div class="row">
                                                                        <div class="col-lg-12 col-md-12">
                                                                            <div class="pull-right">
                                                                                <a href="javascript:void(0);" class="add_btn btn btn-info btn-sm"
                                                                                    title="Add field">Add More</a>
                                                                            </div>
                                                                            <!--./pull-right -->
                                                                        </div>
                                                                        <!--./col-lg-12 -->
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">
                                                                                <label>Summary Literature Review<span style="color: red;">*</span></label>
                                                                                {{ Form::textarea('literature_review', old('literature_review', $call_app->literature_review), ['class="form-control" id="literature_review" rows="5" placeholder="Write literature review..."']) }}
                                                                                <script>
                                                                                    CKEDITOR.replace('literature_review');
                                                                                </script>
                                                                                <span
                                                                                    class="text-danger">{{ $errors->first('literature_review') }}</span>
                                                                            </div>
                                                                            <!--./form-group -->
                                                                        </div>
                                                                        <!--./col-lg-12 -->
                                                                    </div>
                                                                    <!--./row -->

                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">
                                                                                <label>Methodology<span style="color: red;">*</span></label>
                                                                                {{ Form::textarea('methodology', old('methodology', $call_app->methodology), ['class="form-control" id="methodology" rows="5" placeholder="Write methodology..."']) }}
                                                                                <script>
                                                                                    CKEDITOR.replace('methodology');
                                                                                </script>
                                                                                <span class="text-danger">{{ $errors->first('methodology') }}</span>
                                                                            </div>
                                                                            <!--./form-group -->
                                                                        </div>
                                                                        <!--./col-lg-12 -->
                                                                    </div>
                                                                    <!--./row -->

                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">
                                                                                <label>Expected Outputs<span style="color: red;">*</span></label>
                                                                                {{ Form::textarea('research_output', old('research_output', $call_app->research_output), ['class="form-control" id="research_output" rows="5" placeholder="Write research output..."']) }}
                                                                                <script>
                                                                                    CKEDITOR.replace('research_output');
                                                                                </script>
                                                                                <span class="text-danger">{{ $errors->first('research_output') }}</span>
                                                                            </div>
                                                                            <!--./form-group -->
                                                                        </div>
                                                                        <!--./col-lg-12 -->
                                                                    </div>
                                                                    <!--./row -->

                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">
                                                                                <label>Sustainability Measures<span style="color: red;">*</span></label>
                                                                                {{ Form::textarea('sustainability_measures', old('sustainability_measures', $call_app->sustainability_measures), ['class="form-control" id="sustainability_measures" rows="5" placeholder="Write sustainability measures..."']) }}
                                                                                <script>
                                                                                    CKEDITOR.replace('sustainability_measures');
                                                                                </script>
                                                                                <span
                                                                                    class="text-danger">{{ $errors->first('sustainability_measures') }}</span>
                                                                            </div>
                                                                            <!--./form-group -->
                                                                        </div>
                                                                        <!--./col-lg-12 -->
                                                                    </div>
                                                                    <!--./row -->

                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">
                                                                                <label>Project Management Structure<span
                                                                                        style="color: red;">*</span></label>
                                                                                {{ Form::textarea('management_structure', old('management_structure', $call_app->management_structure), ['class="form-control" id="management_structure" rows="5" placeholder="Write management structure..."']) }}
                                                                                <script>
                                                                                    CKEDITOR.replace('management_structure');
                                                                                </script>
                                                                                <span
                                                                                    class="text-danger">{{ $errors->first('management_structure') }}</span>
                                                                            </div>
                                                                            <!--./form-group -->
                                                                        </div>
                                                                        <!--./col-lg-12 -->
                                                                    </div>
                                                                    <!--./row -->

                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">
                                                                                <label>Project Implementation Plan<span style="color: red;">*</span></label>
                                                                                {{ Form::textarea('implementation_plan', old('implementation_plan', $call_app->implementation_plan), ['class="form-control" id="implementation_plan" rows="5" placeholder="Write implementation plan..."']) }}
                                                                                <script>
                                                                                    CKEDITOR.replace('implementation_plan');
                                                                                </script>
                                                                                <span
                                                                                    class="text-danger">{{ $errors->first('implementation_plan') }}</span>
                                                                            </div>
                                                                            <!--./form-group -->
                                                                        </div>
                                                                        <!--./col-lg-12 -->
                                                                    </div>
                                                                    <!--./row -->
                                                                    
                                                                    {{-- <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">
                                                                                <label>Dissemination/Commercialization/up scaling plan<span style="color: red;">*</span></label>
                                                                                {{ Form::textarea('dissemination_plan', old('dissemination_plan', $call_app->dissemination_plan), ['class="form-control" id="dissemination_plan" rows="5" placeholder="Write dissemination/commercialization/up scaling plan..."']) }}
                                                                                <script>
                                                                                    CKEDITOR.replace('dissemination_plan');
                                                                                </script>
                                                                                <span
                                                                                    class="text-danger">{{ $errors->first('dissemination_plan') }}</span>
                                                                            </div>
                                                                            <!--./form-group -->
                                                                        </div>
                                                                        <!--./col-lg-12 -->
                                                                    </div> --}}
                                                                    <!--./row -->

                                                                    <div class="mt-2"></div>
                                                                    <div class="row">
                                                                        <div class="col-lg-12 col-md-12">
                                                                            <div class="form-group float-right">
                                                                                <a href="{{ route('applications.step-1', [$call->id, $call_app->id]) }}"
                                                                                    class="btn btn-info font-weight-600"><i class="fa fa-chevron-left"></i>
                                                                                    Previous</a>

                                                                                <a href="{{ route('applications.cancel', [$call->id, $call_app->id]) }}"
                                                                                    class="btn btn-danger font-weight-600">
                                                                                    Cancel</a>

                                                                                <button type="submit" name="save"
                                                                                    class="btn btn-secondary font-weight-600">Save and
                                                                                    Exit</button>

                                                                                <button type="submit" name="continue"
                                                                                    class="btn btn-success font-weight-600">Save and
                                                                                    Continue <i class="fa fa-chevron-right"></i></button>
                                                                            </div>
                                                                            <!--./form-group -->
                                                                        </div>
                                                                        <!--./col-md -->
                                                                    </div>
                                                                    <!--./row -->
                                                                    {{ Form::close() }}
                                                                </div><!-- ./form -->
                                                            </div>
                                                            <!--./card-body -->
                                                        </div>
                                                        <!--./card -->
                                                    </div>
                                                    <!--./col-lg-12 -->
                                                </div>
                                                <!--./row -->
                                            </div>
                                            <!--./container -->
                                        </div>
                                        <!--./page-content -->
                                    </section>
                                @endsection
