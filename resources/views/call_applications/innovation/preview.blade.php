@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Call Application</h4>
                            <ul>
                                <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('calls') }}" title=""> Research Calls</a></li>
                                <li>Call Application</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <!-- sidebar -->
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat pure-form">
                            <div class="card-body">
                                <h5 class="text-uppercase">{{ $call->title }}</h5>
                                <hr />

                                <div class="stepwizard">
                                    <div class="stepwizard-row setup-panel">
                                        <div class="stepwizard-step col-xs-3">
                                            <a href="#step-1" type="button"
                                                class="btn btn-success btn-circle text-white font-weight-bold">1</a>
                                        </div>
                                        <div class="stepwizard-step col-xs-3">
                                            <a href="#step-2" type="button"
                                                class="btn btn-success btn-circle text-white font-weight-bold"
                                                disabled="disabled">2</a>
                                        </div>
                                        <div class="stepwizard-step col-xs-3">
                                            <a href="#step-3" type="button"
                                                class="btn btn-success btn-circle text-white font-weight-bold"
                                                disabled="disabled">3</a>
                                        </div>
                                        <div class="stepwizard-step col-xs-3">
                                            <a href="#step-4" type="button"
                                                class="btn btn-info btn-circle text-white font-weight-bold"
                                                disabled="disabled">4</a>
                                        </div>
                                    </div>
                                </div>
                                <!--./stepwizard -->

                                <div class="form mt-4">
                                    @if (session()->get('success'))
                                        <div class="alert alert-success">
                                            {{ session()->get('success') }}
                                        </div><br />
                                    @elseif(session()->get('danger'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('danger') }}
                                        </div><br />
                                    @endif

                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5>Step 4/4: Preview and Submit</h5>
                                        </div>
                                        <!--./col-md-12 -->
                                    </div>
                                    <!--./row -->

                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th width="25%"><span class="font-weight-bold">Project Title</span></th>
                                                    <td colspan="3">
                                                        <span
                                                            class="text-uppercase font-weight-bold">{{ $call_app->project_title }}</span>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">Principal Innovator</span></th>
                                                    <td colspan="3">
                                                        {{ $call_app->pi->first_name . ' ' . $call_app->pi->middle_name . ' ' . $call_app->pi->surname }}
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">Internal Partners (UDSM
                                                            Staff)</span>
                                                    </th>
                                                    <td colspan="3">
                                                        @php $i = 1; @endphp
                                                        @foreach ($partners as $v)
                                                            {{ $i . '. ' . $v->partner->first_name . ' ' . $v->partner->middle_name . ' ' . $v->partner->surname }}
                                                            <br />
                                                            @php $i++; @endphp
                                                        @endforeach
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">Project Partner (UDSM
                                                            Students)</span>
                                                    </th>
                                                    <td colspan="3">
                                                        @php $i = 1; @endphp
                                                        @foreach ($students as $v)
                                                            {{ $i . '. ' . $v->partner->first_name . ' ' . $v->partner->middle_name . ' ' . $v->partner->surname }}
                                                            <br />
                                                            @php $i++; @endphp
                                                        @endforeach
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">External Partners (Non-UDSM
                                                            Staff)</span>
                                                    </th>
                                                    <td colspan="3">
                                                        @php $i = 1; @endphp
                                                        @foreach ($external_partners as $v)
                                                            {!! $i . '. ' . $v->partner->full_name !!}
                                                            <br />
                                                            @php $i++; @endphp
                                                        @endforeach
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">Call Category</span></th>
                                                    <td>
                                                        @if ($call_app->callCategory)
                                                            {{ $call_app->callCategory->title }}
                                                        @endif
                                                    </td>

                                                    <th><span class="font-weight-bold"> Type</span></th>
                                                    <td>
                                                        @if ($call_app->researchType)
                                                            {{ $call_app->researchType->name }}
                                                        @endif
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">Call Cluster</span></th>
                                                    <td>
                                                        @if ($call_app->cluster)
                                                            {{ $call_app->cluster->name }}
                                                        @endif
                                                    </td>

                                                    <th><span class="font-weight-bold">Innovation Area</span></th>
                                                    <td>
                                                        @if ($call_app->researchArea)
                                                            {{ $call_app->researchArea->name }}
                                                        @endif
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">Project Description and Purpose </span></th>
                                                    <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->introduction) !!}</td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">Intellectual Property Issues</span></th>
                                                    <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->rationale) !!}</td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">Project Plan and Budget</span></th>
                                                    <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->general_objective) !!}</td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">Planned Activities</span></th>
                                                    <td colspan="3">
                                                        @php $j = 1; @endphp
                                                        @foreach ($call_app->objectives as $v)
                                                            {{ $j . '. ' . $v->objective }}<br />
                                                            @php $j++; @endphp
                                                        @endforeach
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">Product/Process/Service</span></th>
                                                    <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->research_output) !!}</td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">Project Risks and Mitigation Plan</span></th>
                                                    <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->sustainability_measures) !!}</td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">Potential National Benefit of the Outcome of the Project</span></th>
                                                    <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->potential_benefit) !!}</td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">Source Fund</span></th>
                                                    <td colspan="3">{!! preg_replace('/<\/?div[^>]*\>/i', '', $call_app->source_fund) !!}</td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">Proposal Attachment</span></th>
                                                    <td >
                                                        @if (file_exists(public_path() . '/assets/uploads/documents/' . $call_app->attachment))
                                                            <a class="btn btn-primary btn-sm"
                                                                href="{{ asset('assets/uploads/documents/' . $call_app->attachment) }}"
                                                                download><i class="fa fa-download"></i> Proposal Download</a>
                                                        @endif
                                                    </td>
                                                    <th><span class="font-weight-bold">CV Attachment</span></th>
                                                    <td >
                                                        @if (file_exists(public_path() . '/assets/uploads/documents/' . $call_app->cv_attachment))
                                                            <a class="btn btn-primary btn-sm"
                                                                href="{{ asset('assets/uploads/documents/' . $call_app->cv_attachment) }}"
                                                                download><i class="fa fa-download"></i> CV Download</a>
                                                        @endif
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th><span class="font-weight-bold">Budget Summary</span></th>
                                                    <td colspan="3">
                                                        <h5>
                                                            @if ($call_app->callCategory)
                                                                Budget Ceiling :
                                                                {{ $call_app->callCategory->currency . ' ' . number_format($call_app->callCategory->budget_ceiling) }}
                                                            @endif

                                                            <table width="100%" class="table table-bordered">
                                                                <thead>
                                                                    <tr>
                                                                        <th width="75%">Budget Line</th>
                                                                        <th>Amount (TZS)</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @php $i = 1; @endphp
                                                                    @php $sum = 0; @endphp
                                                                    @foreach ($call_app->objectives as $value)
                                                                        <tr>
                                                                            <td>{{ $i . '. ' . $value->objective }}
                                                                            </td>
                                                                            <td>{{ number_format($value->budget) }}</td>
                                                                        </tr>
                                                                        @php $sum += $value->budget; @endphp
                                                                        @php $i++; @endphp
                                                                    @endforeach
                                                                    <tr>
                                                                        <td><b>Total budget</b></td>
                                                                        <td><span
                                                                                id="sum">{{ number_format($sum) }}</span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </h5>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--./col-md-12 -->
                                    </div>
                                    <!--./row -->
                                </div>
                                <!--./form -->

                                <div class="row">
                                    <div class="col-md-12 mt-1">
                                        <div class="form-group float-right">
                                            <a href="{{ route('applications.step-3', [$call->id, $call_app->id]) }}"
                                                class="btn btn-info font-weight-600"><i
                                                    class="fa fa-chevron-left"></i> Previous</a>

                                            <a href="{{ route('applications.cancel', $call_app->id) }}"
                                                class="btn btn-danger font-weight-600">Cancel</a>

                                            <a class="btn btn-success font-weight-600"
                                                href="{{ route('applications.confirm', $call_app->id) }}">Confirm</a>
                                        </div>
                                    </div>
                                    <!--./col-md-12 -->
                                </div>
                                <!--./row -->
                            </div>
                            <!--./card-body -->
                        </div>
                        <!--./card -->
                    </div>
                    <!--./row -->
                </div>
                <!--./container -->
            </div>
            <!--./page-content -->
    </section>
@endsection
