@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Assign Evaluators</h4>
                            <ul>
                                <li><a href="#" title="">Dashboard</a></li>
                                <li><a href="{{ url('calls') }}" title=""> Research Grants Applications</a></li>
                                <li>Assign Evaluators</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat">
                            <h5 class="card-header">Assign Evaluators</h5>
                            <!--./card-header -->
                            <div class="card-body">

                                @if (session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div><br />
                                @endif

                                {{ Form::open(['url' => url('call_applications/batch-store-evaluators'), 'method' => 'POST', 'class' => 'form-horizontal']) }}
                                {{ Form::token() }}

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Research Projects</label>
                                            @if (isset($call_applications) && $call_applications)
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th width="5%">#</th>
                                                            <th width="70%">Title</th>
                                                            <th width="22%">PI</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @php $serial = 1; @endphp
                                                        @foreach ($call_applications as $val)
                                                            <tr>
                                                                <td>{{ $serial }}</td>
                                                                <td>{{ $val->project_title }}</td>
                                                                <td>{!! $val->pi->first_name . ' ' . $val->pi->surname !!}</td>
                                                            </tr>
                                                            @php $serial++; @endphp
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            @else
                                                <div class="alert alert-warning">No any call application</div>
                                            @endif
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->


                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Call Evaluators</label>
                                            @php($_option = [])
                                                @foreach ($users as $us)
                                                    @php($_option[$us->id] = $us->first_name . ' ' . $us->middle_name . ' ' .
                                                        $us->surname)
                                                    @endforeach
                                                    {{ Form::select('evaluator_ids[]', $_option, old('evaluator_ids[]'), ['data-placeholder="   -- Select --" class="form-control chosen-select" multiple required']) }}
                                                    <span class="text-danger">{{ $errors->first('evaluator_ids') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-12 -->
                                        </div>
                                        <!--./row -->

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <button type="submit" name="assign-batch"
                                                        class="btn btn-secondary btn-xs text-medium">Assign</button>
                                                    <a href="{{ url('') }}"
                                                        class="btn btn-danger btn-xs text-medium">Cancel</a>
                                                </div>
                                            </div>
                                            <!--./col-lg-12 -->
                                        </div>
                                        <!--./row -->
                                        {{ Form::close() }}
                                    </div>
                                    <!--./card-body -->
                                </div>
                                <!--./card -->
                            </div>
                            <!--./col-lg-12 -->
                        </div>
                        <!--./row -->
                    </div>
                    <!--./container -->
                </div>
                <!--./page-content -->
            </section>
        @endsection
