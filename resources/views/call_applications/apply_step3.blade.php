@extends('layouts.admin_app')
@section('content')

    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h4>Call Application</h4>
                            <ul>
                                <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('calls') }}" title=""> Calls</a></li>
                                <li>Call Application</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <!-- sidebar -->
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat pure-form">
                            <!--./card-header -->
                            <div class="card-body">
                                <h5 class="text-uppercase">{{ $call->title }}</h5>
                                <hr />

                                <div class="stepwizard">
                                    <div class="stepwizard-row setup-panel">
                                        <div class="stepwizard-step col-xs-3">
                                            <a href="#step-1" type="button"
                                                class="btn btn-success btn-circle text-white font-weight-bold">1</a>
                                        </div>
                                        <div class="stepwizard-step col-xs-3">
                                            <a href="#step-2" type="button"
                                                class="btn btn-success btn-circle text-white font-weight-bold"
                                                disabled="disabled">2</a>
                                        </div>
                                        <div class="stepwizard-step col-xs-3">
                                            <a href="#step-3" type="button"
                                                class="btn btn-info btn-circle text-white font-weight-bold"
                                                disabled="disabled">3</a>
                                        </div>
                                        <div class="stepwizard-step col-xs-3">
                                            <a href="#step-4" type="button"
                                                class="btn btn-danger btn-circle text-white font-weight-bold"
                                                disabled="disabled">4</a>
                                        </div>
                                    </div>
                                </div>
                                <!--./stepwizard -->

                                <div class="form mt-4">
                                    @if (session()->get('success'))
                                        <div class="alert alert-success">
                                            {{ session()->get('success') }}
                                        </div>
                                    @elseif (session()->get('info'))
                                        <div class="alert alert-info">
                                            {{ session()->get('info') }}
                                        </div>
                                    @elseif (session()->get('danger'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('danger') }}
                                        </div>
                                    @endif

                                    {{ Form::open(['url' => route('applications.save-step-3', [$call->id, $call_app->id]), 'method="POST" enctype="multipart/form-data"']) }}
                                    @method('PUT')
                                    {{ Form::token() }}
                                    {{ Form::hidden('user_id', $user->id, ['id' => 'user_id']) }}
                                    {{ Form::hidden('call_id', $call->id, ['id' => 'call_id']) }}

                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5>Step 3/4: Budget Summary - {{ $call_category->title }}
                                                ( Budget Ceiling :
                                                {{ $call_category->currency . ' ' . number_format($call_category->budget_ceiling) }}
                                                )</h5>
                                        </div>
                                        <!--./col-md-12 -->
                                    </div>
                                    <!--./row -->

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table width="100%" class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th width="75%">Specific Objective</th>
                                                        <th>Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $i = 1 @endphp
                                                    @foreach ($call_app->objectives as $val)
                                                        <tr>
                                                            <td>{{ $i . '. ' . $val->objective }}</td>
                                                            <td>
                                                                <div class="form-group">
                                                                    {{ Form::hidden('objective[]', $val->id) }}
                                                                    {{ Form::text('amount[]', old('amount[]', number_format($val->budget)), ['class="amount form-control" placeholder="Write amount..."', 'onkeyup' => 'amountFormat()']) }}
                                                                </div>
                                                                <!--./form-group -->
                                                            </td>
                                                        </tr>
                                                        @php $i++; @endphp
                                                    @endforeach
                                                    <tr>
                                                        <td><b>Project Budget</b></td>
                                                        <td><span id="sum">0</span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!--./col-lg-12 -->
                                    </div>
                                    <!--./row -->

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Sources of funds (other than this grant if any)</label>
                                                {{ Form::textarea('source_fund', old('source_fund', $call_app->source_fund), ['class="form-control" rows="3" placeholder="Write source of funds..."']) }}
                                                <script>
                                                    CKEDITOR.replace('source_fund');

                                                </script>
                                                <span class="text-danger">{{ $errors->first('source_fund') }}</span>
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-lg-12 -->
                                    </div>
                                    <!--./row -->

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Proposal Attachment<span style="color: red;">*</span></label>
                                                {{ Form::file('attachment', ['class="form-control"']) }}
                                                <span class="text-danger">{{ $errors->first('attachment') }}</span>
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-lg-12 -->
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>CV Attachment<span style="color: red;">*</span></label>
                                                {{ Form::file('cv_attachment', ['class="form-control"']) }}
                                                <span class="text-danger">{{ $errors->first('cv_attachment') }}</span>
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                    </div>
                                    <!--./row -->

                                    <div class="mt-2"></div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12">
                                            <div class="form-group float-right">
                                                @if ($call_app->application_grant_category=='innovation_grant')
                                                <a href="{{ route('applications.innov_step-2', [$call->id, $call_app->id]) }}"
                                                    class="btn btn-info btn-sm font-weight-600"><i
                                                        class="fa fa-chevron-left"></i> Previous</a>
                                                @else
                                                <a href="{{ route('applications.step-2', [$call->id, $call_app->id]) }}"
                                                    class="btn btn-info btn-sm font-weight-600"><i
                                                        class="fa fa-chevron-left"></i> Previous</a>
                                                @endif

                                                <a href="{{ route('applications.cancel', [$call->id, $call_app->id]) }}"
                                                    class="btn btn-danger btn-sm font-weight-600">
                                                    Cancel</a>

                                                <button type="submit" name="save"
                                                    class="btn btn-secondary btn-sm font-weight-600">Save and
                                                    Exit</button>

                                                <button type="submit" name="continue"
                                                    class="btn btn-success btn-sm font-weight-600">Save and
                                                    Continue <i class="fa fa-chevron-right"></i></button>
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-md -->
                                    </div>
                                    <!--./row -->
                                    {{ Form::close() }}
                                </div><!-- ./form -->
                            </div>
                            <!--./card-body -->
                        </div>
                        <!--./card -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
    <script type="text/javascript">
        //summation
        $(document).ready(function() {
            //iterate through each textboxes and add keyup
            //handler to trigger sum event
            $(".amount").each(function() {
                $(this).keyup(function() {
                    calculateSum();
                });
            });

        });

        //calculate sum
        function calculateSum() {
            let sum = 0;
            //iterate through each text boxes and add the values
            $(".amount").each(function() {
                // Removing all the commas
                let amount = this.value.replace(/,/g, "");

                //add only if the value is number
                if (!isNaN(amount) && amount.length !== 0) {
                    //sum
                    sum += parseFloat(amount);
                }
            });
            //comma-separated numbers
            sum = sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            $("#sum").html(sum);
        }

    </script>
@endsection
