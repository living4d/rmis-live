@extends('layouts.admin_app')
@section('content')

<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title">
                        <h4>Attach Call Application Proposal</h4>
                        <ul>
                            <li><a href="#" title="">Dashboard</a></li>
                            <li><a href="{{url('calls')}}" title=""> Research Grants</a></li>
                            <li><a href="{{url('calls/'.$call->id)}}" title=""> Grant
                                    Applications</a></li>
                            <li>{{$call->title}}</li>
                        </ul>
                    </div><!-- Page Title -->
                </div>
                <!--./col-lg-10 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    @include('calls.sidebar_menu')
                </div>
                <!--col-md-2 -->
                
                <div class="col-lg-10 col-md-10">
                    <div class="card card-flat">
                        <h5 class="card-header">Attach Call Application Proposal</h5>
                        <!--./card-header -->
                        <div class="card-body">
                            @if(session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                            @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                            @endif

                            {{ Form::open(['url' => url('/call_applications/attach_document/'.$call->id.'/'.$call_app->id), 'method="POST" enctype="multipart/form-data"']) }}
                            @method('PUT')
                            {{ Form::token() }}

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Title of the Research Project</label>
                                        {{Form::textarea('project_title', $call_app->project_title, ['class="form-control" rows="3" readonly=""'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Principal Investigator</label>
                                        {{Form::text('principal_investigator', $investigator->first_name.' '.$investigator->surname, ['class="form-control" readonly=""'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->
                           
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Attachment of the proposal<span style="color: red;">*</span></label>
                                        {{Form::file('attachment', ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('attachment') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <button type="submit" name="attach" class="btn btn-secondary btn-xs text-medium">Attach</button>
                                        <a href="{{url('call_applications/details/'.$call->id.'/'.$call_app->id)}}"
                                            class="btn btn-danger btn-xs text-medium">Cancel</a>
                                    </div>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->
                            {{ Form::close() }}
                        </div>
                        <!--./card-body -->
                    </div>
                    <!--./card -->
                </div>
                <!--./col-lg-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection