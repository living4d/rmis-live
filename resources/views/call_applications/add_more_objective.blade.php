<script type="text/javascript">
    $(document).ready(function () {
        let maxField = 10; //Input fields increment limitation
        let addButton = $('.add_btn'); //Add button selector
        let wrapper = $('.obj_wrapper'); //Input field wrapper
        let fieldHTML = '<div class="col-lg-12 col-md-12">' +
            '<div class="form-group">' +
            '<label>Specific Objective <span style="color: red;">*</span></label>' +
            '<input type="text" name="specific_objective[]" placeholder="Write specific objective..." autocomplete="off" class="form-control">' +
            '<span class="text-danger"><?php echo $errors->first('specific_objective[]') ?></span>' +
            '</div>' +
            '</div>';

        let x = 1; //Initial field counter is 1
        //Once add button is clicked
        $(addButton).click(function () {
            //Check maximum number of input fields
            if (x < maxField) {
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
            }
        });

        //Once remove button is clicked
        $(wrapper).on('click', '.remove_button', function (e) {
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });
    });
</script>