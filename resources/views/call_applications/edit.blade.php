@extends('layouts.admin_app')
@section('content')

    @include('call_applications.add_more_objective')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h4>Edit Application Objectives</h4>
                            <ul>
                                <li><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('calls') }}" title=""> Research Calls</a></li>
                                <li><a href="{{ route('calls.applications', $call->id) }}"> Applications</a></li>
                                <li>Edit application objectives</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat">
                            <h5 class="card-header">Update Call Application Objectives</h5>
                            <!--./card-header -->
                            <div class="card-body">
                                @if (session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div><br />
                                @elseif(session()->get('danger'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('danger') }}
                                    </div><br />
                                @endif

                                {{ Form::open(['url' => route('applications.update'), 'method="POST"']) }}
                                @method('PUT')
                                {{ Form::token() }}
                                {{ Form::hidden('call_id', $call->id, ['id' => 'call_id']) }}
                                {{ Form::hidden('call_app_id', $call_app->id, ['id' => 'call_app_id']) }}

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Title of the Project</label>
                                            {{ Form::textarea('project_title', $call_app->project_title, ['class="form-control" rows="3" readonly=""']) }}
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Principal Investigator</label>
                                            {{ Form::text('principal_investigator', $call_app->pi->first_name . ' ' . $call_app->pi->middle_name . ' ' . $call_app->pi->surname, ['class="form-control" readonly=""']) }}
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->

                                <div class="row obj_wrapper">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                            <label>Specific Objective <span style="color: red;">*</span></label>
                                            {{ Form::text('specific_objective[]', old('specific_objective[]'), ['class="form-control" placeholder="Write specific objective..." autocomplete="off"']) }}
                                            <span class="text-danger">{{ $errors->first('specific_objective') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="pull-right">
                                            <a href="javascript:void(0);" class="add_btn btn btn-info btn-sm"
                                                title="Add field">Add More</a>
                                        </div>
                                        <!--./pull-right -->
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>

                                <div class="row mt-2">
                                    <div class="col-lg-12">
                                        <div class="form-group float-right">
                                            <button type="submit" name="attach"
                                                class="btn btn-secondary btn-sm font-weight-600">Update</button>
                                            <a href="{{ route('applications.details', [$call->id, $call_app->id]) }}"
                                                class="btn btn-danger btn-sm font-weight-600">Cancel</a>
                                        </div>
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->
                                {{ Form::close() }}
                            </div>
                            <!--./card-body -->
                        </div>
                        <!--./card -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
