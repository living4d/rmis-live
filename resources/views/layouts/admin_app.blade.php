@php use App\Perms; @endphp
        <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>RIMS - {{ isset($title) ? $title : 'Research Information Management System' }}</title>

    <!-- favicon -->
    <link rel="icon" type="image/png" href="{{ asset('/assets/img/favicon.png') }}">

    <!-- Font awesome css -->
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Custom styles for datatables -->
    <link href="{{ asset('assets/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">

    <!--chosen select -->
    <link href="{{ asset('assets/plugins/chosen_v1.8.7/chosen.css') }}" rel="stylesheet">

    <!-- custom styles -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/colors.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/forms.css') }}" rel="stylesheet">

    <!-- Core JS files -->
    <script src="https://code.jquery.com/jquery-3.5.0.min.js"
            integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>

    <!-- Chosen JavaScript -->
    <script type="text/javascript" src="{{ asset('assets/plugins/chosen_v1.8.7/chosen.jquery.js') }}"></script>

    <!-- Custom JavaScript -->
    <script src="{{ asset('assets/js/custom.js') }}"></script>

    <!-- add ckeditor -->
    <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
</head>

<body>
<nav class="navbar navbar-expand-md bg-primary fixed-top">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('calls.stats') }}">RIMS</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                @if (Perms::perm_class('calls'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('calls.stats') }}"> Calls for Grants</a>
                    </li>
                @endif

                @if (Perms::perm_class('projects'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('projects.stats') }}">Project Monitoring</a>
                    </li>
                @endif

                @if (Perms::perm_class('publications'))
                    <li class="nav-item">
{{--                        <a class="nav-link" href="{{ url('publications') }}">Publications</a>--}}
                        <a class="nav-link" href="{{ route('publications.stats') }}">Publications</a>
                    </li>
                @endif

                @if (Perms::perm_class('clearance_students') || Perms::perm_class('clearance_staffs'))
                    <li class="nav-item">
{{--                        <a class="nav-link" href="{{ url('clearance') }}">Clearance</a>--}}
                        <a class="nav-link" href="{{ route('clearance.stats') }}">Clearance</a>
                    </li>
                @endif

                @if (Perms::perm_class('notification'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('notification') }}">Notification
                            <span
                                    class="badge badge-danger">{{ \App\Models\Notification::show_notification() }}</span>
                        </a>
                    </li>
                @endif
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                @if (Auth::user()->hasRole('admin'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/colleges') }}"> Settings</a>
                    </li>
                @endif

                @if (Perms::perm_class('users'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('users.lists') }}">Manage Users</a>
                    </li>
                @endif

                @if (Perms::perm_class('colleges'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('colleges') }}">Settings</a>
                    </li>
                @endif

                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user-circle fa-fw"></i> {{ Auth::user()->surname }} <span
                                class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ url('profile') }}">Profile</a>
                        <a class="dropdown-item" href="{{ route('profile.change-password') }}">Change Password</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ url('/logout') }}"><i class="fa fa-sign-out"></i>
                            Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<main role="main" class="mt-4">
    @yield('content')
</main>

<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="copyright">
                    <p>© {{ date('Y') }} UDSM - All Rights Reserved</p>
                </div>
                <!--./copyright -->
            </div>
            <!--./col-lg-12 -->
        </div>
        <!--./row -->
    </div>
    <!--./container -->
</footer>
<!--./footer -->

<!-- Page level plugins -->
<script src="{{ asset('assets/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script type="application/javascript">
    $(document).ready(function () {
        $('.nationality-select').chosen({
            allow_single_deselect: true,
            width: '100%'
        });

        $('.mode-of-transport-select').chosen({
            allow_single_deselect: true,
            width: '100%'
        });

        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({
            allow_single_deselect: true
        });

        //datatable
        $('#dataTable').DataTable();

        //basic datatable
        $('#basic-dt').DataTable({
            "paging": false,
            "ordering": true,
            "info": true,
            "bFilter": true,
            "pageLength": 100,
            language: {
                searchPlaceholder: "Search Records",
                search: ""
            }
        });
        $(document).ready(function () {
            $('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });
        });
        //my table
        $('#myTable').DataTable({
            "paging": false,
            "ordering": true,
            "info": true,
            "bFilter": true,
            "bLengthChange": false,
            "bInfo": false,
            "pageLength": 50,
            language: {
                searchPlaceholder: "Search Records",
                search: ""
            }
        });

        //custom search
        let dTable = $('#dt').DataTable({
            "paging": false,
            "ordering": true,
            "info": false,
            "bFilter": true,
            "pageLength": 100,
            "dom": "lrtip" //to hide default search box but search feature is not disabled
        });

        $('#myCustomSearchBox').keyup(function () {
            dTable.search($(this).val())
                .draw(); // this  is for customized searchbox with datatable search feature.
        });
    });

</script>
</body>

</html>
