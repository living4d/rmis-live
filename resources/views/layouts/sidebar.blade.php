<ul>
    <li class="current dropdown">
        <a href="" class="dropdown-toggle" data-toggle="dropdown">Grants and Proposal
            <span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li><a href="{{url('proposals')}}">Apply for Calls</a></li>
            <li><a title="Register your proposal" href="{{url('')}}">Apply for Proposal Approval</a></li>
        </ul>
    </li>
    <li><a title="Research Projects" href="{{url('projects/brief')}}">Project Management</a></li>
    <li class="dropdown">
        <a href="" class="dropdown-toggle" data-toggle="dropdown">Research Clearance
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{url('clearance/brief')}}">Students</a></li>
            <li><a title="Register your proposal" href="{{url('clearance/brief1')}}">Staffs</a></li>
        </ul>
    </li>
    <li><a href="{{url('publications/brief')}}">Publication Vetting</a></li>
    {{-- <li><a href="#">Research Policies & Guidelines</a></li> --}}
</ul>