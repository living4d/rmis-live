<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>RIMS - {{ isset($title) ? $title : 'Research Information Management System' }}</title>

    <!-- favicon -->
    <link rel="icon" type="image/png" href="{{ asset('/assets/img/favicon.png') }}">

    <!-- Font awesome css -->
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- custom styles -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/colors.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/forms.css') }}" rel="stylesheet">

    <!-- Core JS files -->
    <script src="https://code.jquery.com/jquery-3.5.0.min.js"
        integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
        
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>

    <!-- Custom JavaScript -->
    <script src="{{ asset('assets/js/custom.js') }}"></script>

    <!-- add ckeditor -->
    <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
</head>

<body>
    <!-- Header Start -->
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div id="top-bar">
                        <div id="img">
{{--                            <img src="{{ asset('assets/img/logo_ud.png') }}" />--}}
                            <img src="{{ asset('assets/img/logo.png') }}"  alt="Logo"/>
                        </div>
                        <!--./img -->
                    </div>
                    <!--./top-bar -->
                </div>
                <!--./col-md-8 -->
                <div class="col-md-10 uni-heading">
                    <div id="content">
                        <h1>University of Dar es Salaam</h1>
                        <h3>Research Information Management System (RIMS)</h3>
                    </div>
                    <!--./content -->
                </div>
                <!--./col-md-8 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- Header End -->

    @yield('content')

    <div class="row">
            <div class="col-lg-4">
                            <a class="nav-link" href="{{ route('login') }}" type="button" class="btn btn-primary">go Home</a>
                </div>
            </div>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright">
                        <p>© {{ date('Y') }} UDSM - All Rights Reserved</p>
                    </div>
                    <!--./copyright -->
                </div>
                <!--./col-lg-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </footer>
    <!--./footer -->
    <!-- Footer -->
</body>
</html>
