<!--./tabs -->
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link @if(Request::segment(3) == '') active @endif" href="{{ route('calls.show', $call->id) }}">Information</a>
    </li>

    @if (\App\Perms::perm_methods('calls', 'invitations'))
        <li class="nav-item">
            <a class="nav-link @if(Request::segment(3) == 'invitations') active @endif" href="{{ route('calls.invitations', $call->id) }}">Invitation</a>
        </li>
    @endif

    @if (\App\Perms::perm_methods('calls', 'evaluators'))
        <li class="nav-item">
            <a class="nav-link @if(Request::segment(3) == 'evaluators') active @endif" href="{{ route('calls.evaluators', $call->id) }}">Evaluators</a>
        </li>
    @endif

    @if (\App\Perms::perm_methods('calls', 'applications'))
        <li class="nav-item">
            <a class="nav-link @if(Request::segment(3) == 'applications') active @endif" href="{{ route('calls.applications', $call->id) }}">Applications</a>
        </li>
    @endif

    @if (\App\Perms::perm_methods('calls', 'winners'))
    <li class="nav-item">
        <a class="nav-link @if(Request::segment(3) == 'winners') active @endif" href="{{ route('calls.winners', $call->id) }}">Winners</a>
    </li>
    @endif
</ul>
<!--./tab-lists -->