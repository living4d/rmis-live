@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Research Call Evaluators</h4>
                            <ul>
                                <li><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('calls') }}"> Calls</a></li>
                                <li>Call Evaluators</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <!--./tabs -->
                        @include('calls.menu-bar')

                        <div class="tab-content">
                            <div class="tab-pane fade show active">
                                <div class="row p-2 mt-3">
                                    <div class="col-md-12">
                                        <h5 class="text-uppercase">{{ $call->title }}</h5>
                                        <hr />

                                        @if (isset($call->callClusters) && $call->callClusters)
                                            <table class="table table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th width="3%">#</th>
                                                        <th width="60%">Clusters</th>
                                                        <th width="30%">Evaluators</th>
                                                        <th style="width: 50px;">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $i = 1; @endphp
                                                    @foreach ($call->callClusters as $val)
                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>{{ $val->cluster->name }}</td>
                                                            <td>
                                                                {{ \App\Models\Calls\CallApplication::get_evaluators($call->id, $val->cluster->id) }}
                                                            </td>
                                                            <td>
                                                                <a href="#assignEvaluatorModal" data-toggle="modal"
                                                                    data-target="#assignEvaluatorModal"
                                                                    data-backdrop="static" data-keyboard="false"
                                                                    title="Assign Evaluator" id="assign-evaluator"
                                                                    data-call-id={{ $call->id }}
                                                                    data-cluster-id={{ $val->cluster->id }}
                                                                    data-cluster-name="{{ $val->cluster->name }}">
                                                                    <i class="fa fa-plus-square text-success"></i>
                                                                </a>&nbsp;

                                                                <a href="#dropEvaluatorModal" data-toggle="modal"
                                                                    data-target="#dropEvaluatorModal" data-backdrop="static"
                                                                    data-keyboard="false" title="Drop Evaluator"
                                                                    id="drop-evaluator" data-call-id={{ $call->id }}
                                                                    data-cluster-id={{ $val->cluster->id }}
                                                                    data-cluster-name="{{ $val->cluster->name }}">
                                                                    <i class="fa fa-window-close text-danger"></i>
                                                                </a>&nbsp;
                                                            </td>
                                                        </tr>
                                                        @php $i++; @endphp
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <div class="alert alert-danger">
                                                No any evaluator at the moment
                                            </div>
                                            <!--alert danger -->
                                        @endif
                                    </div>
                                    <!--./col-md-12 -->
                                </div>
                                <!--./row -->
                            </div>
                            <!--./tab-content -->
                        </div>
                        <!--./tab-content -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>

    <!-- Modal -->
    <div class="modal fade" id="assignEvaluatorModal" tabindex="-1" role="dialog" aria-labelledby="assignEvaluatorModal"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-600 text-uppercase">Assign Evaluators</h5>
                </div>
                <!--./model-header-->

                <div class="modal-body">
                    <span id="errorMsg" class="text-danger"></span>

                    <form action="#" id="form-assign-evaluator">
                        <input type="hidden" id="_token" value="{{ csrf_token() }}">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h5>Cluster</h5>
                                    <span id="cluster-name"></span>
                                </div>
                                <!--./form-group -->
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h5>Call Evaluators</h5><br />
                                    @foreach ($users as $val)
                                        <input type="checkbox" name="evaluator_ids[]" id="evaluator_ids"
                                            value="{{ $val->id }}" />
                                        <label>{{ $val->first_name . ' ' . $val->middle_name . ' ' . $val->surname }}</label>
                                        <br />
                                    @endforeach
                                </div>
                                <!--./form-group -->
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" name="save" class="btn btn-primary btn-sm">Assign</button>
                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                </div>
                                <!--./form-group -->
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->
                    </form>
                </div>
                <!--./modal-body-->
            </div>
        </div>
    </div>
    <!--./modal-->

    <!-- Modal -->
    <div class="modal fade" id="dropEvaluatorModal" tabindex="-1" role="dialog" aria-labelledby="dropEvaluatorModal"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-600 text-uppercase">Drop Evaluators</h5>
                </div>
                <!--./model-header-->

                <div class="modal-body">
                    <span id="errorMsg-2" class="text-danger"></span>

                    <form action="#" id="form-drop-evaluator">
                        <input type="hidden" id="_token" value="{{ csrf_token() }}">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h5>Cluster</h5>
                                    <span id="cluster-name-2"></span>
                                </div>
                                <!--./form-group -->
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h5>Call Evaluators</h5><br />
                                    <div id="evaluators_lists"></div>
                                </div>
                                <!--./form-group -->
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" name="drop" class="btn btn-primary btn-sm">Drop</button>
                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                </div>
                                <!--./form-group -->
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->
                    </form>
                </div>
                <!--./modal-body-->
            </div>
        </div>
    </div>
    <!--./modal-->

@endsection
