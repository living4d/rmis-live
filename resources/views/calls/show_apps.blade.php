<div class="row p-2">
    <div class="col-md-12">

        <div class="row mt-2">
            <div class="col-md-12">
                <div class="search-form">
                    {!! Form::open(['url' => '#', 'method' => 'GET', 'class' => 'form-horizontal']) !!}
                    {{ Form::token() }}

                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-12">
                            <div class="form-group">
                                {{ Form::text('keyword', old('keyword'), ['placeholder="Project Title or PI..."', 'class="form-control"']) }}
                            </div>
                            <!--./form-group -->
                        </div>
                        <!--./col-md-3 -->

                        <div class="col-md-3 col-sm-3 col-12">
                            <div class="form-group">
                                {{ Form::date('start_at', old('start_at'), ['class' => 'form-control', 'placeholder' => 'Start Date']) }}
                            </div>
                            <!--./form-group -->
                        </div>
                        <!--./col-md-3 -->

                        <div class="col-md-3 col-sm-3 col-12">
                            <div class="form-group">
                                {{ Form::date('end_at', old('end_at'), ['class' => 'form-control', 'placeholder' => 'End Date']) }}
                            </div>
                            <!--./form-group -->
                        </div>
                        <!--./col-md-3 -->

                        <div class="col-md-2 col-sm-2 col-12">
                            <div class="form-group">
                                <button style="float:right" type="submit" name="filter"
                                    class="btn btn-secondary btn-sm">
                                    <i class="fa fa-search"></i> Filter
                                </button>
                            </div>
                            <!--./form-group -->
                        </div>
                        <!--./col-md-->
                    </div>
                    <!--./row -->
                    {{ Form::close() }}
                </div>
                <!--./search-form -->
            </div>
            <!--./col-md-12 -->
        </div>
        <!--./row -->

        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <div class="float-right">
                    <a title="Export PDF" class="btn btn-outline-primary btn-xs text-small"
                        href="#">
                        <i class="fa fa-file-pdf-o"></i> PDF</a> 

                    <a title="Export xlsx" class="btn btn-outline-info btn-xs text-small"
                        href="{{ url('#') }}">
                        <i class="fa fa-file-excel-o"></i> XLS</a>
                </div>
                <!--./float-right -->
            </div>
            <!--./col-md-2 -->
        </div>
        <!--./row -->

        @if (isset($call_applications) && $call_applications)
            <table id="dt" class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th width="3%">#</th>
                        <th width="40%">Project Title</th>
                        <th width="20%">PI</th>
                        <th width="15%">Submitted</th>
                        <th width="12%">Approval</th>
                        <th style="width: 70px;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php($serial = 1)
                        @foreach ($call_applications as $app)
                            <tr>
                                <td>{{ $serial }}</td>
                                <td>{{ $app->project_title }}</td>
                                <td>
                                    @if ($app->pi)
                                        {!! $app->pi->first_name . ' ' . $app->pi->middle_name . ' ' . $app->pi->surname !!}
                                    @else
                                        {{ 'No user' }}
                                    @endif
                                </td>
                                <td>{{ date('d-m-Y H:i', strtotime($app->created_at)) }}</td>
                                <td>{{ \App\Models\Calls\CallApplicationApprovalLevel::show_approval_status($call->id, $app->id) }}
                                </td>
                                <td>
                                    @if (\App\Perms::perm_methods('call_applications', 'details'))
                                        <a href="{{ url('call_applications/details/' . $call->id . '/' . $app->id) }}"
                                            title="Application details"><i class="fa fa-folder-open text-dark-blue"></i>
                                        </a>&nbsp;
                                    @endif

                                    @if (\App\Perms::perm_methods('call_applications', 'edit'))
                                        <a href="{{ url('call_applications/edit/' . $call->id . '/' . $app->id) }}"
                                            title="Edit Objectives">
                                            <i class="fa fa-pencil text-primary"></i>
                                        </a>&nbsp;
                                    @endif

                                    @if (\App\Perms::perm_methods('call_applications', 'attach_document'))
                                        <a href="{{ url('call_applications/attach_document/' . $call->id . '/' . $app->id) }}"
                                            title="Attach documents"><i class="fa fa-cloud-upload text-success"></i>
                                        </a>&nbsp;
                                    @endif

                                    @if (\App\Perms::perm_methods('call_applications', 'delete'))
                                        <a href="{{ url('call_applications/delete/' . $call->id . '/' . $app->id) }}"
                                            title="Delete" onClick="return confirm('Delete call application?')"><i
                                                class="fa fa-trash text-danger"></i>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                            @php($serial++)
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
            <!--./col-md-12 -->
        </div>
        <!--./row -->
