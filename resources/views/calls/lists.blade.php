@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Research Calls</h4>
                            <ul>
                                <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li>Calls</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <div class="row">
                            <div class="col-md-9">
                                @if (\App\Perms::perm_methods('calls', 'create'))
                                    <a href="{{ url('calls/create') }}" class="btn btn-primary btn-sm">
                                        <i class="fa fa-plus"></i> Register New</a>
                                @endif
                            </div>
                            <!--./col-md-9 -->

                            <div class="col-md-3 float-right">
                                <input type="text" id="myCustomSearchBox" class="form-control"
                                    placeholder="Search details...">
                            </div>
                            <!--./col-md-4 -->
                        </div>
                        <!--./row -->

                        @if (isset($calls) && count($calls) > 0)
                            <table id="dt" class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th width="4%">#</th>
                                        <th width="48%">Title</th>
                                        <th width="8%">Deadline</th>
                                        <th width="8%">Status</th>
                                        <th width="8%">My Application</th>
                                        @if (\App\Perms::manage_view())
                                            <th width="8%">Staff Applications</th>
                                        @endif
                                        <th style="width: 60px; !important">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php($serial = 1)
                                        @foreach ($calls as $v)
                                            <tr>
                                                <td>{{ $serial }}</td>
                                                <td>
                                                    <a class="font-weight-600" href="{{ url('calls/' . $v->id) }}">
                                                        {{ strtoupper($v->title) }}
                                                    </a>
                                                </td>
                                                <td>{{ date('d-m-Y', strtotime($v->deadline)) }}</td>
                                                <td>
                                                    <div class="text-center">
                                                        @if ($v->status === 'active')
                                                            <span class="badge badge-pill badge-success badge-bg">OPEN</span>
                                                        @elseif($v->status === 'cancelled')
                                                            <span
                                                                class="badge badge-pill badge-danger badge-bg">CANCELLED</span>
                                                        @elseif($v->status === 'closed')
                                                            <span class="badge badge-pill badge-danger badge-bg">CLOSED</span>
                                                        @endif
                                                    </div>
                                                </td>

                                                <td>
                                                    @if ($v->my_applications == 0)
                                                        <span class="badge badge-pill badge-danger badge-bg">NOT APPLIED</span>
                                                    @else
                                                        <span class="badge badge-pill badge-success badge-bg">APPLIED</span>
                                                    @endif
                                                </td>

                                                @if (\App\Perms::manage_view())
                                                    <td>
                                                        <div class="text-center">
                                                            @if (\App\Perms::perm_methods('calls', 'show'))
                                                                <a href="{{ route('calls.applications', $v->id) }}">
                                                                    <span
                                                                        class="btn btn-info btn-sm"><span>{{ $v->applications }}</span></span>
                                                                </a>
                                                            @else
                                                                <span
                                                                    class="btn btn-info btn-sm"><span>{{ $v->applications }}</span></span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                @endif
                                                <td>
                                                    @if (\App\Perms::perm_methods('calls', 'show'))
                                                        <a href="{{ url('calls/' . $v->id) }}" title="Details" class="btn btn-outline-primary btn-xs">
                                                            <i class="fa fa-folder-open text-primary"></i>
                                                        </a>
                                                    @endif

                                                    @if (\App\Perms::perm_methods('calls', 'edit'))
                                                        <a href="{{ route('calls.edit', $v->id) }}" title="Edit" class="btn btn-outline-secondary btn-xs">
                                                            <i class="fa fa-pencil text-dark"></i>
                                                        </a>
                                                    @endif

                                                    @if ($v->status === 'active')
                                                        @if (\App\Perms::perm_methods('calls', 'invite'))
                                                            <a href="{{ route('calls.invite', $v->id) }}" title="Invite" class="btn btn-outline-info btn-xs">
                                                                <i class="fa fa-envelope text-info"></i>
                                                            </a>&nbsp;
                                                        @endif

                                                        @if (\App\Perms::perm_methods('call_applications', 'apply'))
                                                            <a href="{{ route('applications.apply', $v->id) }}"
                                                                title="Apply or Continue application" class="btn btn-outline-success btn-xs">
                                                                <i class="fa fa-arrow-circle-o-right text-success"></i>&nbsp;
                                                            </a>
                                                        @endif
                                                    @endif

                                                    @if (\App\Perms::perm_methods('calls', 'delete'))
                                                        @if ($v->status === 'active' || $v->status === 'cancelled')
                                                            <a href="{{ route('calls.close', [$v->id, 'close']) }}"
                                                                title="Cancel" class="btn btn-outline-danger btn-xs"
                                                                onClick="return confirm('Are you sure you want to close call?')">
                                                                <i class="fa fa-times-circle text-danger"></i>
                                                            </a>&nbsp;
                                                        @endif
                                                    @endif


                                                    @if (\App\Perms::perm_methods('calls', 'delete'))
                                                        <a href="{{ route('calls.delete', $v->id) }}" title="Delete" class="btn btn-outline-danger btn-xs"
                                                            onClick="return confirm('Are you sure you want to delete call?')">
                                                            <i class="fa fa-trash text-danger"></i>
                                                        </a>&nbsp;
                                                    @endif
                                                </td>
                                            </tr>
                                            @php($serial++)
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="alert alert-danger"> No any call found.</div>
                                @endif
                            </div>
                            <!--./col-lg-10 -->
                        </div>
                        <!--./row -->
                    </div>
                    <!--./container -->
                </div>
                <!--./page-content -->
            </section>
        @endsection
