@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Research Call Information</h4>
                            <ul>
                                <li><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('calls') }}"> Calls</a></li>
                                <li>Call information</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <!--./tabs -->
                        @include('calls.menu-bar')

                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="information" role="tabpanel"
                                aria-labelledby="information-tab">
                                <div class="row p-2">
                                    <div class="col-md-10">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="25%"><span class="font-weight-bold">Call Title</span></th>
                                                <td colspan="3">
                                                    <span
                                                        class="text-uppercase font-weight-bold">{{ $call->title }}</span>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th><span class="font-weight-bold">Introduction </span></th>
                                                <td colspan="3">
                                                    {!! $call->introduction !!}
                                                </td>
                                            </tr>

                                            <tr>
                                                <th><span class="font-weight-bold">Categories of the calls </span></th>
                                                <td colspan="3">
                                                    @foreach ($call->callCategories as $category)
                                                        <strong>{{ $category->title }}</strong>
                                                        <p>{!! $category->description !!}</p>
                                                    @endforeach
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>
                                                    <span class="font-weight-bold">Research Areas </span>
                                                </th>
                                                <td>
                                                    @php($serial = 1)
                                                        @foreach ($call->callClusters as $call_cluster)
                                                            <h6 class="mt-2">
                                                                {{ 'Cluster ' . $serial . ': ' . $call_cluster->cluster->name }}
                                                            </h6>
                                                            <strong>Research Areas: </strong>
                                                            @foreach ($call_cluster->cluster->researchAreas as $research_area)
                                                                {!! '<i>' . $research_area->name . '</i>;' !!}
                                                            @endforeach
                                                            @php($serial++)
                                                            @endforeach
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>
                                                            <span class="font-weight-bold">Team Composition </span>
                                                        </th>
                                                        <td>
                                                            {{ strip_tags($call->team_composition, '') }}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>
                                                            <span class="font-weight-bold">Eligibility </span>
                                                        </th>
                                                        <td>
                                                            {{ strip_tags($call->eligibility, '') }}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>
                                                            <span class="font-weight-bold">Budget Ceiling </span>
                                                        </th>
                                                        <td>
                                                            <p>The budget ceiling for each proposal should not exceed the amounts
                                                                stated below:
                                                            </p>

                                                            @foreach ($call->callCategories as $category)
                                                                <p>{{ $category->title }}
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $category->currency . ' ' . number_format($category->budget_ceiling) }}
                                                                </p>
                                                            @endforeach
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>
                                                            <span class="font-weight-bold">Project Duration </span>
                                                        </th>
                                                        <td>
                                                            {!! $call->duration !!}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>
                                                            <span class="font-weight-bold">Application Details </span>
                                                        </th>
                                                        <td>
                                                            {!! $call->application_details !!}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>
                                                            <span class="font-weight-bold">Deliverable </span>
                                                        </th>
                                                        <td>
                                                            {!! $call->deliverable !!}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>
                                                            <span class="font-weight-bold">Evaluation Criteria </span>
                                                        </th>
                                                        <td>
                                                            {!! $call->evaluation_criteria !!}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>
                                                            <span class="font-weight-bold">Program Coordinator </span>
                                                        </th>
                                                        <td>
                                                            @if ($call->programCoordinator)
                                                                {{ $call->programCoordinator->first_name . ' ' . $call->programCoordinator->surname }}
                                                            @else
                                                                <span class="red">No any program coordinator</span>
                                                            @endif
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>
                                                            <span class="font-weight-bold">Program Accountant </span>
                                                        </th>
                                                        <td>
                                                            @if ($call->programAccountant)
                                                                {{ $call->programAccountant->first_name . ' ' . $call->programAccountant->surname }}
                                                            @else
                                                                <span class="red">No any program accountant</span>
                                                            @endif
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>
                                                            <span class="font-weight-bold">Application Deadline</span>
                                                        </th>
                                                        <td>
                                                            {{ date('l, jS F, Y', strtotime($call->deadline)) }} at
                                                            {{ date('H:i', strtotime($call->deadline)) }}
                                                        </td>
                                                    </tr>

                                                </table>
                                            </div>
                                            <!--./col-md-10 -->

                                            <div class="col-md-2">
                                                <div class="row">
                                                    @if (\App\Perms::perm_methods('calls', 'edit'))
                                                        <div class="col-md-12 mt-3">
                                                            <a href="{{ route('calls.edit', $call->id) }}"
                                                                class="btn btn-primary btn-block">
                                                                <i class="fa fa-pencil"></i> Edit</a>
                                                        </div>
                                                        <!--./col-md-12 -->
                                                    @endif

                                                    @if (\App\Perms::perm_methods('calls', 'delete'))
                                                        <div class="col-md-12 mt-3">
                                                            <a class="btn btn-danger btn-block"
                                                                href="{{ route('calls.delete', $call->id) }}"
                                                                onClick="return confirm('Delete call?')"><i class="fa fa-trash"></i>
                                                                Delete</a>
                                                        </div>
                                                        <!--./col-md-12 -->
                                                    @endif

                                                    @if ($call->status === 'active')
                                                        @if (\App\Perms::perm_methods('calls', 'invite'))
                                                            <div class="col-md-12 mt-3">
                                                                <a class="btn btn-dark btn-block"
                                                                    href="{{ route('calls.invite', $call->id) }}"><i
                                                                        class="fa fa-envelope-o"></i> Invite</a>
                                                            </div>
                                                            <!--./col-md-12 -->
                                                        @endif

                                                        @if (\App\Perms::perm_methods('call_applications', 'apply'))
                                                            <div class="col-md-12 mt-3">
                                                                <a class="btn btn-success btn-block"
                                                                    href="{{ route('applications.apply', $call->id) }}"
                                                                    title="Apply">
                                                                    <i class="fa fa-arrow-right"></i> Apply
                                                                </a>
                                                            </div>
                                                            <!--./col-md-12 -->
                                                        @endif
                                                    @endif
                                                </div>
                                                <!--./row -->
                                            </div>
                                            <!--./col-md-2 -->
                                        </div>
                                        <!--./row -->
                                    </div>
                                    <!--./information -->
                                </div>
                                <!--./tab-content -->
                            </div>
                            <!--./col-lg-12 -->
                        </div>
                        <!--./row -->
                    </div>
                    <!--./container -->
                </div>
                <!--./page-content -->
            </section>
        @endsection
