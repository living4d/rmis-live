<div class="row p-2 mt-3">
    <div class="col-md-12">

        <div class="row">
            <div class="col-md-9"></div>
            <!--./col-md-9 -->

            <div class="col-md-3 pull-right">
                <input type="text" id="myCustomSearchBox" class="form-control" placeholder="Search details...">
            </div>
            <!--./col-md-4 -->
        </div>
        <!--./row -->
        @if (isset($call_invitations) && $call_invitations)
            <table id="dt" class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th width="3%">#</th>
                        <th width="75%">Name</th>
                        <th width="8%">College</th>
                        <th width="8%">Department</th>
                        <th width="6%">Iteration</th>
                        <th style="width: 50px;">Action</th>
                    </tr>
                </thead>
                <tbody>

                    @php($i = 1)
                        @foreach ($call_invitations as $val)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>
                                    @if ($val->user)
                                        {{ $val->user->first_name . ' ' . $val->user->middle_name . ' ' . $val->user->surname }}
                                    @endif
                                </td>
                                <td>
                                    @if ($val->college)
                                        {{ $val->college->short_name }}
                                    @endif
                                </td>
                                <td>
                                    @if ($val->department)
                                        {{ $val->department->short_name }}
                                    @endif
                                </td>
                                <td>{{ $val->iteration }}</td>
                                <td></td>
                            </tr>
                            @php($i++)
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="alert alert-danger">
                        No any invitation at the moment
                    </div>
                    <!--alert danger -->
                @endif
            </div>
            <!--./col-md-12 -->
        </div>
        <!--./row -->
