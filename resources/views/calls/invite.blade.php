@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Research Calls</h4>
                            <ul>
                                <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('calls') }}"> Calls</a></li>
                                <li>Invite Staff</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat pure-form">
                            <div class="card-body">
                                <h5 class="text-uppercase">{{ $call->title }}</h5>
                                <hr />

                                @if (session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div><br />
                                @endif

                                {{ Form::open(['url' => route('calls.store-invite', $call->id), 'method' => 'POST', 'class' => 'form-horizontal']) }}
                                {{ Form::token() }}

                                <div class="row">
                                    <div class="col-md-12">
                                        @if (isset($colleges) && count($colleges) > 0)
                                            <table>
                                                <tr>
                                                    @php $serial = 0; @endphp
                                                    @foreach ($colleges as $college)
                                                        @if ($serial % 1 === 0)
                                                </tr>
                                                <tr>
                                        @endif
                                        <td valign="top">
                                            {{ Form::checkbox('college_ids[]', $college->id) }}
                                            <label class="font-weight-bold">{{ $college->name }}</label><br />
                                            @if (isset($college->departments) && count($college->departments) > 0)
                                                @foreach ($college->departments as $department)
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;{{ Form::checkbox('department_ids[]', $department->id) }}
                                                    &nbsp;&nbsp;<label>{{ $department->name }}</label><br />
                                                @endforeach
                                            @endif
                                        </td>
                                        <div class="mt-3"></div>
                                        @php $serial++; @endphp
                                        @endforeach
                                        </tr>
                                        </table>
                                        <span class="text-danger">{{ $errors->first('college_ids') }}</span>
                                        @endif
                                    </div><!-- ./col-sm-12 -->
                                </div><!-- ./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <button type="submit"
                                                class="btn btn-secondary btn-sm font-weight-600">Invite</button>
                                            <a href="{{ route('calls.invitations', $call->id) }}"
                                                class="btn btn-danger btn-sm font-weight-600">Cancel</a>
                                        </div>
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->
                                {{ Form::close() }}
                            </div>
                            <!--./card-body -->
                        </div>
                        <!--./card -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
