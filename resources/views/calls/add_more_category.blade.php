<script type="text/javascript">
    $(document).ready(function () {
        let maxField = 5; //Input fields increment limitation
        let addButton = $('.add_button'); //Add button selector
        let wrapper = $('.field_wrapper'); //Input field wrapper
        let fieldHTML = '<div class="col-lg-8 col-md-8">' +
            '<div class="form-group">' +
            '<label>Call Category <span style="color: red;">*</span></label>' +
            '<input type="text" id="call_category[]" name="call_category[]" placeholder="Write call category...">' +
            '</div>' +
            '</div>' +
            '<div class="col-lg-4">' +
            '<div class="form-group">' +
            '<label>Budget Ceiling <span style="color: red;">*</span></label>' +
            '<input type="text" id="budget_ceiling[]" name="budget_ceiling[]" placeholder="Write budget ceiling..." onkeyup="budgetFormat()">' +
            '</div>' +
            '</div>' +
            '<div class="col-lg-12 col-md-12">' +
            '<div class="form-group">' +
            '<label>Call Category Description <span style="color: red;">*</span></label>' +
            '<textarea id="call_category_desc[]" name="call_category_desc" placeholder="Write call category description..." rows="3"></textarea> ' +
            '</div>' +
            '</div>';

        let x = 1; //Initial field counter is 1
        //Once add button is clicked
        $(addButton).click(function () {
            //Check maximum number of input fields
            if (x < maxField) {
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
            }
        });

        //Once remove button is clicked
        $(wrapper).on('click', '.remove_button', function (e) {
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });
    });
</script>