@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Research Call Invitations</h4>
                            <ul>
                                <li><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('calls') }}"> Calls</a></li>
                                <li>Call Invitations</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <!--./tabs -->
                        @include('calls.menu-bar')

                        <div class="tab-content">
                            <div class="tab-pane fade show active">
                                <div class="row p-2 mt-3">
                                    <div class="col-md-12">
                                        <h5 class="text-uppercase">{{ $call->title }}</h5>
                                        <hr />

                                        <div class="row">
                                            <div class="col-md-9">
                                                @if (\App\Perms::perm_methods('calls', 'invite'))
                                                    <a class="btn btn-primary btn-sm"
                                                        href="{{ route('calls.invite', $call->id) }}"><i
                                                            class="fa fa-plus"></i> Invite Staff</a>
                                                @endif
                                            </div>
                                            <!--./col-md-9 -->

                                            <div class="col-md-3 pull-right">
                                                <input type="text" id="myCustomSearchBox" class="form-control"
                                                    placeholder="Search details...">
                                            </div>
                                            <!--./col-md-4 -->
                                        </div>
                                        <!--./row -->
                                        @if (isset($call->callInvitations) && $call->callInvitations)
                                            <table id="dt" class="table table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th width="3%">#</th>
                                                        <th width="60%">Name</th>
                                                        <th width="12%">College</th>
                                                        <th width="12%">Department</th>
                                                        <th width="10%">Iteration</th>
                                                        <th style="width: 50px;">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @php($i = 1)
                                                        @foreach ($call->callInvitations as $val)
                                                            <tr>
                                                                <td>{{ $i }}</td>
                                                                <td>
                                                                    @if ($val->user)
                                                                        {{ $val->user->first_name . ' ' . $val->user->middle_name . ' ' . $val->user->surname }}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if ($val->college)
                                                                        {{ $val->user->college->short_name }}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if ($val->department)
                                                                        {{ $val->user->department->short_name }}
                                                                    @endif
                                                                </td>
                                                                <td>{{ $val->iteration }}</td>
                                                                <td></td>
                                                            </tr>
                                                            @php($i++)
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @else
                                                    <div class="alert alert-danger">
                                                        No any invitation at the moment
                                                    </div>
                                                    <!--alert danger -->
                                                @endif
                                            </div>
                                            <!--./col-md-12 -->
                                        </div>
                                        <!--./row -->
                                    </div>
                                    <!--./tab-content -->
                                </div>
                                <!--./tab-content -->
                            </div>
                            <!--./col-lg-12 -->
                        </div>
                        <!--./row -->
                    </div>
                    <!--./container -->
                </div>
                <!--./page-content -->
            </section>
        @endsection
