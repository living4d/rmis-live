<div id="left_menu">
    <ul id="nav">

        @if (\App\Perms::perm_methods('calls', 'lists'))
            @php echo '<li><a href="' . route('calls.stats') . '">Summary</a></li>'; @endphp
            @php echo '<li><a href="' . url('calls') . '">Calls</a></li>'; @endphp
        @endif

        @php echo '<li><a href="' . route('applications.lists') . '">My Applications</a></li>';@endphp
    </ul>
</div>
