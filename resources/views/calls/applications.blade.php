@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Research Call Applications</h4>
                            <ul>
                                <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('calls') }}"> Calls</a></li>
                                <li>Call Applications</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('calls.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <div class="messages"></div>

                        <!--./tabs -->
                        @include('calls.menu-bar')

                        <div class="tab-content">
                            <div class="tab-pane fade show active">
                                <div class="row p-2 mt-2">
                                    <div class="col-md-12">
                                        <h5 class="text-uppercase">{{ $call->title }}</h5>
                                        <hr />

                                        <div class="search-form">
                                            {!! Form::open(['url' => route('calls.applications', $call->id), 'method' => 'POST', 'class' => 'form-horizontal']) !!}
                                            {{ Form::token() }}

                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-12">
                                                    <div class="form-group">
                                                        <input type="text" id="myCustomSearchBox" class="form-control"
                                                            placeholder="Search details...">
                                                    </div>
                                                    <!--./form-group -->
                                                </div>
                                                <!--./col-md-3 -->

                                                <div class="col-md-3 col-sm-3 col-12">
                                                    <div class="form-group">
                                                        @php $_option = []; @endphp
                                                        @foreach ($departments as $val)
                                                            @php $_option[$val->id] = $val->name; @endphp
                                                        @endforeach
                                                        @php $_option = ['' => 'Department'] + $_option; @endphp
                                                        {{ Form::select('department_id', $_option, old('department_id', $department_id), ['class="form-control chosen-select"']) }}
                                                    </div>
                                                    <!--./form-group -->
                                                </div>
                                                <!--./col-md-3 -->

                                                <div class="col-md-3 col-sm-3 col-12">
                                                    <div class="form-group">
                                                        @php $_option = []; @endphp
                                                        @foreach ($call_clusters as $val)
                                                            @php $_option[$val->id] = $val->cluster->name; @endphp
                                                        @endforeach
                                                        @php $_option = ['' => 'Cluster'] + $_option; @endphp
                                                        {{ Form::select('cluster_id', $_option, old('cluster_id', $cluster_id), ['class="form-control chosen-select"']) }}
                                                    </div>
                                                    <!--./form-group -->
                                                </div>
                                                <!--./col-md-3 -->

                                                <div class="col-md-2 col-sm-2 col-12">
                                                    <div class="float-left">
                                                        <div class="form-group">
                                                            <button type="submit" name="filter"
                                                                class="btn btn-secondary btn-sm">
                                                                <i class="fa fa-search"></i> Filter
                                                            </button>
                                                        </div>
                                                        <!--./form-group -->
                                                    </div>
                                                    <!--./float-right -->
                                                </div>
                                                <!--./col-md-->
                                            </div>
                                            <!--./row -->
                                            {{ Form::close() }}
                                        </div>
                                        <!--./search-form -->
                                    </div>
                                    <!--./col-md-12 -->
                                </div>
                                <!--./row -->

                                <div class="row mt-1">
                                    <div class="col-md-9">
                                        <div class="font-weight-600 text-large">
                                            {{ number_format($count_apps) }} Applications
                                        </div>
                                    </div>
                                    <!--./col-md-9 -->

                                    <div class="col-md-3 float-right">

                                    </div>
                                    <!--./col-md-2 -->
                                </div>
                                <!--./row -->

                                @if (isset($call_apps) && $call_apps)
                                    <table id="dt" class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="3%">#</th>
                                                <th width="24%">Project Title</th>
                                                @if (!Auth::user()->hasRole('evaluators'))
                                                    <th width="10%">PI</th>
                                                @endif
                                                <th width="8%">Budget</th>
                                                <th width="8%">Submitted</th>
                                                <th width="8%">Approval</th>
                                                <th width="8%">Evaluation Status</th>
                                                @if (\App\Perms::perm_methods('calls', 'evaluators'))
                                                    <th width="12%">Evaluators</th>
                                                @endif
                                                <th width="6%">Evaluation Grade</th>
                                                <th style="width: 60px !important;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $serial = 1; @endphp
                                            @foreach ($call_apps as $app)
                                                <tr>
                                                    <td>{{ $serial }}</td>
                                                    <td> {{ $app->project_title }}</td>
                                                    @if (!Auth::user()->hasRole('evaluators'))
                                                        <td>{!! $app->pi->first_name . ' ' . substr($app->pi->middle_name, 0, 1) . ' ' . $app->pi->surname !!}</td>
                                                    @endif
                                                    <td>{{ number_format($app->app_budgets) }}</td>
                                                    <td>{{ date('d-m-Y H:i', strtotime($app->created_at)) }}</td>
                                                    <td>{{ show_application_approval_status($app->call_app_id) }}
                                                    </td>
                                                    <td>
                                                        <div class="text-center">
                                                            @if ($app->eval_status == 0)
                                                                <span class="badge badge-pill badge-danger">NOT
                                                                    STARTED</span>
                                                            @elseif($app->eval_status == 1)
                                                                <span class="badge badge-pill badge-warning">ON
                                                                    PROGRESS</span>
                                                            @elseif($app->eval_status == 2)
                                                                <span class="badge badge-pill badge-success">COMPLETE</span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    @if (\App\Perms::perm_methods('calls', 'evaluators'))
                                                        <td>
                                                            @if ($app->evaluators)
                                                                @php $i = 1; @endphp
                                                                @foreach ($app->evaluators as $val)
                                                                    @if ($val->status == 'COMPLETE')
                                                                        <i class="fa fa-check"></i>
                                                                    @else
                                                                        <i class="fa fa-times"></i>
                                                                    @endif
                                                                    {{ $val->user->first_name . ' ' . $val->user->surname }}<br />
                                                                    @php $i++; @endphp
                                                                @endforeach
                                                            @else
                                                                No any evaluator
                                                            @endif
                                                        </td>
                                                    @endif
                                                    <td>
                                                        <div class="text-center">
                                                            {!! $app->call_evaluation_result ? round($app->call_evaluation_result->avg_points, 2) : '0' !!}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="text-right">
                                                            @if (\App\Perms::perm_methods('call_applications', 'details'))
                                                                <a href="{{ route('applications.details', $app->call_app_id) }}"
                                                                    class="btn btn-outline-primary btn-xs"
                                                                    title="Show details"><i
                                                                        class="fa fa-folder-open text-primary"></i>
                                                                </a>
                                                            @endif

                                                            @if (\App\Perms::perm_methods('call_applications', 'approve'))
                                                                {{ show_application_approval_operation($app->call_app_id) }}
                                                            @endif

                                                            @if ($app->winner != 1 && $app->approval_status == 1)
                                                                @if (\App\Perms::perm_methods('call_evaluations', 'records'))
                                                                    @if (($app->eval_status == 0 || $app->eval_status == 1) && \App\Models\Calls\CallApplicationEvaluator::is_assigned_app($app->call_app_id))
                                                                        <a href="{{ route('evaluations.records', [$app->call_id, $app->call_app_id]) }}"
                                                                            title="Evaluate"
                                                                            class="btn btn-outline-success btn-xs">
                                                                            <i class="fa fa-evaluation"></i>
                                                                        </a>
                                                                    @endif
                                                                @endif
                                                            @endif

                                                            @if ($app->winner != 1 && $app->approval_status == 0)
                                                                @if (\App\Perms::perm_methods('call_applications', 'delete'))
                                                                    <a href="{{ route('applications.delete', $app->call_app_id) }}"
                                                                        class="btn btn-outline-danger btn-xs" title="Delete"
                                                                        class="delete"><i
                                                                            class="fa fa-trash text-danger"></i>
                                                                    </a>
                                                                @endif
                                                            @endif

                                                            @if ($app->approval_status == 1 && $app->eval_status == 2 && $app->winner == 0)
                                                                @if (\App\Perms::perm_methods('call_applications', 'award'))
                                                                    <a href="#awardModal" title="Award Application"
                                                                        data-backdrop="static" data-keyboard="false"
                                                                        data-toggle="modal" data-target="#awardModal"
                                                                        data-app-id="{{ $app->call_app_id }}"
                                                                        id="btnAwardModal"
                                                                        class="btn btn-outline-success btn-xs">
                                                                        <i class="fa fa-check-square-o text-success"></i>
                                                                    </a>&nbsp;
                                                                @endif
                                                            @endif

                                                            @if ($app->approval_status == 1 && $app->eval_status == 2 && $app->winner == 1)
                                                                @if (\App\Perms::perm_methods('call_applications', 'decline'))
                                                                    <a href="#awardModal" data-backdrop="static"
                                                                        data-keyboard="false" data-toggle="modal"
                                                                        data-target="#awardModal"
                                                                        data-app-id="{{ $app->call_app_id }}"
                                                                        id="btnDeclineModal"
                                                                        class="btn btn-outline-danger btn-xs">
                                                                        <i class="fa fa-window-close text-danger"></i>
                                                                    </a>
                                                                @endif
                                                            @endif

                                                            @if (Auth::user()->hasAnyRole(["drp","admin"]))
                                                            @if ($app->winner != 1 && $app->approval_status == 0)
                                                                @if (\App\Perms::perm_methods('call_evaluations', 'records'))
                                                                    @if (($app->eval_status == 0 || $app->eval_status == 1))
                                                                        <a href="{{ route('evaluations.manual_winner', [$app->call_id, $app->call_app_id]) }}"
                                                                            title="Set winner"
                                                                            class="btn btn-outline-success btn-xs">
                                                                            <i class="fa fa-thumbs-up"></i>
                                                                        </a>
                                                                    @endif
                                                                @endif
                                                            @endif
                                                            @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                                @php $serial++; @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="pull-right">
                                        {{ $call_apps->links() }}
                                    </div>
                                    <!--./pull-right -->
                                @else
                                    <div class="alert alert-danger"> No any call application found.</div>
                                @endif
                            </div>
                            <!--./tab-content -->
                        </div>
                        <!--./tab-content -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>

    <!-- Modal -->
    <div class="modal fade" id="awardModal" tabindex="-1" role="dialog" aria-labelledby="awardModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-600">Application Information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!--./model-header-->

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="notificationBar"></div>
                            <span id="errorMsg-2" class="text-danger"></span>

                            <table class="table table-striped table-bordered table-sm">
                                <tr>
                                    <th width="20%"><span class="font-weight-bold">Project Title</span></th>
                                    <td colspan="3"><span id="projectTitle"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Principal Investigator (PI)</span></th>
                                    <td colspan="3"><span id="pi"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Call Category</span></th>
                                    <td colspan="3"><span id="category"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Total Budget</span></th>
                                    <td colspan="3"><span id="budget"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Submitted Date</span></th>
                                    <td colspan="3"><span id="submittedDate"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Evaluation Grade</span></th>
                                    <td colspan="3"><span id="evaluationGrade"></span></td>
                                </tr>
                            </table>
                        </div>
                        <!--./col-md-12 -->

                        <div class="col-md-12">
                            <div class="float-left">
                                <button type="button" id="btn-award" class="btn btn-success font-weight-600"><i
                                        class="fa fa-check"></i> Award</button>&nbsp;
                                <button type="button" id="btn-decline" class="btn btn-danger font-weight-600"><i
                                        class="fa fa-times"></i> Decline</button>
                            </div>
                            <!--./float-right -->
                        </div>
                        <!--./col -->
                    </div>
                    <!--./row -->
                </div>
                <!--./model-body-->

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                        Close
                    </button>
                </div>
                <!--./model-footer-->
            </div>
        </div>
    </div>
    <!--./modal-->

    <!-- Modal -->
    <div class="modal fade" id="approveModal" tabindex="-1" role="dialog" aria-labelledby="approveModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-600">Application Information :
                        <span id="approval-level"></span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!--./model-header-->

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="notificationBar-2"></div>
                            <span id="errorMsg-3" class="text-danger"></span>

                            <table class="table table-striped table-bordered table-sm">
                                <tr>
                                    <th width="20%"><span class="font-weight-bold">Project Title</span></th>
                                    <td colspan="3"><span id="projectTitle-2"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Principal Investigator (PI)</span></th>
                                    <td colspan="3"><span id="pi-2"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Introduction</span></th>
                                    <td colspan="3"><span id="introduction"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">General Objectives</span></th>
                                    <td colspan="3"><span id="generalObjective"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Total Budget</span></th>
                                    <td colspan="3"><span id="budget-2"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Submitted Date</span></th>
                                    <td colspan="3"><span id="submittedDate-2"></span></td>
                                </tr>
                            </table>
                        </div>
                        <!--./col-md-12 -->

                        <div class="col-md-12">
                            <div class="float-left">
                                <button type="button" id="btn-approve" class="btn btn-success font-weight-600"><i
                                        class="fa fa-check"></i> Approve</button>&nbsp;
                            </div>
                            <!--./float-right -->
                        </div>
                        <!--./col -->
                    </div>
                    <!--./row -->
                </div>
                <!--./model-body-->

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                        Close
                    </button>
                </div>
                <!--./model-footer-->
            </div>
        </div>
    </div>
    <!--./modal-->
@endsection
