@extends('layouts.admin_app')

@section('content')
@include('calls.add_more_category')
<script type="text/javascript">
    //suggest_research_areas();
</script>
<!--./script -->
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <h4>Research Calls</h4>
                        <ul>
                            <li><a href="{{ route('calls.stats') }}">Dashboard</a></li>
                            <li><a href="{{url('calls')}}" title=""> Research Calls</a></li>
                            <li>Edit Call</li>
                        </ul>
                    </div><!-- Page Title -->
                </div>
                <!--./col-md-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    @include('calls.sidebar_menu')
                </div>
                <!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    <div class="card card-flat pure-form">
                        <div class="card-body">
                            <h5 class="text-uppercase">Edit Call</h5>
                            <hr />

                            @if ($errors->any())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                {{ $error }}<br />
                                @endforeach
                            </div>
                            @endif

                            {{ Form::open(['url' => route('calls.update', $call->id), 'class="form-horizontal"']) }}
                            @method('PATCH')
                            {{Form::token()}}
                            {{Form::hidden('url', url(''), ['id' => 'url'])}}
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Title <span style="color: red;">*</span></label>
                                        {{Form::textarea("name", $call->title, ['class="form-control"', 'rows="3"', 'placeholder="Write title..."'])}}
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Introduction <span style="color: red;">*</span></label>
                                        {{Form::textarea("introduction", $call->introduction, ['class="form-control" rows="5" placeholder="Write call introduction..."'])}}
                                        <script>
                                            CKEDITOR.replace('introduction');
                                        </script>
                                        <span class="text-danger">{{ $errors->first('introduction') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            @if(isset($call_categories) && count($call_categories) > 0)
                            <div class="row field_wrapper">
                                @foreach($call_categories as $category)
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label>Call Category <span style="color: red;">*</span></label>
                                        {{Form::text("call_category[]", $category->title, ['class="form-control"', 'placeholder="Write call category..."'])}}
                                        <span class="text-danger">{{ $errors->first('call_category') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Budget Ceiling <span style="color: red;">*</span></label>
                                        {{Form::text("budget_ceiling[]", $category->budget_ceiling, ['class="form-control"', 'placeholder="Write budget ceiling..."', 'onkeyup' => 'budgetFormat()'])}}
                                        <span class="text-danger">{{ $errors->first('budget_ceiling') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Call Category Description<span style="color: red;">*</span></label>
                                        {{Form::textarea("call_category_desc[]", $category->description, ['class="form-control"', 'placeholder="Write call category description..."', 'rows' => 4])}}
                                        <span class="text-danger">{{ $errors->first('call_category_desc') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                                @endforeach
                            </div>
                            <!--./row -->
                            @else
                            <div class="row field_wrapper">
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label>Call Category <span style="color: red;">*</span></label>
                                        {{Form::text("call_category[]", old('call_category[]'), ['class="form-control"', 'placeholder="Write call category..."'])}}
                                        <span class="text-danger">{{ $errors->first('call_category') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Budget Ceiling <span style="color: red;">*</span></label>
                                        {{Form::text("budget_ceiling[]", old('budget_ceiling[]'), ['class="form-control"', 'placeholder="Write budget ceiling..."', 'onkeyup' => 'budgetFormat()'])}}
                                        <span class="text-danger">{{ $errors->first('budget_ceiling') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Call Category Description<span style="color: red;">*</span></label>
                                        {{Form::textarea("call_category_desc[]", old('call_category_desc[]'), ['class="form-control"', 'placeholder="Write call category description..."'])}}
                                        <script>
                                            CKEDITOR.replace('call_category_desc[]');
                                        </script>
                                        <span class="text-danger">{{ $errors->first('call_category_desc') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->
                            @endif


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-right">
                                        <a href="javascript:void(0);" class="add_button btn btn-info btn-sm"
                                            title="Add field">Add More</a>
                                    </div>
                                    <!--./pull-right -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Research Areas <span style="color: red;">*</span></label><br />
                                        @if (isset($clusters) && count($clusters) > 0)
                                        @php $serial = 0; @endphp
                                        @foreach ($clusters as $cluster)
                                        {{Form::checkbox('cluster_ids[]', $cluster->id, in_array($cluster->id, $arr_cluster_ids) ? 'checked' : '')}}
                                        <label>{{$cluster->name}}</label><br />
                                        @if(isset($cluster->research_areas) && count($cluster->research_areas) > 0)
                                        @foreach($cluster->research_areas as $area)
                                        {!! '<i>'.$area->name.'</i>;'!!}
                                        @endforeach
                                        @endif
                                        <div class="mt-3"></div>
                                        @php $serial++; @endphp
                                        @endforeach
                                        @endif
                                        <span class="text-danger">{{ $errors->first('cluster_ids') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Team Composition <span style="color: red;">*</span></label>
                                        {{Form::textarea('team_composition', $call->team_composition, ['class="form-control" rows="5" placeholder="Write team composition ..."'])}}
                                        <script>
                                            CKEDITOR.replace('team_composition');
                                        </script>
                                        <span class="text-danger">{{ $errors->first('team_composition') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Eligibility <span style="color: red;">*</span></label>
                                        {{Form::textarea('eligibility', $call->eligibility, ['class="form-control" rows="5" placeholder="Write eligibility..."'])}}
                                        <script>
                                            CKEDITOR.replace('eligibility');
                                        </script>
                                        <span class="text-danger">{{ $errors->first('eligibility') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Project Duration <span style="color: red;">*</span></label>
                                        {{Form::textarea('duration', $call->duration, ['class="form-control" rows="3" placeholder="Write project duration"'])}}
                                        <span class="text-danger">{{ $errors->first('duration') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Application Details <span style="color: red;">*</span></label>
                                        {{Form::textarea('application_details', $call->application_details, ['class="form-control" rows="5" placeholder="Write application details ..."'])}}
                                        <script>
                                            CKEDITOR.replace('application_details');
                                        </script>
                                        <span class="text-danger">{{ $errors->first('application_details') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Deliverable <span style="color: red;">*</span></label>
                                        {{Form::textarea('deliverable', $call->deliverable, ['class="form-control" rows="5" placeholder="Write deliverable..."'])}}
                                        <script>
                                            CKEDITOR.replace('deliverable');
                                        </script>
                                        <span class="text-danger">{{ $errors->first('deliverable') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Evaluation Criteria <span style="color: red;">*</span></label>
                                        {{Form::textarea('evaluation_criteria', $call->evaluation_criteria, ['class="form-control" rows="5" placeholder="Write evaluation criteria..."'])}}
                                        <script>
                                            CKEDITOR.replace('evaluation_criteria');
                                        </script>
                                        <span class="text-danger">{{ $errors->first('evaluation_criteria') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row mt-3">
                                <div class="col-lg-12">
                                    <h6>Program Coordinator/Accountant</h6>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Program Coordinator</label>
                                        @php $_option = []; @endphp
                                        @foreach ($program_coordinators as $val)
                                            @php $_option[$val->id] = $val->first_name . ' ' . $val->middle_name . ' '. $val->surname @endphp
                                        @endforeach
                                        @php $_option = ['' => '-- Select --'] + $_option; @endphp
                                        {{ Form::select('program_coordinator', $_option, old('program_coordinator', $call->program_coordinator), ['data-placeholder="   -- Select --" class="form-control chosen-select"']) }}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-md-6 -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Program Accountant</label>
                                        @php $_option = []; @endphp
                                        @foreach ($program_accountants as $val)
                                            @php $_option[$val->id] = $val->first_name . ' ' . $val->middle_name . ' '. $val->surname @endphp
                                        @endforeach
                                        @php $_option = ['' => '-- Select --'] + $_option; @endphp
                                        {{ Form::select('program_accountant', $_option, old('program_accountant', $call->program_accountant), ['data-placeholder="   -- Select --" class="form-control chosen-select"']) }}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-md-6 -->
                            </div>
                            <!--./row -->

                            <div class="row mt-3">
                                <div class="col-lg-12">
                                    <h6>Application Deadline</h6>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Date <span style="color: red;">*</span></label>
                                        {{Form::date('date', date('Y-m-d',strtotime($call->deadline)), ['class="form-control" placeholder="Write application deadline"', 'min' => date('Y-m-d')])}}
                                        <span class="text-danger">{{ $errors->first('date') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Time <span style="color: red;">*</span></label>
                                        @php($time_option =[])
                                        @php($range = range(strtotime("00:00"), strtotime("23:59"), 30 * 60))

                                        @foreach($range as $time)
                                        @php($time_option[date("H:i", $time)] = date("H:i", $time))
                                        @endforeach
                                        @php($time_option = array('' => 'Time') + $time_option)

                                        {{ Form::select('time', $time_option, old('time', date('H:i', strtotime($call->deadline))), ['class' => 'form-control'])}}
                                        <span class="text-danger">{{ $errors->first('time') }}</span>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="float-right">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-secondary font-weight-600">Update</button>
                                        <a href="{{url('/calls')}}" class="btn btn-danger font-weight-600">Cancel</a>
                                    </div>
                                </div>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->
                            {{ Form::close() }}
                        </div>
                        <!--./card-body -->
                    </div>
                    <!--./card -->
                </div>
                <!--./col-lg-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection