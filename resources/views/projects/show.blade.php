@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Project Information</h4>
                            <ul>
                                <li><a href="{{ route('projects.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('projects') }}"> Projects</a></li>
                                <li>Project Information</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('projects.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <!--./tabs -->
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link @if (Request::segment(3) == '') active @endif" href="#">Information</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link @if (Request::segment(3) == 'activities') active @endif" 
                                href="{{ route('projects.activities', $project->id) }}">Activities</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link @if (Request::segment(3) == 'fund-requests') active @endif"
                                    href="{{ route('projects.fund-requests', $project->id) }}">Fund Requests</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link @if (Request::segment(3) == 'progress-reports') active @endif"
                                    href="{{ route('projects.progress-reports', $project->id) }}">Progress Reports</a>
                            </li>
                        </ul>
                        <!--./tab-lists -->

                        <div class="tab-content" id="myTabContent">
                            <div class="row p-2">
                                <div class="col-md-12">
                                    <table class="table table-striped table-bordered table-sm">
                                        <tr>
                                            <th width="25%"><span class="font-weight-bold">Project Title</span></th>
                                            <td colspan="3">
                                                <span
                                                    class="text-uppercase font-weight-bold">{{ $project->application->project_title }}</span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th><span class="font-weight-bold">Registration Number </span></th>
                                            <td>
                                                {{ $project->project_reg_number }}
                                            </td>

                                            <th><span class="font-weight-bold">Registration Date </span></th>
                                            <td>
                                                {{ date('d-m-Y', strtotime($project->created_at)) }}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th><span class="font-weight-bold">Introduction </span></th>
                                            <td colspan="3">
                                                {!! $project->application->introduction !!}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th><span class="font-weight-bold">Problem statement </span>
                                            </th>
                                            <td colspan="3">
                                                {!! $project->application->problem_statement !!}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th><span class="font-weight-bold">Rationale </span>
                                            </th>
                                            <td colspan="3">
                                                {!! $project->application->rationale !!}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th><span class="font-weight-bold">General objective </span>
                                            </th>
                                            <td colspan="3">
                                                {!! $project->application->general_objective !!}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th><span class="font-weight-bold">Literature review objective </span>
                                            </th>
                                            <td colspan="3">
                                                {!! $project->application->literature_review !!}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th><span class="font-weight-bold">Methodology </span>
                                            </th>
                                            <td colspan="3">
                                                {!! $project->application->methodology !!}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th><span class="font-weight-bold">Research output </span>
                                            </th>
                                            <td colspan="3">
                                                {!! $project->application->research_output !!}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th><span class="font-weight-bold">Capability </span>
                                            </th>
                                            <td colspan="3">
                                                {{ $project->application->capability }}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th><span class="font-weight-bold">Sustainability measures </span>
                                            </th>
                                            <td colspan="3">
                                                {!! $project->application->sustainability_measures !!}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th><span class="font-weight-bold">Management structure </span>
                                            </th>
                                            <td colspan="3">
                                                {!! $project->application->management_structure !!}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th><span class="font-weight-bold">Implementation plan </span>
                                            </th>
                                            <td colspan="3">
                                                {!! $project->application->implementation_plan !!}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th><span class="font-weight-bold">Total Fund</span></th>
                                            <td>TZS {{ number_format($project->total_fund) }}</td>

                                            <th><span class="font-weight-bold">Remained Fund </span></th>
                                            <td>

                                            </td>
                                        </tr>

                                        <tr>
                                            <th><span class="font-weight-bold">Start Date </span></th>
                                            <td>

                                            </td>

                                            <th><span class="font-weight-bold">End Date </span></th>
                                            <td>

                                            </td>
                                        </tr>

                                        <tr>
                                            <th><span class="font-weight-bold">Project Duration (Months) </span></th>
                                            <td colspan="3">
                                                {{ $project->application->call->duration }}
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!--./col-md-10 -->
                            </div>
                            <!--./row -->
                        </div>
                        <!--./tab-content -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
    <script>
    @endsection
