@extends('layouts.admin_app')
@section('content')

    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h4>New Fund Request</h4>
                            <ul>
                                <li><a href="{{ route('projects.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('projects') }}">Projects</a></li>
                                <li><a href="{{ route('projects.new-fund-request', $project->id) }}">Fund Requests</a>
                                </li>
                                <li>New Fund Request</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('projects.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat pure-form">
                            <div class="card-body">
                                <h5 class="text-uppercase">New fund REQUEST</h5>
                                <hr />

                                @if (session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div>
                                @elseif (session()->get('info'))
                                    <div class="alert alert-info">
                                        {{ session()->get('info') }}
                                    </div>
                                @elseif (session()->get('danger'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('danger') }}
                                    </div>
                                @endif

                                {{ Form::open(['url' => route('projects.store-fund-request', $project->id), 'method="POST" class="form-horizontal" enctype="multipart/form-data"']) }}
                                {{ Form::token() }}

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Project Title</label>
                                            {{ Form::textarea('project_title', old('project_title', $project->application->project_title), ['class="form-control" rows="2" readonly=""']) }}
                                        </div>
                                    </div>
                                    <!--./col -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Installments <span style="color: red;">*</span></label>
                                            @php $_options = []; @endphp
                                            @foreach ($project_installments as $value)
                                                @php $_options[$value->id] = $value->name; @endphp
                                            @endforeach
                                            @php $_options = ['' => '-- Select --'] + $_options; @endphp
                                            {!! Form::select('project_installment_id', $_options, old('project_installment_id'), ['class' => 'form-control']) !!}
                                            <span class="text-danger">{{ $errors->first('project_installment_id') }}</span>
                                        </div>
                                    </div>
                                    <!--./col -->

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Remained Fund (TZS)</label>
                                            <input name="remained_fund" readonly type="text"
                                                value="{{ number_format($project->total_fund) }}" class="form-control">
                                        </div>
                                    </div>
                                    <!--./col -->

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Requested Amount (TZS) <span style="color: red;">*</span></label>
                                            {{ Form::text('requested_amount', old('requested_amount'), ['class="form-control" placeholder="Write amount..."', 'id="number_comma"']) }}
                                            <span class="text-danger">{{ $errors->first('requested_amount') }}</span>
                                        </div>
                                    </div>
                                    <!--./col -->

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Budget attachment <span style="color: red;">*</span></label>
                                            {{ Form::file('attachment', ['class="form-control"']) }}
                                            <span class="text-danger">{{ $errors->first('attachment') }}</span>
                                        </div>
                                    </div>
                                    <!--./col -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Activities to be performed <span style="color: red;">*</span></label>
                                            {{ Form::textarea('activities_performed', old('activities_performed'), ['class="form-control" rows="2" placeholder="Write activities to be performed..."']) }}
                                            <script>
                                                CKEDITOR.replace('activities_performed');
                                            </script>
                                            <span class="text-danger">{{ $errors->first('activities_performed') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-md-12 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group pull-right">
                                            <button type="submit" class="btn btn-primary font-weight-600">Submit</button>
                                            <a href="{{ route('projects.new-fund-request', $project->id) }}" class="btn btn-danger font-weight-600">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                                <!--./row -->
                                {{ Form::close() }}
                            </div>
                            <!--./card-body -->
                        </div>
                        <!--./card -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
