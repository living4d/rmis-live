<!--./tabs -->
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link @if(Request::segment(3) == '') active @endif" href="#">Information</a>
    </li>

        <li class="nav-item">
            <a class="nav-link @if(Request::segment(3) == 'invitations') active @endif" href="#">Activities</a>
        </li>
</ul>
<!--./tab-lists -->