<div id="left_menu">
    <ul id="nav">
        @if (\App\Perms::perm_methods('projects', 'stats'))
            <li><a href="{{ route('projects.stats') }}">Summary</a></li>
        @endif

        @if (\App\Perms::perm_methods('projects', 'new_fund_requests'))
            <li><a href="{{ route('fund-requests.lists') }}">Fund Requests
                    <span class="badge badge-danger">
                        {{ show_new_approval_notification() }}
                    </span>
                </a></li>
        @endif

        @if (\App\Perms::perm_methods('projects', 'approved_fund_requests'))
            <li><a href="{{ route('fund-requests.approved') }}">Approved Fund Requests </a></li>
        @endif

        @if (\App\Perms::perm_methods('projects', 'lists'))
            <li><a href="{{ route('projects.lists') }}">Projects</a></li>
        @endif

        <li><a href="{{ route('projects.index') }}">My Projects</a></li>
    </ul>
</div>
