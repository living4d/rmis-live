@extends('layouts.admin_app')

{{-- <link href="{{ asset('/assets/css/letter.css') }}" rel="stylesheet"> --}}
@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Approval Letter</h4>
                            <ul>
                                <li><a href="{{ route('projects.stats') }}"><i class="fa fa-home"></i> Dashboard</a>
                                </li>
                                <li>Approval Letter</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('projects.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div id="messages"></div>
                        <!--./messages -->

                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="text-uppercase">Approval Letter</h5>
                                <hr />

                                <div class="row">
                                    <div class="col-sm-2">
                                        <button class="btn btn-success" onclick="printDiv('printableArea')">
                                            <i class="fa fa-print" aria-hidden="true"></i> Print Approval
                                        </button>
                                    </div>
                                    <div class="col-sm-4">
                                        {{ Form::open(['url' => route('store_approval_printed_status', ['id' => $request->id]), 'method="POST" class="form-horizontal"']) }}
                                        @csrf
                                        <label>is it printed?</label>
                                        <input name="printed_status" type="checkbox" value="is_printed" />
                                        <input type="submit" class="btn btn-sm btn-secondary" />
                                    </div>
                                </div>

                                <div class="mb-4"></div>

                                <div id="printableArea" style="background-color: #FFFFFF; padding:40px;">
                                    <div class="row container mt-3">
                                        <div align="center" class="col-sm-12">
                                            <h1 style="color:darkblue;font-size:55px;"> UNIVERSITY OF DAR ES SALAAM </h1>
                                            <h2 style="color:red;font-size:30px;"> OFFICE OF THE DEPUTY VICE CHANCELLOR -
                                                RESEARCH </h2>
                                            <h3>P.O. BOX 35091 <i class="fa fa-stop" aria-hidden="true"></i> DAR ES
                                                SALAAM <i class="fa fa-stop" aria-hidden="true"></i> TANZANIA</h3>
                                        </div>
                                    </div>
                                    <div class="row container">
                                        <div align='left' class="col-sm-5">
                                            <p>
                                                General: +255 22 2410500 <br>
                                                Direct: +255 22 2410743 <br>
                                                <br>
                                                <span class="font-weight-normal">Ref. No: AD.203/319/01</span>
                                            </p>
                                        </div>
                                        <div align="center" class="col-sm-2">
                                            <img src="{{ asset('assets/img/udsm_logo.png') }}" />
                                        </div>

                                        <div align='right' class="col-sm-5">
                                            <p class="text-right">
                                                Telegraphic: UNIVERSITY OF DAR ES SALAAM <br>
                                                E-mail: dvc.research@udsm.ac.tz <br>
                                                Website address: www.udsm.ac.tz <br>
                                                <!-- <span>Date: {{ date('d/m/Y') }}</span> -->
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row container">
                                        <div class="col-lg-12">
                                            <div class="mt-4" align="center" class="col-sm-12">
                                                <h3 id="re-title">
                                                    FUND REQUEST APPROVAL
                                                </h3>
                                            </div>


                                            <div id="letter-content">
                                                <table class="">
                                                    <tr>
                                                        <td width="20%" valign="top"><b>Project Name:</b></td>
                                                        <td width="80%">
                                                            {{ $request->project->application->project_title }}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td><b>Registration Number:</b></td>
                                                        <td>
                                                            {{ $request->project->project_reg_number }}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td><b>PI:</b></td>
                                                        <td>
                                                            {{ $request->project->application->pi->first_name . ' ' . $request->project->application->pi->surname }}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td><b>Department:</b></td>
                                                        <td>
                                                            {{ $request->project->application->pi->department->name }}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                            <div class="mt-3">
                                                <span class="text-uppercase font-weight-600">
                                                    Activities requested for funds;
                                                </span>
                                            </div>

                                            <div>
                                                <table class="table table-hover table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th width="3%">#</th>
                                                            <th width="20%">Activity</th>
                                                            <th width="30%">Objective</th>
                                                            <th width="10%">Start Date</th>
                                                            <th width="10%">End Date</th>
                                                            <th width="16%">Requested Amount (TZS)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $serial = 1;
                                                            $total = 0;
                                                        @endphp
                                                        @foreach ($request->activities as $values)
                                                            <tr>
                                                                <td>{{ $serial }}</td>
                                                                <td>{{ $values->activity->title }}</td>
                                                                <td>{{ $values->activity->objective->objective }}
                                                                </td>
                                                                <td>{{ date('d-m-Y', strtotime($values->activity->start_date)) }}
                                                                </td>
                                                                <td>{{ date('d-m-Y', strtotime($values->activity->end_date)) }}
                                                                </td>
                                                                <td>
                                                                    {{ number_format($values->activity->remained_fund) }}
                                                                </td>
                                                            </tr>
                                                            @php
                                                                $serial++;
                                                                $total += $values->activity->remained_fund;
                                                            @endphp
                                                        @endforeach

                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td><b>{{ number_format($total) }}</b></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <span class="font-weight-600">
                                                    Fund Management Responsible:
                                                </span>

                                                <table class="">
                                                    <tr>
                                                        <td><b>PI:</b></td>
                                                        <td>
                                                            {{ $request->project->application->pi->first_name . ' ' . $request->project->application->pi->surname }}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td><b>Other:</b></td>
                                                        <td>
                                                            @if ($resp_user == 'only_pi')
                                                                none
                                                            @else
                                                                {{ $resp_user->first_name . ' ' . $resp_user->surname }}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                            <div class="mt-3">
                                                <span class="text-uppercase font-weight-600">
                                                    Approval Status
                                                </span>
                                            </div>

                                            <div>
                                                <table width="80%" style="border: 1px solid #3f3f3f;">
                                                    <tr style="border: 1px solid #3f3f3f; padding: 8px;">
                                                        <td width="24%">Program Coordinator</td>
                                                        <td width="32%">
                                                            {{ $pc->first_name . ' ' . $pc->middle_name . ' ' . $pc->surname }}
                                                        </td>
                                                        <td width="20%">Recommended</td>
                                                    </tr>

                                                    <tr style="border: 1px solid #3f3f3f; padding: 8px;">
                                                        <td width="24%">Director of Research and Publication</td>
                                                        <td width="32%">
                                                            {{ $drp->first_name . ' ' . $drp->middle_name . ' ' . $drp->surname }}
                                                        </td>
                                                        <td width="20%">Recommended</td>
                                                    </tr>

                                                    <tr style="border: 1px solid #3f3f3f; padding: 8px;">
                                                        <td width="34%">Deputy Vice Chancellor - Research</td>
                                                        <td width="32%">
                                                            {{ $dvc->first_name . ' ' . $dvc->middle_name . ' ' . $dvc->surname }}
                                                        </td>
                                                        <td width="20%">Approved</td>
                                                    </tr>
                                                </table>
                                            </div>



                                            <div class="mt-4">
                                                <div class="row">
                                                    <div align="center" class="col-sm-12">
                                                        <b>Approved By</b><br /><br />
                                                        <!-- {{ $dvc->first_name . ' ' . $dvc->middle_name . ' ' . $dvc->surname }} -->
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <b>Signature:</b>
                                                        <hr style="background-color: #3f3f3f; margin-top: 30px;" />
                                                    </div>


                                                    <div class="col-md-4">
                                                        <b>Date</b><br />
                                                        <hr style="background-color: #3f3f3f; margin-top: 30px;" />
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <b>Deputy Vice Chancellor - Research</b><br /><br />
                                                        <!-- {{ $dvc->first_name . ' ' . $dvc->middle_name . ' ' . $dvc->surname }} -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--./col -->
                                    </div>
                                    <!--./row -->
                                </div>
                                <!--./print-content -->
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>

    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
@endsection
