@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
  <section class="page-wrapper">
    <div class="page-heading">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="page-title">
              <h4>My Projects</h4>
              <ul>
                <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                <li>My Projects</li>
              </ul>
            </div><!-- Page Title -->
          </div>
          <!--./col-md-12 -->
        </div>
        <!--./row -->
      </div>
      <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
      <div class="container">
        <div class="row">
          <div class="col-lg-2 col-md-2">
            @include('projects.sidebar_menu')
          </div>
          <!-- sidebar -->
          <!-- sidebar -->

          <!--col-md-2 -->

          <div class="col-lg-10 col-md-10">
            <div class="card card-flat">
              <h5 class="card-header text-uppercase">Project fund request details</h5>
              <!--./card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-12">
                    <h6 class="title text-uppercase">Project fund request</h6>
                  </div>
                  <!--./col-lg-12 -->
                </div>
                <!--./row -->
                <div class="row p-2">
                  <div class="col-md-12">
                    <table class="table table-bordered">

                      <tr>
                        <td width="20%"><span class="font-weight-bold">Amount </span>
                        </td>
                        <td width="80%">
                          <span class="text-uppercase">{{$data['request']['amount']}}</span>
                        </td>
                      </tr>
                      <tr>
                        <td width="20%"><span class="font-weight-bold">Requested on </span>
                        </td>
                        <td width="80%">
                          <span class="text-uppercase">{{$data['request']['created_at']}}</span>
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-12">
                    <h7 class="title text-uppercase">Reverse comments</h7>
                  </div>
                  <!--./col-lg-12 -->
                </div>
                <!--./row -->
                @if (count($data['remarks']) > 0)
                <div class="row mb-2">
                  <div class="col-lg-8">
                    <table width="100%" class="table table-hover table-bordered table-responsive">
                      <thead>
                        <tr>
                          <th width="3%">From</th>
                          <th width="30%">Comments</th>
                          <th width="10%">Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($data['remarks'] as $remark)
                        <tr>
                          <td>{{$remark['comment_from']}}</td>
                          <td>{{$remark['remarks']}}</td>
                          <td>{{$remark['created_at']}}</td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                  <!--./col-lg-6 -->
                </div>
                <!--./row -->
                @endif

                {{-- actions --}}
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <a href="{{route('fund-requests.edit',['id'=>$data['request']->id])}}" class="btn btn-primary btn-xs">EDIT</a>
                    </div>
                  </div>
                </div>
              </div>
              <!--./card-body -->
            </div>
            <!--./card -->
          </div>
          <!--./col-lg-9 -->
        </div>
        <!--./row -->
      </div>
      <!--./container -->
    </div>
    <!--./page-content -->
  </section>
  @endsection