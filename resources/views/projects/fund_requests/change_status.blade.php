@extends('layouts.admin_app')
@section('content')

    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h4>Payment Status</h4>
                            <ul>
                                <li><a href="{{ route('projects.stats') }}"><i class="fa fa-home"></i> Dashboard</a>
                                </li>
                                <li><a href="{{ route('fund-requests.approved') }}"> Approved Requests</a></li>
                                <li>Change Payment Status</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('projects.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-flat pure-form">
                                    <div class="card-body">
                                        <h5 class="text-uppercase">Change Payment Status</h5>
                                        <hr />

                                        {{ Form::open(['url' => route('fund-requests.store-status', $request->id), 'method="POST" class="form-horizontal" enctype="multipart/form-data"']) }}
                                        {{ Form::token() }}

                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-12">
                                                <div class="form-group">
                                                    <label>Payment Status <span style="color: red;">*</span></label>
                                                    @php
                                                        $_options = [
                                                            '' => '-- Select --',
                                                            '1' => 'Paid',
                                                            '0' => 'Unpaid'
                                                        ];
                                                    @endphp
                                                    {{ Form::select('status', $_options, old('status', $request->payment_status), ['class="form-control"']) }}
                                                    <span class="text-danger">{{ $errors->first('status') }}</span>
                                                </div>
                                            </div>
                                            <!--./col -->

                                            <div class="col-lg-6 col-md-6 col-12">
                                                <div class="form-group">
                                                    <label>Payment Attachment <span style="color: red;">*</span></label>
                                                    {{ Form::file('attachment', ['class="form-control"']) }}
                                                    <span class="text-danger">{{ $errors->first('attachment') }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--./row -->

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Description </label>
                                                    {{ Form::textarea('description', old('description'), ['class="form-control" rows="2" placeholder="Write activity description..."']) }}
                                                    {{-- <script>
                                                        CKEDITOR.replace('description');
                                                    </script> --}}
                                                    <span
                                                        class="text-danger">{{ $errors->first('activities_performed') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-md-12 -->
                                        </div>
                                        <!--./row -->

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group pull-right">
                                                    <button type="submit"
                                                        class="btn btn-primary font-weight-600">Change</button>
                                                    <a href="{{ route('fund-requests.approved') }}" class="btn btn-danger font-weight-600">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!--./row -->
                                        {{ Form::close() }}
                                    </div>
                                    <!--./card-body -->
                                </div>
                                <!--./card -->
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->

                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
