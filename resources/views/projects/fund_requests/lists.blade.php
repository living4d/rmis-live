@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Fund Requests</h4>
                            <ul>
                                <li><a href="{{ route('projects.stats') }}"><i class="fa fa-home"></i> Dashboard</a>
                                </li>
                                <li>Fund Requests</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('projects.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div id="messages"></div>
                        <!--./messages -->

                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="text-uppercase">Fund Requests</h5>
                                <hr />

                                <div class="row">
                                    <div class="col-md-9">
                                    </div>
                                    <!--./col-md-9 -->

                                    <div class="col-md-3 pull-right">
                                        <input type="text" id="myCustomSearchBox" class="form-control"
                                            placeholder="Search details...">
                                    </div>
                                    <!--./col-md-4 -->
                                </div>
                                <!--./row -->

                                @if (isset($project_fund_requests) && count($project_fund_requests) > 0)
                                    <table id="dt" class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="3%">#</th>
                                                <th width="20%">Activities</th>
                                                <th width="20%">Project</th>
                                                <th width="10%">PI</th>
                                                <th width="10%">Amount (TZS)</th>
                                                <th width="8%">Requested</th>
                                                <th width="16%">Approval</th>
                                                <th width="10%">Status</th>
                                                <th width="10%">Attachments</th>
                                                <th style="width: 40px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $serial = 1; @endphp
                                            @foreach ($project_fund_requests as $values)
                                                <tr>
                                                    <td>{{ $serial }}</td>
                                                    <td>
                                                        @if ($values->activities)
                                                            @php $i = 1; @endphp
                                                            @foreach ($values->activities as $val)
                                                                {{ $i . '. ' . $val->activity->title }}<br />
                                                                @php $i++; @endphp
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td>{{ $values->project->application->project_title }}</td>
                                                    <td>
                                                        {{ $values->project->application->pi->first_name . ' ' . $values->project->application->pi->middle_name . ' ' . $values->project->application->pi->surname }}
                                                    </td>
                                                    <td>{{ number_format($values->amount) }}</td>
                                                    <td>{{ date('d-m-Y', strtotime($values->created_at)) }}</td>

                                                    <td>
                                                        @php show_fund_approval_status($values->id); @endphp
                                                    </td>
                                                    <td>
                                                        @if ($values->status === 'new')
                                                            <span class="badge badge-pill badge-warning">NEW</span>
                                                        @elseif ($values->status === 'partial-approved')
                                                            <span class="badge badge-pill badge-info">PARTIAL
                                                                APPROVED</span>
                                                        @elseif ($values->status === 'approved')
                                                            <span class="badge badge-pill badge-success">APPROVED</span>
                                                        @elseif ($values->status === 'rejected')
                                                            <span class="badge badge-pill badge-danger">RETURNED</span>
                                                        @endif
                                                    </td>

                                                    <td>
                                                        @if ($values->fundRequestAttachments)
                                                            @php $i = 1; @endphp
                                                            @foreach ($values->fundRequestAttachments as $val)
                                                                @if (file_exists(public_path() . '/assets/uploads/documents/' . $val->attachment))
                                                                    <a href="{{ asset('assets/uploads/documents/' . $val->attachment) }}"
                                                                        class="font-weight-600 text-primary" download>
                                                                        {{ $i . '. ' . $val->attachment_type }}
                                                                    </a>
                                                                    <br />
                                                                @endif
                                                                @php $i++; @endphp
                                                            @endforeach
                                                        @endif
                                                    </td>

                                                    <td>
                                                        @php show_fund_approval_operation($values->id); @endphp

                                                        @if ($values->status === 'approved')
                                                            @if (Auth::user()->hasRole('dvc_research'))
                                                                <a href="{{ route('fund-requests.approval-letter', $values->id) }}"
                                                                    data-toggle="tooltip" data-placement="bottom"
                                                                    title="Approval Letter"
                                                                    class="btn btn-outline-success btn-xs">
                                                                    @if ($values->printed_status==1)
                                                                    <i class="fa fa-print text-danger"></i>
                                                                    
                                                                    @else
                                                                    <i class="fa fa-print text-success"></i>
                                                                    @endif
                                                                </a>
                                                            @endif
                                                        @endif
                                                    </td>
                                                </tr>
                                                @php $serial++; @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="alert alert-danger mt-2">No any fund request for this activity</div>
                                @endif
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>

    <!-- Modal -->
    <div class="modal fade" id="approveFundModal" tabindex="-1" role="dialog" aria-labelledby="approveFundModal"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-600">New Project Fund Request :
                        <span id="approval-level"></span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!--./model-header-->

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="notificationBar"></div>
                            <span id="errorMsg" class="text-danger"></span>

                            <table class="table table-striped table-bordered table-sm">
                                <tr>
                                    <th width="20%"><span class="font-weight-bold">Project Title</span></th>
                                    <td colspan="3"><span id="projectTitle"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Activity Title</span></th>
                                    <td colspan="3"><span id="activityPerformed"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Requested Amount</span></th>
                                    <td colspan="3"><span id="requestedAmount"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Budget Attachments</span></th>
                                    <td colspan="3"><span id="budgetAttachments"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Submitted Date</span></th>
                                    <td colspan="3"><span id="submittedDate"></span></td>
                                </tr>
                            </table>
                        </div>
                        <!--./col-md-12 -->
                    </div>
                    <!--./row -->

                    <div class="row form-remarks" style="display: none;">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Remarks</label>
                                <textarea name="remarks" id="remarks" class="form-control" placeholder="Write remarks..."
                                    rows="3"></textarea>
                            </div>
                            <!--./form-group -->
                        </div>
                        <!--./col-md-12 -->
                    </div>
                    <!--./row -->

                    <div class="row form-remarks" style="display: none;">
                        <div class="col-md-12">
                            <div class="form-group pull-right">
                                <button type="button" id="btn-save-fund"
                                    class="btn btn-primary btn-sm text-medium">Save</button>
                                <button type="button" id="btn-cancel-fund"
                                    class="btn btn-danger btn-sm text-medium">Cancel</button>
                            </div>
                        </div>
                        <!--./col-md-12 -->
                    </div>
                    <!--./row -->

                </div>
                <!--./model-body-->

                <div class="modal-footer">
                    <button type="button" id="btn-print-letter" class="btn btn-success btn-sm">
                        <i class="fa fa-print"></i> Print Letter
                    </button>&nbsp;

                    <!--./combined button -->
                    <button type="button" id="btn-approve-fund" class="btn btn-success btn-sm"></button>&nbsp;
                    <button type="button" id="btn-return-fund" class="btn btn-danger btn-sm">
                        <i class="fa fa-times"></i> Return</button>&nbsp;
                </div>
                <!--./model-footer-->
            </div>
        </div>
    </div>
    <!--./modal-->
@endsection
