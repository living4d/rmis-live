@extends('layouts.admin_app')
@section('content')

    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h4>Request For Fund</h4>
                            <ul>
                                <li><a href="{{ route('projects.stats') }}"><i class="fa fa-home"></i> Dashboard</a>
                                </li>
                                <li><a href="{{ url('projects') }}">Projects</a></li>
                                <li><a href="{{ route('projects.fund-requests', $activities[0]->project->id) }}">Fund
                                        Requests</a>
                                </li>
                                <li>Request For Fund</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('projects.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat pure-form">
                            <div class="card-body">
                                <h5 class="text-uppercase">Request For Fund</h5>
                                <hr />

                                @if (session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div>
                                @elseif (session()->get('info'))
                                    <div class="alert alert-info">
                                        {{ session()->get('info') }}
                                    </div>
                                @elseif (session()->get('danger'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('danger') }}
                                    </div>
                                @endif

                                {{ Form::open(['url' => route('fund-requests.store-batch'), 'method="POST" class="form-horizontal" enctype="multipart/form-data"']) }}
                                {{ Form::token() }}
                                {{ Form::hidden('project_id', $activities[0]->project->id) }}

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Project Title</label>
                                            {{ Form::textarea('project_title', old('project_title', $activities[0]->project->application->project_title), ['class="form-control" rows="2" readonly=""']) }}
                                        </div>
                                    </div>
                                    <!--./col -->

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Remained Fund (TZS)</label>
                                            <input name="remained_fund" readonly type="text"
                                                value="{{ number_format($activities[0]->project->remained_fund) }}"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <!--./col -->
                                </div>
                                <!--./row -->

                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <h5 class="text-uppercase">Activities</h5>
                                        <hr />
                                    </div>
                                    <!--./col -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-md-12">
                                        @if ($activities && count($activities) > 0)
                                            <table class="table table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th width="3%">#</th>
                                                        <th width="20%">Activity</th>
                                                        <th width="30%">Objective</th>
                                                        <th width="10%">Start Date</th>
                                                        <th width="10%">End Date</th>
                                                        <th width="16%">Requested Amount (TZS)</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                        $serial = 1;
                                                        $total = 0;
                                                    @endphp
                                                    @foreach ($activities as $values)
                                                        {{ Form::hidden('activity_ids[]', $values->id) }}
                                                        {{ Form::hidden('requested_amount[]', $values->remained_fund) }}
                                                        <tr>
                                                            <td>{{ $serial }}</td>
                                                            <td>{{ $values->title }}</td>
                                                            <td>{{ $values->objective->objective }}</td>
                                                            <td>{{ date('d-m-Y', strtotime($values->start_date)) }}</td>
                                                            <td>{{ date('d-m-Y', strtotime($values->end_date)) }}</td>
                                                            <td class="text-right">
                                                                {{ number_format($values->remained_fund) }}
                                                            </td>
                                                            @php
                                                                $total += $values->remained_fund;
                                                            @endphp
                                                        </tr>
                                                        @php $serial++; @endphp
                                                    @endforeach
                                                    <tr>
                                                        <td colspan="5">
                                                            <span class="font-weight-600 text-uppercase">Total</span>
                                                        </td>
                                                        <td class="text-right">
                                                            <span class="font-weight-600">
                                                                {{ number_format($total) }}
                                                            </span>
                                                            {{ Form::hidden('total_amount', $total) }}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        @endif
                                    </div>
                                    <!--./col -->
                                </div>
                                <!--./row -->

                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <h5 class="text-uppercase">Attachments</h5>
                                        <hr />
                                    </div>
                                    <!--./col -->
                                </div>
                                <!--./row -->

                                @php
                                    $attachment_types = [
                                        'Contract' => 'Contract',
                                        'Budget' => 'Budget',
                                    ];
                                @endphp

                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="3%">#</th>
                                                    <th width="70%">Attchment Types</th>
                                                    <th width="24%">Attachment Files</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $serial = 1;
                                                @endphp
                                                @foreach ($attachment_types as $key => $val)
                                                    {{ Form::hidden('attachment_types[]', $key) }}
                                                    <tr>
                                                        <td>{{ $serial }}</td>
                                                        <td>{{ $val }}</td>
                                                        <td>
                                                            {{ Form::file('attachments[]', ['class="form-control"', 'required=""']) }}
                                                        </td>
                                                    </tr>
                                                    @php
                                                        $serial++;
                                                    @endphp
                                                @endforeach
                                                <span class="text-danger">{{ $errors->first('attachments') }}</span>
                                            </tbody>
                                        </table>

                                    </div>
                                    <!--./col -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group pull-right">
                                            <button type="submit" class="btn btn-primary font-weight-600">Request</button>
                                            <a href="{{ route('projects.activities', $activities[0]->project->id) }}"
                                                class="btn btn-danger font-weight-600">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                                <!--./row -->
                                {{ Form::close() }}
                            </div>
                            <!--./card-body -->
                        </div>
                        <!--./card -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
