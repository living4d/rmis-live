@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Project Activities</h4>
                            <ul>
                                <li><a href="{{ route('projects.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('projects') }}">My Projects</a></li>
                                <li><a
                                        href="{{ route('activities.lists', [$project->id, $objective->id]) }}">Activities</a>
                                </li>
                                <li>Funds Requests</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('projects.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <!--./tabs -->
                        @include('projects.activities.menu-bar')

                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="information" role="tabpanel"
                                aria-labelledby="information-tab">
                                <div class="row p-2 mt-3">
                                    <div class="col-md-12">
                                        <h5 class="text-uppercase">{{ $activity->title }}</h5>
                                        <hr />

                                        <div class="row">
                                            <div class="col-md-9">
                                                <a class="btn btn-primary btn-sm"
                                                    href="{{ route('activities.create-fund-request', $activity->id) }}"><i
                                                        class="fa fa-plus"></i> New Request</a>
                                            </div>
                                            <!--./col-md-9 -->

                                            <div class="col-md-3 pull-right">
                                                <input type="text" id="myCustomSearchBox" class="form-control"
                                                    placeholder="Search details...">
                                            </div>
                                            <!--./col-md-4 -->
                                        </div>
                                        <!--./row -->

                                        @if (isset($activity->funds) && count($activity->funds) > 0)
                                            <table id="dt" class="table table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th width="3%">#</th>
                                                        <th width="36%">Activities Performed</th>
                                                        <th width="10%">Amount</th>
                                                        <th width="8%">Date</th>
                                                        <th width="10%">Attachment</th>
                                                        <th width="16%">Approval</th>
                                                        <th width="8%">Status</th>
                                                        <th style="width: 40px;">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $serial = 1; @endphp
                                                    @foreach ($activity->funds as $values)
                                                        <tr>
                                                            <td>{{ $serial }}</td>
                                                            <td>{!! $values->activities_performed !!}</td>
                                                            <td>{{ number_format($values->amount) }}</td>
                                                            <td>{{ date('d-m-Y', strtotime($values->created_at)) }}</td>
                                                            <td>
                                                                @if ($values->attachment != null || $values->attachment != '')
                                                                    @if (file_exists(public_path() . '/assets/uploads/documents/' . $values->attachment))
                                                                        <a href="{{ asset('assets/uploads/documents/' . $values->attachment) }}"
                                                                            download><i class="fa fa-download"></i>
                                                                            Download</a>
                                                                    @endif
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @php show_fund_approval_status($values->id); @endphp
                                                            </td>
                                                            <td>
                                                                @if ($values->status == 'new')
                                                                    <span class="badge badge-pill badge-info">NEW</span>
                                                                @elseif($values->status == 'approved')
                                                                    <span
                                                                        class="badge badge-pill badge-success">APPROVED</span>
                                                                @elseif($values->status == 'rejected')
                                                                    <span
                                                                        class="badge badge-pill badge-danger">REJECTED</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <a href="#" data-toggle="tooltip" data-placement="bottom"
                                                                    title="Edit fund request"
                                                                    class="btn btn-outline-secondary btn-xs">
                                                                    <i class="fa fa-pencil text-secondary"></i>
                                                                </a>&nbsp;

                                                                <a href="#" title="Delete"
                                                                    class="btn btn-outline-danger btn-xs delete">
                                                                    <i class="fa fa-trash text-danger"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        @php $serial++; @endphp
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <div class="alert alert-danger mt-2">No any fund request for this activity</div>
                                        @endif
                                    </div>
                                    <!--./col-md-12 -->
                                </div>
                                <!--./row -->
                            </div>
                            <!--./information -->
                        </div>
                        <!--./tab-content -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
