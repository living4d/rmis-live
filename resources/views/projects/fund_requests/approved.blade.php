@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Approved Fund Requests</h4>
                            <ul>
                                <li><a href="{{ route('projects.stats') }}"><i class="fa fa-home"></i> Dashboard</a>
                                </li>
                                <li>Approved Fund Requests</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('projects.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div id="messages"></div>
                        <!--./messages -->

                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="text-uppercase">Approved Fund Requests</h5>
                                <hr />

                                <div class="row">
                                    <div class="col-md-9">
                                    </div>
                                    <!--./col-md-9 -->

                                    <div class="col-md-3 pull-right">
                                        <input type="text" id="myCustomSearchBox" class="form-control"
                                            placeholder="Search details...">
                                    </div>
                                    <!--./col-md-4 -->
                                </div>
                                <!--./row -->

                                @if (isset($project_fund_requests) && count($project_fund_requests) > 0)
                                    <table id="dt" class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="3%">#</th>
                                                <th width="24%">Project Title</th>
                                                <th width="22%">Activities</th>
                                                <th width="12%">Attachments</th>
                                                <th width="10%">Amount</th>
                                                <th width="12%">Requested On</th>
                                                <th width="10%">Status</th>
                                                <th style="width: 60px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $serial = 1; @endphp
                                            @foreach ($project_fund_requests as $values)
                                                <tr>
                                                    <td>{{ $serial }}</td>
                                                    <td>{{ $values->project->application->project_title }}</td>
                                                    <td>
                                                        @if ($values->activities)
                                                            @php $i = 1; @endphp
                                                            @foreach ($values->activities as $val)
                                                                {{ $i . '. ' . $val->activity->title }}<br />
                                                                @php $i++; @endphp
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($values->fundRequestAttachments)
                                                            @php $i = 1; @endphp
                                                            @foreach ($values->fundRequestAttachments as $val)
                                                                @if (file_exists(public_path() . '/assets/uploads/documents/' . $val->attachment))
                                                                    <a href="{{ asset('assets/uploads/documents/' . $val->attachment) }}"
                                                                        class="font-weight-600 text-primary" download>
                                                                        {{ $i . '. ' . $val->attachment_type }}
                                                                    </a>
                                                                    <br />
                                                                @endif
                                                                @php $i++; @endphp
                                                            @endforeach
                                                        @endif
                                                    </td>

                                                    <td class="text-right">{{ number_format($values->amount) }}</td>
                                                    <td>{{ date('d-m-Y', strtotime($values->created_at)) }}</td>

                                                    <td>
                                                        @if ($values->payment_status == 1)
                                                            <span class="badge badge-pill badge-success">PAID</span>
                                                        @elseif ($values->payment_status == 0)
                                                            <span class="badge badge-pill badge-danger">UNPAID</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="#approvedRequestModal" data-backdrop="static"
                                                            data-keyboard="false" data-toggle="modal"
                                                            title="Fund Request Information"
                                                            data-target="#approvedRequestModal"
                                                            data-fund-request-id="{{ $values->id }}"
                                                            class="btn btn-outline-primary btn-xs">
                                                            <i class="fa fa-folder-open text-primary"></i>
                                                        </a>&nbsp;

                                                        @if ($values->payment_status != 1)
                                                            <a href="{{ route('fund-requests.change-status', [$values->id]) }}"
                                                                data-toggle="tooltip" data-placement="bottom"
                                                                title="Fund Request Status" class="btn btn-outline-info btn-xs">
                                                                <i class="fa fa-info-circle text-info"></i>
                                                            </a>&nbsp;
                                                        @endif
                                                    </td>
                                                </tr>
                                                @php $serial++; @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="alert alert-danger mt-2">No any fund request for this activity</div>
                                @endif
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
