@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <h4>Project Information</h4>
                        <ul>
                            <li><a href="{{ route('projects.stats') }}"><i class="fa fa-home"></i> Dashboard</a>
                            </li>
                            <li><a href="{{ url('projects') }}"> Projects</a></li>
                            <li>Project Information</li>
                        </ul>
                    </div><!-- Page Title -->
                </div>
                <!--./col-md-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    @include('projects.sidebar_menu')
                </div>
                <!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    @if (session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div><br />
                    @elseif(session()->get('danger'))
                    <div class="alert alert-danger">
                        {{ session()->get('danger') }}
                    </div><br />
                    @endif

                    <!--./tabs -->
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link @if (Request::segment(3) == '') active @endif" href="{{ route('projects.show', $project->id) }}">Information</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link @if (Request::segment(3) == 'activities') active @endif" href="{{ route('projects.activities', $project->id) }}">Activities</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link @if (Request::segment(3) == 'fund-requests') active @endif" href="#">Fund Requests</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link @if (Request::segment(3) == 'progress-reports') active @endif" href="{{ route('projects.progress-reports', $project->id) }}">Progress Reports</a>
                        </li>
                    </ul>
                    <!--./tab-lists -->

                    <div class="tab-content" id="myTabContent">
                        <div class="row p-2">
                            <div class="col-md-12">

                                <div class="row">
                                    <div class="col-md-9">

                                    </div>
                                    <!--./col-md-9 -->

                                    <div class="col-md-3 float-right">
                                        <input type="text" id="myCustomSearchBox" class="form-control" placeholder="Search here...">
                                    </div>
                                    <!--./col-md-4 -->
                                </div>
                                <!--./row -->

                                @if (isset($project->fundRequests) && count($project->fundRequests) > 0)
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="6%">Installment</th>
                                            <th width="24%">Activity</th>
                                            <th width="10%">Amount</th>
                                            <th width="14%">Requested On</th>
                                            <th width="16%">Approval Status</th>
                                            <th width="10%">Status</th>
                                            <th width="10%">Attachments</th>
                                            <th style="width: 40px;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $serial = 1; @endphp
                                        @foreach ($project->fundRequests as $values)
                                        <tr>
                                            <td class="text-center">{{ $serial }}</td>
                                            <td>
                                                @if ($values->activities)
                                                @php $i = 1; @endphp
                                                @foreach ($values->activities as $val)
                                                {{ $i . '. ' . $val->activity->title }}<br />
                                                @php $i++; @endphp
                                                @endforeach
                                                @endif
                                            </td>
                                            <td class="text-right">
                                                {{ number_format($values->amount) }}
                                            </td>
                                            <td>{{ date('d-m-Y H:i', strtotime($values->created_at)) }}</td>
                                            <td>
                                                @php show_fund_approval_status($values->id); @endphp
                                            </td>
                                            <td>
                                                @if ($values->status == 'new')
                                                <span class="badge badge-pill badge-warning">NEW</span>
                                                @elseif ($values->status == 'partial-approved')
                                                <span class="badge badge-pill badge-info">PARTIAL
                                                    APPROVED</span>
                                                @elseif ($values->status == 'approved')
                                                <span class="badge badge-pill badge-success">APPROVED</span>
                                                @elseif ($values->status == 'rejected')
                                                <span class="badge badge-pill badge-danger">RETURNED</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($values->fundRequestAttachments)
                                                @php $i = 1; @endphp
                                                @foreach ($values->fundRequestAttachments as $val)
                                                @if (file_exists(public_path() . '/assets/uploads/documents/' . $val->attachment))
                                                <a href="{{ asset('assets/uploads/documents/' . $val->attachment) }}" class="font-weight-600 text-primary" download>
                                                    {{ $i . '. ' . $val->attachment_type }}
                                                </a>
                                                <br />
                                                @endif
                                                @php $i++; @endphp
                                                @endforeach
                                                @endif
                                            </td>
                                            <td>
                                                @if ($values->status == 'rejected' || $values->status == 'new')

                                                <a href="{{route('show_fund_request',['id'=>$values->id])}}" data-toggle="tooltip" data-placement="bottom" title="Fund request details" class="btn btn-outline-primary btn-xs">
                                                    <i class="fa fa-folder-open text-primary"></i>
                                                </a>

                                                <a href="{{ route('fund-requests.edit', $values->id) }}" data-toggle="tooltip" data-placement="bottom" title="Edit Fund Request" class="btn btn-outline-secondary btn-xs">
                                                    <i class="fa fa-pencil text-secondary"></i>
                                                </a>&nbsp;
                                                @endif
                                            </td>
                                        </tr>
                                        @php $serial++; @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                <div class="alert alert-danger mt-2">No any fund request at the moment</div>
                                @endif
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->
                    </div>
                    <!--./tab-content -->
                </div>
                <!--./col-lg-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection