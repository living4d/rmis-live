<!--./tabs -->
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link @if (Request::segment(3)=='details' ) active @endif"
            href="{{ route('activities.details', [$activity->id]) }}">Information</a>
    </li>

    <li class="nav-item">
        <a class="nav-link @if (Request::segment(3)=='fund-requests' ) active @endif" 
        href="{{ route('activities.fund-requests', [$activity->id]) }}">
            Funds Requests
        </a>
    </li>
</ul>
<!--./tab-lists -->
