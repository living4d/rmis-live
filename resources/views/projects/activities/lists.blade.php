@extends('layouts.admin_app')
@section('content')

    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h4>Project Activities</h4>
                            <ul>
                                <li><a href="{{ route('projects.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('projects') }}"> Projects</a></li>
                                <li>Activities</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('projects.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <h5 class="text-uppercase">Project Activities</h5>
                        <hr />

                        <table class="table table-striped table-bordered table-sm">
                            <tbody>
                                <tr>
                                    <th width="25%"><span style="font-weight: 600">Project Title:</span></th>
                                    <td colspan="3">{{ $project->application->project_title }}</td>
                                </tr>

                                <tr>
                                    <th width="25%"><span style="font-weight: 600">Project Number:</span></th>
                                    <td>{{ $project->project_reg_number }}</td>

                                    <th width="25%"><span style="font-weight: 600">Objective Budget(TZS):</span></th>
                                    <td>{{ number_format($objective->budget) }}</td>
                                </tr>

                                <tr>
                                    <th width="25%"><span style="font-weight: 600">Objective:</span></th>
                                    <td colspan="3">{{ $objective->objective }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <!--./table -->

                        <div class="mt-3"></div>
                        <!--./mt-3 -->

                        <div class="row">
                            <div class="col-md-9">
                                <a href="{{ route('activities.create', [$project->id, $objective->id]) }}"
                                    class="btn btn-primary btn-sm">
                                    <i class="fa fa-plus"></i> Register New</a>
                            </div>
                            <!--./col -->

                            <div class="col-md-3 pull-right">
                                <input type="text" id="myCustomSearchBox" class="form-control"
                                    placeholder="Search details...">
                            </div>
                            <!--./col -->
                        </div>
                        <!--./row -->

                        @if ($objective->activities && count($objective->activities) > 0)
                            <table id="dt" class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th width="3%">#</th>
                                        <th width="44%">Title</th>
                                        <th width="10%">Budget</th>
                                        <th width="10%">Start Date</th>
                                        <th width="10%">End Date</th>
                                        <th width="10%">Status</th>
                                        <th style="width: 80px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $serial = 1; @endphp
                                    @foreach ($objective->activities as $values)
                                        <tr>
                                            <td>{{ $serial }}</td>
                                            <td>{{ $values->title }}</td>
                                            <td>{{ number_format($values->budget) }}</td>
                                            <td>{{ date('d-m-Y', strtotime($values->start_date)) }}</td>
                                            <td>{{ date('d-m-Y', strtotime($values->end_date)) }}</td>
                                            <td>
                                                @if ($values->status == 'new')
                                                    <span class="badge badge-pill badge-warning">NEW</span>
                                                @elseif ($values->status == 'on-progress')
                                                    <span class="badge badge-pill badge-info">ON PROGRESS</span>
                                                @elseif ($values->status == 'done')
                                                    <span class="badge badge-pill badge-success">DONE</span>
                                                @elseif ($values->status == 'cancelled')
                                                    <span class="badge badge-pill badge-danger">CANCELLED</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="#activityModal" data-backdrop="static" data-keyboard="false"
                                                    data-toggle="modal" title="Activity Information"
                                                    data-target="#activityModal" data-activity-id="{{ $values->id }}"
                                                    class="btn btn-outline-primary btn-xs">
                                                    <i class="fa fa-folder-open text-primary"></i>
                                                </a>&nbsp;

                                                <a href="{{ route('activities.edit', [$project->id, $values->id]) }}"
                                                    data-toggle="tooltip" data-placement="bottom" title="Edit Activity"
                                                    class="btn btn-outline-secondary btn-xs">
                                                    <i class="fa fa-pencil text-secondary"></i>
                                                </a>&nbsp;

                                                <a href="{{ route('activities.change-status', [$values->id]) }}"
                                                    data-toggle="tooltip" data-placement="bottom" title="Activity Status"
                                                    class="btn btn-outline-info btn-xs">
                                                    <i class="fa fa-info text-info"></i>
                                                </a>&nbsp;

                                                <a href="{{ route('activities.delete', [$project->id, $values->id]) }}"
                                                    data-toggle="tooltip" data-placement="bottom" title="Delete Activity"
                                                    class="btn btn-outline-danger btn-xs delete">
                                                    <i class="fa fa-trash text-danger"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @php $serial++; @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-danger mt-2">No any activity found</div>
                        @endif
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>

    <!-- Modal -->
    <div class="modal fade" id="activityModal" tabindex="-1" role="dialog" aria-labelledby="activityModal"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-600">Activity Information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!--./model-header-->

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table-sm">
                                <tr>
                                    <th width="20%"><span class="font-weight-bold">Project Title</span></th>
                                    <td colspan="3"><span id="projectTitle"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Objective</span></th>
                                    <td colspan="3"><span id="objectiveTitle"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Activity Title</span></th>
                                    <td colspan="3"><span id="activityTitle"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Activity Type</span></th>
                                    <td><span id="activityType"></span></td>

                                    <th><span class="font-weight-bold">Responsible</span></th>
                                    <td><span id="responsible"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Start Date</span></th>
                                    <td><span id="startDate"></span>
                                    </td>

                                    <th><span class="font-weight-bold">End Date</span></th>
                                    <td><span id="endDate"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Budget</span></th>
                                    <td><span id="budget"></span></td>

                                    <th><span class="font-weight-bold">Location</span></th>
                                    <td><span id="location"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Justification</span></th>
                                    <td colspan="3"><span id="justification"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Expected Output</span></th>
                                    <td colspan="3"><span id="expectedOutput"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Expected Outcome</span></th>
                                    <td colspan="3"><span id="expectedOutcome"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Performance Indicator</span></th>
                                    <td colspan="3"><span id="performanceIndicator"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Baseline</span></th>
                                    <td colspan="3"><span id="baseline"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Outcome Targets</span></th>
                                    <td colspan="3"><span id="outcomeTargets"></span>
                                    </td>
                                </tr>

                            </table>
                            <!--./table -->
                        </div>
                        <!--./col-md-12 -->
                    </div>
                    <!--./row -->
                </div>
                <!--./model-body-->

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                        Close
                    </button>
                </div>
                <!--./model-footer-->
            </div>
        </div>
    </div>
    <!--./modal-->
@endsection
