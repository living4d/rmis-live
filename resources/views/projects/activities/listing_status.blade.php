@extends('layouts.admin_app')
@section('content')

    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h4>Activity Status</h4>
                            <ul>
                                <li><a href="{{ route('projects.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('projects') }}">My Projects</a></li>
                                <li><a href="{{ route('projects.activities', $project->id) }}"> Activities</a></li>
                                <li>Change Activity Status</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('projects.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <!--./tabs -->
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link @if (Request::segment(3) == 'listing-status') active @endif"
                                    href="{{ route('activities.listing-status', [$activity->id]) }}">
                                    Activity Status
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link @if (Request::segment(3) == 'change-status') active @endif"
                                    href="{{ route('activities.change-status', [$activity->id]) }}">
                                    Change Activity Status
                                </a>
                            </li>
                        </ul>
                        <!--./tab-lists -->

                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="information" role="tabpanel"
                                aria-labelledby="information-tab">
                                <div class="row p-2 mt-3">
                                    <div class="col-md-12">
                                        <h5 class="text-uppercase">Activity Status : {{ $activity->title }}</h5>
                                        <hr />

                                        @if ($project_status && count($project_status) > 0)
                                            <table id="dt" class="table table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th width="3%">#</th>
                                                        <th width="14%">Status</th>
                                                        <th width="50%">Description</th>
                                                        <th width="20%">Attachment</th>
                                                        <th width="14%">Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $serial = 1; @endphp
                                                    @foreach ($project_status as $values)
                                                        <tr>
                                                            <td>{{ $serial }}</td>
                                                            <td>
                                                                @if ($values->status == 'new')
                                                                    <span class="badge badge-pill badge-warning">NEW</span>
                                                                @elseif ($values->status == 'on-progress')
                                                                    <span class="badge badge-pill badge-info">ON
                                                                        PROGRESS</span>
                                                                @elseif ($values->status == 'done')
                                                                    <span class="badge badge-pill badge-success">DONE</span>
                                                                @elseif ($values->status == 'cancelled')
                                                                    <span
                                                                        class="badge badge-pill badge-danger">CANCELLED</span>
                                                                @endif
                                                            </td>
                                                            <td>{!! $values->description !!}</td>
                                                            <td>
                                                                @if (file_exists(public_path() . '/assets/uploads/documents/' . $values->attachment))
                                                                    <a href="{{ asset('assets/uploads/documents/' . $values->attachment) }}"
                                                                        download>{{ $values->attachment }}</a>
                                                                @endif
                                                            </td>
                                                            <td>{{ date('d-m-Y', strtotime($values->created_at)) }}</td>
                                                        </tr>
                                                        @php $serial++; @endphp
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <div class="alert alert-danger mt-2">No any activity status found</div>
                                        @endif
                                    </div>
                                    <!--./col-md-12 -->
                                </div>
                                <!--./row -->
                            </div>
                            <!--./information -->

                        </div>
                        <!--./tab-content -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
