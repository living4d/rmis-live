@extends('layouts.admin_app')
@section('content')

    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h4>Register New Activity</h4>
                            <ul>
                                <li><a href="{{ route('projects.stats') }}"><i class="fa fa-home"></i> Dashboard</a>
                                </li>
                                <li><a href="{{ url('projects') }}">My Projects</a></li>
                                <li><a href="{{ route('projects.activities', $project->id) }}"> Activities</a></li>
                                <li>Register New Activity</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('projects.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat pure-form">
                            <div class="card-body">
                                <h5 class="">REGISTER NEW ACTIVITY (
                                    <span class="text-right">Available Budget:
                                        {{ number_format($project->remained_fund) }}</span>)
                                </h5>
                                <hr />

                                @if (session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div>
                                @elseif (session()->get('info'))
                                    <div class="alert alert-info">
                                        {{ session()->get('info') }}
                                    </div>
                                @elseif (session()->get('danger'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('danger') }}
                                    </div>
                                @endif

                                {{ Form::open(['url' => route('activities.store', $project->id), 'method="POST" class="form-horizontal"']) }}
                                {{ Form::token() }}
                                {{ Form::hidden('remained_fund', old('remained_fund', $project->remained_fund)) }}

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Objective <span style="color: red;">*</span></label>
                                            @php
                                                $_options = [];
                                            @endphp

                                            @foreach ($objectives as $value)
                                                @php $_options[$value->id] = $value->objective @endphp
                                            @endforeach
                                            @php $_options = ['' => '-- Select --'] + $_options; @endphp
                                            {{ Form::select('objective_id', $_options, old('objective_id'), ['data-placeholder' => '-- Select --', 'class' => 'form-control chosen-select']) }}
                                            <span class="text-danger">{{ $errors->first('objective_id') }}</span>
                                        </div>
                                    </div>
                                    <!--./col -->

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Activity <span style="color: red;">*</span></label>
                                            {{ Form::textarea('title', old('title'), ['class="form-control" rows="2" placeholder="Write title..."']) }}
                                            <span class="text-danger">{{ $errors->first('title') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-md-12 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Activity Type <span style="color: red;">*</span></label>
                                            @php
                                                $_options = [];
                                            @endphp

                                            @foreach ($activity_types as $value)
                                                @php $_options[$value->id] = $value->name @endphp
                                            @endforeach
                                            @php $_options = ['' => '-- Select --'] + $_options; @endphp
                                            {{ Form::select('activity_type_id', $_options, old('activity_type_id'), ['data-placeholder' => '-- Select --', 'class' => 'form-control chosen-select']) }}
                                            <span class="text-danger">{{ $errors->first('activity_type_id') }}</span>
                                        </div>
                                    </div>
                                    <!--./col -->

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Responsible <span style="color: red;">*</span></label>
                                            @php $_option = []; @endphp
                                            @foreach ($partners as $val)
                                                @if ($val->partner_type == 'INTERNAL')
                                                    @php $_option[$val->id] = $val->user->first_name.' '.$val->user->middle_name.' '.$val->user->surname @endphp
                                                @elseif($val->partner_type == 'STUDENT')
                                                    @php $_option[$val->id] = $val->student->first_name.' '.$val->student->middle_name.' '.$val->student->surname @endphp
                                                @elseif($val->partner_type == 'EXTERNAL')
                                                    @php $_option[$val->id] = $val->researcher->full_name @endphp
                                                @endif
                                            @endforeach
                                            {{ Form::select('responsible[]', $_option, old('responsible[]'), ['data-placeholder' => '-- Select --', 'class' => 'form-control chosen-select', 'multiple' => 'multiple']) }}
                                            <span class="text-danger">{{ $errors->first('responsible') }}</span>
                                        </div>
                                    </div>
                                    <!--./col -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Start Date <span style="color: red;">*</span></label>
                                            {{ Form::text('start_date', old('start_date'), ['class="form-control" type="date"', 'min' => date('Y-m-d')]) }}
                                            <span class="text-danger">{{ $errors->first('start_date') }}</span>
                                        </div>
                                    </div>
                                    <!--./col -->

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>End Date <span style="color: red;">*</span></label>
                                            {{ Form::text('end_date', old('end_date'), ['class="form-control" type="date"', 'min' => date('Y-m-d')]) }}
                                            <span class="text-danger">{{ $errors->first('end_date') }}</span>
                                        </div>
                                    </div>
                                    <!--./col -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Budget Amount<span style="color: red;">*</span></label>
                                            {{ Form::text('budget_amount', old('budget_amount'), ['class="form-control" placeholder="Write budget..." id="number_comma"']) }}
                                            <span class="text-danger">{{ $errors->first('budget_amount') }}</span>
                                        </div>
                                    </div>
                                    <!--./col -->

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Location</label>
                                            {{ Form::text('location', old('location'), ['class="form-control" placeholder="Write location..."']) }}
                                            <span class="text-danger">{{ $errors->first('location') }}</span>
                                        </div>
                                    </div>
                                    <!--./col -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Expected Output <span style="color: red;">*</span></label>
                                            {{ Form::textarea('expected_output', old('expected_output'), ['class="form-control" placeholder="Write expected output" rows=3']) }}
                                            {{-- <script>
                                                CKEDITOR.replace('expected_output');
                                            </script> --}}
                                            <span class="text-danger">{{ $errors->first('expected_output') }}</span>
                                        </div>
                                    </div>
                                    <!--./col -->

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Performance Indicator <span style="color: red;">*</span></label>
                                            {{ Form::textarea('performance_indicator', old('performance_indicator'), ['class="form-control" placeholder="Write performance indicator" rows=3']) }}
                                            {{-- <script>
                                                CKEDITOR.replace('performance_indicator');
                                            </script> --}}
                                            <span
                                                class="text-danger">{{ $errors->first('performance_indicator') }}</span>
                                        </div>
                                    </div>
                                    <!--./col -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Baseline</label>
                                            {{ Form::textarea('baseline', old('baseline'), ['class="form-control" placeholder="Write baseline" rows=3']) }}
                                            <span class="text-danger">{{ $errors->first('baseline') }}</span>
                                        </div>
                                    </div>
                                    <!--./col -->

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Targets <span style="color: red;">*</span></label>
                                            {{ Form::textarea('outcome_targets', old('outcome_targets'), ['class="form-control" placeholder="Write outcome targets..." rows=3']) }}
                                            <span class="text-danger">{{ $errors->first('outcome_targets') }}</span>
                                        </div>
                                    </div>
                                    <!--./col -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group pull-right">
                                            <button type="submit" name="save" class="btn btn-primary font-weight-600">Save</button>
                                            <!-- <button type="submit" name="continue" class="btn btn-secondary font-weight-600">Request Fund</button> -->
                                            <a href="{{ route('projects.activities', $project->id) }}"
                                                class="btn btn-danger font-weight-600">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                                <!--./row -->
                                {{ Form::close() }}
                            </div>
                            <!--./card-body -->
                        </div>
                        <!--./card -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
