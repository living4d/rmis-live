@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Projects</h4>
                            <ul>
                                <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li>Projects</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('projects.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <div class="row">
                            <div class="col-md-9">

                            </div>
                            <!--./col-md-9 -->

                            <div class="col-md-3 pull-right">
                                <input type="text" id="myCustomSearchBox" class="form-control"
                                    placeholder="Search details...">
                            </div>
                            <!--./col-md-4 -->
                        </div>
                        <!--./row -->

                        @if (isset($projects) && count($projects) > 0)
                            <table id="dt" class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th width="3%">#</th>
                                        <th width="12%">Reg Number</th>
                                        <th width="24%">Project</th>
                                        <th width="10%">PI</th>
                                        <th width="10%">Budget</th>
                                        <th width="12%">Amount Spent</th>
                                        <th width="12%">% completion</th>
                                        <th width="10%">End Date</th>
                                        <th width="8%">Status</th>
                                        <th style="width: 80px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $serial = 1; @endphp
                                    @foreach ($projects as $val)
                                        <tr>
                                            <td>{{ $serial }}</td>
                                            <td>
                                                <a class="font-weight-600" href="{{ url('projects/' . $val->id) }}">
                                                    {{ strtoupper($val->project_reg_number) }}
                                                </a>
                                            </td>
                                            <td>{{ $val->application->project_title }}</td>
                                            <td>
                                                {{ $val->application->pi->first_name . ' ' . $val->application->pi->middle_name . ' ' . $val->application->pi->surname }}
                                            </td>
                                            <td class="text-right">{{ number_format($val->total_fund) }}</td>
                                            <td></td>
                                            <td></td>
                                            <td>{{ $val->end_at }}</td>
                                            <td>
                                                @if ($val->status == 1)
                                                    <span class="badge badge-pill badge-primary">ACTIVE</span>
                                                @elseif ($val->status == 2)
                                                    <span class="badge badge-pill badge-success">COMPLETED</span>
                                                @endif
                                            </td>

                                            <td>
                                                {{-- <a href="{{ url('projects/' . $val->id) }}" data-toggle="tooltip"
                                                    data-placement="bottom" title="Project Information"
                                                    class="btn btn-outline-primary btn-xs">
                                                    <i class="fa fa-folder-open text-primary"></i>
                                                </a> --}}

                                                <a href="{{ route('projects.generate-report', $val->id) }}"
                                                    data-toggle="tooltip" data-placement="bottom" title="Generate Report"
                                                    class="btn btn-outline-primary btn-xs">
                                                    <i class="fa fa-calendar text-secondary"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @php $serial++; @endphp
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pull-right">
                                {{-- {{ $projects->links() }} --}}
                            </div>
                        @else
                            <div class="alert alert-danger"> No any project found.</div>
                        @endif

                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
