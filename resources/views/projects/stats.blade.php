@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Summary</h4>
                            <ul>
                                <li><a href="{{ route('projects.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li>Summary</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('projects.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="card info-box">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3 col-xs-3">
                                                <i class="fa fa-active-project"></i>
                                            </div>
                                            <div class="col-md-9 col-xs-9 text-right">
                                                <div class="text-large font-weight-600">
                                                    {{ $active_projects }}
                                                </div>

                                                <div class="text-medium text-grey">
                                                    Number of active projects
                                                </div>
                                            </div>
                                            <!--./col-xs-9 -->
                                        </div>
                                        <!--./row-->
                                    </div>
                                    <!--./card-body-->
                                </div>
                                <!--./card-->
                            </div>
                            <!--./col-md-4 -->

                            <div class="col-lg-4 col-md-4">
                                <div class="card info-box">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3 col-xs-3">
                                                <i class="fa fa-total-funding"></i>
                                            </div>
                                            <div class="col-md-9 col-xs-9 text-right">
                                                <div class="text-large font-weight-600">
                                                    0
                                                </div>

                                                <div class="text-medium text-grey">
                                                    Total funding
                                                </div>
                                            </div>
                                            <!--./col-xs-9 -->
                                        </div>
                                        <!--./row-->
                                    </div>
                                    <!--./card-body-->
                                </div>
                                <!--./card-->
                            </div>
                            <!--./col-md-4 -->
                        </div>
                        <!--./row -->
                    </div>
                    <!--./col-md-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
