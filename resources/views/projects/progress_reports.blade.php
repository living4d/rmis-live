@extends('layouts.admin_app')
@section('content')

    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h4>Project Activities</h4>
                            <ul>
                                <li><a href="{{ route('projects.stats') }}"><i class="fa fa-home"></i> Dashboard</a>
                                </li>
                                <li><a href="{{ url('projects') }}"> Projects</a></li>
                                <li>Activities</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('projects.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <!--./tabs -->
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link @if (Request::segment(3) == '') active @endif"
                                    href="{{ route('projects.show', $project->id) }}">Information</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link @if (Request::segment(3) == 'activities') active @endif"
                                    href="{{ route('projects.activities', $project->id) }}">Activities</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link @if (Request::segment(3) == 'fund-requests') active @endif"
                                    href="{{ route('projects.fund-requests', $project->id) }}">Fund Requests</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link @if (Request::segment(3) == 'progress-reports') active @endif" href="#">Progress Reports</a>
                            </li>
                        </ul>
                        <!--./tab-lists -->

                        <div class="tab-content" id="myTabContent">
                            <div class="row p-2">
                                <div class="col-md-12">
                                    @if (session()->get('success'))
                                        <div class="alert alert-success">
                                            {{ session()->get('success') }}
                                        </div><br />
                                    @elseif(session()->get('danger'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('danger') }}
                                        </div><br />
                                    @endif

                                    @if (isset($project->application->objectives) && $project->application->objectives)
                                        <table id="dt" class="table table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="3%" class="text-center">
                                                        #
                                                    </th>
                                                    <th width="30%">Activity</th>
                                                    <th width="18%">Activity Type</th>
                                                    <th width="12%">Start Date</th>
                                                    <th width="12%">End Date</th>
                                                    <th width="12%">Status</th>
                                                    <th style="width: 40px;">Action</th>
                                                </tr>
                                            </thead>
                                            @php
                                                $serial = 1;
                                            @endphp
                                            @foreach ($project->application->objectives as $val)
                                                @if ($val->activities && count($val->activities) > 0)
                                                    <thead>
                                                        <tr>
                                                            <th width="3%" colspan="8">Objective: {{ $val->objective }}
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $serial = 1; @endphp
                                                        @foreach ($val->activities as $values)
                                                            <tr>
                                                                <td class="text-center">{{ $serial }}</td>
                                                                <td>{{ $values->title }}</td>
                                                                <td>{{ $values->type->name ?? null }}</td>
                                                                <td>{{ date('d-m-Y', strtotime($values->start_date)) }}
                                                                </td>
                                                                <td>{{ date('d-m-Y', strtotime($values->end_date)) }}
                                                                </td>
                                                                <td>
                                                                    @if ($values->status == 'new')
                                                                        <span class="badge badge-pill badge-warning">NOT
                                                                            STARTED</span>
                                                                    @elseif ($values->status == 'on-progress')
                                                                        <span class="badge badge-pill badge-info">ON
                                                                            PROGRESS</span>
                                                                    @elseif ($values->status == 'completed')
                                                                        <span
                                                                            class="badge badge-pill badge-success">COMPLETED</span>
                                                                    @elseif ($values->status == 'cancelled')
                                                                        <span
                                                                            class="badge badge-pill badge-danger">CANCELLED</span>
                                                                    @endif
                                                                </td>

                                                                <td>
                                                                    <a href="#activityReportModal" data-backdrop="static"
                                                                        data-keyboard="false" data-toggle="modal"
                                                                        title="Progress Report"
                                                                        data-target="#activityReportModal"
                                                                        data-activity-id="{{ $values->id }}"
                                                                        class="btn btn-outline-primary btn-xs">
                                                                        <i class="fa fa-folder-open text-primary"></i>
                                                                    </a>&nbsp;

                                                                    @if ($values->status != 'completed')
                                                                        <a href="{{ route('activity-reports.change-status', [$values->id]) }}"
                                                                            data-toggle="tooltip" data-placement="bottom"
                                                                            title="Activity Status"
                                                                            class="btn btn-outline-info btn-xs">
                                                                            <i class="fa fa-info-circle text-info"></i>
                                                                        </a>&nbsp;
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            @php $serial++; @endphp
                                                        @endforeach
                                                    </tbody>
                                                @endif
                                            @endforeach
                                        </table>
                                    @else
                                        <div class="alert alert-danger mt-2">No any project objectives</div>
                                    @endif
                                </div>
                                <!--./col-md-10 -->
                            </div>
                            <!--./row -->
                        </div>
                        <!--./tab-content -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>

    <!-- Modal -->
    <div class="modal fade" id="activityReportModal" tabindex="-1" role="dialog" aria-labelledby="activityReportModal"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-600">Activity Progress Report</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!--./model-header-->

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table-sm">
                                <tr>
                                    <th width="20%"><span class="font-weight-bold">Project Title</span></th>
                                    <td colspan="3"><span id="projectTitle"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Objective</span></th>
                                    <td colspan="3"><span id="objectiveTitle"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Activity Title</span></th>
                                    <td colspan="3"><span id="activityTitle"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Start Date</span></th>
                                    <td><span id="startDate"></span>
                                    </td>

                                    <th><span class="font-weight-bold">End Date</span></th>
                                    <td><span id="endDate"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Expected Output</span></th>
                                    <td colspan="3"><span id="expectedOutput"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Performance Indicator</span></th>
                                    <td colspan="3"><span id="performanceIndicator"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Baseline</span></th>
                                    <td colspan="3"><span id="baseline"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Targets</span></th>
                                    <td colspan="3"><span id="outcomeTargets"></span>
                                    </td>
                                </tr>
                            </table>
                            <!--./table -->
                        </div>
                        <!--./col-md-12 -->
                    </div>
                    <!--./row -->


                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-12">
                            <h5 class="text-uppercase">Activity Progress</h5>
                            <hr />
                        </div>
                        <!--./col -->
                    </div>
                    <!--./row -->

                    <table class="table table-striped table-bordered table-sm">
                        <tr>
                            <th width="20%"><span class="font-weight-bold">Status</span></th>
                            <td colspan="3"><span id="status"></span>
                            </td>
                        </tr>

                        <tr>
                            <th width="20%"><span class="font-weight-bold">Output Produced</span></th>
                            <td colspan="3"><span id="outputProduced"></span>
                            </td>
                        </tr>

                        <tr>
                            <th width="20%"><span class="font-weight-bold">Achieved Outcome</span></th>
                            <td colspan="3"><span id="achievedOutcome"></span>
                            </td>
                        </tr>

                        <tr>
                            <th width="20%"><span class="font-weight-bold">Additional Information</span></th>
                            <td colspan="3"><span id="additionalInformation"></span>
                            </td>
                        </tr>

                        <tr>
                            <th width="20%"><span class="font-weight-bold">Attachments</span></th>
                            <td colspan="3"><span id="additionalInformation"></span>
                            </td>
                        </tr>

                    </table>
                </div>
                <!--./model-body-->

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                        Close
                    </button>
                </div>
                <!--./model-footer-->
            </div>
        </div>
    </div>
    <!--./modal-->
@endsection
