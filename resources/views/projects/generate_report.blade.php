@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4> Research Progress Report</h4>
                            <ul>
                                <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li>Research Progress Report</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('projects.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="search-form">

                                    {!! Form::open(['url' => route('projects.generate-report', $project->id), 'method' => 'POST', 'class' => 'form-horizontal']) !!}
                                    {{ Form::token() }}

                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-12">
                                            <div class="form-group">
                                                <label>Report Type</label>
                                                @php
                                                    $_options = [
                                                        'complete-report' => 'Combined Progress Report',
                                                        'date-range' => 'Periodic Progress Report',
                                                    ];
                                                @endphp
                                                {{ Form::select('report_type', $_options, old('report_type'), ['class="form-control"']) }}
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-md-3 -->

                                        <div class="col-md-3 col-sm-3 col-12">
                                            <div class="form-group">
                                                <label>Start Date</label>
                                                {{ Form::date('start_at', old('start_at'), ['placeholder="Write start date..."', 'class="form-control"']) }}
                                                <span class="text-danger">{{ $errors->first('start_at') }}</span>
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-md-3 -->

                                        <div class="col-md-3 col-sm-3 col-12">
                                            <div class="form-group">
                                                <label>End Date</label>
                                                {{ Form::date('end_at', old('end_at'), ['placeholder="Write end date..."', 'class="form-control"']) }}
                                                <span class="text-danger">{{ $errors->first('end_at') }}</span>
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-md-3 -->

                                        <div class="col-md-2 col-sm-2 col-12">
                                            <div class="pull-left">
                                                <div class="form-group">
                                                    <label>&nbsp;&nbsp;&nbsp;</label><br />
                                                    <button style="float:right" type="submit" name="submit"
                                                        class="btn btn-primary"> Generate
                                                    </button>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                        </div>
                                        <!--./col-md-->
                                    </div>
                                    <!--./row -->
                                    {{ Form::close() }}
                                </div>
                                <!--./search-form -->
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->

                        @if (($start_at != null && $end_at != null) || $report_type == 'complete-report')

                            <div class="row mt-3">
                                <div class="col-md-12">

                                    <button class="btn btn-success mb-2" onclick="printDiv('printableArea')">
                                        <i class="fa fa-print" aria-hidden="true"></i> Print
                                    </button>

                                    <div id="printableArea" style="background-color: #FFFFFF; padding:40px;">

                                        <div class="row container">
                                            <div class="col-lg-12">
                                                <div>
                                                    <h3 id="re-title">
                                                        PROJECT PROGRESS REPORT
                                                    </h3>
                                                </div>

                                                <div id="letter-content">

                                                    @if ($report_type == 'date-range')
                                                        <div class="mt-3">
                                                            <h6 class="text-uppercase">
                                                                Period (day/month/year) covered by this report
                                                            </h6>
                                                        </div>

                                                        <table class="">
                                                            <tr>
                                                                <td width="22%" valign="top"><b>Start Date:</b></td>
                                                                <td width="80%">
                                                                    {{ date('d/m/Y', strtotime($start_at)) }}
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td><b>End Date:</b></td>
                                                                <td>
                                                                    {{ date('d/m/Y', strtotime($end_at)) }}
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="mt-3"></div>
                                                    @endif


                                                    <table class="">
                                                        <tr>
                                                            <td width="22%" valign="top"><b>Project Name:</b></td>
                                                            <td width="80%">
                                                                {{ $project->application->project_title }}
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><b>Registration Number:</b></td>
                                                            <td>
                                                                {{ $project->project_reg_number }}
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><b>Starting Date of Project:</b></td>
                                                            <td>
                                                                {{ date('d/m/Y', strtotime($project->start_at)) }}
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><b>Estimated cost of Project (in TZS):</b></td>
                                                            <td>
                                                                {{ number_format($project->total_fund) }}
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><b>Budget amount spent (in TZS):</b></td>
                                                            <td>
                                                                {{ number_format($total_amount_spent) }}
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><b>Remained budget amount (in TZS):</b></td>
                                                            <td>
                                                                @php
                                                                    $remained_amount = $project->total_fund - $total_amount_spent;
                                                                @endphp
                                                                {{ number_format($remained_amount) }}
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><b>PI:</b></td>
                                                            <td>
                                                                {{ $project->application->pi->first_name . ' ' . $project->application->pi->surname }}
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><b>Department:</b></td>
                                                            <td>
                                                                {{ $project->application->pi->department->name }}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>


                                                <div class="mt-3">
                                                    <span class="text-uppercase font-weight-600">
                                                        Activities
                                                    </span>
                                                </div>

                                                <div>
                                                    <table class="table table-hover table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th width="3%">#</th>
                                                                <th width="18%">Activity</th>
                                                                <th width="20%">Output Produced</th>
                                                                <th width="20%">Achieved OutCome</th>
                                                                <th width="16%">Additional Information</th>
                                                                <th width="10%">Status</th>
                                                            </tr>
                                                        </thead>

                                                        @php
                                                            $serial = 1;
                                                        @endphp
                                                        @foreach ($objectives as $val)
                                                            <thead>
                                                                <tr>
                                                                    <th width="3%" colspan="7">Objective:
                                                                        {{ $val->objective }}
                                                                    </th>
                                                                </tr>
                                                            </thead>

                                                            @foreach ($val->activities as $values)
                                                                <tr>
                                                                    <td>{{ $serial }}</td>
                                                                    <td>{{ $values->title }}</td>
                                                                    <td>
                                                                        {{ $values->activityReport->output_produced ?? '' }}
                                                                    </td>
                                                                    <td>
                                                                        {{ $values->activityReport->achieved_outcome ?? '' }}
                                                                    </td>
                                                                    <td>
                                                                        {{ $values->activityReport->additional_information ?? '' }}
                                                                    </td>
                                                                    <td>
                                                                        @if ($values->status == 'new')
                                                                            Not Done
                                                                        @elseif ($values->status == 'on-progress')
                                                                            On Progress
                                                                        @elseif ($values->status == 'done' ||
                                                                            $values->status == 'complete')
                                                                            Completed
                                                                        @elseif ($values->status == 'cancelled')
                                                                            Cancelled
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endforeach
                                                    </table>
                                                </div>

                                                <div class="mt-3">
                                                    <span class="text-uppercase font-weight-600">
                                                        Progress Report Attachments
                                                    </span>
                                                </div>

                                                @foreach ($objectives as $val)
                                                    @foreach ($val->activities as $values)
                                                        @if ($values->activityReport)
                                                            @if ($values->activityReport->attachments)
                                                                @foreach ($values->activityReport->attachments as $att)
                                                                    @if (file_exists(public_path() . '/assets/uploads/documents/' . $att->attachment))
                                                                        <a href="{{ asset('assets/uploads/documents/' . $att->attachment) }}"
                                                                            class="font-weight-600 text-primary" download>
                                                                            {{ $att->filename }}
                                                                        </a>
                                                                        <br />
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            </div>
                                            <!--./col -->
                                        </div>
                                        <!--./row -->
                                    </div>
                                    <!--./print-content -->

                                </div>
                                <!--./col-md-12 -->
                            </div>
                            <!--./row -->
                        @endif
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>

    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
@endsection
