@extends('layouts.admin_app')
@section('content')

@include('call_applications.add_more_researcher')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title">
                        <h4>Research Projects</h4>
                        <ul>
                            <li><a href="#" title="">Dashboard</a></li>
                            <li><a href="{{url('/projects')}}"> Research Projects</a></li>
                            <li>Register New</li>
                        </ul>
                    </div><!-- Page Title -->
                </div>
                <!--./col-lg-10 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    @include('projects.sidebar_menu')
                </div>
                <!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    <div class="card card-flat pure-form">
                        <h5 class="card-header">REGISTER NEW RESEARCH PROJECT</h5>
                        <!--./card-header -->
                        <div class="card-body">

                            @if(session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                            @elseif (session()->get('info'))
                            <div class="alert alert-info">
                                {{ session()->get('info') }}
                            </div>
                            @elseif (session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div>
                            @endif

                            {{ Form::open(['url' => url(''), 'method="POST" class="form-horizontal"']) }}
                            {{ Form::token() }}

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Title of the Research Project<span style="color: red;">*</span></label>
                                        {{Form::textarea('project_title', old('project_title'), ['class="form-control" rows="2" placeholder="Write title..."'])}}
                                        <span class="text-danger">{{ $errors->first('project_title') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->


                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Principal Investigator</label>
                                        {{Form::text('principal_investigator', $user->first_name.' '.$user->middle_name.' '.$user->surname, ['readonly' => '', 'class' => "form-control"])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-4 -->

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        {{Form::text('phone', $user->phone, ['readonly' => '', 'class' => "form-control"])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-4 -->

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Email</label>
                                        {{Form::text('email', $user->email, ['readonly' => '', 'class' => "form-control", 'class' => "form-control"])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-4 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>College/School/Institute</label>
                                        {{Form::text('college', ($college) ? $college->name : 'No college', ['readonly' => '', 'class' => "form-control"])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-4 -->

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Department</label>
                                        {{Form::text('department', ($department) ? $department->name : 'No department', ['readonly' => '', 'class' => "form-control"])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-4 -->

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Faculty</label>
                                        {{Form::text('faculty', ($faculty) ? $faculty->name : 'No faculty', ['readonly' => '', 'class' => "form-control"])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-4 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Research Partner (UDSM Staff)</label>
                                        <?php $_option = []; ?>
                                        @foreach($users as $us)
                                        <?php $_option[$us->id] = $us->first_name.' '.$us->middle_name.' '.$us->surname ?>
                                        @endforeach
                                        {{Form::select('partner_ids[]', $_option, old('partner_ids[]'), ['data-placeholder="   -- Select --" class="form-control chosen-select" multiple'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Research Partner (UDSM Students, If any)</label>
                                        @php ($_option = [])
                                        @foreach($students as $us)
                                        @php ($_option[$us->id] = $us->first_name.' '.$us->middle_name.' '.$us->surname)
                                        @endforeach
                                        {{Form::select('student_ids[]', $_option, old('student_ids[]'), ['data-placeholder="   -- Select --" class="form-control chosen-select" multiple'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Research Partner (Non UDSM Staff, If any)</label>
                                        @php ($_option = [])
                                        @foreach($researchers as $us)
                                        @php ($_option[$us->id] = $us->full_name)
                                        @endforeach
                                        {{Form::select('researcher_ids[]', $_option, old('researcher_ids[]'), ['data-placeholder="   -- Select --" class="form-control chosen-select" multiple'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="mt-4"></div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group pull-right">
                                        <a href="{{url('/projects')}}" class="btn btn-danger btn-xs text-medium">Cancel</a>
                                        <button type="submit" class="btn btn-secondary btn-xs text-medium">Continue</button>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            {{ Form::close() }}
                        </div>
                        <!--./card-body -->
                    </div>
                    <!--./card -->
                </div>
                <!--./col-lg-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection