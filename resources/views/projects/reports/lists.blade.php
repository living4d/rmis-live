@extends('layouts.admin_app')
@section('content')

    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h4>Reports</h4>
                            <ul>
                                <li><a href="{{ route('projects.stats') }}"><i class="fa fa-home"></i> Dashboard</a>
                                </li>
                                <li><a href="{{ url('projects') }}"> Projects</a></li>
                                <li>Reports</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('projects.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <!--./tabs -->
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link @if (Request::segment(3) == '') active @endif"
                                    href="{{ route('projects.show', $project->id) }}">Information</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link @if (Request::segment(3) == 'activities') active @endif" href="#">Activities</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link @if (Request::segment(3) == 'fund-requests') active @endif"
                                    href="{{ route('projects.fund-requests', $project->id) }}">Fund Requests</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link @if (Request::segment(3) == 'activities' && Request::segment(5) == 'change-status') active @endif" href="#">Reports</a>
                            </li>
                        </ul>
                        <!--./tab-lists -->

                        <div class="tab-content" id="myTabContent">
                            <div class="row p-2">
                                <div class="col-md-12">
                                    @if (session()->get('success'))
                                        <div class="alert alert-success">
                                            {{ session()->get('success') }}
                                        </div><br />
                                    @elseif(session()->get('danger'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('danger') }}
                                        </div><br />
                                    @endif

                                    <a href="{{ route('activities.create', $project->id) }}"
                                        class="btn btn-primary btn-sm">
                                        <i class="fa fa-plus"></i> Register New</a>

                                    @if (isset($project->application->objectives) && $project->application->objectives)
                                        {{ Form::open(['url' => route('activities.selected'), 'method' => 'POST', 'class' => 'form-horizontal']) }}
                                        {{ Form::token() }}

                                        <table id="dt" class="table table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="5%" class="text-center">
                                                        <input type="checkbox" id="select_all" />
                                                    </th>
                                                    <th width="40%">Title</th>
                                                    <th width="10%">Budget</th>
                                                    <th width="10%">Start Date</th>
                                                    <th width="10%">End Date</th>
                                                    <th width="10%">Status</th>
                                                    <th style="width: 90px;">Action</th>
                                                </tr>
                                            </thead>
                                            @php
                                                $serial = 1;
                                            @endphp
                                            @foreach ($project->application->objectives as $val)
                                                @if ($val->activities && count($val->activities) > 0)
                                                    <thead>
                                                        <tr>
                                                            <th width="3%" colspan="7">Objective: {{ $val->objective }}
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $serial = 1; @endphp
                                                        @foreach ($val->activities as $values)
                                                            <tr>
                                                                <td class="text-center">
                                                                    @if ($values->status == 'new')
                                                                        <input type="checkbox" class="checkbox"
                                                                            id="id" name="id[]"
                                                                            value="{{ $values->id }}">
                                                                    @endif
                                                                </td>
                                                                <td>{{ $values->title }}</td>
                                                                <td>{{ number_format($values->budget) }}</td>
                                                                <td>{{ date('d-m-Y', strtotime($values->start_date)) }}
                                                                </td>
                                                                <td>{{ date('d-m-Y', strtotime($values->end_date)) }}
                                                                </td>
                                                                <td>
                                                                    @if ($values->status == 'new')
                                                                        <span
                                                                            class="badge badge-pill badge-warning">NEW</span>
                                                                    @elseif ($values->status == 'on-progress')
                                                                        <span class="badge badge-pill badge-info">ON
                                                                            PROGRESS</span>
                                                                    @elseif ($values->status == 'done')
                                                                        <span
                                                                            class="badge badge-pill badge-success">DONE</span>
                                                                    @elseif ($values->status == 'cancelled')
                                                                        <span
                                                                            class="badge badge-pill badge-danger">CANCELLED</span>
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <a href="#activityModal" data-backdrop="static"
                                                                        data-keyboard="false" data-toggle="modal"
                                                                        title="Activity Information"
                                                                        data-target="#activityModal"
                                                                        data-activity-id="{{ $values->id }}"
                                                                        class="btn btn-outline-primary btn-xs">
                                                                        <i class="fa fa-folder-open text-primary"></i>
                                                                    </a>&nbsp;

                                                                    @if ($values->status == 'new')
                                                                        <a href="{{ route('activities.edit', [$project->id, $values->id]) }}"
                                                                            data-toggle="tooltip" data-placement="bottom"
                                                                            title="Edit Activity"
                                                                            class="btn btn-outline-secondary btn-xs">
                                                                            <i class="fa fa-pencil text-secondary"></i>
                                                                        </a>&nbsp;
                                                                    @endif

                                                                    @if ($values->status == 'new')
                                                                        <a href="{{ route('fund-requests.create', $values->id) }}"
                                                                            data-toggle="tooltip" data-placement="bottom"
                                                                            title="Request for fund"
                                                                            class="btn btn-outline-secondary btn-xs">
                                                                            <i class="fa fa-hand-o-up text-secondary"></i>
                                                                        </a>&nbsp;
                                                                    @endif

                                                                    <a href="{{ route('activities.change-status', [$values->id]) }}"
                                                                        data-toggle="tooltip" data-placement="bottom"
                                                                        title="Activity Status"
                                                                        class="btn btn-outline-info btn-xs">
                                                                        <i class="fa fa-info-circle text-info"></i>
                                                                    </a>&nbsp;

                                                                    @if ($values->status == 'new')
                                                                        <a href="{{ route('activities.delete', [$project->id, $values->id]) }}"
                                                                            data-toggle="tooltip" data-placement="bottom"
                                                                            title="Delete Activity"
                                                                            class="btn btn-outline-danger btn-xs delete">
                                                                            <i class="fa fa-trash text-danger"></i>
                                                                        </a>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            @php $serial++; @endphp
                                                        @endforeach
                                                    </tbody>
                                                @endif
                                            @endforeach

                                            <tr>
                                                <td colspan="9">
                                                    <b style="font-size: 12px;">With selected:</b>
                                                    <button type="submit" name="request-for-fund"
                                                        class="btn btn-secondary btn-sm text-medium">
                                                        <i class="fa fa-hand-o-up" aria-hidden="true"></i> Request Fund
                                                    </button>
                                                </td>
                                            </tr>
                                        </table>
                                        {{ Form::close() }}
                                    @else
                                        <div class="alert alert-danger mt-2">No any project objectives</div>
                                    @endif
                                </div>
                                <!--./col-md-10 -->
                            </div>
                            <!--./row -->
                        </div>
                        <!--./tab-content -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>

    <!-- Modal -->
    <div class="modal fade" id="activityModal" tabindex="-1" role="dialog" aria-labelledby="activityModal"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-600">Activity Information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!--./model-header-->

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table-sm">
                                <tr>
                                    <th width="20%"><span class="font-weight-bold">Project Title</span></th>
                                    <td colspan="3"><span id="projectTitle"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Objective</span></th>
                                    <td colspan="3"><span id="objectiveTitle"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Activity Title</span></th>
                                    <td colspan="3"><span id="activityTitle"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Activity Type</span></th>
                                    <td><span id="activityType"></span></td>

                                    <th><span class="font-weight-bold">Responsible</span></th>
                                    <td><span id="responsible"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Start Date</span></th>
                                    <td><span id="startDate"></span>
                                    </td>

                                    <th><span class="font-weight-bold">End Date</span></th>
                                    <td><span id="endDate"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Budget</span></th>
                                    <td><span id="budget"></span></td>

                                    <th><span class="font-weight-bold">Location</span></th>
                                    <td><span id="location"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Expected Output</span></th>
                                    <td colspan="3"><span id="expectedOutput"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Performance Indicator</span></th>
                                    <td colspan="3"><span id="performanceIndicator"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Baseline</span></th>
                                    <td colspan="3"><span id="baseline"></span></td>
                                </tr>

                                <tr>
                                    <th><span class="font-weight-bold">Targets</span></th>
                                    <td colspan="3"><span id="outcomeTargets"></span>
                                    </td>
                                </tr>

                            </table>
                            <!--./table -->
                        </div>
                        <!--./col-md-12 -->
                    </div>
                    <!--./row -->
                </div>
                <!--./model-body-->

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                        Close
                    </button>
                </div>
                <!--./model-footer-->
            </div>
        </div>
    </div>
    <!--./modal-->
@endsection
