@extends('layouts.admin_app')
@section('content')

    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h4>Progress Reports</h4>
                            <ul>
                                <li><a href="{{ route('projects.stats') }}"><i class="fa fa-home"></i> Dashboard</a>
                                </li>
                                <li><a href="{{ url('projects') }}">My Projects</a></li>
                                <li><a href="{{ route('projects.progress-reports', $project->id) }}"> Reports</a></li>
                                <li>Progress Reports</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('projects.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        @if (session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br />
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-flat pure-form">
                                    <div class="card-body">
                                        <h5 class="text-uppercase">Progress Reports : {{ $activity->title }}</h5>
                                        <hr />

                                        {{ Form::open(['url' => route('activity-reports.store-status', $activity->id), 'method="POST" class="form-horizontal" enctype="multipart/form-data"']) }}
                                        {{ Form::token() }}

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Project Title</label>
                                                    {{ Form::textarea('project_title', old('project_title', $activity->project->application->project_title), ['class="form-control" rows="2" readonly=""']) }}
                                                </div>
                                            </div>
                                            <!--./col -->

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Objective</label>
                                                    {{ Form::textarea('objective', old('objective', $activity->objective->objective), ['class="form-control" rows="2" readonly=""']) }}
                                                </div>
                                            </div>
                                            <!--./col -->

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Activity</label>
                                                    {{ Form::textarea('activity', old('project_title', $activity->title), ['class="form-control" rows="2" readonly=""']) }}
                                                </div>
                                            </div>
                                            <!--./col -->

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Expected Output</label>
                                                    {!! Form::textarea('expected_output', old('expected_output', $activity->expected_output), ['class="form-control" rows="2" readonly=""']) !!}
                                                </div>
                                            </div>
                                            <!--./col -->

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Performance Indicator</label>
                                                    {!! Form::textarea('performance_indicator', old('performance_indicator', $activity->performance_indicator), ['class="form-control" rows="2" readonly=""']) !!}
                                                </div>
                                            </div>
                                            <!--./col -->

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Targets</label>
                                                    {!! Form::textarea('targets', old('targets', $activity->outcome_targets), ['class="form-control" rows="2" readonly=""']) !!}
                                                </div>
                                            </div>
                                            <!--./col -->
                                        </div>
                                        <!--./row -->

                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-12">
                                                <h5 class="text-uppercase">Execution Period</h5>
                                                <hr />
                                            </div>
                                            <!--./col -->
                                        </div>
                                        <!--./row -->

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Start Date <span style="color: red;">*</span></label>
                                                    {{ Form::text('start_date', old('start_date', $activity->activityReport->start_at ?? null), ['class="form-control" type="date"', 'max' => date('Y-m-d')]) }}
                                                    <span
                                                        class="text-danger">{{ $errors->first('start_date') }}</span>
                                                </div>
                                            </div>
                                            <!--./col -->

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>End Date <span style="color: red;">*</span></label>
                                                    {{ Form::text('end_date', old('end_date', $activity->activityReport->end_at ?? null), ['class="form-control" type="date"', 'max' => date('Y-m-d')]) }}
                                                    <span class="text-danger">{{ $errors->first('end_date') }}</span>
                                                </div>
                                            </div>
                                            <!--./col -->
                                        </div>
                                        <!--./row -->

                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-12">
                                                <h5 class="text-uppercase">Output</h5>
                                                <hr />
                                            </div>
                                            <!--./col -->
                                        </div>
                                        <!--./row -->

                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-12">
                                                <div class="form-group">
                                                    <label>Output Produced <span style="color: red;">*</span></label>
                                                    {{ Form::textarea('output_produced', old('output_produced', $activity->activityReport->output_produced ?? null), ['class="form-control" rows="3" placeholder="Write output produced..."']) }}
                                                    <span
                                                        class="text-danger">{{ $errors->first('output_produced') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-md-6 -->

                                            <div class="col-lg-6 col-md-6 col-12">
                                                <div class="form-group">
                                                    <label>Achieved Outcome <span style="color: red;">*</span></label>
                                                    {{ Form::textarea('achieved_outcome', old('achieved_outcome', $activity->activityReport->achieved_outcome ?? null), ['class="form-control" rows="3" placeholder="Write achieved outcome..."']) }}
                                                    <span
                                                        class="text-danger">{{ $errors->first('achieved_outcome') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-md-6 -->
                                        </div>
                                        <!--./row -->


                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-12">
                                                <div class="form-group">
                                                    <label>Additional Information </label>
                                                    {{ Form::textarea('additional_information', old('additional_information', $activity->activityReport->additional_information ?? null), ['class="form-control" rows="3" placeholder="Write additional information..."']) }}
                                                    <span
                                                        class="text-danger">{{ $errors->first('additional_information') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-md-12 -->

                                            <div class="col-lg-3 col-md-3 col-12">
                                                <div class="form-group">
                                                    <label>Status <span style="color: red;">*</span></label>
                                                    @php
                                                        $_options = [
                                                            '' => '-- Select --',
                                                            'completed' => 'Completed',
                                                            'on-progress' => 'On Progress',
                                                        ];
                                                    @endphp
                                                    {{ Form::select('status', $_options, old('status', $activity->status), ['class="form-control"']) }}
                                                    <span class="text-danger">{{ $errors->first('status') }}</span>
                                                </div>
                                            </div>
                                            <!--./col -->

                                            <div class="col-lg-3 col-md-3 col-12">
                                                <div class="form-group">
                                                    <label>Attachments (Multiple)</label>
                                                    {{ Form::file('attachments[]', ['class="form-control"', 'multiple' => '']) }}
                                                    <span
                                                        class="text-danger">{{ $errors->first('attachments') }}</span>
                                                </div>
                                            </div>
                                            <!--./col -->


                                        </div>
                                        <!--./row -->

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group pull-right">
                                                    <button type="submit"
                                                        class="btn btn-primary font-weight-600">Save</button>
                                                    <a href="#" class="btn btn-danger font-weight-600">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!--./row -->
                                        {{ Form::close() }}
                                    </div>
                                    <!--./card-body -->
                                </div>
                                <!--./card -->
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
