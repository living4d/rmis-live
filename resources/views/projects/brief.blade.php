@extends('layouts.app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="page-title">
                        <h4>Project Management</h4>
                        <ul>
                            <li><a href="{{url('/')}}" title=""><i class="fa fa-home"></i> Home</a></li>
                            <li>Description</li>
                        </ul>
                    </div><!-- Page Title -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <section class="mt-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-flat">
                            <h5 class="card-header text-uppercase">About Project Management</h5><!--./card-header -->
                            <div class="card-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores cumque
                                    minima nemo repudiandae rerum! Aspernatur at, autem expedita id illum
                                    laudantium molestias officiis quaerat, rem sapiente sint totam velit.
                                    Enim.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores cumque
                                    minima nemo repudiandae rerum! Aspernatur at, autem expedita id illum
                                    laudantium molestias officiis quaerat, rem sapiente sint totam velit.
                                    Enim.</p>
                            </div><!--./card-body -->
                        </div><!--./card -->
                    </div><!--./col-md-4 -->


                </div><!--./row -->
            </div><!--./container -->
        </section><!--./section -->
    </section>
@endsection