@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Users Management</h4>
                            <ul>
                                <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li>Users Management</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row mt-2">
                    <div class="col-lg-2 col-md-2">
                        @include('users.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        @if (session()->get('success'))
                            <div class="mt-2 alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @elseif(session()->get('danger'))
                            <div class="mt-2 alert alert-danger">
                                {{ session()->get('danger') }}
                            </div>
                        @endif

                        <div class="row mt-2">
                            <div class="col-md-12">
                                <div class="search-form">
                                    {!! Form::open(['url' => route('users.lists'), 'method' => 'POST', 'class' => 'form-horizontal']) !!}
                                    {{ Form::token() }}

                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-12">
                                            <div class="form-group">
                                                @php $dep_option = []; @endphp
                                                @if (isset($departments) && $departments)
                                                    @foreach ($departments as $v)
                                                        @php $dep_option[$v->id] = $v->name; @endphp
                                                    @endforeach
                                                @endif
                                                @php $dep_option = ['' => '-- Select --'] + $dep_option; @endphp
                                                {{ Form::select('department_id', $dep_option, old('department_id'), ['class="form-control"']) }}
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-md-6 -->

                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                {{ Form::text('keyword', old('keyword'), ['placeholder="First name or Surname..."', 'class="form-control"']) }}
                                            </div>
                                        </div>
                                        <!--./col-md-4 -->

                                        <div class="col-md-2 col-sm-2 col-12">
                                            <div class="form-group">
                                                <button type="submit" name="filter" class="btn btn-secondary">
                                                    <i class="fa fa-search"></i> Filter
                                                </button>
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-md-2-->
                                    </div>
                                    <!--./row -->
                                    {{ Form::close() }}
                                </div>
                                <!--./search-form -->
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->

                        <div class="row mt-2">
                            <div class="col-md-8">
                                @if (\App\Perms::perm_methods('users', 'create'))
                                    <a href="{{ url('users/create') }}" class="btn btn-primary btn-sm">
                                        <i class="fa fa-plus"></i> Register New</a>&nbsp;

                                    <a href="#" class="btn btn-secondary btn-sm">
                                        <i class="fa fa-cloud-upload"></i> Import</a>&nbsp;
                                @endif
                            </div>
                            <!--./col-md-8 -->

                            <div class="col-md-4">
                                <div class="float-right">
                                    <a title="Export xlsx" class="btn btn-outline-primary btn-sm" href="#">
                                        <i class="fa fa-file-excel-o"></i> XLS</a>
                                </div>
                            </div>
                            <!--./col-md-4 -->
                        </div>
                        <!--./row -->

                        @if (isset($users) && $users)
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th width="3%">#</th>
                                        <th width="20%">Name</th>
                                        <th width="8%">Gender</th>
                                        <th width="12%">Rank</th>
                                        <th width="10%">Username</th>
                                        <th width="12%">Contacts</th>
                                        <th width="10%">Roles</th>
                                        <th style="width: 80px;">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $serial = 1; @endphp
                                    @foreach ($users as $values)
                                        <tr>
                                            <td>{{ $serial }}</td>
                                            <td>{{ $values->first_name . ' ' . $values->middle_name . ' ' . $values->surname }}
                                            </td>
                                            <td>{{ ucfirst($values->gender) }}</td>
                                            <td>{{ ucfirst($values->rank) }}</td>
                                            <td>{{ $values->username }}</td>
                                            <td>{!! $values->email . '<br />' . $values->phone !!}</td>
                                            <td>
                                                @php $i = 1; @endphp
                                                @foreach ($values->roles as $role)
                                                    <small>{{ $role }}</small><br />
                                                    @php $i++; @endphp
                                                @endforeach
                                            </td>
                                            <td>
                                                <a href="{{ route('users.show', $values->id) }}"
                                                    data-toggle="tooltip" data-placement="bottom" title="show user"
                                                    class="btn btn-outline-primary btn-xs">
                                                    <i class="fa fa-folder-open text-primary"></i>
                                                </a>

                                                @if (\App\Perms::perm_methods('users', 'manage'))
                                                    <a href="{{ route('users.manage', $values->id) }}"
                                                        data-toggle="tooltip" data-placement="bottom" title="edit user"
                                                        class="btn btn-outline-secondary btn-xs">
                                                        <i class="fa fa-pencil text-secondary"></i>
                                                    </a>
                                                @endif

                                                @if (\App\Perms::perm_methods('users', 'edit'))
                                                    <a href="{{ route('users.edit', $values->id) }}"
                                                        data-toggle="tooltip" data-placement="bottom" title="reset user"
                                                        class="btn btn-outline-secondary btn-xs">
                                                        <i class="fa fa-wrench text-secondary"></i>
                                                    </a>
                                                @endif

                                                @if (\App\Perms::perm_methods('users', 'delete'))
                                                    <a onClick="return confirm('Delete user?')"
                                                        href="{{ route('users.delete', $values->id) }}"
                                                        data-toggle="tooltip" data-placement="bottom" title="delete user"
                                                        class="btn btn-outline-danger btn-xs">
                                                        <i class="fa fa-trash text-danger"></i> </a>
                                                @endif
                                            </td>
                                        </tr>
                                        @php $serial++; @endphp
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pull-right">
                                {{ $users->links() }}
                            </div>
                            <!--./pull-right -->
                        @else
                            <div class="alert alert-danger"> No any user registered found.</div>
                        @endif
                    </div>
                    <!--./col-lg-9 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
