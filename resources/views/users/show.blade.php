@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>{!! $user->first_name . ' ' . $user->middle_name . ' ' . $user->surname !!} Profile</h4>
                            <ul>
                                <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li>Profile</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('users.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="text-uppercase">User Profile</h5>
                                <hr />

                                <table class="table table-stripped table-bordered">
                                    <tr>
                                        <td colspan="4"><b>Personal Information</b></td>
                                    </tr>
                                    <tr>
                                        <th>Fullname</th>
                                        <td>
                                            <span>{{ $user->first_name . ' ' . $user->middle_name . ' ' . $user->surname }}</span>
                                        </td>

                                        <th>Gender</th>
                                        <td>
                                            <span>{{ ucfirst($user->gender) }}</span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>Email</th>
                                        <td>{{ $user->email }}</td>

                                        <th>Phone</th>
                                        <td>{{ $user->phone }}</td>
                                    </tr>

                                    <tr>
                                        <td colspan="4"><b>Work Information</b></td>
                                    </tr>

                                    <tr>
                                        <th>Institution</th>
                                        <td>{{ $user->academic_qualification }}</td>

                                        <th>Specialization</th>
                                        <td>{{ $user->specialization }}</td>
                                    </tr>

                                    <tr>
                                        <th>Research Areas</th>
                                        <td>{{ $research_areas }}</td>

                                        <th>Curricullum Vitae</th>
                                        <td>
                                            @if ($user->cv != null || $user->cv != '')
                                                @if (file_exists(public_path() . '/assets/uploads/cv/' . $user->cv))
                                                    <a href="{{ public_path() . '/assets/uploads/cv/' . $user->cv }}"
                                                        class="btn btn-success btn-xs" download=""><i
                                                            class="fa fa-upload"></i> Download</a>
                                                @endif
                                            @else
                                                <span class="red">No attached CV</span>
                                            @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>Experience</th>
                                        <td colspan="3">{{ $user->experience }}</td>
                                    </tr>

                                    <tr>
                                        <td colspan="4"><b>Other Information</b></td>
                                    </tr>

                                    <tr>
                                        <th>College</th>
                                        <td>
                                            @if ($college)
                                                {{ $college->name }}
                                            @endif
                                        </td>

                                        <th>Department</th>
                                        <td>
                                            @if ($department)
                                                {{ $department->name }}
                                            @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>Username</th>
                                        <td>{{ $user->username }}</td>
                                    </tr>
                                </table>
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
