<div id="left_menu">
    <ul id="nav">
        <?php
        if (Auth::user()->hasRole('admin') || \App\Perms::perm_methods('users', 'lists'))
            echo '<li><a href="' . route('users.lists') . '">Users</a></li>';

        if (Auth::user()->hasRole('admin') || \App\Perms::perm_methods('roles', 'lists'))
            echo '<li><a href="' . url('roles') . '">Roles</a></li>';
        ?>
    </ul>
</div>
