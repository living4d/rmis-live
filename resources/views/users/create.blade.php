@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Register New</h4>
                            <ul>
                                <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="{{ url('users') }}" title=""> Users</a></li>
                                <li>Register New</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('users.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat pure-form">
                            <div class="card-body">
                                <h5 class="text-uppercase">Register New User</h5>
                                <hr />

                                @if (session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div><br />
                                @endif

                                {{ Form::open(['url' => route('users.store'), 'method' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
                                {{ Form::token() }}

                                <div class="row">
                                    <div class="col-lg-12">
                                        <h5>Personal Information</h5>
                                    </div>
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>First Name <span class="red">*</span></label>
                                            {{ Form::text('first_name', old('first_name'), ['class="form-control"', 'placeholder="Write a first name..."']) }}
                                            <span class="text-danger">{{ $errors->first('first_name') }}</span>
                                        </div>
                                    </div>
                                    <!--./col-lg-12 -->

                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>Middle name</label>
                                            {{ Form::text('middle_name', old('middle_name'), ['class="form-control"', 'placeholder="Middle name..."']) }}
                                        </div>
                                    </div>
                                    <!--./col-lg-12 -->

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Surname <span class="red">*</span></label>
                                            {{ Form::text('surname', old('surname'), ['class="form-control"', 'placeholder="Write a surname..."']) }}
                                            <span class="text-danger">{{ $errors->first('surname') }}</span>
                                        </div>
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Email <span class="red">*</span></label>
                                            {{ Form::email('email', old('email'), ['class="form-control"', 'placeholder="Write email..."']) }}
                                            <span class="text-danger">{{ $errors->first('email') }}</span>
                                        </div>
                                    </div>
                                    <!--./col-lg-12 -->

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Phone <span class="red">*</span></label>
                                            {{ Form::text('phone', old('phone'), ['class="form-control"', 'placeholder="Write a phone..."']) }}
                                            <span class="text-danger">{{ $errors->first('phone') }}</span>
                                        </div>
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Gender<span style="color: red;">*</span></label>
                                            {{ Form::select('gender', ['' => '-- Select --', 'male' => 'Male', 'female' => 'Female'], old('gender'), ['class' => 'form-control']) }}
                                            <span class="text-danger">{{ $errors->first('gender') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-12 -->
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Rank/Title</label>
                                            {{ Form::select('rank', ['' => '-- Select --', 'Professor' => 'Professor', 'Associate Professor' => 'Associate Professor', 'Senior Lecturer' => 'Senior Lecturer', 'Lecturer' => 'Lecturer', 'Assistant Lecturer' => 'Assistant Lecturer', 'Tutorial Assistant' => 'Tutorial Assistant'], old('rank'), ['class' => 'form-control']) }}
                                            <span class="text-danger">{{ $errors->first('rank') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                </div>
                                <!--./row -->


                                <div class="row mt-3">
                                    <div class="col-lg-12">
                                        <h5>Education and Work Information</h5>
                                    </div>
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Institution (For external evaluators)</label>
                                            {{ Form::text('institution', old('institution'), ['class="form-control"', 'placeholder="Write institution..."']) }}
                                            <span class="text-danger">{{ $errors->first('institution') }}</span>
                                        </div>
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Academic Qualification </label>
                                            {{ Form::textarea('academic_qualification', old('academic_qualification'), ['class="form-control"', 'placeholder="Write academic qualification..."', 'rows' => 3]) }}
                                            <span
                                                class="text-danger">{{ $errors->first('academic_qualification') }}</span>
                                        </div>
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Specialization </label>
                                            {{ Form::textarea('specialization', old('specialization'), ['class="form-control"', 'placeholder="Write area specialization..."', 'rows' => 3]) }}
                                            <span class="text-danger">{{ $errors->first('specialization') }}</span>
                                        </div>
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Experience </label>
                                            {{ Form::textarea('experience', old('experience'), ['class="form-control"', 'placeholder="Write your experience..."', 'rows' => 3]) }}
                                            <script>
                                                CKEDITOR.replace('experience');
                                            </script>
                                            <span class="text-danger">{{ $errors->first('experience') }}</span>
                                        </div>
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Research Area(s)</label>
                                            @php($_option = [])
                                                @foreach ($research_areas as $ra)
                                                    @php($_option[$ra->id] = $ra->name)
                                                    @endforeach
                                                    {{ Form::select('research_area_ids[]', $_option, old('research_area_ids[]'), ['data-placeholder="   -- Select --" class="form-control chosen-select" multiple']) }}
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-12 -->
                                        </div>
                                        <!--./row -->

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Curriculum Vitae (CV) </label>
                                                    {{ Form::file('attachment', ['class="form-control"']) }}
                                                    <span class="text-danger">{{ $errors->first('attachment') }}</span>
                                                </div>
                                            </div>
                                            <!--./col-lg-12 -->
                                        </div>
                                        <!--./row -->

                                        <div class="row mt-3">
                                            <div class="col-lg-12">
                                                <h5>Login Information</h5>
                                            </div>
                                        </div>
                                        <!--./row -->

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Username <span class="red">*</span></label>
                                                    {{ Form::text('username', old('username'), ['class="form-control"', 'placeholder="Write username..." autocomplete="off"']) }}
                                                    <span class="text-danger">{{ $errors->first('username') }}</span>
                                                </div>
                                            </div>
                                            <!--./col-lg-12 -->

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Level <span class="red">*</span></label>
                                                    <?php
                                                    $level_option = [];
                                                    if (isset($levels) && count($levels) > 0) {
                                                    foreach ($levels as $level) {
                                                    $level_option[$level->id] = $level->name;
                                                    }
                                                    }
                                                    $level_option = ['' => '-- Select --'] + $level_option;
                                                    ?>
                                                    {{ Form::select('level_id', $level_option, old('level_id'), ['class' => 'form-control']) }}
                                                    <span class="text-danger">{{ $errors->first('level_id') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-6 -->
                                        </div>
                                        <!--./row -->

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>College </label>
                                                    <?php
                                                    $college_option = [];
                                                    if (isset($colleges) && count($colleges) > 0) {
                                                    foreach ($colleges as $college) {
                                                    $college_option[$college->id] = $college->name;
                                                    }
                                                    }
                                                    $college_option = ['' => '-- Select --'] + $college_option;
                                                    ?>
                                                    {{ Form::select('college_id', $college_option, old('college_id'), ['class' => 'form-control', 'id' => 'college_id', 'onchange' => 'suggest_departments()']) }}
                                                    <span class="text-danger">{{ $errors->first('college_id') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-6 -->

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Department </label>
                                                    <?php
                                                    $department_option = [];
                                                    if (isset($departments) && count($departments) > 0) {
                                                    foreach ($departments as $dp) {
                                                    $department_option[$dp->id] = $dp->name;
                                                    }
                                                    }
                                                    $department_option = ['' => '-- Select --'] + $department_option;
                                                    ?>
                                                    {{ Form::select('department_id', $department_option, old('department_id'), ['class' => 'form-control', 'id' => 'department_id']) }}
                                                    <span class="text-danger">{{ $errors->first('department_id') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-12 -->
                                        </div>
                                        <!--./row -->

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Faculty </label>
                                                    <?php
                                                    $_option = [];
                                                    if (isset($faculties) && count($faculties) > 0) {
                                                    foreach ($faculties as $fc) {
                                                    $_option[$fc->id] = $fc->name;
                                                    }
                                                    }
                                                    $_option = ['' => '-- Select --'] + $_option;
                                                    ?>
                                                    {{ Form::select('faculty_id', $_option, old('faculty_id'), ['class' => 'form-control', 'id' => 'faculty_id']) }}
                                                    <span class="text-danger">{{ $errors->first('faculty_id') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-12 -->
                                        </div>
                                        <!--./row -->

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Roles <span class="red">*</span></label>
                                                    <table>
                                                        <tr>
                                                            @php($serial = 0)
                                                                @foreach ($roles as $role)
                                                                    @if ($serial % 4 == 0)
                                                            </tr>
                                                            <tr>
                                                                @endif
                                                                <td>
                                                                    {{ Form::checkbox('role_ids[]', $role->id, old('role_ids[]')) }}
                                                                    <label>{{ $role->description }}</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                </td>
                                                                @php($serial++)
                                                                    @endforeach
                                                                </tr>
                                                            </table>
                                                            <span class="text-danger">{{ $errors->first('role_ids') }}</span>
                                                        </div>
                                                    </div>
                                                    <!--./col-lg-12 -->
                                                </div>
                                                <!--./row -->

                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label>Password <span class="red">*</span></label>
                                                            {{ Form::password('password', ['class="form-control"', 'placeholder="Write password..."']) }}
                                                            <span class="text-danger">{{ $errors->first('password') }}</span>
                                                        </div>
                                                    </div>
                                                    <!--./col-lg-12 -->

                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label>Confirm Password <span class="red">*</span></label>
                                                            {{ Form::password('password_confirm', ['class="form-control"', 'placeholder="Confirm password..."']) }}
                                                            <span class="text-danger">{{ $errors->first('password_confirm') }}</span>
                                                        </div>
                                                    </div>
                                                    <!--./col-lg-12 -->
                                                </div>
                                                <!--./row -->

                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="float-right">
                                                            <div class="form-group">
                                                                <button type="submit"
                                                                    class="btn btn-secondary font-weight-600">Save</button>
                                                                <a href="{{ url('users') }}"
                                                                    class="btn btn-danger font-weight-600">Cancel</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--./col-lg-12 -->
                                                </div>
                                                <!--./row -->
                                                {{ Form::close() }}
                                            </div>
                                            <!--./card-body -->
                                        </div>
                                        <!--./card -->
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->
                            </div>
                            <!--./container -->
                        </div>
                        <!--./page-content -->
                    </section>
                @endsection
