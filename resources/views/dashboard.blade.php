@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <h4>Dashboard</h4>
                    <ul>
                        <li><a href="{{url('/')}}" title=""> Dashboard</a></li>
                        <li>Statistics</li>
                    </ul>
                </div><!-- Page Title -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <!-- sidebar -->
                <div class="col-lg-12 col-md-12">
                    <div class="card card-flat">
                        <h5 class="card-header text-uppercase">Statistics</h5>
                        <!--./card-header -->
                        <div class="card-body">


                        </div>
                        <!--./card-body -->
                    </div>
                    <!--./card -->
                </div>
                <!--./col-lg-9 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection