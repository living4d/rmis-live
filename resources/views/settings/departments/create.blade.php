@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Register New</h4>
                            <ul>
                                <li><a href="#" title="">Dashboard</a></li>
                                <li><a href="{{url('/departments')}}" title=""> Departments</a></li>
                                <li>Register New</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('settings.sidebar_menu')
                    </div><!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat">
                            <h5 class="card-header">Register New</h5><!--./card-header -->
                            <div class="card-body">

                                @if(session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div><br/>
                                @endif

                                {{ Form::open(['url' => route('departments.store'), 'method' => 'POST', 'class' => 'form-horizontal']) }}
                                {{ Form::token() }}

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>College/School <span class="red">*</span></label>
                                            @php $_options = []; @endphp
                                            @if (isset($colleges) && $colleges)
                                                @foreach($colleges as $college)
                                                    @php $_options[$college->id] = $college->name @endphp
                                                @endforeach
                                            @endif
                                            @php $_options = ['' => '-- Select --'] + $_options; @endphp
                                            {{ Form::select('college_id', $_options, old('college_id'), ['class="form-control"']) }}
                                            <span class="text-danger">{{ $errors->first('college_id') }}</span>
                                        </div>
                                    </div><!--./col-lg-6 -->

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Name <span class="red">*</span></label>
                                            {{ Form::text('name', old('name'), ['class="form-control"', 'placeholder="Write a department name..."']) }}
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                        </div>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Short Name <span class="red">*</span></label>
                                            {{ Form::text('short_name', old('short_name'), ['class="form-control"', 'placeholder="Write a department short name..."']) }}
                                            <span class="text-danger">{{ $errors->first('short_name') }}</span>
                                        </div>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-secondary btn-xs text-medium">Register</button>
                                            <a href="{{url('/departments')}}" class="btn btn-danger btn-xs text-medium">Cancel</a>
                                        </div>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->
                                {{ Form::close() }}
                            </div><!--./card-body -->
                        </div><!--./card -->
                    </div><!--./col-lg-12 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
@endsection
