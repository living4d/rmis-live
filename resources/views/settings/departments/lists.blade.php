@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Departments</h4>
                            <ul>
                                <li><a href="#" title="">Dashboard</a></li>
                                <li>Departments</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div><!--./col-md-10 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('settings.sidebar_menu')
                    </div><!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        @if(session()->get('success'))
                            <div class="mt-2 alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @elseif(session()->get('danger'))
                            <div class="mt-2 alert alert-danger">
                                {{ session()->get('danger') }}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-md-9">
                                <a href="{{url('departments/create')}}" class="btn btn-primary btn-xs text-medium">
                                    <i class="fa fa-plus"></i> Register New</a>
                            </div><!--./col-md-9 -->

                            <div class="col-md-3 pull-right">
                                <input type="text" id="myCustomSearchBox" class="form-control"
                                       placeholder="Search details...">
                            </div><!--./col-md-4 -->
                        </div><!--./row -->

                        @if(isset($departments) && $departments)
                            <table id="dt" class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th width="3%">#</th>
                                    <th width="50%">Name</th>
                                    <th width="20%">Short Name</th>
                                    <th width="20%">College</th>
                                    <th width="6%">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $serial = 1?>
                                @foreach($departments as $values)
                                    <tr>
                                        <td><?= $serial ?></td>
                                        <td>{{$values->name}}</td>
                                        <td>{{$values->short_name}}</td>
                                        <td>{{$values->college->short_name}}</td>
                                        <td><a href="{{route('departments.edit',$values->id)}}"><i
                                                        class="fa fa-pencil-square-o text-primary"></i> </a>&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <?php $serial++; ?>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-danger"> No any department created.</div>
                        @endif
                    </div><!--./col-lg-9 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
@endsection
