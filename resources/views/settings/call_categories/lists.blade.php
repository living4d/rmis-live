@extends('admin.layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Call Categories</h4>
                            <ul>
                                <li><a href="{{url('/')}}" title=""><i class="fa fa-home"></i> Dashboard</a></li>
                                <li>Call Categories</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div><!--./col-md-10 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                @include('admin.settings.menu')

                <div class="row mt-2">
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-flat">
                            <h5 class="card-header text-uppercase">Categories</h5><!--./card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{route('call_categories.create')}}"
                                           class="btn btn-outline-primary btn-xs">Create
                                            New</a>
                                    </div><!--./col-md-12 -->
                                </div><!--./row -->

                                @if(session()->get('success'))
                                    <div class="mt-2 alert alert-success">
                                        {{ session()->get('success') }}
                                    </div>
                                @elseif(session()->get('danger'))
                                    <div class="mt-2 alert alert-danger">
                                        {{ session()->get('danger') }}
                                    </div>
                                @endif

                                @if(isset($categories) && $categories)
                                    <table id="myTable" class="table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="3%">#</th>
                                            <th width="80%">Name</th>
                                            <th width="6%">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $serial = 1?>
                                        @foreach($categories as $values)
                                            <tr>
                                                <td><?= $serial ?></td>
                                                <td>{{$values->name}}</td>
                                                <td><a href="{{route('call_categories.edit',$values->id)}}"><i
                                                                class="fa fa-pencil fa-lg"></i> </a>&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <?php $serial++; ?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="alert alert-danger"> No any category created.</div>
                                @endif
                            </div><!--./card-body -->
                        </div><!--./card -->
                    </div><!--./col-lg-9 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
@endsection