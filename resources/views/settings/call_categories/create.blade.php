@extends('admin.layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="page-title">
                        <h4>Create New</h4>
                        <ul>
                            <li><a href="{{url('/')}}" title=""><i class="fa fa-home"></i> Home</a></li>
                            <li><a href="{{url('/call_categories')}}" title=""> Call Categories</a></li>
                            <li>Create New</li>
                        </ul>
                    </div><!-- Page Title -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <!-- sidebar -->
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-flat">
                            <h5 class="card-header">Create New</h5><!--./card-header -->
                            <div class="card-body">

                                @if(session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div><br/>
                                @endif

                                {{ Form::open(['url' => route('call_categories.store'), 'method' => 'POST', 'class' => 'form-horizontal']) }}
                                {{ Form::token() }}

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Name <span class="red">*</span></label>
                                            {{ Form::text('name', old('name'), ['class="form-control"', 'placeholder="Write a category name..."']) }}
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                        </div>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Description <span class="red">*</span></label>
                                            {{ Form::textarea('description', old('description'), ['class="form-control"', 'placeholder="Write description..."']) }}
                                            <script>
                                                CKEDITOR.replace('description');
                                            </script>
                                            <span class="text-danger">{{ $errors->first('description') }}</span>
                                        </div>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-secondary btn-xs">Create</button>
                                            <a href="{{url('/call_categories')}}"
                                               class="btn btn-danger btn-xs">Cancel</a>
                                        </div>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->
                                {{ Form::close() }}
                            </div><!--./card-body -->
                        </div><!--./card -->
                    </div><!--./col-lg-12 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
@endsection