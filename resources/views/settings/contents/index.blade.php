@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <h4>Edit Contents</h4>
                        <ul>
                            <li><a href="#" title="">Dashboard</a></li>
                            <li>Edit Contents</li>
                        </ul>
                    </div><!-- Page Title -->
                </div>
                <!--./col-md-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    @include('settings.sidebar_menu')
                </div>
                <!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    <div class="card card-flat">
                        <h5 class="card-header">Edit Content</h5>
                        <!--./card-header -->
                        <div class="card-body">

                            @if(session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                            @endif

                            {{ Form::open(['url' => url('contents/update', $content->id), 'class' => 'form-horizontal']) }}
                            @method('PUT')
                            {{ Form::token() }}

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Title <span class="red">*</span></label>
                                        {{ Form::textarea('name', $content->title, ['class="form-control"', 'placeholder="Write a title..."', 'rows=2']) }}
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div><!--./col-lg-12 -->
                            </div><!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Content <span class="red">*</span></label>
                                        {{ Form::textarea('content', $content->content, ['class="form-control"', 'placeholder="Write content here..."']) }}
                                        <script>
                                            CKEDITOR.replace('content');
                                        </script>
                                        <span class="text-danger">{{ $errors->first('content') }}</span>
                                    </div>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-secondary btn-xs text-medium">Edit</button>
                                        <a href="{{url('/research_areas')}}"
                                            class="btn btn-danger btn-xs text-medium">Cancel</a>
                                    </div>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->
                            {{ Form::close() }}
                        </div>
                        <!--./card-body -->
                    </div>
                    <!--./card -->
                </div>
                <!--./col-lg-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection