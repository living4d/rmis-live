<div id="left_menu">
    <ul id="nav">
        <?php
        echo '<li><a href="' . url('/colleges') . '">Colleges</a></li>';
        echo '<li><a href="' . url('/departments') . '">Departments</a></li>';
        echo '<li><a href="#">Faculties</a></li>';
        echo '<li><a href="' . url('/clusters') . '">Clusters</a></li>';
        echo '<li><a href="' . url('/research_areas') . '">Research Areas</a></li>';
        echo '<li><a href="' . url('/contents') . '">CMS</a></li>';
        ?>
    </ul>
</div>
