<div class="row">
    <div class="col-md-12">
        <div class="dropdown">
            <button class="btn btn-outline-primary dropdown-toggle" type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Settings
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="{{url('/colleges')}}">Colleges</a>
                <a class="dropdown-item" href="{{url('/departments')}}">Departments</a>
                <a class="dropdown-item" href="#">Faculties</a>
                {{--<a class="dropdown-item" href="{{url('/call_categories')}}">Call Categories</a>--}}
                <a class="dropdown-item" href="{{url('/clusters')}}">Clusters</a>
                <a class="dropdown-item" href="{{url('/research_areas')}}">Research Areas</a>
            </div>
        </div><!--.actions -->
    </div><!--./col-md-12 -->
</div><!--./row -->