@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Edit Research Area</h4>
                            <ul>
                                <li><a href="#" title="">Dashboard</a></li>
                                <li><a href="{{url('/research_areas')}}" title=""> Research Areas</a></li>
                                <li>Edit Research Area</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div><!--./col-md-12 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('settings.sidebar_menu')
                    </div><!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat">
                            <h5 class="card-header">Create New</h5><!--./card-header -->
                            <div class="card-body">

                                @if(session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div><br/>
                                @endif

                                {{ Form::open(['url' => route('research_areas.update', $research_area->id), 'class' => 'form-horizontal']) }}
                                @method('PUT')
                                {{ Form::token() }}

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Cluster <span class="red">*</span></label>
                                            @php $_options = []; @endphp
                                            @if (isset($clusters) && $clusters)
                                                @foreach($clusters as $cluster)
                                                    @php $_options[$cluster->id] = $cluster->name @endphp
                                                @endforeach
                                            @endif
                                            @php $_options = ['' => '-- Select --'] + $_options; @endphp
                                            {{ Form::select('cluster_id', $_options, old('cluster_id', $research_area->cluster_id), ['class="form-control"']) }}
                                            <span class="text-danger">{{ $errors->first('cluster_id') }}</span>
                                        </div>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Name <span class="red">*</span></label>
                                            {{ Form::textarea('name', $research_area->name, ['class="form-control"', 'placeholder="Write a research area..."', 'rows=3']) }}
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                        </div>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-secondary btn-xs text-medium">Edit</button>
                                            <a href="{{url('/research_areas')}}"
                                               class="btn btn-danger btn-xs text-medium">Cancel</a>
                                        </div>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->
                                {{ Form::close() }}
                            </div><!--./card-body -->
                        </div><!--./card -->
                    </div><!--./col-lg-12 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
@endsection
