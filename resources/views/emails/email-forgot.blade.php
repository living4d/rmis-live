<h1>Password Reset Email</h1>
Dear {!! $user_rec->surname !!}<br>
Your Username is <b>{!! $user_rec->username !!}</b><br>
<p>{!! $body !!}</p>
<br>
<a href="{{$action_link}}">Reset Password</a>