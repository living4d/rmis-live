@extends('layouts.app')

@section('content')
<section class="page-wrapper mt-4">
    <div class="page-content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 col-sm-6 col-xs-12 col-lg-6">
                    <div class="card card-flat pure-form">
                        <div class="card-body">
                            <h5 class="text-uppercase">Change Password</h5>
                            <hr/>

                            <p><b>Hint: </b> Required to change password on first login</p>

                            @if(session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                            @endif

                            {{ Form::open(['url' => url('change-password'), 'method="POST" class="form-horizontal"']) }}
                            {{ Form::token() }}

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Current Password <span class="red">*</span></label>
                                        {{ Form::password('current_password', ['placeholder="Write a current password..."', 'class="form-control"']) }}
                                        <span class="text-danger">{{ $errors->first('current_password') }}</span>
                                    </div>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>New Password <span class="red">*</span></label>
                                        {{ Form::password('new_password', ['placeholder="Write a new password..."', 'class="form-control"']) }}
                                        <span class="text-danger">{{ $errors->first('new_password') }}</span>
                                    </div>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Confirm Password <span class="red">*</span> </label>
                                        {{ Form::password('new_password_confirm', ['placeholder="Confirm password..."', 'class="form-control"']) }}
                                        <span class="text-danger">{{ $errors->first('new_password_confirm') }}</span>
                                    </div>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <button type="submit"
                                            class="btn btn-secondary btn-xs text-medium">Change</button>
                                    </div>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->
                            {{ Form::close() }}
                        </div>
                        <!--./card-body -->
                    </div>
                    <!--./card -->
                </div>
                <!--./col-lg-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection