@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4> TESTS</h4>
                            <ul>
                                <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li>Tests</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
{{--                    <div class="col-lg-2 col-md-2">--}}
{{--                        @include('publications.side_menu')--}}
{{--                    </div>--}}
                    <!--col-md-2 -->
                </div>
                <!--./row -->
                <br />
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Tests Data</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            @if(count($data) > 0)
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>NAME</th>
                                    <th>EMAIL</th>
                                    <th>STATUS</th>
                                </tr>
                                </thead>
                                @foreach($data as $value)
                                    <tr>
                                        <td>{{ $value->id }}</td>
                                        <td>{{ $value->name }}</td>
                                        <td>{{ $value->email }}</td>
                                        <td>{{ $value->status }}</td>
                                    </tr>
                                @endforeach
                                @else
                                    <td colspan="3">No data found</td>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection