@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4> Import Excel TESTS status</h4>
                            <ul>
                                <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li>Import Excel Tests Status</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
{{--                    <div class="col-lg-2 col-md-2">--}}
{{--                        @include('publications.side_menu')--}}
{{--                    </div>--}}
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <div class="card">
                                    <div class="card-header">IMPORT EXCEL</div>
                                    <div class="card-body">
                                        @if($message = Session::get('success'))
                                            <div class="alert alert-success alert-block">
                                                <button type="button" class="close" data-dismiss="alert">x</button>
                                                <strong> {{ $message }}</strong>
                                            </div>
                                        @endif
                                        @if(isset($errors) && $errors->any())
                                            <div class="alert alert-danger">
                                                @foreach($errors->all() as $error)
                                                    {{ $error }}
                                                @endforeach
                                            </div>
                                        @endif
                                            <form method="post" enctype="multipart/form-data" action="{{ route('import_test') }}">
                                            @csrf
                                            <div class="form-group">
                                                <table class="table">
                                                    <tr>
                                                        <td width="30%"><input type="file" name="excel_file" /></td>
                                                        <td width="30%" align="left"><input type="submit" name="upload" class="btn btn-primary" value="Import"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </form>
                                    </div>
                                    <br/>

                                </div>
                                <!--./search-form -->
                            </div>
                            <!--./col-md-12 -->
                        </div>
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
                <br />
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Tests Data with status</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            @if(count($data) > 0)
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>NAME</th>
                                    <th>STATUS</th>
                                </tr>
                                </thead>
                                @foreach($data as $value)
                                    <tr>
                                        <td>{{ $value->id }}</td>
                                        <td>{{ $value->name }}</td>
                                        <td>{{ $value->t_status }}</td>
                                    </tr>
                                @endforeach
                                @else
                                    <td colspan="3">No data found</td>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection