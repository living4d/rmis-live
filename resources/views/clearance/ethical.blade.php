@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <h4>Ethical Clearance Form</h4>
                </div><!-- Page Title -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <!-- sidebar -->
                <div class="col-lg-2 col-md-2">
                    @include('clearance.side_menu')
                </div>
                <!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div><br />
                    @elseif(session()->get('danger'))
                    <div class="alert alert-danger">
                        {{ session()->get('danger') }}
                    </div><br />
                    @endif

                    <div class="card card-flat">
                        <h5 class="card-header">ETHICAL CLEARANCE FORM</h5>
                        <!--./card-header -->
                        <div class="card-body">
                            <div class="row mt-3">
                                <div class="col-lg-12">
                                    <h6 class="title text-uppercase">Ethical Clearance</h6>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            {{ Form::open(array('url' => 'clearance/store_ethical','id' => 'clearance_form','files'=>'true')) }}
                            <input type="hidden" name="_method" value="GET" />

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        @php($name = $data['user']['first_name'].' '.$data['user']['middle_name'].' ' .$data['user']['surname'])
                                        <input name="names" class="form-control" readonly type="text" value="{{$name}}">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input name="title" class="form-control" readonly type="text" value="{{$data['user']['rank']}}">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>College</label>
                                        <input name="college" class="form-control" readonly type="text" value="{{$data['college']['name']}}">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Department</label>
                                        <input name="department" class="form-control" readonly type="text" value="{{$data['dept']['name']}}">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input name="email" class="form-control" readonly type="email" value="{{$data['user']['email']}}">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input name="phone" class="form-control" readonly value="{{$data['user']['phone']}}">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Project registration number<span style="color: red;">*</span></label>
                                        {{Form::text('project_reg_no', old('project_reg_no'), ['class="form-control" rows="5"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Research Title <span style="color: red;">*</span></label>
                                        {{Form::textarea('research_title', old('research_title'), ['class="form-control" rows="2"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Type of Research <span style="color: red;">*</span></label>
                                        {{Form::select('research_type', ['' => 'Select Type of Research','Commisioned' => 'Commisioned','Consultance' => 'Consultance',
                                            'Postdoctorial' => 'Postdoctorial','Staff development' => 'Staff development'], old('research_type'), ['class'=> 'form-control'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Scientific background<span style="color: red;">*</span></label>
                                        {{Form::textarea('background', old('background'), ['class="form-control" rows="2"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Study design <span style="color: red;">*</span></label>
                                        <input name="design" class="form-control" type="text">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Objectives<span style="color: red;">*</span></label>
                                        {{Form::textarea('objective', old('objective'), ['class="form-control" rows="2"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Hypotheses/Research Questions<span style="color: red;">*</span></label>
                                        {{Form::textarea('hypotheses', old('hypotheses'), ['class="form-control" rows="2"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Start Date<span style="color: red;">*</span></label>
                                        {{Form::text('start_date', old('name'), ['class="form-control" type="date" rows="5"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>End Date <span style="color: red;">*</span></label>
                                        {{Form::text('end_date', old('name'), ['class="form-control" type="date" rows="5"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->


                            <div class="row">
                                <div class="col-lg-12">
                                    <h6 class="title text-uppercase">Project location </h6>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-3">
                                    @php($regions = ['' => 'Select Region'] + $data['regions']->toArray())
                                    <div class="form-group">
                                        <label>Region <span style="color: red;">*</span></label>
                                        {{Form::select('region', $regions, old('region'), ['class'=> 'form-control','id'=> 'region'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-3 -->

                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>District <span style="color: red;">*</span></label>
                                        {{Form::select('district', ['' => 'Select District'], old('district'), ['class'=> 'form-control','id'=> 'district'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-3 -->
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>Ward</label>
                                        {{Form::select('ward', ['' => 'Select ward'], old('ward'), ['class'=> 'form-control','id'=> 'ward'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-3 -->
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>Village/Street</label>
                                        {{Form::select('village', ['' => 'Select village'], old('village'), ['class'=> 'form-control','id'=> 'village'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-3 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Data collection procedures with details of actual method <span style="color: red;">*</span></label>
                                        {{Form::textarea('collection', old('collection'), ['class="form-control" rows="2"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Data collection tools (English)</label>
                                        {{Form::file('english_attachment', ['class="form-control"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Data collection tools (Swahili)</label>
                                        {{Form::file('swahili_attachment', ['class="form-control"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Procedures to process, store and test biological samples</label>
                                        {{Form::textarea('procedures', old('procedures'), ['class="form-control" rows="2"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Adverse consequences to the participant<span style="color: red;">*</span></label>
                                        <input name="adverse" class="form-control" type="text">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Precautions to be taken<span style="color: red;">*</span></label>
                                        <input name="precaution" class="form-control" type="text">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Procedures which may cause discomfort, distress or harm to the participant</label>
                                        {{Form::textarea('procedures_discomfort', old('procedures_discomfort'), ['class="form-control" rows="2"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Degree of discomfort or distress<span style="color: red;">*</span></label>
                                        <input name="degree_discomfort" class="form-control" type="text">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>How will participants be recruited<span style="color: red;">*</span></label>
                                        <input name="recruited" class="form-control" type="text">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Recruiting tools (attachment)</label>
                                        {{Form::file('recruite_attachment', ['class="form-control"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Payment of Participants </label>
                                        <input name="payment_participant" class="form-control" type="text">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->


                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Participant(s) consent (attach consent form)</label>
                                        {{Form::file('consent_attachment', ['class="form-control"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Confidentiality of participant data and how will participant be informed</label>
                                        <input name="confidentiality" class="form-control" type="text">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Involvement of vulnerable population, if yes describe steps taken to protect human subjects</label>
                                        {{Form::textarea('procedures', old('procedures'), ['class="form-control" rows="2"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Research involves investigation of illegal conduct?</label>
                                        {{Form::select('investigation_qn', ['' => 'Select','yes' => 'Yes','no' => 'No'], old('investigation_qn'), ['class'=> 'form-control linkto'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Details and safety measure of researcher from harm or suspicion of illegal conduct</label>
                                        {{Form::textarea('investigation_detail', old('investigation_detail'), ['class="form-control" rows="2"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Expertise of applicant for the proposed research</label>
                                        <input name="expertise" class="form-control" type="text">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Research put investigators in situations</label>
                                        {{Form::select('situation_type', ['' => 'Select','harm' => 'Harm','injury ' => 'Injury ','criminality ' => 'Criminality'], old('situation_type'), ['class'=> 'form-control linkto'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Harm or damage to bystanders or the immediate environment</label>
                                        {{Form::select('env', ['' => 'Select','harm' => 'Harm','damage ' => 'Damage ','immediate' => 'Immediate'], old('situation_type'), ['class'=> 'form-control linkto'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Negative impact on the academic status or reputation of the College</label>
                                        {{Form::textarea('negative_impact', old('negative_impact'), ['class="form-control" rows="2"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <a href="javascript:{}" onclick="document.getElementById('clearance_form').submit();" type="submit" class="btn btn-primary btn-xs">Submit</a>
                                        <a href="{{url('clearance/staff')}}" class="btn btn-danger btn-xs">Cancel</a>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            {{ Form::close() }}

                        </div>
                        <!--./card-body -->
                    </div>
                    <!--./card -->
                </div>
                <!--./col-lg-9 -->

            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>

<script>
    //get districts
    $('#region').change(function() {
        var region_id = $(this).val();
        if (region_id) {
            $.ajax({
                type: "get",
                url: "{{url('/districts')}}/" + region_id,
                success: function(res) {
                    if (res) {
                        $("#district").empty();
                        $("#ward").empty();
                        $("#village").empty();
                        $("#district").append('<option>Select District</option>');
                        $.each(res, function(key, value) {
                            $("#district").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                }

            });
        }
    });

    //get wards
    $('#district').change(function() {
        var district_id = $(this).val();
        if (district_id) {
            $.ajax({
                type: "get",
                url: "{{url('/wards')}}/" + district_id,
                success: function(res) {
                    if (res) {
                        $("#ward").empty();
                        $("#village").empty();
                        $("#ward").append('<option>Select Ward</option>');
                        $.each(res, function(key, value) {
                            $("#ward").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                }

            });
        }
    });

    //get villages
    $('#ward').change(function() {
        var ward_id = $(this).val();
        if (ward_id) {
            $.ajax({
                type: "get",
                url: "{{url('/villages')}}/" + ward_id,
                success: function(res) {
                    if (res) {
                        $("#village").empty();
                        $("#village").append('<option>Select Village</option>');
                        $.each(res, function(key, value) {
                            $("#village").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                }

            });
        }
    });
</script>
@endsection