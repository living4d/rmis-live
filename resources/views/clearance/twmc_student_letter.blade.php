<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>RIMS - {{isset($title) ? $title : 'Research Information Management System'}}</title>

    <!-- favicon -->
    <link rel="icon" type="image/png" href="{{asset('/assets/img/favicon.png')}}">

    <!-- Bootstrap core CSS -->
    <link href="{{asset('/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!--datatables -->
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <!-- Font awesome css -->
    <link href="{{asset('/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- custom styles -->
    <link href="{{ asset('/assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/css/colors.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/css/forms.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/css/letter.css') }}" rel="stylesheet">


    <!-- Custom styles for datatables -->
    <link href="{{asset('/assets/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">

    <!--chosen select -->
    <link href="{{asset('assets/plugins/chosen_v1.8.7/chosen.css')}}" rel="stylesheet">

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Chosen JavaScript -->
    <script type="text/javascript" src="{{asset('assets/plugins/chosen_v1.8.7/chosen.jquery.js')}}"></script>

    <!-- Custom JavaScript -->
    <script src="{{asset('assets/js/custom.js')}}"></script>

    <!-- add ckeditor -->
    <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

    <style>
        .table-hover tbody tr:hover td {
            color: blue
        }
    </style>

</head>

<body>
    <div class="row noprint my-3">
        <div class="col-sm-2 ml-5"></div>
        <div class="col-sm-4 text-left">
            <button class="btn btn-primary" onclick="window.print();return false;"><i class="fa fa-print" aria-hidden="true"></i> Print </button>
            {{-- <button  class="btn btn-warning" ><a href="{{route('clearance')}}">Cancel </a> </button> --}}
            <a class="btn btn-warning" href="{{route('clearance')}}">Cancel </a>
        </div>
    </div>
    <div class="row print">
        <page size="A4">
            <div class="row container mt-3">
                <div align='center' class="col-sm-2 mt-4">
                    <img src="{{asset('assets/img/tz_emblem.png')}}" />
                </div>
                <div align='center' class="col-sm-8">
                    <h1 style="color:black;font-size:20px;"> UNITED REPUBLIC OF TANZANIA </h1>
                    <h2 style="color:black;font-size:20px;"> MINISTRY OF EDUCATION, SCIENCE AND TECHNOLOGY </h2>
                    <h2 style="color:#2980b9;font-size:25px;"> UNIVERSITY OF DAR ES SALAAM </h2>
                    <h2 style="color:red;font-size:25px;"> OFFICE OF THE VICE CHANCELLOR </h2>
                </div>
                <div align='center' class="col-sm-2 mt-4">
                    <img src="{{asset('assets/img/udsm_logo.png')}}" />
                </div>
                <div class="row container">
                    <div align='left' class="col-sm-6">
                        <p>
                        <h3 class="font-weight-normal">Ref. No: AB3/12(B)</h3>
                        </p>
                    </div>
                    <div align='right' class="col-sm-6">
                        <p class="text-right">
                            @php
                            $approved_date = $data['clearance']['approved_date'];
                            $display_approved_date = date("j F, Y", strtotime($approved_date));
                            $date_of_research_commence = $data['clearance']['commencement_date'];
                            $display_date_commence = date("j F, Y", strtotime($date_of_research_commence));
                            $date_of_research_end = $data['clearance']['completion_date'];
                            $display_research_end = date("j F, Y", strtotime($date_of_research_end));
                            @endphp

                        <h3 class="font-weight-normal">Date: {{$display_approved_date}}</h3>
                        </p>
                    </div>
                </div>
                <div class="row container mt-4">
                    <div align='center' class="col-sm-12">
                        <p>
                        <h3 id="re-title"><span style="font-size:20px;font-family:Tahoma;text-align:center">TO WHOM IT MAY CONCERN</span></h3>
                        </p>
                    </div>
                </div>
                <br>

                <div class="row container">
                    <div class="col-lg-12">
                        <p>
                        <h3 id="re-title"><span style="font-size:20px;font-family:Tahoma;text-align:center">RE: REQUEST FOR RESEARCH CLEARANCE</span></h3>
                        </p>
                        <p id="letter-content"><span style="font-size:20px;font-family:Tahoma;font-weight: normal; text-align:justify">
                                1. The purpose of this letter is to introduce to you <b>{{$data['clearance']['names']}}</b> who is a bonafide student of the University of Dar es Salaam and who is at the moment required to
                                conduct research. Our students undertake research activities as part of their study programmes.</span>
                        </p>
                        <p><span style="font-size:20px;font-family:Tahoma;font-weight: normal; text-align:justify">
                                {{$data['clearance']['names']}} has been permitted to conduct a research titled <b>"{{$data['clearance']['research_title']}}."</b></span>
                        </p>

                        <p><span style="font-size:20px;font-family:Tahoma; font-weight: normal; text-align:justify">
                                2. The period for which this permission has been granted is from <b>{{$display_date_commence}} to {{$display_research_end}}</b> and it will cover <b>{{$data['site']['region']}}
                                    region</b>.</span>
                        </p>
                        <p><span style="font-size:20px;font-family:Tahoma; font-weight: normal; text-align:justify">
                                3. It will be appreciated if you will provide any assistance that may enable the candidate achieve the desired research objectives. </span>
                        </p>
                        <p><span style="font-size:20px;font-family:Tahoma; font-weight: normal; text-align:justify">Yours sincerely,</span></p>
                        <br>
                        <p><img src="{{asset('assets/signatures/vc.png')}}" /></p>
                        @php($name = $data['vc']['first_name'].' '.$data['vc']['middle_name'].' ' .$data['vc']['surname'])
                        <h4 class="text-capitalize"><span style="font-size:20px;font-family:Tahoma; font-weight: normal; text-align:left">{{$name}}</span></h4>
                        <h3><span style="font-size:20px;font-family:Tahoma;text-align:justify">VICE CHANCELLOR</span></h3>

                        <hr />
                        <div align='center'><span style="font-size:15px;font-family:Tahoma;font-weight: normal; text-align:justify">Cranford Pratt Building, University of Dar es Salaam, P.O. Box 35091, Dar es Salaam.<br />
                                Phone: +255 22 2410500-8 Ext. 2084, Direct: +255 22 2410700, Email: vc@admin.udsm.ac.tz, Website: www.udsm.ac.tz</span>
                        </div>
                    </div>
                </div>
        </page>
    </div>
</body>

</html>