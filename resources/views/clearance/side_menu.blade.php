
<div id="left_menu">
    <ul id="nav">
        <li><a href="{{ route('clearance.stats') }}">Summary</a></li>
        <?php
            echo '<li><a href="' . url('clearance') . '"> Research Clearance</a></li>';
        ?>
        @if (Auth::user()->hasAnyRole(["staff","hod","principal","drp","dvc","vc","admin"]))
            <li><a href="{{url('clearance/process')}}">My Basket <span class="badge badge-danger">{{$data['notifications']}}</span></a></li>
        @endif
        @if (Auth::user()->hasAnyRole(["hod","principal"]))
            <li><a href="{{url('clearance/processHolderSupervisions')}}">Supervisor's bag <span class="badge badge-danger">{{$data['supervise_notifications']}}</span></a></li>
        @endif
        @if (Auth::user()->hasAnyRole(["admin","drp"]))
            <li><a href="{{url('report/permitReport')}}">Clearance report </a></li>
        @endif
    </ul>
</div>
