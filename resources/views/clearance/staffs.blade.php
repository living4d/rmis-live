@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="page-title">
                        <h4>Research Clearance Form</h4>
                    </div><!-- Page Title -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <!-- sidebar -->
                    <div class="col-lg-2 col-md-2">
                        @include('clearance.side_menu')
                    </div><!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        @if(session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br/>
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br/>
                        @endif

                        <div class="card card-flat">
                            <h5 class="card-header">RESEARCH CLEARANCE FORM</h5><!--./card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="title text-uppercase">Researcher Information</h6>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                            {{ Form::open(array('url' => 'clearance/store_staff_clearance','id' => 'clearance_form','files'=>'true')) }}
                            <input type="hidden" name="_method" value="GET"/>    

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Full Name</label>
                                            @php($name = $data['user']['first_name'].' '.$data['user']['middle_name'].' ' .$data['user']['surname'])            
                                            <input name="names" class="form-control" readonly type="text" value="{{$name}}">
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-6 -->
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input name="staff_title" class="form-control" readonly type="text" value="{{$data['user']['rank']}}">
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-6 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input name="email" class="form-control" readonly type="email" value="{{$data['user']['email']}}">
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-6 -->

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Phone</label>
                                            <input name="phone" class="form-control" readonly value="{{$data['user']['phone']}}">
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-6 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>College</label>
                                                <select name="college_id" class="form-control" id="">
                                                    <option value="{{$data['college']->id}}" selected>{{$data['college']->name}}</option>
                                                        @foreach ($data['college_pool'] as $college)
                                                            <option value="{{$college->id}}">{{$college->name}}  </option>
                                                        @endforeach
                                                    </select>
                                                <span class="text-danger">{{ $errors->first('college_id') }}</span>
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-6 -->
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Department</label>
                                                <select name="department_id" class="form-control" id="">
                                                    <option value="{{$data['dept']->id}}" selected>{{$data['dept']->name}}</option>
                                                        @foreach ($data['dept_pool'] as $department)
                                                            <option value="{{$department->id}}">{{$department->name}}  </option>
                                                        @endforeach
                                                    </select>
                                                <span class="text-danger">{{ $errors->first('department_id') }}</span>
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-6 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="title text-uppercase">Research Clearance Information</h6>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Project registration number<span style="color: red;">*</span></label>
                                            {{Form::text('project_reg_no', old('project_reg_no'), ['class="form-control" rows="5"'])}}
                                            <span class="text-danger">{{ $errors->first('project_reg_no') }}</span>
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Research Title <span style="color: red;">*</span></label>
                                            {{Form::textarea('research_title', old('research_title'), ['class="form-control" rows="2"'])}}
                                            <span class="text-danger">{{ $errors->first('research_title') }}</span>
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Field work Start Date<span style="color: red;">*</span></label>
                                            {{Form::text('start_date', old('start_date'), ['class="form-control" type="date" rows="5"'])}}
                                            <span class="text-danger">{{ $errors->first('start_date') }}</span>
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-12 -->

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Field work End Date <span style="color: red;">*</span></label>
                                            {{Form::text('end_date', old('end_date'), ['class="form-control" type="date" rows="5"'])}}
                                            <span class="text-danger">{{ $errors->first('end_date') }}</span>
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Type of Research <span style="color: red;">*</span></label>
                                            {{Form::select('research_type', ['' => 'Select Type of Research','Commisioned' => 'Commisioned','Consultancy' => 'Consultancy','Independent study' => 'Independent study',
                                            'Postdoctorial' => 'Postdoctorial','Staff development' => 'Staff development'], old('research_type'), ['class'=> 'form-control'])}}
                                            <span class="text-danger">{{ $errors->first('research_type') }}</span>    
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Amount of funds<span style="color: red;">*</span></label>
                                            {{-- <input class="form-control" id="number_comma" pattern="^[\d,]+$" name="fund_amount" placeholder="Enter fund amount" type="text"> --}}
                                            {{Form::text('fund_amount', old('fund_amount'), ['class="form-control" id="number_comma" placeholder="Enter fund amount ---" '])}}
                                            <span class="text-danger">{{ $errors->first('fund_amount') }}</span>
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-12 -->

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Source of Funds <span style="color: red;">*</span></label>
                                            {{Form::text('fund_source', old('fund_source'), ['class="form-control" rows="5"'])}}
                                            <span class="text-danger">{{ $errors->first('fund_source') }}</span>
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Other UDSM Researchers to be involved(If any)</label>
                                            <div class="container">
                                                <label ><span class="badge badge-warning">NOTE: If other researcher(s) involved, please list the names separated by comma. e.g. Meddie Kagere, Fiston Mayele, Asha Kinyota, and Mohammed Hussein</span> </label>
                                            </div>
                                                {{Form::text('other_researcher', old('other_researcher'), ['class="form-control" rows="5"'])}}
                                            <span class="text-danger">{{ $errors->first('other_researcher') }}</span>
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                {{-- sites Information --}}
                                <div class="my-3"  id="research_site">
                                    <div class="mb-3">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h6 class="text-uppercase"><small>Research Site Information</small> </h6>
                                            </div><!--./col-lg-12 -->
                                        </div><!--./row -->
                                        
                                        <div class="container">
                                            <label ><span class="badge badge-warning">NOTE: RAS and DAS letters are generated automatically, do not fill them as addressee</span> </label>
                                        </div>
                                        <div class="container">
                                            <label><span style="float:left;" class="badge badge-default">NOTE 2: If any of you data collection site is in ZANZIBAR, please include one site with the following details:<br />
                                            Organization: Office of The Second Vice President<br />
                                            Title of Addressee: Permanent Secretary<br />
                                            Address: P. O. Box 239<br />
                                            Region: Zanzibar<br />
                                            District: Mjini<br />
                                            <hr />
                                            After Approval, please use the Organization letter to submit<br />
                                             to the office of the 2nd Vice President as it is required by the regulations of Zanzibar</span> </label>
                                        </div>

                                        <div class="site my-2">
                                        <div class="row">
                                            <input type="hidden" name="site_count[]" value="site">

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Organization<span style="color: red;">*</span>
                                                        <small>(where data will be collected)</small>
                                                        </label>
                                                    {{Form::text('organization[]', old('organization[]'), ['class="form-control" rows="5"'])}}
                                                    <span class="text-danger">{{ $errors->first('organization') }}</span>
                                                </div><!--./form-group -->
                                            </div><!--./col-lg-12 -->
                                        </div><!--./row -->
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Title of Addressee <span style="color: red;">*</span><small>(e.g. Director, Manager, District Executive Director etc)</small></label>
                                                    {{Form::text('title[]', old('title[]'), ['class="form-control" rows="5"'])}}
                                                    <span class="text-danger">{{ $errors->first('title') }}</span>
                                                </div><!--./form-group -->
                                            </div><!--./col-lg-12 -->

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Address <span style="color: red;">*</span></label>
                                                    {{Form::text('address[]', old('address[]'), ['class="form-control" rows="5"'])}}
                                                    <span class="text-danger">{{ $errors->first('address') }}</span>
                                                </div><!--./form-group -->
                                            </div><!--./col-lg-12 -->
                                        </div><!--./row -->

                                        <div class="row">
                                            <div class="col-lg-3">
                                            @php($regions = ['' => 'Select Region'] + $data['regions']->toArray())
                                                <div class="form-group">
                                                    <label>Region <span style="color: red;">*</span></label>
                                                    {{Form::select('region[]', $regions, old('region[]'), ['class'=> 'form-control','id'=> 'region'])}}
                                                    <span class="text-danger">{{ $errors->first('region') }}</span>
                                                </div><!--./form-group -->
                                            </div><!--./col-lg-3 -->

                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>District <span style="color: red;">*</span></label>
                                                    {{Form::text('district[]', old('district[]'), ['class="form-control" '])}}
                                                    <span class="text-danger">{{ $errors->first('district') }}</span>
                                                </div><!--./form-group -->
                                            </div><!--./col-lg-3 -->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Ward</label>
                                                    {{Form::text('ward[]', old('ward[]'), ['class="form-control" '])}}
                                                    <span class="text-danger">{{ $errors->first('ward') }}</span>
                                                </div><!--./form-group -->
                                            </div><!--./col-lg-3 -->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Village/Street</label>
                                                    {{Form::text('village[]', old('village[]'), ['class="form-control" '])}}
                                                    <span class="text-danger">{{ $errors->first('village') }}</span>
                                                </div><!--./form-group -->
                                            </div><!--./col-lg-3 -->
                                        </div><!--./row -->
                                    
                                        <div class="row">
                                            <div class="col-lg-6">
                            <div class="form-group">
                              <label>Assigned researcher <span style="color: red;">*</span></label>
                              <select name="assigned_researcher[]" class="form-control" id="">
                                <option value="">Select researchers to assign</option>
                                @foreach ($data['reseachers'] as $researcher)
                                <option value="{{$researcher['id']}}">{{$researcher['first_name']}} {{$researcher['surname']}}</option>
                                @endforeach
                              </select>
                              <span class="text-danger">{{ $errors->first('assigned_researcher') }}</span>
                            </div>
                            <!--./form-group -->
                          </div>

                                        
                                                    <div class="col-lg-6">
                                                        <label>Researcher Assistant </label>
                                                        {{Form::text('assistant[]', old('assistant[]'), ['class="form-control" rows="5"'])}}
                                                        <span class="text-danger">{{ $errors->first('assistant') }}</span>
                                                    </div>
                                        </div>

                                        <div class="row hidebutton">
                                            <div class="col-md-12">
                                                <div class="form-group pull-right"  >
                                                    <i class="fa fa-minus btn btn-danger removeButton" id="removeButton" title="Remove research site"></i>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group pull-right"  >
                                                    <i class="fa fa-plus btn btn-primary addButton" id="addButton" title="Add another research site"> Add more Site</i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Approved Research Proposal/Agreement
                                                <span style="color: red;">*</span>
                                            </label>
                                            {{Form::file('agreement_attachment', ['class="form-control"'])}}
                                            <span class="text-danger">{{ $errors->first('agreement_attachment') }}</span>
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <a href="javascript:{}" onclick="document.getElementById('clearance_form').submit();"
                                            type="submit" class="btn btn-primary btn-xs">Submit</a>
                                            <a href="{{url('clearance/staff')}}"
                                               class="btn btn-danger btn-xs">Cancel</a>
                                        </div>
                                    </div>
                                </div><!--./row -->
                            {{ Form::close() }}

                            </div><!--./card-body -->
                        </div><!--./card -->
                    </div><!--./col-lg-9 -->

                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>

    <script>
        
        //get districts
        $('#region').change(function(){
        var region_id = $(this).val();
        if(region_id){
        $.ajax({
           type:"get",
           url:"{{url('/districts')}}/"+region_id,
           success:function(res)
           {       
                if(res)
                {
                    $("#district").empty();
                    $("#ward").empty();
                    $("#village").empty();
                    $("#district").append('<option>Select District</option>');
                    $.each(res,function(key,value){
                        $("#district").append('<option value="'+key+'">'+value+'</option>');
                    });
                }
           }

        });
        }
    });

    //get wards
    $('#district').change(function(){
        var district_id = $(this).val();
        if(district_id){
        $.ajax({
           type:"get",
           url:"{{url('/wards')}}/"+district_id,
           success:function(res)
           {       
                if(res)
                {
                    $("#ward").empty();
                    $("#village").empty();
                    $("#ward").append('<option>Select Ward</option>');
                    $.each(res,function(key,value){
                        $("#ward").append('<option value="'+key+'">'+value+'</option>');
                    });
                }
           }

        });
        }
    }); 

    //get villages
    $('#ward').change(function(){
        var ward_id = $(this).val();
        if(ward_id){
        $.ajax({
           type:"get",
           url:"{{url('/villages')}}/"+ward_id,
           success:function(res)
           {       
                if(res)
                {
                    $("#village").empty();
                    $("#village").append('<option>Select Village</option>');
                    $.each(res,function(key,value){
                        $("#village").append('<option value="'+key+'">'+value+'</option>');
                    });
                }
           }

        });
        }
    }); 
    </script>
@endsection