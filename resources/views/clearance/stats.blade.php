@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Summary</h4>
                            <ul>
                                <li><a href="{{ route('clearance.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li>Summary</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('clearance.side_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <div class="card info-box">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3 col-xs-3">
                                                <i class="fa fa-active-permits"></i>
                                            </div>
                                            <div class="col-md-9 col-xs-9 text-right">
                                                <div class="text-large font-weight-600">
                                                    {{ $data['active'] }}
                                                </div>

                                                <div class="text-medium text-grey">
                                                    Active Permits
                                                </div>
                                            </div>
                                            <!--./col-xs-9 -->
                                        </div>
                                        <!--./row-->
                                    </div>
                                    <!--./card-body-->
                                </div>
                                <!--./card-->
                            </div>
                            <!--./col-md-4 -->

                            <div class="col-lg-3 col-md-3">
                                <div class="card info-box">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3 col-xs-3">
                                                <i class="fa fa-expired-permits"></i>
                                            </div>
                                            <div class="col-md-9 col-xs-9 text-right">
                                                <div class="text-large font-weight-600">
                                                    {{ $data['expired'] }}
                                                </div>

                                                <div class="text-medium text-grey">
                                                    Expired Permits
                                                </div>
                                            </div>
                                            <!--./col-xs-9 -->
                                        </div>
                                        <!--./row-->
                                    </div>
                                    <!--./card-body-->
                                </div>
                                <!--./card-->
                            </div>
                            <!--./col-md-4 -->

                            <div class="col-lg-3 col-md-3">
                                <div class="card info-box">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3 col-xs-3">
                                                <i class="fa fa-rejected-permits"></i>
                                            </div>
                                            <div class="col-md-9 col-xs-9 text-right">
                                                <div class="text-large font-weight-600">
                                                    {{ $data['rejected'] }}
                                                </div>

                                                <div class="text-medium text-grey">
                                                    Rejected Permits
                                                </div>
                                            </div>
                                            <!--./col-xs-9 -->
                                        </div>
                                        <!--./row-->
                                    </div>
                                    <!--./card-body-->
                                </div>
                                <!--./card-->
                            </div>
                            <!--./col-md-4 -->

                            <div class="col-lg-3 col-md-3">
                                <div class="card info-box">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3 col-xs-3">
                                                <i class="fa fa-inline-permits"></i>
                                            </div>
                                            <div class="col-md-9 col-xs-9 text-right">
                                                <div class="text-large font-weight-600">
                                                    {{ $data['waiting'] }}
                                                </div>

                                                <div class="text-medium text-grey">
                                                    Inline for Approval
                                                </div>
                                            </div>
                                            <!--./col-xs-9 -->
                                        </div>
                                        <!--./row-->
                                    </div>
                                    <!--./card-body-->
                                </div>
                                <!--./card-->
                            </div>
                            <!--./col-md-4 -->
                        </div>
                        <!--./row -->
                    </div>
                    <!--./col-md-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
