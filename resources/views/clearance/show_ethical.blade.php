@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="page-title">
                        <h4>Ethical clearance</h4>
                        <ul>
                            <li><a href="{{url('/')}}" title=""><i class="fa fa-home"></i> Home</a></li>
                            <li>Clearance</li>
                        </ul>
                    </div><!-- Page Title -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <!-- sidebar -->
                     <!-- sidebar -->
                     <div class="col-lg-2 col-md-2">
                        @include('clearance.side_menu')
                    </div><!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat">
                            <h5 class="card-header text-uppercase">Ethical Clearance information</h5><!--./card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        {{-- <h6 class="title text-uppercase">Ethical information</h6> --}}
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row p-2">
                                    <div class="col-md-12">
                                        <table class="table table-bordered">
                                            <tr>
                                                <td width="20%"><span class="font-weight-bold">Research Type </span>
                                                </td>
                                                <td width="80%">
                                                    <span class="text-uppercase">{{$data['ethical']['research_type']}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%"><span class="font-weight-bold">Background </span>
                                                </td>
                                                <td width="80%">
                                                    <span class="text-uppercase">{{$data['ethical']['background']}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%"><span class="font-weight-bold">Design </span>
                                                </td>
                                                <td width="80%">
                                                    <span class="text-uppercase">{{$data['ethical']['design']}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%"><span class="font-weight-bold">Objective </span></td>
                                                <td width="80%">
                                                    <span class="text-uppercase">{{$data['ethical']['objective']}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%"><span class="font-weight-bold">Hypotheses </span></td>
                                                <td width="80%">
                                                    <span class="text-uppercase">{{$data['ethical']['hypotheses']}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%"><span class="font-weight-bold">Region </span></td>
                                                <td width="80%">
                                                    <span class="text-uppercase">{{$data['region']['name']}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%"><span class="font-weight-bold">District </span></td>
                                                <td width="80%">
                                                    <span class="text-uppercase">{{$data['district']['name']}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%"><span class="font-weight-bold">Ward </span></td>
                                                <td width="80%">
                                                    <span class="text-uppercase">{{$data['ward']['name']}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%"><span class="font-weight-bold">Village </span></td>
                                                <td width="80%">
                                                    <span class="text-uppercase">{{$data['village']['name']}}</span>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td width="20%"><span class="font-weight-bold">Collection </span></td>
                                                <td width="80%">
                                                    <span class="text-uppercase">{{$data['ethical']['collection']}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%"><span class="font-weight-bold">Procedures </span></td>
                                                <td width="80%">
                                                    <span class="text-uppercase">{{$data['ethical']['procedures']}}</span>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td width="20%"><span class="font-weight-bold">Adverse </span></td>
                                                <td width="80%">
                                                    <span class="text-uppercase">{{$data['ethical']['adverse']}}</span>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td width="20%"><span class="font-weight-bold">Precaution </span></td>
                                                <td width="80%">
                                                    <span class="text-uppercase">{{$data['ethical']['precaution']}}</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>

                                @if ($data['recommendations'])
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <table width="100%" class="table table-hover table-bordered table-responsive">
                                                <thead>
                                                    <tr>
                                                        <th width="3%">From</th>
                                                        <th width="20%">Comments</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($data['recommendations'] as $recommend)
                                                    <tr>
                                                        <td>{{$recommend['from']}}</th>
                                                        <td>{{$recommend['comment']}}</th>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div><!--./col-lg-6 -->
                                    </div><!--./row -->
                                @endif

                                {{-- actions for level 1 --}}
                            @if ($data['ethical']->level === "1")
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                    @if (Auth::user()->hasRole("hod"))
                                        <a href="{{route('recommendation_panel_ethical',['id'=>$data['ethical']->id])}}"
                                           class="btn btn-primary btn-xs">FORWARD TO PRINCIPAL</a>
                                        
                                    @endif
                                        <a href="{{url('clearance')}}"class="btn btn-danger btn-xs">BACK</a>
                                    </div>
                                </div>
                            </div><!--./row -->
                            @endif
                            @if ($data['ethical']->level === "2")
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                    @if (Auth::user()->hasRole("principal"))
                                        <a href="{{route('recommendation_panel_ethical',['id'=>$data['ethical']->id])}}"
                                           class="btn btn-primary btn-xs">FORWARD TO DRP</a>
                                        
                                    @endif
                                        <a href="{{url('clearance')}}"class="btn btn-danger btn-xs">BACK</a>
                                    </div>
                                </div>
                            </div><!--./row -->
                            @endif
                            @if ($data['ethical']->level === "3")
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                    @if (Auth::user()->hasRole("drp"))
                                        <a href="{{route('recommendation_panel_ethical',['id'=>$data['ethical']->id])}}"
                                           class="btn btn-primary btn-xs">FORWARD TO DVC</a>
                                    @endif
                                        <a href="{{url('clearance')}}"class="btn btn-danger btn-xs">BACK</a>
                                    </div>
                                </div>
                            </div><!--./row -->
                            @endif
                            @if ($data['ethical']->level === "4")
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                    @if (Auth::user()->hasRole("dvc"))
                                        <a href="{{route('recommendation_panel_ethical',['id'=>$data['ethical']->id])}}"
                                           class="btn btn-primary btn-xs">FORWARD TO VC</a>
                                    @endif
                                        <a href="{{url('clearance')}}"class="btn btn-danger btn-xs">BACK</a>
                                    </div>
                                </div>
                            </div><!--./row -->
                            @endif
                            @if ($data['ethical']->level === "5")
                            @if (Auth::user()->hasRole("dvc"))
                            @if ($data['ethical']->approval_status !== "approved")
                             {{ Form::open(array('url' =>['clearance/approve_ethical','id' => $data['ethical']->id],'id' => 'clearance_approve')) }}
                                <input type="hidden" name="_method" value="GET"/>
                                 <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Approve Clearance </label>
                                            {{Form::select('approve_status', ['' => 'Select Option','approved' => 'Approve','reject' => 'Rejected'], old('approve_status'), ['class'=> 'form-control'])}}
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-6 -->
                                 </div><!--./row -->
                                 <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <a href="javascript:{}" onclick="document.getElementById('clearance_approve').submit();"
                                               class="btn btn-primary btn-xs" type="submit">Submit</a>
                                        </div>
                                    </div>
                                </div><!--./row -->
                             {{ Form::close() }}
                             @endif
                             @endif
                            @endif
                            </div><!--./card-body -->
                        </div><!--./card -->
                    </div><!--./col-lg-9 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
@endsection