@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="page-title">
                        <h4>Clearance</h4>
                        <ul>
                            <li><a href="{{ route('clearance.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li>Summary</li>
                        </ul>
                    </div><!-- Page Title -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <!-- sidebar -->
                    <div class="col-lg-2 col-md-2">
                        @include('clearance.side_menu')
                    </div><!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        @if(session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br/>
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br/>
                        @endif
                    
                    
                    
                    

                    @if ($data['table'] == "process")
                        @if (Auth::user()->hasAnyRole(["staff","hod","principal","drp","dvc","vc","admin"]))
                            @if (isset($data['students']) && count($data['students']) > 0)
                                <table id="myTable" class="table table-hover table-bordered">
                                    <thead>
                                    <tr width="100%" bgcolor="#aed6f1" height="30">
                                        <tH colspan="12" ><font color="grey"><b>STUDENTS CLEARANCE</b></font></td>
                                    </tr>
                                    <tr>
                                        <th width="3%"></th>
                                        <th width="25%">Research Title</th>
                                        <th width="12%">Researcher</th>
                                        <th width="11%">Type of research</th>
                                        <th width="12%">Date of commencement</th>
                                        <th width="12%">Date of completion</th>
                                        <th width="5%">Approval status</th>
                                        <th width="10%">Attachment</th>
                                        <th width="5%"></th>
                                        <th width="5%"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $serial = 1; ?>
                                        @foreach ($data['students'] as $student)
                                        <tr>
                                            <td>{{$serial}}</td>
                                            <td class="text-uppercase">{{$student->research_title}}</td>
                                            <td class="text-uppercase">{{$student->names}}</td>
                                            <td>{{$student['type']['name']}}</td>
                                            <td>{{$student->commencement_date}}</td>
                                            <td>{{$student->completion_date}}</td>
                                            <td class="text-uppercase">
                                                @if ($student->approval_status == "supervisor")
                                                    <label class="badge badge-primary text-uppercase">Supervisor to support</label>
                                                @elseif ($student->approval_status == "hod")
                                                    <label class="badge badge-primary text-uppercase">HOD to support</label>
                                                @elseif($student->approval_status == "principal")
                                                    <label class="badge badge-secondary text-uppercase">Principal to endorse</label>
                                                @elseif($student->approval_status == "rejected")
                                                    <label class="badge badge-danger text-uppercase">Rejected</label>
                                                @elseif($student->approval_status == "drp")
                                                    <label class="badge badge-info text-uppercase">DRP to recommend</label>
                                                @elseif($student->approval_status == "dvc")
                                                    <label class="badge badge-secondary text-uppercase">DVC to recommend</label>
                                                @elseif($student->approval_status == "vc")
                                                    <label class="badge badge-success text-uppercase">VC to approve</label>           
                                                @elseif($student->approval_status == "approved" || $student->approval_status == "Approved")
                                                    <label class="badge badge-success text-uppercase">{{$student->approval_status}}</label>
                                                @elseif($student->approval_status == "referred" || $student->approval_status == "Referred")
                                                    <label class="badge badge-warning text-uppercase">{{$student->approval_status}}</label>
                                                @elseif($student->approval_status == "ref_approved" || $student->approval_status == "Ref_Approved")
                                                    <label class="badge badge-success text-uppercase">{{$student->approval_status}}</label>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{asset('updloads')}}/{{$student->proposal_attachment}}" target="_blank" title="Download proposal">
                                                    <i class="fa fa-paperclip fa-lg text-success"></i>
                                                </a> &nbsp;
                                                <a href="{{asset('updloads')}}/{{$student->student_identity}}" target="_blank" title="Download student ID">
                                                    <i class="fa fa-paperclip fa-lg text-secondary"></i>
                                                </a> &nbsp;
                                                <a href="{{asset('updloads')}}/{{$student->finance_statement}}" target="_blank" title="Download financial statement">
                                                    <i class="fa fa-paperclip fa-lg text-warning"></i>
                                                </a> &nbsp;
                                            </td>
                                            <td>
                                                <a href="{{route('show_student_clearance',['id'=>$student->id])}}" title="Details" class="btn btn-outline-secondary btn-xs">
                                                    <i class="fa fa-folder-open fa-lg text-primary"></i>
                                                </a> &nbsp;
                                                
                                            </td>
                                            @if (Auth::user()->hasRole("admin"))
                                            <td>
                                                <a href="{{route('edit_std_clearance_admin',['id'=>$student->id])}}" title="Edit Clearance" class="btn btn-outline-secondary btn-xs">
                                                    <i class="fa fa-pencil fa-lg text-dark"></i>
                                                </a> &nbsp;
                                            </td>
                                            @endif
                                        </tr>
                                        <?php $serial++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            @else
                                <div class="alert alert-danger mt-2"> No any student clearance found which you are supposedly to be a supervisor.</div>
                            @endif
                        @endif

                        
                    @endif
                       </div><!--./card -->
                    </div><!--./col-lg-9 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
@endsection