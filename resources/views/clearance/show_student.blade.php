@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <h4>Student clearance</h4>
                    <ul>
                        <li><a href="{{url('/')}}" title=""><i class="fa fa-home"></i> Home</a></li>
                        <li>Clearance</li>
                    </ul>
                </div><!-- Page Title -->
            </div><!--./row -->
        </div><!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <!-- sidebar -->
                <!-- sidebar -->
                <div class="col-lg-2 col-md-2">
                    @include('clearance.side_menu')
                </div><!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    <div class="card card-flat">
                        <h5 class="card-header text-uppercase">Student Clearance details</h5><!--./card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h6 class="title text-uppercase">Student information</h6>
                                </div><!--./col-lg-12 -->
                            </div><!--./row -->
                            <div class="row p-2">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Full Name </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['student']['names']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Gender </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['student']['gender']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Nationality </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['student']['nationality']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Registration Number </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['student']['reg_no']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Year of Entry </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['student']->year_entry}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">College </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['college_clr']->name}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Department </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['dept_clr']->name}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Degree Programme </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['student']['degree_program']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Duration </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['student']['duration']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Program Mode </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['student']['program_mode']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Delivery Mode </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['student']['delivery_mode']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Contact </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['student']['contact']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Supervisor </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['user']->first_name}} {{$data['user']->middle_name}} {{$data['user']->surname}}</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <h6 class="title text-uppercase">Research Clearance Information</h6>
                                </div><!--./col-lg-12 -->
                            </div><!--./row -->
                            <div class="row p-2">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Research Title </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['student']['research_title']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Type of Research </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['research_type']['name']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Date of commencement </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['student']['commencement_date']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Date of completion </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['student']['completion_date']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Research Budget </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['student']->research_budget}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Source of research fund </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['student']['fund_source']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Research proposal </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase"><a class="form-control" href="{{asset('updloads')}}/{{$data['student']->proposal_attachment}}" target="_blank"><i class="fa fa-paperclip fa-lg text-success"></i> Research proposal</a></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Student ID </span>
                                            </td>
                                            <td width="80%"><span class="text-uppercase"><a class="form-control" href="{{asset('updloads')}}/{{$data['student']->student_identity}}" target="_blank"><i class="fa fa-paperclip fa-lg text-success"></i> Student ID</a></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Financial Statement </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase"><a class="form-control" href="{{asset('updloads')}}/{{$data['student']->finance_statement}}" target="_blank"><i class="fa fa-paperclip fa-lg text-success"></i> Financial statement</a></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <h6 class="title text-uppercase">Research Sites</h6>
                                </div><!--./col-lg-12 -->
                            </div><!--./row -->
                            <?php $serial = 1; ?>
                            @foreach ($data['sites'] as $site)
                            <div class="row p-2">
                                <div class="col-md-10">
                                    <table class="table table-bordered">
                                        <label for="site_no"><span class="badge badge-success">Site {{$serial}}</span> </label>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Organization </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$site['organization']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Title of Addressee</span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$site['title']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Address </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$site['address']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Region </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$site['region']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">District </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$site['district']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Ward </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$site['ward']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Village </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$site['village']}}</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <?php $serial++; ?>
                            @endforeach

                            @if (count($data['recommendations']) > 0)
                            <div class="row mb-2">
                                <div class="col-lg-8">
                                    <table width="100%" class="table table-hover table-bordered table-responsive">
                                        <thead>
                                            <tr>
                                                <th width="3%">From</th>
                                                <th width="30%">Comments</th>
                                                <th width="10%">Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data['recommendations'] as $recommend)
                                            <tr>
                                                <td>{{$recommend['from']}}</td>
                                                <td>{{$recommend['comment']}}</td>
                                                <td>{{$recommend['created_at']}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div><!--./col-lg-6 -->
                            </div><!--./row -->
                            @endif

                            {{-- actions --}}
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        @if ($data['student']->level !== "0" || $data['student']->level !== "7" || $data['student']->level !== "8" || $data['student']->level !== "9" || $data['student']->level !== "6")
                                        @if (Auth::user()->hasRole(["vc"]) && $data['student']->level=="6")

                                        @elseif (Auth::user()->hasAnyRole(["staff","hod","principal","drp","dvc"]))
                                        <a href="{{route('back_panel_student',['id'=>$data['student']->id])}}" class="btn btn-warning btn-xs">RETURN</a>
                                        @endif
                                        @endif

                                        @if ($data['student']->level === "1")
                                        @if (Auth::user()->hasRole("student"))
                                        <a href="{{route('edit_student_clearance',['id'=>$data['student']->id])}}" class="btn btn-primary btn-xs">EDIT</a>
                                        @endif
                                        @if (Auth::user()->hasRole("staff"))
                                        <a href="{{route('recommendation_panel',['id'=>$data['student']->id])}}" class="btn btn-primary btn-xs">FORWARD TO HOD</a>
                                        @endif
                                        @endif

                                        @if ($data['student']->level === "R")
                                        @if (Auth::user()->hasRole("student"))
                                        <a href="{{route('back_panel_student',['id'=>$data['student']->id])}}" class="btn btn-warning btn-xs">REJECT</a>
                                        @endif
                                        @if (Auth::user()->hasRole("student"))
                                        <a href="{{route('edit_student_clearance',['id'=>$data['student']->id])}}" class="btn btn-primary btn-xs">EDIT</a>
                                        @endif
                                        @endif

                                        @if ($data['student']->level === "2")
                                        @if (Auth::user()->hasRole("hod"))
                                        <a href="{{route('recommendation_panel',['id'=>$data['student']->id])}}" class="btn btn-primary btn-xs">FORWARD TO PRINCIPAL</a>
                                        @endif
                                        @endif

                                        @if ($data['student']->level === "3")
                                        @if (Auth::user()->hasRole("principal"))
                                        <a href="{{route('recommendation_panel',['id'=>$data['student']->id])}}" class="btn btn-primary btn-xs">FORWARD TO DRP</a>
                                        @endif
                                        @endif

                                        @if ($data['student']->level === "4")
                                        @if (Auth::user()->hasRole("drp"))
                                        <a href="{{route('recommendation_panel',['id'=>$data['student']->id])}}" class="btn btn-primary btn-xs">FORWARD TO DVC</a>
                                        @endif
                                        @endif

                                        @if ($data['student']->level === "5")
                                        @if (Auth::user()->hasRole("dvc"))
                                        <a href="{{route('recommendation_panel',['id'=>$data['student']->id])}}" class="btn btn-primary btn-xs">FORWARD TO VC</a>
                                        @endif
                                        @endif

                                        @if ($data['student']->level === "6")
                                        @if (Auth::user()->hasRole("vc"))
                                        {{ Form::open(array('url' =>['clearance/approve_student','id' => $data['student']->id],'id' => 'clearance_approve')) }}
                                        <input type="hidden" name="_method" value="GET" />
                                        <div class="form-group">
                                            <label>Approve Clearance </label>
                                            {{Form::select('approve_status', ['' => 'Select Option','approved' => 'Approved','rejected' => 'Rejected'], old('approve_status'), ['class'=> 'form-control'])}}
                                        </div>
                                        <div class="form-group">
                                            <a href="javascript:{}" onclick="document.getElementById('clearance_approve').submit();" class="btn btn-primary btn-xs" type="submit">Submit</a>
                                        </div>
                                        {{ Form::close() }}
                                        @endif
                                        @endif

                                        @if ($data['student']->approval_status === "approved")
                                        <a href="{{route('student_clearance_letter',['id'=>$data['student']->id])}}" class="btn btn-primary btn-xs">Clearance letters</a>
                                        @elseif ($data['student']->approval_status === "ref_approved")
                                        <a href="{{route('ref_student_clearance_letter',['id'=>$data['student']->id])}}" class="btn btn-primary btn-xs">Clearance letters</a>
                                        @endif

                                        <a href="{{url('clearance')}}" class="btn btn-danger btn-xs">CANCEL</a>
                                    </div>
                                </div>
                            </div>
                        </div><!--./card-body -->
                    </div><!--./card -->
                </div><!--./col-lg-9 -->
            </div><!--./row -->
        </div><!--./container -->
    </div><!--./page-content -->
</section>
@endsection