@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <h4>Research Clearance form</h4>
                </div><!-- Page Title -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <!-- sidebar -->
                <div class="col-lg-2 col-md-2">
                    @include('clearance.side_menu')
                </div>
                <!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div><br />
                    @elseif(session()->get('danger'))
                    <div class="alert alert-danger">
                        {{ session()->get('danger') }}
                    </div><br />
                    @endif

                    <div class="card card-flat">
                        <h5 class="card-header">RESEARCH CLEARANCE FORM </h5>
                        <!--./card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h6 class="title text-uppercase">Student Information</h6>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->
                            {{ Form::open(array('url' => 'clearance/store_student_clearance','id' => 'clearance_form','files'=>'true')) }}
                            <input type="hidden" name="_method" value="GET" />
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Full Name <span style="color: red;">*</span></label>
                                        @php($name = $data['user']['first_name'].' '.$data['user']['middle_name'].' ' .$data['user']['surname'])
                                        <input name="names" class="form-control" readonly type="text" value="{{$name}}">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Gender </label>
                                        <input name="gender" class="form-control" readonly type="text" value="{{$data['user']['gender']}}">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>College </label>
                                        <select name="college_id" class="form-control" id="">
                                            <option value="{{$data['college']->id}}" selected>{{$data['college']->name}}</option>
                                            @foreach ($data['college_pool'] as $college)
                                            <option value="{{$college->id}}">{{$college->name}} </option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{ $errors->first('college_id') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Department </label>
                                        <select name="department_id" class="form-control" id="">
                                            <option value="{{$data['dept']->id}}" selected>{{$data['dept']->name}}</option>
                                            @foreach ($data['dept_pool'] as $department)
                                            <option value="{{$department->id}}">{{$department->name}} </option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{ $errors->first('department_id') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Nationality<span style="color: red;">*</span></label>
                                        {{Form::text("nationality", old('nationality'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('nationality') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Contact details <span style="color: red;">*</span></label>
                                        {{Form::text('contact', old('contact'), ['class="form-control" rows="5"'])}}
                                        <span class="text-danger">{{ $errors->first('contact') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Registration Number<span style="color: red;">*</span></label>
                                        {{Form::text("reg_no", old('reg_no'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('reg_no') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Year of Entry (eg. 2020)<span style="color: red;">*</span></label>
                                        {{Form::text("entry_year", old('entry_year'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('entry_year') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Degree Programme<span style="color: red;">*</span></label>
                                        {{Form::text("degree_program", old('degree_program'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('degree_program') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Delivery mode <span style="color: red;">*</span></label>
                                        {{Form::select('delivery_mode', ['' => 'Select Delivery mode',
                                                                            'by thesis' => 'By thesis',
                                                                            'by coursework and dissertation' => 'By Coursework and Dissertation'], old('delivery_mode'), ['class'=> 'form-control'])}}
                                        <span class="text-danger">{{ $errors->first('delivery_mode') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Study Duration (in month)<span style="color: red;">*</span></label>
                                        {{Form::text('duration', old('duration'), ['class'=> 'form-control'])}}
                                        <span class="text-danger">{{ $errors->first('duration') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Programme mode <span style="color: red;">*</span></label>
                                        {{Form::select('program_mode', ['' => 'Select Programme mode',
                                                                            'full time' => 'Full time',
                                                                            'evening' => 'Evening',
                                                                            'executive' => 'Executive'],
                                                                            old('program_mode'), ['class'=> 'form-control'])}}
                                        <span class="text-danger">{{ $errors->first('program_mode') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Supervisor’s details <span style="color: red;">*</span></label>
                                        <?php
                                        $_option = [];
                                        foreach ($data['staff_details'] as $supervisor) {
                                            $_option[$supervisor->id] = $supervisor->first_name . ' ' . $supervisor->middle_name . ' ' . $supervisor->surname;
                                        }
                                        $_option = ['' => '-- Select supervisor--'] + $_option;
                                        ?>
                                        {{ Form::select('supervisor', $_option, old('supervisor'), ['data-placeholder="   -- Select --" class="form-control chosen-select"']) }}
                                        <span class="text-danger">{{ $errors->first('supervisor') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <h6 class="title text-uppercase">Research Clearance Information</h6>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Research Title <span style="color: red;">*</span></label>
                                        {{Form::textarea('research_title', old('research_title'), ['class="form-control" rows="2"'])}}
                                        <span class="text-danger">{{ $errors->first('research_title') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Type of Research <span style="color: red;">*</span></label>
                                        <?php
                                        $_type_options = [];
                                        foreach ($data['research_types'] as $type) {
                                            $_type_options[$type->id] = $type->name;
                                        }
                                        $_type_options = ['' => '-- Select Research type--'] + $_type_options;
                                        ?>
                                        {{ Form::select('research_type', $_type_options, old('research_type'), ['data-placeholder="   -- Select --" class="form-control chosen-select"']) }}
                                        <span class="text-danger">{{ $errors->first('research_type') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Date of commencement<span style="color: red;">*</span></label>
                                        {{Form::text('commencement_date', old('commencement_date'), ['class="form-control" type="date" rows="5"'])}}
                                        <span class="text-danger">{{ $errors->first('commencement_date') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Date of completion<span style="color: red;">*</span></label>
                                        {{Form::text('completion_date', old('completion_date'), ['class="form-control" type="date" rows="5"'])}}
                                        <span class="text-danger">{{ $errors->first('completion_date') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Research Budget <span style="color: red;">*</span></label>
                                        {{Form::text('research_budget', old('research_budget'), ['class="form-control" id="number_comma" placeholder="Enter research budget---"'])}}
                                        <span class="text-danger">{{ $errors->first('research_budget') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Source of Research Funds<span style="color: red;">*</span></label>
                                        {{Form::text('fund_source', old('fund_source'), ['class="form-control" rows="5"'])}}
                                        <span class="text-danger">{{ $errors->first('fund_source') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            {{-- sites Information --}}
                            <div class="my-3" id="research_site">
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h6 class="text-uppercase"><small>Research Site Information</small> </h6>
                                        </div>
                                        <!--./col-lg-12 -->
                                    </div>
                                    <!--./row -->

                                    <div class="container">
                                        <label><span class="badge badge-warning">NOTE 1: RAS and DAS letters are generated automatically, do not fill them as addressee</span> </label>
                                    </div>
                                    <div class="container">
                                        <label><span style="float:left;" class="badge badge-default">NOTE 2: If any of you data collection site is in ZANZIBAR, please include one site with the following details:<br />
                                                Organization: Office of The Second Vice President<br />
                                                Title of Addressee: Permanent Secretary<br />
                                                Address: P. O. Box 239<br />
                                                Region: Zanzibar<br />
                                                District: Mjini<br />
                                                <hr />
                                                After Approval, please use the Organization letter to submit<br />
                                                to the office of the 2nd Vice President as it is required by the regulations of Zanzibar
                                            </span> </label>
                                    </div>


                                    <div class="site my-2">
                                        <div class="row">
                                            <input type="hidden" name="site_count[]" value="site">

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Organization
                                                        <small>(where data will be collected)</small>
                                                        <span style="color: red;">*</span></label>
                                                    {{Form::text('organization[]', old('organization[]'), ['class="form-control" rows="5"'])}}
                                                    <span class="text-danger">{{ $errors->first('organization') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-12 -->
                                        </div>
                                        <!--./row -->
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Title of Addressee <small>(e.g. Director, Manager, District Executive Director etc)</small><span style="color: red;">*</span></label>
                                                    {{Form::text('title[]', old('title[]'), ['class="form-control" rows="5"'])}}
                                                    <span class="text-danger">{{ $errors->first('title') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-12 -->

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Address<span style="color: red;">*</span></label>
                                                    {{Form::text('address[]', old('address[]'), ['class="form-control" rows="5"'])}}
                                                    <span class="text-danger">{{ $errors->first('address') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-12 -->
                                        </div>
                                        <!--./row -->

                                        <div class="row">
                                            <div class="col-lg-3">
                                                @php($regions = ['' => 'Select Region'] + $data['regions']->toArray())
                                                <div class="form-group">
                                                    <label>Region <span style="color: red;">*</span></label>
                                                    {{Form::select('region[]', $regions, old('region[]'), ['class'=> 'form-control','id'=> 'region'])}}
                                                    <span class="text-danger">{{ $errors->first('region') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-3 -->

                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>District <span style="color: red;">*</span></label>
                                                    {{Form::text('district[]', old('district[]'), ['class="form-control" '])}}
                                                    <span class="text-danger">{{ $errors->first('district') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-3 -->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Ward</label>
                                                    {{Form::text('ward[]', old('ward[]'), ['class="form-control" '])}}
                                                    <span class="text-danger">{{ $errors->first('ward') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-3 -->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Village/Street</label>
                                                    {{Form::text('village[]', old('village[]'), ['class="form-control" '])}}
                                                    <span class="text-danger">{{ $errors->first('village') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-3 -->
                                        </div>
                                        <!--./row -->

                                        <div class="row hidebutton">
                                            <div class="col-md-12">
                                                <div class="form-group pull-right">
                                                    <i class="fa fa-minus btn btn-danger removeButton" id="removeButton" title="Remove research site">Remove Site</i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group pull-right">
                                                <i class="fa fa-plus btn btn-primary addButton" id="addButton" title="Add another research site"> Add more Site</i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Approved Research proposal <span style="color: red;">*</span></label>
                                        {{Form::file('proposal_attachment', ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('proposal_attachment') }}</span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Student ID <span style="color: red;">*</span></label>
                                        {{Form::file('student_id', ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('student_id') }}</span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Certified student financial statement <span style="color: red;">*</span></label>
                                        {{Form::file('financial_statement', ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('financial_statement') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <a href="javascript:{}" onclick="document.getElementById('clearance_form').submit();" type="submit" class="btn btn-primary btn-xs">Apply</a>
                                        <a href="{{url('clearance/students')}}" class="btn btn-danger btn-xs">Cancel</a>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            {{ Form::close() }}


                        </div>
                        <!--./card-body -->
                    </div>
                    <!--./card -->
                </div>
                <!--./col-lg-12 -->

            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>

<script>
    //get districts
    $('#region').change(function() {
        var region_id = $(this).val();
        if (region_id) {
            $.ajax({
                type: "get",
                url: "{{url('/districts')}}/" + region_id,
                success: function(res) {
                    if (res) {
                        $("#district").empty();
                        $("#ward").empty();
                        $("#village").empty();
                        $("#district").append('<option>Select District</option>');
                        $.each(res, function(key, value) {
                            $("#district").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                }

            });
        }
    });

    //get wards
    $('#district').change(function() {
        var district_id = $(this).val();
        if (district_id) {
            $.ajax({
                type: "get",
                url: "{{url('/wards')}}/" + district_id,
                success: function(res) {
                    if (res) {
                        $("#ward").empty();
                        $("#village").empty();
                        $("#ward").append('<option>Select Ward</option>');
                        $.each(res, function(key, value) {
                            $("#ward").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                }

            });
        }
    });

    //get villages 
    $('#ward').change(function() {
        var ward_id = $(this).val();
        if (ward_id) {
            $.ajax({
                type: "get",
                url: "{{url('/villages')}}/" + ward_id,
                success: function(res) {
                    if (res) {
                        $("#village").empty();
                        $("#village").append('<option>Select Village</option>');
                        $.each(res, function(key, value) {
                            $("#village").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                }

            });
        }
    });
</script>
@endsection