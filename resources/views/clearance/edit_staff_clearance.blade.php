@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <h4>Research Clearance form</h4>
                </div><!-- Page Title -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <!-- sidebar -->
                <div class="col-lg-2 col-md-2">
                    @include('clearance.side_menu')
                </div>
                <!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div><br />
                    @elseif(session()->get('danger'))
                    <div class="alert alert-danger">
                        {{ session()->get('danger') }}
                    </div><br />
                    @endif

                    <div class="card card-flat">
                        <h5 class="card-header">RESEARCH CLEARANCE FORM </h5>
                        <!--./card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h6 class="title text-uppercase">Researcher Information</h6>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->
                            {{ Form::open(array('url' =>['clearance/update/staff_clearance','id' => $data['staff']->id],'id' => 'clearance_form','files'=>'true')) }}
                            <b>Full name: </b>{{$data['staff']['names']}}<br />
                            <b>Title/Rank: </b>{{$data['staff']['title']}}<br />
                            <input type="hidden" name="_method" value="post" />
                            {{-- <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Full Name <span style="color: red;">*</span></label>
                                        @php($name = $data['user']['first_name'].' '.$data['user']['middle_name'].' ' .$data['user']['surname'])
                                        <input name="names" class="form-control" readonly type="text" value="{{$name}}">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Title/Rank </label>
                                        <input name="title" class="form-control" readonly type="text" value="{{$data['user']['rank']}}">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div> --}}

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Department </label>
                                        {{-- <input name="department" class="form-control" readonly type="text" value="{{$data['dept']['name']}}"> --}}
                                        <select name="department_id" class="form-control" id="">
                                            <option value="{{$data['dept']->id}}" selected>{{$data['dept']->name}}</option>
                                                 @foreach ($data['dept_pool'] as $department)
                                                    <option value="{{$department->id}}">{{$department->name}}  </option>
                                                @endforeach
                                        </select>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>College </label>
                                        {{-- <input name="college" class="form-control" readonly type="text" value="{{$data['college']['name']}}"> --}}
                                        <select name="college_id" class="form-control" id="">
                                            <option value="{{$data['college']->id}}" selected>{{$data['college']->name}}</option>
                                                 @foreach ($data['college_pool'] as $college)
                                                    <option value="{{$college->id}}">{{$college->name}}  </option>
                                                @endforeach
                                        </select>
                                        <span class="text-danger">{{ $errors->first('college_id') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input name="email" class="form-control" radonly type="text" value="{{$data['staff']->email}}">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input name="phone" class="form-control" radonly type="text" value="{{$data['staff']->phone}}">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <h6 class="title text-uppercase">Research Clearance Information</h6>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Project Registration Number</label>
                                        <input name="project registration number" class="form-control" radonly type="text" value="{{$data['staff']->project_reg_no}}">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Research Title <span style="color: red;">*</span></label>
                                        <input class="form-control" type="text" name="research_title" value="{{$data['staff']->research_title}}">
                                        <span class="text-danger">{{ $errors->first('research_title') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Field work start date<span style="color: red;">*</span></label>
                                        <input class="form-control" type="date" name="start_date" value="{{$data['staff']->start_date}}">
                                        <span class="text-danger">{{ $errors->first('start_date') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Field work end date<span style="color: red;">*</span></label>
                                        <input class="form-control" type="date" name="end_date" value="{{$data['staff']->end_date}}">
                                        <span class="text-danger">{{ $errors->first('end_date') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                {{-- <!--                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Type of Research <span style="color: red;">*</span></label>
                                            <select class="form-control" name="research_type" id="">
                                                <option value="{{$data['staff']->research_type}}" selected>{{Form::select('research_type', ['' => 'Select Type of Research','Commisioned' => 'Commisioned','Consultancy' => 'Consultancy','Independent study' => 'Independent study',
                                            'Postdoctorial' => 'Postdoctorial','Staff development' => 'Staff development'], old('research_type'), ['class'=> 'form-control'])}}</option>
                                            
                                            <span class="text-danger">{{ $errors->first('research_type') }}</span>
                                            </select>
                                        </div>./form-group 
                                    </div>./col-lg-12 --> --}}
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Type of Research</label>
                                        {{ Form::select('research_type', ['' => '-- Select --', 'Independent study' => 'Independent study', 'Commisioned' => 'Commisioned', 'Senior Lecturer' => 'Senior Lecturer', 'Lecturer' => 'Lecturer', 'Assistant Lecture' => 'Assistant Lecture', 'Tutorial Assistant' => 'Tutorial Assistant'], old('research_type', $data['staff']->research_type), ['class' => 'form-control']) }}
                                        <span class="text-danger">{{ $errors->first('research_type') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Amount of funds <span style="color: red;">*</span></label>
                                        <input class="form-control" id="number_comma" pattern="^[\d,]+$" name="fund_amount" value="{{$data['staff']->fund_amount}}" placeholder="Enter research budget" type="text">
                                        <span class="text-danger">{{ $errors->first('fund_amount') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Source of Research Funds<span style="color: red;">*</span></label>
                                        <input class="form-control" type="text" name="fund_source" value="{{$data['staff']->fund_source}}">
                                        <span class="text-danger">{{ $errors->first('fund_source') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Other UDSM Researchers to be involved(If any)</label>
                                        <div class="container">
                                            <label ><span class="badge badge-warning">NOTE: If other researcher(s) involved, please list the names separated by comma. e.g. Meddie Kagere, Fiston Mayele, Asha Kinyota, and Mohammed Hussein</span> </label>
                                        </div>
                                        <div class="col-sm-12">
                                            <input class="form-control" type="text" name="other_researcher" value="{{$data['staff']->other_researcher}}">
                                            <span class="text-danger">{{ $errors->first('other_researcher') }}</span>
                                            <a href="{{route('clr_st_researchers',['id'=>$data['staff']->id])}}"
                                                class="btn btn-danger btn-xs">clear</a>
                                        </div><!--./form-group -->
                                    </div>
                                </div><!--./col-lg-12 -->
                            </div><!--./row -->

                            {{-- sites Information --}}
                            <div class="my-3" id="research_site">
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h6 class="text-uppercase">Research Site Information </h6>
                                        </div>
                                        <!--./col-lg-12 -->
                                    </div>
                                    <!--./row -->

                                    @foreach ($data['sites'] as $site)
                                    <div class="site my-2">
                                        <div class="row">
                                            <input type="hidden" name="site_count[]" value="site">

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Organization
                                                        <small>(where data will be collected)</small>
                                                        <span style="color: red;">*</span></label>
                                                    <input class="form-control" type="text" name="organization[]" value="{{$site->organization}}">
                                                    <span class="text-danger">{{ $errors->first('organization') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-12 -->
                                        </div>
                                        <!--./row -->
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Title of Addressee<span style="color: red;">*</span></label>
                                                    <input class="form-control" type="text" name="title[]" value="{{$site->title}}">
                                                    <span class="text-danger">{{ $errors->first('title') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-12 -->

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Address<span style="color: red;">*</span></label>
                                                    <input class="form-control" type="text" name="address[]" value="{{$site->address}}">
                                                    <span class="text-danger">{{ $errors->first('address') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-12 -->
                                        </div>
                                        <!--./row -->

                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Region <span style="color: red;">*</span></label>
                                                    <select class="form-control" name="region[]" id="">
                                                        <option value="{{$site['selected_region']->id}}" selected>{{$site['selected_region']->name}}</option>
                                                        @foreach ($data['regions'] as $region)
                                                        <option value="{{$region->id}}">{{$region->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="text-danger">{{ $errors->first('region') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-3 -->

                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>District <span style="color: red;">*</span></label>
                                                    <input class="form-control" type="text" name="district[]" value="{{$site->district}}">
                                                    <span class="text-danger">{{ $errors->first('district') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-3 -->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Ward</label>
                                                    <input class="form-control" type="text" name="ward[]" value="{{$site->ward}}">
                                                    <span class="text-danger">{{ $errors->first('ward') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-3 -->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Village/Street</label>
                                                    <input class="form-control" type="text" name="village[]" value="{{$site->village}}">
                                                    <span class="text-danger">{{ $errors->first('village') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-3 -->
                                        </div>
                                        <!--./row -->
                                    <div class="row">
                                        <div class="row hidebutton">
                                            <div class="col-md-12">
                                                <div class="form-group pull-right">
                                                    <i class="fa fa-minus btn btn-danger removeButton" id="removeButton" title="Remove research site"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @endforeach

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Contract/Agreement </label>
                                        <input class="form-control" type="file" name="contract_attachment">
                                        <span class="text-danger">{{ $errors->first('contract_attachment') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <a href="javascript:{}" onclick="document.getElementById('clearance_form').submit();" type="submit" class="btn btn-primary btn-xs">Submit</a>
                                        <a href="{{url('clearance/staff')}}" class="btn btn-danger btn-xs">Cancel</a>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            {{ Form::close() }}


                        </div>
                        <!--./card-body -->
                    </div>
                    <!--./card -->
                </div>
                <!--./col-lg-12 -->

            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>

<script>
    //get districts
    $('#region').change(function() {
        var region_id = $(this).val();
        if (region_id) {
            $.ajax({
                type: "get",
                url: "{{url('/districts')}}/" + region_id,
                success: function(res) {
                    if (res) {
                        $("#district").empty();
                        $("#ward").empty();
                        $("#village").empty();
                        $("#district").append('<option>Select District</option>');
                        $.each(res, function(key, value) {
                            $("#district").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                }

            });
        }
    });

    //get wards
    $('#district').change(function() {
        var district_id = $(this).val();
        if (district_id) {
            $.ajax({
                type: "get",
                url: "{{url('/wards')}}/" + district_id,
                success: function(res) {
                    if (res) {
                        $("#ward").empty();
                        $("#village").empty();
                        $("#ward").append('<option>Select Ward</option>');
                        $.each(res, function(key, value) {
                            $("#ward").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                }

            });
        }
    });

    //get villages 
    $('#ward').change(function() {
        var ward_id = $(this).val();
        if (ward_id) {
            $.ajax({
                type: "get",
                url: "{{url('/villages')}}/" + ward_id,
                success: function(res) {
                    if (res) {
                        $("#village").empty();
                        $("#village").append('<option>Select Village</option>');
                        $.each(res, function(key, value) {
                            $("#village").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                }

            });
        }
    });
</script>
@endsection