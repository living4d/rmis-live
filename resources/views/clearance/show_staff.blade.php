@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <h4>staff clearance</h4>
                    <ul>
                        <li><a href="{{url('/')}}" title=""><i class="fa fa-home"></i> Home</a></li>
                        <li>Clearance</li>
                    </ul>
                </div><!-- Page Title -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <!-- sidebar -->
                <!-- sidebar -->
                <div class="col-lg-2 col-md-2">
                    @include('clearance.side_menu')
                </div>
                <!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    <div class="card card-flat">
                        <h5 class="card-header text-uppercase">Staff Clearance details</h5>
                        <!--./card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    {{-- <h6 class="title text-uppercase">Staff information</h6> --}}
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->
                            <div class="row p-2">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Full Name </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['staff']['names']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Rank </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['staff']['title']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">College </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['coll_clr']->name}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Department </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['dept_clr']->name}}</span>
                                            </td>
                                        </tr>
                                        {{-- <tr>
                                                <td width="20%"><span class="font-weight-bold">Academic_unit </span></td>
                                                <td width="80%">
                                                    <span class="text-uppercase">{{$data['staff']['academic_unit']}}</span>
                                        </td>
                                        </tr> --}}
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Email </span></td>
                                            <td width="80%">
                                                <span class="">{{$data['staff']['email']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Phone </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['staff']['phone']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Project Registration Number </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['staff']['project_reg_no']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Research Title </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['staff']['research_title']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Research Type </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['staff']['research_type']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Start Date </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['staff']['start_date']}}</span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">End Date </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['staff']['end_date']}}</span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Fund Amount </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['staff']['fund_amount']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Fund Source </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['staff']['fund_source']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Proposal/Agreement attachment </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">
                                                    <a href="{{asset('updloads')}}/{{$data['staff']->agreement_attachment}}" target="_blank" title="Download proposal">
                                                        <i class="fa fa-paperclip fa-lg text-success"></i> Download agreement document
                                                    </a> &nbsp;</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-2"></div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <hr>
                                </div>
                                <!--./col-lg-12 -->
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <h6 class="title text-uppercase">Research Sites</h6>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->
                            <?php $serial = 1; ?>
                            @foreach ($data['sites'] as $site)
                            <div class="row p-2">
                                <div class="col-md-10">
                                    <table class="table table-bordered">
                                        <label for="site_no"><span class="badge badge-success">Site {{$serial}}</span> </label>
                                        {{-- <tr>
                                                        <td width="20%"><span class="font-weight-bold">Location site </span></td>
                                                        <td width="80%">
                                                            <span class="text-uppercase">{{$site['location_site']}}</span>
                                        </td>
                                        </tr> --}}
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Organization </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$site['organization']}}</span>
                                            </td>
                                        </tr>
                                        {{-- <tr>
                                                        <td width="20%"><span class="font-weight-bold">Organization </span></td>
                                                        <td width="80%">
                                                            <span class="text-uppercase">{{$site['organization']}}</span>
                                        </td>
                                        </tr> --}}
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Title </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$site['title']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Address </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$site['address']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Region </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$site['region']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">District </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$site['district']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Ward </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$site['ward']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Village </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$site['village']}}</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-2"></div>
                            </div>

                            <div class="row">
                                {{-- @if (site researcher exists means test is true)
                                <div class="col-lg-6">
                                    <h6 class="title text-uppercase">Assigned Researchers</h6>
                                    <h6>{{$site['researchers']['first_name']}}{{$site['researchers']['surname']}}</h6>
                                </div><!--./col-lg-6 -->
                                @endif --}}
                                <div class="col-lg-6">
                                    <h6 class="title text-uppercase">Researcher assistants</h6>
                                    @foreach ($site['assistants'] as $assist)
                                    <h6>{{$assist->name}}</h6>
                                    @endforeach
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->
                            <?php $serial++; ?>
                            @endforeach

                            <hr>

                            {{--add research site --}}
                             @if ($data['staff']->level === "1")
                            <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group pull-right">
                                                @if (Auth::user()->hasRole("staff"))
                                                <a class="fa fa-plus btn btn-primary addButton" href="{{route('add_staff_clearance_site',['id'=>$data['staff']->id])}}" class="btn btn-primary btn-xs">Add site</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @endif


                            @if ($data['recommendations'])
                            <div class="row">
                                <div class="col-lg-8">
                                    <table width="100%" class="table table-hover table-bordered table-responsive">
                                        <thead>
                                            <tr>
                                                <th width="3%">From</th>
                                                <th width="30%">Comment</th>
                                                <th width="10%">Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data['recommendations'] as $recommend)
                                            <tr>
                                                <td>{{$recommend['from']}}</td>
                                                <td>{{$recommend['comment']}}</td>
                                                <td>{{$recommend['created_at']}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->
                            @endif

                            {{-- actions for level 1 --}}
                            @if ($data['staff']->level === "1")
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        @if (Auth::user()->hasRole("hod"))
                                        <a href="{{route('return_panel',['id'=>$data['staff']->id])}}" class="btn btn-warning btn-xs">RETURN</a>
                                        <a href="{{route('recommendation_panel_staff',['id'=>$data['staff']->id])}}" class="btn btn-primary btn-xs">FORWARD TO PRINCIPAL</a>
                                        @endif
                                        @if (Auth::user()->hasRole("staff"))
                                        <a href="{{route('edit_staff_clearance',['id'=>$data['staff']->id])}}" class="btn btn-primary btn-xs">EDIT</a>
                                        @endif
                                        <a href="{{url('clearance')}}" class="btn btn-danger btn-xs">BACK</a>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            @endif

                            {{-- actions for level 2 --}}

                            @if ($data['staff']->level === "2")
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        @if (Auth::user()->hasRole("principal"))
                                        <a href="{{route('return_panel',['id'=>$data['staff']->id])}}" class="btn btn-warning btn-xs">RETURN</a>
                                        <a href="{{route('recommendation_panel_staff',['id'=>$data['staff']->id])}}" class="btn btn-primary btn-xs">FORWARD TO DRP</a>
                                        @endif
                                        <a href="{{url('clearance')}}" class="btn btn-danger btn-xs">Cancel</a>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            @endif

                            {{-- actions for level 3 --}}

                            @if ($data['staff']->level === "3")
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        @if (Auth::user()->hasRole("drp"))
                                        <a href="{{route('return_panel',['id'=>$data['staff']->id])}}" class="btn btn-warning btn-xs">RETURN</a>
                                        <a href="{{route('recommendation_panel_staff',['id'=>$data['staff']->id])}}" class="btn btn-primary btn-xs">FORWARD TO DVC</a>
                                        @endif
                                        <a href="{{url('clearance')}}" class="btn btn-danger btn-xs">Cancel</a>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            @endif

                            {{-- actions for level 4 --}}
                            @if ($data['staff']->level === "4")
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        @if (Auth::user()->hasRole("dvc"))
                                        <a href="{{route('return_panel',['id'=>$data['staff']->id])}}" class="btn btn-warning btn-xs">RETURN</a>
                                        <a href="{{route('recommendation_panel_staff',['id'=>$data['staff']->id])}}" class="btn btn-primary btn-xs">FORWARD TO VC</a>
                                        @endif
                                        <a href="{{url('clearance')}}" class="btn btn-danger btn-xs">Cancel</a>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            @endif

                            {{-- actions for level 5 --}}
                            @if ($data['staff']->level === "5")
                            @if (Auth::user()->hasRole("vc"))
                            @if ($data['staff']->approval_status !== "approved")
                            {{ Form::open(array('url' =>['clearance/approve_staff','id' => $data['staff']->id],'id' => 'clearance_approve')) }}
                            <input type="hidden" name="_method" value="GET" />
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Approve Clearance </label>
                                        {{Form::select('approve_status', ['' => 'Select Option','approved' => 'Approved','rejected' => 'Rejected'], old('approve_status'), ['class'=> 'form-control'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <a href="javascript:{}" onclick="document.getElementById('clearance_approve').submit();" class="btn btn-primary btn-xs" type="submit">Submit</a>
                                        {{-- <a href="{{url('clearance')}}"
                                        class="btn btn-danger btn-xs">Cancel</a> --}}
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            {{ Form::close() }}
                            @endif
                            @endif

                            @if ($data['staff']->approval_status === "approved")
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <a href="{{route('staff_clearance_letter',['id'=>$data['staff']->id])}}" class="btn btn-primary btn-xs">Clearance letters</a>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->
                            @endif

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <a href="{{url('clearance')}}" class="btn btn-danger btn-xs">Cancel</a>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            @endif

                        </div>
                        <!--./card-body -->
                    </div>
                    <!--./card -->
                </div>
                <!--./col-lg-9 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection