@extends('layouts.app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="page-title">
                        <h4>Clearance</h4>
                        <ul>
                            <li><a href="{{url('/')}}" title=""><i class="fa fa-home"></i> Home</a></li>
                            <li>Description</li>
                        </ul>
                    </div><!-- Page Title -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <section class="bg-white mt-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-flat h-100">
                            <h5 class="card-header text-uppercase">About students Clearance</h5><!--./card-header -->
                            <div class="card-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores cumque
                                    minima nemo repudiandae rerum! Aspernatur at, autem expedita id illum
                                    laudantium molestias officiis quaerat, rem sapiente sint totam velit.
                                    Enim.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores cumque
                                    minima nemo repudiandae rerum! Aspernatur at, autem expedita id illum
                                    laudantium molestias officiis quaerat, rem sapiente sint totam velit.
                                    Enim.</p>
                            </div><!--./card-body -->
                        </div><!--./card -->
                    </div><!--./col-md-4 -->
    
                    <div class="col-md-4">
                        <div class="card card-flat h-100">
                            <h5 class="card-header text-uppercase">Requirements</h5><!--./card-header -->
                            <div class="card-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores cumque
                                    minima nemo repudiandae rerum! Aspernatur at, autem expedita id illum
                                    laudantium molestias officiis quaerat, rem sapiente sint totam velit.
                                    Enim.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores cumque
                                    minima nemo repudiandae rerum! Aspernatur at, autem expedita id illum
                                    laudantium molestias officiis quaerat, rem sapiente sint totam velit.
                                    Enim.</p>
                            </div><!--./card-body -->
                        </div><!--./card -->
                    </div><!--./col-md-4 -->
    
                    <div class="col-md-4">
                        <a class="btn btn-primary btn-block text-uppercase"
                        href="{{url('clearance/students')}}">Continue <i
                                 class="fa fa-chevron-circle-right"></i></a>
                    </div><!--./col-md-4 -->
                </div><!--./row -->
            </div><!--./container -->
        </section><!--./section -->
    </section>
@endsection