@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <h4>Research Clearance form</h4>
                </div><!-- Page Title -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <!-- sidebar -->
                <div class="col-lg-2 col-md-2">
                    @include('clearance.side_menu')
                </div>
                <!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div><br />
                    @elseif(session()->get('danger'))
                    <div class="alert alert-danger">
                        {{ session()->get('danger') }}
                    </div><br />
                    @endif

                    <div class="card card-flat">
                        <h5 class="card-header">RESEARCH CLEARANCE FORM </h5>
                        <!--./card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h6 class="title text-uppercase">Student Information</h6>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->
                            {{ Form::open(array('url' =>['clearance/update/std_clearance/admin','id' => $data['student']->id],'id' => 'clearance_form','files'=>'true')) }}

                            <b>Full Name: </b>{{$data['user']}}<br />
                            <b>Reg Number: </b>{{$data['reg_number']}}<br />
                            <b>Gender: </b>{{$data['gender']}}<br />
                            {{-- <input type="hidden" name="_method" value="post" />
                                <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Full Name <span style="color: red;">*</span></label>
                                        @php($name = $data['user'])
                                        <input name="names" class="form-control" readonly type="text" value="{{$name}}">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Gender </label>
                                        <input name="gender" class="form-control" readonly type="text" value="{{$data['gender']}}">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>           --}}

                            {{-- <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Department </label>
                                        <input name="department" class="form-control" readonly type="text" value="{{$data['dept']}}">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>College </label>
                                        <input name="college" class="form-control" readonly type="text" value="{{$data['college']}}">
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div> --}}
                            <!--./row -->

                            <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>College <span style="color: red;">*</span></label>
                                            <select name="college_id" class="form-control" id="">
                                            <option value="{{$data['college']->id}}" selected>{{$data['college']->name}}</option>
                                                @foreach ($data['college_pool'] as $college)
                                                    <option value="{{$college->id}}">{{$college->name}}  </option>
                                                @endforeach
                                            </select>
                                            <span class="text-danger">{{ $errors->first('college_id') }}</span>
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-12 -->

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Department <span style="color: red;">*</span></label>
                                            <select name="department_id" class="form-control" id="">
                                            <option value="{{$data['dept']->id}}" selected>{{$data['dept']->name}}</option>
                                                @foreach ($data['dept_pool'] as $department)
                                                    <option value="{{$department->id}}">{{$department->name}}  </option>
                                                @endforeach
                                            </select>
                                            <span class="text-danger">{{ $errors->first('department_id') }}</span>
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-12 -->
                            </div>
                            <!--./row -->
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Nationality<span style="color: red;">*</span></label>
                                        <input class="form-control" type="text" name="nationality" value="{{$data['student']->nationality}}">
                                        <span class="text-danger">{{ $errors->first('nationality') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Contact details <span style="color: red;">*</span></label>
                                        <input class="form-control" type="text" name="contact" value="{{$data['student']->contact}}">
                                        <span class="text-danger">{{ $errors->first('contact') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Degree Programme<span style="color: red;">*</span></label>
                                        <input class="form-control" type="text" name="degree_program" value="{{$data['student']->degree_program}}">
                                        <span class="text-danger">{{ $errors->first('degree_program') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Delivery mode <span style="color: red;">*</span></label>
                                        <select class="form-control" name="delivery_mode" id="">
                                            <option value="{{$data['student']->delivery_mode}}" selected>{{$data['student']->delivery_mode}}</option>
                                            <option value="by thesis">By thesis</option>
                                            <option value="by coursework and dissertation">By Coursework and Dissertation</option>
                                        </select>
                                        <span class="text-danger">{{ $errors->first('delivery_mode') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Year of Entry (eg. 2020)<span style="color: red;">*</span></label>
                                        <input class="form-control" type="text" name="entry_year" value="{{$data['student']->year_entry}}">
                                        <span class="text-danger">{{ $errors->first('entry_year') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Duration(in month)<span style="color: red;">*</span></label>
                                        <input class="form-control" type="text" name="duration" value="{{$data['student']->duration}}">
                                        <span class="text-danger">{{ $errors->first('duration') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Programme mode <span style="color: red;">*</span></label>
                                        <select class="form-control" name="program_mode" id="">
                                            <option value="{{$data['student']->program_mode}}" selected>{{$data['student']->program_mode}}</option>
                                            <option value="full time">Full time</option>
                                            <option value="evening">Evening</option>
                                            <option value="executive">Executive</option>
                                        </select>
                                        <span class="text-danger">{{ $errors->first('program_mode') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->



                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Supervisor’s details<span style="color: red;">*</span></label>
                                        <select name="supervisor" class="form-control">
                                            <option value="{{$data['student']->supervisor}}" selected>{{$data['supervisor']['first_name']}} {{$data['supervisor']['middle_name']}} {{$data['supervisor']['surname']}}</option>
                                            @foreach ($data['staff_details'] as $visor)
                                            <option value="{{$visor['id']}}">{{$visor['first_name']}} {{$visor['middle_name']}} {{$visor['surname']}}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{ $errors->first('supervisor') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <h6 class="title text-uppercase">Research Clearance Information</h6>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Research Title <span style="color: red;">*</span></label>
                                        <input class="form-control" type="text" name="research_title" value="{{$data['student']->research_title}}">
                                        <span class="text-danger">{{ $errors->first('research_title') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Type of Research <span style="color: red;">*</span></label>
                                        <select class="form-control" name="research_type">
                                            {{-- <option value="{{$data['student']->research_type}}">Select research type</option> --}}
                                            <option value="{{$data['student']->research_type}}" selected>{{$data['research_type']['name']}}</option>
                                            @foreach ($data['research_types'] as $type)
                                            <option value="{{$type['id']}}">{{$type['name']}}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{ $errors->first('research_type') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Date of commencement<span style="color: red;">*</span></label>
                                        <input class="form-control" type="date" name="commencement_date" value="{{$data['student']->commencement_date}}">
                                        <span class="text-danger">{{ $errors->first('commencement_date') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Date of completion<span style="color: red;">*</span></label>
                                        <input class="form-control" type="date" name="completion_date" value="{{$data['student']->completion_date}}">
                                        <span class="text-danger">{{ $errors->first('completion_date') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Research Budget <span style="color: red;">*</span></label>
                                        <input class="form-control" id="number_comma" pattern="^[\d,]+$" name="research_budget" value="{{$data['student']->research_budget}}" placeholder="Enter research budget" type="text">
                                        <span class="text-danger">{{ $errors->first('research_budget') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Source of Research Funds<span style="color: red;">*</span></label>
                                        <input class="form-control" type="text" name="fund_source" value="{{$data['student']->fund_source}}">
                                        <span class="text-danger">{{ $errors->first('fund_source') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->

                            </div>
                            <!--./row -->

                            {{-- sites Information --}}
                            <div class="my-3" id="research_site">
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h6 class="text-uppercase"><small>Research Site Information</small> </h6>
                                        </div>
                                        <!--./col-lg-12 -->
                                    </div>
                                    <!--./row -->

                                    @foreach ($data['sites'] as $site)
                                    <div class="site my-2">
                                        <div class="row">
                                            <input type="hidden" name="site_count[]" value="site">

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Organization
                                                        <small>(where data will be collected)</small>
                                                        <span style="color: red;">*</span></label>
                                                    <input class="form-control" type="text" name="organization[]" value="{{$site->organization}}">
                                                    <span class="text-danger">{{ $errors->first('organization') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-12 -->
                                        </div>
                                        <!--./row -->
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Title of Addressee<span style="color: red;">*</span></label>
                                                    <input class="form-control" type="text" name="title[]" value="{{$site->title}}">
                                                    <span class="text-danger">{{ $errors->first('title') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-12 -->

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Address<span style="color: red;">*</span></label>
                                                    <input class="form-control" type="text" name="address[]" value="{{$site->address}}">
                                                    <span class="text-danger">{{ $errors->first('address') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-12 -->
                                        </div>
                                        <!--./row -->

                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Region <span style="color: red;">*</span></label>
                                                    <select class="form-control" name="region[]" id="">
                                                        <option value="{{$site['selected_region']->id}}" selected>{{$site['selected_region']->name}}</option>
                                                        @foreach ($data['regions'] as $region)
                                                        <option value="{{$region->id}}">{{$region->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="text-danger">{{ $errors->first('region') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-3 -->

                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>District <span style="color: red;">*</span></label>
                                                    <input class="form-control" type="text" name="district[]" value="{{$site->district}}">
                                                    <span class="text-danger">{{ $errors->first('district') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-3 -->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Ward</label>
                                                    <input class="form-control" type="text" name="ward[]" value="{{$site->ward}}">
                                                    <span class="text-danger">{{ $errors->first('ward') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-3 -->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Village/Street</label>
                                                    <input class="form-control" type="text" name="village[]" value="{{$site->village}}">
                                                    <span class="text-danger">{{ $errors->first('village') }}</span>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                            <!--./col-lg-3 -->
                                        </div>
                                        <!--./row -->

                                        <div class="row hidebutton">
                                            <div class="col-md-12">
                                                <div class="form-group pull-right">
                                                    <i class="fa fa-minus btn btn-danger removeButton" id="removeButton" title="Remove research site"></i>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    @endforeach

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group pull-right">
                                                {{-- @if (Auth::user()->hasRole("student")) --}}
                                                @if (Auth::user()->hasRole("admin"))
                                                <a class="fa fa-plus btn btn-primary addButton" href="{{route('add_student_clearance_site_edit',['id'=>$data['student']->id])}}" class="btn btn-primary btn-xs">Add site</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            {{-- <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Approved Research proposal </label>
                                        <input class="form-control" type="file" name="proposal_attachment">
                                        <span class="text-danger">{{ $errors->first('proposal_attachment') }}</span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Student ID </label>
                                        <input class="form-control" type="file" name="student_id">
                                        <span class="text-danger">{{ $errors->first('student_id') }}</span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Certified student financial statement </label>
                                        <input class="form-control" type="file" name="financial_statement">
                                        <span class="text-danger">{{ $errors->first('financial_statement') }}</span>
                                    </div>
                                </div>
                            </div> --}}

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <a href="javascript:{}" onclick="document.getElementById('clearance_form').submit();" type="submit" class="btn btn-primary btn-xs">Submit</a>
                                        <a href="{{url('clearance/students')}}" class="btn btn-danger btn-xs">Cancel</a>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            {{ Form::close() }}


                        </div>
                        <!--./card-body -->
                    </div>
                    <!--./card -->
                </div>
                <!--./col-lg-12 -->

            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>

<script>
    //get districts
    $('#region').change(function() {
        var region_id = $(this).val();
        if (region_id) {
            $.ajax({
                type: "get",
                url: "{{url('/districts')}}/" + region_id,
                success: function(res) {
                    if (res) {
                        $("#district").empty();
                        $("#ward").empty();
                        $("#village").empty();
                        $("#district").append('<option>Select District</option>');
                        $.each(res, function(key, value) {
                            $("#district").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                }

            });
        }
    });

    //get wards
    $('#district').change(function() {
        var district_id = $(this).val();
        if (district_id) {
            $.ajax({
                type: "get",
                url: "{{url('/wards')}}/" + district_id,
                success: function(res) {
                    if (res) {
                        $("#ward").empty();
                        $("#village").empty();
                        $("#ward").append('<option>Select Ward</option>');
                        $.each(res, function(key, value) {
                            $("#ward").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                }

            });
        }
    });

    //get villages 
    $('#ward').change(function() {
        var ward_id = $(this).val();
        if (ward_id) {
            $.ajax({
                type: "get",
                url: "{{url('/villages')}}/" + ward_id,
                success: function(res) {
                    if (res) {
                        $("#village").empty();
                        $("#village").append('<option>Select Village</option>');
                        $.each(res, function(key, value) {
                            $("#village").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                }

            });
        }
    });
</script>
@endsection