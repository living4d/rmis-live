@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <h4>Research Clearance form</h4>
                </div><!-- Page Title -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <!-- sidebar -->
                <div class="col-lg-2 col-md-2">
                    @include('clearance.side_menu')
                </div>
                <!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div><br />
                    @elseif(session()->get('danger'))
                    <div class="alert alert-danger">
                        {{ session()->get('danger') }}
                    </div><br />
                    @endif

                    <div class="card card-flat">
                        <h5 class="card-header">RESEARCH SITE FORM </h5>
                        <!--./card-header -->
                        <div class="card-body">

                            {{ Form::open(['url' => route('store_staff_added_site', ['id' => $staff_clearance->id]), 'method="POST" class="form-horizontal"']) }}
                            {{ Form::token() }}
                            <div class="container">
                                <label><span class="badge badge-warning">NOTE: RAS and DAS letters are generated automatically, do not fill them as addressee</span> </label>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Organization
                                            <small>(where data will be collected)</small>
                                            <span style="color: red;">*</span></label>
                                        {{Form::text('organization[]', old('organization[]'), ['class="form-control" rows="5"'])}}
                                        <span class="text-danger">{{ $errors->first('organization') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Title of Addressee <small>(e.g. Director, Manager, District Executive Director etc)</small><span style="color: red;">*</span></label>
                                        {{Form::text('title[]', old('title[]'), ['class="form-control" rows="5"'])}}
                                        <span class="text-danger">{{ $errors->first('title') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Address <span style="color: red;">*</span></label>
                                        {{Form::text("address", old('address'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('address') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">

                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>Region<span style="color: red;">*</span></label>
                                        <select name="region" class="form-control" id="">
                                            <option value="region">Select region</option>
                                            @foreach ($data['regions'] as $region)
                                            <option value="{{$region['id']}}">{{$region['name']}} </option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{ $errors->first('region') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->

                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>District <span style="color: red;">*</span></label>
                                        {{Form::text('district[]', old('district[]'), ['class="form-control" '])}}
                                        <span class="text-danger">{{ $errors->first('district') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-3 -->
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>Ward</label>
                                        {{Form::text('ward[]', old('ward[]'), ['class="form-control" '])}}
                                        <span class="text-danger">{{ $errors->first('ward') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-3 -->
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>Village/Street</label>
                                        {{Form::text('village[]', old('village[]'), ['class="form-control" '])}}
                                        <span class="text-danger">{{ $errors->first('village') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-3 -->
                            </div>
                            <!--./row -->

                            <div class="row hidebutton">
                                <div class="col-md-12">
                                    <div class="form-group pull-right">
                                        <i class="fa fa-minus btn btn-danger removeButton" id="removeButton" title="Remove research site">Remove Site</i>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-secondary text-medium">Submit</button>
                                        <a href="{{url('clearance/students')}}" class="btn btn-danger btn-xs">Cancel</a>
                                    </div>
                                </div>

                            </div>
                            <!--./row -->
                            {{ Form::close() }}


                        </div>
                        <!--./card-body -->
                    </div>
                    <!--./card -->
                </div>
            </div>
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>

@endsection