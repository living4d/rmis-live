@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4> Clearance Permits Summary</h4>
                            <ul>
                                <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li>Permits Summary</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('clearance.side_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="search-form">

                                    {!! Form::open(['url' => route('permit_report', $data), 'method' => 'POST', 'class' => 'form-horizontal']) !!}
                                    {{ Form::token() }}

                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 col-12">
                                            <div class="form-group">
                                                <label>Report Type</label>
                                                @php
                                                    $_options = [
                                                        'periodic' => 'Periodic',
                                                        'combine' => 'Combine',
                                                    ];
                                                @endphp
                                                {{ Form::select('report_type', $_options, old('report_type'), ['class="form-control"']) }}
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-md-3 -->

                                        <div class="col-md-3 col-sm-3 col-12">
                                            <div class="form-group">
                                                <label>Clearance Type</label>
                                                @php
                                                    $_options = [
                                                        'staff' => 'Staff clearances',
                                                        'student' => 'Student clearances',
                                                    ];
                                                @endphp
                                                {{ Form::select('clearance_type', $_options, old('clearance_type'), ['class="form-control"']) }}
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                        <!--./col-md-3 -->

                                        <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>From Date <span style="color: red;">*</span></label>
                                            {{Form::text('date_from', old('date_from'), ['class="form-control" type="date" rows="5"'])}}
                                            <span class="text-danger">{{ $errors->first('date_from') }}</span>
                                        </div><!--./form-group -->
                                        </div>
                                        <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>To Date <span style="color: red;">*</span></label>
                                            {{Form::text('date_to', old('date_to'), ['class="form-control" type="date" rows="5"'])}}
                                            <span class="text-danger">{{ $errors->first('date_to') }}</span>
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-12 -->

                                        <div class="col-md-2 col-sm-2 col-12">
                                            <div class="pull-left">
                                                <div class="form-group">
                                                    <label>&nbsp;&nbsp;&nbsp;</label><br />
                                                    <button style="float:right" type="submit" name="submit"
                                                        class="btn btn-primary"> Generate
                                                    </button>
                                                </div>
                                                <!--./form-group -->
                                            </div>
                                        </div>
                                        <!--./col-md-->
                                    </div>
                                    <!--./row -->
                                    {{ Form::close() }}
                                </div>
                                <!--./search-form -->
                            </div>
                            <!--./col-md-12 -->
                        </div>
                        <!--./row -->

                        {{-- @if (($data['date_from'] != null) || $data['report_type'] == 'periodic') --}}

                            <div class="row mt-3">
                                <div class="col-md-12">

                                    <button class="btn btn-success mb-2" onclick="printDiv('printableArea')">
                                        <i class="fa fa-print" aria-hidden="true"></i> Print
                                    </button>

                                    <div class="btn-group ml-2">
                                        <button type="button" class="btn btn-default">Bulk Actions</button>
                                        <button type="button" class="btn btn-default dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <div class="dropdown-menu" role="menu">
                                            {{-- <a wire:click.prevent = "export" class="dropdown-item" href="/publications/publVet/export/">Export</a> --}}
                                            <a wire:click.prevent = "export" class="dropdown-item" href="#">Export</a>
                                        </div>
                                    </div>

                                    <div id="printableArea" style="background-color: #FFFFFF; padding:40px;">

                                        <div class="row container">
                                            <div class="col-lg-12">
                                                
                                                {{-- <div id="letter-content">

                                                    @if ($data['report_type'] == 'batch')
                                                        <div class="mt-3">
                                                            <h6 class="text-uppercase">
                                                                The Batch of this matrix:
                                                            </h6>
                                                        </div>

                                                        <table class="">
                                                            <tr>
                                                                <td width="22%" valign="top"><b>Batch number:</b></td>
                                                                <td width="80%">
                                                                    {{ $data['batch'] }}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="22%" valign="top"><b>Category:</b></td>
                                                                <td width="80%">
                                                                    {{ $data['category'] }}
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="mt-3"></div>
                                                    @endif
                                                </div> --}}

                                                <div>
                                                    <h3 id="re-title">
                                                        CLEARANCE PERMITS SUMMARY
                                                    </h3>
                                                </div>
                                                <div>
                                                    <table class="table table-hover table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th width="3%">#</th>
                                                                <th width="47%">College</th>
                                                                <th width="15%">Clearance</th>
                                                            </tr>
                                                        </thead>

                                                        @php
                                                            $serial = 1;
                                                            $total = 0;
                                                        @endphp
                                                         
                                                            @foreach ($data['permit_summary'] as $values)
                                                                <tr>
                                                                    <td>{{ $serial }}</td>
                                                                    <td>
                                                                        {{ $values->College }}
                                                                    </td>
                                                                    <td>
                                                                        {{ $values->Clearance }}
                                                                    </td>
                                                                </tr>
                                                                @php
                                                                    $serial = $serial+1;
                                                                    $total = $total+$values->Clearance;
                                                                @endphp
                                                            @endforeach

                                                        
                                                        <tr>
                                                            <td>#</td><td><b>TOTAL</b></td><td><b>{{$total}}</b></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <!--./col -->
                                        </div>
                                        <!--./row -->
                                    </div>
                                    <!--./print-content -->

                                </div>
                                <!--./col-md-12 -->
                            </div>
                            <!--./row -->
                        {{-- @endif --}}
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>

    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
@endsection
