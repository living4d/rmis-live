@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="page-title">
                        <h4>Letters</h4>
                        <ul>
                            <li><a href="{{url('/')}}" title=""><i class="fa fa-home"></i> Home</a></li>
                            <li>Clearance letter</li>
                        </ul>
                    </div><!-- Page Title -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <label ><span class="badge badge-warning">You are strongly advised to print the required letters in a colour printer</span> </label>
                <div class="row">
                    @if (count($data['sites'])>0)
                    
                        @php
                            $serial = 1;
                        @endphp
                        @foreach ($data['sites'] as $site)
                            <label for="site_no"><span class="badge badge-success">Site {{$serial}}</span> </label>
                            <br>
                            <div class="col-md-12">
                                <a href="{{route('ras_student_letter',['id'=>$site->id])}}" class="btn btn-primary">RAS</a>
                                <a href="{{route('das_student_letter',['id'=>$site->id])}}" class="btn btn-primary">DAS</a>
                                <a href="{{route('org_student_letter',['id'=>$site->id])}}" class="btn btn-primary">Organization</a>
                                <a href="{{route('twmc_student_letter',['id'=>$site->id])}}" class="btn btn-primary">TWMC</a>
                            </div>
                            @php
                                $serial++;
                            @endphp
                        @endforeach
                    @else
                        <table class="table table-bordered">
                            <tr>
                                <td width="10%"><label for="">No research sites</label></td>
                            </tr>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection