@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <h4>Send Notification</h4>
                        <ul>
                            <li><a href="#" title="">Dashboard</a></li>
                            <li><a href="{{url('/notification')}}" title="">Notifications</a></li>
                            <li>Send Notification</li>
                        </ul>
                    </div><!-- Page Title -->
                </div>
                <!--./col-md-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    @include('notification.sidebar_menu')
                </div>
                <!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    <div class="card card-flat">
                        <h5 class="card-header">Send Notification</h5>
                        <!--./card-header -->
                        <div class="card-body">
                
                            @if(session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                            @endif
                
                            {{ Form::open(['url' => url('notification/store-message'), 'method' => 'POST', 'class' => 'form-horizontal']) }}
                            {{ Form::token() }}

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Message <span style="color: red;">*</span></label>
                                        {{Form::textarea("message", old('message'), ['class="form-control" rows="4" placeholder="Write message..."'])}}
                                        <span class="text-danger">{{ $errors->first('message') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div><!--./col-lg-12 -->
                            </div><!--./row -->
                
                            <div class="row">
                                <div class="col-md-12">
                                    @if (isset($colleges) && count($colleges) > 0)
                                    <table>
                                        <tr>
                                            @php $serial = 0; @endphp
                                            @foreach ($colleges as $college)
                                            @if (($serial % 1) === 0)
                                        </tr>
                                        <tr>
                                            @endif
                                            <td valign="top">
                                                {{Form::checkbox('college_ids[]', $college->id)}}
                                                <label class="font-weight-bold">{{$college->name}}</label><br />
                                                @if(isset($college->departments) && count($college->departments) > 0)
                                                @foreach($college->departments as $department)
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;{{Form::checkbox('department_ids[]', $department->id)}}
                                                &nbsp;&nbsp;<label>{{$department->name}}</label><br />
                                                @endforeach
                                                @endif
                                            </td>
                                            <div class="mt-3"></div>
                                            @php $serial++; @endphp
                                            @endforeach
                                        </tr>
                                    </table>
                                    <span class="text-danger">{{ $errors->first('college_ids') }}</span>
                                    @endif
                                </div><!-- ./col-sm-12 -->
                            </div><!-- ./row -->
                
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-secondary btn-xs text-medium">Send</button>
                                        <a href="{{url('/notication')}}" class="btn btn-danger btn-xs text-medium">Cancel</a>
                                    </div>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->
                            {{ Form::close() }}
                        </div>
                        <!--./card-body -->
                    </div>
                    <!--./card -->
                </div>
                <!--./col-lg-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection