@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Notification</h4>
                            <ul>
                                <li><a href="#" title="">Dashboard</a></li>
                                <li>Notification</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div><!--./col-md-12 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('notification.sidebar_menu')
                    </div><!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">

                        @if(session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br/>
                        @elseif(session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div><br/>
                        @endif

                        @if (isset($notification) && count($notification) > 0)
                            <table id="myTable" class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th width="3%">#</th>
                                    <th width=18%">Sent By</th>
                                    <th width="12%">Sent Time</th>
                                    <th width="50%">Message</th>
                                    <th width="12%">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($serial = 1)
                                @foreach($notification as $values)
                                    <tr>
                                        <td>{{$serial}}</td>
                                        <td>{{$values->user->first_name . ' ' . $values->user->surname}}</td>
                                        <td>{{ date('j M, Y H:i:s', strtotime($values->created_at)) }}</td>
                                        <td>{!! $values->message !!}</td>
                                        <td>
                                            @if ($values->app_status == 'PENDING' || $values->app_status == 'SENT')
                                                <a href="{{url('notification/change-status/' . $values->id)}}">
                                                   <span class="badge badge-danger"> MARK AS READ </span>
                                                </a>
                                            @elseif ($values->app_status == 'DELIVERED')
                                                <span class="badge badge-success">READ</span>
                                            @endif
                                        </td>
                                    </tr>
                                    @php ($serial++)
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-danger"> No any notification found.</div>
                        @endif
                    </div><!--./col-lg-9 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
@endsection