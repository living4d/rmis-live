<div id="left_menu">
    <ul id="nav">
        <?php
        if (\App\Perms::perm_class('notification')) {
            echo '<li><a href="' . url('notification') . '">Inbox</a></li>';
            echo '<li><a href="'.url('notification/outbox').'">Outbox</a></li>';
            echo '<li><a href="'.url('notification/send').'">Send</a></li>';
        }
        ?>
    </ul>
</div>