@extends('layouts.app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-lg-10">
                    <div class="page-title">
                        <h4>{{$call->title}}</h4>
                        <ul>
                            <li><a href="{{url('/')}}" title=""> Home</a></li>
                            <li><a href="{{url('/proposals')}}" title=""> Open Calls</a></li>
                            <li>{{$call->title}}</li>
                        </ul>
                    </div><!-- Page Title -->
                </div>
                <!--./col-lg-10 -->

                @auth
                <div class="col-lg-2">
                    <div class="pull-right">
                        <a href="{{url('dashboard')}}" class="btn btn-secondary btn-sm"> Dashboard <i
                                class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
                <!--./col-lg-2 -->
                @endauth
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <!-- sidebar -->
                <div class="col-lg-12 col-md-12">
                    <span class="pull-right">
                        @guest
                        <a href="{{url('/login')}}" class="btn btn-outline-primary">Apply For Call</a>
                        @endguest

                        @auth
                        <a href="{{url('call_applications/apply/'.$call->id)}}" class="btn btn-outline-primary">Apply
                            For Call</a>
                        @endif
                    </span>

                    <div class="row p-2">
                        <div class="col-md-12 mt-2">
                            <h5 class="text-uppercase">{{$call->title}}</h5>
                        </div>
                        <!--./col-md-12 -->

                        <div class="col-md-12 mt-3">
                            <h5>1. Introduction</h5>
                            <p>{{strip_tags($call->introduction,'')}}</p>
                        </div>
                        <!--./col-md-12 -->

                        <div class="col-md-12 mt-3">
                            <h5>2. Categories of the calls</h5>
                            @foreach($call_categories as $category)
                            <strong>{{$category->title}}</strong>
                            <p>{{strip_tags($category->description, '')}}</p>
                            @endforeach
                        </div>
                        <!--./col-md-12 -->

                        <div class="col-md-12 mt-3">
                            <h5>3. Research Areas</h5>
                            @php($serial = 1)
                            @foreach($call_clusters as $call_cluster)
                            <h6 class="mt-2">{{'Cluster '.$serial .': '. $call_cluster->cluster->name}}</h6>
                            <strong>Research Areas: </strong>
                            @foreach($call_cluster->research_areas as $research_area)
                            {!! '<i>'.$research_area->name.'</i>;'!!}
                            @endforeach
                            @php($serial++)
                            @endforeach
                        </div>
                        <!--./col-md-12 -->

                        <div class="col-md-12 mt-3">
                            <h5>4. Team Composition</h5>
                            <p>{{strip_tags($call->team_composition, '')}}</p>
                        </div>
                        <!--./col-md-12 -->

                        <div class="col-md-12 mt-3">
                            <h5>5. Eligibility</h5>
                            <p>{{strip_tags($call->eligibility, '')}}</p>
                        </div>
                        <!--./col-md-12 -->

                        <div class="col-md-12">
                            <h5>6. Budget Ceiling</h5>
                            <p>The maximum budget for each proposal should not exceed the amounts
                                stated below:
                            </p>
                            @foreach($call_categories as $category)
                            <p>{{$category->title}}
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$category->currency.' '.number_format($category->budget_ceiling)}}
                            </p>
                            @endforeach
                        </div>
                        <!--./col-md-12 -->

                        <div class="col-md-12">
                            <h5>7. Project Duration</h5>
                            <p>{{strip_tags($call->duration, '')}}</p>
                        </div>
                        <!--./col-md-12 -->

                        <div class="col-md-12">
                            <h5>8. Application Details</h5>
                            <p>{{strip_tags($call->application_details, '')}}</p>
                        </div>
                        <!--./col-md-12 -->

                        <div class="col-md-12 mt-3">
                            <h5>9. Deliverable</h5>
                            <p>{{strip_tags($call->deliverable, '')}}</p>
                        </div>
                        <!--./col-md-12 -->

                        <div class="col-md-12 mt-3">
                            <h5>10. Evaluation Criteria</h5>
                            <p>{{strip_tags($call->evaluation_criteria, '')}}</p>
                        </div>
                        <!--./col-md-12 -->

                        <div class="col-md-12 mt-3">
                            <h5>11. Application Deadline</h5>
                            <p>{{date('l, jS F, Y', strtotime($call->deadline))}} at 23:00 hours</p>
                        </div>
                        <!--./col-md-12 -->
                    </div>
                    <!--./row -->
                </div>
                <!--./col-lg-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection