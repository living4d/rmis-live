@extends('layouts.app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title">
                        <h4>Confirm on Call Application Participation</h4>
                        <ul>
                            <li><a href="{{url('/')}}" title="">Home</a></li>
                            <li><a href="{{url('/proposals')}}" title=""> Open Calls</a></li>
                            <li>{{$call->title}}</li>
                        </ul>
                    </div><!-- Page Title -->
                </div>
                <!--./col-lg-10 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <!-- sidebar -->
                <div class="col-lg-12 col-md-12">
                    <div class="card card-flat pure-form">
                        <div class="card-body">
                            <h5 class="text-uppercase">Confirm on Call Application Participation</h5>
                            <hr />

                            @if(session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                            @elseif (session()->get('info'))
                            <div class="alert alert-info">
                                {{ session()->get('info') }}
                            </div>
                            @elseif (session()->get('danger'))
                            <div class="alert alert-danger">
                                {{ session()->get('danger') }}
                            </div>
                            @endif

                            {{ Form::open(['url' => url('proposals/save-confirmation'), 'method="POST"']) }}
                            {{ Form::token() }}
                            {{Form::hidden('call_id', $call->id, ['id' => 'call_id'])}}
                            {{Form::hidden('app_id', $call_app->id, ['id' => 'app_id'])}}

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Title of the Call</label>
                                        {{Form::textarea('project_title', $call->title, ['class="form-control" rows="2" readonly=""'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Title of the Research Project</label>
                                        {{Form::textarea('project_title', $call_app->project_title, ['class="form-control" rows="2" readonly=""'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Principal Investigator (PI)</label>
                                        {{Form::text('project_title', $user->first_name.' '.$user->middle_name.' '.$user->surname, ['class="form-control" readonly=""'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Principal Investigator (PI) Email</label>
                                        {{Form::text('project_title', $user->email, ['class="form-control" readonly=""'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-md-12">
                                    <h5>Confirmation</h5>
                                </div>
                                <!--./col-md-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Email <span style="color: red;">*</span></label>
                                        {{Form::text('email', old('email'), ['class="form-control" placeholder="Write email..."'])}}
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Verdict <span style="color: red;">*</span></label>
                                        @php($_options = ['' => '-- Select --', 'agree' => 'Agree', 'reject' => 'Reject'])
                                        {{Form::select('verdict', $_options, old('verdict'), ['class="form-control" '])}}
                                        <span class="text-danger">{{ $errors->first('verdict') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Comments <span style="color: red;">*</span></label>
                                        {{Form::textarea('comments', old('comments'), ['class="form-control" rows="3" placeholder="Write comments..."'])}}
                                        <span class="text-danger">{{ $errors->first('comments') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                                </div>
                                <!--./row -->



                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group pull-left">
                                        <a href="{{url('/')}}" class="btn btn-danger btn-xs text-medium">Cancel</a>

                                        <button type="submit"
                                            class="btn btn-success btn-xs text-medium">Confirm</button>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            {{ Form::close() }}

                        </div>
                        <!--./card-body -->
                    </div>
                    <!--./card -->
                </div>
                <!--./col-lg-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection