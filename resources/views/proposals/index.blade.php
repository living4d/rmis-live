@extends('layouts.app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10">
                        <div class="page-title">
                            <h4>Open Calls</h4>
                            <ul>
                                <li><a href="{{url('/')}}" title=""> Home</a></li>
                                <li>Open Calls</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div><!--./col-lg-10 -->

                    @auth
                        <div class="col-lg-2">
                            <div class="pull-right">
                                <a href="{{url('dashboard')}}"
                                   class="btn btn-secondary btn-sm"> Dashboard <i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div><!--./col-lg-2 -->
                    @endauth
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <!-- sidebar -->
                    <div class="col-lg-6 col-md-6">
                        @foreach($calls as $call)
                            <div class="item_wr">
                                <div class="content">
                                    <h6>
                                        <a href="{{url('proposals/details/'.$call->id)}}">
                                            {{$call->title}}
                                        </a>
                                    </h6>

                                    <ul class="meta">
                                        <li>
                                            <div><i class="fa fa-calendar"
                                                    aria-hidden="true"></i>Posted
                                                on {{date('M d, Y', strtotime($call->created_at))}}
                                            </div>
                                        </li>

                                        <li>
                                            <div>Deadline : {{date('M d, Y', strtotime($call->deadline))}}</div>
                                        </li>

                                        <li>
                                            <div>
                                                <a href="{{url('proposals/details/'.$call->id)}}"
                                                   class="font-weight-bold text-primary">Apply
                                                    For Call</a>
                                            </div>
                                        </li>
                                    </ul><!--./stm-event__meta -->
                                </div><!--./content -->
                            </div><!--./item_wr -->
                        @endforeach
                    </div><!--./col-lg-9 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
@endsection