@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="page-title">
                        <h4>Review Publications</h4>
                        <ul>
                            <li><a href="{{url('/')}}" title=""><i class="fa fa-home"></i> Home</a></li>
                            <li><a href="{{url('publications')}}" title="">Publications</a></li>
                            <li>Reviewers</li>
                        </ul>
                    </div><!-- Page Title -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <!-- sidebar -->
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-flat">
                            <h5 class="card-header">ASSIGN PUBLICATION REVIEWERS</h5><!--./card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="title text-uppercase"></h6>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->
                                {{ Form::open(array('url' =>['publication/save_reviewer','id' => $data['publication']->id],'id' => 'publication_final')) }}
                                <input type="hidden" name="_method" value="GET"/>
                                
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Reviewer 1 <span style="color: red;">*</span></label>
                                            <select name="reviewer1" class="form-control" id="">
                                                <option value="">Choose first reviewer</option>
                                                @foreach ($data['users'] as $reviewer)
                                                    <option value="{{$reviewer['id']}}">{{$reviewer['first_name']}}   {{$reviewer['surname']}}</option>
                                                @endforeach
                                            </select>
                                            {{-- {{Form::select('reviewer1', ['' => 'Choose first reviewer',
                                                                        '1' => 'Jose Mtambo',
                                                                        '2' => 'Unju bin Unuki',
                                                                        ], old('reviewer1'), ['class'=> 'form-control pub_type'])}} --}}
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-6 -->
                                </div><!--./row -->
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Reviewer 2 <span style="color: red;">*</span></label>
                                            <select name="reviewer2" class="form-control" id="">
                                                <option value="">Choose first reviewer</option>
                                                @foreach ($data['users'] as $reviewer)
                                                    <option value="{{$reviewer['id']}}">{{$reviewer['first_name']}}   {{$reviewer['surname']}}</option>
                                                @endforeach
                                            </select>
                                            {{-- {{Form::select('reviewer2', ['' => 'Choose second reviewer',
                                                                        '1' => 'Emanuel Elisha',
                                                                        '2' => 'Mobutu Sese Seko',
                                                                        ], old('reviewer2'), ['class'=> 'form-control pub_type'])}} --}}
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-6 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <a href="javascript:{}" onclick="document.getElementById('publication_final').submit();"
                                               class="btn btn-primary btn-xs" type="submit">Submit</a>
                                            <a href="{{route('show_publication',['id'=>$data['publication']->id])}}"
                                               class="btn btn-danger btn-xs">Cancel</a>
                                        </div>
                                    </div>
                                </div><!--./row -->
                            {{ Form::close() }}

                            </div><!--./card-body -->
                        </div><!--./card -->
                    </div><!--./col-lg-9 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
    {{-- publication scripts --}}
    
@endsection