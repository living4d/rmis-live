<div id="left_menu">
    <ul id="nav">
        <?php
        if (\App\Perms::perm_methods('publications','lists'))
            echo '<li><a href="' . url('publications') . '">Publications</a></li>';
        ?>
        @if (Auth::user()->hasAnyRole(["admin"]))
            <li><a href="{{url('publication_types')}}">Publication Type</a></li>
        @endif
         @if (Auth::user()->hasAnyRole(["hod","principal","drp","dvc","vc",]))
            <li><a href="{{url('publications/process')}}">My Basket <span class="badge badge-danger">{{$data['notifications']}}</span></a></li>
        @endif
    </ul>
</div>
