@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4> Import Excel Vetted List</h4>
                            <ul>
                                <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li>Import Excel Vetted List</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('publications.side_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="search-form">

                                    @if(count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li> {{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    @if($message = Session::get('success'))
                                        <div class="alert alert-success alert-block">
                                            <button type="button" class="close" data-dismiss="alert">x</button>
                                            <strong> {{ $message }}</strong>
                                        </div>
                                    @endif
                                        <form method="post" enctype="multipart/form-data" action="{{ route('publications.vetted_import') }}">
                                    {{--                                {!! Form::open(['url' => route('publications.vetted_import', $data), 'method' => 'POST', 'class' => 'form-horizontal']) !!}--}}
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <table class="table">
                                            <tr>
                                                <td width="40%" align="right"><label>Select File for Upload</label></td>
                                                <td width="30%"><input type="file" name="select_file"/></td>
                                                <td width="30%" align="left"><input type="submit" name="upload"
                                                                                    class="btn btn-primary"
                                                                                    value="Upload"></td>
                                            </tr>
                                            <tr>
                                                <td width="40%" align="right"></td>
                                                <td width="30%"><span class="text-muted">.xls, .xlsx</span></td>
                                                <td width="30%" align="left"></td>
                                            </tr>
                                            <tr>
                                                <td width="40%" align="right"></td>
{{--                                                <td width="30%"><span class="text-muted">Sample excel format</span></td>--}}
                                                <td width="30%"><span class="text-muted">
                                                    @if (file_exists(public_path() . '/assets/samples/' . 'sample_import_vet_excel.xls'))
                                                        <a href="{{ asset('assets/samples/' . 'sample_import_vet_excel.xls') }}"
                                                           class="font-weight-600 text-primary" download>
{{--                                                            {{ $i . '. ' . $val->attachment_type }}--}}
                                                            Sample excel format
                                                        </a>
                                                        <br/>
                                                    @endif
                                                </span>
                                                </td>
                                                <td width="30%" align="left"></td>
                                            </tr>
                                        </table>
                                    </div>
                                    </form>
                                    <br/>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Vetted Publications with Recommendations</h3>
                                        </div>
                                        @if (isset($data['verdict']) && count($data['verdict']) > 0)
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped">
                                                        <tr>
                                                            <th>PUBL. ID</th>
                                                            <th>CATEGORY</th>
                                                            <th>PUBLICATION TITLE</th>
                                                            <th>BATCH</th>
                                                            <th>RECOMMENDATION</th>
                                                        </tr>
                                                        @foreach($data['verdict'] as $value)
                                                            <tr>
                                                                <td>{{ $value->publication_id }}</td>
                                                                <td>{{ $value->category }}</td>
                                                                <td>{{ $value->publication_title }}</td>
                                                                <td>{{ $value->batch }}</td>
                                                                <td>{{ $value->recommendation }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                            </div>
                                        @else
                                            <div class="alert alert-danger">
                                                <td colspan="3">No data found</td>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <!--./search-form -->
                            </div>
                            <!--./col-md-12 -->
                        </div>
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection