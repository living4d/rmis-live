<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>RIMS - {{isset($title) ? $title : 'Research Information Management System'}}</title>

    <!-- favicon -->
    <link rel="icon" type="image/png" href="{{asset('/assets/img/favicon.png')}}">

    <!-- Bootstrap core CSS -->
    <link href="{{asset('/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!--datatables -->
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <!-- Font awesome css -->
    <link href="{{asset('/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- custom styles -->
    <link href="{{ asset('/assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/css/colors.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/css/forms.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/css/certificate.css') }}" rel="stylesheet">


    <!-- Custom styles for datatables -->
    <link href="{{asset('/assets/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">

    <!--chosen select -->
    <link href="{{asset('assets/plugins/chosen_v1.8.7/chosen.css')}}" rel="stylesheet">

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Chosen JavaScript -->
    <script type="text/javascript" src="{{asset('assets/plugins/chosen_v1.8.7/chosen.jquery.js')}}"></script>

    <!-- Custom JavaScript -->
    <script src="{{asset('assets/js/custom.js')}}"></script>

    <!-- add ckeditor -->
    <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

    <style>   
    
        .table-hover tbody tr:hover td {
            color: blue
        }
    </style>

</head>
<body>
    <div class="row noprint my-3">
        <div class="col-sm-2 ml-5"></div>
        <div class="col-sm-4 text-left"> 
            <button  class="btn btn-primary" onclick="window.print();return false;" ><i class="fa fa-print" aria-hidden="true"></i> Print </button>
            <a class="btn btn-warning" href="{{route('publications')}}">Cancel </a>
        </div>
    </div>
    <div class="row print">
        <page size="A4" >
            <div class="row container mt-4">
                
                <div align = 'center' class="col-sm-12">                
                <h1 style="color:black;font-size:40px; text-align: center"> UNIVERSITY OF DAR ES SALAAM </h1>
                </div>
                
                <div align = 'center' class="col-sm-12">
                    <img src="{{asset('assets/img/logo_ud.png')}}"/>
                </div>
                
            </div>
            <br>
            <br>
            <div class="row container">
                <div class="col-lg-12 text-center">
                    <h2 style="color:black;font-size:25px;">This is to certify that the publication entitled:</h2> 
                    <br>
                    <h2 class="text-uppercase font-weight-normal" id="re-title" style="color:black;font-size:30px;"><i> "{{$data['publication']->publication_title}}" </i></h2> 
                </div>
            </div>
            <br><br>
            <div class="row container">
                <div class="col-lg-1 text-left"></div>
                <div class="col-lg-11 text-left">
                    <p>
                       <span style="font-size:25px;font-family:Tahoma;font-weight: normal; text-align:left">
                           Authored by : <b>{{$data['publication']->names}}</b> <br>
                           Year of publication : <b>{{$data['publication']->year}}</b> <br>
                           Type of publication : <b>{{$data['type']->name}}</b> <br>
                           Published at : <b>{{$data['publication']->publisher}}</b>
                       </span>
                    </p>
                </div>
            </div>
            <br><br>
            <div class="row container">
                <div class="col-lg-1 text-left"></div>
                <div class="col-lg-10">
                    
                    @php
                        $upc_date = $data['publication']->meeting_date;
                        $display_date = date("F j, Y", strtotime($upc_date));
                    @endphp
                    
                    <p>
                        <span style="font-size:25px;font-family:Tahoma;font-weight: normal; text-align:center">
                        Has been vetted by UNIVERSITY PUBLICATION COMMITTEE (UPC) of {{$display_date}} as per UDSM regulations and is hereby <b><i>{{$data['publication']->vetting_status}}</i></b> for submission for UDSM promotion in the level of {{$data['publication']->category}} ranks.
                        </span>
                    </p>
                </div>
            </div>
            <br><br>
            <div class="row container">
                <div class="col-lg-1 text-left"></div>
                <div class="col-lg-11">
                    @php
                    $approved_date = $data['publication']->approved_date;
                    $display_approved_date = date("d/m/Y", strtotime($approved_date));
                    $vc_names = $data['vc']['first_name'].' '.$data['vc']['middle_name'].' ' .$data['vc']['surname'];
                    $drp_names = $data['drp']['first_name'].' '.$data['drp']['middle_name'].' ' .$data['drp']['surname'];
                    @endphp

                    <table class="table table-bordered">
                        <tr>
                            <td width="10%"><span class="font-weight-bold"> </span>
                            </td>
                            <td width="40%"><span  class="text-uppercase"> Checked by DRP</span>
                            </td>
                            <td width="10%">
                                <span class="text-uppercase"></span>
                            </td>
                            <td width="40%">
                                <span class="text-uppercase">Approved by VC</span>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%"><span> Signature </span>
                            </td>
                            <td width="40%"><span class="text-center"><img src="{{asset('assets/signatures/drp.png')}}"/> </span>
                            </td>
                            <td width="10%">
                                <span class="text-uppercase"></span>
                            </td>
                            <td width="40%">
                                <span class="text-center"><img src="{{asset('assets/signatures/vc.png')}}"/></span>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%"><span> Name</span>
                            </td>
                            <td width="40%"><span>{{$drp_names}} </span>
                            </td>
                            <td width="10%">
                                <span class="text-uppercase"></span>
                            </td>
                            <td width="40%">
                                <span class="text-uppercase">{{$vc_names}}</span>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%"><span> Date</span></td>
                            <td width="40%"><span >{{$display_approved_date}}</span></td>
                            <td width="10%">
                                <span class="text-uppercase"></span>
                            </td>
                            <td width="40%">
                                <span class="text-uppercase">{{$display_approved_date}}</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </page>
    </div>
  </body>
</html>