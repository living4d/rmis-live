@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <h4>Add New Publication</h4>
                </div><!-- Page Title -->
            </div>
            <!--./row -->
        </div>
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <!-- sidebar -->
                <div class="col-lg-2 col-md-2">
                    @include('publications.side_menu')
                </div>
                <!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div><br />
                    @elseif(session()->get('danger'))
                    <div class="alert alert-danger">
                        {{ session()->get('danger') }}
                    </div><br />
                    @endif
                    <div class="card card-flat">
                        <h5 class="card-header">REGISTRATION FORM</h5>
                        <!--./card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h6 class="title text-uppercase">Author Information</h6>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->
                            {{ Form::open(array('url' => 'publications/store','id' => 'publication_form','files'=>'true')) }}
                            <input type="hidden" name="_method" value="GET" />

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        @php($name = $data['user']['first_name'].' '.$data['user']['middle_name'].' ' .$data['user']['surname'])
                                        <input class="form-control" name="names" readonly type="text" value="{{$name}}">
                                        {{-- {{Form::text("names",old('names'), ['class="form-control"'])}} --}}
                                        <span class="text-danger">{{ $errors->first('names') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Title/Rank</label>
                                        <input class="form-control" name="rank" readonly type="text" value="{{$data['user']['rank']}}">
                                        <span class="text-danger">{{ $errors->first('rank') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <input class="form-control" name="gender" readonly type="text" value="{{$data['user']['gender']}}">
                                        <span class="text-danger">{{ $errors->first('gender') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>College <span style="color: red;">*</span></label>
                                        <select name="college_id" class="form-control" id="">
                                            <option value="{{$data['college']->id}}" selected>{{$data['college']->name}}</option>
                                            {{-- <option value="">Select batch</option> --}}
                                            @foreach ($data['college_pool'] as $college)
                                            <option value="{{$college->id}}">{{$college->name}} </option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{ $errors->first('college_id') }}</span>
                                    </div><!--./form-group -->
                                </div><!--./col-lg-12 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Department <span style="color: red;">*</span></label>
                                        <select name="department_id" class="form-control" id="">
                                            <option value="{{$data['dept']->id}}" selected>{{$data['dept']->name}}</option>
                                            {{-- <option value="">Select batch</option> --}}
                                            @foreach ($data['dept_pool'] as $department)
                                            <option value="{{$department->id}}">{{$department->name}} </option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{ $errors->first('department_id') }}</span>
                                    </div><!--./form-group -->
                                </div><!--./col-lg-12 -->
                            </div>
                            <!--./row -->
                            <hr />

                            <div class="row">
                                <div class="col-lg-12">
                                    <h6 class="title text-uppercase">Publication Information</h6>
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->
                            <div class="row">
                                <div class="container">
                                    <label><span class="badge badge-warning">NOTE: In the Category field: Senior Lecturers seeking the Associate professor rank and Associate professors seeking Full professor rank are required to select PROFESSORIAL category<br>
                                            Lecturers and other junior academic staff are required to select NON PROFESSORIAL category. Please follow this rule to avoid your publication to be misplaced during vetting</span> </label>
                                </div>
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label>Category(Professorial/Non-Professorial)<span style="color: red;">*</span></label>
                                        {{Form::select('category', ['' => 'Select Category','Professorial' => 'Professorial','Non-Professorial' => 'Non-Professorial'], old('gender'), ['class'=> 'form-control'])}}
                                        <span class="text-danger">{{ $errors->first('category') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Batch Number <span style="color: red;">*</span></label>
                                        <select name="batch_id" class="form-control" id="">
                                            {{-- <option value="">Select batch</option> --}}
                                            @foreach ($data['batches'] as $batch)
                                            <option value="{{$batch['id']}}">{{$batch['batch_number']}} {{$batch['end_date']}}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{ $errors->first('batch_id') }}</span>
                                    </div><!--./form-group -->
                                </div><!--./col-lg-12 -->

                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label>Title of the publication<span style="color: red;">*</span></label>
                                        {{Form::text("publication_title", old('publication_title'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('publication_title') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-8 -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Year of publication <span style="color: red;">*</span></label>
                                        {{Form::selectYear('year',date('Y')-25,date('Y')+1, old('year'), ['class'=> 'form-control',])}}
                                        <span class="text-danger">{{ $errors->first('year') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Type of Publication <span style="color: red;">*</span></label>
                                        {{Form::select('publication_type', $data['publication_types'], old('publication_type'),  ['class'=> 'form-control pub_type'])}}
                                        <span class="text-danger">{{ $errors->first('publication_type') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="form-group consul_type">
                                        <label>UDSM Registration Number<span style="color: red;">*</span></label>
                                        {{Form::text("consultancy_reg_number", old('consultancy_reg_number'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('consultancy_reg_number') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group city">
                                        <label>City published </label>
                                        {{Form::text("city", old('city'), ['class="form-control"'])}}
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-4 -->

                                <div class="col-lg-6">
                                    <div class="form-group book_author">
                                        <label>Book Author <span style="color: red;">*</span></label>
                                        {{Form::text("book_author", old('book_author'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('book_author') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-4 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group book_title">
                                        <label>Book title <span style="color: red;">*</span></label>
                                        {{Form::text("book_title", old('book_title'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('book_title') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group isb_n">
                                        <label>ISBN<span style="color: red;">*</span></label>
                                        {{Form::text("isbn", old('isbn'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('isbn') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                            </div>
                            <!--./row -->


                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group journal_name">
                                        <label>Journal Name<span style="color: red;">*</span></label>
                                        {{Form::text("journal_name", old('journal_name'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('journal_name') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group iss_n">
                                        <label>ISSN<span style="color: red;">*</span></label>
                                        {{Form::text("issn", old('issn'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('issn') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group conf_name">
                                        <label>Conference publication Name<span style="color: red;">*</span></label>
                                        {{Form::text("conference_publication_name", old('conference_publication_name'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('conference_publication_name') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Publisher <span style="color: red;">*</span></label>
                                        {{Form::text("publisher", old('publisher'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('publisher') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group eis_sn">
                                        <label>eISSN<span style="color: red;">*</span></label>
                                        {{Form::text("eissn", old('eissn'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('eissn') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group pages">
                                        <label>Page(s) <span style="color: red;">*</span></label>
                                        {{Form::text("pages", old('pages'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('pages') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->

                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group volume">
                                        <label>Volume <span style="color: red;">*</span></label>
                                        {{Form::text("volume", old('volume'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('volume') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="form-group issue">
                                        <label>Issue <span style="color: red;">*</span></label>
                                        {{Form::text("issue", old('issue'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('issue') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Accessibility <span style="color: red;">*</span></label>
                                        {{Form::select('accessibility', ['' => 'Select','online' => 'Online','hardcopy' => 'Hardcopy'], old('accessibility'), ['class'=> 'form-control linkto'])}}
                                        <span class="text-danger">{{ $errors->first('accessibility') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->

                                <div class="col-lg-6">
                                    <div class="form-group link">
                                        <label>Link (url) <span style="color: red;">*</span></label>
                                        {{Form::text("link", old('link'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('link') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Indexing <span style="color: red;">*</span></label>
                                        {{Form::textarea("index", old('index'), ['class="form-control" rows="5" '])}}
                                        {{-- <script>
                                                CKEDITOR.replace('index');
                                            </script> --}}
                                        <span class="text-danger">{{ $errors->first('index') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Article Processing charges <span style="color: red;">*</span></label>
                                        {{Form::select('processing_charge', ['' => 'Select Option','yes' => 'Yes','no' => 'No'], old('processing_charge'), ['class'=> 'form-control charge'])}}
                                        <span class="text-danger">{{ $errors->first('processing_charge') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->

                                <div class="col-lg-3">
                                    <div class="form-group currency">
                                        <label>Currency </label>
                                        {{Form::select('currency', ['' => 'Select Option','tsh' => 'TSH','dollar' => 'Dollar','euro' => 'Euro','pound' => 'Pound'], old('currency'), ['class'=> 'form-control'])}}
                                        <span class="text-danger">{{ $errors->first('currency') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                                <div class="col-lg-3">
                                    <div class="form-group amount">
                                        <label>Amount </label>
                                        {{Form::text("amount", old('amount'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('amount') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group ">
                                        <label>Attach publication document (pdf) <span class="doc1" style="color: red;">*</span></label>
                                        {{Form::file('publication_attachment',old('publication_attachment'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('publication_attachment') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->

                                <div class="col-lg-6">
                                    <div class="form-group inst_fee">
                                        <label>Attach Institutional Fee Receipt (pdf) <span class="doc1" style="color: red;">*</span></label>
                                        {{Form::file('receipt_attachment',old('receipt_attachment'), ['class="form-control"'])}}
                                        <span class="text-danger">{{ $errors->first('receipt_attachment') }}</span>
                                    </div>
                                    <!--./form-group -->
                                </div>
                                <!--./col-lg-12 -->
                            </div>
                            <!--./row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <a href="javascript:{}" onclick="document.getElementById('publication_form').submit();" class="btn btn-primary btn-xs" type="submit">Submit</a>
                                        <a href="{{url('publications/')}}" class="btn btn-danger btn-xs">Cancel</a>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            {{ Form::close() }}

                        </div>
                        <!--./card-body -->
                    </div>
                    <!--./card -->
                </div>
                <!--./col-lg-9 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
{{-- publication scripts --}}
<script>
    $(document).ready(function() {
        $(".linkto").change(function() {
            $(this).find("option:selected").each(function() {
                var optionValue = $(this).attr("value");
                if (optionValue == "online") {
                    $(".link").show();
                    $(".doc1").hide();
                } else {
                    $(".link").hide();
                    $(".doc1").show();
                }
            });
        }).change();
    });

    $(document).ready(function() {
        $(".charge").change(function() {
            $(this).find("option:selected").each(function() {
                var optionValue = $(this).attr("value");
                if (optionValue == "yes") {
                    $(".currency").show();
                    $(".amount").show();
                } else {
                    $(".currency").hide();
                    $(".amount").hide();
                }
            });
        }).change();
    });

    $(document).ready(function() {
        $(".pub_type").change(function() {
            $(this).find("option:selected").each(function() {
                var optionValue = $(this).attr("value");
                if (optionValue == 1) {
                    $(".city").show();
                    $(".book_author").hide();
                    $(".book_title").hide();
                    $(".journal_name").hide();
                    $(".volume").hide();
                    $(".pages").hide();
                    $(".issue").hide();
                    $(".conf_name").hide();
                    $(".consul_type").hide();
                    $(".inst_fee").hide();
                    $(".isb_n").show();
                    $(".iss_n").hide();
                    $(".eis_sn").hide();
                } else if (optionValue == 2) {
                    $(".city").show();
                    $(".book_author").show();
                    $(".book_title").show();
                    $(".journal_name").hide();
                    $(".volume").hide();
                    $(".issue").hide();
                    $(".conf_name").hide();
                    $(".consul_type").hide();
                    $(".inst_fee").hide();
                    $(".isb_n").show();
                    $(".iss_n").hide();
                    $(".eis_sn").hide();
                } else if (optionValue == 3) {
                    $(".city").hide();
                    $(".book_author").hide();
                    $(".book_title").hide();
                    $(".journal_name").show();
                    $(".volume").show();
                    $(".issue").show();
                    $(".conf_name").hide();
                    $(".consul_type").hide();
                    $(".inst_fee").hide();
                    $(".isb_n").hide();
                    $(".iss_n").show();
                    $(".eis_sn").show();
                } else if (optionValue == 4) {
                    $(".city").show();
                    $(".book_author").hide();
                    $(".book_title").hide();
                    $(".journal_name").hide();
                    $(".volume").show();
                    $(".issue").hide();
                    $(".conf_name").show();
                    $(".consul_type").hide();
                    $(".inst_fee").hide();
                    $(".isb_n").hide();
                    $(".iss_n").show();
                    $(".eis_sn").show();
                } else if (optionValue == 5) {
                    $(".city").hide();
                    $(".book_author").hide();
                    $(".book_title").hide();
                    $(".journal_name").hide();
                    $(".volume").hide();
                    $(".issue").hide();
                    $(".conf_name").hide();
                    $(".consul_type").show();
                    $(".inst_fee").show();
                    $(".isb_n").hide();
                    $(".iss_n").hide();
                    $(".eis_sn").hide();
                } else {
                    $(".city").hide();
                    $(".book_author").hide();
                    $(".book_title").hide();
                    $(".journal_name").hide();
                    $(".volume").hide();
                    $(".issue").hide();
                    $(".conf_name").hide();
                    $(".consul_type").hide();
                    $(".inst_fee").hide();
                    $(".isb_n").hide();
                    $(".iss_n").hide();
                    $(".eis_sn").hide();
                }
            });
        }).change();
    });
</script>

@endsection