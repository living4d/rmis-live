@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="page-title">
                        <h4>Review Publication</h4>
                        <ul>
                            <li><a href="{{url('/')}}" title=""> Dashboard</a></li>
                            <li><a href="{{url('publications')}}" title="">Publication </a></li>
                            <li>Review</li>
                        </ul>
                    </div><!-- Page Title -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <!-- sidebar -->
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-flat">
                            <h5 class="card-header text-uppercase">Publication review</h5><!--./card-header -->
                            <div class="card-body">
                            {{ Form::open(array('url' =>['publication/save_review','id' => $data['publication']->id],'id' => 'review_form')) }}
                            <input type="hidden" name="_method" value="GET"/>
                                
                                <table width="100%" class="table table-hover table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th width="3%">#</th>
                                        <th width="77%">ASSESSMENT OF PUBLICATIONS</th>
                                        <th width="20%">GRADES</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Coverage of subject matter</td>
                                            <td><select name="mark1" id="one" onchange="getAve()">
                                                <option value="">GRADE</option>
                                                    <option value="5">A</option>
                                                    <option value="4">B+</option>
                                                    <option value="3">B</option>
                                                    <option value="2">C</option>
                                                    <option value="1">D</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Originality</td>
                                            <td><select name="mark2" id="two" onchange="getAve()">
                                                <option value="">GRADE</option>
                                                    <option value="5">A</option>
                                                    <option value="4">B+</option>
                                                    <option value="3">B</option>
                                                    <option value="2">C</option>
                                                    <option value="1">D</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Contribution to knowledge</td>
                                            <td><select name="mark3" id="three" onchange="getAve()">
                                                <option value="">GRADE</option>
                                                    <option value="5">A</option>
                                                    <option value="4">B+</option>
                                                    <option value="3">B</option>
                                                    <option value="2">C</option>
                                                    <option value="1">D</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Relevance to academic discipline</td>
                                            <td><select name="mark4" id="four" onchange="getAve()">
                                                <option value="">GRADE</option>
                                                    <option value="5">A</option>
                                                    <option value="4">B+</option>
                                                    <option value="3">B</option>
                                                    <option value="2">C</option>
                                                    <option value="1">D</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Relevance to individual’s own specialisation in an academic
                                                discipline</td>
                                            <td><select name="mark5" id="five" onchange="getAve()">
                                                <option value="">GRADE</option>
                                                    <option value="5">A</option>
                                                    <option value="4">B+</option>
                                                    <option value="3">B</option>
                                                    <option value="2">C</option>
                                                    <option value="1">D</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Presentation</td>
                                            <td><select name="mark6" id="six" onchange="getAve()">
                                                <option value="">GRADE</option>
                                                    <option value="5">A</option>
                                                    <option value="4">B+</option>
                                                    <option value="3">B</option>
                                                    <option value="2">C</option>
                                                    <option value="1">D</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Overall quality</td>
                                            <td>
                                            {{-- <td><select name="overall" id="">
                                                <option value=""></option>
                                                </select> --}}
                                                <input name="overall" id="overall" type="text" readonly hidden>
                                                <span id="overall-show"></span> 
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group book_author">
                                            <label>Does the quality of the publications assessed in general reflect the Author’s current academic rank? <span style="color: red;">*</span></label>
                                            {{Form::select('general', [''=>'Select','yes'=>'Yes','no'=>'No'],
                                            old('general'),['class'=> 'form-control'])}}
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-4 -->
                                    <div class="col-lg-6">
                                        <div class="form-group book_author">
                                            <label>Does the quality of the publication assessed merit promotion of the Author to the next academic rank? <span style="color: red;">*</span></label>
                                            {{Form::select('promo', [''=>'Select','yes'=>'Yes','no'=>'No'],
                                                            old('promo'),['class'=> 'form-control'])}}
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-4 -->
                                </div><!--./row -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Any other comments, suggestions, or recommendations.<span style="color: red;">*</span></label>
                                            {{Form::textarea('comment', old('comment'), ['class="form-control" rows="3"'])}}
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <a href="javascript:{}" onclick="document.getElementById('review_form').submit();"
                                               class="btn btn-primary btn-xs" type="submit">Submit</a>
                                            <a href="{{url('publications')}}"
                                               class="btn btn-danger btn-xs">Cancel</a>
                                        </div>
                                    </div>
                                </div><!--./row -->
                            {{ Form::close() }}

                            </div><!--./card-body -->
                        </div><!--./card -->
                    </div><!--./col-lg-9 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
    {{-- publication scripts --}}

    <script type="text/javascript">
         function getAve() { 
                 
            mark1 = $('#one').val();; 
            mark2 = $('#two').val(); 
            mark3 = $('#three').val();; 
            mark4 = $('#four').val();
            mark5 = $('#five').val();; 
            mark6 = $('#six').val();  
            output = +mark1 + +mark2 + +mark3 + +mark4 + +mark5 + +mark6;
           ave = output/6;
           if(ave >= 5){
             grade = "A";
           }else if(ave < 5 && ave >= 4){
            grade = "B+";
           }else if(ave < 4 && ave >= 3){
               grade = "B";
           }else if(ave < 3 && ave >= 2){
               grade = "C";
           }else if(ave < 2 ){
               grade = "D";
           }
            $('#overall').val(grade); 
            $('#overall-show').text(grade);
        } 
    </script>
    
@endsection