@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="page-title">
                        <h4>Board Decision</h4>
                        <ul>
                            <li><a href="{{url('/')}}" title=""><i class="fa fa-home"></i> Home</a></li>
                            <li><a href="{{url('publications')}}" title="">Publications</a></li>
                            <li>Commette</li>
                        </ul>
                    </div><!-- Page Title -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <!-- sidebar -->
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-flat">
                            <h5 class="card-header">UPC COMMITTEE DECISION</h5><!--./card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="title text-uppercase"></h6>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->
                                {{ Form::open(array('url' =>['publication/decision','id' => $id],'id' => 'publication_final')) }}
                                <input type="hidden" name="_method" value="GET"/>
                                
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Peer Review Media <span style="color: red;">*</span></label>
                                            {{Form::select('peer_review', ['' => 'Select Option','yes' => 'Yes','no' => 'No'], old('peer_review'), ['class'=> 'form-control'])}}
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-6 -->

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>International Editorial Board Exist? </label>
                                            {{Form::select('international_editor_board', ['' => 'Select Option','yes' => 'Yes','no' => 'No'], old('international_editor_board'), ['class'=> 'form-control'])}}
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-6 -->
                                </div><!--./row -->
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Vetting Decision <span style="color: red;">*</span></label>
                                            {{Form::select('vetting_status', ['' => 'Select Type of Publication',
                                                                        'Recommended' => 'Recommended',
                                                                        'Not Recommended' => 'Not Recommended',
                                                                        ], old('vetting_status'), ['class'=> 'form-control pub_type'])}}
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-6 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Add Recommendation on the quality of the media</label>
                                            {{Form::textarea('recommendation', old('recommendation'), ['class="form-control" rows="5"'])}}
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-6 -->
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>UPC meeting Date </label>
                                            <input type="date" name="meeting_date">
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-6 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <a href="javascript:{}" onclick="document.getElementById('publication_final').submit();"
                                               class="btn btn-primary btn-xs" type="submit">Record Decision</a>
                                            <a href="{{route('show_publication',['id'=>$id])}}"
                                               class="btn btn-danger btn-xs">Cancel</a>
                                        </div>
                                    </div>
                                </div><!--./row -->
                            {{ Form::close() }}

                            </div><!--./card-body -->
                        </div><!--./card -->
                    </div><!--./col-lg-9 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
    {{-- publication scripts --}}
    
@endsection