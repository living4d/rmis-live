@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
  <div class="page-heading">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="page-title">
            <h4>Register New</h4>
            <ul>
              <li><a href="#" title="">Dashboard</a></li>
              <li><a href="{{url('/publications/type')}}" title=""> Publication types</a></li>
              <li>Register New</li>
            </ul>
          </div><!-- Page Title -->
        </div>
      </div>
    </div>
  </div><!-- page-header -->

  <div class="page-content">
    <div class="container">
      <div class="row">
        <div class="col-lg-2 col-md-2">
          @include('publications.side_menu')
        </div>

        <div class="col-lg-10 col-md-10">
          <div class="card card-flat">
            <h5 class="card-header">Register New</h5>
            <div class="card-body">

              @if(session()->get('success'))
              <div class="alert alert-success">
                {{ session()->get('success') }}
              </div><br />
              @endif

              {{ Form::open(['url' => route('publications.publ_type_store'), 'method' => 'POST', 'class' => 'form-horizontal']) }}
              {{ Form::token() }}

              <div class="row">
                <div class="col-lg-2">
                  <div class="form-group">
                    <label>ID <span class="red">*</span></label>
                    {{ Form::text('id', old('id'), ['class="form-control"', 'placeholder="Write a ID number..."']) }}
                    <span class="text-danger">{{ $errors->first('id') }}</span>
                  </div>
                </div>

                <div class="col-lg-10">
                  <div class="form-group">
                    <label>Publication Type <span class="red">*</span></label>
                    {{ Form::text('name', old('name'), ['class="form-control"', 'placeholder="Write a Publication type..."']) }}
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <button type="submit" class="btn btn-secondary btn-xs text-medium">Register</button>
                    <a href="{{url('/publications/type')}}" class="btn btn-danger btn-xs text-medium">Cancel</a>
                  </div>
                </div>
                <!--./col-lg-12 -->
              </div>
              <!--./row -->
              {{ Form::close() }}
            </div>
            <!--./card-body -->
          </div>
          <!--./card -->
        </div>
        <!--./col-lg-12 -->
      </div>
      <!--./row -->
    </div>
    <!--./container -->
  </div>
  <!--./page-content -->
</section>
@endsection