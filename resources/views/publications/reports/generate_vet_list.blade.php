@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <h4> Publication Vet List</h4>
                        <ul>
                            <li><a href="{{ route('calls.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li>Publication Vet List</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    @include('publications.side_menu')
                </div>
                <!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="search-form">

                                {!! Form::open(['url' => route('publications.vet_list', $data), 'method' => 'POST', 'class' => 'form-horizontal']) !!}
                                {{ Form::token() }}

                                <div class="row">
                                    <div class="col-md-3 col-sm-3 col-12">
                                        <div class="form-group">
                                            <label>Report Type</label>
                                            @php
                                            $_options = [
                                            'complete-report' => 'Combined Matrix',
                                            'batch' => 'Active Batch Matrix',
                                            ];
                                            @endphp
                                            {{ Form::select('report_type', $_options, old('report_type'), ['class="form-control"']) }}
                                        </div>
                                    </div>
                                    <!--./col-md-3 -->

                                    <div class="col-md-3 col-sm-3 col-12">
                                        <div class="form-group">
                                            <label>Category</label>
                                            @php
                                            $_options = [
                                            'Professorial' => 'Professorial',
                                            'Non-Professorial' => 'Non Professorial',
                                            ];
                                            @endphp
                                            {{ Form::select('category', $_options, old('category'), ['class="form-control"']) }}
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-md-3 -->

                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Batch Number <span style="color: red;">*</span></label>
                                            <select name="batch_id" class="form-control" id="">
                                                {{-- <option value="">Select batch</option> --}}
                                                @foreach ($data['batches'] as $batch)
                                                <option value="{{$batch['id']}}">{{$batch['batch_number']}} {{$batch['end_date']}}</option>
                                                @endforeach
                                            </select>
                                            <span class="text-danger">{{ $errors->first('batch_id') }}</span>
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-12 -->

                                    <div class="col-md-2 col-sm-2 col-12">
                                        <div class="pull-left">
                                            <div class="form-group">
                                                <label>&nbsp;&nbsp;&nbsp;</label><br />
                                                <button style="float:right" type="submit" name="submit" class="btn btn-primary"> Generate
                                                </button>
                                            </div>
                                            <!--./form-group -->
                                        </div>
                                    </div>
                                    <!--./col-md-->
                                </div>
                                <!--./row -->
                                {{ Form::close() }}
                            </div>
                            <!--./search-form -->
                        </div>
                        <!--./col-md-12 -->
                    </div>
                    <!--./row -->

                    @if (($data['batches'] != null) || $data['report_type'] === 'batch')

                    <div class="row mt-3">
                        <div class="col-md-12">

                            <button class="btn btn-success mb-2" onclick="printDiv('printableArea')">
                                <i class="fa fa-print" aria-hidden="true"></i> Print
                            </button>

                            <div class="btn-group ml-2">
                                <button type="button" class="btn btn-default">Bulk Actions</button>
                                <button type="button" class="btn btn-default dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu">
                                    {{-- <a wire:click.prevent = "export" class="dropdown-item" href="/publications/publVet/export/">Export</a> --}}
                                    <a wire:click.prevent="export" class="dropdown-item" href="#">Export</a>
                                </div>
                            </div>

                            <div id="printableArea" style="background-color: #FFFFFF; padding:40px;">

                                <div class="row container">
                                    <div class="col-lg-12">

                                        <div id="letter-content">

                                            @if ($data['report_type'] === 'batch')
                                            <div class="mt-3">
                                                <h6 class="text-uppercase">
                                                    The Batch of this matrix:
                                                </h6>
                                            </div>

                                            <table class="">
                                                <tr>
                                                    <td width="22%" valign="top"><b>Batch number:</b></td>
                                                    <td width="80%">
                                                        {{ $data['batch'] }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="22%" valign="top"><b>Category:</b></td>
                                                    <td width="80%">
                                                        {{ $data['category'] }}
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="mt-3"></div>
                                            @endif
                                        </div>

                                        <div>
                                            <h3 id="re-title">
                                                PUBLICATION VET LIST
                                            </h3>
                                        </div>
                                        <div>
                                            <table class="table table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th width="3%">#</th>
                                                        <th width="3%">publication_id</th>
                                                        <th width="4%">Category</th>
                                                        <th width="4%">College</th>
                                                        <th width="4%">Department</th>
                                                        <th width="4%">Name</th>
                                                        <th width="10%">Publication_Title</th>
                                                        <th width="4%">Year</th>
                                                        <th width="4%">Publication Type</th>
                                                        <th width="4%">Journal Name</th>
                                                        <th width="4%">Conference Publication Name</th>
                                                        <th width="4%">Book_title</th>
                                                        <th width="4%">Publisher</th>
                                                        <th width="4%">Volume</th>
                                                        <th width="4%">Issue</th>
                                                        <th width="4%">Pages</th>
                                                        <th width="4%">ISBN</th>
                                                        <th width="4%">ISSN</th>
                                                        <th width="4%">eISSN</th>
                                                        <th width="4%">Cons Reg. Number</th>
                                                        <th width="4%">Accessibility</th>
                                                        <th width="4%">Index</th>
                                                        <th width="10%">Link</th>
                                                        <th width="4%">Processing_charge</th>
                                                        <th width="4%">Currency</th>
                                                        <th width="4%">Amount</th>
                                                        <th width="4%">batch</th>
                                                    </tr>
                                                </thead>

                                                @php
                                                $serial = 1;
                                                @endphp

                                                @foreach ($data['vet_list'] as $values)
                                                <tr>
                                                    <td>{{ $serial }}</td>
                                                    <td>{{ $values->id }}</td>
                                                    <td>{{ $values->category }}</td>
                                                    <td>
                                                        {{ $values->College }}
                                                    </td>
                                                    <td>
                                                        {{ $values->Department }}
                                                    </td>
                                                    <td>
                                                        {{ $values->Name }}
                                                    </td>
                                                    <td>
                                                        {{ $values->publication_title }}
                                                    </td>
                                                    <td>
                                                        {{ $values->year }}
                                                    </td>
                                                    <td>
                                                        {{ $values->publication_type }}
                                                    </td>
                                                    <td>
                                                        @if ($values->journal_name == null)
                                                        <span class="badge badge-pill badge-warning">N/A</span>
                                                        @else
                                                        {{ $values->journal_name }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($values->conference_publication_name == null)
                                                        <span class="badge badge-pill badge-warning">N/A</span>
                                                        @else
                                                        {{ $values->conference_publication_name }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($values->book_title == null)
                                                        <span class="badge badge-pill badge-warning">N/A</span>
                                                        @else
                                                        {{ $values->book_title }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{ $values->publisher }}
                                                    </td>
                                                    <td>
                                                        @if ($values->volume == null)
                                                        <span class="badge badge-pill badge-warning">N/A</span>
                                                        @else
                                                        {{ $values->volume }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($values->issue == null)
                                                        <span class="badge badge-pill badge-warning">N/A</span>
                                                        @else
                                                        {{ $values->issue }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($values->pages == null)
                                                        <span class="badge badge-pill badge-warning">N/A</span>
                                                        @else
                                                        {{ $values->pages }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($values->isbn == null)
                                                        <span class="badge badge-pill badge-warning">N/A</span>
                                                        @else
                                                        {{ $values->isbn }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($values->issn == null)
                                                        <span class="badge badge-pill badge-warning">N/A</span>
                                                        @else
                                                        {{ $values->issn }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($values->eissn == null)
                                                        <span class="badge badge-pill badge-warning">N/A</span>
                                                        @else
                                                        {{ $values->eissn }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($values->consultancy_reg_number == null)
                                                        <span class="badge badge-pill badge-warning">N/A</span>
                                                        @else
                                                        {{ $values->consultancy_reg_number }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{ $values->accessibility }}
                                                    </td>
                                                    <td>
                                                        {{ $values->index }}
                                                    </td>
                                                    <td>
                                                        @if ($values->link == null)
                                                        <span class="badge badge-pill badge-warning">N/A</span>
                                                        @else
                                                        {{ $values->link }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{ $values->processing_charge }}
                                                    </td>
                                                    <td>
                                                        @if ($values->currency == null)
                                                        <span class="badge badge-pill badge-warning">N/A</span>
                                                        @else
                                                        {{ $values->currency }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($values->amount == null)
                                                        <span class="badge badge-pill badge-warning">N/A</span>
                                                        @else
                                                        {{ $values->amount }}
                                                        @endif
                                                    </td>
                                                    <td> {{ $values->batch_id }} </td>

                                                </tr>
                                                @php
                                                $serial = $serial+1;
                                                @endphp
                                                @endforeach
                                                {{-- @endforeach --}}
                                            </table>
                                        </div>
                                    </div>
                                    <!--./col -->
                                </div>
                                <!--./row -->
                            </div>
                            <!--./print-content -->

                        </div>
                        <!--./col-md-12 -->
                    </div>
                    <!--./row -->
                    @endif
                </div>
                <!--./col-lg-10 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>

<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>
@endsection