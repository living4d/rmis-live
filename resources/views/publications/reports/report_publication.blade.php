@extends('layouts.admin_app')
@section('content')

    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h4>Report Summary</h4>
                            <ul>
                                <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li><a href="#">Publication</a></li>
                                <li><a href="#">Report</a>
                                </li>
                                
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-lg-10 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        {{-- @include('publications.side_menu') --}}
                        @include('publications.typeside_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat pure-form">
                            <div class="card-body">
                                <h5 class="text-uppercase">Report Summary</h5>
                                <hr />
                                

                                @if (session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div>
                                @elseif (session()->get('info'))
                                    <div class="alert alert-info">
                                        {{ session()->get('info') }}
                                    </div>
                                @elseif (session()->get('danger'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('danger') }}
                                    </div>
                                @endif
                                
                               
                                

                                

                                
                                
                            </div>
                            <!--./card-body -->
                            <div class="col-lg-10 col-md-10">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="card info-box">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-3 col-xs-3">
                                                        {{-- <i class="fa fa-active-project"></i> --}}
                                                        <img src="{{ asset('assets/img/report_icon.jpg') }}" />
                                                    </div>
                                                    <div class="col-md-9 col-xs-9 text-right">
                                                        <div class="text-large font-weight-600">
                                                          {{$proff}}
                                                        </div>
        
                                                        <div class="text-medium text-grey">
                                                            Professorial Publications
                                                        </div>
                                                    </div>
                                                    <!--./col-xs-9 -->
                                                </div>
                                                <!--./row-->
                                            </div>
                                            <!--./card-body-->
                                        </div>
                                        <!--./card-->
                                    </div>
                                    <!--./col-md-4 -->
        
                                    <div class="col-lg-4 col-md-4">
                                        <div class="card info-box">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-3 col-xs-3">
                                                        {{-- <i class="fa fa-total-funding"></i> --}}
                                                        <img src="{{ asset('assets/img/publication.jpg') }}" /> 
                                                    </div>
                                                    <div class="col-md-9 col-xs-9 text-right">
                                                        <div class="text-large font-weight-600">
                                                            {{ $nonProff }}
                                                        </div>
        
                                                        <div class="text-medium text-grey">
                                                            Non-Professorial Publications
                                                        </div>
                                                    </div>
                                                    <!--./col-xs-9 -->
                                                </div>
                                                <!--./row-->
                                            </div>
                                            <!--./card-body-->
                                        </div>
                                        <!--./card-->
                                    </div>
                                    <!--./col-md-4 -->
                                </div>
                                <!--./row -->
                            </div>
                        </div>
                        <!--./card --> 
                        <br/>
                        <div class="row">
                            {{-- <div class="input-group mb-2" class="col-md-1">
                                <input type="date" class="form-control" label="Date from"name="start_date">
                                <input type="date" class="form-control" name="end_date">
                                <button class="btn btn-primary" type="submit">Filter</button>
                            </div> --}}
                            {{-- <div class="col-md-1">
                                <label>Date from:</label> 
                              </div> --}}
                              <div class="col-md-2">
                                            
                                <input class="form-control" type="date" formControlName='start'  placeholder="start date" id="startDate" width="" (change)="setClearFilter()" />
                                </div>
                                {{-- <div class="col-md-1">
                                  <!-- <input class="form-control" type="text" name="end Date:"/> -->
                                  <label >Date to:</label> 
                                </div> --}}
                            <div class="col-md-2">
                             
                                <input class="form-control" formControlName='end' placeholder="end date" id="endDate" width=""  type="date" (change)="setClearFilter()"/>
                            </div>
                            <div class="col-md-1">
                                <input type="submit" name="filter" value="Filter"  class="btn btn-primary w-100" />
                                
                            </div>
                        </div>
                        <br/>
                        <table id="dt" class="table table-hover table-bordered" class="table display">
                            <thead>
                                <tr>
                                    <th width="4%">#</th>
                                    <th width="38%">College</th>
                                    <th width="20%">category</th>
                                    <th width="8%">Number of publications</th>
                                    
                                    {{-- <th style="width: 60px; !important">Action</th> --}}
                                </tr>
                            </thead>
                                <tbody>
                                    <?php $i=1; ?>
                                   @foreach ($data as $data)
                                   <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{$data->college  }}</td>
                                    <td>{{ $data->category }}</td>
                                    <td>{{ $data->id }}</td>
                                </tr>
                                   @endforeach
                                    
                                    
                                </tbody>
                        </table>
                      
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
