@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Update Batch</h4>
                            <ul>
                                <li><a href="#" title="">Dashboard</a></li>
                                <li><a href="{{url('/publications/batches')}}" title=""> Batches</a></li>
                                <li>Update Batch</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('publications.side_menu')
                    </div><!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat">
                            <h5 class="card-header">Update Batch</h5><!--./card-header -->
                            
                            <div class="card-body">

                                @if(session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div><br/>
                                @endif

                                {{ Form::open(['url' => route('batch.publications.status.update', $data['batch']->id), 'class' => 'form-horizontal']) }}
                                @method('PUT')
                                {{ Form::token() }}
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h5>Batch number: {{$data['batch']->batch_number}}</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <h5>Batch Start Date: {{$data['batch']->start_date}}</h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <h5>Batch End Date: {{$data['batch']->end_date}}</h5>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Status <span style="color: red;">*</span></label>
                                            {{Form::select('status', ['' => 'Select Status','open' => 'Open','closed' => 'Closed','vetted' => 'Vetted'], old('status',$data['batch']->status), ['class'=> 'form-control'])}}
                                            <span class="text-danger">{{ $errors->first('status') }}</span>    
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-secondary btn-xs text-medium">Update
                                            </button>
                                            <a href="{{url('/publications/batches')}}" class="btn btn-danger btn-xs text-medium">Cancel</a>
                                        </div>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->
                                {{ Form::close() }}
                            </div><!--./card-body -->
                        </div><!--./card -->
                    </div><!--./col-lg-12 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
@endsection
