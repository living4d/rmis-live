@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <h4>Batches</h4>
                        <ul>
                            <li><a href="#" title="">Dashboard</a></li>
                            <li>Batches</li>
                        </ul>
                    </div><!-- Page Title -->
                </div>
                <!--./col-md-10 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    @include('publications.side_menu')
                </div>

                <div class="col-lg-10 col-md-10">
                    @if(session()->get('success'))
                    <div class="mt-2 alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                    @elseif(session()->get('danger'))
                    <div class="mt-2 alert alert-danger">
                        {{ session()->get('danger') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-9">
                            <a href="{{url('publications/batches/create')}}" class="btn btn-primary btn-xs text-medium">
                                <i class="fa fa-plus"></i> Register New</a>
                        </div>

                        <div class="col-md-3 pull-right">
                            <input type="text" id="myCustomSearchBox" class="form-control" placeholder="Search details...">
                        </div>
                    </div>

                    @if(isset($data['batch']))
                    <table id="dt" class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th width="3%">#</th>
                                <th width="15%">Batch Number</th>
                                <th width="20%">Batch Start Date</th>
                                <th width="20%">Batch End Date</th>
                                <th width="15%">Status</th>
                                <th width="6%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $serial = 1 ?>
                            @foreach($data['batch'] as $batch)
                            <tr>
                                <td><?= $serial ?></td>
                                <td>{{$batch->batch_number}}</td>
                                <td>{{$batch->start_date}}</td>
                                <td>{{$batch->end_date}}</td>
                                <td>{{$batch->status}}</td>
                                <td>
                                    <a href="{{route('batch.edit',['id'=>$batch->id])}}"
                                        data-toggle="tooltip" data-placement="bottom" title="edit batch"
                                            class="btn btn-outline-secondary btn-xs">
                                        <i class="fa fa-pencil text-secondary"></i>
                                    </a>&nbsp;&nbsp;
                                    @if (\App\Perms::perm_methods('users', 'edit'))
                                        <a href="{{ route('batch.pupl.status.edit', $batch->id) }}"
                                            data-toggle="tooltip" data-placement="bottom" title="update batch publications status"
                                            class="btn btn-outline-secondary btn-xs">
                                            <i class="fa fa-calendar-check-o text-secondary"></i>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                            <?php $serial++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <div class="alert alert-danger"> No any batch created.</div>
                    @endif
                </div>
                <!--./col-lg-9 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection