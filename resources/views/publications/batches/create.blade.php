@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
  <div class="page-heading">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="page-title">
            <h4>Create </h4>
            <ul>
              <li><a href="#" title="">Dashboard</a></li>
              <li><a href="{{url('/publications/batches')}}" title=""> Batches</a></li>
              <li>Create </li>
            </ul>
          </div><!-- Page Title -->
        </div>
      </div>
      <!--./row -->
    </div>
    <!--./container -->
  </div><!-- page-header -->

  <div class="page-content">
    <div class="container">
      <div class="row">
        <div class="col-lg-2 col-md-2">
          @include('publications.side_menu')
        </div>
        <!--col-md-2 -->

        <div class="col-lg-10 col-md-10">
          <div class="card card-flat">
            <h5 class="card-header">Create</h5>
            <!--./card-header -->
            <div class="card-body">

              @if(session()->get('success'))
              <div class="alert alert-success">
                {{ session()->get('success') }}
              </div><br />
              @endif

              {{ Form::open(['url' => route('batch.store'), 'method' => 'POST', 'class' => 'form-horizontal']) }}
              {{ Form::token() }}

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label>Batch Number <span class="red">*</span></label>
                    {{ Form::text('batch_number', old('batch_number'), ['class="form-control"', 'placeholder="Write a Batch number..."']) }}
                    <span class="text-danger">{{ $errors->first('batch_number') }}</span>
                  </div>
                </div>
              </div>
              <!--./row -->

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label>Batch Start Date <span style="color: red;">*</span></label>
                    {{ Form::date('start_date', old('start_date'), ['class="form-control" placeholder="Write Batch start date"', 'min' => date('Y-m-d')]) }}
                    <span class="text-danger">{{ $errors->first('start_date') }}</span>
                  </div>
                  <!--./form-group -->
                </div>
                <!--./col-lg-6 -->
              </div>

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label>Batch End Date <span style="color: red;">*</span></label>
                    {{ Form::date('end_date', old('end_date'), ['class="form-control" placeholder="Write Batch end date"', 'min' => date('Y-m-d')]) }}
                    <span class="text-danger">{{ $errors->first('end_date') }}</span>
                  </div>
                  <!--./form-group -->
                </div>
                <!--./col-lg-6 -->
              </div>

              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <button type="submit" class="btn btn-secondary btn-xs text-medium">Create</button>
                    <a href="{{url('/publications/batches')}}" class="btn btn-danger btn-xs text-medium">Cancel</a>
                  </div>
                </div>
                <!--./col-lg-12 -->
              </div>
              <!--./row -->
              {{ Form::close() }}
            </div>
            <!--./card-body -->
          </div>
          <!--./card -->
        </div>
        <!--./col-lg-12 -->
      </div>
      <!--./row -->
    </div>
    <!--./container -->
  </div>
  <!--./page-content -->
</section>
@endsection