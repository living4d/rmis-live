@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <h4>Publications</h4>
                        <ul>
                            <li><a href="{{ route('publications.stats') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li>Summary</li>
                        </ul>
                    </div><!-- Page Title -->
                </div>
                <!--./col-md-12 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <!-- sidebar -->
                <div class="col-lg-2 col-md-2">
                    @include('publications.side_menu')
                </div>
                <!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div><br />
                    @elseif(session()->get('danger'))
                    <div class="alert alert-danger">
                        {{ session()->get('danger') }}
                    </div><br />
                    @endif

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-8">
                            @if (Auth::user()->hasRole("staff"))
                            <a href="{{url('publications/register')}}" class="btn btn-primary btn-xs text-medium">
                                <i class="fa fa-plus"></i> Register New</a>&nbsp;
                            @endif
                        </div>
                    </div>
                    @if (isset($data['publications']) && count($data['publications']) > 0)

                    <table id="myTable" class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th width="3%">#</th>
                                <th width="17%">Title</th>
                                <th width="8%">Staff</th>
                                <th width="4%">Year</th>
                                <th width="10%">Publication type</th>
                                <th width="10%">Media Name</th>
                                <th width="8%">Publisher</th>
                                {{-- <th width="5%">Points</th> --}}
                                <th width="8%">Approval Status</th>
                                <th width="8%">Vetting Status</th>
                                {{-- <th width="8%">Review Status</th> --}}
                                <th width="10%">Actions</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $serial = 1; ?>
                            @php($serial = 1)
                            @foreach ($data['publications'] as $publication)
                            <tr>
                                <td>{{$serial}}</td>
                                <td><a href="{{route('show_publication',['id'=>$publication->id])}}">{{$publication->publication_title}}</a> </td>
                                <td>{{$publication->names}}</td>
                                <td>{{$publication->year}}</td>
                                <td>{{$publication['type']['name']}}</td>
                                <td class="">
                                    @if ($publication->publication_type == "1")
                                    {{$publication->book_author}}
                                    @elseif($publication->publication_type == "2")
                                    {{$publication->journal_name}}
                                    @elseif($publication->publication_type == "3")
                                    {{$publication->journal_name}}
                                    @elseif($publication->publication_type == "4")
                                    {{$publication->conference_publication_name}}
                                    @elseif($publication->publication_type == "5")
                                    UDSM Consultancy Bureau
                                    @endif
                                </td>
                                <td>{{$publication->publisher}}</td>
                                <td class="">
                                    @if ($publication->vetting_status == "hod" || $publication->vetting_status == "HOD")
                                    <label class="badge badge-primary text-uppercase">HOD to support</label>
                                    @elseif($publication->vetting_status == "principal")
                                    <label class="badge badge-secondary text-uppercase">PRINCIPAL to endorse</label>
                                    @elseif($publication->vetting_status == "rejected")
                                    -
                                    @elseif($publication->vetting_status == "drp")
                                    <label class="badge badge-info text-uppercase">UPAC to recommend</label>
                                    @elseif($publication->level == "4")
                                    <label class="badge badge-success text-uppercase">DVC to recommend</label>
                                    @elseif($publication->remark == "approved")
                                    <label class="badge badge-success text-uppercase">Approved</label>
                                    @elseif($publication->remark == "rejected")
                                    <label class="badge badge-danger text-uppercase">Rejected</label>
                                    @elseif($publication->level == "5")
                                    <label class="badge badge-warning text-uppercase">VC to approve</label>
                                    @elseif($publication->level == "10")
                                    <label class="badge badge-basic text-uppercase">UPAC Completed</label>
                                    @endif
                                </td>
                                <td class="">
                                    @if ($publication->vetting_status == "Recommended")
                                    <label class="badge badge-primary text-uppercase">Recommended</label>
                                    @elseif ($publication->vetting_status == "Not Recommended")
                                    <label class="badge badge-success text-uppercase">Not Recommended</label>
                                    @elseif ($publication->vetting_status == "rejected")
                                    <label class="badge badge-danger text-uppercase">Rejected</label>
                                    @elseif ($publication->vetting_status == "completed")
                                    <label class="badge badge-default text-uppercase">Completed</label>
                                    @else
                                    <label class="badge badge-secondary text-uppercase">On progress</label>
                                    @endif
                                </td>
                                {{-- <td>
                                            @if($publication['review_status'] == 'review')
                                                <label class="badge badge-secondary text-uppercase">On review</label>
                                            @elseif($publication['review_status'] == 'reviewed')
                                                <label class="badge badge-info text-uppercase">DONE</label><br>
                                                {{$publication['review']['overall']}}
                                @else
                                <label class="badge badge-success text-uppercase">On vetting</label>
                                @endif
                                </td> --}}
                                <td>
                                    <a href="{{route('show_publication',['id'=>$publication->id])}}" title="Details">
                                        <i class="fa fa-folder-open fa-lg text-primary"></i>
                                    </a> &nbsp;

                                    <a href="{{route('edit_publication',['id'=>$publication->id])}}" title="Edit">
                                        <i class="fa fa-pencil-square text-dark fa-lg"></i>
                                    </a>&nbsp;

                                    <a class="delete" href="{{route('delete_publication',['id'=>$publication->id])}}" title="Delete">
                                        <i class="fa fa-trash fa-lg text-danger"></i>
                                    </a>&nbsp;
                                </td>
                            </tr>
                            @php ($serial++)
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <div class="alert alert-danger mt-2"> No any publication.</div>
                    @endif

                    @if (isset($data['vets']) && count($data['vets']) > 0)
                    <div class="card card-flat">
                        <h5 class="card-header text-uppercase">Vetted Publications</h5>
                    </div>
                    <table id="myTable" class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th width="3%">#</th>
                                <th width="20%">Title</th>
                                <th width="6%">Year</th>
                                <th width="10%">Type of Publication</th>
                                <th width="8%">Media Name</th>
                                <th width="8%">Publisher</th>
                                {{-- <th width="5%">Points</th> --}}
                                <th width="8%">Vetting Status</th>
                                <th width="8%">Review Status</th>
                                <th width="10%"></th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $serial = 1; ?>
                            @php($serial = 1)
                            @foreach ($data['vets'] as $publication)
                            <tr>
                                <td>{{$serial}}</td>
                                <td><a href="{{route('show_publication',['id'=>$publication->id])}}">{{$publication->publication_title}}</a> </td>
                                <td>{{$publication->year}}</td>
                                <td>{{$publication['type']['name']}}</td>
                                <td class="">
                                    @if ($publication->publication_type == "1")
                                    {{$publication->book_author}}
                                    @elseif($publication->book_title == "2")
                                    {{$publication->journal_name}}
                                    @elseif($publication->publication_type == "3")
                                    {{$publication->journal_name}}
                                    @elseif($publication->publication_type == "4")
                                    {{$publication->conference_publication_name}}
                                    @endif
                                </td>
                                <td>{{$publication->publisher}}</td>
                                {{-- <td></td> --}}
                                {{-- <td class="text-capitalize">{{$publication->vetting_status}}</td> --}}
                                <td class="">
                                    @if ($publication->vetting_status == "hod")
                                    <label class="badge badge-primary text-uppercase">{{$publication->vetting_status}}</label>
                                    @elseif($publication->vetting_status == "principal")
                                    <label class="badge badge-secondary text-uppercase">{{$publication->vetting_status}}</label>
                                    @elseif($publication->vetting_status == "rejected")
                                    <label class="badge badge-danger text-uppercase">{{$publication->vetting_status}}</label>
                                    @elseif($publication->vetting_status == "drp")
                                    <label class="badge badge-info text-uppercase">{{$publication->vetting_status}}</label>
                                    @elseif($publication->vetting_status == "vetted" || $publication->vetting_status == "Vetted" || $publication->vetting_status == "review" || $publication->vetting_status == "reviewed")
                                    <label class="badge badge-success text-uppercase">Vetted</label>
                                    {{-- @else --}}
                                    {{-- <label class="badge badge-success text-uppercase">{{$publication->vetting_status}}</label> --}}
                                    @endif
                                </td>
                                <td>
                                    @if($publication['review_status'] == 'review')
                                    <label class="badge badge-secondary text-uppercase">On review</label>
                                    @elseif($publication['review_status'] == 'reviewed')
                                    <label class="badge badge-info text-uppercase">DONE</label><br>
                                    {{$publication['review']['overall']}}
                                    @else
                                    <label class="badge badge-success text-uppercase">On vetting</label>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{route('show_publication',['id'=>$publication->id])}}" title="Details">
                                        <i class="fa fa-folder-open fa-lg text-primary"></i>
                                    </a> &nbsp;

                                    <a href="{{route('edit_publication',['id'=>$publication->id])}}" title="Edit">
                                        <i class="fa fa-pencil-square text-dark fa-lg"></i>
                                    </a>&nbsp;

                                    <a class="delete" href="{{route('delete_publication',['id'=>$publication->id])}}" title="Delete">
                                        <i class="fa fa-trash fa-lg text-danger"></i>
                                    </a>&nbsp;
                                    @if(\App\Perms::perm_methods('publications','assign_panel'))
                                    <a class="assign" href="{{route('assign_panel',['id'=>$publication->id])}}" title="Assign reviwers">
                                        <i class="fa fa-chevron-circle-right fa-lg text-secondary"></i>
                                    </a>&nbsp;
                                    @endif
                                </td>
                            </tr>
                            @php ($serial++)
                            @endforeach
                        </tbody>
                    </table>
                    @endif

                    @if (isset($data['reviews']) && count($data['reviews']) > 0)
                    <div class="card card-flat">
                        <h6 class="card-header text-uppercase">Review Publications</h6>
                    </div>
                    <table id="myTable" class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th width="3%">#</th>
                                <th width="20%">Title</th>
                                <th width="6%">Year</th>
                                <th width="10%">Type of Publication</th>
                                <th width="8%">Media Name</th>
                                <th width="8%">Publisher</th>
                                {{-- <th width="5%">Points</th> --}}
                                <th width="8%">Vetting Status</th>
                                <th width="8%">Review Status</th>
                                <th width="10%"></th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $serial = 1; ?>
                            @php($serial = 1)
                            @foreach ($data['reviews'] as $publication)
                            <tr>
                                <td>{{$serial}}</td>
                                <td><a href="{{route('show_publication',['id'=>$publication->id])}}">{{$publication->publication_title}}</a> </td>
                                <td>{{$publication->year}}</td>
                                <td>{{$publication['type']['name']}}</td>
                                <td class="">
                                    @if ($publication->publication_type == "1")
                                    {{$publication->book_author}}
                                    @elseif($publication->book_title == "2")
                                    {{$publication->journal_name}}
                                    @elseif($publication->publication_type == "3")
                                    {{$publication->journal_name}}
                                    @elseif($publication->publication_type == "4")
                                    {{$publication->conference_publication_name}}
                                    @endif
                                </td>
                                <td>{{$publication->publisher}}</td>
                                {{-- <td></td> --}}
                                {{-- <td class="text-capitalize">{{$publication->vetting_status}}</td> --}}
                                <td class="">
                                    @if ($publication->vetting_status == "hod")
                                    <label class="badge badge-primary text-uppercase">{{$publication->vetting_status}}</label>
                                    @elseif($publication->vetting_status == "principal")
                                    <label class="badge badge-secondary text-uppercase">{{$publication->vetting_status}}</label>
                                    @elseif($publication->vetting_status == "rejected")
                                    <label class="badge badge-danger text-uppercase">{{$publication->vetting_status}}</label>
                                    @elseif($publication->vetting_status == "drp")
                                    <label class="badge badge-info text-uppercase">{{$publication->vetting_status}}</label>
                                    @elseif($publication->vetting_status == "Vetted" || $publication->vetting_status == "vetted" || $publication->vetting_status == "review" || $publication->vetting_status == "reviewed")
                                    <label class="badge badge-success text-uppercase">{{$publication->vetting_status}}</label>
                                    {{-- @else --}}
                                    {{-- <label class="badge badge-success text-uppercase">{{$publication->vetting_status}}</label> --}}
                                    @endif
                                </td>
                                <td>
                                    @if($publication['review_status'] == 'review')
                                    <label class="badge badge-secondary text-uppercase">On review</label>
                                    @elseif($publication['review_status'] == 'reviewed')
                                    <label class="badge badge-info text-uppercase">DONE</label><br>
                                    {{$publication['review']['overall']}}
                                    @else
                                    <label class="badge badge-success text-uppercase">On vetting</label>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{route('show_publication',['id'=>$publication->id])}}" title="Details">
                                        <i class="fa fa-folder-open fa-lg text-primary"></i>
                                    </a> &nbsp;

                                    <a href="{{route('edit_publication',['id'=>$publication->id])}}" title="Edit">
                                        <i class="fa fa-pencil-square text-dark fa-lg"></i>
                                    </a>&nbsp;

                                    <a class="delete" href="{{route('delete_publication',['id'=>$publication->id])}}" title="Delete">
                                        <i class="fa fa-trash fa-lg text-danger"></i>
                                    </a>&nbsp;

                                    @if(\App\Perms::perm_methods('publications','review_tool'))
                                    <a class="review" href="{{route('review_tool',['id'=>$publication->id])}}" title="Review publication">
                                        <i class="fa fa-check fa-lg text-secondary"></i>
                                    </a>&nbsp;
                                    @endif
                                </td>
                            </tr>
                            @php ($serial++)
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
                <!--./col-lg-9 -->
            </div>
            <!--./row -->
            @if ($data['publications']->count()>0)
            <div class="row mt-2">
                {{-- <div class="col-lg-6">
                        <div class="form-group">
                            
                        </div>
                    </div>
                    <div class="col-lg-3 text-left">
                        <div class="form-group">
                            @if (Auth::user()->hasRole("drp"))
                                <a href="{{route('upc')}}" class="btn btn-primary btn-xs">FORWARD TO UPC</a>
                @endif
            </div>
        </div>
        <div class="col-lg-3 text-left">
            <div class="form-group">
                @if (Auth::user()->hasRole("drp"))
                @if(\App\Perms::perm_methods('publications','assign_commit'))
                <a href="{{route('assign_commit')}}" class="btn btn-primary btn-xs">Assign committee members</a>
                @endif
                @endif
            </div>
        </div> --}}
    </div>
    <!--./row -->
    @endif
    </div>
    <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection