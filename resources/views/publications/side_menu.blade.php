<div id="left_menu">
    <ul id="nav">


        <li><a href=" {{ route('publications.stats') }} ">Summary</a></li>

        <?php
        use App\Perms;
        if (Perms::perm_methods('publications', 'lists'))
            echo '<li><a href="' . url('publications') . '">Publications</a></li>';
        ?>
        @if (Auth::user()->hasAnyRole(["hod","principal","drp","dvc","vc","admin"]))
            <li><a href="{{url('publications/process')}}">My Basket <span
                            class="badge badge-danger">{{$data['notifications']}}</span></a></li>
        @endif
        @if (Auth::user()->hasAnyRole(["admin"]))
            <li><a href="{{route('publications.type')}}">Publication Types</a></li>
        @endif
        @if (Auth::user()->hasAnyRole(["admin"]))
            <li><a href="{{route('publications.batches')}}">Vetting Batches</a></li>
        @endif
        {{-- @if (Auth::user()->hasAnyRole(["admin"]))
        <li><a href="{{ route('publications.report_publication') }}">Reports</a></li>
        @endif --}}
        @if (Auth::user()->hasAnyRole(["admin"]))
            <li><a href="{{ route('publications.vet_list') }}">Vet List</a></li>
        @endif
        @if (Auth::user()->hasAnyRole(["admin"]))
            <li><a href="{{ route('publications.vetted_list') }}">Import VETTED Batch</a></li>
        @endif
    </ul>
</div>