@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <h4>Publication details</h4>
                </div><!-- Page Title -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <!-- sidebar -->
                <div class="col-lg-2 col-md-2">
                    @include('publications.side_menu')
                </div>

                <div class="col-lg-10 col-md-10">
                    @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div><br />
                    @elseif(session()->get('danger'))
                    <div class="alert alert-danger">
                        {{ session()->get('danger') }}
                    </div><br />
                    @endif
                    <div class="card card-flat">
                        {{-- <h5 class="card-header text-uppercase">Publication Information</h5> --}}
                        <div class="card-body">
                            <div class="row p-2">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Full Name </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['names']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Title/Rank </span>
                                            </td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['rank']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">College </span>
                                            </td>
                                            <td width="80%">
                                                {{-- <span class="text-uppercase">{{$data['publication']['college']}}</span><br> --}}
                                                <span class="text-uppercase">{{ $data['college']->name }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Department </span></td>
                                            <td width="80%">
                                                {{-- <span class="text-uppercase">{{$data['publication']['department']}}</span><br> --}}
                                                <span class="text-uppercase">{{ $data['department']->name }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Year </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['year']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Gender </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['gender']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Category </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['category']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Type of Publication </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['type']['name']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Title of the publication </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['publication_title']}}</span>
                                            </td>
                                        </tr>
                                        @if ($data['publication']['publication_type'] === 1)
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">City </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['city']}}</span>
                                            </td>
                                        </tr>
                                        @endif
                                        @if ($data['publication']['publication_type'] === 2)
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Book Author </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['book_author']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Book title </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['book_title']}}</span>
                                            </td>
                                        </tr>
                                        @endif
                                        @if ($data['publication']['publication_type'] === 3)
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Journal Name </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['journal_name']}}</span>
                                            </td>
                                        </tr>
                                        @endif
                                        @if ($data['publication']['publication_type'] === 4)
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Conference publication Name </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['conference_publication_name']}}</span>
                                            </td>
                                        </tr>
                                        @endif
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Publisher </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['publisher']}}</span>
                                            </td>
                                        </tr>
                                        {{-- <tr>
                                                <td width="20%"><span class="font-weight-bold">Page(s) </span></td>
                                                <td width="80%">
                                                    <span class="text-uppercase">{{$data['publication']['pages']}}</span>
                                        </td>
                                        </tr> --}}
                                        @if ($data['publication']['publication_type'] === 3)
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Volume </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['volume']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Issue </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['issue']}}</span>
                                            </td>
                                        </tr>
                                        @endif
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Accessibility </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['accessibility']}}</span>
                                            </td>
                                        </tr>
                                        @if ($data['publication']['accessibility'] === "online")
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Link </span></td>
                                            <td width="80%">
                                                <span class=""><a href="{{$data['publication']['link']}}" target="_blank">{{$data['publication']['link']}}</a></span>
                                            </td>
                                        </tr>
                                        @endif
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Indexing </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['index']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Article Processing charges </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['processing_charge']}}</span>
                                            </td>
                                        </tr>
                                        @if ($data['publication']['processing_charge'] === "yes")
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Currency </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['currency']}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Amount </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{$data['publication']['amount']}}</span>
                                            </td>
                                        </tr>
                                        @endif
                                        @if ($data['publication']['attach_doc'])
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Attached document </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase"><a class="form-control" href="{{asset('updloads')}}/{{$data['publication']->attach_doc}}" target="_blank"><i class="fa fa-download fa-lg text-success"></i> Publication document</a></span>
                                            </td>
                                        </tr>
                                        @endif
                                        @if ($data['publication']['consultancy_receipt'])
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Attached Consultancy IF Receipt </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase"><a class="form-control" href="{{asset('updloads')}}/{{$data['publication']->consultancy_receipt}}" target="_blank"><i class="fa fa-download fa-lg text-success"></i> Consultancy Fee Receipt</a></span>
                                            </td>
                                        </tr>
                                        @endif
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Submission date </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{date_format($data['publication']['created_at'], "d F Y")}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="font-weight-bold">Batch Number </span></td>
                                            <td width="80%">
                                                <span class="text-uppercase">{{ $data['publication']['batch_id'] }}</span>
                                            </td>
                                        </tr>

                                        <br>
                                        <table class="table table-bordered">
                                            <tr>
                                                <td width="20%"><span class="font-weight-bold"> Recommendation on the quality of the media</span>
                                                </td>
                                                <td width="80%">
                                                    @if (isset($data['publication']['recommendation']) && $data['publication']['level']==='10')
                                                        <span class="text-uppercase" style="color:blue;font-weight:bold;">{{ $data['publication']['recommendation'] }}</span>
                                                    @else
                                                        <span class="text-uppercase"> Vetting in progress </span>
                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                        {{-- @endif    --}}
                                    </table>
                                </div>
                                <div class="col-md-2"></div>
                            </div>


                            @if (count($data['recommendations'])>0)

                            <div class="row">
                                <div class="col-lg-6">
                                    <table width="100%" class="table table-hover table-bordered table-responsive">
                                        <thead>
                                            <tr>
                                                <th width="3%">From</th>
                                                <th width="20%">Comments</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data['recommendations'] as $recommend)
                                            <tr>
                                                <td>{{$recommend['from']}}</td>
                                                <td>{{$recommend['comments']}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!--./col-lg-6 -->
                            </div>
                            <!--./row -->
                            @endif

                            {{-- actions when media is vetted --}}
                            @if ($data['publication']['vetting_status'] === "approved")
                            @if ($data['review']['status'] !== "DONE")
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        @if (Auth::user()->hasRole("hod"))
                                        <a href="{{route('assign_panel',['id'=>$data['publication']->id])}}" class="btn btn-primary btn-xs">Assign Reviewers</a>
                                        @endif
                                        @if (Auth::user()->hasRole("hod"))
                                        <a href="{{route('review_tool',['id'=>$data['publication']->id])}}" class="btn btn-primary btn-xs">Review</a>
                                        @endif
                                        <a href="{{url('publications')}}" class="btn btn-danger btn-xs">BACK</a>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            @endif
                            @else
                            {{-- actions for level 1 --}}
                            @if ($data['publication']['level'] === "1")
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        @if (Auth::user()->hasRole("hod"))
                                        <a href="{{route('back_panel',['id'=>$data['publication']->id])}}" class="btn btn-warning btn-xs">REJECT</a>
                                        <a href="{{route('forward_panel',['id'=>$data['publication']->id])}}" class="btn btn-primary btn-xs">FORWARD TO PRINCIPAL</a>
                                        @endif
                                        <a href="{{url('publications')}}" class="btn btn-danger btn-xs">BACK</a>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            @endif

                            {{-- actions for level 2 --}}
                            @if ($data['publication']['level'] === "2")
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        @if (Auth::user()->hasRole("principal"))
                                        <a href="{{route('back_panel',['id'=>$data['publication']->id])}}" class="btn btn-warning btn-xs">RETURN TO HOD</a>
                                        <a href="{{route('forward_panel',['id'=>$data['publication']->id])}}" class="btn btn-primary btn-xs">FORWARD TO DRP</a>
                                        @endif
                                        <a href="{{url('publications')}}" class="btn btn-danger btn-xs">BACK</a>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            @endif

                            {{-- actions for level 3 --}}
                            @if ($data['publication']['level'] === "3")
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        @if (Auth::user()->hasRole("drp"))
                                        <a href="{{route('back_panel',['id'=>$data['publication']->id])}}" class="btn btn-warning btn-xs">RETURN TO PRINCIPAL</a>
                                        <a href="{{route('board_panel',['id'=>$data['publication']->id])}}" class="btn btn-primary btn-xs">Vetting</a>
                                        @endif
                                        <a href="{{url('publications')}}" class="btn btn-danger btn-xs">BACK</a>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            @endif

                            @if ($data['publication']['level'] === "4")
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        @if (Auth::user()->hasRole("dvc"))
                                        <a href="{{route('forward_panel',['id'=>$data['publication']->id])}}" class="btn btn-primary btn-xs">FORWARD TO VC</a>
                                        @endif
                                        <a href="{{url('publications')}}" class="btn btn-danger btn-xs">BACK</a>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            @endif

                            {{-- @if ($data['publication']['vetting_status'] !== "approved") --}}

                            @if ($data['publication']['level'] === "5")
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        @if (Auth::user()->hasRole("vc"))
                                        <a href="{{route('senate_approve',['id'=>$data['publication']->id])}}" class="btn btn-primary btn-xs">Approved</a>
                                        <a href="{{route('vc_disapprove',['id'=>$data['publication']->id])}}" class="btn btn-danger btn-xs">Not Approved</a>
                                        @endif
                                        @if($data['publication']->remark === "approved")
                                        <a href="{{route('generate_certificate',['id'=>$data['publication']->id])}}" class="btn btn-primary btn-xs">Certificate</a>
                                        @endif
                                        <a href="{{url('publications')}}" class="btn btn-danger btn-xs">BACK</a>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            @endif
                            @if ($data['publication']['level'] === "6")
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        @if($data['publication']->remark === "approved")
                                            <a href="{{route('generate_certificate',['id'=>$data['publication']->id])}}" class="btn btn-primary btn-xs">Certificate</a>
                                        @endif
                                        <a href="{{url('publications')}}" class="btn btn-danger btn-xs">BACK</a>
                                    </div>
                                </div>
                            </div>
                            <!--./row -->
                            @endif

                            {{-- @endif --}}

                            @endif
                        </div>
                        <!--./card-body -->
                    </div>
                    <!--./card -->
                </div>
                <!--./col-lg-9 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection