@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="page-title">
                        <h4>SEND BACK</h4>
                        <ul>
                            <li><a href="{{url('/')}}" title=""><i class="fa fa-home"></i> Home</a></li>
                            <li><a href="{{url('publications')}}" title="">Publications</a></li>
                            <li>Add recomendation</li>
                        </ul>
                    </div><!-- Page Title -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <!-- sidebar -->
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-flat">
                            <h5 class="card-header">REVERSE COMMENTS</h5><!--./card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="title text-uppercase"></h6>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->
                                {{ Form::open(array('url' =>['publication/back_recomend','id' => $id],'id' => 'publication_back')) }}
                                <input type="hidden" name="_method" value="GET"/>
                                
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Add Comments</label>
                                            {{Form::textarea('recomendation', old('recomendation'), ['class="form-control" rows="5"'])}}
                                            <span class="text-danger">{{ $errors->first('recomendation') }}</span>
                                        </div><!--./form-group -->
                                    </div><!--./col-lg-6 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <a href="javascript:{}" onclick="document.getElementById('publication_back').submit();"
                                               class="btn btn-primary btn-xs" type="submit">Submit</a>
                                            <a href="{{route('show_publication',['id'=>$id])}}"
                                               class="btn btn-danger btn-xs">Cancel</a>
                                        </div>
                                    </div>
                                </div><!--./row -->
                            {{ Form::close() }}

                            </div><!--./card-body -->
                        </div><!--./card -->
                    </div><!--./col-lg-9 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
    {{-- publication scripts --}}
    
@endsection