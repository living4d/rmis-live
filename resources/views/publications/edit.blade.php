@extends('layouts.admin_app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <h4>Edit Publication</h4>
                    <ul>
                        <li><a href="{{url('/')}}" title=""><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="{{url('publications')}}" title="">Publications</a></li>
                        <li>Edit Publication</li>
                    </ul>
                </div><!-- Page Title -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <!-- sidebar -->
                <div class="col-lg-2 col-md-2">
                    @include('publications.side_menu')
                </div>
                <!--col-md-2 -->

                <div class="col-lg-10 col-md-10">
                    @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div><br />
                    @elseif(session()->get('danger'))
                    <div class="alert alert-danger">
                        {{ session()->get('danger') }}
                    </div><br />
                    @endif
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-flat">
                            <h5 class="card-header">PUBLICATION FORM</h5>
                            <!--./card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="title text-uppercase">Author Information</h6>
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->
                                {{ Form::open(['url' => route('update_publication', $data['publication']->id),'files'=>'true', 'class="form-horizontal" id="edit_form" files="true"']) }}
                                @method('GET')
                                {{Form::token()}}

                                <table class="table table-bordered">
                                    <tr>
                                        <td width="20%"><span class="font-weight-bold">Full Name </span>
                                        </td>
                                        <td width="80%">
                                            <span class="text-uppercase">{{$data['publication']['names']}}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="font-weight-bold">Gender </span>
                                        </td>
                                        <td width="80%">
                                            <span class="text-uppercase">{{$data['publication']['gender']}}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="font-weight-bold">Title/Rank </span>
                                        </td>
                                        <td width="80%">
                                            <span class="text-uppercase">{{$data['publication']['rank']}}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="font-weight-bold">College </span>
                                        </td>
                                        <td width="80%">
                                            {{-- <span class="text-uppercase">{{$data['publication']['college']}}</span><br> --}}
                                            <span class="text-uppercase">{{ $data['author_college']->name }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="font-weight-bold">Department </span></td>
                                        <td width="80%">
                                            {{-- <span class="text-uppercase">{{$data['publication']['department']}}</span><br> --}}
                                            <span class="text-uppercase">{{ $data['author_dept']->name }}</span>
                                        </td>
                                    </tr>
                                </table>
                                <hr />
                                <div class="row">
                                    <label><span class="badge badge-warning">NOTE: In the Category field: Senior Lecturers seeking the Associate professor rank and Associate professors seeking Full professor rank are required to select PROFESSORIAL category<br>
                                            Lecturers and other junior academic staff are required to select NON PROFESSORIAL category. Please follow this rule to avoid your publication to be misplaced during vetting</span> </label>

                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label>Category(Professorial/Non-Professorial)<span style="color: red;">*</span></label>
                                            <select class="form-control" name="category" id="">
                                                <option value="{{$data['publication']->category}}" selected>{{$data['publication']->category}}</option>
                                                <option value="Professorial">Professorial</option>
                                                <option value="Non-Professorial">Non-Professorial</option>
                                            </select>
                                            <span class="text-danger">{{ $errors->first('category') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-8 ">
                                        <div class="form-group">
                                            <label>Title of the publication<span style="color: red;">*</span></label>
                                            <input class="form-control" name="publication_title" type="text" value="{{$data['publication']->publication_title}}">
                                            <span class="text-danger">{{ $errors->first('publication_title') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-6 -->
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Year of publication <span style="color: red;">*</span></label>
                                            {{Form::selectYear('year',date('Y')-25,date('Y')+1, old('year',$data['publication']->year), ['class'=> 'form-control',])}}
                                            <span class="text-danger">{{ $errors->first('year') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-6 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Type of Publication <span style="color: red;">*</span></label>

                                            <select class="form-control pub_type" name="publication_type" id="">
                                                <option value="{{$data['pub_type']->id}}" selected>{{$data['pub_type']->name}}</option>
                                                @foreach ($data['publication_types'] as $type)
                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="text-danger">{{ $errors->first('publication_type') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-6 -->
                                    <div class="col-lg-6">
                                        <div class="form-group consul_type">
                                            <label>UDSM Registration Number<span style="color: red;">*</span></label>
                                            <input type="text" name="consultancy_reg_number" class="form-control" value="{{$data['publication']->consultancy_reg_number}}">
                                            <span class="text-danger">{{ $errors->first('consultancy_reg_number') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group city">
                                            <label>City published </label>
                                            <input type="text" name="city" class="form-control" value="{{$data['publication']->city}}">
                                            <span class="text-danger">{{ $errors->first('city') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-6 -->

                                    <!--./col-lg-6 -->
                                    <div class="col-lg-6">
                                        <div class="form-group book_author">
                                            <label>Book Author <span style="color: red;">*</span></label>
                                            <input type="text" name="book_author" class="form-control" value="{{$data['publication']->book_author}}">
                                            <span class="text-danger">{{ $errors->first('book_author') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-4 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group book_title">
                                            <label>Book title <span style="color: red;">*</span></label>
                                            <input type="text" name="book_title" class="form-control" value="{{$data['publication']->book_title}}">
                                            <span class="text-danger">{{ $errors->first('book_title') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group isb_n">
                                            <label>ISBN<span style="color: red;">*</span></label>
                                            <input type="text" name="isbn" class="form-control" value="{{$data['publication']->isbn}}">
                                            <span class="text-danger">{{ $errors->first('isbn') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group journal_name">
                                            <label>Journal Name<span style="color: red;">*</span></label>
                                            <input type="text" name="journal_name" class="form-control" value="{{$data['publication']->journal_name}}">
                                            <span class="text-danger">{{ $errors->first('journal_name') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group iss_n">
                                            <label>ISSN<span style="color: red;">*</span></label>
                                            <input type="text" name="issn" class="form-control" value="{{$data['publication']->issn}}">
                                            <span class="text-danger">{{ $errors->first('issn') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group conf_name">
                                            <label>Conference publication Name<span style="color: red;">*</span></label>
                                            <input type="text" name="conference_publication_name" class="form-control" value="{{$data['publication']->conference_publication_name}}">
                                            <span class="text-danger">{{ $errors->first('conference_publication_name') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-6 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Publisher <span style="color: red;">*</span></label>
                                            <input type="text" name="publisher" class="form-control" value="{{$data['publication']->publisher}}">
                                            <span class="text-danger">{{ $errors->first('publisher') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group eis_sn">
                                            <label>eISSN<span style="color: red;">*</span></label>
                                            <input type="text" name="eissn" class="form-control" value="{{$data['publication']->eissn}}">
                                            <span class="text-danger">{{ $errors->first('eissn') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group pages">
                                            <label>Page(s) <span style="color: red;">*</span></label>
                                            <input type="text" name="pages" class="form-control" value="{{$data['publication']->pages}}">
                                            <span class="text-danger">{{ $errors->first('pages') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-6 -->

                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group volume">
                                            <label>Volume <span style="color: red;">*</span></label>
                                            <input type="text" name="volume" class="form-control" value="{{$data['publication']->volume}}">
                                            <span class="text-danger">{{ $errors->first('volume') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-6 -->
                                    <div class="col-lg-6">
                                        <div class="form-group issue">
                                            <label>Issue <span style="color: red;">*</span></label>
                                            <input type="text" name="issue" class="form-control" value="{{$data['publication']->issue}}">
                                            <span class="text-danger">{{ $errors->first('issue') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-6 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Accessibility <span style="color: red;">*</span></label>
                                            <select class="form-control linkto" name="accessibility">
                                                <option value="{{$data['publication']->accessibility}}" selected>{{$data['publication']->accessibility}}</option>
                                                <option value="online">Online</option>
                                                <option value="hardcopy">Hardcopy</option>
                                            </select>
                                            <span class="text-danger">{{ $errors->first('accessibility') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-6 -->

                                    <div class="col-lg-6">
                                        <div class="form-group link">
                                            <label>Link (url) <span style="color: red;">*</span></label>
                                            <input type="text" name="link" class="form-control" value="{{$data['publication']->link}}">
                                            <span class="text-danger">{{ $errors->first('link') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-6 -->
                                </div>
                                <!--./row -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Indexing <span style="color: red;">*</span></label>
                                            <textarea class="form-control" name="index" id="" cols="30" rows="10">{{$data['publication']->index}}</textarea>
                                            {{-- {{Form::textarea("index",$data['publication']->index, old('index'), ['class="form-control" rows="5" '])}} --}}
                                            {{-- <script>
                                                CKEDITOR.replace('index');
                                            </script> --}}
                                            <span class="text-danger">{{ $errors->first('index') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-6 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Article Processing charges <span style="color: red;">*</span></label>
                                            <select class="form-control charge" name="processing_charge" id="">
                                                <option value="{{$data['publication']->processing_charge}}" selected>{{$data['publication']->processing_charge}}</option>
                                                <option value="yes">Yes</option>
                                                <option value="no">No</option>
                                            </select>
                                            <span class="text-danger">{{ $errors->first('processing_charge') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-6 -->

                                    <div class="col-lg-3">
                                        <div class="form-group currency">
                                            <label>Currency </label>
                                            <select class="form-control" name="currency" id="">
                                                <option value="{{$data['publication']->currency}}" selected>{{$data['publication']->currency}}</option>
                                                <option value="tsh">TSH</option>
                                                <option value="dollar">Dollar</option>
                                                <option value="euro">euro</option>
                                                <option value="pound">Pound</option>
                                            </select>
                                            <span class="text-danger">{{ $errors->first('currency') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-6 -->
                                    <div class="col-lg-3">
                                        <div class="form-group amount">
                                            <label>Amount </label>
                                            <input type="text" name="amount" class="form-control" value="{{$data['publication']->amount}}">
                                            <span class="text-danger">{{ $errors->first('amount') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-6 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group ">
                                            <label>Attach publication document (pdf) <span class="doc1" style="color: red;">*</span></label>
                                            <input class="form-control" type="file" name="publication_attachment">
                                            <span class="text-danger">{{ $errors->first('publication_attachment') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-12 -->
                                    <div class="col-lg-6">
                                        <div class="form-group inst_fee">
                                            <label>Attach Institutional Fee Receipt (pdf) <span class="doc1" style="color: red;">*</span></label>
                                            <input class="form-control" type="file" name="receipt_attachment">
                                            <span class="text-danger">{{ $errors->first('receipt_attachment') }}</span>
                                        </div>
                                        <!--./form-group -->
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <a href="javascript:{}" onclick="document.getElementById('edit_form').submit();" class="btn btn-primary btn-xs" type="submit">Update</a>
                                            <a href="{{url('publications')}}" class="btn btn-danger btn-xs">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                                <!--./row -->
                                {{ Form::close() }}

                            </div>
                            <!--./card-body -->
                        </div>
                        <!--./card -->
                    </div>
                    <!--./col-lg-9 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
</section>
{{-- publication scripts --}}
{{-- publication scripts --}}
<script>
    $(document).ready(function() {
        $(".linkto").change(function() {
            $(this).find("option:selected").each(function() {
                var optionValue = $(this).attr("value");
                if (optionValue == "online") {
                    $(".link").show();
                    $(".doc1").hide();
                } else {
                    $(".link").hide();
                    $(".doc1").show();
                }
            });
        }).change();
    });

    $(document).ready(function() {
        $(".charge").change(function() {
            $(this).find("option:selected").each(function() {
                var optionValue = $(this).attr("value");
                if (optionValue == "yes") {
                    $(".currency").show();
                    $(".amount").show();
                } else {
                    $(".currency").hide();
                    $(".amount").hide();
                }
            });
        }).change();
    });

    $(document).ready(function() {
        $(".pub_type").change(function() {
            $(this).find("option:selected").each(function() {
                var optionValue = $(this).attr("value");
                if (optionValue == 1) {
                    $(".city").show();
                    $(".book_author").hide();
                    $(".book_title").hide();
                    $(".journal_name").hide();
                    $(".volume").hide();
                    $(".pages").hide();
                    $(".issue").hide();
                    $(".conf_name").hide();
                    $(".consul_type").hide();
                    $(".inst_fee").hide();
                    $(".isb_n").show();
                    $(".iss_n").hide();
                    $(".eis_sn").hide();
                } else if (optionValue == 2) {
                    $(".city").show();
                    $(".book_author").show();
                    $(".book_title").show();
                    $(".journal_name").hide();
                    $(".volume").hide();
                    $(".issue").hide();
                    $(".conf_name").hide();
                    $(".consul_type").hide();
                    $(".inst_fee").hide();
                    $(".isb_n").show();
                    $(".iss_n").hide();
                    $(".eis_sn").hide();
                } else if (optionValue == 3) {
                    $(".city").hide();
                    $(".book_author").hide();
                    $(".book_title").hide();
                    $(".journal_name").show();
                    $(".volume").show();
                    $(".issue").show();
                    $(".conf_name").hide();
                    $(".consul_type").hide();
                    $(".inst_fee").hide();
                    $(".isb_n").hide();
                    $(".iss_n").show();
                    $(".eis_sn").show();
                } else if (optionValue == 4) {
                    $(".city").show();
                    $(".book_author").hide();
                    $(".book_title").hide();
                    $(".journal_name").hide();
                    $(".volume").show();
                    $(".issue").hide();
                    $(".conf_name").show();
                    $(".consul_type").hide();
                    $(".inst_fee").hide();
                    $(".isb_n").hide();
                    $(".iss_n").show();
                    $(".eis_sn").show();
                } else if (optionValue == 5) {
                    $(".city").hide();
                    $(".book_author").hide();
                    $(".book_title").hide();
                    $(".journal_name").hide();
                    $(".volume").hide();
                    $(".issue").hide();
                    $(".conf_name").hide();
                    $(".consul_type").show();
                    $(".inst_fee").show();
                    $(".isb_n").hide();
                    $(".iss_n").hide();
                    $(".eis_sn").hide();
                } else {
                    $(".city").hide();
                    $(".book_author").hide();
                    $(".book_title").hide();
                    $(".journal_name").hide();
                    $(".volume").hide();
                    $(".issue").hide();
                    $(".conf_name").hide();
                    $(".consul_type").hide();
                    $(".inst_fee").hide();
                    $(".isb_n").hide();
                    $(".iss_n").hide();
                    $(".eis_sn").hide();
                }
            });
        }).change();
    });
</script>
@endsection