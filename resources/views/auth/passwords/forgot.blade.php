@extends('layouts.app')

@section('content')
    @guest
        <section class="bg-white mt-4 mb-4">
            <div class="container">
                <div class="row">
                <div class="col-md-4">
                </div>
                    <div class="col-md-4">
                        <div class="card card-flat pure-form">
                            <h4 class="text-center">Forgot Password</h4><hr />
                            <div class="card-body">
                                @if (session()->get('danger'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('danger') }}
                                    </div>
                                @endif
                                @if (session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div>
                                @endif

                                <form action="{{ route('forgot.password.link') }}" class="form-horizontal" method="POST">
                                    @csrf
                                    <p>
                                        Enter your email address and we will send you a link to reset your password
                                    </p>
                                    <div class="form-group">
                                        <label>Email <span class="text-danger">*</span></label>
                                        <input id="email" type="text" placeholder="Enter your email address..."
                                            class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                            name="email" value="{{ old('email') }}">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <!--./form-group -->

                                    <div class="form-group last">
                                        <button type="submit" class="btn btn-primary btn-block text-medium">
                                            {{ __('Send Reset Password Link') }} <i class="fa fa-chevron-circle-right"></i>
                                        </button>
                                    </div><!-- form-group -->
                                </form>
                                <a href="{{ route('login') }}" class="text-right text-primary">
                                {{ __('Login') }}
                                </a>
                            </div>
                            <!--./card-body -->
                        </div>
                        <!--./card -->
                    </div>
                    <!--./col-md-4 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </section>
        <!--./section -->
    @endguest
@endsection
