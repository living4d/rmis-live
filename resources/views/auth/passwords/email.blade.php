@extends('layouts.app')

@section('content')
<section class="page-wrapper">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <h4>Reset Password</h4>
                    <ul>
                        <li><a href="{{url('/')}}" title="">Home</a></li>
                        <li>Reset Password</li>
                    </ul>
                </div><!-- Page Title -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div><!-- page-header -->

    <div class="page-content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-4 col-sm-6 col-xs-12 col-lg-4">

                    <div class="card">
                        <div class="card-body">
                            <div class="mb-3 text-center">
                                <h6 class="text-uppercase font-weight-700">Reset Password</h6>
                                <p class="text-center">Write email address to reset password</p>
                            </div>

                            @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                            @endif

                            <form method="POST" action="{{ route('password.email') }}">
                                @csrf
                                <div class="form-group">
                                    <label for="email">{{ __('E-Mail Address') }}</label>
                                    <input id="email" type="email"
                                        class="form-control @error('email') is-invalid @enderror" name="email"
                                        value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <!--./form-group -->

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block text-uppercase text-medium">
                                        {{ __('Send Password Reset Link') }}
                                    </button>
                                </div>
                                <!--./form-group -->
                            </form>
                        </div>
                        <!--./card-body -->
                    </div>
                    <!--./card -->
                </div>
                <!--./col-md-8 -->
            </div>
            <!--./row -->
        </div>
        <!--./container -->
    </div>
    <!--./page-content -->
</section>
@endsection