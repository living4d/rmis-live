@extends('layouts.app')

@section('content')
    @guest
        <section class="bg-white mt-4 mb-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 h-100">

                        {!! $content->content !!}
                        <div class="mt-4"></div>

                        <h5 class="text-uppercase">Open Calls</h5>
                        @if ($calls)
                            @foreach ($calls as $call)
                                <div class="item_wr">
                                    <div class="content">
                                        <h6>
                                            <a href="{{ url('proposals/details/' . $call->id) }}">
                                                {{ $call->title }}
                                            </a>
                                        </h6>

                                        <ul class="meta">
                                            <li>
                                                <div><i class="fa fa-calendar" aria-hidden="true"></i>Posted
                                                    on {{ date('M d, Y', strtotime($call->created_at)) }}
                                                </div>
                                            </li>

                                            <li>
                                                <div>Deadline : {{ date('M d, Y', strtotime($call->deadline)) }}</div>
                                            </li>

                                            <li>
                                                <div>
                                                    <a href="{{ url('proposals/details/' . $call->id) }}"
                                                        class="text-primary">Apply
                                                        For Call</a>
                                                </div>
                                            </li>
                                        </ul>
                                        <!--./stm-event__meta -->
                                    </div>
                                    <!--./content -->
                                </div>
                                <!--./item_wr -->
                            @endforeach
                            <a href="{{ url('proposals') }}" class="btn btn-outline-primary btn-sm">View all Calls</a>
                        @else
                            <div class="alert alert-danger">No any new call found</div>
                        @endif
                    </div>
                    <!--./col-lg-8 -->


                    <div class="col-md-4">
                        <div class="card card-flat pure-form">
                            <h6 class="text-uppercase text-center">LOGIN TO YOUR ACCOUNT</h6>
                            <p class="text-center font-weight-600">Enter your credentials below</p>
                            
                            <!--./card-header -->
                            <div class="card-body">
                                @if (session()->get('danger'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('danger') }}
                                    </div>
                                @endif

                                <form action="{{ route('login') }}" class="form-horizontal" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label>Username <span class="text-danger">*</span></label>
                                        <input id="username" type="text" placeholder="Write username..."
                                            class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}"
                                            name="username" value="{{ old('username') }}">
                                        @if ($errors->has('username'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <!--./form-group -->

                                    <div class="form-group">
                                        <label>Password <span class="text-danger">*</span></label>
                                        <input id="password" type="password" placeholder="Write password..."
                                            class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                            name="password" value="{{ old('password') }}">
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <!--./form-group -->

                                    <div class="form-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                                {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                    <!--./form-group -->

                                    <div class="form-group last">
                                        <button type="submit" class="btn btn-primary btn-block text-uppercase text-medium">
                                            {{ __('Login') }} <i class="fa fa-chevron-circle-right"></i>
                                        </button>
                                    </div><!-- form-group -->
                                </form>
                                <a href="{{ route('forgot.password.form') }}" class="text-right text-primary">
                                {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                            <!--./card-body -->
                        </div>
                        <!--./card -->
                    </div>
                    <!--./col-md-4 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </section>
        <!--./section -->
    @endguest
@endsection
