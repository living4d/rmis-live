@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Profile </h4>
                            <ul>
                                <li><a href="#">Dashboard</a></li>
                                <li>Profile</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div><!--./col-md-12 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('profile.sidebar_menu')
                    </div><!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="row">
                            <div class="col-md-4 pr-2">
                                <div class="info">
                                    <h5 class="text-uppercase font-weight-600">Personal Information</h5>
                                    <p>Name
                                        <span>{!! strtoupper($user->first_name) . ' ' . strtoupper($user->middle_name);
                                            !!}</span>
                                    </p>
                                    <p>Surname <span>{{ strtoupper($user->surname) }}</span></p>
                                </div>
                                <!--./info -->
                            </div>
                            <!--./col-md-3 -->
                    
                            <div class="col-md-4">
                                <div class="info">
                                    <h5 class="text-uppercase font-weight-600">&nbsp;&nbsp;</h5>
                                    <p>Email <span>{{$user->email}}</span></p>
                                    <p>Phone <span>{{$user->phone}}</span></p>
                                </div>
                                <!--./info -->
                            </div>
                            <!--./col-md-4 -->
                        </div>
                        <!--./row -->
                    
                        <div class="row">
                            <div class="col-md-12 pr-2">
                                <div class="info">
                                    <h5 class="text-uppercase font-weight-600">Work Information</h5>
                                    <p>Institution <span>{{$user->academic_qualification}}</span></p>
                                    <p>Specialization <span>{{$user->specialization}}</span></p>
                                    <p>Research Areas <span>{{$research_areas}}</span></p>
                                    @if ($user->cv != null)
                                    @if (file_exists(public_path().'/assets/uploads/cv/'.$user->cv))
                                    <p>Curricullum Vitae <span>
                                            <a href="{{public_path().'/assets/uploads/cv/'.$user->cv}}"
                                                class="btn btn-secondary btn-xs">Download</a>
                                        </span></p>
                                    @endif
                                    @endif
                                </div>
                                <!--./info -->
                            </div>
                            <!--./col-md-4 -->
                        </div>
                        <!--./row -->
                    
                        <div class="row">
                            <div class="col-md-12 pr-2">
                                <div class="info">
                                    <h5 class="text-uppercase font-weight-600">Other Information</h5>
                                    <p>Username <span>{{$user->username}}</span></p>
                    
                                    @if($level)
                                    <p>Level <span>{{$level->name}}</span></p>
                                    @endif
                    
                                    @if($college)
                                    <p>College <span>{{$college->name}}</span></p>
                                    @endif
                    
                                    @if($department)
                                    <p>Department <span>{{$department->name}}</span></p>
                                    @endif
                                </div>
                                <!--./info -->
                            </div>
                            <!--./col-md-4 -->
                        </div>
                        <!--./row -->
                    </div>
                    <!--./col-lg-10 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
@endsection