@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Change Password</h4>
                            <ul>
                                <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li>Change Password</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                    <!--./col-md-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('profile.sidebar_menu')
                    </div>
                    <!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat">
                            <div class="card-body">
                                <h5 class="text-uppercase">Change Password</h5>
                                <hr />

                                @if (session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div><br />
                                @endif

                                {{ Form::open(['url' => route('profile.change-password'), 'method="POST" class="form-horizontal"']) }}
                                {{ Form::token() }}

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Current Password <span class="red">*</span></label>
                                            {{ Form::password('current_password', ['placeholder="Write a current password..."', 'class="form-control"']) }}
                                            <span class="text-danger">{{ $errors->first('current_password') }}</span>
                                        </div>
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>New Password <span class="red">*</span></label>
                                            {{ Form::password('new_password', ['placeholder="Write a new password..."', 'class="form-control"']) }}
                                            <span class="text-danger">{{ $errors->first('new_password') }}</span>
                                        </div>
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Confirm Password <span class="red">*</span> </label>
                                            {{ Form::password('new_password_confirm', ['placeholder="Confirm password..."', 'class="form-control"']) }}
                                            <span class="text-danger">{{ $errors->first('new_password_confirm') }}</span>
                                        </div>
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="float-right">
                                            <div class="form-group">
                                                <button type="submit"
                                                    class="btn btn-secondary text-medium">Change</button>
                                                <a href="{{ url('/profile') }}"
                                                    class="btn btn-danger text-medium">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--./col-lg-12 -->
                                </div>
                                <!--./row -->
                                {{ Form::close() }}
                            </div>
                            <!--./card-body -->
                        </div>
                        <!--./card -->
                    </div>
                    <!--./col-lg-12 -->
                </div>
                <!--./row -->
            </div>
            <!--./container -->
        </div>
        <!--./page-content -->
    </section>
@endsection
