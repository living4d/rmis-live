@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Edit User</h4>
                            <ul>
                                <li><a href="#" title="">Dashboard</a></li>
                                <li><a href="{{url('/users')}}" title=""> Users</a></li>
                                <li>Edit user</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('users.sidebar_menu')
                    </div><!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat pure-form">
                            <h5 class="card-header">Edit User</h5><!--./card-header -->
                            <div class="card-body">

                                @if(session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div><br/>
                                @endif

                                {{ Form::open(['url' => route('roles.update', $role->id), 'class' => 'form-horizontal']) }}
                                @method('PUT')
                                {{ Form::token() }}

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Name <span class="red">*</span></label>
                                            {{ Form::text('name', $role->name, ['class="form-control"', 'placeholder="Write a role name..."']) }}
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                        </div>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Description <span class="red">*</span></label>
                                            {{ Form::textarea('description', $role->description, ['class="form-control"', 'placeholder="Write role description..."', 'rows' => 3]) }}
                                            <span class="text-danger">{{ $errors->first('description') }}</span>
                                        </div>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-secondary btn-xs text-medium">Update</button>
                                            <a href="{{url('roles')}}" class="btn btn-danger btn-xs text-medium">Cancel</a>
                                        </div>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->
                                {{ Form::close() }}
                            </div><!--./card-body -->
                        </div><!--./card -->
                    </div><!--./col-lg-12 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
@endsection
