@extends('layouts.admin_app')

@section('content')
    <section class="page-wrapper">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h4>Assign Permissions</h4>
                            <ul>
                                <li><a href="#" title="">Dashboard</a></li>
                                <li><a href="{{url('/roles')}}" title=""> Roles</a></li>
                                <li>Assign Permissions</li>
                            </ul>
                        </div><!-- Page Title -->
                    </div>
                </div><!--./row -->
            </div><!--./container -->
        </div><!-- page-header -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        @include('users.sidebar_menu')
                    </div><!--col-md-2 -->

                    <div class="col-lg-10 col-md-10">
                        <div class="card card-flat pure-form">
                            <h5 class="card-header">Assign Permissions : {{$role->name}} - {{$role->description}}</h5>
                            <!--./card-header -->
                            <div class="card-body">

                                @if(session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div><br/>
                                @endif

                                {{ Form::open(['url' => url('roles/store_perms/'.$role->id), 'method' => 'POST', 'class' => 'form-horizontal']) }}
                                {{ Form::token() }}

                                @foreach($perms as $key => $value)
                                <div class="mt-3"></div>
                                    <h6 class="page-header"> {{$key}}</h6>
                                    <input type="hidden" name="classes[]" value="{{$key}}">
                                    <input type="hidden" name="role_id" value="{{$role->id}}">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table>
                                                <tr>
                                                    <?php $serial = 0; ?>
                                                    @foreach ($value as $k => $v)
                                                        @if (($serial % 10) == 0)
                                                </tr>
                                                <tr>
                                                    @endif
                                                    <td>
                                                        <input type="checkbox" name="perms[]"
                                                               value="{{$v[1]}}" {{ (in_array($v[1], $assigned_perms)) ? 'checked' : '' }} />
                                                        <label>{{$v[2]}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>&nbsp;
                                                    </td>
                                                    <?php $serial++;?>
                                                    @endforeach
                                                </tr>
                                            </table>
                                        </div><!-- ./col-sm-12 -->
                                    </div><!-- ./row -->
                                @endforeach

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-secondary btn-xs text-medium">Update</button>
                                            <a href="{{url('/roles')}}" class="btn btn-danger btn-xs text-medium">Cancel</a>
                                        </div>
                                    </div><!--./col-lg-12 -->
                                </div><!--./row -->
                                {{ Form::close() }}
                            </div><!--./card-body -->
                        </div><!--./card -->
                    </div><!--./col-lg-12 -->
                </div><!--./row -->
            </div><!--./container -->
        </div><!--./page-content -->
    </section>
@endsection
