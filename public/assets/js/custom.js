/*
 Copyright (C) Renfrid Ngolongolo
 Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).*/
//Document => Start Jquery
$(document).ready(function() {
    //apply tooltip
    $('[data-toggle="tooltip"]').tooltip();

    //approve application
    $('#approveModal').on('show.bs.modal', function(e) {
        //data
        appId = $(e.relatedTarget).data('app-id');
        roleName = $(e.relatedTarget).data('role-name');

        //url 
        baseURL = window.location.origin;

        //approval Level
        $('#approval-level').text(roleName.toUpperCase() + " Approval");

        $.ajax({
            url: baseURL + "/applications/" + appId + "/api_details",
            type: 'get',
            success: function(data) {
                app = JSON.parse(data);

                //assign data view
                $('#projectTitle-2').text(app.title);
                $('#pi-2').text(app.pi);
                $('#introduction').html(app.introduction);
                $('#generalObjective').html(app.general_objective);
                $('#budget-2').text(app.budget);
                $('#submittedDate-2').text(app.submitted_date);
            }
        });

        //approve application
        $('#btn-approve').on('click', function(e) {
            $.ajax({
                url: baseURL + "/applications/" + appId + "/" + roleName + "/api_approve",
                type: "get",
                success: function(data) {
                    app = JSON.parse(data);

                    if (app.status === "success") {
                        $("#approveModal").modal('hide');
                        $('#btnApproveModal-' + appId).hide();

                        //messages
                        var messages = $('.messages');
                        var successHtml = '<div class="alert alert-success">Application approved successfully.</div>';
                        $(messages).html(successHtml);

                    } else if (app.status === "failed") {
                        $('#errorMsg-3').text('Failed to approve application');
                    }
                },
                beforeSend() {
                    $("#notificationBar-2").html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw" aria-hidden="true"></i> Please wait ...');
                    $("#errorMsg-3").html('');
                }
            });
        });
    });

    //assign evaluators
    $('#assignEvaluatorModal').on('show.bs.modal', function(e) {
        //data
        var callId = $(e.relatedTarget).data('call-id');
        console.log("callId => " + callId);

        var clusterId = $(e.relatedTarget).data('cluster-id');
        console.log("clusterId => " + clusterId);

        var clusterName = $(e.relatedTarget).data('cluster-name');
        console.log("clusterName => " + clusterName);

        //url
        baseURL = window.location.origin;

        //cluster name
        $('#cluster-name').text(clusterName);

        //if save evaluators
        $('#form-assign-evaluator').on('submit', function(e) {
            e.preventDefault();

            var evaluatorArr = [];
            $("input[type=checkbox]:checked").each(function() {
                evaluatorArr.push(this.value);
            });

            //selected evaluators
            if (evaluatorArr.length > 0) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: baseURL + "/calls/" + callId + "/assign_evaluators",
                    type: "POST",
                    data: { cluster_id: clusterId, evaluators: evaluatorArr },
                    dataType: 'JSON',
                    success: function(result) {
                        console.log(result.status);

                        if (result.status === "success") {
                            $("#assignEvaluatorModal").modal('hide');
                            location.reload();
                        } else if (result.status === "failed") {
                            $('#errorMsg').text('Failed to assign evaluator');
                        }
                    }
                });
            } else {
                $('#errorMsg').text('Evaluator not selected from list');
            }
        });
    });

    //drop evaluators
    $('#dropEvaluatorModal').on('show.bs.modal', function(e) {
        //data
        var callId = $(e.relatedTarget).data('call-id');
        console.log("callId => " + callId);

        var clusterId = $(e.relatedTarget).data('cluster-id');
        console.log("clusterId => " + clusterId);

        var clusterName = $(e.relatedTarget).data('cluster-name');
        console.log("clusterName => " + clusterName);

        //url
        baseURL = window.location.origin;

        //cluster name
        $('#cluster-name-2').text(clusterName);

        $.ajax({
            url: baseURL + "/calls/" + callId + "/" + clusterId + "/assigned_evaluators",
            type: "get",
            success: function(data) {
                var result = JSON.parse(data);

                console.log(result.status);
                $("#evaluators_lists").html("");

                //check for data
                let html = "";
                if (result.status === "success") {
                    console.log(result.evaluators);

                    $.each(result.evaluators, function(i, evaluator) {
                        html += '<input type="checkbox" name="evaluator_ids[]" id="evaluator_ids" value="' + evaluator.id + '" />&nbsp;';
                        html += '<label>' + evaluator.name + '</label><br/>';
                    });
                    $("#evaluators_lists").html(html);
                } else if (result.status === "failed") {
                    $("#evaluators_lists").html('No any evaluator');
                }
            }
        });

        //if drop
        $('#form-drop-evaluator').on('submit', function(e) {
            e.preventDefault();

            var evaluatorArr = [];
            $("input[type=checkbox]:checked").each(function() {
                evaluatorArr.push(this.value);
            });

            //selected evaluators
            if (evaluatorArr.length > 0) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: baseURL + "/calls/" + callId + "/drop_evaluators",
                    type: "POST",
                    data: { cluster_id: clusterId, evaluators: evaluatorArr },
                    dataType: 'JSON',
                    success: function(result) {
                        console.log(result.status);

                        if (result.status === "success") {
                            $("#dropEvaluatorModal").modal('hide');
                            location.reload();
                        } else if (result.status === "failed") {
                            $('#errorMsg-2').text('Failed to drop evaluator');
                        }
                    }
                });
            }
        });
    });

    //award or decline a call
    $('#awardModal').on('show.bs.modal', function(e) {
        //data
        var appId = $(e.relatedTarget).data('app-id');
        baseURL = window.location.origin;

        $.ajax({
            url: baseURL + "/applications/" + appId + "/api_details",
            type: "get",
            success: function(data) {
                //convert json object to object
                app = JSON.parse(data);

                //assign data view
                $('#projectTitle').text(app.title);
                $('#pi').text(app.pi);
                $('#category').text(app.category);
                $('#budget').text(app.budget);
                $('#submittedDate').text(app.submitted_date);
                $('#evaluationGrade').text(app.evaluation_grade);

                //hide and show
                if (app.winner == 0) {
                    $('#btn-award').show();
                    $('#btn-decline').hide();
                } else if (app.winner == 1) {
                    $('#btn-award').hide();
                    $('#btn-decline').show();
                }
            }
        });

        //award application
        $('#btn-award').on('click', function(e) {
            $.ajax({
                url: baseURL + "/applications/" + appId + "/api_award",
                type: "get",
                success: function(data) {
                    app = JSON.parse(data);

                    if (app.status === "success") {
                        $("#awardModal").modal('hide');
                        $('#btnAwardModal').hide();
                        $('#btnDeclineModal').show();

                        //messages
                        var messages = $('.messages');
                        var successHtml = '<div class="alert alert-success">Application awarded successfully.</div>';
                        $(messages).html(successHtml);

                    } else if (app.status === "failed") {
                        $('#errorMsg-2').text('Failed to award application');
                    }
                },
                beforeSend() {
                    $("#notificationBar").html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw" aria-hidden="true"></i> Please wait ...');
                    $("#errorMsg-2").html('');
                }
            });
        });

        //decline application
        $('#btn-decline').on('click', function(e) {
            $.ajax({
                url: baseURL + "/applications/" + appId + "/api_decline",
                type: "get",
                success: function(data) {
                    app = JSON.parse(data);

                    if (app.status === "success") {
                        $("#awardModal").modal('hide');
                        $('#btnDeclineModal').hide();

                        //messages
                        var messages = $('.messages');
                        var successHtml = '<div class="alert alert-danger">Application declined.</div>';
                        $(messages).html(successHtml);

                    } else if (app.status === "failed") {
                        $('#errorMsg-2').text('Failed to decline application');
                    }
                },
                beforeSend() {
                    $("#notificationBar").html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw" aria-hidden="true"></i> Please wait ...');
                    $("#errorMsg-2").html('');
                }
            });
        });
    });

    //register project modal
    $('#projectModal').on('show.bs.modal', function(e) {
        //data
        var appId = $(e.relatedTarget).data('app-id');
        baseURL = window.location.origin;

        $.ajax({
            url: baseURL + "/applications/" + appId + "/api_details",
            type: "get",
            success: function(data) {
                //convert json object to object
                app = JSON.parse(data);

                //assign data view
                $('#projectTitle').text(app.title);
                $('#projectNumber').text(app.project_number);
            }
        });

        $('#btn-register-project').on('click', function(e) {
            $.ajax({
                url: baseURL + "/applications/" + appId + "/api_register_project",
                type: "get",
                success: function(data) {
                    app = JSON.parse(data);

                    if (app.status === "success") {
                        $("#projectModal").modal('hide');
                        $('#btnRegisterModal').hide();

                        //messages
                        var messages = $('.messages');
                        var successHtml = '<div class="alert alert-success">Project registered.</div>';
                        $(messages).html(successHtml);

                    } else if (app.status === "failed") {
                        $('#errorMsg-2').text('Failed to register project');
                    }
                },
                beforeSend() {
                    $("#notificationBar").html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw" aria-hidden="true"></i> Please wait ...');
                    $("#errorMsg-2").html('');
                }
            });
        });
    });


    //project activities
    $('#activityModal').on('show.bs.modal', function(e) {
        //data
        var activityId = $(e.relatedTarget).data('activity-id');
        baseURL = window.location.origin;

        $.ajax({
            url: baseURL + "/activities/" + activityId + "/api_show",
            type: "get",
            success: function(data) {
                //convert json object to object
                activity = JSON.parse(data);
                console.log("activity => " + activity.title);

                //assign data view
                $('#projectTitle').text(activity.project_title);
                $('#objectiveTitle').text(activity.objective_title);
                $('#activityTitle').text(activity.title);
                $('#activityType').text(activity.activity_type);
                $('#responsible').text(activity.responsible);
                $('#startDate').text(activity.start_date);
                $('#endDate').text(activity.end_date);
                $('#location').text(activity.location);
                $('#budget').text(activity.budget);
                $('#justification').html(activity.justification);
                $('#expectedOutput').html(activity.expected_output);
                $('#expectedOutcome').html(activity.expected_outcome);
                $('#performanceIndicator').html(activity.performance_indicator);
                $('#baseline').html(activity.baseline);
                $('#outcomeTargets').html(activity.outcome_targets);
            }
        });
    });

    //project activity progress Report
    $('#activityReportModal').on('show.bs.modal', function(e) {
        //data
        var activityId = $(e.relatedTarget).data('activity-id');
        baseURL = window.location.origin;

        $.ajax({
            url: baseURL + "/activity-reports/" + activityId + "/api_show",
            type: "get",
            success: function(data) {
                //convert json object to object
                activity = JSON.parse(data);
                console.log("activity => " + activity.title);

                //assign data view
                $('#projectTitle').text(activity.project_title);
                $('#objectiveTitle').text(activity.objective_title);
                $('#activityTitle').text(activity.title);
                $('#activityType').text(activity.activity_type);
                $('#responsible').text(activity.responsible);
                $('#startDate').text(activity.start_date);
                $('#endDate').text(activity.end_date);
                $('#location').text(activity.location);
                $('#budget').text(activity.budget);
                $('#justification').html(activity.justification);
                $('#expectedOutput').html(activity.expected_output);
                $('#expectedOutcome').html(activity.expected_outcome);
                $('#performanceIndicator').html(activity.performance_indicator);
                $('#baseline').html(activity.baseline);
                $('#outcomeTargets').html(activity.outcome_targets);

                //progress
                $('#status').html(activity.status);
                $('#outputProduced').html(activity.output_produced);
                $('#achievedOutcome').html(activity.achieved_outcome);
                $('#additionalInformation').html(activity.additional_information);

            }
        });
    });


    ///Approve Project Fund Request
    $('#approveFundModal').on('show.bs.modal', function(e) {
        //hide button
        $('#btn-print-letter').hide();

        //data
        fundRequestId = $(e.relatedTarget).data('project-fund-request-id');
        roleName = $(e.relatedTarget).data('role-name');
        baseURL = window.location.origin;

        //approval Level
        $('#approval-level').text(roleName.toUpperCase() + " Approval");

        //check role name and change text
        if (roleName === 'dvc_research')
            $('#btn-approve-fund').html('<i class="fa fa-check"></i> Approve');
        else
            $('#btn-approve-fund').html('<i class="fa fa-check"></i> Recommend');

        $.ajax({
            url: baseURL + "/fund-requests/" + fundRequestId + "/api_details",
            type: 'get',
            success: function(data) {
                app = JSON.parse(data);

                //assign data view
                $('#projectTitle').text(app.project_title);
                $('#activityPerformed').html(app.activity);
                $('#requestedAmount').html(app.amount);
                $('#budgetAttachments').html(app.attachments);
                $('#submittedDate').text(app.created_at);

                //
            }
        });

        //btn return
        $('#btn-return-fund').on('click', function(e) {
            $('.form-remarks').show();
            $('.modal-footer').hide();
        });

        //cancel fund
        $('#btn-cancel-fund').on('click', function(e) {
            $('.form-remarks').hide();
            $('.modal-footer').show();
        });

        //save fund
        $('#btn-save-fund').on('click', function(e) {
            e.preventDefault();
            var remarks = $('#remarks').val();

            $.ajax({
                url: baseURL + "/fund-requests/" + fundRequestId + "/" + roleName + "/" + remarks + "/api_reject",
                type: "get",
                success: function(data) {
                    app = JSON.parse(data);

                    if (app.status === "success") {
                        $("#approveFundModal").modal('hide');
                        $('#btnApproveFundModal-' + fundRequestId).hide();

                        //reload the page
                        location.reload();

                        //messages
                        var successHtml = '<div class="alert alert-success">Project fund request returned with remarks.</div>';
                        $('#messages').html(successHtml);

                    } else if (app.status === "failed") {
                        $('#errorMsg').text('Failed to return project fund request');
                    }
                },
                beforeSend() {
                    $("#notificationBar").html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw" aria-hidden="true"></i> Please wait ...');
                    $("#errorMsg").html('');
                }
            });
        });

        //btn approve
        $('#btn-approve-fund').on('click', function(e) {
            e.preventDefault();

            //query
            $.ajax({
                url: baseURL + "/fund-requests/" + fundRequestId + "/" + roleName + "/api_approve",
                type: "get",
                success: function(data) {
                    app = JSON.parse(data);

                    if (app.status === "success") {
                        //check if dvc_research show action to print letter
                        if (roleName === 'dvc_research') {
                            //hide buttons
                            $('#btn-approve-fund').hide();
                            $('#btn-return-fund').hide();

                            //show print button
                            //$('#btn-print-letter').show();

                            //remove loading 
                            //$("#notificationBar").html('');

                            //redirect to printing page
                            window.location.href = '/fund-requests/' + fundRequestId + '/approval-letter';
                            return false;
                        } else {
                            //hide modal and reload page
                            $("#approveFundModal").modal('hide');
                            $('#btnApproveFundModal-' + fundRequestId).hide();

                            //reload the page
                            location.reload();

                            //messages
                            var successHtml = '<div class="alert alert-success">Project fund request recommended successfully.</div>';
                            $('#messages').html(successHtml);
                        }
                    } else if (app.status === "failed") {
                        $('#errorMsg').text('Failed to recommend project fund request');
                    }
                },
                beforeSend() {
                    $("#notificationBar").html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw" aria-hidden="true"></i> Please wait ...');
                    $("#errorMsg").html('');
                }
            });
        });

        //PRINT Approval Letter
        // $('#btn-print-letter').on('click', function (e) {
        //     e.preventDefault();

        //     window.location.href = '/fund-requests/' + fundRequestId + '/approval-letter';
        //     return true;
        // });
    });


    //printing
    $('#btn-print-document').on('click', function(e) {
        var options = {
            //'width': 800,
        };
        var pdf = new jsPDF('p', 'pt', 'a4');
        pdf.addHTML($("#print-contents"), 0, 0, options, function() {
            pdf.save('Approval-Letter.pdf');
        });
    });


    //select clusters after choosing call_id
    //on change
    $('#fiter_call_id').on('change', function(e) {
        let call_id = $(this).val();
        let baseURL = window.location.origin;

        $.ajax({
            url: baseURL + "/calls/get_clusters_from_call/" + call_id,
            type: "get",

            success: function(data) {
                console.log(data);

                $('#filter_cluster_id').html(data);
            }
        });
    });

    //approve
    $('.approve').on('click', function() {
        return confirm("Are you sure you want to approve?");
    });

    //delete
    $('.delete').on('click', function() {
        return confirm("Are you sure you want to delete?");
    });

    //format amount 
    $('#amount').on('keyup', function(e) {
        $amount = $(this).val();
        amount = amount.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        //return $amount;
    });

});


//suggest department
function suggest_departments() {
    let base_url = document.getElementById('url').value;
    let college_id = document.getElementById('college_id').value;

    //create XMLRequest object
    let xhr = new XMLHttpRequest(),
        method = "GET",
        url = base_url + "/colleges/departments/" + college_id;

    xhr.open(method, url, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            let response = xhr.responseText;
            e = document.getElementById('department_id');
            if (response !== "") {
                e.innerHTML = response;
                e.style.display = "block";
            } else {
                e.style.display = "none";
            }
        }
    };
    xhr.send();
}

//suggest research areas
function suggest_research_areas() {
    //form data
    let base_url = document.getElementById('url').value;
    let cluster_id = document.getElementById('cluster_id').value;

    //XMLhttpRequest Object
    let xhr = new XMLHttpRequest(),
        method = "GET",
        url = base_url + '/clusters/research_areas/' + cluster_id;

    xhr.open(method, url, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            let response = xhr.responseText;
            e = document.getElementById('research_area_id');
            if (response !== "") {
                e.innerHTML = response;
                e.style.display = "block";
            } else {
                e.style.display = "none";
            }
        }
    };
    xhr.send();
}

//format amount
function amountFormat() {
    let amount = document.getElementsByName("amount[]");

    for (let i = 0; i < amount.length; i++) {
        amount[i].value = amount[i].value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}

//format budget ceiling 
function budgetFormat() {
    let amount = document.getElementsByName("budget_ceiling[]");

    for (let i = 0; i < amount.length; i++) {
        amount[i].value = amount[i].value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}

//delete data
function deleteData() {
    var confirmDelete = confirm("Are you sure you want to delete?");

    if (confirmDelete) {
        return true;
    } else {
        e.preventDefault();
    }
}

//jquery
$(document).ready(function() {
    //calculate points from score
    // $('.score').on('keyup', function(e){
    //     var score = $('.score').val();
    //     var percent = $(this).attr('percent');

    //     //validate score
    //     if($.isNumeric(score)){
    //         console.log("score => " + score);
    //         console.log("percent => " + percent);
    //         if (score > percent) {
    //             //display error message
    //             $("#errmsg").html("Score should not exceed percentage value");
    //             return false;
    //         }else{
    //             $("#errmsg").html("");

    //             //calculate point
    //             var points = score*percent;
    //             $('.points').text(points);
    //         }   
    //     }else{
    //         //display error message
    //         $("#errmsg").html("Only numeric value");
    //         return false;
    //     }
    // });

    //validate inputs
    $('#formEv').on('submit', function(e) {
        //validate score
        // $('input[name^="score"]').each(function () {
        //     if(this.value.length === 0){
        //         $("#errmsg").html("Score required");
        //         return false;
        //     }else{
        //         return true
        //     }
        // });

        // var score = $('.score').val();
        // var comments = $('.comments').val();

        // if(score.length === 0){
        //     //display error message
        //     $("#errmsg").html("Score required");
        //     return false;  
        // } else if(comments.length === 0){
        //     //display error message
        //     $("#errmsg").html("Comments required");
        //     return false;
        // }else{
        //     return true;
        // }

    });

    //select all
    $('#select_all').click(function(event) { //on click
        if (this.checked) { // check select status
            $('.checkbox').each(function() { //loop through each checkbox
                this.checked = true; //select all checkboxes with class "checkbox"
            });
        } else {
            $('.checkbox').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox"
            });
        }
    });

});

//decimal with comma
$(document).ready(function() {
    document.getElementById('number_comma').addEventListener('input', event =>
        event.target.value = (parseInt(event.target.value.replace(/[^\d]+/gi, '')) || 0).toLocaleString('en-US')
    );
});

$(document).ready(function() {
    $('.hidebutton').hide();
});

$(document).ready(function() {

    $('#addButton').click(function() {

        // Create clone of <div class='input-form'>
        var old = $('.site');
        var newel = $('.site').clone();

        $(newel).removeClass('.hidebutton');
        $(newel).insertAfter(".site");

        $(old).removeClass('site');
    });

});